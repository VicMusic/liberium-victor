var idCausa =  $("#idCausa").val();

function comentar(){ 
	var id = $('#comentarioIdEditar').val();
	var comentario = "";
	if(id != null && id != ""){
		comentario = $("#contenidoEditar").val();
	}else{
		comentario = $("#contenido").val();
	}
	
	setTimeout(function(){ 
		
		$.ajax({
	        url: '/api/causa/comentar?id='+ id +'&contenido=' + comentario + '&idCausa=' + idCausa,
	        dataType: "json",
	        type: "POST",
	        success: function (data) {
	        	$("#agregar").hide();

	        	$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
		                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
		                + '<h4><i class="icon fa fa-info"></i> Info!</h4>'
		                + 'El comentario fue enviado con éxito'
		                + '</div>').show();
	        	
	        	setTimeout(function () {
	    			$("#alerta").hide();
	    	    }, 5000);

	        	window.location = '/administracion/causa/ver/' + idCausa;
	        	
	        },
	 		error: function (data){
	 			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
		                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
		                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
		                + 'Ha ocurrido un error al enviar el comentario. ' + data.responseJSON.message
		                + '</div>').show();
	 			setTimeout(function () {
	    			$("#modalAlerta").hide();
	    	    }, 5000);
	 		}
	    });	
	}, 3000);
	  
 }

function cerrarComentario(){
	$('#contenidoEditar').val("");
	$('#comentarioIdEditar').val("");
}

function editarComentario(componente){
	$('#contenidoEditar').val(componente.getAttribute('contenido'));
	$('#comentarioIdEditar').val(componente.getAttribute('idComentario'));
}


function eliminarComentario(componente){
	$('#comentarioId').val(componente.getAttribute('idComentario'));
}


function ComentarioEliminar() {
	console.log("entro eliminar");
	console.log($("#modalEliminarId").val());
	$
			.ajax({
				url : '/api/causa/eliminarComentario?id=' + $("#comentarioId").val(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#eliminar").hide();
					$("#modalConfirmar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'El comentario fue eliminado con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location = '/administracion/causa/ver/' + idCausa;

				},
				error : function(data) {
					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al eliminar el comentario. '
											+ data.responseJSON.message
											+ '</div>').show();
					$("#modalConfirmar").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);
				}
			});
}