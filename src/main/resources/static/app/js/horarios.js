function definirDuracion(){
	$.ajax({
		url : '/api/abogado/obtenerduracion',
		dataType : "json",
		type : "GET",
		async : true,
		success : function(data) {
			$("#duracion").val(data.duracion);
			if (data.duracion === 30) {
				mostrar30();
			}else{
				ocultar30();
			}
		},
	});	
	
}

function duracion(){
	var duracion = $("#duracion").val();
	if(duracion === '30'){
		mostrar30();
	} else {
		ocultar30();
	}
}

function mostrar30(){
	$(".celda30").css("display", "block");
	$(".celda1hora[minutos=30]").css("display", "block");
}

function ocultar30(){
	$(".celda30").css("display", "none");
	$(".celda1hora[minutos=30]").css("display", "none");
}

function cargarHorarios(){
	$.ajax({
		url : '/api/horario/listado',
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			jQuery.each(data, function(i, valor) {
				var campo = $("#" + valor.dia + valor.hora.replace(':',''));
				seleccionar(campo);
			});
		},
	});	
}

function cargarHorariosPerfil(){
	$.ajax({
		url : '/api/horario/listadoperfil',
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			jQuery.each(data, function(i, valor) {
				var campo = $("#" + valor.dia + valor.hora.replace(':',''));
				seleccionar(campo);
			});
		},
	});	
}

function modificarHorario() {
	$.ajax({
		url : '/api/horario/guardar',
		data : $("#horariosform").serialize(),
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			window.location.reload();
		},
		error : function(data) {
			$("#alerta")
					.empty()
					.html(
							'<div class="alert alert-danger alert-dismissible">'
									+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
									+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
									+ 'Ha ocurrido un error al crear el horario. '
									+ data.responseJSON.message
									+ '</div>').show();
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

var x = 0;
var y = 1;

function seleccionar(campo) {
	var id = campo.attr('id');
	var status = campo.attr('data-status');
	if (status === "paused") {
		campo.text('');
		campo.css('background-color', '#8fb23a');
		campo.attr('data-status', 'playing');

		$("#horariosform").append('<input type="hidden" name="horarios" id="C' + id + '" value="' + id + '"/>');

	} else {
		campo.text('Turno Disponible');
		campo.css('background-color', 'darkgrey');
		campo.attr('data-status', 'paused');
		$("#C" + id).remove();
	}
}

$('body #horarios').on('click', 'div', function() {

	var dia = $(this).attr('dia');

	switch (dia) {

	case '1':
		seleccionar($(this));
		break;
	case '2':
		seleccionar($(this));
		break;
	case '3':
		seleccionar($(this));
		break;
	case '4':
		seleccionar($(this));
		break;
	case '5':
		seleccionar($(this));
		break;
	case '6':
		seleccionar($(this));
		break;
	case '7':
		seleccionar($(this));
		break;
	}

});