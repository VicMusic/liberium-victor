$(document).ready(function() {
	/*
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
			+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
	*/
});

function cerrar() {
	window.location.href = window.location.origin + window.location.pathname + window.location.search;
}

function selectFuero() {
	var fuero = $("#tipoFuero").val();
	if (fuero == "NACIONAL") {
		$("#ocultable").attr("style", "display:none");
	} else if (fuero == 'PROVINCIAL') {
		$("#ocultable").removeAttr("style", "display:none");
	}
}

function getProvinciaChecklist(idPais, provincia) {
	console.log("Get Provincia");
	armarCombo(provincia, "provinciaChecklist", '/api/provincia/combo?idPais=' + idPais);
	$("#provinciaChecklist").removeAttr("disabled");
}


function getCircunscripcion(idProvincia, circunscripcion) {
	console.log("Get Circunscripciones");
	if (idProvincia != null && idProvincia != "") {
		armarCombo(circunscripcion, "circunscripcionChecklist", '/api/circunscripcion/combo?idProvincia=' + idProvincia);
		$("#circunscripcionChecklist").removeAttr("disabled");
	}
}

function getInstancia(idCircunscripcion, instancia) {
	console.log("getInstancia...");
	console.log("idCircunscripcion: " + idCircunscripcion);
	console.log("instancia: " + instancia);
	if (idCircunscripcion != null && idCircunscripcion != "") {
		armarCombo(instancia, "instanciaChecklist", '/api/instancia/combo?idCircunscripcion=' + idCircunscripcion);
	}
}

function getTipoCausa(idMateria) {
	console.log("idMateria: " + idMateria);
	armarCombo(tipoCausa, "tipoCausaChecklist", '/api/tipocausa/combo?idMateria=' + idMateria);
}

function resetFormulario() {
	$("#modalFormulario").trigger("reset");
	$(".select2").val(null).trigger('change');
	$("#circunscripcionChecklist").empty().trigger("change").prop("disabled", "disabled");
	//$("#instanciaChecklist").empty().trigger("change").prop("disabled", false);
	$("#instanciaChecklist option[value='0']").attr("selected", true);
	$("#bloqueItem").empty();
	$("#tituloChecklist").html("Crear Checklist");
}

function modificarChecklist(componente) {
	resetFormulario();
	$("#idChecklist").val(componente.getAttribute('objeto'));
	$("#nombreChecklist").val(componente.getAttribute('nombre'));
	$("#tipoChecklist").val(componente.getAttribute('rol')).trigger('change');
	$("#tipoFueroChecklist").val(componente.getAttribute('tipoFuero')).trigger('change');
	$("#paisChecklist").val(componente.getAttribute('idPais')).trigger('change');
	$("#formularioChecklist").val(componente.getAttribute('idFormulario')).trigger('change');

	selectFuero();
	getProvincia(componente.getAttribute('paisChecklist'), componente.getAttribute('provinciaChecklist'));
	getCircunscripcion(componente.getAttribute('provinciaChecklist'), componente.getAttribute('circunscripcionChecklist'));
	$("#instanciaChecklist").val(componente.getAttribute('instanciaChecklist')).trigger('change');
	$("#tituloChecklist").html("Editar Checklist");
	$("#materiaChecklist").val(componente.getAttribute('materiaChecklist')).trigger('change');
	getTipoCausa(componente.getAttribute('materiaChecklist'), componente.getAttribute('tipoCausaChecklist'));
	$("#tipoCausaChecklist").val(componente.getAttribute('tipoCausaChecklist')).trigger('change');
	window.location = '/administracion/checklist/listado#agregar';
}

function clonarChecklist(componente) {
	modificarChecklist(componente);
	verItems(componente, true);
	$("#idChecklist").val("");
}

function verItems(componente, clonar) {
	var item = "";
	var idChecklist = componente.getAttribute('objeto');

	$.ajax({
		url: '/api/checklist/buscar/item/' + idChecklist,
		dataType: 'json',
		type: "GET",
		success: function(data) {
			for (var i = 0; i < data.length; i++) {
				item += generarItem(null, data[i], clonar);
			}
			$("#bloqueItem").html(item);
		},
		error: function(data) {
			console.log("error");
			console.log(data);
		}
	});
}

function generarItem(ordenNuevo, data, clonar) {
	var id = "";
	var nombre = "";
	var orden = "";
	var subItem = "";
	var ordenEtiqueta = "";
	var archivosOcultar = "none";
	var archivos = "";
	var idEscritos = "";
	var limite = "";
	var habil = "";
	var idClonado = "";
	var inputClonado = "";
	if (data != null && data != "") {
		archivos = data.mArchivo;
		if (archivos != null && archivos.length > 0) {
			archivosOcultar = "block";
			idClonado = data.id;
		}

		var escritos = data.mEscritos;
		if (escritos != null && escritos.length > 0) {
			archivosOcultar = "block";
			for (var i = 0; i < escritos.length; i++) {
				idEscritos += escritos[i].id + ",";
			}
			idEscritos = idEscritos.substring(0, idEscritos.length - 1);
		}

		if (data.limite != null && data.limite != "") {
			archivosOcultar = "block";
			limite = data.limite;
			habil = data.habil;
		}

		orden = data.orden;
		nombre = data.nombre;
		id = data.id;
		ordenEtiqueta = data.orden;
		if (clonar == true) {
			inputClonado = '<input type="hidden" class="form-control" value="' + id + '" name="idItemClonado-' + orden + '">'
			id = "";
		}
		for (var j = 0; j < data.mSubItem.length; j++) {
			subItem += generarSubItem(orden, data.mSubItem[j], clonar);
		}
	} else {
		orden = ordenNuevo;
		ordenEtiqueta = $('.labelItem:visible').length + 1
	}

	var item = '<div class="bloqueItem" id="item' + orden + '">'
		+ '<input type="hidden" class="form-control idItem" value="' + id + '" name="idItem-' + orden + '">'
		+ '<div class="labelItem" for=""><label>Check ' + ordenEtiqueta + '</label>'
		+ '<input type="hidden" class="form-control" value="' + ordenEtiqueta + '" name="orden-' + orden + '"></div>'
		+ '<input type="text" placeholder="Ingrese el nombre del item." value="' + nombre + '" name="nombreItem-' + orden
		+ '">'
		+ '<ul class="trespuntos"><li class="dropdown user user-menu trespuntosli"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
		+ '<div class=""><img src="/dist/img/3puntos.png" alt=""></div></a>'
		+ '<ul class="dropdown-menu trespuntosmenu"><li><a onclick="agregarSub(' + orden + ')">'
		+ '<img src="/dist/img/suma.png" alt=""><p>Subcheck</p></a></li><hr><li onclick="abrirAdjuntar(' + orden + ', \'\',\'' + idClonado + '\')"><a href="#adjuntar">'
		+ '<img src="/dist/img/paperclip.png" alt=""><p>Adjuntar</p></a></li><hr><li>'
		+ '<a onclick="eliminarItem(\'' + orden + '\',\'' + id + '\')"><img src="/dist/img/iconomenu-01.png" alt=""><p>Eliminar</p></a></li></ul></li></ul>'
		+ '<input type="hidden" id="adjuntarFileNombre-' + orden + '">'
		+ '<a id="adjuntos-' + orden + '" style="display:' + archivosOcultar + '" href="#verAdjuntos" onclick="verAdjuntos(' + orden + ', \'\',\'' + idClonado + '\')" class="adjuntos-checklist">Ver adjuntos</a>'
		+ '<input type="hidden" name="escritos-' + orden + '" id="escritos-' + orden + '" value="' + idEscritos + '">'
		+ '<input type="hidden" name="limite-' + orden + '" id="limite-' + orden + '" value="' + limite + '">'
		+ '<input type="hidden" name="habil-' + orden + '" id="habil-' + orden + '" value="' + habil + '">'
		+ '<div id="subItems' + orden + '">';

	item += inputClonado;

	if (subItem != null && subItem != "") {
		item += subItem;
	}
	item += '</div></div>';

	return item;
}

function generarSubItem(orden, subItem, clonar) {
	var id = "";
	var subNombre = "";
	var sub = $('.opcion' + orden).length + 1;
	var archivosOcultar = "none";
	var archivos = "";
	var idEscritos = "";
	var limite = "";
	var habil = "";
	var idClonado = "";
	var inputClonado = "";
	if (subItem != null && subItem != "") {
		archivos = subItem.mArchivo;
		if (archivos != null && archivos.length > 0) {
			archivosOcultar = "block";
			idClonado = subItem.id;
		}

		var escritos = subItem.mEscritos;
		if (escritos != null && escritos.length > 0) {
			archivosOcultar = "block";
			for (var i = 0; i < escritos.length; i++) {
				idEscritos += escritos[i].id + ",";
			}
			idEscritos = idEscritos.substring(0, idEscritos.length - 1);
		}

		if (subItem.limite != null && subItem.limite != "") {
			archivosOcultar = "block";
			limite = subItem.limite;
			habil = subItem.habil;
		}

		id = subItem.id;
		sub = subItem.orden;
		if (clonar == true) {
			inputClonado = '<input type="hidden" class="form-control" value="' + id + '" name="idItemClonado-' + orden + '-' + sub + '">'
			id = "";
		}
		subNombre = subItem.subNombre;
	}

	return '<div class="opcionitem2 opcion opcion' + orden
		+ '" id="opcion' + orden + '-' + sub + '"><input type="hidden" class="form-control idSubItem" value="' + id + '" name="idSubItem-' + orden + '-' + sub + '">'
		+ '<input type="text" name="opcion-' + orden + '-' + sub + '" value="' + subNombre + '" placeholder="Ingrese sub item."/>'
		+ '<ul class="trespuntos"><li class="dropdown user user-menu trespuntosli"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
		+ '<div class=""><img src="/dist/img/3puntos.png" alt=""></div></a>'
		+ '<ul class="dropdown-menu trespuntosmenu"><li onclick="abrirAdjuntar(' + orden + ', ' + sub + ',\'' + id + '\')"><a href="#adjuntar">'
		+ '<img src="/dist/img/paperclip.png" alt=""><p>Adjuntar</p></a></li><hr><li>'
		+ '<a onclick="eliminarSub(' + orden + ',' + sub + ', \'' + id + '\')"><img src="/dist/img/basurero.png" alt=""><p>Eliminar</p></a></li></ul></li></ul>'
		+ '<input type="hidden" id="adjuntarFileNombre-' + orden + '-' + sub + '">'
		+ '<input type="hidden" name="escritos-' + orden + '-' + sub + '" id="escritos-' + orden + '-' + sub + '" value="' + idEscritos + '">'
		+ '<input type="hidden" name="limite-' + orden + '-' + sub + '" id="limite-' + orden + '-' + sub + '" value="' + limite + '">'
		+ '<input type="hidden" name="habil-' + orden + '-' + sub + '" id="habil-' + orden + '-' + sub + '" value="' + habil + '">'
		+ '<a id="adjuntos-' + orden + '-' + sub + '" onclick="verAdjuntos(' + orden + ', ' + sub + ',\'' + id + '\')" style="display:' + archivosOcultar + '" href="#verAdjuntos" class="adjuntos-checklist">Ver adjuntos</a></div>'
		+ inputClonado;
}

function botonAgregarItem() {
	var orden = cantidad();
	var html = generarItem(orden, null)
	$("#bloqueItem").append(html);
}

function cantidad() {
	return $(".bloqueItem").length + 1;
}

function agregarSub(orden) {
	var subItem = generarSubItem(orden, null);
	$('#subItems' + orden).append(subItem);
}


function guardarCheck(recargar) {
	console.log("Guardar Check en checklistNuevo.js...");
	
	//Nombre
	if ($("#nombreNuevoChecklist").val() == "defecto" || $("#nombreNuevoChecklist").val() == null || $("#nombreNuevoChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Debe ingresar un nombre al Checklist.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Rol procesal
	if ($("#tipoRolCausasChecklist").val() == "defecto" || $("#tipoRolCausasChecklist").val() == null || $("#tipoRolCausasChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione un rol procesal.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Pais
	if ($("#paisChecklist").val() == "defecto" || $("#paisChecklist").val() == null || $("#paisChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione un país.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Fuero
	if ($("#tipoFueroChecklist").val() == "defecto" || $("#tipoFueroChecklist").val() == null || $("#tipoFueroChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione un fuero.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Materia
	if ($("#materiaChecklist").val() == "defecto" || $("#materiaChecklist").val() == null || $("#materiaChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione una materia.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Tipo de causa
	if ($("#tipoCausaChecklist").val() == "defecto" || $("#tipoCausaChecklist").val() == null || $("#tipoCausaChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione un tipo de causa.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Provincia
	if ($("#provinciaChecklist").val() == "defecto" || $("#provinciaChecklist").val() == null || $("#provinciaChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione una provincia.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Cricunscripción
	if ($("#circunscripcionChecklist").val() == "defecto" || $("#circunscripcionChecklist").val() == null || $("#circunscripcionChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione una circrunscripción}.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Instancia
	if ($("#instanciaChecklist").val() == "defecto" || $("#instanciaChecklist").val() == null || $("#instanciaChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione una instancia.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	
	//Formulario
	if ($("#formularioChecklist").val() == "defecto" || $("#formularioChecklist").val() == null || $("#formularioChecklist").val() == "") {
		mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Por favor seleccione un formulario.", "show");
		$("#agregarinfo").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}

	var accion = "nuevoChecklist";
	var urlController = "";
	var form = document.getElementById('modalFormularioChecklist');
	var formData = new FormData(form);
	console.log(formData);

	if(accion == "nuevoChecklist"){
		urlController = '/api/checklist/guardarNuevoChecklist/';
	}
	$.ajax({
		url: urlController,
		enctype: 'multipart/form-data',
		data: formData,
		dataType: "json",
		type: "POST",
		processData: false,
		contentType: false,
		success: function(data) {
			console.log(JSON.stringify(data));
			if (recargar == true) {
				mjeAlerta("#modalAlertaNuevoCheckList", "Exito!", "El checklist fue guardado con éxito'", "show");
				setTimeout(function() {
					clonarChecklistEnCausa(data.id);
					mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
					//window.location.href = window.location.origin + window.location.pathname + window.location.search;
				}, 1000);
			}
		},
		error: function(data) {
			mjeAlerta("#modalAlertaNuevoCheckList", "Error!", "Ha ocurrido un error al crear el checklist.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlertaNuevoCheckList", "Ocultar!", "Ocultar", "hide");
			}, 3000);
		}
	});
}


function eliminarChecklist(componente) {
	$("#modalChecklist").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$
		.ajax({
			url: '/api/checklist/eliminar?id=' + $("#modalEliminarId").val(),
			dataType: "json",
			type: "POST",
			async: true,
			success: function(data) {

				$("#eliminar").hide();
				$("#modalConfirmar").hide();

				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El checklist fue eliminado con éxito'
						+ '</div>').show();

				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);

				window.location.href = window.location.origin + window.location.pathname + window.location.search;

			},
			error: function(data) {
				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el checklist. '
						+ data.responseJSON.message
						+ '</div>').show();
				$("#modalConfirmar").modal("hide");
				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);
			}
		});
}

function eliminarItem(orden, id) {
	$("#item" + orden).append('<input type="hidden" class="form-control" value="true" name="eliminado-'
		+ orden + '">').hide();

	if (id != null && id != "") {
		$.ajax({
			url: '/api/checklist/eliminarItem?id=' + id,
			dataType: "json",
			type: "POST",
			async: true,
			success: function(data) {

				$("#eliminar").hide();
				$("#modalConfirmar").hide();


				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);

			},
			error: function(data) {
				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el item. '
						+ data.responseJSON.message
						+ '</div>').show();
				$("#modalConfirmar").modal("hide");
				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);
			}
		});

	}

	setTimeout(() => {
		$(".labelItem:visible").each(function(i) {
			var nameInput = $(this).find("input").attr('name');
			var etiqueta = '<label>Check ' + (i + 1) + '</label>'
				+ '<input type="hidden" class="form-control" value="' + (i + 1) + '" name="' + nameInput + '">';

			$(this).empty();
			$(this).append(etiqueta);
		});
	});
}

function eliminarSub(orden, sub, id) {
	$('#opcion' + orden + '-' + sub).remove();
	if (id != null && id != "") {
		$.ajax({
			url: '/api/checklist/eliminarSub?id=' + id,
			dataType: "json",
			type: "POST",
			async: true,
			success: function(data) {

				$("#eliminar").hide();
				$("#modalConfirmar").hide();
				guardarCheck(false);

				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);

			},
			error: function(data) {
				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el sub item. '
						+ data.responseJSON.message
						+ '</div>').show();
				$("#modalConfirmar").modal("hide");
				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);
			}
		});
	}
}

function abrirAdjuntar(orden, subOrden, id) {
	$("#listadoSeleccionados").hide();
	$("#escritos").val(null).trigger('change');
	$("#limite").val("");
	$('input:checkbox').prop('checked', false);
	$("#files").val(null);
	$("#adjuntarOrden").val(orden);
	$("#adjuntarSubOrden").val(subOrden);
	$("#adjuntarId").val(id);

	adjuntar(orden, subOrden, id);

}

var initQuantitiesDropdown = function() {
	$.ajax({
		url: '/api/escrito/traerportipocausa?tipoCausa=' + $("#idTipoCausa").val(),
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var opciones = [];
			for (var i = 0; i < data.length; i++) {
				opciones.push({
					text: data[i].titulo,
					id: data[i].id
				});
			}
			$("#escritos").empty().select2({
				data: opciones
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function adjuntar(orden, subOrden, id) {
	$("#listadoNombre").empty();
	orden = unificarOrden(orden, subOrden);

	initQuantitiesDropdown();

	var escritos = $("#escritos-" + orden).val();
	if (escritos != null && escritos != "") {
		escritos = escritos.split(",");
		let ids = [];
		for (var i = 0; i < escritos.length; i++) {
			ids.push(escritos[i]);
		}
		console.log(ids)
		$("#escritos").select2().val(ids).change();
	}

	var texto = verArchivosGuardados(id, subOrden);
	if (texto != null && texto != "") {
		$("#listadoSeleccionados").show();
	}
	$("#listadoNombre").append(texto);

	var archivos = $("#adjuntarFileNombre-" + orden).val();
	var str = archivos.split(";");
	if (str.length > 0 && str != "") {
		$("#listadoSeleccionados").show();
		for (var i = 0; i < str.length; i++) {
			$("#listadoNombre").append('<div class="archivodedescarga">'
				+ '<p class="nombrearchivodescarga">' + str[i] + '</p>'
				+ '</div>');
		}
	}

	var limite = $("#limite-" + orden).val();
	if (limite != null && limite != "") {
		$("#limite").val(limite);
		var habil = $("#habil-" + orden).val();
		if (habil == "true") {
			$("#habiles").prop("checked", true);
		} else {
			$('#habiles').prop('checked', false);
			console.log($('#habiles').is(":checked"));
		}

	}
}

function agregarAdjuntos() {
	var orden = $("#adjuntarOrden").val();
	var subOrden = $("#adjuntarSubOrden").val();
	var files = $("#files").val();
	var escritos = $("#escritos").val();
	var limite = $("#limite").val();

	orden = unificarOrden(orden, subOrden);

	if (files != null && files != "") {
		adjuntarArchivos(orden);
	}

	adjuntarEscritos(orden, escritos);
	adjuntarLimites(orden, limite);

	$("#adjuntos-" + orden).show();

	location.href = "#agregar";

}

function adjuntarEscritos(orden, escritos) {
	$("#escritos-" + orden).val(escritos);

}

function adjuntarLimites(orden, limite) {
	$("#limite-" + orden).val(limite);
	if (limite != null && limite != "") {
		var habil = $('#habiles').is(":checked");
		$("#habil-" + orden).val(habil);
	}


}

function adjuntarArchivos(orden) {

	//** Con esta funcion clono el files que esta en el modal de adjuntar **//
	var clone = $("#files").clone();
	clone.attr('id', 'files-' + orden);
	clone.attr('name', 'files-' + orden);
	clone.attr('style', 'display:none');
	$("#item" + $("#adjuntarOrden").val()).append(clone);


	let archivos = $("#adjuntarFileNombre-" + orden).val();
	if (archivos != "") {
		archivos += ";";
	}
	var files = $('#files').prop("files");
	for (var i = 0; i < files.length; i++) {
		archivos += files[i].name + ";";
	}
	archivos = archivos.substring(0, archivos.length - 1);
	$("#adjuntarFileNombre-" + orden).val(archivos);
	$("#files").val(null);
}

function verAdjuntos(orden, subOrden, id) {
	orden = unificarOrden(orden, subOrden);
	var textoAdjuntar = '<h4 style="margin-top:7px; margin-bottom: 20px;">Archivos adjuntos</h4><div>' + verArchivosGuardados(id, subOrden);

	var archivos = $("#adjuntarFileNombre-" + orden).val();
	var str = archivos.split(";");
	if (str != null && str != "") {
		for (var i = 0; i < str.length; i++) {
			textoAdjuntar += '<div class="archivodedescarga">'
				+ '<p>' + str[i] + '</p>'
				+ '</div>';
		}
	}

	textoAdjuntar += '</div>';

	var escritos = $("#escritos-" + orden).val();
	if (escritos != null && escritos != "") {
		textoAdjuntar += '<h4 style="margin-top:7px; margin-bottom: 20px;">Escritos</h4><div>';
		$.ajax({
			url: '/api/escrito/ver?ids=' + escritos,
			dataType: 'json',
			type: "GET",
			async: false,
			success: function(data) {
				for (var i = 0; i < data.length; i++) {
					textoAdjuntar += '<div class="archivodedescarga">'
						+ '<p>' + data[i].titulo + '</p>'
						+ '</div>';
				}
			},
			error: function(data) {
				console.log("error");
				console.log(data);
			}
		});
		textoAdjuntar += '</div>';
	}

	var limite = $("#limite-" + orden).val();
	if (limite != null && limite != "") {
		var habil = $("#habil-" + orden).val();
		if (habil != null && habil == "true") {
			habil = "Hábiles";
		} else {
			habil = "Corridos";
		}
		textoAdjuntar += '<h4 style="margin-top:7px; margin-bottom: 20px;">Vencimiento plazos</h4><div>';
		textoAdjuntar += '<div class="archivodedescarga">'
			+ '<p>' + limite + ' ' + habil + '</p>'
			+ '</div></div>';
	}


	$("#contenidoVerAdjuntos").empty().html(textoAdjuntar);

}

function verArchivosGuardados(id, subOrden) {
	if (id != null && id != "") {
		var tipo = "ITEM";
		if (subOrden != null && subOrden != "") {
			tipo = "SUBITEM";
		}
		var textoAdjuntar = "";
		$.ajax({
			url: '/api/item/ver/archivos?id=' + id + "&tipo=" + tipo,
			dataType: 'json',
			type: "GET",
			async: false,
			success: function(data) {

				for (var i = 0; i < data.length; i++) {
					textoAdjuntar += '<div class="archivodedescarga">'
						+ '<a class="nombre-archivo" href="/administracion/archivo/multimedia/' + data[i].id + '" target="_blank" download><p>' + data[i].nombre + '</p></a>'
						+ '<a href="/administracion/archivo/multimedia/' + data[i].id + '" target="_blank" download><img src="/dist/img/descarga.svg" alt=""></a>'
						+ '<a href="#" onclick="eliminarArchivo(\'' + data[i].id + '\', \'' + id + '\', \'' + tipo + '\')"><img src="/dist/img/basurero.png" alt=""></a>'
						+ '</div>';


				}



			},
			error: function(data) {
				console.log("error");
				console.log(data);
			}
		});

		return textoAdjuntar;

	}

	return "";
}

function unificarOrden(orden, subOrden) {
	if (subOrden != null && subOrden != "") {
		orden = orden + "-" + subOrden;
	}

	return orden;
}

function eliminarArchivo(idArchivo, id, tipo) {
	$.ajax({
		url: '/api/archivo/eliminar/' + idArchivo + "/" + id + "/" + tipo,
		dataType: 'json',
		type: "POST",
		async: false,
		success: function(data) {
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El archivo fue eliminado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
				window.location.href = window.location.origin + window.location.pathname + window.location.search;
			}, 1000);
		},
		error: function(data) {
			console.log("error");
			console.log(data);
		}
	})
}

$('#files').on('change', function() {
	var orden = $("#adjuntarOrden").val();
	var subOrden = $("#adjuntarSubOrden").val();
	var id = $("#adjuntarId").val();
	adjuntar(orden, subOrden, id);
	$("#listadoSeleccionados").show();
	for (var i = 0; i < this.files.length; i++) {
		$("#listadoNombre").append('<div class="archivodedescarga">'
			+ '<p>' + this.files[i].name + '</p>'
			+ '</div>');
	}
});

$("#nombre").on('focus', function() {
	$("#modalAlerta").hide();
});

//Alerta
function mjeAlerta(idAlerta, mjeTitulo, mjeTexto, action) {
	var classAlert = "alert-info";
	if (mjeTitulo == 'Exito!' || mjeTitulo == 'Info!') {
		classAlert = "alert-info";
	} else {
		classAlert = "alert-danger";
	}
	if (action == "show") {
		$(idAlerta).empty().html('<div class="alert ' + classAlert + ' alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-ban"></i> ' + mjeTitulo + ' </h4> ' + mjeTexto + '</div>').show();
	} else {
		$(idAlerta).hide();
	}
}