function guardarJurisdiccion() {
	$.ajax({
				url : '/api/jurisdiccion/guardar',
				data : $("#modalFormulario").serialize(),
				dataType : "json",
				type : "POST",
				success : function(data) {
					$("#agregar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La jurisdiccion fue creado con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location = '/administracion/jurisdiccion/listado';

				},
				error : function(data) {
					$("#modalAlerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al crear la jurisdiccion. '
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#modalAlerta").hide();
					}, 5000);
				}
			});
}

function modificarJurisdiccion(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarJurisdiccion").val(componente.getAttribute('provincia'));
	$("#agregar").show();
}

function eliminarJurisdiccion(componente) {
	$("#modalJurisdiccion").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {

	$.ajax({
				url : '/api/jurisdiccion/eliminar?id='
						+ $("#modalEliminarId").val(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#eliminar").hide();
					$("#modalConfirmar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La jurisdiccion fue eliminada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location = '/administracion/jurisdiccion/listado';

				},
				error : function(data) {
					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al eliminar la jurisdicción. '
											+ data.responseJSON.message
											+ '</div>').show();
					$("#modalConfirmar").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);
				}
			});
}
