$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarPais() {
	$.ajax({
		url: '/api/pais/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El pais fue creado con éxito'
				+ '</div>').show();
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el pais. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarPais() {
	$.ajax({
		url: '/api/pais/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#editar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El pais fue editado con éxito'
				+ '</div>').show();
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al editar el pais. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function modificarPais(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	//window.location = '/administracion/pais/listado#editar';
}

function eliminarPais(componente) {
	$("#modalPais").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/pais/eliminar?id=' + $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data){
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El país fue eliminado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al eliminar el país. ' + data.responseJSON.message
				+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}



