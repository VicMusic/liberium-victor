function mensaje(){
	$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
            + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
            + 'Debe aceptar los terminos y condiciones para proseguir.. '
            + '</div>').show();
    setTimeout(function () {
		$("#modalAlerta").hide();
    }, 5000);	
}

function traerProvinciasRegistro(idPais, provincia) {
	$.ajax({
		url : '/api/provincia/combo?idPais=' + idPais,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			if (data.length === 0) {
				$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
		            + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
		            + 'La plataforma no esta disponible esta ubicación.'
		            + '</div>').show();
			    setTimeout(function () {
					$("#modalAlerta").hide();
			    }, 5000);	
			} else {
				var selectList = "<option selected disabled value=''>Seleccione una provincia</option>";
				for (var x = 0; x < data.length; x++) {
					if (provincia == data[x].id) {
						selectList.replace("selected", "");
						selectList += '<option selected value="' + data[x].id + '">'
						+ data[x].value + "</option>";
					}else {
					selectList += '<option value="' + data[x].id + '">'
							+ data[x].value + "</option>";
					}
				}	
				$('#provincia').html(selectList);
				$('#provinciaEditar').html(selectList);
				$('#proviciaRegistro').html(selectList);
			}
		},
		error : function(data) {
		}
	});
}

function guardar(){
	 $.ajax({
	        url: '/api/usuario/registrar',
	        data: $("#formRegistro").serialize(),
	        dataType: "json",
	        type: "POST",
	        async: true,
	        success: function (data) {
	        	$("#modalAlerta").empty().html('<div class="alert alert-info alert-dismissible">'
		                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
		                + '¡Bienvenido! Su cuenta en Liberium ha sido creada, Te estamos redireccionando al siguiente paso. '
		                + '</div>').show();
	        	setTimeout(function () {
	    			$("#modalAlerta").hide();
	    			
	    			window.location.href = "/confirmar-mail";
	        	}, 5000);

	        },
	 		error: function (data){
	 			window.scrollTo(0, 0);
	 			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
		                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
		                + 'Ha ocurrido un error al crear el usuario. ' + data.responseJSON.message
		                + '</div>').show();
	 			setTimeout(function () {
	    			$("#modalAlerta").hide();
	    	    }, 5000);
	 		}
	    }); 
 }

$('#checkV').on("change",function () {
    if ($(this).is(':checked')) {
        
    	$('#botonInicial').attr('onclick', 'guardar()'); 
        
    } else {
        $('#botonInicial').attr('onclick', 'mensaje()'); 
       
        
    }
});