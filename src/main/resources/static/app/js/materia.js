$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
			+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarMateria() {
	$.ajax({
		url: '/api/materia/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El materia fue creada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear la materia. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarMateria() {
	$.ajax({
		url: '/api/materia/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#editar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El materia fue editada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al editar la materia. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function modificarMateria(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarPais").val(componente.getAttribute('pais'));
	$("#modalEditarInstancia").val(componente.getAttribute('instancia'));
	//window.location = '/administracion/materia/listado#editar';
}

function eliminarMateria(componente) {
	$("#modalMateria").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/materia/eliminar?id='
			+ $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La materia fue eliminado con éxito'
					+ '</div>').show();
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar la materia. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}
