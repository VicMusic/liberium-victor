function eliminarArchivo(idItem, tipo, idArchivo){
	console.log('eliminarArchivo() -- idArrchivo = ' + idArchivo + ', tipo = ' + tipo +', idItem = ' +  idItem);
	$.ajax({
		url:'/api/archivo/eliminar/'+ idArchivo + '/'+ idItem + '/'+ tipo,
		type: 'POST',
		success:function(data){
			console.log('Elimainado Exitosamente');
			console.log(data);
		},
		error:function(data){
			console.log('Eliminacion insatisfecha, penosa, repulsiva...');
			console.log(data);
		}
	});
}

function descargarArchivo(idArchivo){
	window.open('/administracion/archivo/descargar/' + idArchivo);
}

function modalEleccionArchivo(){
	var item = document.getElementsByClassName("my-card active");
	var subItem = document.getElementsByClassName("my-subCard active"); 
	$("#archivoItem").val(item[0].getAttribute('value'));
	$("#archivoSubItem").val(subItem[0].getAttribute('value'));
}

function modalAgregarArchivo(orden, tipo, id){
	$("#idPasoSubPaso").val(id);
	$("#tipoDePaso").val(tipo);
	$("#file").attr("name", "files-" + orden);
	$("#orden").val(orden);
}

function modalAgregarArchivoSubPaso(){
	modalAgregarArchivo(($("#ordenMenu").val()), "SUBITEM", $("#idSubPasoMenu").val());
}

function guardarArchivo(){
	var idCausa = document.getElementById('idCausaAgregarArchivo').value;
	var form = document.getElementById('guardarArchivoForm');
	var formData = new FormData(form);
	
	$.ajax({
		url:'/api/archivo/guardarArchivo',
		enctype: 'multipart/form-data',
		data: formData,
		dataType : "json",
		type : "POST",
		processData: false,  
		contentType: false,
		success:function(data){
			window.location.href = '/administracion/causa/editar/' + idCausa;
		},
		error:function(data){
			console.log("Error: " + JSON.stringify(data));
		}
	});
}

function verArchivos(idItem, tipo){
	var archivos = "";
	$.ajax({
		url:'/api/item/ver/archivos?id=' + idItem + '&tipo=' + tipo,
		type: 'GET',
		success: function(data){
			for(var i = 0; i < data.length; i++){
				if(data[i].nombre != null && data[i].nombre != undefined && data[i].nombre != ''){
				archivos += '<div class="box-archivos">'
				      +'<p class="nombre-descarga" style="margin-left: 20%;">' + data[i].nombre + '</p>'
				      +'<input type="hidden" value=' + data[i].id + ' id="archivo' + i + '">'
				      +'<button style="margin-right: 0%; margin-top: -7%; margin-bottom: 2%;" type="button" onclick="descargarArchivo(\'' + data[i].id + '\')">Descargar</button>'
				      +'</div>';
				}
			}
			$("#archivos").html(archivos);
		},
		error: function(data){
			
		}
	
	});
		
	verEscritos(idItem, tipo);
	verDias(idItem, tipo);
}

function verEscritos(idItem, tipo){
	var escritos = "";
	var idCausa =  $("#idCausa").val();
	$.ajax({
		url:'/api/item/ver/escritos?id=' + idItem + '&tipo=' + tipo,
		type: 'GET',
		success: function(data){
			for(var i = 0; i < data.length; i++){
				if(data[i].titulo != null && data[i].titulo != undefined && data[i].titulo != ''){
					console.log("entre " + i);
					console.log("titulo " + i + " " + data[i].titulo);
					escritos += '<div>'
				      +'<p style="margin-left: 20%;">' + data[i].titulo + '</p>'
				      +'<input type="hidden" value=' + data[i].id + ' id="escrito' + i + '">'
//				      +'<div class="formulario3">'
		         		+'<ul class="trespuntos" style="margin-top: -7%;">' 
		         			+'<li class="dropdown user user-menu trespuntosli">'
		         			  +'<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
				                +'<img src="/dist/img/3puntos.png" alt="">'
				              +'</a>'
				              +'<ul class="dropdown-menu trespuntosmenu">'
									+'<li>'
										 +'<a href="/administracion/escrito/listado?idItem=' + idItem + '&idCausa=' + idCausa + '&idEscrito=' + data[i].id + '">'
										 +'<img src="/dist/img/disquete.png" alt=""> Usar</a><hr>'
									+'</li>'
									+'<li>'
										+'<a href="#" onclick="descargar(\'' + data[i].id + '\', \'pdf\')">'
										+'<img src="/dist/img/disquete.png" alt=""> Descargar Pdf</a><hr>'
									+'</li>'
									+'<li>'
										+'<a href="#" onclick="descargar(\'' + data[i].id + '\', \'docx\')">'
										+'<img src="/dist/img/disquete.png" alt=""> Descargar Word</a>'
									+'</li>'
				              +'</ul>'
		         			+'</li>'
		         		+'</ul>'
//		       		+'</div>'
				  +'</div>';
				}
			}
			$("#escrit").html(escritos);
		},
		error: function(data){
			
		}
	
	
});
}

function verDias(idItem, tipo){
	var cantDias = "";
	var idCausa =  $("#idCausa").val();
	$.ajax({
		url:'/api/item/ver/dias?id=' + idItem + '&tipo=' + tipo,
		type: 'GET',
		success: function(data){
			console.log(data);
				if(data != null && data != undefined && data != ''){
					cantDias += '<p style="margin-left: 20%;">' + data + '</p>';
				}
			$("#diasLimit").html(cantDias);
		},
		error: function(data){
			
		}
	
	
});
}

/*function verArchivosSub(){
	var subItem = document.getElementsByClassName("my-subCard active"); 
	var valor = subItem[0].getAttribute('value');
	var valores = valor.split('/');
	var id = valores[0];
	var tipo = valores[1];
	verArchivos(id, tipo);
}*/

function verArchivosSub(){
	verArchivos($("#idSubPasoMenu").val(), 'SUBITEM');
}

