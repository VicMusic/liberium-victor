
$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
			+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarInstancia() {
	$.ajax({
		url: '/api/instancia/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La instancia fue creado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			console.log(data.responseJSON.message)
			$("#modalAlertaGuardar").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear la instancia. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaGuardar").hide();
			}, 5000);
		}
	});
}

function editarInstancia() {
	$.ajax({
		url: '/api/instancia/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#editar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La instancia fue editada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlertaEditar").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al editar la instancia. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaEditar").hide();
			}, 5000);
		}
	});
}

function modificarInstancia(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	//window.location = '/administracion/instancia/listado#editar';
}

function eliminarInstancia(componente) {
	$("#modalInstancia").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/instancia/eliminar?id='
			+ $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La instancia fue eliminada con éxito'
					+ '</div>').show();
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar la instancia. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#modalConfirmar").hide();
			}, 5000);
		}
	});
}
