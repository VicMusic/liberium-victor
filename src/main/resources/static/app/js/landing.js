$("#calendario").slimscroll({
	height: '400px',
	width: '99%'
});

function recargar() {
	console.log("Entro al boton");
	window.location.href = "/#modal1";
	//window.location.reload(true);
}

function verificarNombreCliente() {
	var nombreCliente = document.getElementById("nombreCliente").value;
	if (nombreCliente.length == 0) {
		alert("Debe ingresar el nombre");
	}
}

function verificarTelefonoCliente() {
	var telefonoCliente = document.getElementById("telefonoCliente").value;
	if (telefonoCliente.length == 0) {
		alert("Debe ingresar el Telefono");
	}
}

function verificarEmailCliente() {
	var emailCliente = document.getElementById("emailCliente").value;
	if (emailCliente.length == 0) {
		alert("Debe ingresar el email");
	}
}

function convertirStringToEnumTG(string) {
	var textoEnum;

	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
		console.log(textoEnum);
	} else {
		textoEnum = string.toUpperCase();
		console.log(textoEnum);
	}

	$("#tipoGenero").val(textoEnum);

}

function buscarProvincia(id) {
	$
		.ajax({
			url: '/api/usuario/combo1/' + id,
			type: 'GET',
			contentType: "application/json",
			data: 'json',
			success: function(data) {
				var selectList = "<option selected disabled value=''>Provincia</option>";
				for (var x = 0; x < data.length; x++) {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
				$('#provincia').html(selectList);
				$('#idProvincia').html(selectList);
			},
			error: function(data) {
				console.log(data);
			}
		});
}

function buscarLocalidad(id) {
	$
		.ajax({
			url: '/api/usuario/combo2/' + id,
			type: 'GET',
			contentType: "application/json",
			data: 'json',
			success: function(data) {

				var selectList = "<option selected disabled value=''>Localidad</option>";
				for (var x = 0; x < data.length; x++) {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
				$('#localidad').html(selectList);
				$('#modalEditarLocalidad').html(selectList);
			},
			error: function(data) {
				console.log(data);
			}
		});
}

function revisarAbogados() {
	var idTipoCausa = $("#idTipoCausa").val();
	var idLocalidad = $("#idLocalidad").val();

	console.log("idLocalidad= " + idLocalidad + " idTipoCausa= " + idTipoCausa);

	$.ajax({
		url: '/api/consultas/verificarabogados/' + idTipoCausa + '/' + idLocalidad,
		data: '',
		type: "GET",
		datatype: "JSON",
		success: function(data) {
			if (data.is == false) {
				console
					.log("NO SE ENCONTRARON ABOGADOS EN LA LOCALIDAD CON ESA MATERIA");
				$("#errormodal3")
					.empty()
					.html(
						'<div class="alert alert-danger">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ "No se encontraron abogados"
						+ '</div>').show();

				setTimeout(function() {
					$("#errormodal3").hide();
				}, 5000);

			} else {
				console
					.log("SE ENCONTRARON ABOGADOS EN LA LOCALIDAD CON ESA MATERIA");

				window.location.href = "/#modal7";
			}

		},
		error: function(data) {

		}
	});
}

function traerAbogados() {
	console.log("ENTRO en landing.js en function traerAbogados(), VARIABLES: nombre = " + $("#nombreAbogado").val()
		+ " motivoConsulta = " + $("#motivoConsulta").val()
		+ " clienteNombre = " + $("#nombreCliente").val()
		+ " clienteTelefono = " + $("#telefonoCliente").val()
		+ " clienteEmail = " + $("#emailCliente").val()
	);

	var nombre = $("#nombreAbogado").val();
	var clienteNombre = $("#nombreCliente").val();
	var clienteTelefono = $("#telefonoCliente").val();
	var clienteEmail = $("#emailCliente").val();
	$("#clienteEmail").val(clienteEmail);

	if (clienteNombre.length == 0) {
		alert("Debe ingresar su nombre");
		return 0;
	}

	if (clienteTelefono.length == 0) {
		alert("Debe ingresar su telefono");
		return 0;
	}

	if (clienteEmail.length == 0) {
		alert("Debe ingresar su email");
		return 0;
	}

	if (nombre.length == 0) {
		alert("Debe ingresar el nombre del abogado a buscar");
		return 0;
	}

	$.ajax({
		url: '/api/consultas/abogados/' + nombre + '/' + clienteEmail + '/' + clienteNombre + '/' + clienteTelefono,
		type: 'GET',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {
			console.log("SUCCESS:");

			var string = "";

			var isData = jQuery.isEmptyObject(data);

			console.log("LA LISTA DE DATA ES " + isData);
			if (isData) {
				window.location.href = "/#modal2";
				console.log("DATA ES VACIA REE DIRIGIENDO A /#modal2");
			} else {
				console.log("DATA NO ESTA VACIA");
				$("#cargando").show();
				window.location.href = "/#cargando";
				var tiempoEspera = 3000;
			}

			for (var x = 0; x < data.length; x++) {
				tiempoEspera += 100;
				console.log(data[x]);

				if (data[x].mMateria.length > 1) {
					console.log("LA MATERIA ES MAYOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ ", "
						+ data[x].mMateria[1].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="calendario(\''
						+ data[x].id
						+ '\')"></label></div>';
				} else {
					console.log("LA MATERIA ES MENOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="calendario(\''
						+ data[x].id
						+ '\')"></label></div>';
				}
			}
			$('#abogados').html(string);

			if (isData == false) {
				setTimeout(function() {
					$("#cargando").hide();
					console.log("MOSTRANDO ABOGADOS");
					window.location.href = "/#modal3";
				}, tiempoEspera);
			}

		},
		error: function(data) {
			console.log("ERROR: " + data);
			window.location.href = "/#modal2";
		}
	});
}

function traerAbogadosPorMateriaYProvinciaLocalidad() {
	var pais = $("#idPais").val();
	var provincia = $("#idProvincia").val();
	var localidad = $("#modalEditarLocalidad").val();
	var materia = $("#idMateria").val();
	var clienteNombre = $("#nombreCliente").val();
	var clienteTelefono = $("#telefonoCliente").val();
	var clienteMail = $("#emailCliente").val();
	$("#clienteEmail").val($("#emailCliente").val());

	if (clienteNombre.length == 0) {
		alert("Debe ingresar su nombre");
		return 0;
	}

	if (clienteTelefono.length == 0) {
		alert("Debe ingresar su telefono");
		return 0;
	}

	if (clienteEmail.length == 0) {
		alert("Debe ingresar su email");
		return 0;
	}

	$.ajax({
		url: '/api/consultas/abogadosPorMyP/' + pais + '/' + provincia + '/' + localidad + '/' + materia + '/' + clienteNombre + '/' + clienteTelefono + '/' + clienteMail,
		type: 'GET',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {
			console.log("SUCCESS:");

			var string = "";

			var isData = jQuery.isEmptyObject(data);

			console.log(isData);

			if (isData == true) {
				console.log("DATA ES VACIA");
				window.location.href = "/#modal2";
			} else {
				console.log("DATA NO ESTA VACIA");
				$("#cargando").show();
				window.location.href = "/#cargando";
				var tiempoEspera = 3000;
			}

			for (var x = 0; x < data.length; x++) {
				tiempoEspera += 300;
				console.log(data[x]);

				if (data[x].mMateria.length > 1) {
					console.log("LA MATERIA ES MAYOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ ", "
						+ data[x].mMateria[1].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="calendario(\''
						+ data[x].id
						+ '\')"></label></div>';
				} else {
					console.log("LA MATERIA ES MENOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="calendario(\''
						+ data[x].id
						+ '\')"></label></div>';
				}
			}
			$('#abogados').html(string);

			if (isData == false) {
				setTimeout(function() {
					$("#cargando").hide();
					window.location.href = "/#modal3";
				}, tiempoEspera);
			}

		},
		error: function(data) {
			console.log("ERROR: " + data);
			window.location.href = "/#modal2";
		}
	});
}

function traerAbogadosPorMateriaYLocalidad() {

	var idLocalidad = $("#idLocalidad").val();
	var idTipoCausa = $("#idTipoCausa").val();

	$.ajax({
		url: '/api/consultas/verificarabogados?idTipoCausa=' + idTipoCausa + "&idLocalidad=" + idLocalidad,
		type: 'GET',
		contentType: "application/json",
		datatype: 'JSON',
		success: function(data) {

			console.log("SUCCESS:");

			var string = "";

			var isData = jQuery.isEmptyObject(data);

			console.log(isData);

			if (isData == true) {
				console.log("DATA ES VACIA");
				window.location.href = "/#errorconsulta";
			} else {
				console.log("DATA NO ESTA VACIA");
				$("#cargando").show();
				window.location.href = "/#cargando";
				var tiempoEspera = 3000;
			}

			for (var x = 0; x < data.length; x++) {
				tiempoEspera += 300;
				console.log(data[x]);

				if (data[x].mMateria.length > 1) {
					console.log("LA MATERIA ES MAYOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ ", "
						+ data[x].mMateria[1].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="guardarAbogado(\''
						+ data[x].id
						+ '\')"></label></div>';
				} else {
					console.log("LA MATERIA ES MENOR A 1:");
					string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
						+ data[x].mUsuario.foto
						+ '" style="background-image: url(/administracion/usuario/foto/'
						+ data[x].mUsuario.id
						+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
						+ data[x].mUsuario.nombre
						+ '</span></h4><p>Zona: <span>'
						+ data[x].mProvincia.nombre
						+ '</span></p><p>Especialidad: <span>'
						+ data[x].mMateria[0].nombre
						+ '</span></p></div></div></div><input type="checkbox" name="radio" onclick="guardarAbogado(\''
						+ data[x].id
						+ '\')"></label></div>';
				}
			}
			$('#abogadoslistaconsulta').html(string);

			if (isData == false) {
				setTimeout(function() {
					$("#cargando").hide();
					window.location.href = "/#abogadosconsulta";
				}, tiempoEspera);
			}

		},
		error: function(data) {
			$("#errorConsultaModal").empty().html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!  </h4>'
				+ data.responseJSON.message
				+ '</div>').show();

		}
	});
}

function guardarAbogado(id) {
	$("#abogadoslistaconsulta").hide();
	$("#idAbogado2").val(id);
	window.location.href = "/#modal7";

}

function seleccionar(campo) {
	campo.text('Turno Disponible');
	campo.removeClass('tocupado');
	campo.addClass('tdisponible');
}

function resetEvents(eventos) {
	$('#fullcalendar').fullCalendar('removeEvents');
	$('#fullcalendar').fullCalendar('addEventSource', eventos);
	$('#fullcalendar').fullCalendar('rerenderEvents');
}

function calendario(id) {

	$.ajax({
		url: '/api/consultas/turnos/' + id,
		type: 'GET',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {

			var eventos = [];

			for (var i = 0; i < data.horarios.length; i++) {
				var evento = {
					id: '',
					title: '',
					color: '',
					start: '',
					end: ''
				};
				evento.id = data.horarios[i].id;
				evento.title = data.horarios[i].titulo;
				evento.color = 'green';
				evento.start = data.horarios[i].fechaInicio;
				evento.end = data.horarios[i].fechaFin;
				eventos.push(evento);
			}

			$("#fullcalendar").fullCalendar({
				defaultView: 'agendaWeek',
				height: '100%',
				contentHeight: 600,
				locale: 'es',
				eventLimit: true,
				displayEventTime: false,
				events: eventos,
				header: {
					left: 'prev',
					center: 'title',
					right: 'next'
				},
				buttonText: {
					today: 'Hoy',
					month: 'Mensual',
					week: 'Semanal',
					day: 'Diario'
				},
				eventRender: function(event, element) {
					element.attr('id', event.id);
				},
				eventClick: function(calEvent, jsEvent, view) {
					crearTurno(calEvent);
				},

				timeZone: 'local',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
			});

			resetEvents(eventos);

			$("#nombreabogado").html(data.nombre);

			$('#abogadoElegido').val(data.id);

			var turnoConsulta = $("#turnoConsulta").val();

			if (turnoConsulta == "CONSULTA") {
				$("#volver").hide();
			}

			window.location.href = "/#modal4";
		},
		error: function(data) {
			console.log("ERROR: " + data);
			window.location.href = "/#modal3";
		}
	});
}

function crearTurno(evento) {
	console.log(evento);
	var fechaElegida = evento.id;
	var idAbogado = $("#abogadoElegido").val();
	var idConsulta = $("#idConsulta").val();
	var mailCliente = $("#clienteEmail").val();
	if (mailCliente == "vacio") {
		mailCliente = $("#mailConsulta").val();
	}
	console.log("FECHA ELEGIDA: " + fechaElegida);
	console.log("ABOGADO ELEGIDO: " + idAbogado);
	console.log("MAIL CLIENTE: " + mailCliente);
	console.log("CONSULTA: " + idConsulta);
	
	if(idConsulta == null || idConsulta == ""){
		idConsulta = 'a';
	}

	$.ajax({
		url: '/api/consultas/crearturno/' + fechaElegida + '/' + idAbogado + '/' + mailCliente + '/' + idConsulta,
		type: 'POST',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {
			console.log("SUCCESS:");

			var turnoConsulta = $("#turnoConsulta").val();
			console.log("TURNO CONSULTA:" + turnoConsulta);
			console.log(data);
			$("#elegido").val(data.elegido);
			$("#idTurno").val(data.id);

			// ERROR TURNO ACA

			if (turnoConsulta == "CONSULTA") {

				$("#abogadoNombreConsulta").html(data.mUsuario.nombre);
				$('#abogadoMailConsulta').html(data.mUsuario.email);
				$('#fechaTurnoConsulta').html(data.fecha);
				$('#horarioTurnoConsulta').html(data.hora);
				$('#abogadoTelefonoConsulta').html(data.mUsuario.telefono);
				$('#abogadoDireccionConsulta').html(data.mAbogado.domicilio);

				var pasos = data.mAbogado.pasos;
				if (pasos != null) {
					var pasosAbogado = "";
					for (var i = 0; i < pasos.length; i++) {
						pasosAbogado += '<p>'
							+ '<span>•</span>'
							+ pasos[i].substring(2)
							+ '</p>';
					}
					$("#pasosAbogdado").append(pasosAbogado);
				}

				notificarTurno();

				window.location.href = "/#modal9";
			} else {

				$("#abogadoNombre").html(data.mUsuario.nombre);
				$("#abogadoNombre2").html(data.mUsuario.nombre);
				$('#abogadoMail').html(data.mUsuario.email);
				$('#abogadoTelefono').html(data.mUsuario.telefono);
				$('#abogadoDireccion').html(data.mAbogado.domicilio);
				$('#fechaTurno').html(data.fecha);
				$('#horarioTurno').html(data.hora);

				window.location.href = "/#modal5";
			}
		},
		error: function(data) {

			$("#errormodal69")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!  </h4>'
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#errormodal69").hide();
			}, 5000);

		}
	});
}

function notificarTurno() {

	var mailAbogado = $("#abogadoMail").val();
	var nombreCliente = $("#nombreCliente").val();
	var telefonoCliente = $("#telefonoCliente").val();
	var mailCliente = $("#clienteEmail").val();
	var fechaTurno = $("#elegido").val();

	if (mailCliente == "[object%20Object]" || mailCliente == "vacio") {
		mailCliente = $("#mailConsulta").val();
		mailAbogado = $("#idAbogado2").val();
	}
	
	if(nombreCliente == "[object%20Object]" || nombreCliente == "vacio" || nombreCliente == ""){
		nombreCliente = 'a';
	}
	if(telefonoCliente == "[object%20Object]" || telefonoCliente == "vacio" | telefonoCliente == ""){
		telefonoCliente = 'a';
	}
	
	if (mailAbogado == "[object%20Object]" || mailAbogado == "vacio" || mailAbogado == "") {
		mailAbogado = 'a';
	}

	$.ajax({
		url: '/api/consultas/notificarturno/' + mailAbogado + '/' + nombreCliente + '/' + telefonoCliente + '/'+ mailCliente + '/'+ fechaTurno,
		type: 'GET',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {
			console.log("SUCCESS:");
			$("#idTurno").val(data.id);

			var turnoConsulta = $("#turnoConsulta").val();
			console.log(turnoConsulta);

			if (turnoConsulta == "CONSULTA") {
				setTurnoYFormularioConsulta();
			}

		},
		error: function(data) {

		}
	});

}

function setTurnoYFormularioConsulta() {
	var idConsulta = $("#idConsulta").val();
	var idTurno = $("#idTurno").val();
	var idFormulario = $("#idFormulario").val();

	console.log(idConsulta + " Y " + idTurno + " Y " + idFormulario);

	$.ajax({
		url: '/api/consultas/setturno/' + idConsulta + '/' + idTurno + '/'
			+ idFormulario,
		type: 'POST',
		contentType: "application/json",
		datatype: 'JSON',
		data: '',
		success: function(data) {
			console.log("SUCCESS:");

		},
		error: function(data) {

		}
	});
}

function revisarAbogados() {
	var idTipoCausa = $("#idTipoCausa").val();
	var idLocalidad = $("#idLocalidad").val();

	console.log("idLocalidad= " + idLocalidad + " idTipoCausa= " + idTipoCausa);

	$
		.ajax({
			url: '/api/consultas/verificarabogados/' + idTipoCausa + '/'
				+ idLocalidad,
			data: '',
			type: "GET",
			datatype: "JSON",
			success: function(data) {

				if (data.is == false) {
					console
						.log("NO SE ENCONTRARON ABOGADOS EN LA LOCALIDAD CON ESA MATERIA");
					$("#errormodal3")
						.empty()
						.html(
							'<div class="alert alert-danger">'
							+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
							+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
							+ "No se encontraron abogados"
							+ '</div>').show();

					setTimeout(function() {
						$("#errormodal3").hide();
					}, 5000);

				} else {
					console
						.log("SE ENCONTRARON ABOGADOS EN LA LOCALIDAD CON ESA MATERIA");

					window.location.href = "/#modal7";
				}

			},
			error: function(data) {

			}
		});
}

function crearConsulta() {
	$
		.ajax({
			url: '/api/consultas/guardarconsulta',
			data: $("#modalFormulario").serialize(),
			type: "POST",
			datatype: 'JSON',

			success: function(data) {

				if (data.error == null) {
					$("#idConsulta").val(data.id);
					fijarIds(data.id);
					$("#errormodal4")
						.empty()
						.html(
							'<div class="alert alert-info alert-dismissible">'
							+ '<a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>'
							+ '<h4><i class="icon fa fa-ban"></i> Procesando los datos ingresados!</h4>'
							+ '</div>').show();
					console.log(data);
					$("#turnoConsulta").val("CONSULTA");
					$("#mailConsulta").val(data.mail);
					traerFormulario();
					setTimeout(function() {
						window.location.href = "/#modal8";
					}, 1000);

				}
				if (data.error != null) {
					$("#errormodal4")
						.empty()
						.html(
							'<div class="alert alert-danger">'
							+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
							+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
							+ data.error + '</div>').show();

					setTimeout(function() {
						$("#errormodal3").hide();
					}, 5000);
				}
			},
			error: function(data) {
				console.log("Error.");
			}

		});
}

function traerIdTipoCausa(idTipoCausa) {
	console.log(idTipoCausa)
	$("#idTipoCausa").val(idTipoCausa);
	$("#idTipoCausa2").val(idTipoCausa);
}

function traerIdLocalidad(idLocalidad) {
	$("#idLocalidad").val(idLocalidad);
	$("#idLocalidad2").val(idLocalidad);

}

function fijarIds(id) {
	var idTipoCausa = $("#idTipoCausa").val();
	var idConsulta = id;

	console.log("IdTipoCausa: " + idTipoCausa);
	console.log("IdConsulta: " + idConsulta);

}

function traerFormulario() {
	var idConsulta = $("#idConsulta").val();

	$
		.ajax({
			url: '/api/consultas/formulario/' + idConsulta,
			type: 'GET',
			contentType: "application/json",
			datatype: 'JSON',
			data: '',
			success: function(data) {
				console.log("SUCCESS:");
				$("#idFormulario").val(data.idFormulario);

				$('#tituloTipoCausa').html(data.tituliTipoCausa);
				console.log(data);

				var string = "";

				for (var x = 0; x < data.preguntas.length; x++) {

					if (data.preguntas[x].tipo == "ABIERTA") {
						string += '<div class="box1 cuestionario cuestionariob">'
							+ '<label for="">¿' + data.preguntas[x].titulo + '?</label>'
							+ '<input type="hidden" value="' + data.preguntas[x].orden + '" name="orden">'
							+ '<input type="hidden" value="' + data.preguntas[x].id + '" name="idPregunta">';

						switch (data.preguntas[x].inputRespuesta) {
							case 'TEXT':
								string += '<input type="text" name="respuesta">'
									+ '</div>';
								break;
							case 'DATE':
								string += '<input type="date" name="respuesta">'
									+ '</div>';
								break;
							case 'NUM':
								string += '<input type="number" name="respuesta">'
									+ '</div>';
								break;
							default:
								break;
						}
						$('#preguntas').html(string);

					} else {
						var opciones = "";
						for (var i = 0; i < data.preguntas[x].opciones.length; i++) {
							opciones += '<option value="' + data.preguntas[x].opciones[i].titulo + '" name="respuesta">' + data.preguntas[x].opciones[i].titulo + '</option>';
						}

						string += '<div class="box1 cuestionario cuestionariob">'
							+ '<label for="">¿'
							+ data.preguntas[x].titulo
							+ '?</label>'
							+ '<select onchange="fijarValue(this.value, '
							+ data.preguntas[x].orden
							+ ')">'
							+ '<option selected disabled >Seleccione una opcion</option>'
							+ opciones
							+ '<input type="hidden" value="'
							+ data.preguntas[x].orden
							+ '" name="orden">'
							+ '</select>'
							+ '<input id="respuesta'
							+ data.preguntas[x].orden
							+ '" type="hidden" value="" name="respuesta">'
							+ '<input type="hidden" value="' + data.preguntas[x].id + '" name="idPregunta"></div>';

						$('#preguntas').html(string);
					}
				}

			},
			error: function(data) {

			}
		});
}

function fijarValue(respuesta, orden) {

	console.log("RESPUESTA: " + respuesta + " ORDEN: " + orden);

	$("#respuesta" + orden).val(respuesta);

}

function borrarTurno() {
	console.log($("#idTurno").val());
	$.ajax({
		url: '/api/consultas/borrarturno/' + $("#idTurno").val(),
		type: "POST",
		success: function(data)  {
		},
		error: function(data) {
		}
	});
}

function guardarRespuestas() {
	$.ajax({
		url: '/api/consultas/guardarrespuestas',
		data: $("#modalRespuestas").serialize(),
		type: "POST",
		datatype: 'JSON',
		success: function(data) {
			console.log(data);
			if (data.error == null) {
				$("#errormodal7")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Revisando tus respuestas!</h4>'
						+ '</div>').show();

				setTimeout(function() {
					calendario($("#idAbogado2").val());
				}, 5000);
			}
			if (data.error != null) {
				console.log(data);
				$("#errormodal7")
					.empty()
					.html(
						'<div class="alert alert-danger">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ data.error + '</div>').show();

				setTimeout(function() {
					$("#errormodal7").hide();
				}, 5000);
			}
		},
		error: function(data) {
			console.log("Error.");
		}

	});

}

function traerTipoCausa(idMateria, tipo) {
	$.ajax({
		url: '/api/formulario/combo/' + idMateria,
		type: 'GET',
		dataType: 'json',
		success: function(data) {
			var selectList = "<option disabled value='' selected>--Seleccione tipo de causa--</option>";
			for (var x = 0; x < data.length; x++) {
				if (data[x].id == tipo) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">' + data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">' + data[x].nombre + "</option>";
				}
			}
			$('#tipoCausaSelectContulta').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

$('.tocupado').click(false);

$('.celda1hora').click(function() {

	var anterior = $("#elegido").val();
	if (anterior != null && anterior != '') {
		$("#" + anterior).css('background-color', '#4470b4');
		$("#" + anterior).text("Turno Disponible");
	}

	var id = $(this).attr('id');
	$("#elegido").val(id);

	$(this).css('background-color', '#8fb23a');
	$(this).text('Seleccionado');
});
