//ELECCION SOBRE AÑADIR PASO O SUB PASO
function elegir(ordenPasoActual, idPasoActual) {
	$("#ordenPasoActual").val(ordenPasoActual);
	$("#idPasoActual").val(idPasoActual);
	$("#idCausaAgregarItem").val(idCausa);
}

//AGREGAR ITEM
function nuevoItem(componente) {
	$("#ordenItemAnterior").val(componente.value);
}

function editarItem(idItem, titulo) {
	console.log(idItem);
	console.log(titulo)
	$("#idItemEditar").val(idItem);
	$("#tituloItemNuevo").val(titulo);
}

function agregarItem() {
	if ($("#tituloItemNuevo").val().length < 2) {
		mjeAlerta("#modalAlertaItem", "Error!", "Debe ingresar el titulo.", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaItem", "Ocultar", "Ocultar", "hide");
		}, 4000);
		return 0;
	}

	$.ajax({
		url: '/api/item/agregar/paso',
		data: $('#agregarItemForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			mjeAlerta("#modalAlertaItem", "Exito!", "El Item se guardo correctamente.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlertaItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			//cambiarPag($('#idCausa').val());
			$("#tituloItemNuevo").val("");
			$("#ordenItemAnterior").val("");
			$("#idItemEditar").val("");
			window.location.href = "#";
			cargarPasos(0);
		},
		error: function(data) {
			if (data.responseJSON.status == 404) {
				mjeAlerta("#modalAlertaItem", "Error!", " " + data.responseJSON.message + " - No posee permisos.", "show");
			} else {
				mjeAlerta("#modalAlertaItem", "Error!", "Ocurrio un error al guardar el paso", "show");
			}
			setTimeout(function() {
				mjeAlerta("#modalAlertaItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
		}
	});
}

//AGREGAR SUB ITEM
function nuevoSubItemAlFinal(componente) {
	$("#idItemAgregarSubItem").val(componente.value);
	$("#tituloSubItemNuevo").val("");
}

function nuevoSubItem(orden, idItem) {
	$("#idItemAgregarSubItem").val(idItem);
	$("#ordenAnteriorSubItem").val(orden);
}

function nuevoSubItemMenu() {
	nuevoSubItem($("#ordenMenu").val(), $("#idPasoMenu").val());
}

function editarSubItem(idSubItem, idItem, titulo) {
	$("#idItemAgregarSubItem").val(idItem);
	$("#idSubItemEditar").val(idSubItem);
	$("#tituloSubItemNuevo").val(titulo);
}

function editarSubItemMenu() {
	editarSubItem($("#idSubPasoMenu").val(), $("#idPasoMenu").val(), $("#nombreSubPasoMenu").val());
}

function agregarSubItem() {
	var idItem = $("#idItemAgregarSubItem").val();
	if ($("#tituloSubItemNuevo").val().length < 2) {
		mjeAlerta("#modalAlertaSubItem", "Error!", "Debe ingresar el titulo.", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaSubItem", "Ocultar", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	$.ajax({
		url: '/api/item/agregar/subPaso',
		data: $('#agregarSubItemForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			mjeAlerta("#modalAlertaSubItem", "Exito!", "El SubItem se guardo correctamente.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlertaSubItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			//cambiarPag($('#idCausa').val());
			$("#tituloSubItemNuevo").val("");
			$("#idItemAgregarSubItem").val("");
			$("#ordenAnteriorSubItem").val("");
			$("#idSubItemEditar").val("");
			window.location.href = "#";
			cargarPasos(0);
			//cambiarSub(idItem);
		},
		error: function(data) {
			if (data.responseJSON.status == 404) {
				mjeAlerta("#modalAlertaSubItem", "Error!", " " + data.responseJSON.message + " - No posee permisos.", "show");
			} else {
				mjeAlerta("#modalAlertaSubItem", "Error!", "Ocurrio un error al guardar el sub paso", "show");
			}
			setTimeout(function() {
				mjeAlerta("#modalAlertaSubItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			return 0;
		}
	});
}

//ELIMINAR ITEMS
function deleteItem(idItem) {
	if (idItem != null && idItem != undefined) {
		$("#idItemEliminar").val(idItem);
	}
}

function eliminarItemE() {
	$.ajax({
		url: '/api/item/eliminarItem',
		data: $('#eliminarItemForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			console.log("Data elminiar: " + JSON.stringify(data));
			mjeAlerta("#modalAlertaEliminarItem", "Exito!", "El Paso se elimino correctamente.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlertaEliminarItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			//cambiarPag($('#idCausa').val());
			$("#idItemEliminar").val("");
			window.location.href = "#";
			cargarPasos(0);
		},
		error: function(data) {
			if (data.responseJSON.status == 404) {
				mjeAlerta("#modalAlertaEliminarItem", "Error!", " " + data.responseJSON.message + " - No posee permisos.", "show");
			} else {
				mjeAlerta("#modalAlertaEliminarItem", "Error!", "Ocurrio un error al eliminar el paso", "show");
			}
			setTimeout(function() {
				mjeAlerta("#modalAlertaSubItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			return 0;
		}
	});
}

//ELIMINAR SUB ITEMS
function eliminarSub(idSubItem, idItem) {
	if (idSubItem != null && idSubItem != undefined) {
		$("#subItemIdSubItemEliminar").val(idSubItem);
		$("#itemIdSubItemEliminar").val(idItem);
	}
}

function eliminarSubItemMenu() {
	eliminarSub($("#idSubPasoMenu").val(), $("#idPasoMenu").val());
}

function eliminarSubItemE() {
	$.ajax({
		url: '/api/item/eliminarSubItem',
		data: $('#eliminarSubItemForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			mjeAlerta("#modalAlertaEliminarSubItem", "Exito!", "El SubPaso se elimino correctamente.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlertaEliminarSubItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			//cambiarPag($('#idCausa').val());
			$("#subItemIdSubItemEliminar").val("");
			$("#itemIdSubItemEliminar").val("");
			window.location.href = "#";
			cargarPasos(0);
		},
		error: function(data) {
			if (data.responseJSON.status == 404) {
				mjeAlerta("#modalAlertaEliminarSubItem", "Error!", " " + data.responseJSON.message + " - No posee permisos.", "show");
			} else {
				mjeAlerta("#modalAlertaEliminarSubItem", "Error!", "Ocurrio un error al eliminar el SubPaso", "show");
			}
			setTimeout(function() {
				mjeAlerta("#modalAlertaEliminarSubItem", "Ocultar", "Ocultar", "hide");
			}, 4000);
			return 0;
		}
	});
}

//Alerta
function mjeAlerta(idAlerta, mjeTitulo, mjeTexto, action) {
	var classAlert = "alert-info";
	if (mjeTitulo == 'Exito!' || mjeTitulo == 'Info!') {
		classAlert = "alert-info";
	} else {
		classAlert = "alert-danger";
	}
	if (action == "show") {
		$(idAlerta).empty().html('<div class="alert ' + classAlert + ' alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-ban"></i> ' + mjeTitulo + ' </h4> ' + mjeTexto + '</div>').show();
	} else {
		$(idAlerta).hide();
	}
}
