$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
	var id = $("#id").val();
	if (id != '') {
		cargar(id);
	}
});

function guardarTipo() {
	if ($("#nombre").val() == '') {
		$("#modalAlerta")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Debe ingresar un nombre'
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlerta").hide();
		}, 4000);
		return 0;
	}
	if ($("#materia").val() == null) {
		$("#modalAlerta")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Debe seleccionar una materia'
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlerta").hide();
		}, 4000);
		return 0;
	}
	$.ajax({
		url: '/api/tipocausa/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El tipo de causa fue creado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 4000);

			window.location.href = window.location.origin + window.location.pathname + window.location.search;

		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear el tipo de causa. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 4000);
		}
	});
}

function editarTipo() {
	$.ajax({
		url: '/api/tipocausa/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El tipo de causa fue editado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location.href = window.location.origin + window.location.pathname + window.location.search;

		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al editar el tipo de causa. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function modificarTipo(componente) {
	console.log(componente)
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarMateria").val(componente.getAttribute('materia')).trigger("change");
	//window.location = '/administracion/tipocausa/listado#editar';
}

function eliminarTipo(componente) {
	$("#modalJuzgado").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/tipocausa/eliminar?id='
			+ $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El tipo de causa fue eliminado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar el tipo de causa. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function cargarTipoCausa(componente) {
	var id1 = componente.getAttribute('th:objeto');
	cargar(id1);
}
