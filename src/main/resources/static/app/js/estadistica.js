var usuarioLog = $("#usuarioLog").val();
console.log(usuarioLog);
var fecha = $("#fecha").val();

// INGRESOS
function ingresos(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/ingresos',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreIngresos").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaIngresos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Ingresos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
    $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=INGRESO ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreIngresos").text(data.nombre);
			console.log(data);
			console.log(data.ene);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaIngresos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Ingresos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});}	
}

//GASTOS
function gastos(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/gastos',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreGastos").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaGastos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Gastos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=GASTO ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreGastos").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaGastos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Gastos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
	}	
}

//INGRESOS ACTIVOS
function activos(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
  
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/ingresos/activos',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreActivos").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaActivos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Ingresos Activos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=ACTIVO ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreActivos").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaActivos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Ingresos Activos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}

//OTROS GASTOS
function otrosGastos(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/otros/gastos',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreOtrosGastos").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaOtrosGastos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Otros Gastos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=OTROGASTO ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreOtrosGastos").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // BAR CHART
			var ctx = document.getElementById('estadisticaOtrosGastos');
			var myBarChart  = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Otros Gastos',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}


//ESTADISTCAS DE CANTIDAD DE TIEMPO EN SESION
function tiempoSesion(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/tiempo',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantTiempo").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaTiempo');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Tiempo en Sesion',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=Tiempo de sesión ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantTiempo").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaTiempo');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Tiempo en Sesion',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}



//ESTADISTCAS DE CANTIDAD DE CAUSAS TOTALES
function cantidadDeCausas(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/causas',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasTotales").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasTotales');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Totales',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=Cantidad totales de Causas ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasTotales").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasTotales');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Totales',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}

//ESTADISTCAS DE CANTIDAD DE CAUSAS ARCHIVADAS
function cantidadDeCausasArchivadas(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/causas/archivadas',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasArchivadas").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasArchivadas');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Archivadas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=Cantidad totales de Causas archivadas ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasArchivadas").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasArchivadas');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Archivadas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}

//ESTADISTCAS DE CANTIDAD DE CAUSAS ACTIVAS
function cantidadDeCausasActivas(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/causas/activas',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasActivas").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasActivas');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Activas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=Cantidad totales de Causas activas ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasActivas").text(data.nombre);
			console.log(data);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasActivas');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Causas Activas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:'rgba(99, 191, 63, 1)'
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}

//ESTADISTCAS DE CANTIDAD DE CAUSAS SEGUN EL ESTADO
function cantidadDeCausasEstado(){
	
	var enev = 0;
	var febv = 0;
	var marv = 0;
	var abrv = 0;
	var mayv = 0;
	var junv = 0;
	var julv = 0;
	var agov = 0;
	var sepv = 0;
	var octv = 0;
	var novv = 0;
	var dicv = 0;
	
	var ener = 0;
	var febr = 0;
	var marr = 0;
	var abrr = 0;
	var mayr = 0;
	var junr = 0;
	var julr = 0;
	var agor = 0;
	var sepr = 0;
	var octr = 0;
	var novr = 0;
	var dicr = 0;
	
	var enea = 0;
	var feba = 0;
	var mara = 0;
	var abra = 0;
	var maya = 0;
	var juna = 0;
	var jula = 0;
	var agoa = 0;
	var sepa = 0;
	var octa = 0;
	var nova = 0;
	var dica = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/causas/estado',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombreCantCausasEstado").text("Cantidad totales de Causas por estado 2020");
			console.log(data);
			enev = data[0].cantidadPorMes.ene;
			febv = data[0].cantidadPorMes.feb;
			marv = data[0].cantidadPorMes.mar;
			abrv = data[0].cantidadPorMes.abr;
			mayv = data[0].cantidadPorMes.may;
			junv = data[0].cantidadPorMes.jun;
			julv = data[0].cantidadPorMes.jul;
			agov = data[0].cantidadPorMes.ago;
			sepv = data[0].cantidadPorMes.sep;
			octv = data[0].cantidadPorMes.oct;
			novv = data[0].cantidadPorMes.nov;
			dicv = data[0].cantidadPorMes.dic;
			
			ener = data[1].cantidadPorMes.ene;
			febr = data[1].cantidadPorMes.feb;
			marr = data[1].cantidadPorMes.mar;
			abrr = data[1].cantidadPorMes.abr;
			mayr = data[1].cantidadPorMes.may;
			junr = data[1].cantidadPorMes.jun;
			julr = data[1].cantidadPorMes.jul;
			agor = data[1].cantidadPorMes.ago;
			sepr = data[1].cantidadPorMes.sep;
			octr = data[1].cantidadPorMes.oct;
			novr = data[1].cantidadPorMes.nov;
			dicr = data[1].cantidadPorMes.dic;
			
			enea = data[2].cantidadPorMes.ene;
			feba = data[2].cantidadPorMes.feb;
			mara = data[2].cantidadPorMes.mar;
			abra = data[2].cantidadPorMes.abr;
			maya = data[2].cantidadPorMes.may;
			juna = data[2].cantidadPorMes.jun;
			jula = data[2].cantidadPorMes.jul;
			agoa = data[2].cantidadPorMes.ago;
			sepa = data[2].cantidadPorMes.sep;
			octa = data[2].cantidadPorMes.oct;
			nova = data[2].cantidadPorMes.nov;
			dica = data[2].cantidadPorMes.dic;
			 // LINE CHART
			var ctx = document.getElementById('estadisticaCausasEstado');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'verde',
			            data: [enev, febv, marv, abrv, mayv, junv, julv, agov, sepv, octv, novv, dicv],
			            borderColor:['rgba(99, 191, 63, 1)'],
			            fill: false
			        },
			        {
			            label: 'rojo',
			            data: [ener, febr, marr, abrr, mayr, junr, julr, agor, sepr, octr, novr, dicr],
			            borderColor:['rgba(255, 15, 15, 1)'],
			            fill: false
			        },
			        {
			            label: 'amarillo',
			            data: [enea, feba, mara, abra, maya, juna, jula, agoa, sepa, octa, nova, dica],
			            borderColor:['rgba(255, 255, 15, 1)'],
			            fill: false
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
 	});
  }else if(usuarioLog == "ADMINISTRADOR"){
  $("#estadoColor").css('display', 'none');
  }		
}

//ESTADISTCAS DE PROMEDIO DE EL AVANCE DE LAS CAUSAS
function promedioDeAvanceDeCausas(){
	
	var ene = 0;
	var feb = 0;
	var mar = 0;
	var abr = 0;
	var may = 0;
	var jun = 0;
	var jul = 0;
	var ago = 0;
	var sep = 0;
	var oct = 0;
	var nov = 0;
	var dic = 0;
	
  if(usuarioLog == "USUARIO"){	
	$.ajax({
		url: '/api/estadistica/buscar/causas/promedioAvance',
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombrePromCausaAvance").text(data.nombre);
			console.log(data);
			if(data.cantidadPorMes != null){
			ene = data.cantidadPorMes.ene;
			feb = data.cantidadPorMes.feb;
			mar = data.cantidadPorMes.mar;
			abr = data.cantidadPorMes.abr;
			may = data.cantidadPorMes.may;
			jun = data.cantidadPorMes.jun;
			jul = data.cantidadPorMes.jul;
			ago = data.cantidadPorMes.ago;
			sep = data.cantidadPorMes.sep;
			oct = data.cantidadPorMes.oct;
			nov = data.cantidadPorMes.nov;
			dic = data.cantidadPorMes.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaPromCausaAvance');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Avances Causas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:['rgba(99, 191, 63, 1)']
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }else if(usuarioLog == "ADMINISTRADOR"){
  $.ajax({
		url: '/api/estadistica/buscar/estadisticas/admin?nombre=Cantidad promedio de estado de avance de las causas ' + fecha,
		type : 'GET',
		contentType : "application/json",
		data : 'json',
		success : function(data) {
			$("#nombrePromCausaAvance").text(data.nombre);
			console.log(data);
			console.log(data.oct);
			if(data != null){
			ene = data.ene;
			feb = data.feb;
			mar = data.mar;
			abr = data.abr;
			may = data.may;
			jun = data.jun;
			jul = data.jul;
			ago = data.ago;
			sep = data.sep;
			oct = data.oct;
			nov = data.nov;
			dic = data.dic;
			}
			 // LINE CHART
			var ctx = document.getElementById('estadisticaPromCausaAvance');
			var myLineChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			        datasets: [{
			            label: 'Avances Causas',
			            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
			            backgroundColor:['rgba(99, 191, 63, 1)']
			            
			        }]
			    },
			    options: {}
			});
			

		},
		error: function(data) {
			
		}
	});
  }	
}

