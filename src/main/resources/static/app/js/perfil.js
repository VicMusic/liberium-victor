function causasTotales(){
 	$.ajax({
		url : '/api/causa/contarcausas?idEstudio=' + $("#selectIdEstudio").val(),
		dataType : "json",
		type : "GET",
		success : function(data) {
			$("#numeroCausasActivas").html(data[0]);
			$("#numeroCausasTotales").html(data[1]);
		}
	});
}

function botonAgrgarPaso() {
	var orden = $(".bloquePasos").length + 1;
	var html = 	'<div class="bloquePasos" id="paso'+ orden +'">'
				+'<label class="labelItem" for="">Paso ' + orden + ' </label>'
				+'<input type="text" placeholder="Ingrese el texto del paso a seguir." name="pasos" style="width: 91%; float: left;">'
				+'<img src="/dist/img/basurero.png" onclick="eliminaPaso(\'' + orden +'\')" alt="" style="height: 22px; width: auto; margin-left: 5px; float: left; margin-top: 7px;">'
				+'</div>';
	 $("#pasos").append(html);
}


function traerInfoTurnoConsulta(){
	$.ajax({
		url : '/api/usuario/buscar-abogado',
		dateType : 'GET',
		success : function (data){
			$("#pasos").empty();
			$("#cantidadSemanas").val(data.cantSemanas);
			var html = "";
			if (data.pasos != null){
				for (var i = 0; i < data.pasos.length; i++) {
					var orden = data.pasos[i].substring(0 ,1);
					var texto = data.pasos[i].substring(2);
					html += '<div class="bloquePasos" id="paso'+ orden +'">'
					+'<label class="labelItem" for="">Paso ' + orden + ' </label>'
					+'<input type="text" placeholder="Ingrese el texto del paso a seguir." name="pasos" style="width: 91%; float: left;" value="'+ texto +'">'
					+'<img src="/dist/img/basurero.png" onclick="eliminaPaso(\'' + orden + '\'' + ','   + '\''+ data.pasos[i] +'\')" alt="" style="height: 22px; width: auto; margin-left: 5px; float: left; margin-top: 7px;">'
					+'</div>';
					
				}
				$("#pasos").append(html);
			}

		},
		error : function (data){
			
		}
	});
}

function eliminaPaso(orden, texto){
	$("#paso" + orden).empty();
	$.ajax({
		url : '/api/usuario/eliminarpaso?paso=' + texto,
		dataType : "json",
		type : "POST", 
		success : function (data){

		},
		error : function (data){
			
		}
	});
}


function guardarInfoTurnoConsulta(){
	$.ajax({
		url : '/api/usuario/turnoconsulta',
		data : $("#formTurnoConsulta").serialize(),
		dataType : "json",
		type : "POST", 
		success : function (data){
			window.location = '/administracion/usuario/perfil';
		},
		error : function (data){
		$("#modalTurnoConsulta").empty().html('<div class="alert alert-danger alert-dismissible">'
		+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
		+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
		+ data.responseJSON.message
		+ '</div>').show();
		}
	});
}

function cambiarPassword() {
	$.ajax({
		url : '/api/usuario/cambiar/password',
		data : $("#formPassword").serialize(),
		dataType : "json",
		type : "POST",
		success : function(data) {
			$("#modalAlertaPassword").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El password fue cambiado con éxito.'
				+ '</div>').show();

			setTimeout(function() {
				window.location = '/administracion/usuario/perfil';
			}, 4000);
		},
		error : function(data) {
			$("#modalAlertaPassword").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaPassword").hide();
			}, 5000);
		}
	});
}


function eliminarUsuario(componente) {
	$("#eliminar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url : '/api/usuario/eliminar?id=' + $("#modalEliminarId").val(),
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			$("#eliminar").modal('hide');
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue eliminado con éxito'
						+ '</div>').show();
			
			setTimeout(function() {
				window.location = '/administracion/usuario/listado';
			}, 1000);
		},
		error : function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
			$("#eliminar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function buscarLocalidad(id) {
	$
			.ajax({
				url : '/api/usuario/combo2/' + id,
				type : 'GET',
				contentType : "application/json",
				data : 'json',
				success : function(data) {

					var selectList = "<option selected disabled value=''></option>";
					for (var x = 0; x < data.length; x++) {
						selectList += '<option value="' + data[x].id + '">'
								+ data[x].nombre + "</option>";
					}
					$('#localidad').html(selectList);
					$('#modalEditarLocalidad').html(selectList);
				},
				error : function(data) {
					console.log(data);
				}
			});
}