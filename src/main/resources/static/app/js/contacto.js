function cambiarValue(booleano) {
	if (booleano) {
		$("#opcionContactoExistente").val("nuevo");
	} else {
		$("#opcionContactoExistente").val("existente");
	}
}

function guardarContacto() {
	var idCausa = $('#idCausa').val();
	var opciones = $("#opcionContactoExistente").val();
	var data;
	console.log("Guardar Contacto");
	if (opciones == "existente") {
		data = $("#modalFormularioContactoExistente").serialize();
		console.log("entre");
	} else {
		data = $("#modalFormularioContacto").serialize();
	}
	$.ajax({
		url: '/api/contacto/guardar',
		data: data,
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El contacto fue creado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			if (idCausa != '' && idCausa != null
				&& idCausa != undefined) {
				window.location = "/administracion/causa/editar/"
					+ idCausa;
			} else {
				window.location = '/contactos/ver/personales/' + $("#selectIdEstudio").val();
			}
		},
		error: function(data) {
			$("#modalAlertaContacto")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear el contacto. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function agregarContactoExistente() {
	if ($("#selectContactoExistente").val() == '0') {
		$("#modalAlertaContacto")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Debe seleccionar un contacto. '
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlertaContacto").hide();
		}, 4000);
		return 0;
	}
	$.ajax({
		url: '/api/contacto/agregar?contactoId=' + $("#selectContactoExistente").val() + '&causaId=' + $("#causaIdContacto").val(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#modalAlertaContacto")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El contacto fue agregado con éxito'
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaContacto").hide();
				window.location.href = '/administracion/causa/editar/' + idCausa;
			}, 4000);
		},
		error: function(data) {
			$("#modalAlertaContacto")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al agregar el contacto. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaContacto").hide();
			}, 4000);
		}
	});
}

function quitarContacto(){
	$.ajax({
		url: '/api/contacto/quitar?contactoId=' + $("#contactoId").val() + '&causaId=' + $("#causaIdContactoEliminar").val(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			window.location.href = '/administracion/causa/editar/' + idCausa;
		},
		error: function(data) {
		}
	});
}

function cargarTablaContacto(){
	$.ajax({
		url: '/api/causa/contactoTabla?causaId=' + $("#causaIdContacto").val(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			var tablaData = "";
			for(var i = 0; i < data.length; i++){
				tablaData += '<tr>' +
					'<td scope="row">' + data[i].creado + '</td>' +
					'<td>' + data[i].tituloNota + '</td>' +
					'<td>' + data[i].textoNota + '</td>' +
					'<td><a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "ver" + '\')"><i class="fas fa-eye"></i><\a>' +
					'<a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "editar" + '\')"><i class="fas fa-pencil-alt"></i><\a>' +
					'<a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "eliminar" + '\')"><i class="fas fa-trash-alt"></i><\a>' +
					'</td>' +
					'<tr>';
			}
			$('#bodyTablaNotas').html(tablaData);
		},
		error: function(data) {
			$("#modalAlertaContacto")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al agregar el contacto. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaContacto").hide();
			}, 4000);
		}
	});
}

function verContacto(componente) {
	var id = componente.getAttribute('idContacto');
	console.log("ID: " + id);
	$.ajax({
		url: '/api/contacto/buscarContacto?id=' + id,
		dataType: "json",
		type: "GET",
		success: function(data) {
			//traerProvincias(data.pais.id, data.provincia.id);
			//traerLocalidad(data.provincia.id, data.localidad.id);
			//$("#modalEditarId").val(data.id);
			//convertirEnumToStringTP(data.tipoPersona);
			//$("#modalCambiarTipoPersona").val(data.tipoPersona);
			//convertirEnumToStringTV(data.tipoVinculo);
			//$("#modalCambiarTipoVinculo").val(data.tipoVinculo);
			//convertirEnumToStringTG(data.tipoGenero);
			//$("#modalCambiarTipoGenero").val(data.tipoGenero);
			$("#modalEditarApellido").val(data.apellido);
			$("#modalEditarApellido").prop('disabled', true);
			$("#modalEditarNombre").val(data.nombre);
			$("#modalEditarNombre").prop('disabled', true);
			$("#modalEditarTelefonoFijo").val(data.telefonoFijo);
			$("#modalEditarTelefonoFijo").prop('disabled', true);
			$("#modalEditarTelefonoCelular").val(data.telefonoCelular);
			$("#modalEditarTelefonoCelular").prop('disabled', true);
			$("#modalEditarEmail").val(data.email);
			$("#modalEditarEmail").prop('disabled', true);
			$("#modalEditarDni").val(data.dni);
			$("#modalEditarDni").prop('disabled', true);
			$("#modalEditarCuilCuit").val(data.cuilCuit);
			$("#modalEditarCuilCuit").prop('disabled', true);
			$("#modalEditarDomicilio").val(data.domicilio);
			$("#modalEditarDomicilio").prop('disabled', true);
			$("#modalEditarAltura").val(data.alturaDomicilio);
			$("#modalEditarAltura").prop('disabled', true);
			//$("#modalEditarPais").val(data.pais.id);
			//$("#modalEditarPais").prop('disabled', true);
			//$("#modalEditarPais option[value='" + data.pais.id + "']").attr("selected", true);		
			//$("#modalEditarTelefonoAdicional").val(data.telefonoAdicional);
			//$("#modalEditarTelefonoAdicional").prop('disabled', true);
			//$("#modalEditarNombreAdicional").val(data.nombreAdicional);
			//$("#modalEditarNombreAdicional").prop('disabled', true);
			//$("#modalEditarObservacion").val(data.observacion);
			//$("#modalEditarObservacion").prop('disabled', true);
			//$("#modalBtnGuardarContacto").css('display', 'none');
			//$('#modalEditarProvincia').prop('disabled', true);
			//$('#modalEditarLocalidad').prop('disabled', true);
		},
		error: function(data) {

		}
	});

	$("#contactoModal").show();

}

function mostrarBloque(tipo) {
	console.log(tipo + " " + $("#selectIdEstudio").val())
	window.location = '/contactos/ver/' + tipo + '/' + $("#selectIdEstudio").val();
}

function buscarContacto(idContacto) {
	$.ajax({
		url: '/api/contacto/buscarContacto?id=' + idContacto,
		dataType: "json",
		type: "GET",
		success: function(data) {

			$("#tipoPersonaExistente").val(data.tipoPersona);
			$("#tipoVinculoExistente").val(data.tipoVinculo);
			$("#sexoExistente").val(data.tipoGenero);
			$("#nombreExistente").val(data.nombre);
			$("#nacionalidadExistente").val(data.pais.id);
			$("#dniExistente").val(data.dni);
			$("#cuilExistente").val(data.cuilCuit);
			$("#domicilioExistente").val(data.domicilio);
			$("#alturaExistente").val(data.alturaDomicilio);
			$("#telefonoCelularExistente").val(data.telefonoCelular);
			$("#telefonoFijoExistente").val(data.telefonoFijo);
			$("#telefonoAdicionalExistente").val(data.telefonoAdicional);
			$("#nombreAdicionalExistente").val(data.nombreAdicional);
			$("#observacionExistente").val(data.observacion);
			$("#mailExistente").val(data.email);
			$("#provinciaExistente").val(data.provincia.id);
			$("#localidadExistente").val(data.localidad.id)
			console.log(data);
		},
		error: function(data) {

		}

	});

}

function modificarContacto(componente) {
	var id = componente.getAttribute('idContacto');
	$.ajax({
		url: '/api/contacto/buscarContacto?id=' + id,
		dataType: "json",
		type: "GET",
		success: function(data) {
			console.log(data)
			$("#modalAlertaContacto").hide();
			$("#tituloModal").html("Editar Contacto");
			$("#modalEditarId").val(data.id);
			$("#modalEditarTipoPersona").val(data.tipoPersona).trigger("change")
			$("#modalEditarTipoVinculo").val(data.tipoVinculo).trigger("change");
			$("#modalEditarTipoGenero").val(data.tipoGenero).trigger("change");
			$("#modalEditarApellido").val(data.apellido);
			$("#modalEditarNombre").val(data.nombre);
			$("#modalEditarPais").val(data.pais.id).trigger("change");
			$("#modalEditarPais option[value='" + data.pais.id + "']").attr("selected", true);
			$("#modalEditarDni").val(data.dni);
			$("#modalEditarCuilCuit").val(data.cuilCuit);
			$("#modalEditarDomicilio").val(data.domicilio);
			$("#modalEditarAltura").val(data.alturaDomicilio);
			$("#modalEditarTelefonoCelular").val(data.telefonoCelular);
			$("#modalEditarTelefonoFijo").val(data.telefonoFijo);
			$("#modalEditarTelefonoAdicional").val(data.telefonoAdicional);
			$("#modalEditarNombreAdicional").val(data.nombreAdicional);
			$("#modalEditarObservacion").val(data.observacion);
			$("#modalEditarEmail").val(data.email);
			traerProvincias(data.pais.id, data.provincia.id);
			traerLocalidad(data.provincia.id, data.localidad.id);
		},
		error: function(data) {

		}

	});

	$("#contactoModal").show();
}

function eliminarContacto(componente) {
	$("#modalContacto").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#contactoId").val(componente.getAttribute('idContacto'));
}

function contactoEliminar() {
	var causa = "";
	if (document.getElementById('causaIdContactoEliminar') != null && document.getElementById('causaIdContactoEliminar') != undefined && document.getElementById('causaIdContactoEliminar') != "") {
		causa = document.getElementById('causaIdContactoEliminar').value;
	}
	$.ajax({
		url: '/api/contacto/eliminar',
		data: $('#eliminarContactoForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {

			$("#eliminar").hide();
			$("#modalConfirmar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El contacto fue eliminado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			if (causa != '' && causa != null && causa != undefined) {
				window.location = "/administracion/causa/editar/"
					+ causa;
			} else {
				window.location = '/contactos/ver/personales/' + $("#selectIdEstudio").val();
			}

		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar el contacto. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function traerProvincias(idPais, idProvicia) {
	$
		.ajax({
			url: '/api/contacto/combo1/' + idPais,
			type: 'GET',
			contentType: "json",
			data: 'json',
			success: function(data) {
				if (data.length == 0) {
					var selectList1 = "<option selected disabled value=''>No hay provincias disponibles</option>";
					var selectList2 = "<option selected disabled value=''>No hay localidades disponibles</option>";

					$('#provincia').html(selectList1);
					$('#modalEditarProvincia').html(selectList1);
					$('#localidad').html(selectList2);
					$('#modalEditarLocalidad').html(selectList2);
				} else {
					var selectList = "<option selected disabled value=''>Provincia</option>";
					for (var x = 0; x < data.length; x++) {
						if (idProvicia == data[x].id) {
							selectList.replace("selected", "");
							selectList += '<option selected value="'
								+ data[x].id + '">' + data[x].nombre
								+ "</option>";
						} else {
							selectList += '<option value="' + data[x].id + '">'
								+ data[x].nombre + "</option>";
						}

					}
					$('#provincia').html(selectList);
					$('#modalEditarProvincia').html(selectList);
				}
			},
			error: function(data) {
				console.log(data);
			}
		});
}

function traerLocalidad(idProvincia, idLocalidad) {

	$
		.ajax({
			url: '/api/usuario/combo1/' + idProvincia,
			type: 'GET',
			contentType: "application/json",
			data: 'json',
			success: function(data) {

				var selectList = "<option selected disabled value=''>Localidad o Municipio</option>";
				for (var x = 0; x < data.length; x++) {
					if (idLocalidad == data[x].id) {
						selectList.replace("selected", "");
						selectList += '<option selected value="'
							+ data[x].id + '">' + data[x].nombre
							+ "</option>";
					} else {
						selectList += '<option value="' + data[x].id + '">'
							+ data[x].nombre + "</option>";
					}
				}
				$('#localidad').html(selectList);
				$('#modalEditarLocalidad').html(selectList);
			},
			error: function(data) {
				console.log(data);
			}
		});
}

function modalAgregar() {
	$("#tituloModal").html("Agregar Contacto");
	$("#idEstudio").val($("#selectIdEstudio").val());
	resetForm()
}

function resetForm() {
	$("#modalEditarId").val("");
	$("#modalEditarTipoPersona").val("").trigger("change")
	$("#modalEditarTipoVinculo").val("").trigger("change");
	$("#modalEditarTipoGenero").val("").trigger("change");
	$("#modalEditarNombre").val("");
	$("#modalEditarApellido").val("");
	$("#modalEditarPais").val("").trigger("change");
	$("#modalEditarProvincia").val("").trigger("change");
	$("#modalEditarLocalidad").val("").trigger("change");
	$("#modalEditarDni").val("");
	$("#modalEditarCuilCuit").val("");
	$("#modalEditarDomicilio").val("");
	$("#modalEditarAltura").val("");
	$("#modalEditarTelefonoCelular").val("");
	$("#modalEditarTelefonoFijo").val("");
	$("#modalEditarTelefonoAdicional").val("");
	$("#modalEditarNombreAdicional").val("");
	$("#modalEditarObservacion").val("");
	$("#modalEditarEmail").val("");
}

