function editar(componente) {
	resetFormulario();
	$("#id").val(componente.getAttribute('objeto'));
	$("#email").val(componente.getAttribute('email'));
	$("#rol").val(componente.getAttribute('rol'));
	$("#rol").prop("disabled", false);
	$("#modalBtnEditar").prop("disabled", false);
	//$("#agregar").show();
}

function visualizar(componente){
	editar(componente);
	//$("#modalFormulario *").prop('disabled', true);
	$("#rol").prop("disabled", true);
	$("#modalBtnEditar").prop("disabled", true);
}

function resetFormulario(){
	$("#modalFormulario *").prop('disabled', false);
	$("#modalFormulario").trigger("reset");
}

function resetFormularioRol(){
	console.log("resetFormularioRol...")
	$("#modalFormularioNuevoRol *").prop('disabled', false);
	$("#modalFormularioNuevoRol").trigger("reset");
	$("#id").val("");
	$('input:checkbox').removeAttr('checked');
	$("#textoSeleccionar").empty().html("Seleccionar Todos");
}

function guardar() {
	if(document.getElementById("idEmailBuscar").value == ""){
		alert("Debe ingresar el mail del abogado a agregar al estudio");
		return 0;
	}
	if(document.getElementById("idRolSelect").value == "-1"){
		alert("Debe seleccionar un rol para el abogado");
		return 0;
	}
    $("#modalBtnGuardar").prop("disabled",true);
	$.ajax({
		url : '/api/miembro/guardar',
		data : $("#modalFormulario").serialize(),
		dataType : "json",
		type : "POST",
		success : function(data) {
			console.log(JSON.stringify(data));
			$("#agregar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ data.message
				+ '</div>').show();

			setTimeout(function() {
			    $("#modalBtnGuardar").prop("disabled",false);
			
				window.location = '/administracion/miembro/estudio/listado';
			}, 100);
		},
		error : function(data) {
		    $("#modalBtnGuardar").prop("disabled",false);
			$("#modalAlertaEditar").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el miembro. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaEditar").hide();
			}, 3000);
		}
	});
}

function editarMiembro() {
    $("#modalBtnEditar").prop("disabled",true);
	$.ajax({
		url : '/api/miembro/editar',
		data : $("#editarFormulario").serialize(),
		dataType : "json",
		type : "POST",
		success : function(data) {
			$("#editar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El miembro fue editado con éxito.'
				+ '</div>').show();

			setTimeout(function() {
				$("#modalBtnEditar").prop("disabled",false);
				window.location = '/administracion/miembro/estudio/listado';
			}, 100);
		},
		error : function(data) {
		 	$("#modalBtnEditar").prop("disabled",false);
			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al editar el miembro. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function eliminarMiembro(componente) {
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url : '/api/miembro/eliminar?id=' + $("#modalEliminarId").val(),
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			$("#eliminar").modal('hide');
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue eliminado con éxito'
						+ '</div>').show();
			
			setTimeout(function() {
				window.location = '/administracion/miembro/estudio/listado';
			}, 100);
		},
		error : function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
			$("#eliminar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function guardarRol() {
	var permisosChecked = "";
	jQuery("input[name='permisos']:checked").each(function() {
		var id = (this.id);
		permisosChecked +=  id + ";";
	});
	
	var id = document.getElementById("idRol").value;
	var nombre = document.getElementById("nombreRol").value;
	var isAbogado = document.getElementById("isAbogadoRol").value;
	
	permisosChecked = permisosChecked.substring(0,(permisosChecked.lastIndexOf(";")));
	var formulario = $("#modalFormularioNuevoRol").serialize() + "&permisosChecked=" + permisosChecked + "&nombre=" + nombre + "&isAbogado=" + isAbogado + "&id=" + id;
	$.ajax({
		url : '/api/roles/guardarRolEstudio',
		data : formulario,
		dataType : "json",
		type : "POST",
		success : function(data) {
			$("#agregarRol").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El rol fue creado con éxito.'
				+ '</div>').show();

			setTimeout(function() {
				window.location = '/administracion/miembro/estudio/listado';
			}, 100);
		},
		error : function(data) {
			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el rol. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarRol(componente) {
	console.log("editarRol...")
	resetFormularioRol();
	$("#idRol").val(componente.getAttribute('objeto'));
	$("#nombreRol").val(componente.getAttribute('nombre'));
	if(componente.getAttribute('isAbogado') == "true"){
		$("#isAbogadoRol").attr("checked", true);
	}
	
	var permisos = JSON.parse(componente.getAttribute('permisosUsuario'));
	for (var i = 0; i < permisos.length; i++) {
		$("#" + permisos[i].id).attr("checked", true);
	}
	
	$("#agregarRol").show();
}

function visualizarRol(componente){
	editarRol(componente);
	$("#modalFormularioNuevoRol *").prop('disabled', true);
}

$(function() {
	$('#seleccionarTodos').change(function() {
	  var checkboxes = $(this).closest('form').find('.permisos').not($(this));
	  checkboxes.prop('checked', $(this).is(':checked'));
	  
	  if($(this).is(':checked')){
		  $("#textoSeleccionar").empty().html("Descartar Todos");
	  }else{
		  $("#textoSeleccionar").empty().html("Seleccionar Todos");
	  }
	  
	});
});

function eliminarRol(componente) {
	$("#eliminarRol").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminarRolMje() {
	$.ajax({
		url : '/api/roles/eliminarRolEstudio?id=' + $("#modalEliminarId").val(),
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			$("#agregarRol").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El rol fue eliminado con éxito.'
				+ '</div>').show();
			
			setTimeout(function() {
				window.location = '/administracion/miembro/estudio/listado';
			}, 2000);
		},
		error : function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el rol. '
						+ data.responseJSON.message
						+ '</div>').show();
			$("#eliminar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}