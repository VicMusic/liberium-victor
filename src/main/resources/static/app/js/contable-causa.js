$(document).ready(function() {
	buscarContabilidadCausa();
});

function buscarContabilidadCausa(){
	var idCausa = $("#idCausaOG").val();
	
	$.ajax({
		url: '/api/contable-causa/buscar-contabilidad/' + idCausa,
		data: '',
		dataType: "json",
		type: "GET",
		success: function (data){
			
			$("#tabla1").empty();
			$("#tabla2").empty();
			$("#tabla3").empty();
			$("#instancias").empty();
			
			var tabla1 = "";
			var tabla2 = "";
			var tabla3 = "";
			var total1 = 0;
			var total2 = 0;
			var total3 = 0;
			
			for (var i = 0; i < data.length; i++) {
				switch (data[i].uHtml) {
				case "TABLA1":
					tabla1 += '<div class="celdadetalle">'
							 	 + '<div class="cd9"><p>' + data[i].detalle + '</p></div>'
							 	 + '<div class="cd5"><p>' + data[i].tipoDeTasa + '</p></div>'
							 	 + '<div class="cd8"><p>' + data[i].desde + '</p></div>'
							 	 + '<div class="cd8"><p>' + data[i].hasta + '</p></div>'
							 	 + '<div class="cd8"><p>' + data[i].montoCI + '</p></div>'
							 	 + '<div class="cd8"><p>' + data[i].montoSI + '</p></div>'
							 	 + '<div class="cd6"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
						 	 + '</div>';
					if (data[i].tipoDePago == "Un solo pago") {
						total1 += data[i].montoCI;
					}
					break;
				case "TABLA2":
					var vencimiento = "";
					if (data[i].vencimiento != null) {
						vencimiento += '<p>' + data[i].vencimiento + '</p>';
					} else {
						vencimiento += '<p>' + 'Editar vencimiento' + '</p>';
					}
					tabla2 += '<div class="celdadetalle">'
					 	 + '<div class="cd5"><p>' + data[i].tipoDePago + '</p></div>'
					 	 + '<div class="cd5"><p>' + data[i].detalle + '</p></div>'
					 	 + '<div class="cd5"><p>' + data[i].tipoDeTasa + '</p></div>'
					 	 + '<div class="cd5"><p>' + data[i].montoCI + '</p></div>'
					 	 + '<div class="cd5"onclick="modalVencimiento(\'' + data[i].id + '\',\'' + data[i].vencimiento + '\')">'+vencimiento+'</div>'
					 	 + '<div class="cd6"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
					 	 + '</div>';
					if (data[i].tipoDePago == "Un solo pago") {
						total2 += data[i].montoCI;
					}
					break;
				case "TABLA3":
					tabla3 += '<div class="celdadetalle">'
					 	 + '<div class="cd2"><p>' + data[i].detalle + '</p></div>'
					 	 + '<div class="cd7"><p>' + data[i].tipoDePago + '</p></div>'
					 	 + '<div class="cd5"><p>' + data[i].tipoDeTasa + '</p></div>'
					 	 + '<div class="cd5"><p>$' + data[i].montoCI + '</p></div>'
					 	 + '<div class="cd5"><p>' + data[i].vencimiento + '</p></div>'
					 	 + '<div class="cd6"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
					 	 + '</div>';
					total3 += data[i].montoCI;
					break;
				default:
					break;
				}
				
			}
			
			$("#tabla1").append(tabla1);
			$("#tabla2").append(tabla2);
        	$("#tabla3").append(tabla3);
        	$("#total1").html("$"+total1);
        	$("#total2").html("$"+total2);
        	$("#total3").html("$"+total3);
        	
        	buscarHonorariosCausa(data);
        	
        	window.location = '/contabilidad-causa/ver/' + $("#idCausaOG").val() +'#';
        	
		},
		error: function (data){
			console.log(data);
		}
	});
}

function buscarHonorariosCausa(elementosContables){
	
	var idCausa = $("#idCausaOG").val();
		
	$.ajax({
		url: '/api/contable-causa/buscar-honorarios/' + idCausa,
		data: '',
		dataType: "json",
		type: "GET",
		success: function (data){
			var honorarios = [];

			for (var i = 0; i < data.length; i++) {
				var honorario = {
						id: '',
						nombre: '',
						tipoInstancia: ''
				};
				honorario.id = data[i].id;
				honorario.nombre = data[i].nombre;
				honorario.tipoInstancia = data[i].tipoInstancia;
				honorarios.push(honorario);
			}
			var string = "";
			
			for (var x = 0; x < honorarios.length; x++) {
				
				if (honorarios[x].tipoInstancia == true) {
					var input1 = '<input id="radiobtn_1" class="radiobtn" type="radio" value="" tabindex="1" checked>';
					var input2 = '<input id="radiobtn_2" class="radiobtn" type="radio" value="" tabindex="1">';
				} else {
					var input1 = '<input id="radiobtn_1" class="radiobtn" type="radio" value="" tabindex="1">';
					var input2 = '<input id="radiobtn_2" class="radiobtn" type="radio" value="" tabindex="1" checked>';
				}
				
				string += '<div class="' + honorarios[x].nombre + '">'
			        	+ '<div class="celdaconcepto" style="margin-bottom:15px;">'
			        	+ '<p class="ct1">' + honorarios[x].nombre + '</p>'
			        	+ '<a onclick="modalEditarInstancia(\'' + honorarios[x].id + '\',\'' + honorarios[x].nombre + '\')"><img src="/dist/img/imgmodal-04.png" alt="" style="margin-left: 10px; height: 12px; width: 12px; margin-bottom: 4px;"></a>'
			        	+ '<div id="'+ honorarios[x].id + 'INSTANCIA' +'" class="box-tools cd3" onclick="cerrarInstancia(\''+ honorarios[x].id +'\')"><img src="/dist/img/arrowblue.png" alt="" class="btn-box-tool">'
			        	+ '</div></div></div>'
			        	+ '<div class="box-body" id="'+honorarios[x].id+'" style="display: block;">'
			        	+ '<div class="box1 bloque">'
//			        	+ ' <div class="nombretabla2"><h4>Cliente</h4><p class="ct2">$000.00</p></div>'
			        	+ '<div class="celdadetalle">'
			        	+ '<p class="ct1">Tipo de Instancia</p>'
			        	+ '</div>'
			        	+ '<div class="celdadetalle sbb">'
			        	+ '<div class="buttons">'
			        	+ input1
			        	+ '<span></span>'
			        	+ '<label onclick="setTipoInstancia(\'' + true + '\',\'' + honorarios[x].id + '\')" for="radiobtn_1" style="font-weight:600;">Por convenio (escrito autom&aacute;tico c/ formulario y m&uacute;ltiple choice)</label>'
			        	+ '</div>'
			        	+ '<div class="buttons">'
			        	+ input2
			        	+ '<span></span>'
			        	+ '<label onclick="setTipoInstancia(\'' + false + '\',\'' + honorarios[x].id + '\')" for="radiobtn_2" style="font-weight:600;">Por Anticipo</label>'
			        	+ '</div>'
			        	+ '</div>'
			        	+ '<div class="box1 bloque2">'
			        	+ '<div class="celdaconcepto">'
			        	+ '<div class="cd5"><p>Detalle</p></div>'
			        	+ '<div class="cd5"><p>Forma de Pago</p></div>'
			        	+ '<div class="cd5"><p>Tasas</p></div>'
			        	+ '<div class="cd5"><p>Monto</p></div>'
			        	+ '<div class="cd5"><p>Vencimiento</p></div>'
			        	+ '<div class="cd6"><a href="#calcularJucio"><img src="/dist/img/suma2.png" alt="" onclick="modalInteres(\'' + honorarios[x].id + '\',\'' + 'HONORARIO1' + '\')"></a></div>'
			        	+ '</div>'
			        	+ '<div id="'+ honorarios[x].id + "TABLA1" +'"></div>'
			        	+ '</div>'
			        	+ '<div class="celdatotal2">'
			        	+ '<select name="" id="" class="ct1">'
			        	+ '<option value="">Monto Estimado</option>'
			        	+ '<option value="">Al finalizar: Por porcentaje</option>'
			        	+ '<option value="">Monto fijo</option>'
			        	+ '<option value="">RegulaciÃ³n del juez en la sentencia</option>'
			        	+ '<option value="">Hora de trabajo</option>'
			        	+ '<option value="">Sugerencia Liberium</option>'
			        	+ '</select>'
			        	+ '<p class="ct2" id="' + honorarios[x].id + "TOTAL1" + '"></p>'
			        	+ '</div>'
			        	+ '</div>'
			        	+ '<div class="box1 bloque">'
			        	+ '<div class="nombretabla2"><h4>Cobros <span>(AVISOS de cuotas devengadas al Usuario con habilitaci&oacute;n a otros usuarios y cliente)</span></h4></div>'
			        	+ '<div class="box1 bloque2">'
			        	+ '<div class="celdaconcepto">'
			        	+ '<div class="cd1"><p>Concepto</p></div>'
			        	+ '<div class="cd1"><p>Fecha</p></div>'
			        	+ '<div class="cd1"><p>Monto</p></div>'
			        	+ '<div class="cd3"><a href="#crearGastoHonorario" ><img src="/dist/img/suma2.png" alt="" onclick="modalGastoHonorario(\''+ honorarios[x].id +'\')"></a></div>'
			        	+ '</div>'
			        	+ '<div id="'+ honorarios[x].id + "TABLA2" +'"></div>'
			        	+ '</div>'
			        	+ '</div>'
			        	+ '<div class="box1">'
			        	+ '<div class="nombretabla2"><h4>Contraparte</h4></div>'
			        	+ '<div class="celdadetalle"><p class="ct1">Estimativo sobre demanda</p><p class="ct2" id="' + honorarios[x].id + "TOTAL3" + '"></p></div>'
			        	+ '<div class="box1 bloque2">'
			        	+ '<div class="celdaconcepto">'
			        	+ '<div class="cd5"><p>Detalle</p></div>'
			        	+ '<div class="cd5"><p>Forma de Pago</p></div>'
			        	+ '<div class="cd5"><p>Tasas</p></div>'
			        	+ '<div class="cd5"><p>Monto</p></div>'
			        	+ '<div class="cd5"><p>Vencimiento</p></div>'
			        	+ '<div class="cd6"><a href="#calcularJucio"><img src="/dist/img/suma2.png" alt="" onclick="modalInteres(\''+ honorarios[x].id +'\')"></a></div>'
			        	+ '</div>'
			        	+ '<div id="'+ honorarios[x].id + "TABLA3" +'"></div>'
			        	+ '</div>'
			        	+ '</div>'
			        	+ '</div>';
				buscarContablePorHonorario(honorarios[x].id);
			}
			
			$("#instancias").append(string);
			
		},
		error: function (data){
			console.log(data);
		}
	});
}

function buscarContablePorHonorario(idHonorario){
	
	$.ajax({
		url: '/api/contable-causa/buscar-contable-honorario/' + idHonorario,
		data: '',
		dataType: "json",
		type: "GET",
		success: function (data){
			
			$("#" + idHonorario + "TABLA1").empty(tablaHonorario1);
			$("#" + idHonorario + "TABLA2").empty(tablaHonorario2);
			$("#" + idHonorario + "TABLA3").empty(tablaHonorario3);
			
			var tablaHonorario1 = "";
			var tablaHonorario2 = "";
			var tablaHonorario3 = "";
			var total1 = 0;
			var total2 = 0;
			var total3 = 0;
			
			for (var i = 0; i < data.length; i++) {
				switch (data[i].uHtml) {
				case "HONORARIO1":
					var vencimiento = "";
					if (data[i].vencimiento != null) {
						vencimiento += '<p>' + data[i].vencimiento + '</p>';
					} else {
						vencimiento += '<p>' + 'Editar vencimiento' + '</p>';
					}
					tablaHonorario1 += '<div class="celdadetalle">'
					+ '<div class="cd5"><p>' + data[i].detalle + '</p></div>'
					+ '<div class="cd5"><p>Efectivo</p></div>'
					+ '<div class="cd5"><p>' + data[i].tipoDeTasa + '</p></div>'
					+ '<div class="cd5"><p>$' + data[i].montoCI + '</p></div>'
				 	+ '<div class="cd5"onclick="modalVencimiento(\'' + data[i].id + '\',\'' + data[i].vencimiento + '\')">'+vencimiento+'</div>'
					+ '<div class="cd6"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
					+ '</div>';
					if (data[i].tipoDePago == "Un solo pago") {
						total1 += data[i].montoCI;
					}
					break;
				case "HONORARIO2":
					tablaHonorario2 += '<div class="celdadetalle">'
					+ '<div class="cd1"><p>' + data[i].detalle + '</p></div>'
					+ '<div class="cd1"><p>' + data[i].desde + '</p></div>'
					+ '<div class="cd1"><p>$' + data[i].montoSI + '</p></div>'
					+ '<div class="cd3"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
					+ '</div>';
					break;
				case "HONORARIO3":
					var vencimiento = "";
					if (data[i].vencimiento != null) {
						vencimiento += '<p>' + data[i].vencimiento + '</p>';
					} else {
						vencimiento += '<p>' + 'Editar vencimiento' + '</p>';
					}
					tablaHonorario3 += '<div class="celdadetalle">'
						+ '<div class="cd5"><p>' + data[i].detalle + '</p></div>'
						+ '<div class="cd5"><p>Efectivo</p></div>'
						+ '<div class="cd5"><p>' + data[i].tipoDeTasa + '</p></div>'
						+ '<div class="cd5"><p>$' + data[i].montoCI + '</p></div>'
					 	+ '<div class="cd5"onclick="modalVencimiento(\'' + data[i].id + '\',\'' + data[i].vencimiento + '\')">'+vencimiento+'</div>'
						+ '<div class="cd6"><a onclick="eliminarContableCausa(\''+ data[i].id +'\')"><img src="/dist/img/eliminar2.png" alt=""></a></div>'
						+ '</div>';
					if (data[i].tipoDePago == "Un solo pago") {
						total3 += data[i].montoCI;
					}
					break;
				default:
					break;
				}
				
			}
			$("#" + idHonorario + "TABLA1").append(tablaHonorario1);
			$("#" + idHonorario + "TABLA2").append(tablaHonorario2);
			$("#" + idHonorario + "TABLA3").append(tablaHonorario3);
			$("#" + idHonorario + "TOTAL1").html("$" + total1);
			$("#" + idHonorario + "TOTAL2").html("$" + total2);
			$("#" + idHonorario + "TOTAL3").html("$" + total3);
			
		},
		error: function (data){
			console.log(data);
		}
	});
}

function calcularJucio(){
	
	$.ajax({
		url: '/api/contable-causa/calcular-juicio/',
		data: $("#formularioInteresJuicio").serialize(),
        dataType: "json",
        type: "POST",
        success: function (data) {
        	
        	buscarContabilidadCausa();
        },
 		error: function (data){
 			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + data.responseJSON.message + '</div>').show();
 			setTimeout(function () {
    			$("#modalAlerta").hide();
    	    }, 5000);
 		}
	});
	
}

function crearGasto(){
	$.ajax({
		url: '/api/contable-causa/crear-gasto',
		data: $("#formularioGasto").serialize(),
        dataType: "json",
        type: "POST",
        success: function (data) {
        	console.log(data);
        	buscarContabilidadCausa();
        },
 		error: function (data){
 			$("#modalAlertaGasto").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + data.responseJSON.message + '</div>').show();
 			setTimeout(function () {
    			$("#modalAlerta").hide();
    	    }, 5000);
 		}
	});
}

function eliminarContableCausa(idContable){
	
	$.ajax({
		url: '/api/contable-causa/eliminar/' + idContable,
		data: '',
		dataType: "json",
		type: "POST",
		success: function (data){
			
			if (data.error == null) {
				buscarContabilidadCausa();				
			}
			
		},
		error: function (data){
			
		}
		
	});
}

function editarVencimiento(){
	
	$.ajax({
		url: '/api/contable-causa/editar-vencimiento',
		data: $("#formularioVencimiento").serialize(),
		dataType: "json",
		type: "POST",
		success: function (data){
						
			buscarContabilidadCausa();
		},
		error: function (data){
			$("#modalAlertaVencimiento").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + data.responseJSON.message + '</div>').show();
 			setTimeout(function () {
    			$("#modalAlerta").hide();
    	    }, 5000);
		}
		
	});
	
}


function crearInstancia(){
	
	$.ajax({
		url: '/api/contable-causa/crear-instancia',
		data: $("#formularioInstancia").serialize(),
		dataType: "json",
		type: "POST",
		success: function (data){
						
			buscarContabilidadCausa();
		},
		error: function (data){
			$("#modalAlertaHonorario").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + data.responseJSON.message + '</div>').show();
 			setTimeout(function () {
    			$("#modalAlerta").hide();
    	    }, 5000);
		}
		
	});
}

function crearGastoHonorario(){
	
	$.ajax({
		url: '/api/contable-causa/crear-cobro',
		data: $("#formularioGastoHonorario").serialize(),
		dataType: "json",
		type: "POST",
		success: function (data){
						
			buscarContabilidadCausa();
		},
		error: function (data){
			$("#modalAlertaCobro").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + data.responseJSON.message + '</div>').show();
 			setTimeout(function () {
    			$("#modalAlerta").hide();
    	    }, 5000);
		}
		
	});
}

function setTipoInstancia(booleano, idHonorario){
	
	console.log(idHonorario + " " + booleano);
	
	$.ajax({
		url: '/api/contable-causa/set-tipo-instancia/' + idHonorario + '/' + booleano,
		data: '',
		dataType: "json",
		type: "POST",
		success: function (data){
			buscarContabilidadCausa();
		},
		error: function (data){
			
		}
		
	});
	
}

function modalEditarInstancia(idInstancia, nombre){
	if (idInstancia == null || nombre == null) {
		$("#nombreInstancia").val("");
		$("#idInstancia").val("");
	} else {
		$("#nombreInstancia").val(nombre);
		$("#idInstancia").val(idInstancia);
	}
	window.location = "#crearHonorarios";
}

function mostrarInstancia(id){
	$("#"+ id).show();
	$("#"+ id + "INSTANCIA").removeAttr("onclick");
	$("#"+ id + "INSTANCIA").attr('onclick','cerrarInstancia(\''+ id +'\')');
}

function cerrarInstancia(id){
	$("#" + id).hide();
	$("#"+ id + "INSTANCIA").removeAttr("onclick");
	$("#"+ id + "INSTANCIA").attr('onclick','mostrarInstancia(\''+ id +'\')');
}

function mostrarFechas(id){
	if (id != "SIN_INTERES") {
		$("#fechas").show();
	} else {
		$("#fechas").hide();
	}
}

function cuotas(valor){
	if (valor == "NO") {
		$("#numeroCuotasId").val('');
		$("#numeroDeCuotas").hide();
	} else {
		$("#numeroDeCuotas").show();
	}
}