$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarInteres() {
	$.ajax({
		url: '/api/interes/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El Interes fue creado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#error1").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el interes. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarInteres() {
	$.ajax({
		url: '/api/interes/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#editar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El interes fue editado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#error2").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al editar el interes. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function modificarInteres(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarPorcentaje").val(componente.getAttribute('porcentaje'));
	//window.location = '/administracion/interes/listado#editar';
}

function eliminar(componente) {
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminarInteres() {
	$.ajax({
		url: '/api/interes/eliminar',
		dataType: "json",
		data: $("#modalFormularioEliminar").serialize(),
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El interes fue eliminado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al eliminar el interes. ' + data.responseJSON.message
				+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}