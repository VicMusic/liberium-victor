
function mostrarPlantilla() {
	$("#boxplantilla").show();
	$("#boxescritos").hide();
	$("#boxplantillaEditar").hide();
	$("#boxplantillaMostrar").hide();

}
function remplazarEscrito(idPlantilla){
	var parrafos="";
	var html ="";
	$.ajax({
		url: '/api/plantilla/buscar/bloques/' + idPlantilla,
		dataType: 'json',
		type: 'GET',
		success:function(data){
			for(var i = 0; i<data.length; i++){
				parrafos+= "\t" + data[i].texto + "\n\n\n";
				
			}
			html = "<textarea name='texto' id='textoDeArea' class='textarea'" + 
			" style='width: 100%; height: 650px; font-size: 16	px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'"+
			"required=''>"+ parrafos+"</textarea>";
			
			$("#remplazoXPlantilla").empty().html(html);
			mostrarEscrito();
		},
		error: function(data){
			console.log("error");
			console.log(data.responseJSON.message);
		}
	});
}
function modificarPlantilla(titulo,idPlantilla){
	console.log("entre");
	var html = "";
//	var idPLantilla = componente.getAttribute('idPlantilla');
	$.ajax({
		url: '/api/plantilla/buscar/bloques/' + idPlantilla,
		dataType: 'json',
		type: 'GET',
		
		success:function(data){
			console.log("bloques");
			console.log(data);
			for(var i = 0; i<data.length; i++){
				console.log("entre al for: "+ i);
				 html += 
					'<div class="box1 cuestionario" id="cuestionario'+ data[i].orden +'">' + 
					'<div class="box1 agregarinfo2">'+
					'<select name="tipoBloque" id="tipoBloque'+ data[i].orden +'" required>' + 
						'<option value="'+ data[i].tipoBloque +'">'+ data[i].tipoBloque +'</option>'+
						'<option value="TEXTOCOMUN">Texto común</option>'+
						'<option value="DEMANDADO">Texto para Demandado</option>'+
						'<option value="DEMANDANTE">Texto para Demandante</option>'+
					'</select>'+
					'<input hidden name="id" value="'+ data[i].id +'">'+
	                  '<input hidden name="orden" value="'+ data[i].orden +'">'+
						'<input type="text" name="texto" id="tarea'+ data[i].orden +'" value="'+ data[i].texto +'"  class="textarea"'+
							'style="width: 100%; height: 50px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>'+
							'</div>'+
							'</div>';
				 $("#editarBloques").html(html);
			}
		},
		error: function(data){
			console.log("error");
			console.log(data.responseJSON.message);
		}
	});
	
	$("#editarTitulo").val(titulo);
	$("#modalEditarId").val(idPlantilla);
	$("#boxplantillaMostrar").hide();
	$("#boxplantilla").hide();
	$("#boxplantillaEditar").show();
	$("#boxescritos").hide();
}

function verPlantilla(titulo,idPlantilla){
	console.log("entre");
	var html = "";
//	var idPLantilla = componente.getAttribute('idPlantilla');
	$.ajax({
		url: '/api/plantilla/buscar/bloques/' + idPlantilla,
		dataType: 'json',
		type: 'GET',
		
		success:function(data){
			console.log("bloques");
			console.log(data);
			for(var i = 0; i<data.length; i++){
				console.log("entre al for: "+ i);
				 html += 
					'<div class="box1 cuestionario" id="cuestionario'+ data[i].orden +'">' + 
					'<div class="box1 agregarinfo2">'+
						'<input type="text" readonly="readonly" name="texto" id="tarea'+ data[i].orden +'" value="'+ data[i].texto +'"  class="textarea"'+
							'style="width: 100%; height: 50px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>'+
							'</div>'+
							'</div>';
				 $("#verBloques").html(html);
			}
		},
		error: function(data){
			console.log("error");
			console.log(data.responseJSON.message);
		}
	});
	
	$("#verTitulo").val(titulo);
	$("#modalVerId").val(idPlantilla);
	$("#boxplantilla").hide();
	$("#boxplantillaEditar").hide();
	$("#boxplantillaMostrar").show();
	$("#boxescritos").hide();
}


function guardarPlantilla() {
	console.log($("#modalFormularioPlantilla").serialize());
	$
			.ajax({
				url : '/api/plantilla/guardar',
				data : $("#modalFormularioPlantilla").serialize(),
				dataType : "json",
				type : "POST",
				success : function(data) {

					console.log(data.id);
					console.log(data.titulo);

					$("#modalAlertaBloque")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La plantilla fue creada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#modalAlertaBloque").hide();
					}, 5000);

					window.location = '/administracion/escrito/crear';

				},
				error : function(data) {
					$("#modalAlertaPlantilla")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al crear la plantilla. '
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#modalAlertaPlantilla").hide();
					}, 7000);
				}
			});
}

function editarPlantilla() {
	$
			.ajax({
				url : '/api/plantilla/guardar',
				data : $("#modalFormularioEditar").serialize(),
				dataType : "json",
				type : "POST",
				success : function(data) {
					$("#editar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La plantilla fue editada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					 window.location = '/administracion/escrito/crear'; 
					// quiero que se me recargue la página

				},
				error : function(data) {
					$("#modalAlerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al editar la plantilla. '
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#modalAlerta").hide();
					}, 5000);
				}
			});
}

//function modificarPlantilla(componente) {
//	$("#modalEditarId").val(componente.getAttribute('objeto'));
//	$("#modalEditarTitulo").val(componente.getAttribute('titulo'));
//	$("#agregar").show();
//}

function eliminarPlantilla(componente) {
	$("#modalPlantilla").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {

	$
			.ajax({
				url : '/api/plantilla/eliminar?id='
						+ $("#modalEliminarId").val(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#eliminar").hide();
					$("#modalConfirmar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La plantilla fue eliminada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location = '/administracion/escrito/crear';

				},
				error : function(data) {
					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al eliminar la plantilla. '
											+ data.responseJSON.message
											+ '</div>').show();
					$("#modalConfirmar").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);
				}
			});

}

function volver() {
	console.log("CERRAR");
	window.location = '/administracion/escrito/crear';
}
