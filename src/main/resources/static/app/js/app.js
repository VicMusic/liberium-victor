function buscarLocalidadJudiciales(id, localidad) {

	$.ajax({
		url: '/api/usuario/combo2/' + id,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var selectList = "<option disabled value=''>Seleccione una localidad</option>";
			for (var x = 0; x < data.length; x++) {
				if (localidad == data[x].id) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
			}
			$('#localidad').html(selectList);
			$('#modalEditarLocalidad').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function guardarUsuario() {
	$
		.ajax({
			url: '/api/usuario/guardar',
			data: $("#modalFormulario").serialize(),
			dataType: "json",
			type: "POST",
			success: function(data) {
				$("#agregar").hide();

				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue creado con éxito. La contraseña por defecto de los usuarios creados por este medio es: "liberium".'
						+ '</div>').show();

				setTimeout(function() {
					$("#alerta").hide();
					window.location = '/administracion/usuario/listado';

				}, 2000);


			},
			error: function(data) {
				$("#modalAlerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al crear el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
				setTimeout(function() {
					$("#modalAlerta").hide();
				}, 5000);
			}
		});
}

function modificarAbogado() {
	$.ajax({
		url: '/api/abogado/actualizar',
		data: $("#formAbogado").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			window.location = '/administracion/usuario/perfil';
		},
		error: function(data) {
			$("#alertaDato2").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ data.responseJSON.message
				+ '</div>').show();
		}
	});
}

function editarUsuarie() {
	$
		.ajax({
			url: '/api/usuario/guardar',
			data: $("#modalFormularioEditar").serialize(),
			dataType: "json",
			type: "POST",
			success: function(data) {
				$("#editar").hide();

				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue editado con éxito'
						+ '</div>').show();

				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);

				window.location = '/administracion/usuario/listado';

			},
			error: function(data) {
				$("#modalAlerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al editar el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
				setTimeout(function() {
					$("#modalAlerta").hide();
				}, 5000);
			}
		});
}

function modificarUsuarioA(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarEmail").val(componente.getAttribute('mail'));
	$("#modalEditarRol").val(componente.getAttribute('rol'));

	$("#agregar").show();
}

function modificarJudiciales(componente) {
	$("#modalEditarProvincia").val(componente.getAttribute('provincia')).trigger("change");
	buscarLocalidadJudiciales(componente.getAttribute('provincia'), componente.getAttribute('localidad'));
	console.log("Materias: " + componente.getAttribute('materias'));
	var materias = componente.getAttribute('materias');
	console.log("Vamos: " + materias);
}

function modificarUsuario() {
	// $("#modalEditarId").val(componente.getAttribute('objeto'));
	// $("#modalEditarNombre").val(componente.getAttribute('nombre'));
	// $("#modalEditarEmail").val(componente.getAttribute('mail'));
	// $("#agregar").show();
	//	 
	console.log("entre");
	$
		.ajax({

			url: '/api/usuario/editar',
			data: $("#editarUsuario").serialize(),
			dataType: "json",
			type: "POST",
			async: true,
			success: function(data) {
				window.location.href = "/administracion/usuario/perfil";
			},
			error: function(data) {
				$("#alertaDato")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al editar el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
				setTimeout(function() {
					$("#alertaDato").hide();
				}, 5000);
			}
		});
}

function eliminarUsuario(componente) {
	$("#modalUsuario").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {

	$
		.ajax({
			url: '/api/usuario/eliminar?id=' + $("#modalEliminarId").val(),
			dataType: "json",
			type: "POST",
			async: true,
			success: function(data) {

				$("#eliminar").hide();
				$("#modalConfirmar").hide();

				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue eliminado con éxito'
						+ '</div>').show();

				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);

				window.location = '/administracion/usuario/listado';

			},
			error: function(data) {
				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
				$("#modalConfirmar").modal("hide");
				setTimeout(function() {
					$("#alerta").hide();
				}, 5000);
			}
		});
}

function guardarDuracion() {

	var duracion = parseFloat($("#duracion").val());
	console.log(duracion);
	$
		.ajax({
			url: '/api/abogado/duracion' + duracion,
			dataType: "json",
			type: "POST",
			success: function(data) {
				$("#agregar").hide();
				console.log("funciono");
				$("#alerta")
					.empty()
					.html(
						'<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El usuario fue creado con éxito. La contraseña por defecto de los usuarios creados por este medio es: "liberium".'
						+ '</div>').show();

				setTimeout(function() {
					$("#alerta").hide();

				}, 2000);


			},
			error: function(data) {
				$("#modalAlerta")
					.empty()
					.html(
						'<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al crear el usuario. '
						+ data.responseJSON.message
						+ '</div>').show();
				setTimeout(function() {
					$("#modalAlerta").hide();
				}, 5000);
			}
		});
}