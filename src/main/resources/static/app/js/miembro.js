function editar(componente) {
	resetFormulario();
	$("#id").val(componente.getAttribute('objeto'));
	$("#nombre").val(componente.getAttribute('nombre'));
	$("#mail").val(componente.getAttribute('mail'));
	var tipoUsuario = componente.getAttribute('tipoUsuario');
	$("#tipoUsuario").val(tipoUsuario);
	console.log(componente.getAttribute('rol'));
	if(tipoUsuario == "OPERADOR"){
		$("#divPermisos").show();
		var permisos = JSON.parse(componente.getAttribute('permisosUsuario'));
		for (var i = 0; i < permisos.length; i++) {
			$("#" + permisos[i].id).attr("checked", true);
		}
	}
	
	$("#agregar").show();
}

function visualizar(componente){
	editar(componente);
	$("#modalFormulario *").prop('disabled', true);
}

function resetFormulario(){
	$("#modalFormulario *").prop('disabled', false);
	$("#modalFormulario").trigger("reset");
	$("#id").val("");
	$('input:checkbox').removeAttr('checked');
	$("#divPermisos").hide();
}

//function eliminarUsuario(componente) {
//	$("#eliminar").modal("toggle");
//	$("#modalEliminarId").val(componente.getAttribute('objeto'));
//}
//
//function eliminar() {
//	$.ajax({
//		url : '/api/usuario/eliminar?id=' + $("#modalEliminarId").val(),
//		dataType : "json",
//		type : "POST",
//		async : true,
//		success : function(data) {
//			$("#eliminar").modal('hide');
//			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
//						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
//						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
//						+ 'El usuario fue eliminado con éxito'
//						+ '</div>').show();
//			
//			setTimeout(function() {
//				window.location = '/administracion/usuario/listado';
//			}, 1000);
//		},
//		error : function(data) {
//			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
//						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
//						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
//						+ 'Ha ocurrido un error al eliminar el usuario. '
//						+ data.responseJSON.message
//						+ '</div>').show();
//			$("#eliminar").modal("hide");
//			setTimeout(function() {
//				$("#alerta").hide();
//			}, 5000);
//		}
//	});
//}