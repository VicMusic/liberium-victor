function crear(){
	$.ajax({
		url : '/api/contable/guardar',
		data : $("#formularioContabilidad").serialize(),
		dataType : "json",
		type : "POST",
		success : function (data){
			console.log(data);
			asignarEstudio(data.mEstudio.id);
		},
		error : function (data){
			$("#error").empty().html('<div class="alert alert-danger">'
					 + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					 + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					 + data.responseJSON.message
					 + '</div>').show();
			setTimeout(function() {
			$("#error").hide();
			}, 5000);
		}
	});
}

function eliminar(id){
	console.log(id);
	$.ajax({
		url : '/api/contable/eliminar/' + id,
		dataType : "json",
		type : "POST",
		success : function (data){
			console.log(data);
			asignarEstudio(data.mEstudio.id);
		},
		error : function (data){
			
		}
	});
}

function crearHonorario(){
	$.ajax({
		url : '/api/contable/guardarHonorario',
		data : $("#formularioHonorario").serialize(),
		dataType : "json",
		type : "POST",
		success : function (data){
			console.log(data);
			asignarEstudio(data.estudio.id);
		},
		error : function (data){
			$("#errorHonorario").empty().html('<div class="alert alert-danger">'
					 + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					 + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					 + data.responseJSON.message
					 + '</div>').show();
			setTimeout(function() {
			$("#errorHonorario").hide();
			}, 5000);
		}
	});
}

function eliminarHonorario(id){
	$.ajax({
		url : '/api/contable/eliminarHonorario/' + id,
		dataType : "json",
		type : "POST",
		success : function (data){
			console.log(data);
			asignarEstudio(data.estudio.id);
		},
		error : function (data){
			
		}
	});
}

function buscarAbogadosEstudio(){
	$.ajax({
		url: '/api/contable/buscarMiembros/' + $("#idEstudio").val(),
		dataType: 'json',
		type: 'GET',
		success: function (data){
			console.log(data);
			$('#detalle').hide();
			$('#selectMiembros').empty();
			var selectMiembros = '<label>Miembro</label>'
	          	 			   + '<select name="idMiembro">'
	          	 			   + '<option disabled="disabled" selected="selected" hidden="">Seleccione un Miembro</option>';
			for (var x = 0; x < data.length; x++) {
				selectMiembros += '<option value="' + data[x].id + '">' + data[x].mUsuario.nombre + '</option>'
			}
			selectMiembros += '</select>';
			$('#selectMiembros').append(selectMiembros);
		},
		error: function (data){
			console.log(data);
		}
	});
}

function asignarEstudioConFechas(){
	asignarEstudio($("#idEstudio").val(), $("#startDate").val(), $("#endDate").val());
}

function asignarEstudio(valor, startDate, endDate) {
	console.log(valor, startDate, endDate);
	$("#idEstudio").val(valor);
	$("#idEstudioHonorario").val(valor);
	$.ajax({
        url: '/api/contable/buscar/' + valor +"?fechaInicio=" + startDate + "&fechaFin=" + endDate,
        dataType: "json",
        type: "GET",
        success: function (data) {
        	$("#tablaIngresos").empty();
        	$("#tablaActivos").empty();
        	$("#tablaGastos").empty();
        	$("#tablaOtrosGastos").empty();
        	$("#totalIngresos").empty();
        	$("#totalActivos").empty();
        	$("#totalGastos").empty();
        	$("#totalOtrosGastos").empty();
        	$("#totalDeTotalesIngresos").empty();
        	$("#totalDeTotalesGastos").empty();
        	$("#totalDeTotales").empty();
        	$("#saldoCaja").empty();
        	$("#saldoCajaBanco").empty();
        	$("#saldoDistribuciones").empty();
        	
        	var tablaIngresos = "";
        	var tablaActivos = "";
        	var tablaGastos= "";
        	var tablaOtrosGastos= "";
        	
        	var totalIngresos = 0;
        	var totalActivos = 0;
        	var totalGastos= 0;
        	var totalOtrosGastos= 0;
        	
        	for (var i = 0; i < data.length; i++) {

        		switch (data[i].tipoContabilidad) {
				case "INGRESO":
					tablaIngresos +=  '<div class="celdate">'
						+ '<div class="ctc8"><p>' + data[i].detalle + '</p></div>'
						+ '<div class="ctc8"><p>' + data[i].tipoPago + '</p></div>'
						+ '<div class="ctc9"><p>' + data[i].fechaContabilidad + '</p></div>'
						+ '<div class="ctc9 textr"><p>' + currencyFormat(data[i].monto) + '</p></div>'
						+ '<div class="ctc6"><a onclick="eliminar(\'' + data[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					totalIngresos += data[i].monto;
					break;
				case "ACTIVO":
					tablaActivos +=  '<div class="celdate">'
						+ '<div class="ctc8"><p>' + data[i].detalle + '</p></div>'
						+ '<div class="ctc8"><p>' + data[i].tipoPago + '</p></div>'
						+ '<div class="ctc9"><p>' + data[i].fechaContabilidad + '</p></div>'
						+ '<div class="ctc9 textr"><p>' + currencyFormat(data[i].monto) + '</p></div>'
						+ '<div class="ctc6"><a onclick="eliminar(\'' + data[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					totalActivos += data[i].monto;
					break;
				case "GASTO":
					tablaGastos +=  '<div class="celdate">'
						+ '<div class="ctc8"><p>' + data[i].detalle + '</p></div>'
						+ '<div class="ctc8"><p>' + data[i].tipoPago + '</p></div>'
						+ '<div class="ctc9"><p>' + data[i].fechaContabilidad + '</p></div>'
						+ '<div class="ctc9 textr"><p>' + currencyFormat(data[i].monto) + '</p></div>'
						+ '<div class="ctc6"><a onclick="eliminar(\'' + data[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					totalGastos += data[i].monto;
					break;
				case "OTROGASTO":
					tablaOtrosGastos +=  '<div class="celdate">'
						+ '<div class="ctc8"><p>' + data[i].detalle + '</p></div>'
						+ '<div class="ctc8"><p>' + data[i].tipoPago + '</p></div>'
						+ '<div class="ctc9"><p>' + data[i].fechaContabilidad + '</p></div>'
						+ '<div class="ctc9 textr"><p>' + currencyFormat(data[i].monto) + '</p></div>'
						+ '<div class="ctc6"><a onclick="eliminar(\'' + data[i].id + '\')")><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					totalOtrosGastos += data[i].monto;
					break;
				default:
					break;
				}
			}
        	var totalCaja = (totalIngresos + totalActivos) - (totalGastos + totalOtrosGastos);
        	
        	$("#tablaIngresos").append(tablaIngresos);
        	$("#tablaActivos").append(tablaActivos);
        	$("#tablaGastos").append(tablaGastos);
        	$("#tablaOtrosGastos").append(tablaOtrosGastos);
        	$("#totalIngresos").append(currencyFormat(totalIngresos));
        	$("#totalActivos").append(currencyFormat(totalActivos));
        	$("#totalGastos").append(currencyFormat(totalGastos));
        	$("#totalOtrosGastos").append(currencyFormat(totalOtrosGastos));
        	
        	$("#totalDeTotalesIngresos").append(currencyFormat(totalIngresos + totalActivos));
        	$("#totalDeTotalesGastos").append(currencyFormat(totalGastos + totalOtrosGastos));
        	$("#totalDeTotales").append(currencyFormat(totalCaja));
        	$("#saldoCaja").append(currencyFormat(totalCaja));
        	$("#boxContabilidad").show();
        	
        	buscarHonorarios(valor, data, totalCaja);
        	window.location = "/contabilidad/ver#";
        },
        error: function(data){
        	console.log(data);
        }
    }); 
}

function buscarHonorarios(idEstudio, contables, totalCaja){
	console.log("METHOD: buscarHonorarios()");
	$.ajax({
        url: '/api/contable/buscarHonorarios/' + idEstudio,
        dataType: "json",
        type: "GET",
        success: function (data){
        	$("#distribuciones").empty();
        	$("#bancos").empty();
        	var bancos = "";
        	var distribuciones = "";
        	var saldoMovimientosBancarios = 0;
        	var saldoDistribucion = 0;
        	for (var x = 0; x < data.length; x++) {
        		var totalBancoIngresos = 0;
        		var totalBancoEngresos = 0;
        		var totalDistriIngresos = 0;
        		var totalDistriEngresos = 0;
        		if (data[x].tipoHonorario == "BANCO") {
					bancos += '<div class="nombretabla" style="margin-top:20px;"><h4>BANCO ' + data[x].nombre + '</h4>'
    						+ '<div class="ctc6"><a onclick="eliminarHonorario(\'' + data[x].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					bancos += '<div class="tablaingresos">'
							+ '<div class="celdati">'
							+ '<div class="ctc8"><p>Detalle</p></div>'
							+ '<div class="ctc8"><p>Forma de Pago</p></div>'
							+ '<div class="ctc9"><p>Fecha</p></div>'
							+ '<div class="ctc9"><p class="textr">Monto</p></div>'
							+ '<div class="ctc6"><a onclick="mostrarModal(\'' + "BANCOINGRESO" +  '\',\'' + data[x].id +'\')"><img src="/dist/img/suma2.png" alt=""></a></div>'
							+ '</div>';
					for (var i = 0; i < contables.length; i++) {
						if (contables[i].mHonorario.id != null && contables[i].mHonorario.id == data[x].id && contables[i].tipoContabilidad == "BANCOINGRESO") {
							bancos +=  '<div class="celdate">'
								+ '<div class="ctc8"><p>' + contables[i].detalle + '</p></div>'
								+ '<div class="ctc8"><p>' + contables[i].tipoPago + '</p></div>'
								+ '<div class="ctc9"><p>' + contables[i].fechaContabilidad + '</p></div>'
								+ '<div class="ctc9 textr"><p>' + currencyFormat(contables[i].monto) + '</p></div>'
								+ '<div class="ctc6"><a onclick="eliminar(\'' + contables[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
							totalBancoIngresos += contables[i].monto;
						}
					}
					bancos += '<div class="celdati2"><div class="ctc10"><p>TOTAL INGRESOS</p></div><div class="ctc9 textr"><p>' + currencyFormat(totalBancoIngresos) + '</p></div></div>'
							+ '</div><div class="tablaegresos"><div class="box1"><div class="celdati">'
							+ '<div class="ctc8"><p>Detalle</p></div>'
							+ '<div class="ctc8"><p>Forma de Pago</p></div>'
							+ '<div class="ctc9"><p>Fecha</p></div>'
							+ '<div class="ctc9"><p class="textr">Monto</p></div>'
							+ '<div class="ctc6"><a  onclick="mostrarModal(\'' + "BANCOEGRESO" +  '\',\'' + data[x].id +'\')"><img src="/dist/img/suma2.png" alt=""></a></div></div>';
					for (var i = 0; i < contables.length; i++) {
						if (contables[i].mHonorario.id != null && contables[i].mHonorario.id == data[x].id && contables[i].tipoContabilidad == "BANCOEGRESO") {
							bancos +=  '<div class="celdate">'
								+ '<div class="ctc8"><p>' + contables[i].detalle + '</p></div>'
								+ '<div class="ctc8"><p>' + contables[i].tipoPago + '</p></div>'
								+ '<div class="ctc9"><p>' + contables[i].fechaContabilidad + '</p></div>'
								+ '<div class="ctc9 textr"><p>' + currencyFormat(contables[i].monto) + '</p></div>'
								+ '<div class="ctc6"><a onclick="eliminar(\'' + contables[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
							totalBancoEngresos += contables[i].monto;
						}
					}
					saldoMovimientosBancarios += totalBancoIngresos - totalBancoEngresos;
					bancos += '<div class="celdati2"><div class="ctc10"><p>TOTAL EGRESOS</p></div><div class="ctc9 textr"><p>' + currencyFormat(totalBancoEngresos) + '</p></div></div></div></div>';
					bancos += '<div class="celdati"><div class="ctc1"><p>TOTAL BANCO NOMBRE</p></div>'
							+ '<div class="ctc2"></div><div class="ctc2"><p>' + currencyFormat(totalBancoIngresos - totalBancoEngresos) + '</p></div></div>';
				} else if (data[x].tipoHonorario == "DISTRIBUCION"){
					distribuciones += '<div class="nombretabla" style="margin-top:20px;"><h4>DISTRIBUCION ' + data[x].nombre + '</h4>'
					+ '<div class="ctc6"><a onclick="eliminarHonorario(\'' + data[x].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
					distribuciones += '<div class="tablaingresos" style="width: 100%;">'
							+ '<div class="celdati">'
							+ '<div class="ctc8"><p>Miembro Estudio</p></div>'
							+ '<div class="ctc8"><p>Forma de Pago</p></div>'
							+ '<div class="ctc9"><p>Fecha</p></div>'
							+ '<div class="ctc9"><p class="textr">Monto</p></div>'
							+ '<div class="ctc6"><a onclick="mostrarModal(\'' + "DISTRIINGRESO" +  '\',\'' + data[x].id +'\')"><img src="/dist/img/suma2.png" alt=""></a></div>'
							+ '</div>';
					for (var i = 0; i < contables.length; i++) {
						if (contables[i].mHonorario.id != null && contables[i].mHonorario.id == data[x].id && contables[i].tipoContabilidad == "DISTRIINGRESO") {
							distribuciones +=  '<div class="celdate">'
								+ '<div class="ctc8"><p>' + contables[i].mMiembro.mUsuario.nombre + '</p></div>'
								+ '<div class="ctc8"><p>' + contables[i].tipoPago + '</p></div>'
								+ '<div class="ctc9"><p>' + contables[i].fechaContabilidad + '</p></div>'
								+ '<div class="ctc9 textr"><p>' + currencyFormat(contables[i].monto) + '</p></div>'
								+ '<div class="ctc6"><a onclick="eliminar(\'' + contables[i].id + '\')"><img src="/dist/img/eliminar2.png" alt=""></a></div></div>';
							totalDistriIngresos += contables[i].monto;
						}
					}
					saldoDistribucion += totalDistriIngresos;
					distribuciones += '<div class="celdati2"><div class="ctc10"><p>TOTAL INGRESOS</p></div><div class="ctc9 textr"><p>' + currencyFormat(totalDistriIngresos) + '</p></div></div>';
					distribuciones += '<div class="celdati"><div class="ctc1"><p>TOTAL DISTRIBUCION</p></div>'
							+ '<div class="ctc2"></div><div class="ctc2"><p>' + currencyFormat(totalDistriIngresos - totalDistriEngresos) + '</p></div></div>';
				}
        	}
        	
        	var saldoPositivo = totalCaja + saldoMovimientosBancarios;
        	console.log(totalCaja);
        	
        	bancos += '<div class="celdati backgrey"><div class="ctc1"><p>SALDO MOVIMIENTOS BANCARIOS</p></div><div class="ctc2"></div><div class="ctc2"><p>' + currencyFormat(saldoMovimientosBancarios) + '</p></div></div>';
        	distribuciones += '<div class="celdati backgrey"><div class="ctc1"><p>SALDO DISTRIBUCIÓN</p></div><div class="ctc2"></div><div class="ctc2"><p>' + currencyFormat(saldoDistribucion) + '</p></div></div>'
            				+ '<div class="celdati"><div class="ctc1"><p>PATRIMONIO NETO</p></div><div class="ctc2"></div><div class="ctc2"><p>' + currencyFormat(saldoPositivo - saldoDistribucion) + '</p></div></div>'
        	$("#saldoCajaBanco").append(currencyFormat(saldoMovimientosBancarios));
        	$("#saldoDistribuciones").append(currencyFormat(saldoDistribucion));
        	$("#bancos").append(bancos);
        	$("#distribuciones").append(distribuciones);
        },
        error: function (data){
        	console.log(data);
        }
	});
}

function mostrarModal(valor, idHonorario){
	$("#formularioHonorario").trigger('reset');
	$("#formularioContabilidad").trigger('reset');

	$('#selectMiembros').empty();
	switch (valor) {
	case "INGRESO":
		$('#detalle').show();
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "ACTIVO":
		$('#detalle').show();
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "GASTO":
		$('#detalle').show();
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "OTROGASTO":
		$('#detalle').show();
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "BANCOINGRESO":
		$('#detalle').show();
		$("#idHonorario").val(idHonorario);
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "BANCOEGRESO":
		$('#detalle').show();
		$("#idHonorario").val(idHonorario);
		$("#tipoContabilidad").val(valor);
		window.location = '/contabilidad/ver#crear';
		break;
	case "DISTRIINGRESO":
		$("#idHonorario").val(idHonorario);
		$("#tipoContabilidad").val(valor);
		buscarAbogadosEstudio();
		window.location = '/contabilidad/ver#crear';
		break;
	case "BANCO":
		$("#creacionText").empty();
		$("#honorarioLabel").empty();
		$("#creacionText").append("Creacion de un Banco");
		$("#honorarioLabel").append("Nombre Banco");
		$('#inputText').attr('placeholder','Ingrese el nombre de el banco');
		$("#inputText").attr('type', 'text');
		$("#tipoHonorario").val(valor);
		window.location = '/contabilidad/ver#crearHonorario';
		break;
	case "DISTRIBUCION":
		$("#creacionText").empty();
		$("#honorarioLabel").empty();
		$("#creacionText").append("Creacion de una Distribucion");
		$("#honorarioLabel").append("Fecha de la Distribucion");
		$("#inputText").attr('type', 'date');
		$("#tipoHonorario").val(valor);
		window.location = '/contabilidad/ver#crearHonorario';
		break;
	default:
		break;
	}
}

function currencyFormat(num) {
	  return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

