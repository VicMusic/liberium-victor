var mesG = '';
var anioG = '';

$(document).ready(function() {
	cargarTodosLosEventos();
	$("#wrapper").show();
	var id1 = $("#id1").val();
	if(id1 != null && id1 != ''){
		cargar(id1);
	}
});

function calendar(eventos) {

	$('#modalHorarioEvento').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		timePickerIncrement : 10,
		locale : {
			format : 'DD/MM/YYYY H:mm'
		}
	});
	// $('#visualizar').click(function(){
	// refrescar();
	// });
	var tempVar = "";

	setTimeout(function() {
		$("#contenedor").show();
		$("#wrapper").remove();

		$('#calendar').fullCalendar({
			monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
		    dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
			locale : 'es',
			eventLimit : true,
			header : {
				left : 'prev,next today',
				center : 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			buttonText : {
				today : 'Hoy',
				month : 'Mensual',
				week : 'Semanal',
				day : 'Diario'
			},
			timeZone : 'local',
			events : eventos,
			slotLabelFormat : "HH:mm",
			timeFormat : 'HH:mm',
			eventClick : function(event) {
				verEvento(event);
				return false;
			},
			dayClick : function(date, jsEvent, view) {
				listarEventosPorDia(date.format());

				if (tempVar == "") {
					$(this).css('background-color', '#ffeaea');
					tempVar = this;
				} else {
					$(this).css('background-color', '#ffeaea');
					$(tempVar).css('background-color', 'white');
					tempVar = this;
				}
			},
			viewRender : function(view, element) {
				render();
			}

		});
	}, 1000);
}

function cargarTodosLosEventos() {
	var eventos = [];

	$.ajax({
		url : '/api/calendario/listar/' + $("#idUsuario").val(),
		data : '',
		dataType : "json",
		type : "GET",
		success : function(data) {

			for (var i = 0; i < data.length; i++) {

				var evento = {
					title : '',
					color : '',
					start : '',
					end : ''
				};

				evento.id = data[i].id;
				evento.title = data[i].titulo;
				evento.color = data[i].color;
				evento.start = data[i].fechaInicio;

				if (evento.color == "coral") {
					evento.start = data[i].fechaFin;
				} else {
					evento.end = data[i].fechaFin;
				}

				if (evento.color == "navy") {
					evento.rendering = 'background';
					evento.backgroundColor = '#B0B0B0';
					evento.allDay = true;
				}

				eventos.push(evento);

			}

			$('#calendar').fullCalendar('removeEvents');
			$('#calendar').fullCalendar('addEventSource', eventos);
			$('#calendar').fullCalendar('rerenderEvents');

			calendar(eventos);

		},
		error : function(data) {

		}
	});
}

function traerDiasNoLaborables(){
	
	$.ajax({
		url: '/api/calendario/listardiasnohabiles',
		data: '',
		dataType: "json",
		type: "GET",
		success : function(data){
			
			var textoEvento = "";
			
			for (let evento of data) {
				console.log(evento);

				var fecha1 = evento.fechaInicio;
				var fecha2 = evento.fechaFin;

				if (fecha1 != null && fecha2 != null) {
					fecha1 = fecha1.slice(0, 16);
					fecha2 = fecha2.slice(0, 16);
					fecha1 = fecha1.replace("T", " ");
					fecha2 = fecha2.replace("T", " ");
				}
			
				var tipoEvento = enumATexto(evento.tipoEvento);
				
				var tresPuntos = '<ul class="trespuntos" sec:authorize="hasAuthority(\'EDITAR_DIAS_NO_LABORABLES\')">'
						+ '<li class="dropdown user user-menu trespuntosli">'
						+ '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
						+ '<div class="">'
						+ '<img src="/dist/img/3puntos.png" alt="">'
						+ '</div></a>'
						+ '<ul class="dropdown-menu trespuntosmenu"><li>'
						+ '<a sec:authorize="hasAuthority(\'EDITAR_AGENDA_PROPIA\')" href="#editar" onclick="editarEvento(\'' + evento.id + '\')">'
						+ '<img src="/dist/img/imgmodal-04.png" alt="">'
						+ '<p>Editar</p></a></li><li>'
						+ '<a sec:authorize="hasAuthority(\'EDITAR_AGENDA_PROPIA\')" href="#eliminar" onclick="eliminarEvento(\'' + evento.id + '\')">'
						+ '<img src="/dist/img/iconomenu-01.png" alt="">'
						+ '<p>Eliminar</p></a></li></ul></li></ul>';
						

				textoEvento += '<hr><div class="row">'
						+ '<div class="box1">'
						+ '<label>Titulo: </label>' + tresPuntos
						+ '<span class="span-modal">'
						+ evento.titulo
						+ '</span>'
						+ '</div>'
						+ '<div class="box1">'
						+ '<label>Tipo Evento: </label>'
						+ ' <span class="span-modal">'
						+ tipoEvento
						+ '</span>'
						+ '</div>'
						+ '<div class="box1">'
						+ '<label>Horarios: </label> <span class="span-modal">'
						+ fecha1 + " - " + fecha2
						+ '</span>' + '</div>'
						+ '</div>';
			}
			
			$("#eventosNoLaborales").html(textoEvento);
			
			
		},
		error : function(data){
			
		}
	});
	
}

function listasEventosPorTipoEvento(data) {

	var eventos = [];

	var textoEnum = data;

	if (data == "TODOS") {
		cargarTodosLosEventos();
	} else {
	if (/\s/.test(data)) {
		textoEnum = data.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = data.toUpperCase();
	}

	$.ajax({
		url : '/api/calendario/q/' + textoEnum,
		data : '',
		dataType : "json",
		type : "GET",
		success : function(data) {

			for (var i = 0; i < data.length; i++) {

				var evento = {
					title : '',
					color : '',
					start : '',
					end : ''
				};

				evento.id = data[i].id;
				evento.title = data[i].titulo;
				evento.color = data[i].color;
				evento.start = data[i].fechaInicio;

				if (evento.color == "coral") {
					evento.start = data[i].fechaFin;
				} else {
					evento.end = data[i].fechaFin;
				}

				if (evento.color == "navy") {
					evento.rendering = 'background';
					evento.backgroundColor = '#B0B0B0';
					evento.allDay = true;
				}

				eventos.push(evento);
			}

			$('#calendar').fullCalendar('removeEvents');
			$('#calendar').fullCalendar('addEventSource', eventos);
			$('#calendar').fullCalendar('rerenderEvents');

			calendar(eventos);

		},
		error : function(data) {

		}
	});
	
	}

	
}

function listarEventosPorDia(dia) {
		$.ajax({
				url : '/api/calendario/ver/dia',
				data : "dia=" + dia,
				type : "GET",

				success : function(resultado) {

					if (resultado.length == 0) {
						crearEventoDia(dia);
					} else {
						var textoEvento = "";

						if (jQuery.isEmptyObject(resultado)) {
							window.location = "#modal1";
						} else {
							
							$.each(resultado, function(key1, evento) {
	
							var fecha1 = evento.fechaInicio;
							var fecha2 = evento.fechaFin;
							fecha1 = fecha1.slice(0, 16);
							fecha2 = fecha2.slice(0, 16);
							fecha1 = fecha1.replace("T", " ");
							fecha2 = fecha2.replace("T", " ");
	
							var tipoEvento = enumATexto(evento.tipoEvento);
	
							textoEvento += '<div class="row">'
									+ '<div class="box1">'
									+ '<label>Título: </label>'
									+ '<span class="span-modal">'
									+ evento.titulo
									+ '</span>'
									+ '</div>'
									+ '<div class="box1">'
									+ '<label>Tipo Evento: </label>'
									+ ' <span class="span-modal">'
									+ tipoEvento
									+ '</span>'
									+ '</div>'
									+ '<div class="box1">'
									+ '<label>Horarios: </label> <span class="span-modal">'
									+ fecha1 + " - " + fecha2
									+ '</span>' + '</div>'
									+ '</div>' + '<hr>';
	
						});
							$("#eventosDias").html(textoEvento);
							window.location = "#listadoEventos";
						}
					}
				},
				error : function(resultado) {

				}
			});
}

function crearEventoDia(dia) {
	document.getElementById("tituloEvento").value = "";
	document.getElementById("selectEvento").value = "";
	$("#fechaInicioAgregar").val(dia + "T00:00");
	$("#fechaFinAgregar").val(dia + "T00:00");
	window.location.href = "#agregar";
}

function verEvento(event) {
	console.log(event);
	$.ajax({
		url : '/api/calendario/ver/' + event.id,
		data : '',
		type : "GET",
		success : function(data) {

			var fecha1 = data.fechaInicio;
			var fecha2 = data.fechaFin;
			fecha1 = fecha1.slice(0, 16);
			fecha2 = fecha2.slice(0, 16);
			fecha1 = fecha1.replace("T", " ");
			fecha2 = fecha2.replace("T", " ");

			var tipoEvento = enumATexto(data.tipoEvento);

			var string = "";

			string += '<div class="label100">' + '<label for="">Datos de contacto</label>'
					+ '<p>'
					+ data.titulo
					+ '</p>'
					+ '</div>'
					+ '<label for="">Tipo de Evento</label>'
					+ '<p>'
					+ tipoEvento
					+ '</p>'
					+ '<div class="label50 mlright">'
					+ '<label for="">Fecha inicio</label>'
					+ '<p>'
					+ fecha1
					+ '</p>'
					+ '</div>'
					+ '<div class="label50 mlright">'
					+ '<label for="">Fecha fin</label>'
					+ '<p>'
					+ fecha2
					+ '</p>'
					+ '</div>'
					+ '<input type="hidden" id="idEvento" value='
					+ data.id
					+ '>';

			$('#infoEvento').html(string);

			if (tipoEvento == "Turno") {
				$("#editarBoton").hide();
			} else {
				$("#editarBoton").show();
			}

			window.location.href = "#ver";

		},
		error : function(data) {

		}

	});
}

function cargar(id1) {
	$.ajax({
		url : '/api/calendario/ver2/' + id1,
		data : '',
		type : "GET",
		success : function(data) {

			var fecha1 = data.fechaInicio;
			var fecha2 = data.fechaFin;
			fecha1 = fecha1.slice(0, 16);
			fecha2 = fecha2.slice(0, 16);
			fecha1 = fecha1.replace("T", " ");
			fecha2 = fecha2.replace("T", " ");

			var tipoEvento = enumATexto(data.tipoEvento);

			var string = "";

			string += '<div class="label100">' + '<label for="">Título</label>'
					+ '<p>'
					+ data.titulo
					+ '</p>'
					+ '</div>'
					+ '<label for="">Tipo de Evento</label>'
					+ '<p>'
					+ tipoEvento
					+ '</p>'
					+ '<div class="label50 mlright">'
					+ '<label for="">Fecha inicio</label>'
					+ '<p>'
					+ fecha1
					+ '</p>'
					+ '</div>'
					+ '<div class="label50 mlright">'
					+ '<label for="">Fecha fin</label>'
					+ '<p>'
					+ fecha2
					+ '</p>'
					+ '</div>'
					+ '<input type="hidden" id="idEvento" value='
					+ data.id
					+ '>';

			$('#infoEvento').html(string);

			if (tipoEvento == "Turno") {
				$("#editarBoton").hide();
			} else {
				$("#editarBoton").show();
			}

			window.location.href = "#ver";

		},
		error : function(data) {

		}

	});
	
}

function editarEvento(idEvento) {

	if (idEvento == null) {
		var idEvento = $("#idEvento").val();
	}
	
	$.ajax({
				url : '/api/calendario/ver/' + idEvento,
				data : '',
				type : "GET",
				success : function(data) {
										
					var fecha1 = data.fechaInicio;
					var fecha2 = data.fechaFin;
					fecha1 = fecha1.slice(0, 16);
					fecha2 = fecha2.slice(0, 16);
					
					$("#idEventoEditar").val(data.id);

					var tipoEvento = enumATexto(data.tipoEvento);

					if (tipoEvento == "Plazo") {
						var tipoPlazo = enumATexto(data.tipoPlazo);

						if (tipoPlazo == "Corrido") {
							$("#tipoDiaNoLaborableShoweEditar").hide();
						} else {
							var tipoDia = enumATexto(data.tipoDia);
							$("#tipoDiaNoLaborableShoweEditar").show();
						}

						$("#tituloEditar").val(data.titulo);
						$("#tipoEventoEditar").val(data.tipoEvento);
						$("#selectEventoEditar").val(tipoEvento);
						$("#fechaInicioEditar").val(fecha1);
						$("#tipoPlazoEditar").val(data.tipoPlazo);
						$("#editarTipoPlazo").val(tipoPlazo);
						$("#editarCantDias").val(data.cantDias);
						$("#tipoDiaNoLaborableEditar").val(data.tipoDia);
						$("#editarTipoDia").val(tipoDia);

						$("#asdf").remove();
						$("#fechaFinalDiv").hide();
						$("#inputEventoEditar").hide();
						$("#tipoPlazoShowEditar").show();
						$("#selectEventoEditar").show();
						$("#tipoEventoEditar").show();
						$("#cantDiasEditar").show();

					} else if (tipoEvento == "Otro") {
						$("#tituloEditar").val(data.titulo);
						$("#fechaInicioEditar").val(fecha1);
						$("#fechaFinalEditar").val(fecha2);

						var input = '<input id="asdf" name="tipoEvento" placeholder="Escriba aqui el tipo de evento" type="text" value="'
								+ data.tipoEvento + '">'
						var inputEvento = $("#inputEventoEditar");
						inputEvento.append(input);

						$("#inputEventoEditar").show();
						$("#fechaFinalDiv").show();
						$("#selectEventoEditar").hide();
						$("#tipoEventoEditar").hide();
						$("#tipoPlazoShowEditar").hide();
						$("#tipoDiaNoLaborableShoweEditar").hide();
						$("#cantDiasEditar").hide();

					} else {
						$("#asdf").remove();
						$("#tituloEditar").val(data.titulo);
						$("#tipoEventoEditar").val(data.tipoEvento);
						$("#selectEventoEditar").val(tipoEvento);
						$("#fechaInicioEditar").val(fecha1);
						$("#fechaFinalEditar").val(fecha2);
						$("#fechaFinalDiv").show();
						$("#selectEventoEditar").show();
						$("#tipoEventoEditar").show();
						$("#inputEventoEditar").hide();
						$("#tipoPlazoShowEditar").hide();
						$("#tipoDiaNoLaborableShoweEditar").hide();
						$("#cantDiasEditar").hide();
					}

					window.location.href = "#editar";

				},
				error : function(data) {

				}

			});
}

function guardarEvento() {
	var tituloEvento = $("#tituloEvento").val();
	var tipoEvento = $("#tipoEvento").val();
	var fechaInicioAgregar = $("#fechaInicioAgregar").val();
	var fechaFinAgregar = $("#fechaFinAgregar").val();
	
	if(tituloEvento.length == 0){
		alert("Debe ingresar el titulo del evento");
		return 0;
	}
	
	if(tipoEvento.length == 0){
		alert("Debe ingresar el tipo de evento");
		return 0;
	}
	
	if(fechaFinAgregar == null || tipoEvento == "Plazo"){
		alert("Debe ingresar la fecha de inicio");
		return 0;
	}
	
	if(fechaInicioAgregar == null){
		alert("Debe ingresar la fecha de inicio");
		return 0;
	}
	
	$.ajax({
				url : '/api/calendario/crear',
				data : $("#modalEvento").serialize(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible" id"mje">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'El evento fue creado con éxito'
											+ '</div>').show();
					// refrescar();
					$("#modalCalendario").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location = '/calendario/ver';

				},
				error : function(data) {
					console.log(data);
					$("#errorAgregarEvento").empty().html('<div class="alert alert-danger" id"mje">'
												 + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
												 + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
												 + data.responseJSON.message
												 + '</div>').show();
					setTimeout(function() {
						$("#errormodal4").hide();
					}, 5000);
					$("#errormodal5").empty().html('<div class="alert alert-danger" id"mje">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#errormodal5").hide();

					}, 5000);
				}
			});
}

function openModalEventos(){
	hiddenMjes();
	document.getElementById("tituloEvento").value = "";
	document.getElementById("selectEvento").value = "";
	document.getElementById("fechaInicioAgregar").value = "";
	document.getElementById("fechaFinAgregar").value = "";
	window.location.href = "#agregar";
}

function hiddenMjes() {
	console.log("HiddenMjes...");	
	$("#errorAgregarEvento").hide();
}

function guardarEditarEvento() {
	$
			.ajax({
				url : '/api/calendario/crear',
				data : $("#modalEditarEvento").serialize(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'El evento fue creado con éxito'
											+ '</div>').show();
					// refrescar();
					$("#modalCalendario").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					location.reload();

//					window.location = '/calendario/ver';

				},
				error : function(data) {
					$("#errormodal4")
							.empty()
							.html(
									'<div class="alert alert-danger">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ data.responseJSON.message
											+ '</div>').show();

					setTimeout(function() {
						$("#errormodal3").hide(500);
					}, 5000);
				}
			});
}

function eliminarEventoTurno() {

	$.ajax({
		url : '/api/calendario/ver/' + $("#idEvento").val(),
		data : '',
		type : "GET",
		success : function(data) {

			if (data.tipoEvento != "TURNO") {
				window.location.href = "#eliminar";

			} else {

				$.ajax({
					url : '/api/calendario/qturno/' + $("#idEvento").val(),
					data : '',
					type : "GET",
					success : function(data) {
						$("#eliminarEvento").attr(
								'href',
								'/cancelar-turno-abogado?tokenCancelado='
										+ data.tokenCancelado);

						window.location.href = "#eliminarTurno";

					},
					error : function(data) {

					}

				});

			}

		},
		error : function(data) {

		}

	});
}

function eliminarEvento(idEvento) {
	
	 		console.log(idEvento + " antes del if");
			
	 		if (idEvento == null) {
				console.log("idEvento es null");
				var idEvento = $("#idEvento").val();
			}
			
			console.log(idEvento + " despues del if");
			
			$.ajax({
				url : '/api/calendario/eliminar/' + idEvento,
				data : '',
				type : "POST",
				success : function(data) {

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'El evento fue eliminado con éxito'
											+ '</div>').show(100);
					// refrescar();
					$("#modalCalendario").modal("hide");
					setTimeout(function() {
						$("#alerta").hide(100);
					}, 5000);

					window.location.href = "/calendario/ver";

				},
				error : function(data) {
					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al eliminar el evento. '
											+ data.responseJSON.message
											+ '</div>').show(100);
					$("#modalConfirmar").modal("hide");
					setTimeout(function() {
						$("#alerta").hide(100);
					}, 5000);

				}

			});

}

function render() {
	var moment = $('#calendar').fullCalendar('getDate');

	var mes = moment.format('M');
	if (mes < 10) {
		mes = '0' + mes;
	}

	var anio = moment.format('Y');

	if (mesG != mes || anioG != anio) {
		mesG = mes;
		anioG = anio;
		// refrescar();
	}
}

function convertirStringToEnumTE(string) {
	var textoEnum;

	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}

	switch (textoEnum) {
	case "OTRO":
		$("#selectEvento").hide();
		$("#tipoEvento").remove();
		var input = '<input name="tipoEvento" placeholder="Escriba aqui el tipo de evento" type="text">'
		var inputEvento = $("#inputEvento");
		inputEvento.append(input);
		break;
	case "PLAZO":
		$("#fechaFinal").hide();
		$("#cantDias").show();
		$("#tipoPlazoShow").show();
		break;

	case "FERIADO":
		$("#cantDias").hide();
		$("#tipoPlazoShow").hide();
		$("#tipoDiaNoLaborableShow").show();
		$("#fechaFinal").show();
		break;

	case "FERIA_JUDICIAL":
		$("#cantDias").hide();
		$("#tipoPlazoShow").hide();
		$("#tipoDiaNoLaborableShow").show();
		$("#fechaFinal").show();
		break;
	case "DIA_INHABIL":
		$("#cantDias").hide();
		$("#tipoPlazoShow").hide();
		$("#tipoDiaNoLaborableShow").show();
		$("#fechaFinal").show();
		break;
	default:
		$("#tipoPlazo").val("");
		$("#tipoDiaNoLaborable").val("");
		$("#cantDias").hide();
		$("#tipoPlazoShow").hide();
		$("#tipoDiaNoLaborableShow").hide();
		$("#fechaFinal").show();
		break;
	}

	$("#tipoEvento").val(textoEnum);

}

function convertirStringToEnumTE2(string) {
	var textoEnum;

	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}

	if (textoEnum == "OTRO") {
		$("#selectEventoEditar").hide();
		$("#tipoEventoEditar").remove();
		var input = '<input name="tipoEvento" placeholder="Escriba aqui el tipo de evento" type="text">'
		var inputEvento = $("#inputEventoEditar");
		inputEvento.append(input);
	}

	if (textoEnum == "PLAZO") {
		$("#fechaFinalEditar").hide();
		$("#cantDiasEditar").show();
	} else {
		$("#cantDiasEditar").hide();
		$("#fechaFinalEditar").show();
	}

	$("#tipoEventoEditar").val(textoEnum);

}

function convertirStringToEnumTE3(string) {
	var textoEnum;

	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}

	if (textoEnum == "DIAS_HABILES") {
		$("#tipoDiaNoLaborableShow").show();
	} else if (textoEnum == "CORRIDO") {
		$("#tipoDiaNoLaborableShow").hide();
	}

	console.log(textoEnum);

	$("#tipoPlazo").val(textoEnum);
	$("#tipoPlazoEditar").val(textoEnum);

}

function convertirStringToEnumTE4(string) {
	var textoEnum;

	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}

	if (textoEnum == "DIAS_HABILES") {
		$("#tipoDiaNoLaborableShow").show();
	} else if (textoEnum == "CORRIDO") {
		$("#tipoDiaNoLaborableShow").hide();
	}

	console.log(textoEnum);
	
	$("#tipoDiaNoLaborableEditar").val(textoEnum);
	$("#tipoDiaNoLaborable").val(textoEnum);

}

function enumATexto(string) {
	
	var tipoEvento = string.replace("_", " ");

	if (tipoEvento.includes(" ")) {
		tipoEvento = tipoEvento.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
		return tipoEvento;

	} else {
		tipoEvento = tipoEvento.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
		return tipoEvento;
	}
}