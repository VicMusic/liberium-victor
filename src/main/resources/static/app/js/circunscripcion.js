$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarCircunscripcion() {
	$.ajax({
		url: '/api/circunscripcion/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La circunscripción fue creada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear la circunscripción. ' + data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarCircunscripcion() {
	$.ajax({
		url: '/api/circunscripcion/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#editar").hide();
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al editar la circunscripción. ' + data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function modificarCircunscripcion(componente) {
	/*
	console.log("ModalID: " + componente.getAttribute('objeto') + " - Nombre: " + componente.getAttribute('nombre') + " - ProvinciaID: " + componente.getAttribute('provinciaID') + " - Provincia: " + componente.getAttribute('provincia') + " - Localidades: " + componente.getAttribute('localidades'));
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalProvinciaEditar").val(componente.getAttribute('provincia'));
	buscarLocalidad(componente.getAttribute('provinciaID'));
	$("#modalLocalidadesEditar").val(componente.getAttribute('localidades'));
	*/

	$.ajax({
		url: '/api/circunscripcion/buscar?id=' + componente.getAttribute('objeto'),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#modalEditarId").val(data.id);
			$("#modalEditarNombre").val(data.nombre);
			$("#modalProvinciaEditar").append('<option selected value="' + data.mProvincia.id + '">' + data.mProvincia.nombre + '</option>');
			buscarLocalidad(data.mProvincia.id);
			var list = data.mLocalidades;
			$.each(list, function(index, item) {
				$("#modalLocalidadesEditar").append('<option selected value="' + item.id + '">' + item.nombre + '</option>');
			});
			window.location = '/administracion/circunscripcion/listado#editar';
		},
		error: function(data) {
			console.log("Error: " + JSON.stringify(data));
		}
	});
}

function eliminarCircunscripcion(componente) {
	$("#modalCircunscripcion").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/circunscripcion/eliminar?id='
			+ $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La circunscripción fue eliminada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar la circunscripción. ' + data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

/*
function buscarLocalidad(idProvincia, idLocalidades) {
	console.log(idProvincia)
	console.log(idLocalidades)
	$.ajax({
		url: '/api/usuario/combo1/' + idProvincia,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var selectList = "<option disabled >Seleccione una localidad</option>";
			for (var x = 0; x < data.length; x++) {
				selectList += '<option value="' + data[x].id + '">'
					+ data[x].nombre + "</option>";
			}
			$('#localidades').html(selectList);
			$('#modalEditarLocalidad').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}
*/

function buscarLocalidad(id) {
	$.ajax({
		url: '/api/usuario/combo2/' + id,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			for (var x = 0; x < data.length; x++) {
				$("#localidad").append('<option value="' + data[x].id + '">' + data[x].nombre + '</option>');
				$("#modalLocalidadesEditar").append('<option value="' + data[x].id + '">' + data[x].nombre + '</option>');
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
}