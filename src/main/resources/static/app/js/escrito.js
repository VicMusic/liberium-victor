$(document).ready(function() {
	var id1 = $("#id1").val();
	if(id1 != null && id1 != ''){
		cargar(id1);
	}
});	
	function remplazarVariables(){
	if ($("#escritoLiberium").val() == "SI") {
		$.ajax({
			url: '/api/escrito/variables?idCausa=' + $("#idCausa").val() + "&idEscrito=" + $("#idEscrito").val() + "&idItem=" + $("#idItem").val(),
			dataType: "json",
			type: "GET",
			success: function (data){
				console.log(data);
				agregarEscrito(true);
				mostrarNuevasVariables(data.causa.mTipoCausa.id);
				var textoEscrito = data.textoEscrito;
				
				for (var i = 0; i < data.respuestas.length; i++) {
					var variable = data.respuestas[i].pregunta.variableValue;
					var respuesta = data.respuestas[i].respuesta;
					if (textoEscrito.includes(variable)) {
						textoEscrito = textoEscrito.replace(new RegExp(variable, "g"), respuesta);
					}
				}

				if (textoEscrito.includes("@NombreActor")) {
					textoEscrito = textoEscrito.replace(/@NombreActor/g, data.causa.actor);
				}
				if (textoEscrito.includes("@TipoCausa")) {
					textoEscrito = textoEscrito.replace(/@TipoCausa/g, data.causa.mTipoCausa.nombre);
				}
				if (textoEscrito.includes("@NombreDemandado")) {
					textoEscrito = textoEscrito.replace(/@NombreDemandado/g, data.causa.demandado);
				}
				if (textoEscrito.includes("@Fuero")) {
					textoEscrito = textoEscrito.replace(/@Fuero/g, data.causa.fuero);
				}
				if (textoEscrito.includes("@Juzgado")) {
					textoEscrito = textoEscrito.replace(/@Juzgado/g, data.causa.juzgadoOficina.nombre);
				}
				if (textoEscrito.includes("@Instancia")) {
					textoEscrito = textoEscrito.replace(/@Instancia/g, data.causa.mInstancia.nombre);
				}
				if (textoEscrito.includes("@NombreCaratula")) {
					textoEscrito = textoEscrito.replace(/@NombreCaratula/g, data.causa.nombreCaratula);
				}
				if (textoEscrito.includes("@NumExpediente")) {
					textoEscrito = textoEscrito.replace(/@NumExpediente/g, data.causa.numeroExpediente);
				}
				if (textoEscrito.includes("@NombreAbogado")) {
					textoEscrito = textoEscrito.replace(/@NombreAbogado/g, data.abogado.mUsuario.nombre);
				}
				if (textoEscrito.includes("@MatriculaAbogado")) {
					textoEscrito = textoEscrito.replace(/@MatriculaAbogado/g, data.abogado.matricula);
				}
				if (textoEscrito.includes("@DomicilioAbogado")) {
					textoEscrito = textoEscrito.replace(/@DomicilioAbogado/g, data.abogado.domicilio);
				}
				CKEDITOR.instances.editorTexto.setData(textoEscrito);
				$("#titulo").val(data.tituloEscrito);
			},
			error: function (data){
				console.log(data);
			}
		});
	}
}

function guardarEscrito(){
	$("#texto").val(CKEDITOR.instances.editorTexto.getData());
	$.ajax({
		url: '/api/escrito/guardar',
		dataType: "json",
		data: $("#formEscrito").serialize(),
		type: "POST",
		success: function (data){
			$("#alertaForm").empty().html('<div class="alert alert-info alert-dismissible">'
	                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
	                + '<h3><i class="icon fa fa-info"></i> Info!</h3>' + 'El escrito fue guardado con exito.' + '</div>').show();
			
			if($("#escritoLiberium").val() == 'SI'){
			guardarEnItem($("#idItem").val(), $("#idEscrito").val(), data.id);
			setTimeout(function () {
		    $("#alertaForm").hide();
			window.location.href = "/administracion/causa/editar/" + $("#idCausa").val();
			}, 3000);
			}else{
				setTimeout(function () {
	    			$("#alertaForm").hide();
	    			window.location = "/administracion/escrito/listado";
	    	    }, 3000);
			}
			
		},
		error: function (data){
			$("#alertaForm").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
	                + 'Ha ocurrido un error al crear el escrito. ' + data.responseJSON.message
	                + '</div>').show();
 			setTimeout(function () {
    			$("#alertaForm").hide();
    	    }, 3000);
		}
	});
}

function hacerPropioEscrito(){
	$("#texto").val(CKEDITOR.instances.editorTexto.getData());
	var id1 = $("#id1").val();
	var id2 = $("#id").val();
	if(id1 == null || id1 === ""){
		id1 = id2;
	}
	var id3 = $("#idItem").val();
	console.log(id3);
	$.ajax({
		url: '/api/escrito/hacerPropio/' + id1 + "/" + id3,
		type: "POST",
		success: function (data){
			$("#alertaForm").empty().html('<div class="alert alert-info alert-dismissible">'
	                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
	                + '<h4><i class="icon fa fa-info"></i> Info!</h4>' + 'El escrito fue generado con éxito.' + '</div>').show();
			
			if($("#escritoLiberium").val() == 'SI'){
			guardarEnItem($("#idItem").val(), $("#idEscrito").val(), data.id);
			setTimeout(function () {
		    $("#alertaForm").hide();
			window.location.href = "/administracion/causa/editar/" + $("#idCausa").val();
			}, 3000);
			}else{
				setTimeout(function () {
	    			$("#alertaForm").hide();
	    			window.location = "/administracion/escrito/listado";
	    	    }, 3000);
			}
			
		},
		error: function (data){
			$("#alertaForm").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
	                + 'Ha ocurrido un error al generar el escrito. ' + data.responseJSON.message
	                + '</div>').show();
 			setTimeout(function () {
    			$("#alertaForm").hide();
    	    }, 3000);
		}
	});
}

function guardarEnItem(idItem, idEscritoUsado, idEscritoNuevo){
	$.ajax({
		url: '/api/escrito/guardar/item?idItemSubItem=' +  idItem + '&idEscritoUsado=' + idEscritoUsado + '&idEscritoNuevo=' + idEscritoNuevo,
		dataType: "json",
		type: "POST"
	});
}

function eliminarEscritoVal(idEscrito, val){
	$("#modalEliminarEscritoId").val(idEscrito);
}

function eliminarEscrito(){
	$.ajax({
		url: '/api/escrito/eliminar',
		data: $("#modalFormularioEliminar").serialize(),
		dataType: "json",
		type: "POST",
		success: function (data){
			window.location = "/administracion/escrito/listado#";
			$("#alertaEscritos").empty().html('<div class="alert alert-info alert-dismissible">'
	                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
	                + '<h3><i class="icon fa fa-info"></i> Info!</h3>'
	                + 'El escrito fue eliminado con éxito'
	                + '</div>').show();
 			setTimeout(function () {
    			$("#alertaEscritos").hide();
    			window.location = "/administracion/escrito/listado";
    	    }, 3000);
		},
		error: function (data){
			window.location = "/administracion/escrito/listado#";
			$("#alertaEscritos").empty().html('<div class="alert alert-danger alert-dismissible">'
	                + '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
	                + '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
	                + 'Ha ocurrido un error al eliminar el escrito. ' + data.responseJSON.message
	                + '</div>').show();
 			setTimeout(function () {
    			$("#alertaEscritos").hide();
    	    }, 3000);
		}
	});
}

function cargar(id1) {
	$.ajax({
		url: '/api/escrito/id/' + id1,
		type: "GET",
		success: function (data){
			$("#escritosLiberium").hide();
			$("#escritosLocales").hide();
			$("#escritosPropuestos").hide();
			$("#filtros").hide();
			$("#editor").show();
			$("#original").val(data.original);
			$("#titulo").val(data.titulo);
			$("#idTipoCausa").val(data.mTipoCausa.id);
			$("#idTipoCausa").change();
			$("#id").val(data.id);
			CKEDITOR.instances.editorTexto.setData(data.texto);
			$("#titulo").prop("disabled", true);
			$("#botonGuardar").hide();
			$("#botonHacerPropio").prop("disabled", false);
			CKEDITOR.instances.editorTexto.setReadOnly(true);
		}
	});
}

function cargarEscrito(componente) {
	var id1 = componente.getAttribute('th:objeto');
	cargar(id1);
}

function traerEscrito(id, tipo){
	$.ajax({
		url: '/api/escrito/id/' + id,
		type: "GET",
		success: function (data){
			$("#escritosLiberium").hide();
			$("#escritosLocales").hide();
			$("#escritosPropuestos").hide();
			$("#filtros").hide();
			$("#editor").show();
			$("#original").val(data.original);
			$("#titulo").val(data.titulo);
			$("#idTipoCausa").val(data.mTipoCausa.id);
			$("#idTipoCausa").change();
			CKEDITOR.instances.editorTexto.setData(data.texto);
			if (tipo == "VER") {
				$("#titulo").prop("disabled", true);
				$("#botonHacerPropio").prop("disabled", false);
				$("#botonGuardar").hide();
				CKEDITOR.instances.editorTexto.setReadOnly(true);
			} else {
				$("#titulo").prop("disabled", false);
				$("#botonGuardar").prop("disabled", false);
				$("#botonHacerPropio").hide();
				CKEDITOR.instances.editorTexto.setReadOnly(false);
			}
			
			if (tipo == "CLONAR"){
				$("#id").val("");
			} else {
				$("#id").val(data.id);
			}
			
			if (tipo === 'USAR_LIBERIUM' || tipo === 'USAR' || tipo === 'CLONAR_LIBERIUM') {
				$("#id").val("");
				$("#original").val(false);
			}
		}
	});
}

function cerrarEscrito(){
	$("#editor").hide();
	$("#id").val("");
	$("#original").val("");
	$("#titulo").val("");
	CKEDITOR.instances.editorTexto.setData("");
	switch ($("#selectEscrito").val()) {
		case "PRECARGADOS":
			$("#escritosLiberium").show();
			break;
		case "LOCALES":
			$("#escritosLocales").show();
			break;
		case "PROPUESTOS":
			$("#escritosPropuestos").show();
			break;
	}
	$("#filtros").show();
}

function mostrarNuevasVariables(idTipoCausa){
	$("#variablesExtras").empty();
	$.ajax({
		url: '/api/escrito/buscarvariables/' + idTipoCausa,
		type: "GET",
		success: function (data){
		
			var coloresBoton = [
			    'btn btn-primary',
			    'btn btn-secondary',
			    'btn btn-success',
			    'btn btn-danger',
			    'btn btn-warning',
			    'btn btn-info'
			];
			var string = "";
			for (var i = 0; i < data.length; i++) {
				if (data[i].variableAmigable != null && data[i].variableValue != null) {
					var randomNumber = Math.floor(Math.random()*coloresBoton.length);
					string += '<div class="draggable" draggable="true">'
							+ '<button class="'+ coloresBoton[randomNumber] +'" style="margin: 2px; width: 100%;" value="'+ data[i].variableValue +'">'+ data[i].variableAmigable +'</button>'
							+ '</div>';
				}
			}
			$("#variablesExtras").append(string);
			initDragAndDrop();
		},
		error: function (data){
			console.log(data);
		}
	});
}

function mostrarBloque(val){
	switch (val) {
		case "PRECARGADOS":
			$("#escritosLiberium").show();
			$("#escritosLocales").hide();
			$("#escritosPropuestos").hide();
			break;
		case "LOCALES":
			$("#escritosLocales").show();
			$("#escritosLiberium").hide();
			$("#escritosPropuestos").hide();
			break;
		case "PROPUESTOS":
			$("#escritosLocales").hide();
			$("#escritosLiberium").hide();
			$("#escritosPropuestos").show();
			break;
	}
	$("#editor").hide();
}

function agregarEscrito(isOriginal){
	$("#escritosLiberium").hide();
	$("#escritosLocales").hide();
	$("#escritosPropuestos").hide();
	$("#editor").show();
	$("#original").val(isOriginal);
}

function descargar(idEscrito, url) {
	window.open("/download/" + url + "/" + idEscrito);
}

function resaltarVariable(variable){
	var texto = CKEDITOR.instances.editorTexto.getData();
	var highlighted = '&nbsp;<span class="marker">'+ variable +'</span>&nbsp;';
	texto = texto.replace(new RegExp(variable, "g"), highlighted);
	CKEDITOR.instances.editorTexto.setData(texto);
}

initDragAndDrop();

function initDragAndDrop() {
    // Collect all draggable elements and drop zones
    let draggables = document.querySelectorAll(".draggable");
    let dropZones = document.querySelectorAll(".drop-zone");
    initDraggables(draggables);
    initDropZones(dropZones);
}

function initDraggables(draggables) {
    for (const draggable of draggables) {
        initDraggable(draggable);
    }
}

function initDropZones(dropZones) {
    for (let dropZone of dropZones) {
        initDropZone(dropZone);
    }
}

/**
 * Set all event listeners for draggable element
 * https://developer.mozilla.org/en-US/docs/Web/API/DragEvent#Event_types
 */
function initDraggable(draggable) {
    draggable.addEventListener("dragstart", dragStartHandler);
    draggable.addEventListener("drag", dragHandler);
    draggable.addEventListener("dragend", dragEndHandler);

    // set draggable elements to draggable
    draggable.setAttribute("draggable", "true");
}

/**
 * Set all event listeners for drop zone
 * https://developer.mozilla.org/en-US/docs/Web/API/DragEvent#Event_types
 */
function initDropZone(dropZone) {
    dropZone.addEventListener("dragenter", dropZoneEnterHandler);
    dropZone.addEventListener("dragover", dropZoneOverHandler);
    dropZone.addEventListener("dragleave", dropZoneLeaveHandler);
    dropZone.addEventListener("drop", dropZoneDropHandler);
}

/**
 * Start of drag operation, highlight drop zones and mark dragged element
 * The drag feedback image will be generated after this function
 * https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/Drag_operations#dragfeedback
 */
function dragStartHandler(e) {
//	debugger;
    setDropZonesHighlight();
    this.classList.add('dragged', 'drag-feedback');
    // we use these data during the drag operation to dicide
    // if we handle this drag event or not
    e.dataTransfer.setData("type/dragged-box", 'dragged');
    e.dataTransfer.setData("text/plain", this.firstElementChild.value);
    deferredOriginChanges(this, 'drag-feedback');
}

/**
 * While dragging is active we can do something
 */
function dragHandler() {
    // do something... if you want
}

/**
 * Very last step of the drag operation, remove all added highlights and others
 */
function dragEndHandler() {
    setDropZonesHighlight(false);
    this.classList.remove('dragged');
    resaltarVariable(this.firstElementChild.value);
}

/**
 * When entering a drop zone check if it should be allowed to
 * drop an element here and highlight the zone if needed
 */
function dropZoneEnterHandler(e) {
    // we can only check the data transfer type, not the value for security reasons
    // https://www.w3.org/TR/html51/editing.html#drag-data-store-mode
    if (e.dataTransfer.types.includes('type/dragged-box')) {
        this.classList.add("over-zone");
        // The default action of this event is to set the dropEffect to "none" this way
        // the drag operation would be disallowed here we need to prevent that
        // if we want to allow the dragged element to be drop here
        // https://developer.mozilla.org/en-US/docs/Web/API/Document/dragenter_event
        // https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer/dropEffect
        e.preventDefault();
    }
}

/**
 * When moving inside a drop zone we can check if it should be
 * still allowed to drop an element here
 */
function dropZoneOverHandler(e) {
    if (e.dataTransfer.types.includes('type/dragged-box')) {
        // The default action is similar as above, we need to prevent it
        e.preventDefault();
    }
}

/**
 * When we leave a drop zone we check if we should remove the highlight
 */
function dropZoneLeaveHandler(e) {
    if (e.dataTransfer.types.includes('type/dragged-box') &&
        e.relatedTarget !== null &&
        e.currentTarget !== e.relatedTarget.closest('.drop-zone')) {
        // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/relatedTarget
        this.classList.remove("over-zone");
    }
}

/**
 * On successful drop event, move the element
 */
function dropZoneDropHandler(e) {
    // We have checked in the "dragover" handler (dropZoneOverHandler) if it is allowed
    // to drop here, so it should be ok to move the element without further checks
    let draggedElement = document.querySelector('.dragged');
    e.currentTarget.appendChild(draggedElement);
    // We  drop default action (eg. move selected text)
    // default actions detailed here:
    // https://www.w3.org/TR/html51/editing.html#drag-and-drop-processing-model
    e.preventDefault();

}


/**
 * Highlight all drop zones or remove highlight
 */
function setDropZonesHighlight(highlight = true) {
    const dropZones = document.querySelectorAll(".drop-zone");
    for (const dropZone of dropZones) {
        if (highlight) {
            dropZone.classList.add("active-zone");
        } else {
            dropZone.classList.remove("active-zone");
            dropZone.classList.remove("over-zone");
        }
    }
}

/**
 * After the drag feedback image has been generated we can remove the class we added
 * for the image generation and/or change the originally dragged element
 * https://javascript.info/settimeout-setinterval#zero-delay-settimeout
 */
function deferredOriginChanges(origin, dragFeedbackClassName) {
    setTimeout(() => {
        origin.classList.remove(dragFeedbackClassName);
    });
}

