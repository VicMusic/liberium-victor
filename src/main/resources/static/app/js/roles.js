
$( document ).ready(function() {
	var resultado = $("#resultado").val();
	if(resultado){
		$("#alerta").html(
				'<div class="alert alert-info alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
				+ 'No se encontraron resultados con la búsqueda realizada. '
				+ '</div>').show();
	}
});

$(function() {
	$('#seleccionarTodos').change(function() {
	  var checkboxes = $(this).closest('form').find('.permisos').not($(this));
	  checkboxes.prop('checked', $(this).is(':checked'));
	  
	  if($(this).is(':checked')){
		  $("#textoSeleccionar").empty().html("Descartar Todos");
	  }else{
		  $("#textoSeleccionar").empty().html("Seleccionar Todos");
	  }
	  
	});
});

function editar(componente) {
	resetFormulario();
	$("#id").val(componente.getAttribute('objeto'));
	$("#nombre").val(componente.getAttribute('nombre'));
	if(componente.getAttribute('isAbogado') == "true"){
		$("#isAbogado").attr("checked", true);
	}
	
	var permisos = JSON.parse(componente.getAttribute('permisosUsuario'));
	for (var i = 0; i < permisos.length; i++) {
		$("#" + permisos[i].id).attr("checked", true);
	}
	
	$("#agregar").show();
}

function visualizar(componente){
	editar(componente);
	$("#modalFormulario *").prop('disabled', true);
}

function resetFormulario(){
	$("#modalFormulario *").prop('disabled', false);
	$("#modalFormulario").trigger("reset");
	$("#id").val("");
	$('input:checkbox').removeAttr('checked');
	$("#textoSeleccionar").empty().html("Seleccionar Todos");
}

function guardar() {
	var permisosChecked = "";
	jQuery("input[name='permisos']:checked").each(function() {
		var id = (this.id);
		permisosChecked +=  id + ";";
	});
	
	permisosChecked = permisosChecked.substring(0,(permisosChecked.lastIndexOf(";")));
	var formulario = $("#modalFormulario").serialize() + "&permisosChecked=" + permisosChecked;
	$.ajax({
		url : '/api/roles/guardar',
		data : formulario,
		dataType : "json",
		type : "POST",
		success : function(data) {
			$("#agregar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El rol fue creado con éxito.'
				+ '</div>').show();

			setTimeout(function() {
				window.location = '/administracion/roles/listado';
			}, 4000);
		},
		error : function(data) {
			$("#modalAlerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el rol. '
				+ data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}


function eliminarRol(componente) {
	$("#eliminar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url : '/api/roles/eliminar?id=' + $("#modalEliminarId").val(),
		dataType : "json",
		type : "POST",
		async : true,
		success : function(data) {
			$("#eliminar").modal('hide');
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
						+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
						+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
						+ 'El rol fue eliminado con éxito'
						+ '</div>').show();
			
			setTimeout(function() {
				window.location = '/administracion/roles/listado';
			}, 1000);
		},
		error : function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
						+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
						+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
						+ 'Ha ocurrido un error al eliminar el rol. '
						+ data.responseJSON.message
						+ '</div>').show();
			$("#eliminar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}