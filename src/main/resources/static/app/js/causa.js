var idCausa = $("#idCausa").val();
const formulario = document.getElementById('guardarCausa');
const inputs = document.querySelectorAll('#guardarCausa input');

function cambiarPag(id) {
	window.location.href = '/administracion/causa/editar/' + id;
}

function modalEliminarMiembro(componente) {
	$("#miembroId").val(componente.getAttribute('idMiembro'));
}

function miembroEliminar() {
	$.ajax({
		url: '/api/miembro/eliminarmiembrocausa?idCausa=' + idCausa + '&idMiembros=' + $("#miembroId").val(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			window.location.href = '/administracion/causa/editar/' + idCausa;
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function guardarMiembroCausa() {
	$.ajax({
		url: '/api/miembro/guardarmiembrocausa?idCausa=' + idCausa +
			'&idEstudio=' + $("#selectIdEstudio").val() + '&idMiembros=' + $("#selectMiembroEstudio").val() + '&nuevo=false',
		dataType: "json",
		type: "POST",
		success: function(data) {
			mjeAlerta("#alertaGuardarMiembroExistente", "Exito!", "Se agregaron los/el miembros correctamente", "show");
			setTimeout(function() {
				mjeAlerta("#alertaGuardarMiembroExistente", "Ocultar", "Ocultar", "hide");
				window.location.href = '/administracion/causa/editar/' + idCausa;
			}, 5000);
		},
		error: function(data) {
			console.log(data)
			mjeAlerta("#alertaGuardarMiembroExistente", "Error!", "Ocurrio un error al guardar los/el miembros", "show");
			setTimeout(function() {
				mjeAlerta("#alertaGuardarMiembroExistente", "Ocultar", "Ocultar", "hide");
			}, 5000);
		}
	});
}

function guardarMiembroNuevo(idMiembro) {
	$.ajax({
		url: '/api/miembro/guardarmiembrocausa?idCausa=' + idCausa +
			'&idEstudio=' + $("#selectIdEstudio").val() + '&idMiembros=' + idMiembro + '&nuevo=true',
		dataType: "json",
		type: "POST",
		success: function(data) {
			mjeAlerta("#alertaGuardarMiembroExistente", "Exito!", "Se agregaron los/el miembros correctamente", "show");
			setTimeout(function() {
				mjeAlerta("#alertaGuardarMiembroExistente", "Ocultar", "Ocultar", "hide");
				window.location.href = '/administracion/causa/editar/' + idCausa;
			}, 4000);
		},
		error: function(data) {
			console.log(data)
			mjeAlerta("#alertaGuardarMiembroExistente", "Error!", "Ocurrio un error al guardar los/el miembros", "show");
			setTimeout(function() {
				mjeAlerta("#alertaGuardarMiembroExistente", "Ocultar", "Ocultar", "hide");
			}, 4000);
		}
	});
}

function crearMiembroNuevo() {
	if ($("#idEmailBuscar").val().length < 2) {
		mjeAlerta("#modalAlertaNuevoMiembro", "Advertencia!", "Debe ingresar un mail de usuario", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoMiembro", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	}
	if ($("#idRolSelect").val() == "defecto" || $("#idRolSelect").val() == null) {
		mjeAlerta("#modalAlertaNuevoMiembro", "Error!", "Por favor seleccione un rol.", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNuevoMiembro", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	}
	$.ajax({
		url: '/api/miembro/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			console.log(data.content);
			guardarMiembroNuevo(data.content);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function traerMiembrosEstudio() {
	$.ajax({
		url: '/api/miembro/miembrosestudio?idEstudio=' + $("#selectIdEstudio").val() + '&idCausa=' + idCausa,
		dataType: "json",
		type: "GET",
		success: function(data) {
			console.log(data);
			$("#selectMiembroEstudio").empty();
			var opciones = "";
			for (let i = 0; i < data.length; i++) {
				//opciones += "<option value=" + data[i].mUsuario.id + ">" + data[i].mUsuario.email + "</option>";
				opciones += "<option value=" + data[i].id + ">" + data[i].mUsuario.email + "</option>";
			}
			$("#selectMiembroEstudio").append(opciones);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function filtrar(filtro) {
	var path = location.pathname;
	if (filtro == "tipo de causa" || filtro == "zona" || filtro == "usuarios") {
		var select = "";
		switch (filtro) {

			case "tipo de causa":
				$.ajax({
					url: "/api/tipocausa/listar",
					type: "GET",
					success: function(data) {
						select += "<option>Seleccione Tipo de Causa</option>";
						for (var i = 0; i < data.length; i++) {
							select += "<option value=" + data[i].id + ">" + data[i].nombre + "</option>";
						}
						$("#segundoFiltro").html(select);
					},
					error: function(data) {
						console.log(data);
					}
				});
				break;
			case "usuarios":
				$.ajax({
					url: "/api/usuario/listar",
					type: "GET",
					success: function(data) {
						select += "<option>Seleccione Usuario</option>";
						for (var i = 0; i < data.length; i++) {
							select += "<option value=" + data[i].id + ">" + data[i].nombre + "</option>";
						}
						$("#segundoFiltro").html(select);
					},
					error: function(data) {
						console.log(data);
					}
				});
				break;
			case "zona":
				$.ajax({
					url: "/api/provincia/listar",
					type: "GET",
					success: function(data) {
						select += "<option>Seleccione Zona</option>";
						for (var i = 0; i < data.length; i++) {
							select += "<option value=" + data[i].id + ">" + data[i].nombre + "</option>";
						}
						$("#segundoFiltro").html(select);
					},
					error: function(data) {
						console.log(data);
					}
				});
				break;
		}
		$("#segundoFiltro").css('display', 'block');
	} else {
		window.location.href = path + "?filtro=" + filtro;
	}
}

function segundoFiltro(clave) {
	var path = location.pathname;
	var filtro = $("#primerFiltro").val()
	window.location.href = path + "?filtro=" + filtro + "&clave=" + clave;
}

$(document).ready(function() {

	//Causa
	//$('#provinciaCausa').prop('disabled', true);
	//Checklist
	camposNuevoChecklist("hide");
	$('#guardarChecklist').prop('hidden', true);

	if (fuero == 'NACIONAL') {
		$("#idProvincia").css('display', 'none');
		$("#juzgado").css('display', 'none');
		$("#instancia").css('display', 'none');
	} else {
		$("#idProvincia").css('display', 'block');
		$("#juzgado").css('display', 'block');
		$("#instancia").css('display', 'block');
	}

	if ($("#nombreCheck") != null && $("#nombreCheck") != undefined && $("#nombreCheck") != '') {
		var checkArray = $("#nombreCheck").text().split("creado");
		var nombre = checkArray[0];
		$("#nombreCheck").text(nombre);
	}
	var fuero = document.getElementById('tipoFuero').value;
	var idTipoCausa = document.getElementById('tipoCausa').value;
	$.ajax({
		url: '/api/checklist/buscar/checklists?idTipoCausa=' + idTipoCausa + '&fuero=' + fuero,
		dataType: 'json',
		type: "GET",
		success: function(data) {
			if (document.getElementById('checklistNombre') != undefined && document.getElementById('checklistNombre') != null && document.getElementById('checklistNombre') != '') {
				checklistNombre = document.getElementById('checklistNombre').value;
			}
			if (document.getElementById('checklistId') != undefined && document.getElementById('checklistId') != null && document.getElementById('checklistId') != '') {
				checklistId = document.getElementById('checklistId').value;
			}
			if (checklistNombre != undefined && checklistNombre != null && checklistNombre != '') {
				var checkArray = checklistNombre.split("creado");
				var nombre = checkArray[0];
				var selectList = "<option selected value='" + checklistId + "'>" + nombre + "</option>";
			} else {
				var selectList = "<option selected disabled value=''>Seleccione Checklist</option>";
			}
			selectList += '<option value="nuevochecklist">Nuevo Checklist</option>';
			for (var x = 0; x < data.length; x++) {
				if (nombre != data[x].nombre) {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
			}
			$('#checklist').html(selectList);
		},
		error: function(data) {
			console.log("error");
		}
	});
});

function finanzasCausa() {
	console.log(idCausa);
	window.location = '/contabilidad-causa/ver/' + idCausa;
}

function fueroNacional(componente) {
	var fuero = componente.value;
	if (fuero == 'NACIONAL') {
		$("#idProvincia").css('display', 'none');
		$("#juzgado").css('display', 'none');
		$("#instancia").css('display', 'none');
	} else {
		$("#idProvincia").css('display', 'block');
		$("#juzgado").css('display', 'block');
		$("#instancia").css('display', 'block');
	}
}

function guardarCausa() {
	//Apellido y Nombre de Actor y Demandado
	if ($("#actorApellido").val().length < 2 || $("#actorNombre").val().length < 2 || $("#demandadoApellido").val().length < 2 || $("#demandadoNombre").val().length < 2) {
		mjeAlerta("#modalAlerta", "Error!", "Debe ingresar el apellido y el nombre del actor y demandado.", "show");
		$("#contenidoModalAgregarCausa").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	//Tipo de Causa
	if ($("#tipoCausa").val() == "defecto" || $("#tipoCausa").val() == null) {
		mjeAlerta("#modalAlerta", "Error!", "Por favor seleccione un tipo de Causa.", "show");
		$("#contenidoModalAgregarCausa").scrollTop(0);
		setTimeout(function() {
			mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
		}, 4000);
		return 0;
	}
	guardar();
}

function guardar() {
	$.ajax({
		url: '/api/causa/guardar',
		data: $("#guardarCausa").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();
			mjeAlerta("#modalAlerta", "Info!", "La causa fue creada con éxito.", "show");
			setTimeout(function() {
				mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
			}, 4000);
			window.location.href = '/administracion/causa/listado';
		},
		error: function(data) {
			console.log("Error al guardar Causa.");
			mjeAlerta("#modalAlerta", "Error!", "Ha ocurrido un error al crear la causa. " + data.responseJSON.message, "show");
			setTimeout(function() {
				mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
			}, 5000);
		}
	});
}

/*--Mensaje Modificar Causa Inicio */
const validarFormulario =(e) => {
	switch (e.target.name) {
        case "actorApellido":
		  document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
          console.log('modificar Apellido del Actor');
            break;
        case "actorNombre":
			document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
            console.log('modificar el Nombre del Actor');
            break;
        case "demandadoApellido":
			document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
            console.log('modificar Apellido del demandado');
            break;
        case "demandadoNombre":
			document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
            console.log('modificar el nombre del Demandado');
            break;
		case "nroExpediente":
			document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
            console.log('modificar el numero de expediente');
            break;
                          }
}

inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
});

function mensajeModificarCausa() {	
	console.log('Se ejecuto');
	document.getElementById('mensaje-modificado').classList.add('mensaje-modificado-activo');
}
/*--Mensaje Modificar Causa Fin */

function eliminarCausa(componente) {
	$("#causaId").val(componente.getAttribute('idCausa'));
}

function causaEliminar() {
	$.ajax({
		url: '/api/causa/eliminar',
		data: $('#eliminarCausaForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			mjeAlerta("#alerta", "Info!", "La causa fue eliminada con éxito", "show");
			setTimeout(function() {
				mjeAlerta("#alerta", "Ocultar!", "Ocultar", "hide");
			}, 5000);
			window.location = "/administracion/causa/listado";
		},
		error: function(data) {
			mjeAlerta("#alerta", "Error!", "Ha ocurrido un error al eliminar la causa. " + data.responseJSON.message + ".", "show");
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				mjeAlerta("#alerta", "Ocultar!", "Ocultar", "hide");
			}, 5000);
		}
	});
}

function causaArchivar(componente) {
	$("#causaIdArchivar").val(componente.getAttribute('objeto'));
}

function archivarCausa() {
	$.ajax({
		url: '/api/causa/archivar',
		data: $('#archivarCausaForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			window.location = "/administracion/causa/listado";
		},
		error: function(data) {
		}
	});
}

function causaActivar(componente) {
	$("#causaIdActivar").val(componente.getAttribute('objeto'));
}

function activarCausa() {
	$.ajax({
		url: '/api/causa/activar',
		data: $('#activarCausaForm').serialize(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			window.location = "/administracion/causa/listado";
		},
		error: function(data) {
		}
	});
}

function getProvincia(idPais) {
	$('#provinciaCausa').prop('disabled', false);
	var select = "";
	$.ajax({
		url: '/api/causa/combo1/' + idPais,
		type: "GET",
		success: function(data) {
			select += "<option selected disabled>Seleccione una provincia</option>";
			for (var i = 0; i < data.length; i++) {
				select += "<option value=" + data[i].id + ">" + data[i].nombre + "</option>";
			}
			$("#provinciaCausa").html(select);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

$("#tipoFuero").on('change', function() {
	$('#instancia').prop('disabled', false);
});

$("#dependencia").on('change', function() {
	$('#tipoCausa').prop('disabled', false);
});

// CHECKLIST
var checklistNombre = null;
var checklistId = null;

$("#checklist").on('change', function() {
	if (document.getElementById('checklistNombre').value != undefined && document.getElementById('checklistNombre').value != null && document.getElementById('checklistNombre').value != '') {
		mjeAlerta("#alertaChecklist", "Advertencia!", "Si cambia de checklist perderá todos los cambios del checklist actual.", "show");
		$('#cancelarChecklist').prop('hidden', false);
		$('#cambiarChecklist').prop('hidden', false);
		$('#guardarChecklist').prop('hidden', true);
	} else {
		if (document.getElementById('checklist').value == 'nuevochecklist') {
			window.location.href = '#agregarNuevoChecklist';
			//camposNuevoChecklist("show");
			$('#containerCard').hide();
			$('#containerSubCard').hide();
			$('#mensajeSeleccionChecklist').show();
		} else {
			//camposNuevoChecklist("hide");
			//$('#cancelarChecklist').prop('hidden', true);
			$('#guardarChecklist').prop('hidden', false);
			//$('#containerCard').hide();
			//$('#containerSubCard').hide();
			//$('#mensajeSeleccionChecklist').show();
		}
		$("#checklistId").val($("#checklist").val());
		$("#checklistNombre").val($("#checklist").text());
	}
});

$("#tipoCausa").on('change', function() {
	$('#rolCausa').prop('disabled', false);
	$('#checklistSelect').prop('disabled', false);
	if (document.getElementById('checklistNombre') != undefined && document.getElementById('checklistNombre') != null && document.getElementById('checklistNombre') != '') {
		mjeAlerta("#alertaChecklist", "Advertencia!", "Si cambia de tipo de causa perderá todos los cambios del checklist actual.", "show");
	}
	var fuero = document.getElementById('tipoFuero').value;
	var idTipoCausa = document.getElementById('tipoCausa').value;
	$.ajax({
		url: '/api/checklist/buscar/checklists?idTipoCausa=' + idTipoCausa + '&fuero=' + fuero,
		dataType: 'json',
		type: "GET",
		success: function(data) {
			var selectList = "<option selected disabled value=''>Seleccione Checklist</option>";
			selectList += '<option value="0">Nuevo Checklist</option>';
			for (var x = 0; x < data.length; x++) {
				selectList += '<option value="' + data[x].id + '">'
					+ data[x].nombre + "</option>";
			}
			$('#checklistSelect').html(selectList);
		},
		error: function(data) {
			console.log("error");
		}
	});
});

$("#tipoFuero").on('change', function() {
	var tipoFuero = document.getElementById("tipoFuero").value;
	var provinciaCausa = document.getElementById("provinciaCausa");
	if(tipoFuero === 'INTERNACIONAL'){
		provinciaCausa.setAttribute('disabled', true);
	}else{
		if(provinciaCausa.hasAttribute('disabled')){
			provinciaCausa.removeAttribute('disabled');
		}
	}

	if (document.getElementById('checklistNombre') != undefined && document.getElementById('checklistNombre') != null && document.getElementById('checklistNombre') != '') {
		mjeAlerta("#alertaChecklist", "Advertencia!", "Si cambia de fuero perderá todos los cambios del checklist actual.", "show");
	}
});

$("#guardarChecklist").on('click', function() {
	clonarChecklistEnCausa($("#checklistId").val());
});

function clonarChecklistEnCausa(idChecklistGuardado) {
	var idCausaLocal = $("#idCausa").val();
	$.ajax({
		url: '/api/causa/clonar/checklist?idCausa=' + idCausaLocal + '&idChecklist=' + idChecklistGuardado,
		dataType: 'json',
		type: "POST",
		success: function() {
			//window.location.reload();
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function() {
			console.log("Algo salio mal");
		}
	});
}

$("#paisChecklist").on('change', function() {
	var idPais = $('#paisChecklist').val();
	$.ajax({
		url: '/api/causa/combo1/' + idPais,
		type: "GET",
		success: function(data) {
			var select = "<option>Seleccione Provincia</option>";
			for (var i = 0; i < data.length; i++) {
				select += "<option value=" + data[i].id + ">" + data[i].nombre + "</option>";
			}
			$("#provinciaChecklist").html(select);
		},
		error: function(data) {
			console.log(data);
		}
	});
});

$("#provinciaChecklist").on('change', function() {
	var idProvincia = $('#provinciaChecklist').val();
	$.ajax({
		url: '/api/circunscripcion/combo?idProvincia=' + idProvincia,
		type: "GET",
		success: function(data) {
			var select = "<option>Seleccione Circunscripcion</option>";
			for (var i = 0; i < data.length; i++) {
				select += "<option value=" + data[i].id + ">" + data[i].value + "</option>";
			}
			$("#circunscripcionChecklist").html(select);
		},
		error: function(data) {
			console.log(data);
		}
	});
});

$("#provinciaCausa").on('change', function() {
	buscarJuzgados($("#provinciaCausa").val());
});

$("#checklistNombre").ready(function() {
	$('#cancelarChecklist').prop('hidden', true);
	$('#cambiarChecklist').prop('hidden', true);
	$('#guardarChecklist').prop('hidden', true);
	if ($("#checklistNombre").val() != "") {
		cargarPasos(0);
	}
});

function cambiar(e) {
	console.log("Cambio: " + e);
	$slide = $('.active').width();
	if ($(e).hasClass('next')) {
		$('.card-carousel').stop(false, true).animate({ left: '-=' + $slide });
	} else if ($(e).hasClass('prev')) {
		$('.card-carousel').stop(false, true).animate({ left: '+=' + $slide });
	}
	$(e).removeClass('prev next');
	$(e).siblings().removeClass('prev active next');
	$(e).addClass('active');
	$(e).prev().addClass('prev');
	$(e).next().addClass('next');
}

function cargarPasos(idCambio) {
	cargarBarra();
	numPaso = 0;
	$("#subPasos").css('display', 'none');
	var items = "";
	if (idCausa != null && idCausa != undefined) {
		$.ajax({
			url: '/api/causa/buscar/pasos/' + idCausa,
			dataType: 'json',
			type: "GET",
			success: function(data) {
				for (var i = 0; i < data.length; i++) {
					var idPaso = data[i].id;
					var orden = data[i].orden;
					items += '<div class="my-card" id="card" value="' + data[i].id + '/ITEM" onclick="cambiar(this); cargarSubPasos(\'' + idPaso + '\', \'' + orden + '\')">'
						+ '<i class="fas fa-arrow-left btnpasoprev"></i>'
						+ '<i class="fas fa-arrow-right btnpasonext"></i>'
						+ '<div class="ite-1">'
						+ '<h4>Paso ' + data[i].orden + '</h4>'
						+ '<ul class="links-checklist">'
						+ '<li>'
						+ '<a href="#agregarItem" onclick="editarItem(\'' + data[i].id + '\', \'' + data[i].nombre + '\')"><i class="fas fa-pencil-alt"></i></a>'
						+ '</li>'
						+ '<li>'
						+ '<a href="#agregarArchivo" onclick="modalAgregarArchivo(' + data[i].orden + ', \'ITEM\', \'' + data[i].id + '\')"><i class="fas fa-paperclip"></i></a>'
						+ '</li>'
						+ '<li class="dropdown user user-menu trespuntosli">'
						+ '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
						+ '<div class="contpoints">'
						+ '<i class="fas fa-plus" class="tamanio-trespuntos"></i>'
						+ '</div>'
						+ '</a>'
						+ '<ul class="dropdown-menu trespuntosmenu">'
						+ '<li>'
						+ '<a href="#eleccionPaso" onclick="elegir(' + data[i].orden + ', \'' + data[i].id + '\')"><img src="/dist/img/suma.png" alt=""><p>Agregar Paso</p></a>'
						+ '</li>'
						+ '<hr>'
						+ '<li>'
						+ '<a href="#agregarNota" onclick="agregarNota(\'' + data[i].id + '\', \'' + data[i].nombre + '\')"><i class="fas fa-sticky-note"></i><p>Notas</p></a>'
						+ '</li>'
						+ '</ul>'
						+ '<li>'
						+ '<a href="#eliminarItem" onclick="deleteItem(\'' + data[i].id + '\')"><i class="fas fa-trash-alt"></i></a>'
						+ '</li>'
						+ '</li>'
						+ '</ul>'
						+ '</div>'
						+ '<div>'
						+ '</div>'
						+ '<div class="box1 checkregister2ti ite-2ti">'
						+ '<label class="containers">';
					if (data[i].estado) {
						items += '<input class="checkmark2" disabled="disabled" type="checkbox" id="check' + i + '"  value=' + data[i].estado + ' checked>';
					} else {
						items += '<input class="checkmark2" disabled="disabled" type="checkbox" id="check' + i + '" value=' + data[i].estado + '>';
					}

					items += '<span class="checkmark2"></span>'
						+ '</label><input hidden id="paso' + data[i].orden + '" value=' + data[i].id + '><p>' + data[i].nombre + '</p>'
						+ '<hr>'
						+ '</div>'
						+ '<div>'
						+ '<div id="subItems' + orden + '" class="subcheck-box"></div>'
						+ '</div>';
					if (data[i].mArchivo != null && data[i].mArchivo.length > 0 || data[i].mEscritos != null && data[i].mEscritos.length > 0 || data[i].limite != null && data[i].limite > 0) {
						items += '<div class="ite-3">'
							+ '<div class="descargar-archivos">'
							+ '<div class="descargaimg"><img src="dist/img/causas-01.svg" alt="" style="float:left;"></div>'
							+ '<div class="descargaenlace"><a href="#verArchivos" onclick="verArchivos(\'' + data[i].id + '\' , \'ITEM\')">Ver adjuntos</a></div>'
							+ '</div>'
							+ '</div>';
					}
					items += '</div>';
				}
				+ '<a href="#verArchivos" onclick="verArchivosSub()"><img src="/dist/img/causas-07.svg" alt=""></a>';
				$("#items").html(items);
				if (data[0].mSubItem != null && data[0].mSubItem.length > 0) {
					cargarSubPasos(data[0].id, data[0].orden);
				}
				/*$num = $('.my-card').length;
				$even = $num / 2;
				$odd = ($num + 1) / 2;
				if ($num % 2 == 0) {*/
					$('.my-card:nth-child(' + 1 + ')').addClass('active');
					$('.my-card:nth-child(' + 1 + ')').prev().addClass('prev');
					$('.my-card:nth-child(' + 1 + ')').next().addClass('next');
				/*} else {
					$('.my-card:nth-child(' + 1 + ')').addClass('active');
					$('.my-card:nth-child(' + 1 + ')').prev().addClass('prev');
					$('.my-card:nth-child(' + 1 + ')').next().addClass('next');
				}*/
			},
			error: function(data) {
				console.log("Error: " + JSON.stringify(data));
			}
		});
	}
	
//	if(idCambio != 0){
//		cambiar(idCambio);
//	}
}

function cargarSubPasos(idItem, orden) {
	console.log("Cargar Sub Pasos: subItems" + orden);
	var subPasos = "";
	$.ajax({
		url: '/api/causa/buscar/subPasos/' + idItem,
		dataType: 'json',
		type: "GET",
		success: function(data) {
			for (var i = 0; i < data.length; i++) {
				subPasos += '<div class="my-subCard" value="' + data[i].id + '/SUBITEM" style="height: 40px;" id="subCard" onclick="cambiarSub(this)">'
					+ '<div class="box1 checkregister2">'
					+ '<label class="containers">';
				if (data[i].estado) {
					subPasos += '<input class="checkmark2" type="checkbox" id="subCheck' + i + '" onclick="checkearSubItemMje(\'' + data[i].id + '\', \'' + idItem + '\',' + data[i].orden + ')" value=' + data[i].estado + ' checked>';
				} else {
					subPasos += '<input class="checkmark2" type="checkbox" id="subCheck' + i + '" onclick="checkearSubItem(\'' + data[i].id + '\', \'' + idItem + '\',' + data[i].orden + ')" value=' + data[i].estado + '>';
				}
				subPasos += '<span class="checkmark2"></span>'
					+ '</label><input hidden id="paso' + i + '" value=' + data[i].id + '><p>' + data[i].subNombre + '</p>'
					+ '</div>'
					+ '<ul class="links-subchecklist">'
					+ '<li class="dropdown user user-menu trespuntosli">'
					+ '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">'
					+ '<div class="contpoints">'
					+ '<a href="#menuSubItem" onclick="menuSubItemAbrir(\'' + data[i].id + '\', \'' + idItem + '\', \'' + data[i].subNombre + '\', \'' + data[i].orden + '\')"><i class="fas fa-ellipsis-v"></i></a>'
					+ '</div>'
					+ '</a>'
					+ '</li>'
					+ '</ul>'
					+ '</div>';
			}
			if (data.length > 0) {
				$("#subPasos").css('display', 'block');
			} else {
				$("#subPasos").css('display', 'none');
			}
			$("#subItems" + orden).html(subPasos);
			/*$num = $('#subCard').length;
			$even = $num / 2;
			$odd = ($num + 1) / 2;
			if ($num % 2 == 0) {*/
				$('.my-subCard:nth-child(' + orden + ')').addClass('active');
				$('.my-subCard:nth-child(' + 1 + ')').prev().addClass('prev');
				$('.my-subCard:nth-child(' + 1 + ')').next().addClass('next');
			/*} else {
				$('.my-subCard:nth-child(' + orden + ')').addClass('active');
				$('.my-subCard:nth-child(' + 1 + ')').prev().addClass('prev');
				$('.my-subCard:nth-child(' + 1 + ')').next().addClass('next');
			}*/
		},
		error: function(data) {
			console.log("error");
			console.log(data.responseJSON.message);
		}
	});
}

function menuSubItemAbrir(idSubPaso, idPaso, nombreSubPaso, orden) {
	console.log("Sub Menu...");
	$("#idSubPasoMenu").val(idSubPaso);
	$("#idPasoMenu").val(idPaso);
	$("#nombreSubPasoMenu").val(nombreSubPaso);
	$("#ordenMenu").val(orden);
}

//Notas
function agregarNota(id, nombre) {
	$("#idAsignarNota").val(id);
	cargarNotasTabla();
}

function agregarNotaMenu() {
	agregarNota($("#idSubPasoMenu").val(), "ninguno");
}


function agregarNotaSubItem(orden, id) {
	$("#idAsignarNota").val(id);
}

function guardarNota() {
	if ($("#tituloNota").val().length < 2) {
		mjeAlerta("#modalAlertaNotas", "Advertencia!", "Debe ingresar un titulo para la nota", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNotas", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	} else if ($("#tituloNota").val().length > 50) {
		mjeAlerta("#modalAlertaNotas", "Advertencia!", "El titulo de la nota no debe superar los 50 caracteres", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNotas", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	}

	if ($("#textoNota").val().length < 2) {
		mjeAlerta("#modalAlertaNotas", "Advertencia!", "Debe ingresar un texto para la nota", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNotas", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	} else if ($("#textoNota").val().length > 250) {
		mjeAlerta("#modalAlertaNotas", "Advertencia!", "El texto de la nota no debe superar los 250 caracteres", "show");
		setTimeout(function() {
			mjeAlerta("#modalAlertaNotas", "Ocultar!", "Ocultar", "hide");
		}, 3000);
		return 0;
	}

	$.ajax({
		url: '/api/causa/guardarNota',
		data: $("#agregarNotaForm").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			mjeAlerta("#modalAlertaNotas", "Info!", "La nota fue creada con éxito.", "show");
			cargarNotasTabla();
			limpiarCamposNota();
			setTimeout(function() {
				mjeAlerta("#modalAlertaNotas", "Ocultar!", "Ocultar", "hide");
			}, 4000);

		},
		error: function(data) {
			console.log("Error al guardar la nota.");
			mjeAlerta("#modalAlertaNotas", "Error!", "Ha ocurrido un error al guardar la nota. " + data.responseJSON.message, "show");
			setTimeout(function() {
				mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
			}, 5000);
		}
	});
}

function eliminarNota() {
	$.ajax({
		url: '/api/causa/eliminarNota',
		data: $("#agregarNotaForm").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			mjeAlerta("#modalAlertaEliminarNota", "Info!", "La nota fue eliminada con éxito.", "show");
			cargarNotasTabla();
			limpiarCamposNota();
			setTimeout(function() {
				mjeAlerta("#modalAlertaEliminarNota", "Ocultar!", "Ocultar", "hide");
			}, 4000);
		},
		error: function(data) {
			console.log("Error al guardar la nota.");
			mjeAlerta("#modalAlertaEliminarNota", "Error!", "Ha ocurrido un error al eliminar la nota. " + data.responseJSON.message, "show");
			setTimeout(function() {
				mjeAlerta("#modalAlerta", "Ocultar!", "Ocultar", "hide");
			}, 5000);
		}
	});
}

function cargarNotasTabla() {
	$.ajax({
		url: '/api/causa/notasTabla',
		data: $("#agregarNotaForm").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			var tablaData = "";
			for (var i = 0; i < data.length; i++) {
				var fecha = new Date(data[i].creado);
				var fechaFormato = fecha.getDate() + " / " + (fecha.getMonth() + 1) + " / " + fecha.getFullYear();
				tablaData += '<tr>' +
					'<td class="tnmin" scope="row">' + fechaFormato + '</td>' +
					'<td>' + data[i].tituloNota + '</td>' +
					'<td>' + data[i].textoNota + '</td>' +
					'<td class="tnmin"><a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "ver" + '\')"><i class="fas fa-eye"></i><\a>' +
					'<a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "editar" + '\')"><i class="fas fa-pencil-alt"></i><\a>' +
					'<a onclick="javascript:seleccionarNota(\'' + data[i].id + '\', \'' + data[i].tituloNota + '\', \'' + data[i].textoNota + '\', \'' + "eliminar" + '\')"><i class="fas fa-trash-alt"></i><\a>' +
					'<tr>';
			}
			$('#bodyTablaNotas').html(tablaData);
		},
		error: function(data) {
			console.log("Error al cargar las notas." + data.responseJSON.message);
		}
	});
}

function seleccionarNota(idNota, tituloNota, textoNota, accion) {
	console.log("Seleccionar");
	if ((accion == 'ver') || (accion == 'editar')) {
		$("#idNota").val(idNota);
		$("#tituloNota").val(tituloNota);
		$("#textoNota").val(textoNota);
	} else if (accion == 'eliminar') {
		console.log("Eliminar");
		window.location.href = "#eliminarNota";
		$("#idNotaEliminar").val(idNota);
	}
}

function limpiarCamposNota() {
	$("#tituloNota").val("");
	$("#textoNota").val("");
}

function cambiarSub(e) {
	console.log("Cambiando...");
	$slide = $('.active').width();
	console.log($('.active').position().left);
	if ($(e).hasClass('next')) {
		$('.subCard-carousel').stop(false, true).animate({ left: '-=' + $slide });
	} else if ($(e).hasClass('prev')) {
		$('.subCard-carousel').stop(false, true).animate({ left: '+=' + $slide });
	}
	$(e).removeClass('prev next');
	$(e).siblings().removeClass('prev active next');
	$(e).addClass('active');
	$(e).prev().addClass('prev');
	$(e).next().addClass('next');
}

$("#cancelarChecklist").click(function() {
	location.reload();
});

$("#cambiarChecklist").click(function() {
	$("#alertaChecklist").prop('hidden', true);
	$("#cancelarChecklist").prop('hidden', true);
	$("#cambiarChecklist").prop('hidden', true);
	$("#guardarChecklist").prop('hidden', false);
	$('#mensajeSeleccionChecklist').hide();

	if (document.getElementById('checklist').value == 'nuevochecklist') {
		window.location.href = '#agregarNuevoChecklist';
		//camposNuevoChecklist("show");
		$('#containerCard').hide();
		$('#containerSubCard').hide();
		$('#mensajeSeleccionChecklist').show();
	} else {
		//camposNuevoChecklist("hide");
		$('#containerCard').hide();
		$('#containerSubCard').hide();
		$('#mensajeSeleccionChecklist').show();
	}
	$("#checklistId").val($("#checklist").val());
	console.log("ChecklistID: " + $("#checklistId").val());
});
// END CHECKLIST

function eliminarConsulta() {
	var idCausa = $("#idCausaConsulta").val();
	$.ajax({
		url: '/api/causa/eliminarconsulta/' + idCausa,
		data: '',
		dataType: 'json',
		type: "POST",
		success: function(data) {
			window.location = '/administracion/causa/editar/' + data.id;
		},
		error: function(data) {
		}
	});
}

//CHECKEAR ITEMS Y SUB ITEMS
function checkearItem(idPaso, orden) {
	var idItemSiguiente = $("#paso" + (orden + 1)).val();
	$.ajax({
		url: '/api/causa/checkearItem?id=' + idPaso + '&idCausa=' + idCausa + '&idItemSiguiente=' + idItemSiguiente,
		type: "POST",
		success: function(data) {
		},
		error: function(data) {
		}
	});
}

function checkearSubItemMje(idSubPaso, idItem, orden) {
	console.log("Orden Mje: " + orden);
	window.location.href = "#checkSubItem";
	$("#idSubPasoCheckSubItem").val(idSubPaso);
	$("#idItemCheckSubItem").val(idItem);
	$("#ordenCheckSubItem").val(orden);
}

function checkearSubItemCambiar() {
	$.ajax({
		url: '/api/causa/desCheckearSubItem?id=' + $("#idSubPasoCheckSubItem").val() + '&idCausa=' + idCausa + '&idItem=' + $("#idItemCheckSubItem").val(),
		type: "POST",
		success: function(data) {
			console.log("Data checkSubItem: " + JSON.stringify(data));
			//cambiarPag($('#idCausa').val());
			$("#idSubPasoCheckSubItem").val("");
			$("#idItemCheckSubItem").val("");
			$("#ordenCheckSubItem").val("");
			window.location.href = "#";
			cargarPasos(0);
		},
		error: function(data) {
			console.log("Error checkSubItem: " + JSON.stringify(data));
			mjeAlerta("#alertaChecklist", "Exito!", "Error al checkear el sub paso", "show");
			setTimeout(function() {
				mjeAlerta("#alertaChecklist", "Ocultar", "Ocultar", "hide");
			}, 5000);
		}
	});
}

function checkearSubItem(idSubPaso, idItem, orden) {
	console.log("Orden: " + orden);
	var idSubItemSiguiente = $("#paso" + (orden + 1)).val();
	console.log("Id Sub Sug: " + idSubItemSiguiente);
	$.ajax({
		url: '/api/causa/checkearSubItem?id=' + idSubPaso + '&idCausa=' + idCausa + '&idItem=' + idItem + '&idSubItemSiguiente=' + idSubItemSiguiente,
		type: "POST",
		success: function(data) {
			$("#idSubPasoCheckSubItem").val("");
			$("#idItemCheckSubItem").val("");
			$("#ordenCheckSubItem").val("");
			window.location.href = "#";
			cargarPasos(0);
		},
		error: function(data) {
			if (data.responseJSON.status == 404) {
				mjeAlerta("#alertaChecklist", "Error!", " " + data.responseJSON.message + " - No posee permisos.", "show");
			} else {
				mjeAlerta("#alertaChecklist", "Error!", " Error al checkear el sub paso", "show");
			}
			setTimeout(function() {
				mjeAlerta("#alertaChecklist", "Ocultar", "Ocultar", "hide");
			}, 5000);
		}
	});
}

//VERIFICAR QUE ESTEN CHECKEADOS 
function verificarCheck(cant) {
	for (var i = 0; i < cant; i++) {
		var input = document.getElementById("check" + i);
		var estado = input.value;
		if (estado === "true") {
			input.checked = true;
		} else {
			input.checked = false;
		}
	}
}

function verificarSubCheck(cant) {
	console.log("Verificar Check: " + cant);
	for (var i = 0; i < cant; i++) {
		var input = $("#subCheck" + i).is(":checked");
		if (input === "true") {
			$("#subCheck" + i).prop("checked", true);
		} else {
			$("#subCheck" + i).prop("checked", true);
		}
	}
}

//BUSCAR JUZGADOS
function buscarJuzgados(idProvincia) {
	if (idProvincia != null && idProvincia != undefined && idProvincia != '') {
		$.ajax({
			url: '/api/causa/buscar/juzgadosProvincia?idProvincia=' + idProvincia,
			type: 'GET',
			contentType: "application/json",
			data: 'json',
			success: function(data) {
				var selectList = "<option selected disabled>Seleccione dependencia</option>";
				for (var x = 0; x < data.length; x++) {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
				$('#juzgadoOficina').html(selectList);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
}

// VER ESCRITO
function verEscritos(idItem, idCausa, idEscrito) {
	$.ajax({
		url: '/api/escrito/variables?idItem=' + idItem + '&idCausa=' + idCausa + '&idEscrito=' + idEscrito,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function modalAgregar() {
	$("#tituloModal").html("Agregar Causa");
	resetForm()
}


function resetForm() {
	$("#actorApellido").val("").trigger("change")
	$("#actorNombre").val("").trigger("change");
	$("#demandadoApellido").val("").trigger("change");
	$("#demandadoNombre").val("").trigger("change");
	$("#paisCausa").val("").trigger("change");
	$("#tipoFuero").val("").trigger("change");
	$("#provinciaCausa").val("").trigger("change");
	$("#idInstancia").val("").trigger("change");
	$("#juzgadoOficina").val("").trigger("change");
	$("#tipoCausa").val("").trigger("change");
	$("#rolCausa").val("").trigger("change");
	$("#nroExpediente").val("").trigger("change");
	$("#checklistSelect").val("").trigger("change");
}
//Cargar Barra
function cargarBarra() {
	$.ajax({
		url: '/administracion/causa/barraCausa/' + idCausa,
		dataType: "json",
		type: "POST",
		success: function(data) {
			var datosBarra = '<p class="datosbarracausa1">Estado de la Causa</p>';
			if (data.estado != null) {
				datosBarra += '<p class="datosbarracausa2">' + data.estado + '%</p>';
			} else {
				datosBarra += '<p class="datosbarracausa2">0%</p>';
			}
			$('#datosbarra').html(datosBarra);

			var colorBarra = '';
			if (data.estado != null) {
				if (data.color == 'rojo') {
					colorBarra = '<div class="porcentajecausa barrared" style="width: ' + data.estado + '%"></div>';
				} else if (data.color == 'amarillo') {
					colorBarra = '<div class="porcentajecausa barrayellow" style="width: ' + data.estado + '%"></div>';
				} else if (data.color == 'verde') {
					colorBarra = '<div class="porcentajecausa barragreen" style="width: ' + data.estado + '%"></div>';
				}
				$('#colorbarra').html(colorBarra);
			}
		},
		error: function(data) {
			console.log(data.responseJSON);
			if(data.responseJSON != undefined){
				console.log("Cargar Barra Error: " + data.responseJSON.message);
			}
		}
	});
}

//Alerta
function mjeAlerta(idAlerta, mjeTitulo, mjeTexto, action) {
	var classAlert = "alert-info";
	if (mjeTitulo == 'Exito!' || mjeTitulo == 'Info!') {
		classAlert = "alert-info";
	} else {
		classAlert = "alert-danger";
	}
	if (action == "show") {
		$(idAlerta).empty().html('<div class="alert ' + classAlert + ' alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-ban"></i> ' + mjeTitulo + ' </h4> ' + mjeTexto + '</div>').show();
	} else {
		$(idAlerta).hide();
	}
}

//Ocultar
function camposNuevoChecklist(action) {
	if (action == 'show') {
		$('#encabezadoChecklist').show();
	} else {
		$('#encabezadoChecklist').hide();
	}
}
//Limpiar Formulario guardarCausa
function limpiarForm(){
	var limpiar = document.querySelector('guardarCausa');
	document.querySelector('a').addEventListener('click',function(a){
		e.preventDefault();
		limpiar.reset();
	}, false);
}

//Limpiar Formulario guardarArchivo
function limpiarFormArchivo(){
	var limpiar = document.querySelector('guardarArchivo');
	document.querySelector('a').addEventListener('click',function(a){
		e.preventDefault();
		limpiar.reset();
	}, false);
}
//Deshabilita botonGuardar
function deshabilitar(){
	
	actorNombre = document.getElementById("actorNombre").value;
	actorApellido = document.getElementById("actorApellido").value;
	demandadoNombre = document.getElementById("demandadoNombre").value;
	demandadoApellido = document.getElementById("demandadoApellido").value;
	tipoCausa = document.getElementById("tipoCausa").value;
	
    val = 0;
    
	if(actorNombre == ""){
		val++;
	}
	if(actorApellido == ""){
		val++;
	}
	if(demandadoNombre == ""){
		val++;
	}
	if(demandadoApellido == ""){
		val++;
	}
	if(tipoCausa == ""){
		val++;
	}
    
	if(val == 0){
		document.getElementById("botonGuardar").disabled = true;
		//document.getElementById("botonGuardar").addEventListener("click", disabled);
	}else {
		document.getElementById("botonGuardar").disabled = false;
	}
}
document.getElementById("botonGuardar").addEventListener("click", deshabilitar);
