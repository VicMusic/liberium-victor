function verConsulta(id){
	console.log(id);
	var id = id.getAttribute('idConsulta');
	$.ajax({
		url: '/api/consultas/buscarConsulta/' + id,
        data: '',
        dataType: "json",
        type: "GET",
        success: function (data) {
        	$("#nombre").html(data.nombre);
        	$("#apellido").html(data.apellido);
        	$("#dni").html(data.dni);
        	$("#fechaNacimiento").html(data.nacimiento);
        	$("#edad").html(data.edad);
        	$("#genero").html(data.genero);
        	$("#direccion").html(data.direccion);
        	$("#email").html(data.mail);
        	$("#telefono").html(data.telefono);
        	$("#localidad").html(data.mLocalidad.nombre);
        	$("#idFormulario").val(data.mFormulario.id);
        	$("#idConsulta").val(data.id);
        }, 
        error: function (data){
        }
	});
}

function verFormulario(){
		
	var idFormulario = $("#idFormulario").val();
	var idConsulta = $("#idConsulta").val();
	
	console.log(idFormulario + " " + idConsulta);
	
	$.ajax({
		url: '/api/consultas/buscarFormulario/' + idFormulario + '/' + idConsulta,
		data: '',
		dataType: "json",
		type: "GET",
		success: function (data){
			console.log(data);
			
			console.log(data.respuestas.length);
			
			string = "";
			
			for (var i = 0; i < data.respuestas.length; i++) {
				
				string += '<div>'
						+ 	'<h4>¿' + data.preguntas[i].titulo + '?</h4>' 
						+ 	'<p>' + data.respuestas[i].respuesta + '</p>'
						+ '</div>';
			}
			
			$('#preguntasRespuestas').html(string);
			$('#divPreguntasRespuestas').show();
			$("#divForm").hide();
			$("#ocultarFormulario").show();
			
		}, 
		error: function (data){
			console.log(data);
		}
	});
}

function ocultarFormulario(){
	$("#divPreguntasRespuestas").hide();
	$("#divForm").show();
	$("#ocultarFormulario").hide();

}

function crearCausa(){
	
	var idConsulta = $("#idConsulta").val();
	
	$.ajax({
		url: '/api/causa/causaconsulta/' + idConsulta,
		data: '',
		dataType: "json",
		type: "POST",
		success: function (data){
			console.log(data);
			
			window.location = '/administracion/causa/editar/' + data.id;
			
		}, 
		error: function (data){
			console.log(data);
		}
	});

	
}