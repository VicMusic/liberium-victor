function armarCombo(idSeleccionado, select, url) {
	$.ajax({
		url : url,
		type : 'GET',
		async : false,
		success : function(data) {
			$('#' + select).removeAttr("disabled", "disabled").val(null).trigger('change');
			var opciones = "<option selected value=''>Seleccione</option>";
			$.each(data, function(i, item) {
				if(idSeleccionado != null && idSeleccionado == item.id){
					opciones = opciones + '<option value="' + item.id + '" selected="selected">'+ item.value+'</option>';
				}else{
					opciones = opciones + '<option value="' + item.id + '">'+ item.value +'</option>';
				}
			});
			$('#' + select).html(opciones).trigger('change');
		}
	});
}

function deshabilitarCombo(select, mensaje){
	$("#" + select).empty()
	.prop('disabled', 'disabled')
	if(mensaje != null && mensaje != ""){
	    $("#" + select).append('<option value="-1" selected="selected" disabled="disabled">' + mensaje + '</option>')
	}
	$("#" + select).trigger("chosen:updated");
}

	