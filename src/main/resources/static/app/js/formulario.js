function crearFormulario() {
	$.ajax({
		url: '/api/formulario/guardar',
		data: $("#crearFomularioForm").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#alertaModalCrear").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Exito!</h4>'
				+ 'Se ha guardado el formulario con exito. '
				+ '</div>').show();

			setTimeout(function() {
				$("#alertaModalCrear").hide();
			}, 5000);
			$("#idFormulario").val(data.id);
			$("#divPreguntas").show();
			$("#botonCancelar").hide();
			$("#botonGuardar").hide();
		},
		error: function(data) {
			console.error(data.responseJSON.message);
			$("#alertaModalCrear").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el formulario. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#alertaModalCrear").hide();
			}, 5000);
		}
	});
}

function mostrarBotones() {
	$("#botonCancelar").show();
	$("#botonGuardar").show();
}

function modificarFormulario(componente) {
	var idFormulario = componente.getAttribute('objeto');
	var idMateria = componente.getAttribute('materia');
	console.log("Materia:" + idMateria)
	$.ajax({
		url: '/api/formulario/buscar/preguntas/' + idFormulario,
		dataType: "json",
		type: "GET",
		success: function(data) {
		console.log(data)
			$("#idFormulario").val(componente.getAttribute('objeto'));
			$("#titulo").val(componente.getAttribute('titulo'));
			$("#materia").val(componente.getAttribute('materia')).trigger('change');;
			$("#modalAgregarTipo").val(componente.getAttribute('tipo'));
			$("#pais").val(componente.getAttribute('idPais')).trigger('change');
			traerTipoCausa(componente.getAttribute('materia'), componente.getAttribute('tipo'));
			traerProvincias(componente.getAttribute('idPais'), componente.getAttribute('idProvincia'));
			$("#alertaPregunta").hide();
			$("#divPreguntas").show();
			$("#preguntas").empty();
			var preguntas = '';
			for (let i = 0; i < data.length; i++) {
				preguntas +=
					'<div class="pregselect" id="pregunta' + data[i].id + '">'
					+ '<label class="labelPregunta">Pregunta ' + data[i].tipo + '</label>'
					+ '<input type="text" disabled="disabled" value="' + data[i].titulo + '">'
					+ '<a onclick="eliminarPregunta(\'' + data[i].id + '\')"><img src="/dist/img/basurero.png" alt=""></a>'
					+ '<a onclick="editarPregunta(\'' + data[i].id + '\')"><img src="/dist/img/imgmodal-04.png" alt="" style="right: 29px;"></a></div>';
			}
			$("#preguntas").append(preguntas);
			$("#botonCancelar").hide();
			$("#botonGuardar").hide();
			window.location.href = "#agregar";
		},
		error: function(data) {
		}
	});
}

function cancelarPregunta() {
	$('#preguntaFormularioForm').trigger("reset");
}

function guardarPregunta() {

	$.ajax({
		url: '/api/formulario/guardarpregunta',
		data: $("#preguntaFormularioForm").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			console.log(data);
			$("#alertaPregunta").hide();
			$("#preguntas").empty();
			var preguntas = '';
			for (let i = 0; i < data.length; i++) {
				preguntas +=
					'<div class="pregselect" id="pregunta' + data[i].id + '">'
					+ '<label class="labelPregunta">Pregunta ' + data[i].tipo + '</label>'
					+ '<input type="text" disabled="disabled" value="' + data[i].titulo + '">'
					+ '<a onclick="eliminarPregunta(\'' + data[i].id + '\')"><img src="/dist/img/basurero.png" alt=""></a>'
					+ '<a onclick="editarPregunta(\'' + data[i].id + '\')"><img src="/dist/img/imgmodal-04.png" alt="" style="right: 29px;"></a></div>';
			}
			$("#preguntas").append(preguntas);
			$('#preguntaFormularioForm').trigger("reset");
			window.location.href = "#agregar";
		},
		error: function(data) {
			console.error(data.responseJSON.message);
			$("#alertaModalPregunta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear la pregunta. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#alertaModalPregunta").hide();
			}, 5000);
		}
	});

}


function eliminar() {

	$.ajax({
		url: '/api/formulario/eliminar?id=' + $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {

			$("#eliminar").hide();
			$("#modalConfirmar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El formulario fue eliminado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location = '/administracion/formulario/listado';

		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar el proceso. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}


function eliminarPregunta(idPregunta) {
	$.ajax({
		url: '/api/formulario/pregunta/eliminar/' + idPregunta,
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#pregunta" + data.id).empty();
			window.location.href = "#agregar";
		},
		error: function(data) {
			console.error(data.responseJSON.message);
			$("#alertaModalPregunta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al eliminar la pregunta. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#alertaModalPregunta").hide();
			}, 5000);
		}
	});
}

function editarPregunta(idPregunta) {
	$.ajax({
		url: '/api/formulario/pregunta/buscar/' + idPregunta,
		dataType: "json",
		type: "GET",
		success: function(data) {
			console.log(data);
			$("#idFormularioPregunta").val(data.formulario.id);
			$("#idPregunta").val(data.id);
			$("#tipoPregunta").val(data.tipo);
			$("#textoPregunta").val(data.titulo);
			$("#variable").val(data.variableAmigable);
			$("#opcionesInputs").empty();
			switch (data.tipo) {
				case 'ABIERTA':
					$("#tipoInput").val(data.inputRespuesta);
					$("#selectTipoRespuesta").show();
					$("#opciones").hide();
					break;
				case 'CERRADA':
					for (let i = 0; i < data.opciones.length; i++) {
						$('#opcionesInputs').append(
							'<div class="pregselect" id="' + data.opciones[i].id + '">'
							+ '<input type="text" name="opciones" placeholder="Ingrese la opcion" value="' + data.opciones[i].titulo + '"/>'
							+ '<a onclick="eliminarOpcion(\'' + data.opciones[i].id + '\')">'
							+ '<img src="/dist/img/basurero.png" alt=""></a></div>');
					}
					$("#selectTipoRespuesta").hide();
					$("#opciones").show();
					break;
				default:
					break;
			}
			window.location.href = "#pregunta";
		},
		error: function(data) {
			console.error(data.responseJSON.message);
			$("#alertaModalPregunta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al eliminar la pregunta. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#alertaModalPregunta").hide();
			}, 5000);
		}
	});
}

function agregarOpcion() {
	var orden = $(".pregselect").length;
	var idOpcion = 'opcion' + orden;

	$('#opcionesInputs').append(
		'<div class="pregselect" id="' + idOpcion + '">'
		+ '<input type="text" name="opciones" placeholder="Ingrese la opcion"/>'
		+ '<a onclick="eliminarOpcion(\'' + idOpcion + '\')">'
		+ '<img src="/dist/img/basurero.png" alt=""></a></div>');
}

function eliminarOpcion(idOpcion) {
	$("#" + idOpcion).empty();
}

function traerTipoCausa(idMateria, tipo) {
	$.ajax({
		url: '/api/formulario/combo/' + idMateria,
		type: 'GET',
		dataType: 'json',
		success: function(data) {
			var selectList = "<option disabled value=''>--Seleccione tipo de causa--</option>";
			for (var x = 0; x < data.length; x++) {
				if (data[x].id == tipo) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">' + data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">' + data[x].nombre + "</option>";
				}
			}
			if (idMateria != null) {
				$("#materia").val(idMateria);
			}
			$('#modalEditarTipo').html(selectList);
			$('#modalAgregarTipo').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function traerProvincias(idPais, idProvincia) {
	console.log("Traer provincias...");
	$.ajax({
		url: '/api/contacto/combo1/' + idPais,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {

			var selectList = "<option selected disabled>Seleccione una provincia</option>";
			for (var x = 0; x < data.length; x++) {
				if (idProvincia == data[x].id) {
					selectList += '<option selected value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
			}
			if (idPais != null) {
				$("#pais").val(idPais);
			}
			$('#provincia').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}


function modalPregunta(tipoPregunta, idPregunta) {
	$("#idFormularioPregunta").val($("#idFormulario").val());
	$('#preguntaFormularioForm').trigger("reset");
	$("#idPregunta").val("");
	$("#tipoPregunta").val(tipoPregunta);
	if (idPregunta != null) {
		$("#idPregunta").val(idPregunta);
	}

	if (tipoPregunta == 'ABIERTA') {
		$("#selectTipoRespuesta").show();
		$("#opciones").hide();
	} else {
		$("#selectTipoRespuesta").hide();
		$("#opciones").show();
	}

	window.location.href = '#pregunta';
}


function eliminarFormulario(componente) {
	$("#agregar").modal("hide");
	$("#eliminar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}