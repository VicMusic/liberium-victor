
$( document ).ready(function() {
	var resultado = $("#resultado").val();
	if(resultado){
		$("#alerta").html(
				'<div class="alert alert-info alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
				+ 'No se encontraron resultados con la búsqueda realizada. '
				+ '</div>').show();
	}
});

function guardarLocalidad() {
	$.ajax({
				url : '/api/localidad/guardar',
				data : $("#modalFormulario").serialize(),
				dataType : "json",
				type : "POST",
				success : function(data) {
					$("#agregar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La localidad fue creada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location.href = window.location.origin + window.location.pathname + window.location.search;	

				},
				error : function(data) {
					$("#modalAlerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al crear la localidad. '
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#modalAlerta").hide();
					}, 5000);
				}
			});
}

function editarLocalidad() {
	$.ajax({
				url : '/api/localidad/guardar',
				data : $("#modalFormularioEditar").serialize(),
				dataType : "json",
				type : "POST",
				success : function(data) {
					$("#editar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La localidad fue editada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location.href = window.location.origin + window.location.pathname + window.location.search;	

				},
				error : function(data) {
					$("#modalAlerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al editar la localidad. '
											+ data.responseJSON.message
											+ '</div>').show();
					setTimeout(function() {
						$("#modalAlerta").hide();
					}, 5000);
				}
			});
}

function modificarLocalidad(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#modalEditarProvincia").val(componente.getAttribute('provincia')).trigger("change");
	//window.location = '/administracion/localidad/listado#editar';
}

function eliminarLocalidad(componente) {
	$("#modalLocalidad").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {

	$.ajax({
				url : '/api/localidad/eliminar?id='
						+ $("#modalEliminarId").val(),
				dataType : "json",
				type : "POST",
				async : true,
				success : function(data) {

					$("#eliminar").hide();
					$("#modalConfirmar").hide();

					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-info alert-dismissible">'
											+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
											+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
											+ 'La localidad fue eliminada con éxito'
											+ '</div>').show();

					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);

					window.location.href = window.location.origin + window.location.pathname + window.location.search;	

				},
				error : function(data) {
					$("#alerta")
							.empty()
							.html(
									'<div class="alert alert-danger alert-dismissible">'
											+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
											+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
											+ 'Ha ocurrido un error al eliminar la localidad. '
											+ data.responseJSON.message
											+ '</div>').show();
					$("#modalConfirmar").modal("hide");
					setTimeout(function() {
						$("#alerta").hide();
					}, 5000);
				}
			});
}
