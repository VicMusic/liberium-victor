var idAbogadoInvitar = 0, idRolAbogadoInvitar = 0, emailAbogadoInvitar = "";

function guardarEstudio() {
	$.ajax({
		url: '/api/estudio/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El estudio fue creado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location = '/administracion/usuario/perfil/';

		},
		error: function(data) {
			$("#modalAlertaEstudio").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el estudio. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaEstudio").hide();
			}, 5000);
		}
	});
}

function modificarEstudio(componente) {
	$("#editarIdEstudio").val(componente.getAttribute('objeto'));
	$("#editarNombreEstudio").val(componente.getAttribute('nombre'));
	$("#editarCrear").html('Editar estudio');
}

function eliminarEstudioC(componente) {
	$("#idEstudioEliminar").val(componente.getAttribute('objeto'));
}

function enviarEstudio(componente) {
	$("#idEstudioVer").val(componente.getAttribute('objeto'));
	console.log("enviarEstudio: " + $("#idEstudioVer").val());
	verMiembros($("#idEstudioVer").val());
}

function cerrar() {
	$("#editarIdEstudio").val('');
	$("#editarNombreEstudio").val('');
	$("#editarCrear").html('Crear un Estudio');
}

function eliminarEstudio() {
	$.ajax({
		url: '/api/estudio/eliminar?id=' + $("#idEstudioEliminar").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta").empty().html('<div class="alert alert-info alert-dismissible">'
				+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
				+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
				+ 'El estudio fue eliminado con éxito'
				+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location = '/administracion/usuario/perfil';

		},
		error: function(data) {
			$("#alerta").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al eliminar el estudio. ' + data.responseJSON.message
				+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function verAbogados() {
	console.log("Funcion verAbogados - Buscar DNI: " + $("#query").val());

	var query = $("#query").val();

	if (query == "") {
		alert("Debe ingresar un DNI");
		return 0;
	}

	$.ajax({
		url: '/api/estudio/abogados' + query,
		data: '',
		contentType: "application/json",
		dataType: "json",
		type: "GET",
		success: function(data) {
			
			$("#btnInvitar").prop('disabled', false);
			idAbogadoInvitar = data.mUsuario.id;
			emailAbogadoInvitar = data.mUsuario.email;
			
			string = "";

			string += '<div class="boxabogados fondoabogado1"><label class="containerc"><div class="abogado"><div class="abogado"><div class="boxabogado" alt="'
				+ data.mUsuario.foto
				+ '" style="background-image: url(/administracion/usuario/foto/'
				+ data.mUsuario.id
				+ ');width: 80px; height: 80px; background-size:cover; background-position:center;"></div><div class="datosabogado"><h4><span>'
				+ data.mUsuario.nombre
				+ '</span></p>'
				+ '<p><span>' + data.mUsuario.email + '</span></p>'
				+ '</div></div></div></span></label></div>';

			$('#abogados').html(string);

			setTimeout(function() {
				console.log("MOSTRANDO ABOGADOS");
				$("#abogados").show();
				//window.location.href = "#verMiembros";
			}, 100);
		},
		error: function(data) {
			console.log("Error...");
			idAbogadoInvitar = 0;
			
			$("#abogados").hide();
			$("#btnInvitar").prop('disabled', true);
			
			alert("No se encontraron abogados");
			
			$("#modalAlertaEstudio").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'No se encuentra el abogado. '
				+ '</div>').show();
				
			setTimeout(function() {
				$("#modalAlertaEstudio").hide();
			}, 5000);
		}
	});
}

function buscarRoles(idRol) {
	/*
	$
			.ajax({
				url : '/api/usuario/combo1/' + id,
				type : 'GET',
				contentType : "application/json",
				data : 'json',
				success : function(data) {

					var selectList = "<option selected disabled value=''>Seleccione una provincia</option>";
					for (var x = 0; x < data.length; x++) {
						selectList += '<option value="' + data[x].id + '">'
								+ data[x].nombre + "</option>";
					}
					$('#provincia').html(selectList);
					$('#idProvincia').html(selectList);
				},
				error : function(data) {
					console.log(data);
				}
			});
			*/
}

function verMiembros() {

	var idEstudio = $("#idEstudioVer").val();

	$.ajax({
		url: '/api/estudio/miembros' + idEstudio,
		data: '',
		contentType: "application/json",
		dataType: "json",
		type: "GET",
		success: function(data) {
			string = "";

			for (var x = 0; x < data.length; x++) {
				console.log(data[x]);

				string += '<td style="border: none;">' + data[x].mUsuario.nombre + '</td>'
					+ '<td style="border: none;"><div>'
					+ '<a href="#nuevoEstudio">'
					+ '<img src="/dist/img/tabla-02.png" style="width: 24.3%;" alt="">'
					+ '</a></div><div>'
					+ '<a href="#eliminarEstudio">'
					+ '<img src="/dist/img/tabla-01.png" style="width: 24.3%;" alt=""></a></div></td>'

				console.log(data[x].mUsuario.nombre);

			}
			$('#miembros').html(string);

			setTimeout(function() {
				console.log("MOSTRANDO ABOGADOS");
				window.location.href = "#verMiembros";
			}, 100);
		},
		error: function(data) {
			$("#modalAlertaEstudio").empty().html('<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
				+ 'Ha ocurrido un error al crear el estudio. ' + data.responseJSON.message
				+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaEstudio").hide();
			}, 5000);
		}
	});
}

function invitarAbogado() {
	console.log("Entro a Invitar");
	console.log("Id: " + idAbogadoInvitar);
	
	
}

