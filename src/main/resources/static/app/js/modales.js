var intereses = '';

function buscarIntereses(){
	$.ajax({
		url: '/api/contable-causa/buscar-intereses/',
		data: '',
		dataType: "json",
		type: "GET",
		success: function (data){
			
			for (var i = 0; i < data.length; i++) {
				intereses += '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
			}
			
			return intereses;
		},
		error: function (data){
			console.log(data);
		}
	});
}



var idCausa = $("#idCausaOG").val();

function modalInteres(idHonorario, posicionInteres){
	
	console.log(idHonorario + " " + posicionInteres);
	
	$("#modalInteres").empty();
	
	var inputHonorario = "";
	var inputPosicion = "";
	var string = "";
	
	if (posicionInteres != null) {
		inputPosicion = '<input type="hidden" name="posicionInteres" value="' + posicionInteres + '">';
	} else {
		inputPosicion = '<input type="hidden" name="posicionInteres" value="POR_DEFECTO">';
	}
	
	if (idHonorario != null) {
		inputHonorario = '<input type="hidden" name="idHonorario" value="' + idHonorario + '">';
	} else {
		inputHonorario = '<input type="hidden" name="idHonorario" value="SIN_HONORARIO">';
	}
	
string = '<div id="modalAlerta"></div>'
		   + '<input type="hidden" name="idCausa" value="' + idCausa + '" id="idCausa">'
		   +  inputHonorario
		   +  inputPosicion
		   + '<div class="label50">'
		   + '<label for="">Monto</label>'
		   + '<input name="monto" type="number" id="monto" placeholder="Monto del gasto">'
		   + '</div>'
		   + '<div class="label50">'
		   + '<label for="">Detalle</label>'
		   + '<input name="detalle" type="text" id="detalle" placeholder="Ingrese un detalle" style="margin-left: 5px;">'
		   + '</div>'
		   + '<div class="label100">'
		   + '<label for="">Tipo de Interes</label>'
		   + '<select class="form-control" name="idInteres" style="width: 100%">'
		   + '<option selected disabled value="">Seleccione un Interes</option>'
		   + '<option value="SIN_INTERES">Sin Interes</option>'
		   + intereses
		   + '</select>'
		   + '</div>'
		   + '<div class="label50 mlright">'
		   + '<label for="">Fecha inicio</label>'
		   + '<input name="fechaInicio" type="date">'
		   + '</div>'
		   + '<div class="label50" id="fechaFinal">'
		   + '<label for="">Fecha final</label>'
		   + '<input name="fechaFin" type="date" id="fechaFinAgregar">'
		   + '</div>';
	
	$("#modalInteres").append(string);
	
}

function modalGasto(idHonorario){
	
	$("#modalGasto").empty();
	
	var inputHonorario = "";
	var string = "";
	
	if (idHonorario != null) {
		inputHonorario = '<input type="hidden" name="idHonorario" value="' + idHonorario + '">';
	} else {
		inputHonorario = '<input type="hidden" name="idHonorario" value="SIN_HONORARIO">';
	}
	
	string += '<div id="modalAlertaGasto"></div>'
    + '<input type="hidden" name="idCausa" value="' + idCausa + '" id="idCausa">'
	+ inputHonorario
	+ '<div class="label50" style="margin-right: 5px;">'
	+ '<label for="">Monto:</label>'
	+ '<input name="monto" type="number" placeholder="Monto del gasto">'
	+ '</div>'
	+ '<div class="label50">'
	+ '<label for="">Fecha del gasto:</label>'
	+ '<input name="fechaGasto" type="date">'
	+ '</div>'
	+ '<div class="label100">'
	+ '<label for="">Tipo de Interes:</label>'
	+ '<select class="form-control" name="idInteres" style="width: 100%" onchange="mostrarFechas(this.value)">'
	+ '<option selected disabled value="">Seleccione un Interes</option>'
	+ '<option value="SIN_INTERES">Sin Interes</option>'
	+ intereses
	+ '</select>'
	+ '</div>'
	+ '<div id="fechas" hidden="">'
	+ '<div class="label50 mlright">'
	+ '<label for="">Fecha inicio</label>'
	+ '<input name="fechaInicio" type="date">'
	+ '</div>'
	+ '<div class="label50" id="fechaFinal">'
	+ '<label for="">Fecha final</label>'
	+ '<input name="fechaFin" type="date">'
	+ '</div>'
	+ '</div>'
	+ '<div class="label50" style="margin-right: 5px;">'
	+ '<label for="">Detalle:</label>'
	+ '<input name="detalle" type="text" placeholder="Detalle del gasto">'
	+ '</div>'
	+ '<div class="label50">'
	+ '<label for="">Pagado por:</label>'
	+ '<input name="nombre" type="text" placeholder="Ingrese un nombre">'
	+ '</div>';
	
	$("#modalGasto").append(string);
}

function modalGastoHonorario(idHonorario){
	
	$("#modalGastoHonorario").empty();
	
	var inputHonorario = '<input type="hidden" name="idHonorario" value="' + idHonorario + '">';
	var string = "";
	
	string += '<div id="modalAlertaCobro"></div>'
    + '<input type="hidden" name="idCausa" value="' + idCausa + '" id="idCausa">'
	+ inputHonorario
	+ '<div class="label100">'
	+ '<label for="">Concepto:</label>'
	+ '<input name="nombre" type="text" placeholder="Ingrese un concepto">'
	+ '</div>'
	+ '<div class="label50" style="margin-right: 5px;">'
	+ '<label for="">Monto:</label>'
	+ '<input name="monto" type="number" placeholder="Monto del cobro">'
	+ '</div>'
	+ '<div class="label50">'
	+ '<label for="">Fecha:</label>'
	+ '<input name="fechaGasto" type="date">'
	+ '</div>';
	
	$("#modalGastoHonorario").append(string);
}

function modalVencimiento(idElementoContable, fechaVecimiento){
	
 	$("#modalVencimientoVer").empty();
	
	var string = "";
	
	string += '<div id="modalAlertaVencimiento"></div>'
    + '<input type="hidden" name="idElementoContable" value="' + idElementoContable + '">';
    
    if (fechaVecimiento != null) {
    	string += '<div class="label100">'
    	+ '<label for="">Fecha de vencimiento:</label>'
    	+ '<input name="fechaVencimiento" type="date" value="' + fechaVecimiento + '">'
    	+ '</div>';
	} else {
		string += '<div class="label100">'
		+ '<label for="">Fecha de vencimiento:</label>'
		+ '<input name="fechaVencimiento" type="date">'
		+ '</div>';
	}
    
	
	$("#modalVencimientoVer").append(string);
	
	window.location = "#modalVencimiento";
	
}