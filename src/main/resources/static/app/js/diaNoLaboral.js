function guardarDiaNoLaboral() {
	$.ajax({
		url: '/api/dianolaborable/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			console.log(data)
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El dia no laborable fue creado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear el dia no laborable. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 5000);
		}
	});
}

function editarDia() {
	console.log($("#modalFormularioEditar").serialize())
	$.ajax({
		url: '/api/dianolaborable/guardar',
		data: $("#modalFormularioEditar").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			console.log(data)
			$("#editar").hide();
			$("#modalAlertaEditar")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El dia no laborable fue creado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#modalAlertaEditar").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#modalAlertaEditar")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al crear el dia no laborable. '
					+ data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlertaEditar").hide();
			}, 5000);
		}
	});
}

function buscarDiaNoLaboral() {
	$.ajax({
		url: '/api/dianolaborable/buscardia/' + $("#modalEditarId").val(),
		data: '',
		dataType: "json",
		type: "GET",
		success: function(data) {
			var fecha1 = data.fechaInicio;
			var fecha2 = data.fechaFin;
			fecha1 = fecha1.slice(0, 16);
			fecha2 = fecha2.slice(0, 16);
			var tipoEvento = enumATexto(data.tipoEvento);
			var tipoDia = enumATexto(data.tipoDiaNoLaborable);
			$("#editarTitulo").val(data.titulo);
			$("#modalEditarFechaInicio").val(fecha1);
			$("#modalEditarFechaFin").val(fecha2);
			$("#tipoEventoEditar").val(data.tipoEvento).trigger("change");
			$("#selectEventoEditar").val(tipoEvento).trigger("change");
			$("#tipoDiaNoLaborableEditar").val(data.tipoDiaNoLaborable).trigger("change");
			$("#selectTipoDia").val(tipoDia).trigger("change");

			if (tipoDia == "Federal") {
				$("#paisDivEditar").show();
				$("#provinciaDivEditar").hide();
				$("#paisEditar").val(data.mPais.id).trigger("change");
			} else {
				$("#paisDivEditar").show();
				$("#provinciaDivEditar").show();
				$("#paisEditar").val(data.mPais.id).trigger("change");
				traerProvincias(data.mPais.id, data.mProvincia.id);
			}

			$("#fechaInicioEditar").val(fecha1);
			$("#fechaFinEditar").val(fecha2);
		}
	});
}

function modificarDia(componente) {
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	buscarDiaNoLaboral();
	//window.location = '/administracion/dianolaborable/listado#editar';
}

function eliminarDia(componente) {
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/dianolaborable/eliminar/' + $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function() {
			$("#eliminar").hide();
			$("#modalConfirmar").hide();
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'El dia no laborable fue eliminado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
			window.location.href = window.location.origin + window.location.pathname + window.location.search;
		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h4><i class="icon fa fa-ban"></i> Error!</h4>'
					+ 'Ha ocurrido un error al eliminar el dia no laborable. '
					+ data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function traerProvincias(idPais, provincia) {
	$.ajax({
		url: '/api/dianolaborable/combo/' + idPais,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var selectList = "<option selected disabled value=''>Seleccione una provincia</option>";
			for (var x = 0; x < data.length; x++) {
				if (provincia == data[x].id) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}
			}
			$('#provincia').html(selectList);
			$('#provinciaEditar').html(selectList);
			$('#proviciaRegistro').html(selectList);
		}
	});
}

function convertirStringToEnumTE(string) {
	var textoEnum;
	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}
	$("#tipoEvento").val(string);
}

function convertirStringToEnumTE2(string) {
	var textoEnum;
	if (/\s/.test(string)) {
		textoEnum = string.toUpperCase();
		textoEnum = textoEnum.replace(/ /g, "_");
	} else {
		textoEnum = string.toUpperCase();
	}

	if (textoEnum == "FEDERAL") {
		$("#paisDiv").show();
		$("#provinciaDiv").hide();
	}

	if (textoEnum == "PROVINCIAL") {
		$("#paisDiv").show();
		$("#provinciaDiv").show();
	}

	$("#tipoDiaNoLaborable").val(textoEnum);
}

function enumATexto(string) {
	var tipoEvento = string.replace("_", " ");
	if (tipoEvento.includes(" ")) {
		tipoEvento = tipoEvento.replace(/\w\S*/g, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
		return tipoEvento;

	} else {
		tipoEvento = tipoEvento.replace(/\w\S*/g, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
		return tipoEvento;
	}
}