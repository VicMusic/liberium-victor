$(document).ready(function() {
	var resultado = $("#resultado").val();
	if (resultado) {
		$("#alerta").html(
			'<div class="alert alert-info alert-dismissible">'
			+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
			+ '<h4><i class="icon fa fa-exclamation"></i> Info!</h4>'
			+ 'No se encontraron resultados con la búsqueda realizada. '
			+ '</div>').show();
	}
});

function guardarJuzgado() {
	if ($("#modalEditarNombre").val().length < 2) {
		$("#modalAlerta")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
				+ 'Debe ingresar un nombre.'
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlerta").hide();
		}, 4000);
		return 0;
	}

	if ($("#tipoFuero").val() == null) {
		$("#modalAlerta")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
				+ 'Debe seleccionar un fuero.'
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlerta").hide();
		}, 4000);
		return 0;
	}

	if ($("#instancia").val() == null) {
		$("#modalAlerta")
			.empty()
			.html(
				'<div class="alert alert-danger alert-dismissible">'
				+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
				+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
				+ 'Debe seleccionar una instancia.'
				+ '</div>').show();
		setTimeout(function() {
			$("#modalAlerta").hide();
		}, 4000);
		return 0;
	}

	$.ajax({
		url: '/api/juzgado/guardar',
		data: $("#modalFormulario").serialize(),
		dataType: "json",
		type: "POST",
		success: function(data) {
			$("#agregar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La dependencia fue creada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 4000);

			window.location.href = window.location.origin + window.location.pathname + window.location.search;

		},
		error: function(data) {
			$("#modalAlerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
					+ 'Ha ocurrido un error al crear la dependencia. ' + data.responseJSON.message
					+ '</div>').show();
			setTimeout(function() {
				$("#modalAlerta").hide();
			}, 4000);
		}
	});
}

function modificarJuzgado(componente) {
	cambioFuero(componente.getAttribute('fuero'));
	$("#modalEditarId").val(componente.getAttribute('objeto'));
	$("#modalEditarNombre").val(componente.getAttribute('nombre'));
	$("#tipoFuero").val(componente.getAttribute('fuero')).trigger("change");
	$("#circunscripcion").val(componente.getAttribute('circunscripcion')).trigger("change");
	$("#instancia").val(componente.getAttribute('instancia')).trigger("change");
	$("#pais").val(componente.getAttribute('pais')).trigger("change");

	traerProvincias(componente.getAttribute('pais'), componente.getAttribute('provincia'));
	traerCircunscripciones(componente.getAttribute('provincia'), componente.getAttribute('circunscripcion'));
	//window.location = '/administracion/juzgado/listado#agregar';
}

function resetCampos() {
	$("#modalEditarId").val("");
	$("#modalEditarNombre").val("");
	$("#tipoFuero option[value='0']").attr("selected", true);
	$("#divCircunscripcion").hide();
	$("#divPais").hide();
	$("#divProvincia").hide();
}

function clonarJuzgado(componente) {
	$("#modalJuzgado").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalClonarId").val(componente.getAttribute('objeto'));
}

function clonar() {
	$.ajax({
		url: '/api/juzgado/clonar?id='
			+ $("#modalClonarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La dependencia fue clonada con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location.href = window.location.origin + window.location.pathname + window.location.search;

		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
					+ 'Ha ocurrido un error al clonar la dependencia. ' + data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function eliminarJuzgado(componente) {
	$("#modalJuzgado").modal("hide");
	$("#modalConfirmar").modal("toggle");
	$("#modalEliminarId").val(componente.getAttribute('objeto'));
}

function eliminar() {
	$.ajax({
		url: '/api/juzgado/eliminar?id='
			+ $("#modalEliminarId").val(),
		dataType: "json",
		type: "POST",
		async: true,
		success: function(data) {

			$("#eliminar").hide();
			$("#modalConfirmar").hide();

			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-info alert-dismissible">'
					+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
					+ '<h4><i class="icon fa fa-info"></i> Info!</h4>'
					+ 'La dependencia fue eliminado con éxito'
					+ '</div>').show();

			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);

			window.location.href = window.location.origin + window.location.pathname + window.location.search;

		},
		error: function(data) {
			$("#alerta")
				.empty()
				.html(
					'<div class="alert alert-danger alert-dismissible">'
					+ '<a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>'
					+ '<h3><i class="icon fa fa-ban"></i> Error!</h3>'
					+ 'Ha ocurrido un error al eliminar la dependencia. ' + data.responseJSON.message
					+ '</div>').show();
			$("#modalConfirmar").modal("hide");
			setTimeout(function() {
				$("#alerta").hide();
			}, 5000);
		}
	});
}

function traerProvincias(idPais, idProvincia) {
	console.log(idPais)
	$.ajax({
		url: '/api/contacto/combo1/' + idPais,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var selectList = "<option selected disabled value=''>Seleccione una provincia</option>";
			for (var x = 0; x < data.length; x++) {
				if (idProvincia == data[x].id) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">' + data[x].nombre + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].nombre + "</option>";
				}

			}
			if (idPais != null) {
				$("#pais").val(idPais);
			}
			$('#provincia').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function traerCircunscripciones(idProvincia, idCircunscripciones) {
	$.ajax({
		url: '/api/circunscripcion/combo?idProvincia=' + idProvincia,
		type: 'GET',
		contentType: "application/json",
		data: 'json',
		success: function(data) {
			var selectList = "<option selected disabled value=''>Seleccione una circunscripcion</option>";
			for (var x = 0; x < data.length; x++) {
				if (idCircunscripciones == data[x].id) {
					selectList.replace("selected", "");
					selectList += '<option selected value="' + data[x].id + '">' + data[x].value + "</option>";
				} else {
					selectList += '<option value="' + data[x].id + '">'
						+ data[x].value + "</option>";
				}

			}
			$('#circunscripcion').html(selectList);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function cambioFuero(fuero) {
	switch (fuero) {
		case "INTERNACIONAL":
			$("#divCircunscripcion").hide();
			$("#divPais").hide();
			$("#divProvincia").hide();
			break;
		case "FEDERAL":
			$("#divPais").show();
			$("#divProvincia").hide();
			$("#divCircunscripcion").hide();
			break;
		case "PROVINCIAL":
			$("#divCircunscripcion").show();
			$("#divPais").show();
			$("#divProvincia").show();
			break;
	}
}