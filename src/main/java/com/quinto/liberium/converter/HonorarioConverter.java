package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Honorario;
import com.quinto.liberium.model.MHonorario;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.HonorarioService;

@Component
public class HonorarioConverter extends Converter<MHonorario, Honorario>{

	@Autowired
	private CausaService causaService;
	
	@Autowired
	private EstudioService estudioService;

	@Autowired
	private HonorarioService honorarioService;
	
	public Honorario modeloEntidad(MHonorario modelo) {
		Honorario honorario = new Honorario();
		
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			honorario = honorarioService.buscarHonorarioPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, honorario);
			
			if (modelo.getmCausa() != null) {
				Causa causa = causaService.buscarPorId(modelo.getmCausa().getId());
				honorario.setCausa(causa);
			}
			
			if (modelo.getEstudio() != null) {
				Estudio estudio = estudioService.buscarPorIdEstudio(modelo.getEstudio().getId());
				honorario.setEstudio(estudio);
			}
			
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del honorario en entidad", e);
		}
		return honorario;
	}

	public MHonorario entidadModelo(Honorario honorario) {
		MHonorario modelo = new MHonorario();
		try {
			BeanUtils.copyProperties(honorario, modelo);
			
			if (honorario.getCausa() != null) {
				modelo.getmCausa().setId(honorario.getCausa().getId());
			}
			
			if (honorario.getEstudio() != null) {
				modelo.getEstudio().setId(honorario.getEstudio().getId());
			}
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de honorario", e);
		}
		return modelo;
	}

	public List<MHonorario> entidadesModelos(List<Honorario> honorarios) {
		List<MHonorario> modelos = new ArrayList<>();
		for (Honorario honorario: honorarios) {
			modelos.add(entidadModelo(honorario));
		}
		return modelos;
	}

	public List<Honorario> modelosEntidades(List<MHonorario> mHonorarios) {
		List<Honorario> entidades = new ArrayList<>();
		for (MHonorario mHonorario : mHonorarios) {
			entidades.add(modeloEntidad(mHonorario));
		}
		return entidades;
	}
}
