package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.model.MTipoCausa;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.TipoCausaService;

@Component
public class TipoCausaConverter extends Converter<MTipoCausa, TipoCausa> {
    
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@Autowired
	private MateriaService materiaService;

	public TipoCausa modeloEntidad(MTipoCausa modelo) {
	    TipoCausa tipoCausa = new TipoCausa();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			tipoCausa = tipoCausaService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, tipoCausa);
			
			Materia materia = materiaService.buscarMateriaPorId(modelo.getmMateria().getId());
			tipoCausa.setMateria(materia);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del tipo causa en entidad", e);
		}
		return tipoCausa;
	}

	public MTipoCausa entidadModelo(TipoCausa tipoCausa) {
		MTipoCausa modelo = new MTipoCausa();
		try {
			BeanUtils.copyProperties(tipoCausa, modelo);
			MMateria materia = materiaService.buscarMateriaModelPorId(tipoCausa.getMateria().getId());
			modelo.setmMateria(materia);
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de tipo causa", e);
		}
		return modelo;
	}

	public List<MTipoCausa> entidadesModelos(List<TipoCausa> tipoCausas) {
		List<MTipoCausa> modelos = new ArrayList<>();
		for (TipoCausa tipoCausa : tipoCausas) {
			modelos.add(entidadModelo(tipoCausa));
		}
		return modelos;
	}

	public List<TipoCausa> modelosEntidades(List<MTipoCausa> mTipoCausas) {
		List<TipoCausa> entidades = new ArrayList<>();
		for (MTipoCausa mTipoCausa : mTipoCausas) {
			entidades.add(modeloEntidad(mTipoCausa));
		}
		return entidades;
	}

}