package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.OficinaAdministrativa;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MOficinaAdministrativa;
import com.quinto.liberium.service.OficinaAdministrativaService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class OficinaAdministrativaConverter extends Converter<MOficinaAdministrativa, OficinaAdministrativa> {

	@Autowired
	private OficinaAdministrativaService oficinaService;

	@Autowired
	private ProvinciaService provinciaService;

	public OficinaAdministrativa modeloEntidad(MOficinaAdministrativa modelo) {
		OficinaAdministrativa oficina = new OficinaAdministrativa();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			oficina = oficinaService.buscarOficinaAdministrativaPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, oficina);

			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			oficina.setProvincia(provincia);

		} catch (Exception e) {
			log.error("Error al convertir el modelo del Oficina Administrativa en entidad", e);
		}
		return oficina;
	}

	public MOficinaAdministrativa entidadModelo(OficinaAdministrativa oficina) {
		MOficinaAdministrativa modelo = new MOficinaAdministrativa();

		try {
			BeanUtils.copyProperties(oficina, modelo);

			modelo.getmProvincia().setId(oficina.getProvincia().getId());
			modelo.getmProvincia().setNombre(oficina.getProvincia().getNombre());

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de circunscripcion", e);
		}
		return modelo;
	}

	public List<MOficinaAdministrativa> entidadesModelos(List<OficinaAdministrativa> oficinaAdministrativas) {
		List<MOficinaAdministrativa> modelos = new ArrayList<>();
		for (OficinaAdministrativa oficinaAdministrativa : oficinaAdministrativas) {
			modelos.add(entidadModelo(oficinaAdministrativa));
		}
		return modelos;
	}

	public List<OficinaAdministrativa> modelosEntidades(List<MOficinaAdministrativa> mOficinaAdministrativas) {
		List<OficinaAdministrativa> entidades = new ArrayList<>();
		for (MOficinaAdministrativa mOficinaAdministrativa : mOficinaAdministrativas) {
			entidades.add(modeloEntidad(mOficinaAdministrativa));
		}
		return entidades;
	}

}
