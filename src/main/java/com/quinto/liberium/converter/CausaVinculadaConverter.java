package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.CausaVinculada;
import com.quinto.liberium.model.MCausaVinculada;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.CausaVinculadaService;

@Component
public class CausaVinculadaConverter extends Converter<MCausaVinculada, CausaVinculada> {

	@Autowired
	private CausaVinculadaService causaVinculadaService;
	
	@Autowired
	private CausaService causaService;

	public CausaVinculada modeloEntidad(MCausaVinculada modelo) {
		CausaVinculada causaVinculada = new CausaVinculada();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			causaVinculada = causaVinculadaService.buscarPorId(modelo.getId());
		}
		try {
			
			Causa causa = causaService.buscarPorId(modelo.getmCausa().getId());
			causaVinculada.setCausa(causa);
			
			Causa vinculada = causaService.buscarPorId(modelo.getmVinculada().getId());
			causaVinculada.setVinculada(vinculada);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo de la causa vinculada en entidad", e);
		}
		return causaVinculada;
	}

	public MCausaVinculada entidadModelo(CausaVinculada causa) {
		MCausaVinculada modelo = new MCausaVinculada();
		try {
			modelo.getmCausa().setId(causa.getCausa().getId());
			modelo.getmVinculada().setId(causa.getVinculada().getId());
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de causa vinculada", e);
		}
		return modelo;
	}

	public List<MCausaVinculada> entidadesModelos(List<CausaVinculada> causasVinculadas) {
		List<MCausaVinculada> modelos = new ArrayList<>();
		for (CausaVinculada causaVincula : causasVinculadas) {
			modelos.add(entidadModelo(causaVincula));
		}
		return modelos;
	}

	public List<CausaVinculada> modelosEntidades(List<MCausaVinculada> mCausaVinculadas) {
		List<CausaVinculada> entidades = new ArrayList<>();
		for (MCausaVinculada mCausaVinculada : mCausaVinculadas) {
			entidades.add(modeloEntidad(mCausaVinculada));
		}
		return entidades;
	}

}