package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.model.MChecklist;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.TipoCausaService;

@Component
public class ChecklistConverter extends Converter<MChecklist, Checklist>{

	@Autowired
	private ChecklistService checklistService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private CircunscripcionService circunscripcionService;
	
	@Autowired
	private InstanciaService instanciaService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	public Checklist modeloEntidad(MChecklist modelo) {
		Checklist checkList = new Checklist();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			checkList = checklistService.buscarCheckListPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, checkList);

			Pais pais = paisService.buscarPaisPorId(modelo.getmPais().getId());
			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			Circunscripcion circunscripcion = circunscripcionService.buscarCircunscripcionPorId(modelo.getmCircunscripcion().getId());
			Instancia instancia = instanciaService.buscarInstanciaPorId(modelo.getmInstancia().getId());
			TipoCausa tipoCausa = tipoCausaService.buscarPorId(modelo.getmTipoCausa().getId());
			
			checkList.setPais(pais);
			checkList.setProvincia(provincia);
			checkList.setTipoCausa(tipoCausa);
			checkList.setCircunscripcion(circunscripcion);
			checkList.setInstancia(instancia);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del checklist en entidad", e);
		}
		return checkList;

	}

	public MChecklist entidadModelo(Checklist checkList) {
		MChecklist modelo = new MChecklist();
		try {
			BeanUtils.copyProperties(checkList, modelo);

			if(checkList.getPais() != null) {
				modelo.getmPais().setId(checkList.getPais().getId());
				modelo.getmPais().setNombre(checkList.getNombre());	
			}
			
			if(checkList.getProvincia() != null) {
				modelo.getmProvincia().setId(checkList.getProvincia().getId());
				modelo.getmProvincia().setNombre(checkList.getProvincia().getNombre());	
			}
			
			if(checkList.getTipoCausa() != null) {
				modelo.getmTipoCausa().setId(checkList.getTipoCausa().getId());
				modelo.getmTipoCausa().setNombre(checkList.getTipoCausa().getNombre());	
			}
			
			if(checkList.getCircunscripcion() != null) {
				modelo.getmCircunscripcion().setId(checkList.getCircunscripcion().getId());
				modelo.getmCircunscripcion().setNombre(checkList.getCircunscripcion().getNombre());	
			}
			
			if(checkList.getInstancia() != null) {
				modelo.getmInstancia().setId(checkList.getInstancia().getId());
				modelo.getmInstancia().setNombre(checkList.getInstancia().getNombre());	
			}
			
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de sub item", e);
		}
		return modelo;
	}

	public List<MChecklist> entidadesModelos(List<Checklist> checkLists) {
		List<MChecklist> modelos = new ArrayList<>();
		for (Checklist checkList : checkLists) {
			modelos.add(entidadModelo(checkList));
		}
		return modelos;
	}

	public List<Checklist> modelosEntidades(List<MChecklist> mChecklists) {
		List<Checklist> entidades = new ArrayList<>();
		for (MChecklist mChecklist : mChecklists) {
			entidades.add(modeloEntidad(mChecklist));
		}
		return entidades;
	}
}
