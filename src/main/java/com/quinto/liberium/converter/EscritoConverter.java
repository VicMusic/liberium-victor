package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.model.MEscrito;
import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.TipoCausaService;

@Component
public class EscritoConverter extends Converter<MEscrito, Escrito> {

	@Autowired
	private EscritoService escritoService;

	@Autowired
	private TipoCausaService tipoCausaService;
	
	public Escrito modeloEntidad(MEscrito modelo) {
		Escrito escrito = new Escrito();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			escrito = escritoService.buscarEscritoPorId(modelo.getId());
		}

		try {
			BeanUtils.copyProperties(modelo, escrito);
			
			if (modelo.getmTipoCausa() != null) {
				escrito.setTipoCausa(tipoCausaService.buscarPorId(modelo.getmTipoCausa().getId()));
			}
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de Escrito", e);
		}
		return escrito;
	}

	public MEscrito entidadModelo(Escrito escrito) {
		MEscrito modelo = new MEscrito();
		if(escrito == null){
			escrito = new Escrito();
		}
		try {
		    BeanUtils.copyProperties(modelo, escrito);
		    if (escrito.getTipoCausa() != null) {
				modelo.getmTipoCausa().setId(escrito.getTipoCausa().getId());
				modelo.getmTipoCausa().setNombre(escrito.getTipoCausa().getNombre());
			}
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de Escrito", e);
		}
		return modelo;
    }
	
	public List<MEscrito> entidadesModelos(List<Escrito> escritos){
		List<MEscrito> modelos = new ArrayList<MEscrito>();
		for (Escrito escrito : escritos) {
			modelos.add(entidadModelo(escrito));
		}
		return modelos;
	}
	
	public List<Escrito> modelosEntidades(List<MEscrito> modelos){
		List<Escrito> escritos = new ArrayList<Escrito>();
		for (MEscrito modelo : modelos) {
			escritos.add(modeloEntidad(modelo));
		}
		return escritos;
	}

}
