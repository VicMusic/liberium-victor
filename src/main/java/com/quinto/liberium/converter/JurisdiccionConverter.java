package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Jurisdiccion;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MJurisdiccion;
import com.quinto.liberium.service.JurisdiccionService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class JurisdiccionConverter extends Converter<MJurisdiccion, Jurisdiccion> {

	@Autowired
	private JurisdiccionService  jurisdiccionService;
	@Autowired
	private ProvinciaService provinciaService;
	
	public Jurisdiccion modeloEntidad(MJurisdiccion modelo) {
		Jurisdiccion jurisdiccion = new Jurisdiccion();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			jurisdiccion = jurisdiccionService.buscarJurisdiccionPorId(modelo.getId());
		}
		try {
		    BeanUtils.copyProperties(modelo, jurisdiccion);
		    
		    Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
		    jurisdiccion.setProvincia(provincia);
		    
		} catch (Exception e) {
		    log.error("Error al convertir el modelo del circunscripcion en entidad", e);
		}
		return jurisdiccion;
	    }

	    public MJurisdiccion entidadModelo(Jurisdiccion jurisdiccion) {
		MJurisdiccion modelo = new MJurisdiccion();
		try {
		    BeanUtils.copyProperties(jurisdiccion, modelo);
		    
		    modelo.getmProvincia().setId(jurisdiccion.getProvincia().getId());
		    modelo.getmProvincia().setNombre(jurisdiccion.getProvincia().getNombre());
		    
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de circunscripcion", e);
		}
		return modelo;
	    }

	    public List<MJurisdiccion> entidadesModelos(List<Jurisdiccion> jurisdiccions) {
		List<MJurisdiccion> modelos = new ArrayList<>();
		for (Jurisdiccion jurisdiccion : jurisdiccions) {
		    modelos.add(entidadModelo(jurisdiccion));
		}
		return modelos;
	    }

	    public List<Jurisdiccion> modelosEntidades(List<MJurisdiccion> mJurisdiccions) {
		List<Jurisdiccion> entidades = new ArrayList<>();
		for (MJurisdiccion mJurisdiccion : mJurisdiccions) {
		    entidades.add(modeloEntidad(mJurisdiccion));
		}
		return entidades;
	    }
}
