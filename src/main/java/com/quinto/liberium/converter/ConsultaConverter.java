package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MFormulario;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.TipoCausaService;

@Component
public class ConsultaConverter extends Converter<MConsulta, Consulta> {

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private LocalidadService localidadService;
	
	@Autowired
	private FormularioService formularioService;
	
	public Consulta modeloEntidad(MConsulta modelo) {
		Consulta consulta = new Consulta();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			consulta = consultaService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, consulta);

			TipoCausa tipoCausa = tipoCausaService.buscarPorId(modelo.getmTipoCausa().getId());
			consulta.setTipoCausa(tipoCausa);
			Localidad localidad = localidadService.buscarPorId(modelo.getmLocalidad().getId());
			consulta.setLocalidad(localidad);
			

		} catch (Exception e) {
			log.error("Error al convertir el modelo de la consulta en entidad", e);
		}
		return consulta;
	}

	public MConsulta entidadModelo(Consulta consulta) {
		MConsulta modelo = new MConsulta();
		try {
			BeanUtils.copyProperties(consulta, modelo);

			modelo.getmTipoCausa().setId(consulta.getTipoCausa().getId());
			modelo.getmTipoCausa().setNombre(consulta.getTipoCausa().getNombre());
			modelo.getmLocalidad().setId(consulta.getLocalidad().getId());
			modelo.getmLocalidad().setNombre(consulta.getLocalidad().getNombre());
			modelo.getmFormulario().setId(formularioService.buscarMFormularioPorTipoDeCausa(consulta.getTipoCausa().getId()).getId());

			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de Consulta", e);
		}
		return modelo;
	}

	public List<MConsulta> entidadesModelos(List<Consulta> consultas) {
		List<MConsulta> modelos = new ArrayList<>();
		for (Consulta consulta : consultas) {
			modelos.add(entidadModelo(consulta));
		}
		return modelos;
	}

	public List<Consulta> modelosEntidades(List<MConsulta> modelos) {
		List<Consulta> entidades = new ArrayList<>();
		for (MConsulta modelo : modelos) {
			entidades.add(modeloEntidad(modelo));
		}
		return entidades;
	}
}
