package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Notas;
import com.quinto.liberium.model.MNotas;

@Component
public class NotasConverter  extends Converter<MNotas, Notas> {
	
	public Notas modeloEntidad(MNotas modelo) {
		Notas notas = new Notas();
		try {
			BeanUtils.copyProperties(modelo, notas);
		} catch (Exception e) {
			log.error("Error al convertir el modelo de la nota en entidad", e);
		}
		return notas;
	}

	public MNotas entidadModelo(Notas nota) {
		MNotas modelo = new MNotas();
		try {
			BeanUtils.copyProperties(nota, modelo);
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de nota", e);
		}
		return modelo;
	}
	
	public List<MNotas> entidadesModelos(List<Notas> notas) {
		List<MNotas> modelos = new ArrayList<>();
		for (Notas nota : notas) {
			modelos.add(entidadModelo(nota));
		}
		return modelos;
	}

	public List<Notas> modelosEntidades(List<MNotas> mNotas) {
		List<Notas> entidades = new ArrayList<>();
		for (MNotas mNota : mNotas) {
			entidades.add(modeloEntidad(mNota));
		}
		return entidades;
	}
}
