package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Respuesta;
import com.quinto.liberium.model.RespuestaM;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.RespuestaService;

@Component
public class RespuestaConverter extends Converter<RespuestaM, Respuesta> {

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private RespuestaService respuestaService;

	public Respuesta modeloEntidad(RespuestaM modelo) {
		Respuesta respuesta = new Respuesta();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			respuesta = respuestaService.buscarRespuestaPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, respuesta);

			Consulta consulta = consultaService.buscarPorId(modelo.getConsulta().getId());
			respuesta.setConsulta(consulta);

		} catch (Exception e) {
			log.error("Error al convertir el modelo del respuesta en entidad", e);
		}
		return respuesta;
	}

	public RespuestaM entidadModelo(Respuesta respuesta) {
		RespuestaM modelo = new RespuestaM();
		try {
			BeanUtils.copyProperties(respuesta, modelo);

			modelo.getConsulta().setId(respuesta.getConsulta().getId());

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de respuesta", e);
		}
		return modelo;
	}

	public List<RespuestaM> entidadesModelos(List<Respuesta> respuestas) {
		List<RespuestaM> modelos = new ArrayList<>();
		for (Respuesta respuesta : respuestas) {
			modelos.add(entidadModelo(respuesta));
		}
		return modelos;
	}

	public List<Respuesta> modelosEntidades(List<RespuestaM> mRespuestas) {
		List<Respuesta> entidades = new ArrayList<>();
		for (RespuestaM mRespuesta : mRespuestas) {
			entidades.add(modeloEntidad(mRespuesta));
		}
		return entidades;
	}

}
