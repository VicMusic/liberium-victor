package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Opcion;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.enumeration.TipoPregunta;
import com.quinto.liberium.model.MPregunta;
import com.quinto.liberium.repository.OpcionRepository;
import com.quinto.liberium.repository.PreguntaRepository;

@Component
public class PreguntaConverter extends Converter<MPregunta, Pregunta> {

	
	@Autowired
	private PreguntaRepository preguntaRepository;
	
	@Autowired
	private OpcionRepository opcionRepository;

	public Collection<Pregunta> convertir(Map<String,String[]> mapa){
		HashMap<String, Pregunta> preguntas = new HashMap<>();

		for(String key : mapa.keySet()) {
			if(key.contains("ABIERTA") || key.contains("CERRADA")) {
				String[] datos = key.split("-");
				
				Pregunta pregunta = null;
				if(preguntas.containsKey(datos[0])) {
					pregunta = preguntas.get(datos[0]);
				} else {
					pregunta = new Pregunta();
					preguntas.put(datos[0], pregunta);
				}

				pregunta.setCreado(new Date());
				pregunta.setOrden(new Integer(datos[0]));
				pregunta.setTipo(TipoPregunta.valueOf(datos[1]));
				pregunta.setTitulo(mapa.get(key)[0]);
			} else if(key.contains("OPCION")){
				String[] datos = key.split("-");
				
				Pregunta pregunta = null;
				if(preguntas.containsKey(datos[0])) {
					pregunta = preguntas.get(datos[0]);
				} else {
					pregunta = new Pregunta();
					preguntas.put(datos[0], pregunta);
				}
				
				for(String titulo : mapa.get(key)) {
					Opcion opcion = new Opcion();
					opcion.setTitulo(titulo);

					pregunta.getOpciones().add(opcion);
				}
			} else if(key.contains("TIPOVAIABLE")) {
				String[] datos = key.split("-");
				
				Pregunta pregunta = null;
				if(preguntas.containsKey(datos[0])) {
					pregunta = preguntas.get(datos[0]);
				} else {
					pregunta = new Pregunta();
					preguntas.put(datos[0], pregunta);
				}
				pregunta.setVariableAmigable(mapa.get(key)[0]);
				String valueVariable = convert(mapa.get(key)[0]);
				pregunta.setVariableValue("@" + valueVariable);
			}
		}
		
		return preguntas.values();
	}
	
	public Pregunta setTipoVariable(Pregunta pregunta) {
		
		return pregunta;
		
	}
	
//	public Collection<Pregunta> convertirConId(Map<String,String[]> mapa,String[] ids,String[] idsOpciones){
//		HashMap<String, Pregunta> preguntas = new HashMap<>();
//		int x = 0;
//		int z = 0;
//		for(String key : mapa.keySet()) {
//			if(key.contains("ABIERTA") || key.contains("CERRADA")) {
//				String[] datos = key.split("-");
//				
//				Pregunta pregunta = null;
//				if(preguntas.containsKey(datos[0])) {
//					pregunta = preguntas.get(datos[0]);
//				} else {
//					pregunta = new Pregunta();
//					preguntas.put(datos[0], pregunta);
//				}
//				
//                pregunta.setId(ids[x]);
//				pregunta.setOrden(new Integer(datos[0]));
//				pregunta.setTipo(TipoPregunta.valueOf(datos[1]));
//				pregunta.setTitulo(mapa.get(key)[0]);
//				x++;
//			} else if(key.contains("OPCION")){
//				String[] datos = key.split("-");
//				
//				Pregunta pregunta = null;
//				if(preguntas.containsKey(datos[0])) {
//					pregunta = preguntas.get(datos[0]);
//				} else {
//					pregunta = preguntaRepository.buscarPorId(ids[x-1]);
//					preguntas.put(datos[0], pregunta);
//				}
//				for(String titulo : mapa.get(key)) {
//					Opcion opcion = new Opcion();
//					opcion.setId(idsOpciones[z]);
//					opcion.setTitulo(titulo);
//
//					pregunta.getOpciones().add(opcion);
//				}
//				z++;
//			}
//		}
//		
//		return preguntas.values();
//	}
	public List<Pregunta> convertirPregunta(List<String> idsPreguntas,List<String> textosPreguntas,List<String> tiposPreguntas,List<Integer> ordenes,List<String> idsOpciones,List<String> textosOpciones, List<String> cantOpciones) {
		MPregunta mPregunta = null;
		Pregunta pregunta = null;
		int e = 1;
		int x = 0;
		List<Pregunta> preguntas = new ArrayList<>();
		for(int i = 0 ; i < textosPreguntas.size() ; i++) {
			mPregunta = new MPregunta();
			pregunta = new Pregunta();
			
			if(e <= idsPreguntas.size()) {
				mPregunta.setId(idsPreguntas.get(i));
				mPregunta.setTitulo(textosPreguntas.get(i));
				mPregunta.setOrden(ordenes.get(i));
			}else {
				
				pregunta.setTitulo(textosPreguntas.get(i));
				pregunta.setOrden(ordenes.get(i));
			}
			switch(tiposPreguntas.get(i)) {
			
			case "ABIERTA":
				if(e > idsPreguntas.size()) {
					pregunta.setTipo(TipoPregunta.ABIERTA);
					preguntaRepository.save(pregunta);
				}else {
					mPregunta.setTipo(TipoPregunta.ABIERTA);

				}
			break;
			case "CERRADA":
				Opcion opcion = null;
				List<Opcion> opciones = new ArrayList<>();
				int cantidad = Integer.parseInt(cantOpciones.get(x));
				
				
				
				if(e > idsPreguntas.size()) {
					pregunta.setTipo(TipoPregunta.CERRADA);
						
						for(int j = 0; j < cantidad; j++) {
							opcion = new Opcion();
								opcion.setTitulo(textosOpciones.get(j));
								opcionRepository.save(opcion);
							opciones.add(opcion);
							
						}
						pregunta.setOpciones(opciones);
						preguntaRepository.save(pregunta);

				}else {
					Pregunta p = preguntaRepository.buscarPorId(idsPreguntas.get(i));

					if(!p.getOpciones().isEmpty() && p.getOpciones() != null) {
						
						for(int j = 0; j < cantidad; j++) {
							if( j < p.getOpciones().size()) {
								opcion = opcionRepository.buscarPorId(idsOpciones.get(j));
							    opcion.setTitulo(textosOpciones.get(j));
								opciones.add(opcion);	
							}else {
							    opcion = new Opcion();
								opcion.setTitulo(textosOpciones.get(j));
								opcionRepository.save(opcion);
								opciones.add(opcion);
							}
							
						}
						mPregunta.setTipo(TipoPregunta.CERRADA);
						mPregunta.setOpciones(opciones);

					}else {
						for(int j = 0; j < cantidad; j++) {
							opcion = new Opcion();
						      opcion.setTitulo(textosOpciones.get(j));
						      opcionRepository.save(opcion);
							opciones.add(opcion);
						}
						mPregunta.setTipo(TipoPregunta.CERRADA);
						mPregunta.setOpciones(opciones);
					}
				}
				x++;
			break;
//			case "FECHA":
//				
//				if(e > idsPreguntas.size()) {
//					pregunta.setTipo(TipoPregunta.FECHA);
//					preguntaRepository.save(pregunta);
//				}else {
//					mPregunta.setTipo(TipoPregunta.FECHA);
//
//				}
//			break;	
			}
			
			
			if(e <= idsPreguntas.size()) {
				pregunta = modeloEntidad(mPregunta);

			}
			preguntas.add(pregunta);
			e++;
		}
		
		
		
		return preguntas;
	}
	
	public Pregunta modeloEntidad(MPregunta modelo) {
		Pregunta pregunta = new Pregunta();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			pregunta = preguntaRepository.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, pregunta);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del país en entidad.", e);
		}
		return pregunta;
	}

	public MPregunta entidadModelo(Pregunta pregunta) {
		MPregunta modelo = new MPregunta();
		try {
			BeanUtils.copyProperties(pregunta, modelo);
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de pregunta.", e);
		}
		return modelo;
	}

	public List<MPregunta> entidadesModelos(List<Pregunta> preguntaes) {
		List<MPregunta> modelos = new ArrayList<>();
		for (Pregunta pregunta : preguntaes) {
			modelos.add(entidadModelo(pregunta));
		}
		return modelos;
	}

	public List<Pregunta> modelosEntidades(List<MPregunta> mPreguntaes) {
		List<Pregunta> entidades = new ArrayList<>();
		for (MPregunta mPregunta : mPreguntaes) {
			entidades.add(modeloEntidad(mPregunta));
		}
		return entidades;
	}
	
    static String convert(String s) 
    { 
        int cnt= 0; 
        int n = s.length(); 
        char ch[] = s.toCharArray(); 
        int res_ind = 0; 
  
        for (int i = 0; i < n; i++)  
        { 
            if (ch[i] == ' ') 
            { 
                cnt++; 
                ch[i + 1] = Character.toUpperCase(ch[i + 1]); 
                continue; 
            } 
            else
                ch[res_ind++] = ch[i];  
        } 
        return String.valueOf(ch, 0, n - cnt); 
    } 
}