package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MCircunscripcion;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class CircunscripcionConverter extends Converter<MCircunscripcion, Circunscripcion> {

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private LocalidadService localidadService;

	public Circunscripcion modeloEntidad(MCircunscripcion modelo) {
		Circunscripcion circunscripcion = new Circunscripcion();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			circunscripcion = circunscripcionService.buscarCircunscripcionPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, circunscripcion);

			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			circunscripcion.setProvincia(provincia);

			if (modelo.getmLocalidades() != null) {
				List<Localidad> localidades = new ArrayList<>();
				for (int i = 0; i < modelo.getmLocalidades().size(); i++) {
					Localidad localidad = localidadService.buscarPorId(modelo.getmLocalidades().get(i).getId());
					localidades.add(localidad);
				}
				circunscripcion.setLocalidades(localidades);
			}

		} catch (Exception e) {
			log.error("Error al convertir el modelo del circunscripcion en entidad", e);
		}
		return circunscripcion;
	}

	public MCircunscripcion entidadModelo(Circunscripcion circunscripcion) {
		MCircunscripcion modelo = new MCircunscripcion();
		MLocalidad mLocalidad = new MLocalidad();
		List<MLocalidad> mLocalidades = new ArrayList<>();
		try {
			BeanUtils.copyProperties(circunscripcion, modelo);

			modelo.getmProvincia().setId(circunscripcion.getProvincia().getId());
			modelo.getmProvincia().setNombre(circunscripcion.getProvincia().getNombre());
			
			for(int i = 0; i <circunscripcion.getLocalidades().size(); i++) {
				mLocalidad = new MLocalidad();
				mLocalidad.setId(circunscripcion.getLocalidades().get(i).getId());
				mLocalidad.setNombre(circunscripcion.getLocalidades().get(i).getNombre());
				mLocalidades.add(mLocalidad);
			}
			modelo.setmLocalidades(mLocalidades);

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de circunscripcion", e);
		}
		return modelo;
	}

	public List<MCircunscripcion> entidadesModelos(List<Circunscripcion> circunscripcions) {
		List<MCircunscripcion> modelos = new ArrayList<>();
		for (Circunscripcion circunscripcion : circunscripcions) {
			modelos.add(entidadModelo(circunscripcion));
		}
		return modelos;
	}

	public List<Circunscripcion> modelosEntidades(List<MCircunscripcion> mCircunscripcions) {
		List<Circunscripcion> entidades = new ArrayList<>();
		for (MCircunscripcion mCircunscripcion : mCircunscripcions) {
			entidades.add(modeloEntidad(mCircunscripcion));
		}
		return entidades;
	}

}