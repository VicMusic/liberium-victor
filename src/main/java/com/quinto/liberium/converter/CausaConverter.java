package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Juzgado;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.JuzgadoService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.TipoCausaService;
import com.quinto.liberium.service.UsuarioService;

@Component
public class CausaConverter extends Converter<MCausa, Causa> {

	@Autowired
	private CausaService causaService;
	
	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private InstanciaService instanciaService;
	
	@Autowired
	private ConsultaService consultaService;
	
	@Autowired
	private ItemConverter itemConverter;

	@Autowired
	private JuzgadoService juzgadoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private MiembroConverter miembroConverter;

	public Causa modeloEntidad(MCausa modelo) {
		Causa causa = new Causa();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			causa = causaService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, causa);
			TipoCausa tipoCausa = tipoCausaService.buscarPorId(modelo.getmTipoCausa().getId());
			Instancia instancia = instanciaService.buscarInstanciaPorId(modelo.getmInstancia().getId());
			Consulta consulta = consultaService.buscarPorId(modelo.getmConsulta().getId());
			List<Item> items = itemConverter.modelosEntidades(modelo.getmItem());
			Juzgado juzgado = juzgadoService.buscarJuzgadoPorId(modelo.getJuzgadoOficina().getId());
			Usuario creador = usuarioService.buscarPorId(modelo.getCreador().getId());
			Pais pais = paisService.buscarPaisPorId(modelo.getPais().getId());
			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getProvincia().getId());
			Estudio estudio = estudioService.buscarPorIdEstudio(modelo.getmEstudio().getId());
			Set<Miembro> miembros = miembroConverter.modelosEntidadesSet(modelo.getmMiembro());
			
			causa.setMiembrosCausa(miembros);
			causa.setEstudio(estudio);
			causa.setPais(pais);
			causa.setProvincia(provincia);
			causa.setCreador(creador);
			causa.setItems(items);
			causa.setConsulta(consulta);
			causa.setTipoCausa(tipoCausa);
			causa.setInstancia(instancia);
			causa.setJuzgadoOficina(juzgado);
		} catch (Exception e) {
			log.error("Error al convertir el modelo de la causa en entidad", e);
		}
		return causa;
	}

	public MCausa entidadModelo(Causa causa) {
		MCausa modelo = new MCausa();
		try {
			BeanUtils.copyProperties(causa, modelo);
			if(causa.getConsulta() != null) {
				modelo.getmConsulta().setId(causa.getConsulta().getId());	
			}
			if(causa.getMiembrosCausa() != null) {
				modelo.setmMiembro(miembroConverter.entidadesModelosSet(causa.getMiembrosCausa()));	
			}
			if(causa.getJuzgadoOficina() != null) {
				modelo.getJuzgadoOficina().setId(causa.getJuzgadoOficina().getId());
				modelo.getJuzgadoOficina().setNombre(causa.getJuzgadoOficina().getNombre());	
			}
			if(causa.getPais() != null) {
				modelo.getPais().setId(causa.getPais().getId());
				modelo.getPais().setNombre(causa.getPais().getNombre());
			}
			if(causa.getProvincia() != null) {
				modelo.getProvincia().setId(causa.getProvincia().getId());
				modelo.getProvincia().setNombre(causa.getProvincia().getNombre());
			}
			if(causa.getTipoCausa() != null) {
				modelo.getmTipoCausa().setId(causa.getTipoCausa().getId());
				modelo.getmTipoCausa().setNombre(causa.getTipoCausa().getNombre());
			}
			if(causa.getInstancia() != null) {
				modelo.getmInstancia().setId(causa.getInstancia().getId());
				modelo.getmInstancia().setNombre(causa.getInstancia().getNombre());
			}

			if(causa.getEstudio() != null) {
				modelo.getmEstudio().setId(causa.getEstudio().getId());
				modelo.getmEstudio().setNombre(causa.getEstudio().getNombre());
			}

			modelo.setmItem(itemConverter.entidadesModelos(causa.getItems()));
			
			
			modelo.getCreador().setId(causa.getCreador().getId());
			modelo.getCreador().setNombre(causa.getCreador().getNombre());
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de causa", e);
		}
		return modelo;
	}

	public List<MCausa> entidadesModelos(List<Causa> causas) {
		List<MCausa> modelos = new ArrayList<>();
		for (Causa causa : causas) {
			modelos.add(entidadModelo(causa));
		}
		return modelos;
	}

	public List<Causa> modelosEntidades(List<MCausa> mCausas) {
		List<Causa> entidades = new ArrayList<>();
		for (MCausa mCausa : mCausas) {
			entidades.add(modeloEntidad(mCausa));
		}
		return entidades;
	}

}