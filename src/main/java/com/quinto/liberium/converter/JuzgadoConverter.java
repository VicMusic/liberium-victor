package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Juzgado;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.repository.JuzgadoRepository;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.TipoCausaService;

@Component
public class JuzgadoConverter extends Converter<MJuzgado, Juzgado> {

	@Autowired
	private JuzgadoRepository juzgadoRepository;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private ProvinciaService provinciaService;

	public Juzgado modeloEntidad(MJuzgado modelo) {
		Juzgado juzgado = new Juzgado();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			juzgado = juzgadoRepository.buscarPorId(modelo.getId());
		}
		try {

			BeanUtils.copyProperties(modelo, juzgado);

			Instancia instancia = instanciaService.buscarInstanciaPorId(modelo.getmInstancia().getId());
			juzgado.setInstancia(instancia);

			Circunscripcion circunscripcion = circunscripcionService
					.buscarCircunscripcionPorId(modelo.getCircunscripcion().getId());
			juzgado.setCircunscripcion(circunscripcion);

			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getProvincia().getId());
			juzgado.setProvincia(provincia);

		} catch (Exception e) {
			log.error("Error al convertir el modelo del juzgado en entidad", e);
		}
		return juzgado;
	}

	public MJuzgado entidadModelo(Juzgado juzgado) {
		MJuzgado modelo = new MJuzgado();
		try {
			BeanUtils.copyProperties(juzgado, modelo);
			
			if (juzgado.getInstancia() != null && juzgado.getInstancia().getId() != null) {
				modelo.getmInstancia().setId(juzgado.getInstancia().getId());
				modelo.getmInstancia().setNombre(juzgado.getInstancia().getNombre());
			}

			if (juzgado.getCircunscripcion() != null && juzgado.getCircunscripcion().getId() != null) {
				modelo.getCircunscripcion().setId(juzgado.getCircunscripcion().getId());
				modelo.getCircunscripcion().setNombre(juzgado.getCircunscripcion().getNombre());
			}
			if (juzgado.getPais() != null && juzgado.getPais().getId() != null) {
				modelo.getPais().setId(juzgado.getPais().getId());
				modelo.getPais().setNombre(juzgado.getPais().getNombre());
			}
			if (juzgado.getProvincia() != null && juzgado.getProvincia().getId() != null) {
				modelo.getProvincia().setId(juzgado.getProvincia().getId());
				modelo.getProvincia().setNombre(juzgado.getProvincia().getNombre());
			}

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de juzgado", e);
		}
		return modelo;
	}

	public List<MJuzgado> entidadesModelos(List<Juzgado> juzgados) {
		List<MJuzgado> modelos = new ArrayList<>();
		for (Juzgado juzgado : juzgados) {
			modelos.add(entidadModelo(juzgado));
		}
		return modelos;
	}

	public List<Juzgado> modelosEntidades(List<MJuzgado> mJuzgados) {
		List<Juzgado> entidades = new ArrayList<>();
		for (MJuzgado mJuzgado : mJuzgados) {
			entidades.add(modeloEntidad(mJuzgado));
		}
		return entidades;
	}

}
