package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.model.MArchivo;
import com.quinto.liberium.service.ArchivoService;

@Component
public class ArchivoConverter extends Converter<MArchivo, Archivo> {
	
	private final Log LOG = LogFactory.getLog(ArchivoConverter.class);
	@Autowired
	private ArchivoService archivoService;
	
	@Override
	public Archivo modeloEntidad(MArchivo modelo) {
		Archivo entidad = new Archivo();
		if(modelo !=  null && !modelo.getId().isEmpty()) {
			entidad = archivoService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, entidad);
			
		} catch (Exception e) {
			LOG.error("Error al querer convertir en modelo de Archivo en Entidad", e);
		}
		return null;
	}

	@Override
	public MArchivo entidadModelo(Archivo entidad) {
		MArchivo modelo = new MArchivo();
		try {
		    BeanUtils.copyProperties(entidad, modelo);
		    		    		    
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de contable", e);
		}
		return modelo;
	    }
	
	public List<Archivo> modelosEntidades(List<MArchivo> modelos){
		List<Archivo> entidades = new ArrayList<Archivo>();
		for (MArchivo modelo  : modelos) {
			entidades.add(modeloEntidad(modelo)) ;
		}
		return entidades;
	}
	public List<MArchivo> entidadesModelos(List<Archivo> entidades){
		List<MArchivo> modelos = new ArrayList<>();
		for (Archivo entidad : entidades) {
		    modelos.add(entidadModelo(entidad));
		}
		return modelos;
	}

}
