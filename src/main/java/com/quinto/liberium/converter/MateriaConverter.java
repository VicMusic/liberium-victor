package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.service.MateriaService;

@Component
public class MateriaConverter extends Converter<MMateria, Materia> {

	@Autowired
	private MateriaService materiaService;

	public Materia modeloEntidad(MMateria modelo) {
		Materia materia = new Materia();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			materia = materiaService.buscarMateriaPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, materia);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del materia en entidad", e);
		}
		return materia;
	}

	public MMateria entidadModelo(Materia materia) {
		MMateria modelo = new MMateria();
		try {
			BeanUtils.copyProperties(materia, modelo);
		} catch (Exception e) {
			log.info("Error al convertir la entidad en el modelo de materia", e);
		}
		return modelo;
	}

	public List<MMateria> entidadesModelos(List<Materia> materias) {
		List<MMateria> modelos = new ArrayList<>();
		for (Materia materia : materias) {
			modelos.add(entidadModelo(materia));
		}
		return modelos;
	}

	public List<Materia> modelosEntidades(List<MMateria> mMaterias) {
		List<Materia> entidades = new ArrayList<>();
		for (MMateria mMateria : mMaterias) {
			entidades.add(modeloEntidad(mMateria));
		}
		return entidades;
	}

}