package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.model.MInstancia;
import com.quinto.liberium.service.InstanciaService;

@Component
public class InstanciaConverter extends Converter<MInstancia, Instancia> {

	@Autowired
	private InstanciaService instanciaService;

	public Instancia modeloEntidad(MInstancia modelo) {
		Instancia instancia = new Instancia();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			instancia = instanciaService.buscarInstanciaPorId(modelo.getId());
		}
		try {

			BeanUtils.copyProperties(modelo, instancia);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del instancia en entidad", e);
		}
		return instancia;
	}

	public MInstancia entidadModelo(Instancia instancia) {
		MInstancia modelo = new MInstancia();
		try {
			BeanUtils.copyProperties(instancia, modelo);
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de instancia", e);
		}
		return modelo;
	}

	public List<MInstancia> entidadesModelos(List<Instancia> instancias) {
		List<MInstancia> modelos = new ArrayList<>();
		for (Instancia instancia : instancias) {
			modelos.add(entidadModelo(instancia));
		}
		return modelos;
	}

	public List<Instancia> modelosEntidades(List<MInstancia> mInstancias) {
		List<Instancia> entidades = new ArrayList<>();
		for (MInstancia mInstancia : mInstancias) {
			entidades.add(modeloEntidad(mInstancia));
		}
		return entidades;
	}

}