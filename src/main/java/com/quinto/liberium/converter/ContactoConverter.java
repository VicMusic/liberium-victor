package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MContacto;
import com.quinto.liberium.service.ContactoService;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class ContactoConverter extends Converter<MContacto, Contacto> {

	@Autowired
	private ContactoService contactoService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private LocalidadService localidadService;

	public Contacto modeloEntidad(MContacto modelo) {
		Contacto contacto = new Contacto();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			contacto = contactoService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, contacto);
			
			Pais pais = paisService.buscarPaisPorId(modelo.getmPais().getId());
			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			Localidad localidad = localidadService.buscarPorId(modelo.getmLocalidad().getId());
			
			contacto.setPais(pais);
			contacto.setProvincia(provincia);
			contacto.setLocalidad(localidad);
			

		} catch (Exception e) {
			log.error("Error al convertir el modelo de contacto en entidad", e);
		}
		return contacto;
	}

	public MContacto entidadModelo(Contacto contacto) {
		MContacto modelo = new MContacto();
		try {
			BeanUtils.copyProperties(contacto, modelo);
			
			if (contacto.getPais() != null) {
				modelo.getmPais().setId(contacto.getPais().getId());
				modelo.getmPais().setNombre(contacto.getPais().getNombre());
			}
			
			if (contacto.getProvincia() != null) {
				modelo.getmProvincia().setId(contacto.getProvincia().getId());
				modelo.getmProvincia().setNombre(contacto.getProvincia().getNombre());
			}
			
			if (contacto.getLocalidad() != null) {
				modelo.getmLocalidad().setId(contacto.getLocalidad().getId());			
				modelo.getmLocalidad().setNombre(contacto.getLocalidad().getNombre());
			}
		
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de contacto", e);
		}
		return modelo;
	}

	public List<MContacto> entidadesModelos(List<Contacto> contactos) {
		List<MContacto> modelos = new ArrayList<>();
		for (Contacto contacto : contactos) {
			modelos.add(entidadModelo(contacto));
		}
		return modelos;
	}

	public List<Contacto> modelosEntidades(List<MContacto> mContactos) {
		List<Contacto> entidades = new ArrayList<>();
		for (MContacto mContacto : mContactos) {
			entidades.add(modeloEntidad(mContacto));
		}
		return entidades;
	}

}