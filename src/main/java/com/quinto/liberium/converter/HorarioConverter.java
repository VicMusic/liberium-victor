package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Horario;
import com.quinto.liberium.model.MHorario;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.HorarioService;

@Component
public class HorarioConverter extends Converter<MHorario, Horario>{
	
	@Autowired
	private HorarioService horarioService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	public Horario modeloEntidad(MHorario modelo) {
		Horario horario = new Horario();
		
		if (modelo.getId() != null && modelo.getId().isEmpty()) {
			horario = horarioService.buscarHorarioPorId(modelo.getId());
			
			Abogado abogado = abogadoService.buscarAbogadoPorId(modelo.getmAbogado().getId());
			horario.setAbogado(abogado);
		}
		
		try {
			BeanUtils.copyProperties(modelo, horario);
		} catch (Exception e) {
			log.error("Error al convertir el modelo de horario en entidad.", e);
		}
		
		return horario;
	}
	
	public MHorario entidadModelo(Horario horario) {
		MHorario modelo = new MHorario();
		
		try {
			BeanUtils.copyProperties(horario, modelo);
			
			modelo.getmAbogado().setId(horario.getAbogado().getId());
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de horario", e);
		}
		return modelo;
	}
	public List<MHorario> entidadesModelos(List<Horario> horarios) {
		List<MHorario> modelos = new ArrayList<>();
		for (Horario horario : horarios) {
		    modelos.add(entidadModelo(horario));
		}
		return modelos;
	    }

	    public List<Horario> modelosEntidades(List<MHorario> mHorarios) {
		List<Horario> entidades = new ArrayList<>();
		for (MHorario mHorario : mHorarios) {
		    entidades.add(modeloEntidad(mHorario));
		}
		return entidades;
	    }
}
