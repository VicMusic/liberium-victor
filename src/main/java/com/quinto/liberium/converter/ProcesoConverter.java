package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Proceso;
import com.quinto.liberium.model.MProceso;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.ProcesoService;

@Component
public class ProcesoConverter extends Converter<MProceso, Proceso>{

	@Autowired 
	private ProcesoService procesoService;
	
	@Autowired
	private MateriaService materiaService;
	
	public Proceso modeloEntidad(MProceso modelo) {
		Proceso proceso = new Proceso();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			proceso = procesoService.buscarProcesoPorId(modelo.getId());
		}
		try {

			BeanUtils.copyProperties(modelo, proceso);

			Materia materia = materiaService.buscarMateriaPorId(modelo.getmMateria().getId());
			proceso.setMateria(materia);

		} catch (Exception e) {
			log.error("Error al convertir el modelo del proceso en entidad", e);
		}
		return proceso;
	}

	public MProceso entidadModelo(Proceso proceso) {
		MProceso modelo = new MProceso();
		try {
			BeanUtils.copyProperties(proceso, modelo);
			
			modelo.getmMateria().setId(proceso.getMateria().getId());
			modelo.getmMateria().setNombre(proceso.getMateria().getNombre());
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de proceso", e);
		}
		return modelo;
	}

	public List<MProceso> entidadesModelos(List<Proceso> instancias) {
		List<MProceso> modelos = new ArrayList<>();
		for (Proceso instancia : instancias) {
			modelos.add(entidadModelo(instancia));
		}
		return modelos;
	}

	public List<Proceso> modelosEntidades(List<MProceso> mInstancias) {
		List<Proceso> entidades = new ArrayList<>();
		for (MProceso mInstancia : mInstancias) {
			entidades.add(modeloEntidad(mInstancia));
		}
		return entidades;
	}
}
