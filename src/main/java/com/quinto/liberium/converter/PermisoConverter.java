package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.model.MPermiso;
import com.quinto.liberium.service.PermisoService;

@Component
public class PermisoConverter extends Converter<MPermiso, Permiso> {

	@Autowired
	private PermisoService permisoService;

	public Permiso modeloEntidad(MPermiso modelo) {
		Permiso permiso = new Permiso();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			permiso = permisoService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, permiso);
		} catch (Exception e) {
			log.error("Error al convertir el modelo de permiso en entidad", e);
		}
		return permiso;
	}

	public MPermiso entidadModelo(Permiso permiso) {
		MPermiso modelo = new MPermiso();
		try {
			BeanUtils.copyProperties(permiso, modelo);
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de permiso", e);
		}
		return modelo;
	}

	public List<MPermiso> entidadesModelos(List<Permiso> permisos) {
		List<MPermiso> modelos = new ArrayList<>();
		for (Permiso permiso : permisos) {
			modelos.add(entidadModelo(permiso));
		}
		return modelos;
	}

	public List<Permiso> modelosEntidades(List<MPermiso> mPermisos) {
		List<Permiso> entidades = new ArrayList<>();
		for (MPermiso mPermiso : mPermisos) {
			entidades.add(modeloEntidad(mPermiso));
		}
		return entidades;
	}

}