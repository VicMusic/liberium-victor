package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MUsuario;
import com.quinto.liberium.service.UsuarioService;

@Component
public class UsuarioConverter extends Converter<MUsuario, Usuario> {

	@Autowired
	private UsuarioService usuarioService;
	
	public Usuario modeloEntidad(MUsuario modelo) {
		Usuario usuario = new Usuario();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			usuario = usuarioService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, usuario);
			
			usuario.setTipoUsuario(modelo.getTipoUsuario());
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del usuario en entidad", e);
		}
		return usuario;
	}

	public MUsuario entidadModelo(Usuario usuario) {
		MUsuario modelo = new MUsuario();
		try {
			BeanUtils.copyProperties(usuario, modelo);
			modelo.setTipoUsuario(usuario.getTipoUsuario());
			modelo.setFoto(usuario.getFoto());
			
			if (usuario.getPais() != null) {
				modelo.getmPais().setId(usuario.getPais().getId());
				modelo.getmPais().setNombre(usuario.getPais().getNombre());
			}
			
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de usuario", e);
		}
		return modelo;
	}

	public List<MUsuario> entidadesModelos(List<Usuario> usuarios) {
		List<MUsuario> modelos = new ArrayList<>();
		for (Usuario usuario : usuarios) {
			modelos.add(entidadModelo(usuario));
		}
		return modelos;
	}

	public List<Usuario> modelosEntidades(List<MUsuario> mUsuarios) {
		List<Usuario> entidades = new ArrayList<>();
		for (MUsuario mUsuario : mUsuarios) {
			entidades.add(modeloEntidad(mUsuario));
		}
		return entidades;
	}

}