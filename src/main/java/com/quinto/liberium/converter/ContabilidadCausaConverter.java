package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.ContabilidadCausa;
import com.quinto.liberium.entity.Honorario;
import com.quinto.liberium.model.MContabilidadCausa;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ContabilidadCausaService;
import com.quinto.liberium.service.HonorarioService;

@Component
public class ContabilidadCausaConverter extends Converter<MContabilidadCausa, ContabilidadCausa> {

	@Autowired
	private CausaService causaService;
	
	@Autowired
	private HonorarioService honorarioService;
	
	@Autowired
	private ContabilidadCausaService contabilidadCausaService;
	
	public ContabilidadCausa modeloEntidad(MContabilidadCausa modelo) {
		ContabilidadCausa contabilidadCausa = new ContabilidadCausa();
		
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			contabilidadCausa = contabilidadCausaService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, contabilidadCausa);
			
			Causa causa = causaService.buscarPorId(modelo.getmCausa().getId());
			Honorario honorario = honorarioService.buscarHonorarioPorId(modelo.getmHonorario().getId());
			
			contabilidadCausa.setHonorario(honorario);
			contabilidadCausa.setCausa(causa);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo de contabilidad causa en entidad", e);
		}
		return contabilidadCausa;
	}

	public MContabilidadCausa entidadModelo(ContabilidadCausa contabilidadCausa) {
		MContabilidadCausa modelo = new MContabilidadCausa();
		try {
			BeanUtils.copyProperties(contabilidadCausa, modelo);
			
			modelo.getmCausa().setId(contabilidadCausa.getCausa().getId());
			
			if (contabilidadCausa.getHonorario() != null) {
				modelo.getmHonorario().setNombre(contabilidadCausa.getHonorario().getNombre());
				modelo.getmHonorario().setId(contabilidadCausa.getHonorario().getId());
			}
			
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de contabilidad causa", e);
		}
		return modelo;
	}

	public List<MContabilidadCausa> entidadesModelos(List<ContabilidadCausa> contabilidadCausas) {
		List<MContabilidadCausa> modelos = new ArrayList<>();
		for (ContabilidadCausa contabilidadCausa: contabilidadCausas) {
			modelos.add(entidadModelo(contabilidadCausa));
		}
		return modelos;
	}

	public List<ContabilidadCausa> modelosEntidades(List<MContabilidadCausa> mContabilidadCausas) {
		List<ContabilidadCausa> entidades = new ArrayList<>();
		for (MContabilidadCausa mContabilidadCausa : mContabilidadCausas) {
			entidades.add(modeloEntidad(mContabilidadCausa));
		}
		return entidades;
	}

	
}
