package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Notificacion;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MNotificacion;
import com.quinto.liberium.service.NotificacionService;
import com.quinto.liberium.service.UsuarioService;

@Component
public class NotificacionConverter extends Converter<MNotificacion, Notificacion>{

	@Autowired
	private NotificacionService notificacionService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	public Notificacion modeloEntidad(MNotificacion modelo) {
		Notificacion notificacion = new Notificacion();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			notificacion = notificacionService.buscarNotificacionPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, notificacion);
			Usuario usuario = usuarioService.buscarPorId(modelo.getmUsuario().getId());
			notificacion.setUsuario(usuario);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo de notificacion en entidad", e);
		}
		return notificacion;
	}
	
	public MNotificacion entidadModelo(Notificacion notificacion) {
		MNotificacion modelo = new MNotificacion();
		try {
		    BeanUtils.copyProperties(notificacion, modelo);
		    
		    modelo.getmUsuario().setId(notificacion.getUsuario().getId());
		    
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de notificacion", e);
		}
		return modelo;
    }

    public List<MNotificacion> entidadesModelos(List<Notificacion> notificacions) {
		List<MNotificacion> modelos = new ArrayList<>();
		for (Notificacion notificacion : notificacions) {
		    modelos.add(entidadModelo(notificacion));
		}
		return modelos;
    }

    public List<Notificacion> modelosEntidades(List<MNotificacion> mNotificacions) {
		List<Notificacion> entidades = new ArrayList<>();
		for (MNotificacion mNotificacion: mNotificacions) {
		    entidades.add(modeloEntidad(mNotificacion));
		}
		return entidades;
    }
}
