package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.ContabilidadEstudio;
import com.quinto.liberium.model.MContabilidadEstudio;

@Component
public class ContabilidadEstudioConverter extends Converter<MContabilidadEstudio, ContabilidadEstudio>{
	
	public ContabilidadEstudio modeloEntidad(MContabilidadEstudio modelo) {
		ContabilidadEstudio entidad = new ContabilidadEstudio();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
//			entidad = bancoDistribucionService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, entidad);

		} catch (Exception e) {
			log.error("Error al convertir el modelo de banco o distribución en entidad", e);
		}
		return entidad;
	}

	public MContabilidadEstudio entidadModelo(ContabilidadEstudio entidad) {
		MContabilidadEstudio modelo = new MContabilidadEstudio();
		try {
			BeanUtils.copyProperties(entidad, modelo);
			
			modelo.getmEstudio().setId(entidad.getEstudio().getId());
			modelo.getmEstudio().setNombre(entidad.getEstudio().getNombre());
			
			if (entidad.getHonorario() != null) {
				modelo.getmHonorario().setId(entidad.getHonorario().getId());
			}
			
			if (entidad.getMiembro() != null) {
				modelo.getmMiembro().getmUsuario().setNombre(entidad.getMiembro().getUsuario().getNombre());
			}

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de banco o distribución", e);
		}
		return modelo;
	}

	public List<MContabilidadEstudio> entidadesModelos(List<ContabilidadEstudio> entidades) {
		List<MContabilidadEstudio> modelos = new ArrayList<>();
		for (ContabilidadEstudio entidad : entidades) {
			modelos.add(entidadModelo(entidad));
		}
		return modelos;
	}

	public List<ContabilidadEstudio> modelosEntidades(List<MContabilidadEstudio> modelos) {
		List<ContabilidadEstudio> entidades = new ArrayList<>();
		for (MContabilidadEstudio modelo : modelos) {
			entidades.add(modeloEntidad(modelo));
		}
		return entidades;
	}
	
}
