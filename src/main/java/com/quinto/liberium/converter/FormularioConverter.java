package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.model.MFormulario;
import com.quinto.liberium.repository.FormularioRepository;

@Component
public class FormularioConverter extends Converter<MFormulario, Formulario> {

	
	@Autowired
	private FormularioRepository formularioRepository;

	
	
	public Formulario modeloEntidad(MFormulario modelo) {
		Formulario formulario = new Formulario();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			formulario = formularioRepository.buscarPorId(modelo.getId());
		}
		try {		
			BeanUtils.copyProperties(modelo, formulario);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del país en entidad.", e);
		}
		return formulario;
	}

	public MFormulario entidadModelo(Formulario formulario) {
		MFormulario modelo = new MFormulario();
		try {
			BeanUtils.copyProperties(formulario, modelo);
			
			modelo.getIdTipoCausa().setId(formulario.getTipo().getId());
			modelo.getIdTipoCausa().setNombre(formulario.getTipo().getNombre());
			
			modelo.getmPais().setId(formulario.getPais().getId());
			modelo.getmPais().setNombre(formulario.getPais().getNombre());
			
			modelo.getmProvincia().setId(formulario.getProvincia().getId());
			modelo.getmProvincia().setNombre(formulario.getProvincia().getNombre());
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de formulario.", e);
		}
		return modelo;
	}

	public List<MFormulario> entidadesModelos(List<Formulario> formularioes) {
		List<MFormulario> modelos = new ArrayList<>();
		for (Formulario formulario : formularioes) {
			modelos.add(entidadModelo(formulario));
		}
		return modelos;
	}

	public List<Formulario> modelosEntidades(List<MFormulario> mFormularioes) {
		List<Formulario> entidades = new ArrayList<>();
		for (MFormulario mFormulario : mFormularioes) {
			entidades.add(modeloEntidad(mFormulario));
		}
		return entidades;
	}

}