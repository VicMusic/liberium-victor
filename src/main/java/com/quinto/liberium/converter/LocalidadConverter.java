package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class LocalidadConverter extends Converter<MLocalidad, Localidad>{

	@Autowired 
	private LocalidadService localidadService;
	@Autowired
	private ProvinciaService provinciaService;
	
	public Localidad modeloEntidad(MLocalidad modelo) {
		Localidad localidad = new Localidad();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
		    localidad = localidadService.buscarPorId(modelo.getId());
		}
		try {
		    BeanUtils.copyProperties(modelo, localidad);
		    
		    Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getProvincia().getId());
		    localidad.setProvincia(provincia);
		    
		} catch (Exception e) {
		    log.error("Error al convertir el modelo de la localidad en entidad", e);
		}
		return localidad;
	    }

	    public MLocalidad entidadModelo(Localidad localidad) {
		MLocalidad modelo = new MLocalidad();
		try {
		    BeanUtils.copyProperties(localidad, modelo);
		    
		    modelo.getProvincia().setId(localidad.getProvincia().getId());
		    modelo.getProvincia().setNombre(localidad.getProvincia().getNombre());
		    
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de localidad", e);
		}
		return modelo;
	    }

	    public List<MLocalidad> entidadesModelos(List<Localidad> entidades) {
		List<MLocalidad> modelos = new ArrayList<>();
		for (Localidad localidad : entidades) {
		    modelos.add(entidadModelo(localidad));
		}
		return modelos;
	    }

	    public List<Localidad> modelosEntidades(List<MLocalidad> modelos) {
		List<Localidad> entidades = new ArrayList<>();
		for (MLocalidad modelo : modelos) {
		    entidades.add(modeloEntidad(modelo));
		}
		return entidades;
	    }
	
}
