package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.ItemService;

@Component
public class ItemConverter extends Converter<MItem, Item>{

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ChecklistService checklistService;
	
	@Autowired
	private SubItemConverter subItemConverter;
	
	@Autowired
	private ArchivoConverter archivoConverter;
	
	@Autowired
	private EscritoConverter escritoConverter;
	
	
	public Collection<Item> convertir(Map<String,String[]> mapa){
		HashMap<String, Item> items = new HashMap<>();
		int ordenSub = 1;
		for(String key : mapa.keySet()) {
			if(key.contains("nombreItem")) {
				String[] datos = key.split("-");
				
				Item item = null;
				if(items.containsKey(datos[0])) {
					item = items.get(datos[0]);
				} else {
					item = new Item();
					items.put(datos[0], item);
				}

				item.setOrden(new Integer(datos[0]));
				item.setNombre(mapa.get(key)[0]);
				ordenSub = 1;
			} else if(key.contains("OPCION")){
				String[] datos = key.split("-");
				
				Item item = null;
				if(items.containsKey(datos[0])) {
					item = items.get(datos[0]);
				} else {
					item = new Item();
					items.put(datos[0], item);
				}
				
				for(String titulo : mapa.get(key)) {
					SubItem opcion = new SubItem();
					opcion.setNombre(titulo);
					opcion.setCreado(new Date());
					opcion.setOrden(ordenSub);
					item.getSubItems().add(opcion);
					ordenSub++;
				}
				

			}
		}
		
		return items.values();
	}
	
	
	
	public Collection<Item> convertirConId(Map<String,String[]> mapa,String[] ids, String[] idSubItem){
		HashMap<String, Item> preguntas = new HashMap<>();
		int x = 0;
		int ordenSub = 1;
		for(String key : mapa.keySet()) {
			if(key.contains("nombreItem")) {
				String[] datos = key.split("-");
				
				Item item = null;
				if(preguntas.containsKey(datos[0])) {
					item = preguntas.get(datos[0]);
					
				} else {
					item = new Item();
					preguntas.put(datos[0], item);
				}
				item.setId(ids[x]);
                item.setOrden(new Integer(datos[0]));
				item.setNombre(mapa.get(key)[0]);;
				x++;
				ordenSub = 1;
			} else if(key.contains("OPCION")){
				String[] datos = key.split("-");
				
				Item item = null;
				if(preguntas.containsKey(datos[0])) {
					item = preguntas.get(datos[0]);
				} else {
					item = new Item();
					preguntas.put(datos[0], item);
				}
				
				for(String titulo : mapa.get(key)) {
					SubItem opcion = new SubItem();
					
					opcion.setNombre(titulo);
					opcion.setCreado(new Date());
					opcion.setOrden(ordenSub);
                 
					item.getSubItems().add(opcion);
					ordenSub++;
				}
				
				item = null;
			}
		}
		
		return preguntas.values();
	}

	
	public Item modeloEntidad(MItem modelo) {
		Item item = new Item();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			item = itemService.buscarItemPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, item);

			Checklist checkList = checklistService.buscarCheckListPorId(modelo.getmChecklists().getId());
			item.setCheckList(checkList);
			List<SubItem> subItems = subItemConverter.modelosEntidades(modelo.getmSubItem());
			item.setSubItems(subItems);

		} catch (Exception e) {
			log.error("Error al convertir el modelo del item en entidad", e);
		}
		return item;

	}

	public MItem entidadModelo(Item item) {
		MItem modelo = new MItem();
		try {
			BeanUtils.copyProperties(item, modelo);

			modelo.getmChecklists().setId(item.getCheckList().getId());
			modelo.getmChecklists().setNombre(item.getCheckList().getNombre());
			modelo.setmSubItem(subItemConverter.entidadesModelos(item.getSubItems()));
			
			if (item.getArchivos() != null) {
				modelo.setmArchivo(archivoConverter.entidadesModelos(item.getArchivos()));
			}
			
			if (item.getEscritos() != null) {
				modelo.setmEscritos(escritoConverter.entidadesModelos(item.getEscritos()));
			}

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de item", e);
		}
		return modelo;
	}

	public List<MItem> entidadesModelos(List<Item> items) {
		List<MItem> modelos = new ArrayList<>();
		for (Item item : items) {
			modelos.add(entidadModelo(item));
		}
		return modelos;
	}

	public List<Item> modelosEntidades(List<MItem> mItems) {
		List<Item> entidades = new ArrayList<>();
		for (MItem mItem : mItems) {
			entidades.add(modeloEntidad(mItem));
		}
		return entidades;
	}
	
}
