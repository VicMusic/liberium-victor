package com.quinto.liberium.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.model.MEvento;
import com.quinto.liberium.service.EventoService;

@Component
public class EventoConverter extends Converter<MEvento, Evento> {

	@Autowired
	private EventoService eventoService;

	public Evento modeloEntidad(MEvento modelo) {
		Evento evento = new Evento();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			evento = eventoService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, evento);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del evento en entidad", e);
		}
		return evento;
	}

	public MEvento entidadModelo(Evento evento) {
		MEvento modelo = new MEvento();
		try {
			BeanUtils.copyProperties(evento, modelo);
				
			Date fechaInicio = evento.getFechaInicio();
			Date fechaFin = evento.getFechaFin();
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT-3"));

			if (evento.getFechaInicio() != null || evento.getFechaFin() != null) {
				String stringFechaInicio = dateFormat.format(fechaInicio);
				String stringFechaFin = dateFormat.format(fechaFin);
	
				modelo.setFechaInicio(stringFechaInicio);
				modelo.setFechaFin(stringFechaFin);	
			}
			
			modelo.setCantDias(evento.getCantDias());
			modelo.setTipoPlazo(evento.getTipoPlazo());
			modelo.setTipoDia(evento.getTipoDiaNoLaborable());
			
			if(evento.getTurno() != null) {
				modelo.setTitulo(modelo.getTitulo() + " - " + evento.getTurno().getNombreCliente() + " - " + evento.getTurno().getTelefonoCliente() + " - " + evento.getTurno().getMailCliente());
			}
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de evento", e);
		}
		return modelo;
	}

	public List<MEvento> entidadesModelos(List<Evento> eventos) {
		List<MEvento> modelos = new ArrayList<>();
		for (Evento evento : eventos) {
			modelos.add(entidadModelo(evento));
		}
		return modelos;
	}

	public List<Evento> modelosEntidades(List<MEvento> mEventos) {
		List<Evento> entidades = new ArrayList<>();
		for (MEvento mEvento : mEventos) {
			entidades.add(modeloEntidad(mEvento));
		}
		return entidades;
	}

}