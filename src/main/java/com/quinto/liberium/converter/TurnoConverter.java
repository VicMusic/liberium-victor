package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.model.MTurno;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.TurnoService;

@Component
public class TurnoConverter extends Converter<MTurno, Turno>{
	@Autowired
	private TurnoService turnoService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	public Turno modeloEntidad(MTurno modelo) {
		Turno turno = new Turno();

		if (modelo.getId() != null && modelo.getId().isEmpty()) {
			turno = turnoService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, turno);

			Abogado abogado = abogadoService.buscarAbogadoPorId(modelo.getmAbogado().getId());
			
			turno.setAbogado(abogado);


		} catch (Exception e) {
			log.error("Error al convertir el modelo del turno en entidad", e);
		}

		return turno;
	}

	public MTurno entidadModelo(Turno turno) {
		MTurno modelo = new MTurno();
		try {
			BeanUtils.copyProperties(turno, modelo);
			
			modelo.setElegido(turno.getFechaOriginal());
			modelo.getmUsuario().setEmail(turno.getAbogado().getUsuario().getEmail());
			modelo.getmUsuario().setNombre(turno.getAbogado().getUsuario().getNombre());
			modelo.getmUsuario().setTelefono(turno.getAbogado().getUsuario().getTelefono());
			modelo.getmAbogado().setDomicilio(turno.getAbogado().getDomicilio());
			modelo.getmAbogado().setPasos(turno.getAbogado().getPasos());
			modelo.setNombreCliente(turno.getNombreCliente());
			modelo.setTelefonoCliente(turno.getTelefonoCliente());
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad del turno en modelo", e);
		}
		return modelo;
	}

	public List<MTurno> entidadesModelos(List<Turno> turnos) {
		List<MTurno> modelos = new ArrayList<>();
		for (Turno turno : turnos) {
			modelos.add(entidadModelo(turno));
		}
		return modelos;
	}

	public List<Turno> modelosEntidades(List<MTurno> mTurnos) {
		List<Turno> entidades = new ArrayList<>();
		for (MTurno mturno: mTurnos) {
			entidades.add(modeloEntidad(mturno));
		}
		return entidades;
	}
}
