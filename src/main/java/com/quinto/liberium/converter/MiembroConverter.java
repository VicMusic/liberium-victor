package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.service.MiembroService;

@Component
public class MiembroConverter extends Converter<MMiembro, Miembro> {

	@Autowired
	private MiembroService miembroService;

	@Autowired
	private PermisoConverter permisoConverter;

	public Miembro modeloEntidad(MMiembro modelo) {
		Miembro miembro = new Miembro();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			miembro = miembroService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, miembro);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del miembro en entidad", e);
		}
		return miembro;
	}

	public MMiembro entidadModelo(Miembro miembro) {
		MMiembro modelo = new MMiembro();
		try {
			BeanUtils.copyProperties(miembro, modelo);

			modelo.getmUsuario().setId(miembro.getUsuario().getId());
			modelo.getmUsuario().setNombre(miembro.getUsuario().getNombre());
			modelo.getmUsuario().setEmail(miembro.getUsuario().getEmail());

			if (miembro.getRol() != null) {
				modelo.getmRol().setNombre(miembro.getRol().getNombre());
				modelo.getmRol().setPermiso(permisoConverter.entidadesModelos(miembro.getRol().getPermisos()));
			}

			modelo.getmEstudio().setId(miembro.getEstudio().getId());
			modelo.getmEstudio().setNombre(miembro.getEstudio().getNombre());

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de miembro", e);
		}
		return modelo;
	}

	public List<MMiembro> entidadesModelos(List<Miembro> miembros) {
		List<MMiembro> modelos = new ArrayList<>();
		for (Miembro miembro : miembros) {
			modelos.add(entidadModelo(miembro));
		}
		return modelos;
	}

	public Set<MMiembro> entidadesModelosSet(Set<Miembro> miembros) {
		Set<MMiembro> modelos = new HashSet<>();
		for (Miembro miembro : miembros) {
			modelos.add(entidadModelo(miembro));
		}
		return modelos;
	}

	public List<Miembro> modelosEntidades(List<MMiembro> mMiembros) {
		List<Miembro> entidades = new ArrayList<>();
		for (MMiembro mMiembro : mMiembros) {
			entidades.add(modeloEntidad(mMiembro));
		}
		return entidades;
	}

	public Set<Miembro> modelosEntidadesSet(Set<MMiembro> mMiembros) {
		Set<Miembro> entidades = new HashSet<>();
		for (MMiembro mMiembro : mMiembros) {
			entidades.add(modeloEntidad(mMiembro));
		}
		return entidades;
	}

}