package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.service.PaisService;

@Component
public class PaisConverter extends Converter<MPais, Pais> {

	@Autowired
	private PaisService paisService;

	public Pais modeloEntidad(MPais modelo) {
		Pais pais = new Pais();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			pais = paisService.buscarPaisPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, pais);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del país en entidad", e);
		}
		return pais;
	}

	public MPais entidadModelo(Pais pais) {
		MPais modelo = new MPais();
		try {
			BeanUtils.copyProperties(pais, modelo);
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de país", e);
		}
		return modelo;
	}

	public List<MPais> entidadesModelos(List<Pais> paises) {
		List<MPais> modelos = new ArrayList<>();
		for (Pais pais : paises) {
			modelos.add(entidadModelo(pais));
		}
		return modelos;
	}

	public List<Pais> modelosEntidades(List<MPais> mPaises) {
		List<Pais> entidades = new ArrayList<>();
		for (MPais mPais : mPaises) {
			entidades.add(modeloEntidad(mPais));
		}
		return entidades;
	}

}