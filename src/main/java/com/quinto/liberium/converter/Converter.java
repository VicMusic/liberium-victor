package com.quinto.liberium.converter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.quinto.liberium.exception.WebException;

public abstract class Converter< M extends Object, E extends Object> {
    public abstract E modeloEntidad(M m) throws WebException;

    public abstract M entidadModelo(E e);

    protected Log log;

    public Converter() {
	this.log = LogFactory.getLog(getClass());
    }
}