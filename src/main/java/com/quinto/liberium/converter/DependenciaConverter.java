package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Dependencia;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.model.MDependencia;
import com.quinto.liberium.service.DependenciaService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.TipoDependenciaService;

@Component
public class DependenciaConverter extends Converter<MDependencia, Dependencia> {

    @Autowired
    private DependenciaService dependenciaService;
    
   
    @Autowired
    private TipoDependenciaService tipoDependenciaService;
    
    @Autowired
    private InstanciaService instanciaService;

   
    public Dependencia modeloEntidad(MDependencia modelo) {
	Dependencia dependencia = new Dependencia();
	if (modelo.getId() != null && !modelo.getId().isEmpty()) {
	    dependencia = dependenciaService.buscarDependenciaPorId(modelo.getId());
	}
	try {
	    BeanUtils.copyProperties(modelo, dependencia);
	    TipoDependencia juzgado = tipoDependenciaService.buscarTipoDependenciaPorId(modelo.getmTipoDependencia().getId());
	    Instancia instancia = instanciaService.buscarInstanciaPorId(modelo.getmInstancia().getId());

	    
	    dependencia.setTipoDependencia(juzgado);
	    dependencia.setInstancia(instancia);
	    

	} catch (Exception e) {
	    log.error("Error al convertir el modelo del dependencia en entidad", e);
	}
	return dependencia;
    }

    public MDependencia entidadModelo(Dependencia dependencia) {
	MDependencia modelo = new MDependencia();
	try {
	    BeanUtils.copyProperties(dependencia, modelo);
	     
	    modelo.getmTipoDependencia().setId(dependencia.getTipoDependencia().getId());
	    modelo.getmTipoDependencia().setNombre(dependencia.getTipoDependencia().getNombre());
	    
	    modelo.getmInstancia().setId(dependencia.getInstancia().getId());
	    modelo.getmInstancia().setNombre(dependencia.getInstancia().getNombre());
	    
	   
	  
	} catch (Exception e) {
	    log.error("Error al convertir la entidad en el modelo de dependencia", e);
	}
	return modelo;
    }

    public List<MDependencia> entidadesModelos(List<Dependencia> dependencias) {
	List<MDependencia> modelos = new ArrayList<>();
	for (Dependencia dependencia : dependencias) {
	    modelos.add(entidadModelo(dependencia));
	}
	return modelos;
    }

    public List<Dependencia> modelosEntidades(List<MDependencia> mDependencias) {
	List<Dependencia> entidades = new ArrayList<>();
	for (MDependencia mDependencia : mDependencias) {
	    entidades.add(modeloEntidad(mDependencia));
	}
	return entidades;
    }

}