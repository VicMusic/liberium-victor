package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class ProvinciaConverter extends Converter<MProvincia, Provincia> {

    @Autowired
    private ProvinciaService provinciaService;
    
    @Autowired
    private PaisService paisService;

    public Provincia modeloEntidad(MProvincia modelo) {
	Provincia provincia = new Provincia();
	if (modelo.getId() != null && !modelo.getId().isEmpty()) {
	    provincia = provinciaService.buscarProvinciaPorId(modelo.getId());
	}
	try {
	    BeanUtils.copyProperties(modelo, provincia);
	    
	    Pais pais = paisService.buscarPaisPorId(modelo.getmPais().getId());
	    provincia.setPais(pais);
	    
	} catch (Exception e) {
	    log.error("Error al convertir el modelo del provincia en entidad", e);
	}
	return provincia;
    }

    public MProvincia entidadModelo(Provincia provincia) {
	MProvincia modelo = new MProvincia();
	try {
	    BeanUtils.copyProperties(provincia, modelo);
	    
	    modelo.getmPais().setId(provincia.getPais().getId());
	    modelo.getmPais().setNombre(provincia.getPais().getNombre());
	    
	} catch (Exception e) {
	    log.error("Error al convertir la entidad en el modelo de provincia", e);
	}
	return modelo;
    }

    public List<MProvincia> entidadesModelos(List<Provincia> provincias) {
	List<MProvincia> modelos = new ArrayList<>();
	for (Provincia provincia : provincias) {
	    modelos.add(entidadModelo(provincia));
	}
	return modelos;
    }

    public List<Provincia> modelosEntidades(List<MProvincia> mProvincias) {
	List<Provincia> entidades = new ArrayList<>();
	for (MProvincia mProvincia : mProvincias) {
	    entidades.add(modeloEntidad(mProvincia));
	}
	return entidades;
    }

}