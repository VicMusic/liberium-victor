package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.model.MTipoDependencia;
import com.quinto.liberium.service.TipoDependenciaService;

@Component
public class TipoDependenciaConverter extends Converter<MTipoDependencia, TipoDependencia>{
	
	@Autowired
	private TipoDependenciaService tipoDependenciaService;
	

	public TipoDependencia modeloEntidad(MTipoDependencia modelo) {
		TipoDependencia tipoDependencia= new TipoDependencia();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
		    tipoDependencia = tipoDependenciaService.buscarTipoDependenciaPorId(modelo.getId());
		}
		try {
		    BeanUtils.copyProperties(modelo, tipoDependencia);
		
		    
		} catch (Exception e) {
		    log.error("Error al convertir el modelo de tipo de dependencia en entidad", e);
		}
		return tipoDependencia;
	    }

	    public MTipoDependencia entidadModelo(TipoDependencia tipoDependencia) {
		MTipoDependencia modelo = new MTipoDependencia();
		try {
		    BeanUtils.copyProperties(tipoDependencia, modelo);
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de tipo de dependencia", e);
		}
		return modelo;
	    }

	    public List<MTipoDependencia> entidadesModelos(List<TipoDependencia> tipoDependencias) {
		List<MTipoDependencia> modelos = new ArrayList<>();
		for (TipoDependencia tipoDependencia : tipoDependencias) {
		    modelos.add(entidadModelo(tipoDependencia));
		}
		return modelos;
	    }

	    public List<TipoDependencia> modelosEntidades(List<MTipoDependencia> mTipoDependencias) {
		List<TipoDependencia> entidades = new ArrayList<>();
		for (MTipoDependencia mTipoDependencia : mTipoDependencias) {
		    entidades.add(modeloEntidad(mTipoDependencia));
		}
		return entidades;
	    }
}
