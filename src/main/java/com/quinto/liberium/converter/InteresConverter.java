package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Interes;
import com.quinto.liberium.model.MInteres;
import com.quinto.liberium.service.InteresService;

@Component
public class InteresConverter extends Converter<MInteres, Interes>{
	
	@Autowired
	private InteresService interesService;
	
	public Interes modeloEntidad(MInteres modelo) {
		Interes interes = new Interes();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			interes = interesService.buscarInteresPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, interes);
		} catch (Exception e) {
			log.error("Error al convertir el modelo del país en entidad", e);
		}
		return interes;
	}
	
	public MInteres entidadModelo(Interes interes) {
		MInteres modelo = new MInteres();
		try {
			BeanUtils.copyProperties(interes, modelo);
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de interes", e);
		}
		return modelo;
	}
	
	public List<MInteres> entidadesModelos(List<Interes> intereses) {
		List<MInteres> modelos = new ArrayList<>();
		for (Interes interes : intereses) {
			modelos.add(entidadModelo(interes));
		}
		return modelos;
	}

	public List<Interes> modelosEntidades(List<MInteres> mIntereses) {
		List<Interes> entidades = new ArrayList<>();
		for (MInteres mInteres : mIntereses) {
			entidades.add(modeloEntidad(mInteres));
		}
		return entidades;
	}


}
