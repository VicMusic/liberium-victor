package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@Component
public class AbogadoConverter extends Converter<MAbogado, Abogado> {

	@Autowired
	private AbogadoService abogadoService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private LocalidadService localidadService;
	
	@Autowired
	private ProvinciaService provinciaService;

	public Abogado modeloEntidad(MAbogado modelo) {
		Abogado abogado = new Abogado();

		if (modelo.getId() != null && modelo.getId().isEmpty()) {
			abogado = abogadoService.buscarAbogadoPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, abogado);

			Usuario usuario = usuarioService.buscarPorId(modelo.getmUsuario().getId());
			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			Localidad localidad = localidadService.buscarPorId(modelo.getmLocalidad().getId());
			
			abogado.setUsuario(usuario);

			List<Materia> materias = new ArrayList<>();

			for (MMateria materiaa : modelo.getmMateria()) {
				Materia eMateria = new Materia();

				eMateria.setId(materiaa.getId());
				eMateria.setNombre(materiaa.getNombre());

			}
			
			abogado.setLocalidad(localidad);

			abogado.setMaterias(materias);

			abogado.setProvincia(provincia);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del abogado en entidad", e);
		}

		return abogado;
	}

	@Override
	public MAbogado entidadModelo(Abogado abogado) {
		MAbogado modelo = new MAbogado();

		try {

			BeanUtils.copyProperties(abogado, modelo);

			modelo.getmUsuario().setId(abogado.getUsuario().getId());
			modelo.getmUsuario().setNombre(abogado.getUsuario().getNombre());
			modelo.getmUsuario().setEmail(abogado.getUsuario().getEmail());

			List<MMateria> materias = new ArrayList<>();

			for (Materia materia : abogado.getMaterias()) {
				MMateria mMateria = new MMateria();
				mMateria.setId(materia.getId());
				mMateria.setNombre(materia.getNombre());

				materias.add(mMateria);
			}

			modelo.setmMateria(materias);
			
			if (abogado.getLocalidad() != null) {
				modelo.getmLocalidad().setId(abogado.getLocalidad().getId());
				modelo.getmLocalidad().setNombre(abogado.getLocalidad().getNombre());
			}
			if (abogado.getProvincia() != null) {
				modelo.getmProvincia().setId(abogado.getProvincia().getId());
				modelo.getmProvincia().setNombre(abogado.getProvincia().getNombre());
			}
			
			if (abogado.getDuracion() != null && abogado.getDuracion() == 30 || abogado.getDuracion() == 1) {
				modelo.setDuracion(abogado.getDuracion());
			}
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad del abogado en modelo", e);
		}
		return modelo;
	}

	public List<MAbogado> entidadesModelos(List<Abogado> abogados) {
		List<MAbogado> modelos = new ArrayList<>();
		for (Abogado abogado : abogados) {
			modelos.add(entidadModelo(abogado));
		}
		return modelos;
	}

	public List<Abogado> modelosEntidades(List<MAbogado> mAbogados) {
		List<Abogado> entidades = new ArrayList<>();
		for (MAbogado mAbogado : mAbogados) {
			entidades.add(modeloEntidad(mAbogado));
		}
		return entidades;
	}

}
