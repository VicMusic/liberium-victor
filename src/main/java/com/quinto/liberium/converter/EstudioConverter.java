package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MEstudio;
import com.quinto.liberium.service.EstudioService;

@Component
public class EstudioConverter extends Converter<MEstudio, Estudio> {

	@Autowired
	private EstudioService estudioService;

	public Estudio modeloEntidad(MEstudio modelo) {
		Estudio estudio = new Estudio();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			estudio = estudioService.buscarPorIdEstudio(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, estudio);
			
			List<Abogado> abogados = new ArrayList<>();
			
			if (modelo.getAbogados() != null) {

				for (MAbogado mAbogado : modelo.getAbogados()) {
					Abogado eAbogado = new Abogado();
					
					eAbogado.setId(mAbogado.getId());
					abogados.add(eAbogado);
					
				}
				
				estudio.setAbogados(abogados);				
			}
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo del estudio en entidad", e);
		}
		return estudio;
	}

	public MEstudio entidadModelo(Estudio estudio) {
		MEstudio modelo = new MEstudio();
		try {
			BeanUtils.copyProperties(estudio, modelo);
			
			List<MAbogado> abogados = new ArrayList<>();
			
			if (estudio.getAbogados() != null) {
				for (Abogado abogado : estudio.getAbogados()) {
					MAbogado mAbogado = new MAbogado();
					mAbogado.setId(abogado.getId());

					abogados.add(mAbogado);
					modelo.setAbogados(abogados);
				}	
			}
			
		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de estudio", e);
		}
		return modelo;
	}

	public List<MEstudio> entidadesModelos(List<Estudio> estudios) {
		List<MEstudio> modelos = new ArrayList<>();
		for (Estudio estudio : estudios) {
			modelos.add(entidadModelo(estudio));
		}
		return modelos;
	}

	public List<Estudio> modelosEntidades(List<MEstudio> mEstudios) {
		List<Estudio> entidades = new ArrayList<>();
		for (MEstudio mEstudio : mEstudios) {
			entidades.add(modeloEntidad(mEstudio));
		}
		return entidades;
	}

}