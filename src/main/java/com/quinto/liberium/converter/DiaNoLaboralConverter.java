package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.DiaNoLaboral;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.model.MDiaNoLaboral;
import com.quinto.liberium.service.DiaNoLaboralService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;

@Component
public class DiaNoLaboralConverter extends Converter<MDiaNoLaboral, DiaNoLaboral>{

	@Autowired
	private DiaNoLaboralService diaNoLaboralService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	public DiaNoLaboral modeloEntidad(MDiaNoLaboral modelo) {
		DiaNoLaboral diaNoLaboral = new DiaNoLaboral();
		
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			diaNoLaboral = diaNoLaboralService.buscarEntidadDiaNoLaboralPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, diaNoLaboral);
			
			Pais pais = paisService.buscarPaisPorId(modelo.getmPais().getId());
			Provincia provincia = provinciaService.buscarProvinciaPorId(modelo.getmProvincia().getId());
			diaNoLaboral.setPais(pais);
			diaNoLaboral.setProvincia(provincia);
			
		} catch (Exception e) {
		    log.error("Error al convertir el modelo del DiaNoLaboral en entidad", e);
		}
		return diaNoLaboral;
	}
	
	public MDiaNoLaboral entidadModelo(DiaNoLaboral diaNoLaboral) {
		MDiaNoLaboral modelo = new MDiaNoLaboral();

		try {
			
			BeanUtils.copyProperties(diaNoLaboral, modelo);
			
			if (diaNoLaboral.getProvincia() == null) {
				modelo.getmPais().setId(diaNoLaboral.getPais().getId());
				modelo.getmPais().setNombre(diaNoLaboral.getPais().getNombre());
			} else {
				modelo.getmPais().setId(diaNoLaboral.getPais().getId());
				modelo.getmPais().setNombre(diaNoLaboral.getPais().getNombre());
				modelo.getmProvincia().setId(diaNoLaboral.getProvincia().getId());
				modelo.getmProvincia().setNombre(diaNoLaboral.getProvincia().getNombre());
			}
			
		} catch (Exception e) {
		    log.error("Error al convertir la entidad en el modelo de DiaNoLaboral", e);
		}
		return modelo;
	}
	
	public List<MDiaNoLaboral> entidadesModelos(List<DiaNoLaboral> diaNoLaborales){
		
		List<MDiaNoLaboral> modelos = new ArrayList<>();
		for (DiaNoLaboral diaNoLaboral : diaNoLaborales) {
			modelos.add(entidadModelo(diaNoLaboral));
		}
		return modelos;
	}
	
	public List<DiaNoLaboral> modelosEntidades(List<MDiaNoLaboral> mDiasNoLaborales){
		List<DiaNoLaboral> entidades = new ArrayList<>();
		for (MDiaNoLaboral mDiaNoLaboral : mDiasNoLaborales) {
			entidades.add(modeloEntidad(mDiaNoLaboral));
		}
		return entidades;
	}
	
}
