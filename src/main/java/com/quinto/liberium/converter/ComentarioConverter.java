package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.Comentario;
import com.quinto.liberium.model.MComentario;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.service.ComentarioService;

@Component
public class ComentarioConverter extends Converter<MComentario, Comentario>{

	@Autowired
	private ComentarioService comentarioService;
	
	@Autowired
	private CausaRepository causaRepository;
	
	@Autowired
	private UsuarioConverter usuarioConverter;
	
	public Comentario modeloEntidad(MComentario modelo) {
		Comentario entidad = new Comentario();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			entidad = comentarioService.buscarPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, entidad);

//			Causa causa = causaRepository.buscarPorId(modelo.getCausa().getId());
//			entidad.setCausa(causa);
			
		} catch (Exception e) {
			log.error("Error al convertir el modelo de comentario en entidad", e);
		}
		return entidad;
	}

	public MComentario entidadModelo(Comentario entidad) {
		MComentario modelo = new MComentario();
		try {
			BeanUtils.copyProperties(entidad, modelo);
			modelo.setUsuario(usuarioConverter.entidadModelo(entidad.getUsuario()));
//			modelo.getCausa().setId(entidad.getCausa().getId());

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de comentario", e);
		}
		return modelo;
	}

	public List<MComentario> entidadesModelos(List<Comentario> entidades) {
		List<MComentario> modelos = new ArrayList<>();
		for (Comentario entidad : entidades) {
			modelos.add(entidadModelo(entidad));
		}
		return modelos;
	}

	public List<Comentario> modelosEntidades(List<MComentario> modelos) {
		List<Comentario> entidades = new ArrayList<>();
		for (MComentario modelo : modelos) {
			entidades.add(modeloEntidad(modelo));
		}
		return entidades;
	}
	
}
