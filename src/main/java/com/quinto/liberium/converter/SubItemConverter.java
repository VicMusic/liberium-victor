package com.quinto.liberium.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.service.SubItemService;

@Component
public class SubItemConverter extends Converter<MSubItem, SubItem> {

	@Autowired
	private SubItemService subItemService;
	
	@Autowired
	private ArchivoConverter archivoConverter;
	
	@Autowired
	private EscritoConverter escritoConverter;

	public SubItem modeloEntidad(MSubItem modelo) {
		SubItem subItem = new SubItem();
		if (modelo.getId() != null && !modelo.getId().isEmpty()) {
			subItem = subItemService.buscarSubItemPorId(modelo.getId());
		}
		try {
			BeanUtils.copyProperties(modelo, subItem);


		} catch (Exception e) {
			log.error("Error al convertir el modelo del sub item en entidad", e);
		}
		return subItem;

	}

	public MSubItem entidadModelo(SubItem subItem) {
		MSubItem modelo = new MSubItem();
		try {
			BeanUtils.copyProperties(subItem, modelo);
			modelo.setSubNombre(subItem.getNombre());
			
			if (subItem.getArchivos() != null) {
				modelo.setmArchivo(archivoConverter.entidadesModelos(subItem.getArchivos()));
			}
			
			if (subItem.getEscritos() != null) {
				modelo.setmEscritos(escritoConverter.entidadesModelos(subItem.getEscritos()));
			}

		} catch (Exception e) {
			log.error("Error al convertir la entidad en el modelo de sub item", e);
		}
		return modelo;
	}

	public List<MSubItem> entidadesModelos(List<SubItem> subItems) {
		List<MSubItem> modelos = new ArrayList<>();
		for (SubItem subItem : subItems) {
			modelos.add(entidadModelo(subItem));
		}
		return modelos;
	}

	public List<SubItem> modelosEntidades(List<MSubItem> mSubItems) {
		List<SubItem> entidades = new ArrayList<>();
		for (MSubItem mSubItem : mSubItems) {
			entidades.add(modeloEntidad(mSubItem));
		}
		return entidades;
	}

}
