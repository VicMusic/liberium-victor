package com.quinto.liberium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LiberiumApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiberiumApplication.class, args);
	}

}
