package com.quinto.liberium.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.TurnoService;

@Component("NotificacionTask")
public class NotificacionTask {
	private static final Logger log = LoggerFactory.getLogger(NotificacionTask.class);

	@Autowired
	private TurnoService turnoService;
	
	@Autowired
	private CausaService causaService;

	
	@Scheduled(cron = "0 0 0 * * *")
	public void cancelarTurnosPasados() {
		log.info("SCHEDUELED: cancelarTurnosPasados()");
		turnoService.cancelarTurnosAnteriores();
	}
	
	@Scheduled(cron = "0 0 21 * * *")
	public void notificarTurnosDiaAntes() {
		log.info("SCHEDUELED: notificarTurnosDiaAntes()");
		turnoService.notificarTurnosDiaAntes();
	}
	
	@Scheduled(cron = "0 0 0 * * *")
	public void cambiarColoresCausas() {
		log.info("SCHEDUELED: cambiarColoresCausas()");
		causaService.colorBarra(null, null, null, false);
	}
	
}
