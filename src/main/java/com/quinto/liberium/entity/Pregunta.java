package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.quinto.liberium.enumeration.TipoPregunta;

@Entity
public class Pregunta implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String titulo;

	private boolean obligatoria;
	
	private String variableAmigable;
	private String variableValue;
	
	private String inputRespuesta;

	private int orden;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	
	@ManyToOne
	private Usuario editor;
	
	@ManyToOne
	private Usuario eliminador;

	@ManyToOne
	private Formulario formulario;

	@Enumerated(EnumType.STRING)
	private TipoPregunta tipo;


	@OneToMany(fetch=FetchType.LAZY)
	private List<Opcion> opciones;

	public Pregunta() {
		this.opciones = new ArrayList<>();
	}
	
	public String getInputRespuesta() {
		return inputRespuesta;
	}
	
	public void setInputRespuesta(String inputRespuesta) {
		this.inputRespuesta = inputRespuesta;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<Opcion> getOpciones() {
		return opciones;
	}
	
	public void setOpciones(List<Opcion> opciones) {
		this.opciones = opciones;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isObligatoria() {
		return obligatoria;
	}

	public void setObligatoria(boolean obligatoria) {
		this.obligatoria = obligatoria;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	
	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Formulario getFormulario() {
		return formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public TipoPregunta getTipo() {
		return tipo;
	}

	public void setTipo(TipoPregunta tipo) {
		this.tipo = tipo;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}
	
	
	public String getVariableAmigable() {
		return variableAmigable;
	}
	
	public void setVariableAmigable(String variableAmigable) {
		this.variableAmigable = variableAmigable;
	}
	
	
	public void setVariableValue(String variableValue) {
		this.variableValue = variableValue;
	}
	
	public String getVariableValue() {
		return variableValue;
	}
	
	@Override
	public String toString() {
		return "ID: " + id + " Titulo: " + titulo + " Tipo: " + tipo + " Orden: " + orden; 
	}
}
