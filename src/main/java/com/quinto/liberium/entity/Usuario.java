package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.util.JSONBuilder;

@Entity
public class Usuario implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String nombre;

	private String email;

	private String password;

	private String dni;

	private String telefono;

	@Lob
	@JsonIgnore
	@Basic(fetch = FetchType.LAZY)
	private byte[] foto;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDeNacimiento;

	private String resetToken;

	private String confirmToken;

	private boolean enabled;
	private boolean isAbogado;
	private boolean tieneFoto;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Permiso> permisos;
	
	@Transient
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	private String permisosJson;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	private Pais pais;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	private Provincia provincia;

	@Enumerated(EnumType.STRING)
	private TipoUsuario tipoUsuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaLogin;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaLogOut;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario creador;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario editor;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario eliminador;

	public Usuario() {
		super();
	}

	public Usuario(String id, String nombre, String email, String password, String dni, String telefono,
			Date fechaDeNacimiento) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.password = password;
		this.dni = dni;
		this.telefono = telefono;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public boolean isTieneFoto() {
		return tieneFoto;
	}
	
	public boolean isAbogado() {
		return isAbogado;
	}

	public void setAbogado(boolean isAbogado) {
		this.isAbogado = isAbogado;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public void setTieneFoto(boolean tieneFoto) {
		this.tieneFoto = tieneFoto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public String getConfirmToken() {
		return confirmToken;
	}

	public void setConfirmToken(String confirmToken) {
		this.confirmToken = confirmToken;
	}

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public Date getFechaLogin() {
		return fechaLogin;
	}

	public void setFechaLogin(Date fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public Date getFechaLogOut() {
		return fechaLogOut;
	}

	public void setFechaLogOut(Date fechaLogOut) {
		this.fechaLogOut = fechaLogOut;
	}

	public List<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}
	
	public String getPermisosJson() {
		JSONBuilder json = new JSONBuilder();
		json.initArray();
		if(permisos != null && permisos.size() > 0) {
			for (Permiso permiso : permisos) {
				
				JSONBuilder jsonPermiso = new JSONBuilder();
				jsonPermiso.initEntity();
				
				jsonPermiso.add("id", permiso.getId());
				jsonPermiso.add("nombre", permiso.getNombre());
				jsonPermiso.add("nombreAmigable", permiso.getNombreAmigable());
				jsonPermiso.add("administrador", permiso.getAdministrador());
				
				jsonPermiso.endEntity();
				json.add(jsonPermiso);
			}
		}
		
		json.endArray();
		return json.toJSON();
	}
	
}