package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Miembro implements Serializable, Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
	private Usuario usuario;

    @ManyToOne
    private Estudio estudio;
    
    @ManyToOne
    private Rol rol;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	
	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;

    public Miembro() {
	super();
    }

    public Miembro(String id, Usuario usuario, Estudio estudio, Rol rol, Date creado, Date editado, Date eliminado,
	    Usuario creador, Usuario editor, Usuario eliminador) {
	super();
	this.id = id;
	this.usuario = usuario;
	this.estudio = estudio;
	this.rol = rol;
	this.creado = creado;
	this.editado = editado;
	this.eliminado = eliminado;
	this.creador = creador;
	this.editor = editor;
	this.eliminador = eliminador;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
	public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getEditado() {
        return editado;
    }

    public void setEditado(Date editado) {
        this.editado = editado;
    }

    public Date getEliminado() {
        return eliminado;
    }

    public void setEliminado(Date eliminado) {
        this.eliminado = eliminado;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void setCreador(Usuario creador) {
        this.creador = creador;
    }

    public Usuario getEditor() {
        return editor;
    }

    public void setEditor(Usuario editor) {
        this.editor = editor;
    }

    public Usuario getEliminador() {
        return eliminador;
    }

    public void setEliminador(Usuario eliminador) {
        this.eliminador = eliminador;
    }
    
}