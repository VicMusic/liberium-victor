package com.quinto.liberium.entity;

import java.util.Date;

public interface Auditable {


	public Date getCreado();
	public void setCreado(Date creado);
	public Usuario getCreador(); 
	public void setCreador(Usuario creador);
	public Date getEditado();
	public void setEditado(Date editado);
	public Usuario getEditor();
	public void setEditor(Usuario editor);
	public Date getEliminado();
	public void setEliminado(Date eliminado);
	public Usuario getEliminador();
	public void setEliminador(Usuario eliminador);
	
	
}