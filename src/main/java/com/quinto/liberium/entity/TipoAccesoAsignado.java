package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class TipoAccesoAsignado implements Serializable,Auditable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	
	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Date getCreado() {
		return creado;
	}

	@Override
	public void setCreado(Date creado) {
		this.creado = creado;		
	}

	@Override
	public Usuario getCreador() {
		return creador;
	}

	@Override
	public void setCreador(Usuario creador) {
		this.creador = creador;		
	}

	@Override
	public Date getEditado() {
		return editado;
	}

	@Override
	public void setEditado(Date editado) {
		this.editado =editado;
	}

	@Override
	public Usuario getEditor() {
		return editor;
	}

	@Override
	public void setEditor(Usuario editor) {
		this.editor = editor;		
	}

	@Override
	public Date getEliminado() {
		return eliminado;
	}

	@Override
	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;		
	}

	@Override
	public Usuario getEliminador() {
		return eliminador;
	}

	@Override
	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;		
	}
	
}
