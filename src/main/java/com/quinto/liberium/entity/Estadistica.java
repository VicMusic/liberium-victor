package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Estadistica implements  Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String nombre;
	
	
	@ElementCollection(targetClass=Integer.class, fetch = FetchType.EAGER)
	@MapKeyColumn(name="mes")
	private Map<String, Integer> cantidadPorMes = new LinkedHashMap<>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Estudio estudio;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	
	@ManyToOne
	private Usuario creador;
	
	@ManyToOne
	private Usuario editor;
	
	@ManyToOne
	private Usuario eliminador;

	public Estadistica() {
		super();
	}

	public Estadistica(String id, String nombre, Map<String, Integer> cantidadPorMes, Date creado, Date editado, Date eliminado,
			Usuario creador, Usuario editor, Usuario eliminador) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cantidadPorMes = cantidadPorMes;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
		this.creador = creador;
		this.editor = editor;
		this.eliminador = eliminador;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Map<String, Integer> getCantidadPorMes() {
		return cantidadPorMes;
	}

	public void setCantidadPorMes(Map<String, Integer> cantidadPorMes) {
		this.cantidadPorMes = cantidadPorMes;
	}


	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Estudio getEstudio() {
		return estudio;
	}

	public void setEstudio(Estudio estudio) {
		this.estudio = estudio;
	}
	
}
