package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;

@Entity
public class Checklist implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String nombre;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Pais pais;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Provincia provincia;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Circunscripcion circunscripcion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Instancia instancia;
	
	@ManyToOne
	private Materia materia;
	
	@ManyToOne
	private TipoCausa tipoCausa;
	
	@ManyToOne
	private Formulario formulario;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;
	
	@Enumerated(EnumType.STRING)
	private TipoRolCausa rol;
	
	@Enumerated(EnumType.STRING)
	private TipoFuero tipoFuero;
	
	private String visibilidad;
	
	private boolean plantilla;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}
	
	public Materia getMateria() {
		return materia;
	}
	
	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public TipoCausa getTipoCausa() {
		return tipoCausa;
	}

	public void setTipoCausa(TipoCausa tipoCausa) {
		this.tipoCausa = tipoCausa;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TipoRolCausa getRol() {
		return rol;
	}

	public void setRol(TipoRolCausa rol) {
		this.rol = rol;
	}

	public Circunscripcion getCircunscripcion() {
		return circunscripcion;
	}

	public void setCircunscripcion(Circunscripcion circunscripcion) {
		this.circunscripcion = circunscripcion;
	}

	public Instancia getInstancia() {
		return instancia;
	}

	public void setInstancia(Instancia instancia) {
		this.instancia = instancia;
	}

	public TipoFuero getTipoFuero() {
		return tipoFuero;
	}

	public void setTipoFuero(TipoFuero tipoFuero) {
		this.tipoFuero = tipoFuero;
	}

	public Formulario getFormulario() {
		return formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public String getVisibilidad() {
		return visibilidad;
	}

	public void setVisibilidad(String visibilidad) {
		this.visibilidad = visibilidad;
	}

	/**
	 * @return the plantilla
	 */
	public boolean isPlantilla() {
		return plantilla;
	}

	/**
	 * @param plantilla the plantilla to set
	 */
	public void setPlantilla(boolean plantilla) {
		this.plantilla = plantilla;
	}
}
