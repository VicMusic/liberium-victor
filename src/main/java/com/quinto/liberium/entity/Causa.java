package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quinto.liberium.enumeration.ModoCausa;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;

@Entity
public class Causa implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String actor;
	
	private String demandado;

	private String nombre;

	private String numeroExpediente;
	
	private String color;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Juzgado juzgadoOficina;

	private Integer estado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Consulta consulta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Estudio estudio;

	@ManyToOne
	private TipoCausa tipoCausa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Instancia instancia;

	@ManyToOne
	private CausaVinculada causaVinculada;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Checklist checklist;

	@OneToMany(fetch = FetchType.LAZY)
	@JsonIgnore 
	private List<Item> items;

	@ManyToMany(fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<Miembro> miembrosCausa;
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<Contacto> contacto;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	
	@ManyToOne
	private Usuario editor;
	
	@ManyToOne
	private Usuario eliminador;
	
	@Enumerated(EnumType.STRING)
	private TipoRolCausa rol;
	
	@Enumerated(EnumType.STRING)
	private ModoCausa modo;
	
	@Enumerated(EnumType.STRING)
	private TipoFuero fuero;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Pais pais;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore 
	private Provincia provincia;

	public Causa() {
		super();
	}

	public Causa(String id, String actor, String demandado, String nombre, String numeroExpediente, String color, Juzgado juzgadoOficina,
			Integer estado, Estudio estudio, TipoCausa tipoCausa, Instancia instancia, CausaVinculada causaVinculada, Checklist checklist,
			List<Item> items, Set<Miembro> miembroCausa, List<Contacto> contacto, Date creado, Date editado, Date eliminado,
			Usuario creador, Usuario editor, Usuario eliminador, TipoRolCausa rol, ModoCausa modo, TipoFuero fuero, Provincia provincia, Pais pais) {
		super();
		this.id = id;
		this.actor = actor;
		this.demandado = demandado;
		this.nombre = nombre;
		this.numeroExpediente = numeroExpediente;
		this.color = color;
		this.juzgadoOficina = juzgadoOficina;
		this.estado = estado;
		this.estudio = estudio;
		this.tipoCausa = tipoCausa;
		this.instancia = instancia;
		this.items = items;
		this.checklist = checklist;
		this.causaVinculada = causaVinculada;
		this.miembrosCausa = miembroCausa;
		this.contacto = contacto;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
		this.creador = creador;
		this.editor = editor;
		this.eliminador = eliminador;
		this.rol = rol;
		this.modo = modo;
		this.fuero = fuero;
		this.provincia = provincia;
		this.pais = pais;
	}
	
	public Consulta getConsulta() {
		return consulta;
	}
	
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Contacto> getContacto() {
		return contacto;
	}

	public void setContacto(List<Contacto> contacto) {
		this.contacto = contacto;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getDemandado() {
		return demandado;
	}

	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public CausaVinculada getCausaVinculada() {
		return causaVinculada;
	}

	public void setCausaVinculada(CausaVinculada causaVinculada) {
		this.causaVinculada = causaVinculada;
	}

	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
	public Set<Miembro> getMiembrosCausa() {
		return miembrosCausa;
	}
	public void setMiembrosCausa(Set<Miembro> miembrosCausa) {
		this.miembrosCausa = miembrosCausa;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Juzgado getJuzgadoOficina() {
		return juzgadoOficina;
	}

	public void setJuzgadoOficina(Juzgado juzgadoOficina) {
		this.juzgadoOficina = juzgadoOficina;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Estudio getEstudio() {
		return estudio;
	}

	public void setEstudio(Estudio estudio) {
		this.estudio = estudio;
	}

	public TipoCausa getTipoCausa() {
		return tipoCausa;
	}

	public void setTipoCausa(TipoCausa tipoCausa) {
		this.tipoCausa = tipoCausa;
	}

	public Instancia getInstancia() {
		return instancia;
	}

	public void setInstancia(Instancia instancia) {
		this.instancia = instancia;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	public TipoRolCausa getRol() {
		return rol;
	}

	public void setRol(TipoRolCausa rol) {
		this.rol = rol;
	}

	public ModoCausa getModo() {
		return modo;
	}

	public void setModo(ModoCausa modo) {
		this.modo = modo;
	}

	public TipoFuero getFuero() {
		return fuero;
	}

	public void setFuero(TipoFuero fuero) {
		this.fuero = fuero;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}
	
	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
}