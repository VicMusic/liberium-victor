package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class ContabilidadCausa implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	private String tipoDePago;
	private String tipoDeTasa;
	private String detalle;
	private String uHtml;

	private Integer orden;
	
	private Double montoSI;
	private Double montoCI;

	@ManyToOne
	private Causa causa;
	
	@ManyToOne
	private Honorario honorario;
	
	@Temporal(TemporalType.DATE)
	private Date desde;
	@Temporal(TemporalType.DATE)
	private Date hasta;
	@Temporal(TemporalType.DATE)
	private Date vencimiento;



	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;

	public Integer getOrden() {
		return orden;
	}
	
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getuHtml() {
		return uHtml;
	}
	
	public void setuHtml(String uHtml) {
		this.uHtml = uHtml;
	}

	public String getTipoDePago() {
		return tipoDePago;
	}
	
	public void setTipoDePago(String tipoDePago) {
		this.tipoDePago = tipoDePago;
	}
	
	public String getTipoDeTasa() {
		return tipoDeTasa;
	}

	public void setTipoDeTasa(String tipoDeTasa) {
		this.tipoDeTasa = tipoDeTasa;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Double getMontoSI() {
		return montoSI;
	}

	public void setMontoSI(Double montoSI) {
		this.montoSI = montoSI;
	}

	public Double getMontoCI() {
		return montoCI;
	}

	public void setMontoCI(Double montoCI) {
		this.montoCI = montoCI;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public Date getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}
	
	public Honorario getHonorario() {
		return honorario;
	}
	
	public void setHonorario(Honorario honorario) {
		this.honorario = honorario;
	}

	public Causa getCausa() {
		return causa;
	}

	public void setCausa(Causa causa) {
		this.causa = causa;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

}
