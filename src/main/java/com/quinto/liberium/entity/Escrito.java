package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Escrito implements Serializable, Auditable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String titulo;
	private String texto;
	
//	este valor me indica si esta anclado a un item o subitem con mayor facilidad
	private boolean anclado;
	
	private boolean original;
	
	@ManyToOne
	private TipoCausa tipoCausa;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario eliminador;
	@ManyToOne
	private Usuario editor;

	public TipoCausa getTipoCausa() {
		return tipoCausa;
	}
	
	public void setTipoCausa(TipoCausa tipoCausa) {
		this.tipoCausa = tipoCausa;
	}
	
	public void setOriginal(boolean original) {
		this.original = original;
	}
	
	public boolean isAnclado() {
		return anclado;
	}
	
	public boolean isOriginal() {
		return original;
	}
	
	public void setAnclado(boolean anclado) {
		this.anclado = anclado;
	}
	public boolean getAnclado() {
		return this.anclado;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	@Override
	public java.util.Date getCreado() {
		return creado;
	}

	@Override
	public void setCreado(java.util.Date creado) {
		this.creado = creado;
	}

	@Override
	public Usuario getCreador() {
		return creador;
	}

	@Override
	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	@Override
	public java.util.Date getEditado() {
		return editado;
	}

	@Override
	public void setEditado(java.util.Date editado) {
		this.editado = editado;
	}

	@Override
	public Usuario getEditor() {
		return editor;
	}

	@Override
	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	@Override
	public java.util.Date getEliminado() {
		return eliminado;
	}

	@Override
	public void setEliminado(java.util.Date eliminado) {
		this.eliminado = eliminado;
	}

	@Override
	public Usuario getEliminador() {
		return eliminador;
	}

	@Override
	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

}
