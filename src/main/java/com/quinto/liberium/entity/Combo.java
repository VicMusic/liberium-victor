package com.quinto.liberium.entity;

import java.util.List;

import com.quinto.liberium.util.JSONBuilder;

public class Combo {
	private List<Seleccionable> seleccionable;

	public Combo(List<Seleccionable> seleccionable) {
		this.seleccionable = seleccionable;
	}
	
	public Combo() {
		
	}
	
	public List<Seleccionable> getSeleccionable() {
		return seleccionable;
	}

	public void setSeleccionable(List<Seleccionable> seleccionable) {
		this.seleccionable = seleccionable;
	}

	public String toJSON(){
		JSONBuilder json = new JSONBuilder();
		json.initArray();
		
		for (Seleccionable objeto : seleccionable) {
			JSONBuilder s = new JSONBuilder();
			
			s.initEntity();
			s.add("id", objeto.getId());
			s.add("value", objeto.getNombre()); 
			s.endEntity();
			
			json.addArray(s.toJSON());
			json.next();
		}
		
		json.endArray();

		return json.toJSON();
	}
	
	public String getCombo() {
		return toJSON();
	}
	
}
