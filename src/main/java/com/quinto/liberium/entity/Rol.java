package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.quinto.liberium.util.JSONBuilder;

@Entity
public class Rol implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String nombre;
	
	private boolean isAbogado;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Permiso> permisos;
	

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario creador;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario editor;

	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario eliminador;
	
	@Transient
	private String permisosJson;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Permiso> getPermisos() {
		return permisos;
	}
	
	public boolean isAbogado() {
		return isAbogado;
	}

	public void setAbogado(boolean isAbogado) {
		this.isAbogado = isAbogado;
	}

	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}
	
	public String getPermisosJson() {
		JSONBuilder json = new JSONBuilder();
		json.initArray();
		for (Permiso permiso : permisos) {
			
			JSONBuilder jsonPermiso = new JSONBuilder();
			jsonPermiso.initEntity();
			
			jsonPermiso.add("id", permiso.getId());
			jsonPermiso.add("nombre", permiso.getNombre());
			jsonPermiso.add("nombreAmigable", permiso.getNombreAmigable());
			jsonPermiso.add("administrador", permiso.getAdministrador());
			
			jsonPermiso.endEntity();
			json.add(jsonPermiso);
		}
		json.endArray();
		return json.toJSON();
	}
	
}