package com.quinto.liberium.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Permiso implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String nombre;
	private String nombreAmigable;
	
	private Boolean administrador;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreAmigable() {
		return nombreAmigable;
	}

	public void setNombreAmigable(String nombreAmigable) {
		this.nombreAmigable = nombreAmigable;
	}

	public Boolean getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}

	public Permiso() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Permiso(String id, String nombre, String nombreAmigable, Boolean administrador) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.nombreAmigable = nombreAmigable;
		this.administrador = administrador;
	}

	

	
	
}