package com.quinto.liberium.entity;

public interface Seleccionable {
	public String getId();
	public String getNombre();
}
