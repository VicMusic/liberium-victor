package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Archivo implements Serializable, Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	// Para este atributo tan solo está el nombre del archivo, no la ruta completa,
	// ya que la ruta es siempre la misma y no me interesa mostrarla
	private String ruta;
	private String contentType;
	private String nombre;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Date getCreado() {
		return creado;
	}

	@Override
	public void setCreado(Date creado) {
		this.creado = creado;

	}

	@Override
	public Usuario getCreador() {
		return creador;
	}

	@Override
	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	@Override
	public Date getEditado() {
		return editado;
	}

	@Override
	public void setEditado(Date editado) {
		this.editado = editado;
	}

	@Override
	public Usuario getEditor() {
		return editor;
	}

	@Override
	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	@Override
	public Date getEliminado() {
		return eliminado;
	}

	@Override
	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	@Override
	public Usuario getEliminador() {
		return eliminador;
	}

	@Override
	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	public Archivo() {
		super();
	}

	public Archivo(String id, String ruta, Date creado, Date editado, Date eliminado, Usuario creador, Usuario editor,
			Usuario eliminador) {
		super();
		this.id = id;
		this.ruta = ruta;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
		this.creador = creador;
		this.editor = editor;
		this.eliminador = eliminador;
	}

}
