package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.enumeration.TipoPlazo;

@Entity
public class Evento implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String titulo;

	private String tipoEvento;

	private String color;
	
	private Integer cantDias;

	private boolean activo;
	
	private boolean isDiaNoLaboral;
	
	@Enumerated(EnumType.STRING)
	private TipoPlazo tipoPlazo;
	
	@Enumerated(EnumType.STRING)
	private TipoDiaNoLaborable tipoDiaNoLaborable;

	@ManyToOne
	private Turno turno;
	
	@JsonIgnore
	@ManyToOne
	private Provincia provincia;
	
	@JsonIgnore
	@ManyToOne
	private Pais pais;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFin;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;

	public Evento() {
		super();
	}

	public boolean isDiaNoLaboral() {
		return isDiaNoLaboral;
	}
	
	public void setDiaNoLaboral(boolean isDiaNoLaboral) {
		this.isDiaNoLaboral = isDiaNoLaboral;
	}
	
	public TipoDiaNoLaborable getTipoDiaNoLaborable() {
		return tipoDiaNoLaborable;
	}
	
	public void setTipoDiaNoLaborable(TipoDiaNoLaborable tipoDiaNoLaborable) {
		this.tipoDiaNoLaborable = tipoDiaNoLaborable;
	}
	
	public Integer getCantDias() {
		return cantDias;
	}
	
	public void setCantDias(Integer cantDias) {
		this.cantDias = cantDias;
	}
	
	public Pais getPais() {
		return pais;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public String getTipoEvento() {
		return tipoEvento;
	}
	
	public Provincia getProvincia() {
		return provincia;
	}
	
	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}
	
	public TipoPlazo getTipoPlazo() {
		return tipoPlazo;
	}
	public void setTipoPlazo(TipoPlazo tipoPlazo) {
		this.tipoPlazo = tipoPlazo;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public boolean isActivo() {
		return activo;
	}

	public Turno getTurno() {
		return turno;
	}

	public String getColor() {
		return color;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getCreado() {
		return creado;
	}

	public void setCreado(Date creado) {
		this.creado = creado;
	}

	public Date getEditado() {
		return editado;
	}

	public void setEditado(Date editado) {
		this.editado = editado;
	}

	public Date getEliminado() {
		return eliminado;
	}

	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public Usuario getEditor() {
		return editor;
	}

	public void setEditor(Usuario editor) {
		this.editor = editor;
	}

	public Usuario getEliminador() {
		return eliminador;
	}

	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}

	@Override
	public String toString() {
		return "Evento [id=" + id + ", titulo=" + titulo + ", tipoEvento=" + tipoEvento + ", color=" + color
				+ ", cantDias=" + cantDias + ", activo=" + activo + ", tipoPlazo=" + tipoPlazo + ", turno=" + turno
				+ ", provincia=" + provincia + ", pais=" + pais + ", fechaInicio=" + fechaInicio + ", fechaFin="
				+ fechaFin + ", creado=" + creado + ", editado=" + editado + ", eliminado=" + eliminado + ", creador="
				+ creador + ", editor=" + editor + ", eliminador=" + eliminador + "]";
	}
	
}