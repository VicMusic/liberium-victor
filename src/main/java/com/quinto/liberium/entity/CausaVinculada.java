package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class CausaVinculada implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private Causa causa;
	private Causa vinculada;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;
	
	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;
	
	
	public CausaVinculada() {
		super();
	}
	public CausaVinculada(String id, Causa causa, Causa vinculada, Date creado, Date editado, Date eliminado,
			Usuario creador, Usuario editor, Usuario eliminador) {
		super();
		this.id = id;
		this.causa = causa;
		this.vinculada = vinculada;
		this.creado = creado;
		this.editado = editado;
		this.eliminado = eliminado;
		this.creador = creador;
		this.editor = editor;
		this.eliminador = eliminador;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Causa getCausa() {
		return causa;
	}
	
	public void setCausa(Causa causa) {
		this.causa = causa;
	}
	
	public Causa getVinculada() {
		return vinculada;
	}
	
	public void setVinculada(Causa vinculada) {
		this.vinculada = vinculada;
	}
	
	public Date getCreado() {
		return creado;
	}
	
	public void setCreado(Date creado) {
		this.creado = creado;
	}
	
	public Date getEditado() {
		return editado;
	}
	
	public void setEditado(Date editado) {
		this.editado = editado;
	}
	
	public Date getEliminado() {
		return eliminado;
	}
	
	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}
	
	public Usuario getCreador() {
		return creador;
	}
	
	public void setCreador(Usuario creador) {
		this.creador = creador;
	}
	
	public Usuario getEditor() {
		return editor;
	}
	
	public void setEditor(Usuario editor) {
		this.editor = editor;
	}
	
	public Usuario getEliminador() {
		return eliminador;
	}
	
	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}
}