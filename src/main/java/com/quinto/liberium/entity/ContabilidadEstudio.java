package com.quinto.liberium.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quinto.liberium.enumeration.TipoTransaccion;

@Entity
public class ContabilidadEstudio implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	private String detalle;
	private String tipoPago;

	private Double monto;
	
	@Temporal(TemporalType.DATE)
	private Date fechaContabilidad;
	
	@Enumerated(EnumType.STRING)
	private TipoTransaccion tipoContabilidad;
	
	@ManyToOne
	private Honorario honorario;
	
	@ManyToOne
	private Miembro miembro;

	@ManyToOne
	@JsonIgnore
	private Estudio estudio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date editado;
	@Temporal(TemporalType.TIMESTAMP)
	private Date eliminado;

	@ManyToOne
	private Usuario creador;
	@ManyToOne
	private Usuario editor;
	@ManyToOne
	private Usuario eliminador;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Date getFechaContabilidad() {
		return fechaContabilidad;
	}
	public void setFechaContabilidad(Date fechaContabilidad) {
		this.fechaContabilidad = fechaContabilidad;
	}
	public TipoTransaccion getTipoContabilidad() {
		return tipoContabilidad;
	}
	public void setTipoContabilidad(TipoTransaccion tipoContabilidad) {
		this.tipoContabilidad = tipoContabilidad;
	}
	public Honorario getHonorario() {
		return honorario;
	}
	public void setHonorario(Honorario honorario) {
		this.honorario = honorario;
	}
	public Miembro getMiembro() {
		return miembro;
	}
	public void setMiembro(Miembro miembro) {
		this.miembro = miembro;
	}
	public Estudio getEstudio() {
		return estudio;
	}
	public void setEstudio(Estudio estudio) {
		this.estudio = estudio;
	}
	public Date getCreado() {
		return creado;
	}
	public void setCreado(Date creado) {
		this.creado = creado;
	}
	public Date getEditado() {
		return editado;
	}
	public void setEditado(Date editado) {
		this.editado = editado;
	}
	public Date getEliminado() {
		return eliminado;
	}
	public void setEliminado(Date eliminado) {
		this.eliminado = eliminado;
	}
	public Usuario getCreador() {
		return creador;
	}
	public void setCreador(Usuario creador) {
		this.creador = creador;
	}
	public Usuario getEditor() {
		return editor;
	}
	public void setEditor(Usuario editor) {
		this.editor = editor;
	}
	public Usuario getEliminador() {
		return eliminador;
	}
	public void setEliminador(Usuario eliminador) {
		this.eliminador = eliminador;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
