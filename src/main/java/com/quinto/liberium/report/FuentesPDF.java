package com.quinto.liberium.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;

public class FuentesPDF {
	public final static Font TITULO = new Font(FontFamily.HELVETICA, 16, Font.BOLD);
	public final static Font SUB_TITULO = new Font(FontFamily.HELVETICA, 12, Font.BOLD);
	
	
	public final static Font NORMAL = new Font(FontFamily.HELVETICA, 10, Font.NORMAL);
	public final static Font NORMAL_MINIMA = new Font(FontFamily.HELVETICA, 8, Font.NORMAL);
	public final static Font NORMAL_TABLA_MINIMA = new Font(FontFamily.HELVETICA, 9, Font.NORMAL, new BaseColor(255, 255, 255));
	public final static Font NORMAL_NEGRITA = new Font(FontFamily.HELVETICA, 10, Font.BOLD);
	
	public final static Font NORMAL_NEGRITA_GRIS = new Font(FontFamily.HELVETICA, 10, Font.BOLD, new BaseColor(90,90,90));
	public final static Font NORMAL_MINIMA_MINIMA = new Font(FontFamily.HELVETICA, 7, Font.NORMAL, new BaseColor(90,90,90));
	public final static Font NORMAL_MINIMA_GRIS = new Font(FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(90,90,90));
	public final static Font NORMAL_TABLA_MINIMA_GRIS = new Font(FontFamily.HELVETICA, 9, Font.NORMAL, new BaseColor(90,90,90));
	
	
}
