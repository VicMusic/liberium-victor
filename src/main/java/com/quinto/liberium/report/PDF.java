package com.quinto.liberium.report;

import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDF<O extends Object> extends Reporte<Document, O> {

	protected PdfWriter writer;
	protected PdfPTable cabecera = new PdfPTable(new float[]{2,8});
	protected PdfPTable footer = new PdfPTable(new float[]{2,8});
	

	public PDF(String titulo, Class<O> clase, Rectangle tamano) {
		super(titulo, new Document(tamano), clase);
		this.titulo = titulo;		
	}
	
	public PDF(String titulo, Class<O> clase) {
		this(titulo,clase, PageSize.A4);
	}
	
	public PdfWriter getWriter() {
		return writer;
	}

	
	public void addCampo(String label, String texto) throws Exception{
		addCampo(label,texto,Paragraph.ALIGN_LEFT);
	}
	
	public void addCampo(String label, String texto, int alineacion) throws Exception{
		Paragraph parrafo = new Paragraph();
		parrafo.setAlignment(alineacion);
		
		if(label == null){
			label = "";
		}
		
		parrafo.add(new Chunk(label, FuentesPDF.NORMAL_NEGRITA));
		if(texto == null){
			texto = "";
		}
		
		parrafo.add(new Chunk(texto, FuentesPDF.NORMAL));
		motor.add(parrafo);
	}
	
	public void addCampoColor(String label, String texto) throws Exception{
		Paragraph parrafo = new Paragraph();
		
		if(label == null){
			label = "";
		}
		
		parrafo.add(new Chunk(label, FuentesPDF.NORMAL_NEGRITA_GRIS));
		if(texto == null){
			texto = "";
		}
		
		parrafo.add(new Chunk(texto, FuentesPDF.NORMAL));
		motor.add(parrafo);
	}
	
	
	public void exportar(OutputStream out){
		try {
			writer = PdfWriter.getInstance(motor, out);
			writer.setPageEvent(new Footer(titulo));
			motor.open();
			generar();
			writer.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void exportarPersonalizado(OutputStream out, int footer){
		try {
			writer = PdfWriter.getInstance(motor, out);
			if(footer == 0){
				writer.setPageEvent(new Footer(titulo));
			}
			motor.open();
			generar();
			writer.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}



	
	public void cabecera() throws Exception {

		cabecera.setWidthPercentage(100);
		
		Image imagen = Image.getInstance(new URL(logo));
		imagen.setAlignment(Image.ALIGN_RIGHT);

		PdfPCell celda1 = new PdfPCell();
		celda1.addElement(imagen);
		celda1.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		celda1.setRowspan(2);
		cabecera.addCell(celda1);
		
		PdfPCell celda2 = new PdfPCell();
		celda2.addElement(new Paragraph("Liberium", FuentesPDF.TITULO));
		celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celda2.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		celda2.setPadding(5);
		cabecera.addCell(celda2);
		
		PdfPCell celda3 = new PdfPCell();
		celda3.addElement(new Paragraph(titulo, FuentesPDF.SUB_TITULO));
		celda3.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celda3.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		celda3.setPadding(5);
		cabecera.addCell(celda3);		
	}
	
	
	public void cuerpo() throws Exception {
		motor.add(cabecera);
		
		PdfPTable tabla = new PdfPTable(columnas.length);
		tabla.setWidthPercentage(100F);
		
		if(tamanos != null){
			tabla.setWidths(tamanos);
		}
		
		for (String columna : columnas) {
			Phrase texto = new Phrase(columna, FuentesPDF.NORMAL_NEGRITA);
			
			PdfPCell celda = new PdfPCell(texto);
			celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celda.setPadding(4);
			tabla.addCell(celda);
		}

		if(objetos != null){
			for (O objeto : objetos) {
	
				Map<String, Object> valores = BeanUtilsBean.getInstance().getPropertyUtils().describe(objeto);
	
				for (String campo : campos) {
					String valor = leerValor(clase, valores, campo);
					
					PdfPCell celda = new PdfPCell(new Phrase(valor, FuentesPDF.NORMAL_MINIMA));
					celda.setPadding(4);
					tabla.addCell(celda);
				}
				
			}
		}

		tabla.setSummary("Total");

		motor.add(tabla);
	}
	
	public PdfPCell crearCeldaTexto(String texto, Font fuente, int alineacion){
		Phrase frase = new Phrase(texto, fuente);
		
		PdfPCell celda = new PdfPCell(frase);
		celda.setPadding(5);
		celda.setHorizontalAlignment(alineacion);
		celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		
		return celda;
	}
	
	public PdfPCell crearCeldaBarcode(String codigo, int alineacion){
		Barcode128 barcode = new Barcode128();
		barcode.setCode(codigo);

		Image image = barcode.createImageWithBarcode(writer.getDirectContent(), BaseColor.BLACK, BaseColor.GRAY);
		image.setWidthPercentage(50);
		image.setAlignment(alineacion);

		
		PdfPCell celda = new PdfPCell(image);
		celda.setPadding(5);
		celda.setHorizontalAlignment(alineacion);
		celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		
		return celda;
	}

	@Override
	public void addCampoColor(String label, String texto, int alineacion) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void footer() throws Exception {
		
		
	}




}
