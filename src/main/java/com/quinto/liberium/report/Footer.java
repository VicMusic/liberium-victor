package com.quinto.liberium.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class Footer extends PdfPageEventHelper {
	private String titulo;
	
	public Footer(String titulo) {
		this.titulo = titulo;
	}
	
	Font ffont = new Font(Font.FontFamily.UNDEFINED, 5, Font.ITALIC);

	public void onEndPage(PdfWriter writer, Document document) {
		PdfContentByte cb = writer.getDirectContent();
		Phrase header = new Phrase("Liberium - " + titulo, ffont);
		ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header, (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 10, 0);
		
		Phrase footer = new Phrase("Página " + writer.getCurrentPageNumber(), ffont);
		ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 10, 0);
	}
	
	
}
