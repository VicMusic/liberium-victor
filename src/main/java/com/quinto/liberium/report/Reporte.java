package com.quinto.liberium.report;

import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;

public abstract class Reporte<Engine extends Object, Objeto extends Object> {

	protected String titulo;
	protected String logo = "http://localhost:8080/img/logoliberium1.png";
	protected String subtitulo;

	protected List<Objeto> objetos;
	protected String[] campos;
	protected String[] columnas;
	protected float[] tamanos;

	protected Class<Objeto> clase;
	
	protected Engine motor;
	

	public Reporte(String titulo, Engine motor, Class<Objeto> clase) {
		this.titulo = titulo;
		this.motor = motor;
		this.clase = clase;
	}
	
	public void generar() {
		try {
			cabecera();
			cuerpo();
			footer();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes"})
	protected String leerValor(Class clase, Map<String, Object> valores, String campo) throws Exception {
		try {
			
			int index = campo.indexOf('.');

			boolean isPrimitive = false;
			if (index > 0) {
				String atributoRelacion = campo.substring(0, index);

				Class relacionClass = clase.getDeclaredField(atributoRelacion).getType();
				Object relacionObject = valores.get(atributoRelacion);

				if (relacionObject != null) {
					Map<String, Object> relacionValores = BeanUtilsBean.getInstance().getPropertyUtils().describe(relacionObject);
					campo = campo.substring(index + 1);
					
					return leerValor(relacionClass, relacionValores, campo);
				} else {
					return "";
				}
			} else {
				Field field = clase.getDeclaredField(campo);
				if(field != null){
					isPrimitive = field.getType().isPrimitive();
				}
			}

			if (isPrimitive) {
				return valores.get(campo).toString();
			} else {
				Object valor = valores.get(campo);

				if (valor == null) {
					valor = "";
				}

//				valor = StringEscapeUtils.escapeHtml4(valor.toString());
				valor = valor.toString().replaceAll("(\r\n|\n)", "<br />");

				return valor.toString();
			}
		} catch (NoSuchFieldException e) {
			if (clase.getSuperclass() != null) {
				return leerValor(clase.getSuperclass(), valores, campo);
			} else {
				return "";
			}
		}
	}


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubtitulo() {
		return subtitulo;
	}

	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	public List<Objeto> getObjetos() {
		return objetos;
	}

	public void setObjetos(List<Objeto> objetos) {
		this.objetos = objetos;
	}

	public String[] getCampos() {
		return campos;
	}

	public void setCampos(String[] campos) {
		this.campos = campos;
	}

	public String[] getColumnas() {
		return columnas;
	}

	public void setColumnas(String[] columnas) {
		this.columnas = columnas;
	}

	public float[] getTamanos() {
		return tamanos;
	}

	public void setTamanos(float[] tamanos) {
		this.tamanos = tamanos;
	}

	public Class<Objeto> getClase() {
		return clase;
	}

	public void setClase(Class<Objeto> clase) {
		this.clase = clase;
	}

	public abstract void cabecera() throws Exception;

	public abstract void footer() throws Exception;

	public abstract void cuerpo() throws Exception;

	public abstract void exportar(OutputStream out);
	
	public abstract void addCampoColor(String label, String texto, int alineacion) throws Exception;
}
