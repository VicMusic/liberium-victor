package com.quinto.liberium.model;

public class MCategoriaEvento {

	private String id;

	private String nombre;
	private String color;
	

	public MCategoriaEvento() {
		super();
	}

	public MCategoriaEvento(String id, String nombre, String color) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.color = color;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
}