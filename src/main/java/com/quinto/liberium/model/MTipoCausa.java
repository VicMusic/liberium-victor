package com.quinto.liberium.model;

import com.quinto.liberium.entity.Seleccionable;

public class MTipoCausa implements Seleccionable {

	private String id;

	private String nombre;

	private MMateria mMateria;

	public MTipoCausa() {
		super();
	}

	public MTipoCausa(String id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MMateria getmMateria() {
		return mMateria;
	}

	public void setmMateria(MMateria mMateria) {
		this.mMateria = mMateria;
	}

}