package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;

public class MCausa {

	private String id;
	
	private String actor;
	
	private String demandado;
	
	private String nombre;

	private String numeroExpediente;
	
	private String color;
	
	private Integer estado;
	
	private MJuzgado juzgadoOficina = new MJuzgado();

	private MConsulta mConsulta = new MConsulta();
	
	private MEstudio mEstudio = new MEstudio();
	
	private MTipoCausa mTipoCausa = new MTipoCausa();
	
	private MInstancia mInstancia = new MInstancia();
	
	private Set<MMiembro> mMiembro = new HashSet<>();
	
	private List<MContacto> mContacto = new ArrayList<>();
	
	private List<MItem> mItem = new ArrayList<>();
	
	private TipoRolCausa rol;
	
	private TipoFuero fuero;
	
	private MChecklist checklist = new MChecklist();
	
	private MPais pais = new MPais();
	
	private MProvincia provincia = new MProvincia();
	
	private MUsuario creador = new MUsuario();
	
	public MCausa() {
		super();
	}
	
	public MCausa(String id, String actor, String demandado, String nombre, String numeroExpediente, String color, Integer estado, MJuzgado juzgadoOficina, MEstudio mEstudio, MTipoCausa mTipoCausa, MInstancia mInstancia, Set<MMiembro> mMiembro, List<MContacto> mContacto, List<MItem> mItem, TipoRolCausa rol, TipoFuero fuero, MUsuario creador, MChecklist checklist, MPais pais, MProvincia provincia) {
		super();
		this.id = id;
		this.actor = actor;
		this.demandado = demandado;
		this.nombre = nombre;
		this.numeroExpediente = numeroExpediente;
		this.color = color;
		this.estado = estado;
		this.juzgadoOficina = juzgadoOficina;
		this.mEstudio = mEstudio;
		this.mTipoCausa = mTipoCausa;
		this.mInstancia = mInstancia;
		this.mMiembro = mMiembro;
		this.mContacto = mContacto;
		this.mItem = mItem;
		this.rol = rol;
		this.fuero = fuero;
		this.checklist = checklist;
		this.pais = pais;
		this.provincia = provincia;
		this.creador = creador;
	}

	public MConsulta getmConsulta() {
		return mConsulta;
	}
	
	public void setmConsulta(MConsulta mConsulta) {
		this.mConsulta = mConsulta;
	}
	
	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}
	
	public String getDemandado() {
		return demandado;
	}

	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}

	public String getNumeroExpediente() {
	    return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
	    this.numeroExpediente = numeroExpediente;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public MJuzgado getJuzgadoOficina() {
	    return juzgadoOficina;
	}

	public void setJuzgadoOficina(MJuzgado juzgadoOficina) {
	    this.juzgadoOficina = juzgadoOficina;
	}

	public MEstudio getmEstudio() {
	    return mEstudio;
	}

	public void setmEstudio(MEstudio mEstudio) {
	    this.mEstudio = mEstudio;
	}

	public MTipoCausa getmTipoCausa() {
	    return mTipoCausa;
	}

	public void setmTipoCausa(MTipoCausa mTipoCausa) {
	    this.mTipoCausa = mTipoCausa;
	}

	public MInstancia getmInstancia() {
	    return mInstancia;
	}

	public void setmInstancia(MInstancia mInstancia) {
	    this.mInstancia = mInstancia;
	}

	public Set<MMiembro> getmMiembro() {
		return mMiembro;
	}
	
	public void setmMiembro(Set<MMiembro> mMiembro) {
		this.mMiembro = mMiembro;
	}

	public List<MContacto> getmContacto() {
		return mContacto;
	}

	public void setmContacto(List<MContacto> mContacto) {
		this.mContacto = mContacto;
	}
	
	public List<MItem> getmItem() {
		return mItem;
	}

	public void setmItem(List<MItem> mItem) {
		this.mItem = mItem;
	}

	public TipoRolCausa getRol() {
		return rol;
	}

	public void setRol(TipoRolCausa rol) {
		this.rol = rol;
	}

	public TipoFuero getFuero() {
		return fuero;
	}

	public void setFuero(TipoFuero fuero) {
		this.fuero = fuero;
	}
	
	public MPais getPais() {
		return pais;
	}

	public void setPais(MPais pais) {
		this.pais = pais;
	}

	public MProvincia getProvincia() {
		return provincia;
	}

	public void setProvincia(MProvincia provincia) {
		this.provincia = provincia;
	}

	public MChecklist getChecklist() {
		return checklist;
	}

	public void setChecklist(MChecklist checklist) {
		this.checklist = checklist;
	}

	public MUsuario getCreador() {
		return creador;
	}

	public void setCreador(MUsuario creador) {
		this.creador = creador;
	}
}