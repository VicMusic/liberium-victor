package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MConsultaTurno {
	private String id;
	private String nombre;
	private boolean solicitado;
	private String fecha;
	private String titulo;
	private Date dia;

	private List<String> disponibles = new ArrayList<>();
	private List<Integer> dias = new ArrayList<>();
	private List<MEvento> horarios = new ArrayList<>();
	
	public List<MEvento> getHorarios() {
		return horarios;
	}
	
	public void setHorarios(List<MEvento> horarios) {
		this.horarios = horarios;
	}
	
	public List<Integer> getDias() {
		return dias;
	}

	public void setDias(List<Integer> dias) {
		this.dias = dias;
	}

	public void setSolicitado(boolean solicitado) {
		this.solicitado = solicitado;
	}

	public boolean isSolicitado() {
		return solicitado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public List<String> getDisponibles() {
		return disponibles;
	}

	public void setDisponibles(List<String> disponibles) {
		this.disponibles = disponibles;
	}

	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
