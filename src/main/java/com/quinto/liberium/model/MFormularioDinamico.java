package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

import com.quinto.liberium.entity.Pregunta;

public class MFormularioDinamico {

	private String idConsulta;
	private String idTipoCausa;
	private String idFormulario;
	private String tituliTipoCausa;

	private List<Pregunta> preguntas = new ArrayList<>();

	public String getIdFormulario() {
		return idFormulario;
	}

	public void setIdFormulario(String idFormulario) {
		this.idFormulario = idFormulario;
	}

	public String getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(String idConsulta) {
		this.idConsulta = idConsulta;
	}

	public String getIdTipoCausa() {
		return idTipoCausa;
	}

	public void setIdTipoCausa(String idTipoCausa) {
		this.idTipoCausa = idTipoCausa;
	}

	public String getTituliTipoCausa() {
		return tituliTipoCausa;
	}

	public void setTituliTipoCausa(String tituliTipoCausa) {
		this.tituliTipoCausa = tituliTipoCausa;
	}

	public List<Pregunta> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}
}
