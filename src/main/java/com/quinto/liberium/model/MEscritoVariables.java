package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

import com.quinto.liberium.entity.Respuesta;

public class MEscritoVariables {
	
	private String idEscrito;
	private String tituloEscrito;
	private String textoEscrito;
	private String idItemSubItem;
	private MCausa causa = new MCausa();
	private MAbogado abogado = new MAbogado();
	private List<Respuesta> respuestas = new ArrayList<>();
	
	public String getIdEscrito() {
		return idEscrito;
	}
	public void setIdEscrito(String idEscrito) {
		this.idEscrito = idEscrito;
	}
	public String getTituloEscrito() {
		return tituloEscrito;
	}
	public void setTituloEscrito(String tituloEscrito) {
		this.tituloEscrito = tituloEscrito;
	}
	public String getTextoEscrito() {
		return textoEscrito;
	}
	public void setTextoEscrito(String textoEscrito) {
		this.textoEscrito = textoEscrito;
	}
	public String getIdItemSubItem() {
		return idItemSubItem;
	}
	public void setIdItemSubItem(String idItemSubItem) {
		this.idItemSubItem = idItemSubItem;
	}
	public MCausa getCausa() {
		return causa;
	}
	public void setCausa(MCausa causa) {
		this.causa = causa;
	}
	public MAbogado getAbogado() {
		return abogado;
	}
	public void setAbogado(MAbogado abogado) {
		this.abogado = abogado;
	}
	public List<Respuesta> getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(List<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}

}
