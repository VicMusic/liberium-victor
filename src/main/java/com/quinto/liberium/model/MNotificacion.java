package com.quinto.liberium.model;

import java.util.Date;

public class MNotificacion {

	private String id;
	
	private String texto;
	
	private String url;
	
	private String idEntidad;
	
	public String getIdEntidad() {
		return idEntidad;
	}
	public void setIdEntidad(String idEntidad) {
		this.idEntidad = idEntidad;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private boolean vista;
	
	private Date fechaNotificacion;
	
	private MUsuario mUsuario = new MUsuario();

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public boolean isVista() {
		return vista;
	}
	public void setVista(boolean vista) {
		this.vista = vista;
	}
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
	public MUsuario getmUsuario() {
		return mUsuario;
	}
	public void setmUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}
}
