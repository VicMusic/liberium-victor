package com.quinto.liberium.model;

import java.util.Date;

import javax.persistence.ManyToOne;

import com.quinto.liberium.enumeration.TipoTransaccion;

public class MFinanza {

	private String id;

	private String detalle;
	private String formaPago;
	private Date fecha;

	private Double saldoAnterior;
	private Double monto;
	
	private TipoTransaccion tipo;

	@ManyToOne
	private MContabilidadEstudio contable = new MContabilidadEstudio();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String nombre) {
		this.detalle = nombre;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Double getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(Double saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public MContabilidadEstudio getContable() {
		return contable;
	}

	public void setContable(MContabilidadEstudio contable) {
		this.contable = contable;
	}

	public TipoTransaccion getTipo() {
		return tipo;
	}

	public void setTipo(TipoTransaccion tipo) {
		this.tipo = tipo;
	}
	
	

}
