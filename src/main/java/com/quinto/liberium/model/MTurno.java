package com.quinto.liberium.model;

import java.util.Date;

public class MTurno {

	private String id;
	private String consulta;
	private String nombreCliente;
	private String telefonoCliente;
	private String mailCliente;
	private String fecha;
	private String hora;
	private String elegido;

	private Date fechaTurno;

	private Double precio;

	private MAbogado mAbogado = new MAbogado();
	private MJuzgado mJuzgado = new MJuzgado();
	private MUsuario mUsuario = new MUsuario();

	public String getElegido() {
		return elegido;
	}
	
	public void setElegido(String elegido) {
		this.elegido = elegido;
	}
	
	public Date getFechaTurno() {
		return fechaTurno;
	}

	public void setFechaTurno(Date fechaTurno) {
		this.fechaTurno = fechaTurno;
	}

	public String getMailCliente() {
		return mailCliente;
	}

	public void setMailCliente(String mailCliente) {
		this.mailCliente = mailCliente;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getFecha() {
		return fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public MAbogado getmAbogado() {
		return mAbogado;
	}

	public void setmAbogado(MAbogado mAbogado) {
		this.mAbogado = mAbogado;
	}

	public MJuzgado getmJuzgado() {
		return mJuzgado;
	}

	public void setmJuzgado(MJuzgado mJuzgado) {
		this.mJuzgado = mJuzgado;
	}

	public MUsuario getmUsuario() {
		return mUsuario;
	}

	public void setmUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getTelefonoCliente() {
		return telefonoCliente;
	}

	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

}
