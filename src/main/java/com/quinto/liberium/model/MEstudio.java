package com.quinto.liberium.model;

import java.util.List;

public class MEstudio {

	private String id;

	private String nombre;
	private MUsuario mUsuario = new MUsuario();
	private List<MAbogado> abogados;

	public MEstudio() {
		super();
	}

	public MEstudio(String id, String nombre, String idUsuario, String nombreUsuario) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.mUsuario.setId(idUsuario);
		this.mUsuario.setNombre(nombreUsuario);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MUsuario getmUsuario() {
		return mUsuario;
	}

	public void setmUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}

	public List<MAbogado> getAbogados() {
		return abogados;
	}

	public void setAbogados(List<MAbogado> abogados) {
		this.abogados = abogados;
	}

}