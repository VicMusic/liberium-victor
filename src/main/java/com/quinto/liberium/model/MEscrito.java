package com.quinto.liberium.model;

public class MEscrito {
	
	private String id;
	private String titulo;
	private String texto;
	private boolean anclado;
	private boolean original;
	
	private MTipoCausa mTipoCausa = new MTipoCausa();
	
	public MTipoCausa getmTipoCausa() {
		return mTipoCausa;
	}
	
	public void setmTipoCausa(MTipoCausa mTipoCausa) {
		this.mTipoCausa = mTipoCausa;
	}
	
	public void setAnclado(boolean anclado) {
		this.anclado = anclado;
	}
	public boolean getAnclado() {
		return anclado;
	}
	
	public boolean isAnclado() {
		return anclado;
	}
	
	public boolean isOriginal() {
		return original;
	}
	
	public void setOriginal(boolean original) {
		this.original = original;
	}
	
	public MEscrito() {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}



	@Override
	public String toString() {
		return "MEscrito [id=" + id + ", titulo=" + titulo + ", anclado="+ anclado +  "]";
	}

	
}
