package com.quinto.liberium.model;

import com.quinto.liberium.enumeration.TipoBloque;

public class MBloque {
	
	private String id;
	
	private String texto;
	
	private TipoBloque tipoBloque;
	
	
	public MBloque(){}
	
	public MBloque(String id, String texto, TipoBloque tipoBloque){
		this.id = id;
		this.texto = texto;
		this.tipoBloque = tipoBloque;
	}
	
	

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setTexto(String texto){
		this.texto = texto;
	}
	
	public String getTexto() {
		return texto;
	}
	
	public void setTipoBloque(TipoBloque tipoBloque) {
		this.tipoBloque = tipoBloque;
	}
	
	public TipoBloque getTipoBloque() {
		return tipoBloque;
	}

	@Override
	public String toString() {
		return "MBloque [id=" + id + ", texto=" + texto + ", tipoBloque=" + tipoBloque + "]";
	}

}
