package com.quinto.liberium.model;

import java.util.Date;

import com.quinto.liberium.enumeration.TipoTransaccion;

public class MContabilidadEstudio {

	private String id;
	private String detalle;
	private String tipoPago;
	
	private Double monto;
	
	private Date fechaContabilidad;
	
	private TipoTransaccion tipoContabilidad;
	
	private MHonorario mHonorario = new MHonorario();
	private MMiembro mMiembro = new MMiembro();
	private MEstudio mEstudio = new MEstudio();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Date getFechaContabilidad() {
		return fechaContabilidad;
	}
	public void setFechaContabilidad(Date fechaContabilidad) {
		this.fechaContabilidad = fechaContabilidad;
	}
	public TipoTransaccion getTipoContabilidad() {
		return tipoContabilidad;
	}
	public void setTipoContabilidad(TipoTransaccion tipoContabilidad) {
		this.tipoContabilidad = tipoContabilidad;
	}
	public MHonorario getmHonorario() {
		return mHonorario;
	}
	public void setmHonorario(MHonorario mHonorario) {
		this.mHonorario = mHonorario;
	}
	public MMiembro getmMiembro() {
		return mMiembro;
	}
	public void setmMiembro(MMiembro mMiembro) {
		this.mMiembro = mMiembro;
	}
	public MEstudio getmEstudio() {
		return mEstudio;
	}
	public void setmEstudio(MEstudio mEstudio) {
		this.mEstudio = mEstudio;
	}

}
