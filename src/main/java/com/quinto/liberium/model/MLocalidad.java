package com.quinto.liberium.model;

public class MLocalidad {

	private String id;
	private String nombre;
	
	private MProvincia provincia = new MProvincia();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MProvincia getProvincia() {
		return provincia;
	}

	public void setProvincia(MProvincia provincia) {
		this.provincia = provincia;
	}
	
	
	
}
