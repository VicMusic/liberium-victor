package com.quinto.liberium.model;

import java.util.Date;

import com.quinto.liberium.entity.Usuario;

public class MCredencial {

	private String id;

	private MUsuario mUsuario = new MUsuario();

	private String referencia;

	private String ipEmision;

	private String ipUltimoUso;

	private Date fechaUltimoUso;

	private String token;

	public MCredencial() {
		super();
	}

	public MCredencial(String id, Usuario usuario, String referencia, String ipEmision, String ipUltimoUso,
			Date fechaUltimoUso, String token, String idUsuario, String nombreUsuario) {
		super();
		this.id = id;
		this.mUsuario.setId(idUsuario);
		this.mUsuario.setNombre(nombreUsuario);
		this.referencia = referencia;
		this.ipEmision = ipEmision;
		this.ipUltimoUso = ipUltimoUso;
		this.fechaUltimoUso = fechaUltimoUso;
		this.token = token;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIpEmision() {
		return ipEmision;
	}

	public void setIpEmision(String ipEmision) {
		this.ipEmision = ipEmision;
	}

	public String getIpUltimoUso() {
		return ipUltimoUso;
	}

	public void setIpUltimoUso(String ipUltimoUso) {
		this.ipUltimoUso = ipUltimoUso;
	}

	public Date getFechaUltimoUso() {
		return fechaUltimoUso;
	}

	public void setFechaUltimoUso(Date fechaUltimoUso) {
		this.fechaUltimoUso = fechaUltimoUso;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MUsuario getMUsuario() {
		return mUsuario;
	}

	public void setMUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}
	
	

}