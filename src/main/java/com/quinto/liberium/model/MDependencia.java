package com.quinto.liberium.model;

public class MDependencia{

	private String id;
	private String nombre;
	
	private MTipoDependencia mTipoDependencia = new MTipoDependencia();
	private MInstancia mInstancia = new MInstancia();
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public MTipoDependencia getmTipoDependencia() {
		return mTipoDependencia;
	}
	public void setmTipoDependencia(MTipoDependencia mTipoDependencia) {
		this.mTipoDependencia = mTipoDependencia;
	}
	public MInstancia getmInstancia() {
		return mInstancia;
	}
	public void setmInstancia(MInstancia mInstancia) {
		this.mInstancia = mInstancia;
	}
	
	
}
