package com.quinto.liberium.model;

public class MCausaContacto {

	private String id;

	private MCausa mCausa = new MCausa();
	
	private MContacto mContacto = new MContacto();
	
	public MCausaContacto() {
		super();
	}

	public MCausaContacto(String id, String causaId, String contactoId) {
		super();
		this.id = id;
		this.mCausa.setId(causaId);
		this.mContacto.setId(contactoId);
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}

	public MCausa getmCausa() {
		return mCausa;
	}

	public void setmCausa(MCausa mCausa) {
		this.mCausa = mCausa;
	}

	public MContacto getmContacto() {
	    return mContacto;
	}

	public void setmContacto(MContacto mContacto) {
	    this.mContacto = mContacto;
	}
	
}