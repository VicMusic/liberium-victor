package com.quinto.liberium.model;

public class MChecklist {

	private String id;
	private String nombre;
	private String visibilidad;
	private boolean plantilla;
	private String tipoRolCausa;
	private String fuero;

	private MPais mPais = new MPais();
	private MProvincia mProvincia = new MProvincia();
	private MCircunscripcion mCircunscripcion = new MCircunscripcion();
	private MMateria mMateria = new MMateria();
	private MInstancia mInstancia = new MInstancia();
	private MTipoCausa mTipoCausa = new MTipoCausa();
	private MFormulario mFormulario = new MFormulario();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MPais getmPais() {
		return mPais;
	}

	public void setmPais(MPais mPais) {
		this.mPais = mPais;
	}

	public MProvincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}

	public MTipoCausa getmTipoCausa() {
		return mTipoCausa;
	}

	public void setmTipoCausa(MTipoCausa mTipoCausa) {
		this.mTipoCausa = mTipoCausa;
	}

	public MCircunscripcion getmCircunscripcion() {
		return mCircunscripcion;
	}

	public void setmCircunscripcion(MCircunscripcion mCircunscripcion) {
		this.mCircunscripcion = mCircunscripcion;
	}

	public MInstancia getmInstancia() {
		return mInstancia;
	}

	public void setmInstancia(MInstancia mInstancia) {
		this.mInstancia = mInstancia;
	}

	public MFormulario getmFormulario() {
		return mFormulario;
	}

	public void setmFormulario(MFormulario mFormulario) {
		this.mFormulario = mFormulario;
	}

	/**
	 * @return the visibilidad
	 */
	public String getVisibilidad() {
		return visibilidad;
	}

	/**
	 * @param visibilidad the visibilidad to set
	 */
	public void setVisibilidad(String visibilidad) {
		this.visibilidad = visibilidad;
	}

	/**
	 * @return the plantilla
	 */
	public boolean isPlantilla() {
		return plantilla;
	}

	/**
	 * @param plantilla the plantilla to set
	 */
	public void setPlantilla(boolean plantilla) {
		this.plantilla = plantilla;
	}

	/**
	 * @return the tipoRolCausa
	 */
	public String getTipoRolCausa() {
		return tipoRolCausa;
	}

	/**
	 * @param tipoRolCausa the tipoRolCausa to set
	 */
	public void setTipoRolCausa(String tipoRolCausa) {
		this.tipoRolCausa = tipoRolCausa;
	}

	/**
	 * @return the fuero
	 */
	public String getFuero() {
		return fuero;
	}

	/**
	 * @param fuero the fuero to set
	 */
	public void setFuero(String fuero) {
		this.fuero = fuero;
	}

	/**
	 * @return the mMateria
	 */
	public MMateria getmMateria() {
		return mMateria;
	}

	/**
	 * @param mMateria the mMateria to set
	 */
	public void setmMateria(MMateria mMateria) {
		this.mMateria = mMateria;
	}

}
