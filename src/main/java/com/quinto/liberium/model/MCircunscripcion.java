package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

import com.quinto.liberium.entity.Seleccionable;

public class MCircunscripcion implements Seleccionable {

	private String id;
	private String nombre;

	private MProvincia mProvincia = new MProvincia();
	private List<MLocalidad> mLocalidades = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MProvincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}

	public List<MLocalidad> getmLocalidades() {
		return mLocalidades;
	}

	public void setmLocalidades(List<MLocalidad> mLocalidades) {
		this.mLocalidades = mLocalidades;
	}

}
