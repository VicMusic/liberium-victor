package com.quinto.liberium.model;

public class MMiembro {

    private String id;

    private MUsuario mUsuario = new MUsuario();

    private MEstudio mEstudio = new MEstudio();

    private MRol mRol = new MRol();

    public MMiembro() {
	super();
    }

    public MMiembro(String id, String nombreUsuario, String emailUsuario, String nombreEstudio, String idRol, String nombreRol) {
		super();
		this.id = id;
		this.mUsuario.setNombre(nombreUsuario);
		this.mUsuario.setEmail(emailUsuario);
		this.mEstudio.setNombre(nombreEstudio);
		this.mRol.setId(idRol);
		this.mRol.setNombre(nombreRol);
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public MUsuario getmUsuario() {
	return mUsuario;
    }

    public void setmUsuario(MUsuario mUsuario) {
	this.mUsuario = mUsuario;
    }

    public MEstudio getmEstudio() {
	return mEstudio;
    }

    public void setmEstudio(MEstudio mEstudio) {
	this.mEstudio = mEstudio;
    }

    public MRol getmRol() {
	return mRol;
    }

    public void setmRol(MRol mRol) {
	this.mRol = mRol;
    }

}