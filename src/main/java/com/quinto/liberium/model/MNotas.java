package com.quinto.liberium.model;

public class MNotas {

	private String id;
	private String tituloNota;
	private String textoNota;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTituloNota() {
		return tituloNota;
	}

	public void setTituloNota(String tituloNota) {
		this.tituloNota = tituloNota;
	}

	public String getTextoNota() {
		return textoNota;
	}

	public void setTextoNota(String textoNota) {
		this.textoNota = textoNota;
	}
	
}
