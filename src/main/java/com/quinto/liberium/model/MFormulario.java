package com.quinto.liberium.model;

public class MFormulario {
	
	private String id;
	private String titulo;
	private MTipoCausa idTipoCausa = new MTipoCausa();
	private MPais mPais = new MPais();
	private MProvincia mProvincia = new MProvincia();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public MTipoCausa getIdTipoCausa() {
		return idTipoCausa;
	}
	
	public void setIdTipoCausa(MTipoCausa idTipoCausa) {
		this.idTipoCausa = idTipoCausa;
	}
	
	public MPais getmPais() {
		return mPais;
	}
	
	public void setmPais(MPais mPais) {
		this.mPais = mPais;
	}
	
	public MProvincia getmProvincia() {
		return mProvincia;
	}
	
	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}
}
