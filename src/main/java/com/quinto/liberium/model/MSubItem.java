package com.quinto.liberium.model;

import java.util.List;

public class MSubItem {

	private String id;
	private String subNombre;
	
	private boolean estado;

	private List<MEscrito> mEscritos;

	private List<MArchivo> mArchivo;

	private Integer orden;
	
	private Integer limite;
	
	private Boolean habil;
	
	public List<MArchivo> getmArchivo() {
		return mArchivo;
	}

	public void setmArchivo(List<MArchivo> mArchivo) {
		this.mArchivo = mArchivo;
	}

	public void setmEscritos(List<MEscrito> mEscritos) {
		this.mEscritos = mEscritos;
	}
	
	public List<MEscrito> getmEscritos() {
		return mEscritos;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubNombre() {
		return subNombre;
	}

	public void setSubNombre(String subNombre) {
		this.subNombre = subNombre;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getLimite() {
		return limite;
	}

	public void setLimite(Integer limite) {
		this.limite = limite;
	}

	public Boolean getHabil() {
		return habil;
	}

	public void setHabil(Boolean habil) {
		this.habil = habil;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
}
