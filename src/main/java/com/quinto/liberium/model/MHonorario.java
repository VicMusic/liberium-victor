package com.quinto.liberium.model;

public class MHonorario {

	private String id;
	private String nombre;
	private String tipoHonorario;
	private boolean tipoInstancia;
	private MCausa mCausa = new MCausa();
	private MEstudio estudio = new MEstudio();
	
	public String getId() {
		return id;
	}
	
	public String getTipoHonorario() {
		return tipoHonorario;
	}
	
	public void setTipoHonorario(String tipoHonorario) {
		this.tipoHonorario = tipoHonorario;
	}
	
	public MCausa getmCausa() {
		return mCausa;
	}
	
	public MEstudio getEstudio() {
		return estudio;
	}
	
	public void setEstudio(MEstudio estudio) {
		this.estudio = estudio;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setmCausa(MCausa mCausa) {
		this.mCausa = mCausa;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isTipoInstancia() {
		return tipoInstancia;
	}
	
	public void setTipoInstancia(boolean tipoInstancia) {
		this.tipoInstancia = tipoInstancia;
	}
}
