package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

public class MPlantilla {

	private String id;

	private String titulo;
	
	private List<MBloque> bloques = new ArrayList<>();

	public MPlantilla() {
		super();
	}

	public MPlantilla(String id, String titulo) {
		super();
		this.id = id;
		this.titulo = titulo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<MBloque> getBloques() {
		return bloques;
	}

	public void setBloques(List<MBloque> bloques) {
		this.bloques = bloques;
	}

	
	

}
