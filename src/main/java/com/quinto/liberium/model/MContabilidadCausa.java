package com.quinto.liberium.model;

import java.util.Date;

public class MContabilidadCausa {

	private String id;
	private String tipoDePago;
	private String tipoDeTasa;
	private String detalle;
	private String uHtml;

	private Integer orden;
	
	private Double montoSI;
	private Double montoCI;

	private Date desde;
	private Date hasta;
	private Date vencimiento;
	
	private MCausa mCausa = new MCausa();
	private MHonorario mHonorario = new MHonorario();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public MHonorario getmHonorario() {
		return mHonorario;
	}
	
	public void setmHonorario(MHonorario mHonorario) {
		this.mHonorario = mHonorario;
	}

	public String getTipoDePago() {
		return tipoDePago;
	}
	
	public void setTipoDePago(String tipoDePago) {
		this.tipoDePago = tipoDePago;
	}
	
	public Integer getOrden() {
		return orden;
	}
	
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	public void setuHtml(String uHtml) {
		this.uHtml = uHtml;
	}
	
	public String getuHtml() {
		return uHtml;
	}
	
	public String getTipoDeTasa() {
		return tipoDeTasa;
	}

	public void setTipoDeTasa(String tipoDeTasa) {
		this.tipoDeTasa = tipoDeTasa;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Double getMontoSI() {
		return montoSI;
	}

	public void setMontoSI(Double montoSI) {
		this.montoSI = montoSI;
	}

	public Double getMontoCI() {
		return montoCI;
	}
	
	public void setMontoCI(Double montoCI) {
		this.montoCI = montoCI;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public Date getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}

	public MCausa getmCausa() {
		return mCausa;
	}

	public void setmCausa(MCausa mCausa) {
		this.mCausa = mCausa;
	}

}
