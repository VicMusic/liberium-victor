package com.quinto.liberium.model;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.enumeration.TipoGenero;

public class MConsulta {

	private String id;
	private String nombre;
	private String apellido;
	private String mail;
	private String dni;
	private String direccion;
	private String telefono;

	private TipoGenero genero;
	private MTipoCausa mTipoCausa = new MTipoCausa();
	private Localidad mLocalidad = new Localidad();
	private Formulario mFormulario = new Formulario();
	private Turno mTurno = new Turno();

	@Temporal(TemporalType.TIMESTAMP)
	private Date nacimiento;
	private Integer edad;

	public Turno getmTurno() {
		return mTurno;
	}

	public void setmTurno(Turno mTurno) {
		this.mTurno = mTurno;
	}

	public Formulario getmFormulario() {
		return mFormulario;
	}

	public void setmFormulario(Formulario mFormulario) {
		this.mFormulario = mFormulario;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Localidad getmLocalidad() {
		return mLocalidad;
	}

	public void setmLocalidad(Localidad mLocalidad) {
		this.mLocalidad = mLocalidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public TipoGenero getGenero() {
		return genero;
	}

	public void setGenero(TipoGenero genero) {
		this.genero = genero;
	}

	public MTipoCausa getmTipoCausa() {
		return mTipoCausa;
	}
	
	public void setmTipoCausa(MTipoCausa mTipoCausa) {
		this.mTipoCausa = mTipoCausa;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

}
