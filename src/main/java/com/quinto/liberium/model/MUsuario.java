package com.quinto.liberium.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.util.Fecha;

public class MUsuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String nombre;

	private String email;

	private String password;

	private TipoUsuario tipoUsuario;
	
	private List<MPermiso> permisos;

	private String dni;

	private String telefono;

	private boolean tieneFoto;

	private byte[] foto;

	private Date fechaDeNacimiento;
	
	private MPais mPais = new MPais();

	private boolean enabled;
	private boolean isAbogado;
	
	public MPais getmPais() {
		return mPais;
	}
	
	public void setmPais(MPais mPais) {
		this.mPais = mPais;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public boolean isAbogado() {
		return isAbogado;
	}

	public void setAbogado(boolean isAbogado) {
		this.isAbogado = isAbogado;
	}

	public MUsuario() {
		super();
	}

	public MUsuario(String id, String nombre, String email, String password, String dni, String telefono,
			Date fechaDeNacimiento) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.password = password;
		this.dni = dni;
		this.telefono = telefono;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public boolean isTieneFoto() {
		return tieneFoto;
	}

	public void setTieneFoto(boolean tieneFoto) {
		this.tieneFoto = tieneFoto;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public String getFechaIngles() {
		if (fechaDeNacimiento != null) {
			return Fecha.FECHA_GUIONES_INGLES.format(fechaDeNacimiento);
		} else {
			return "";
		}
	}

	public String getFecha() {
		if (fechaDeNacimiento != null) {
			return Fecha.FECHA_GUIONES.format(fechaDeNacimiento);
		} else {
			return "";
		}
	}

	public List<MPermiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<MPermiso> permisos) {
		this.permisos = permisos;
	}
	
	

}