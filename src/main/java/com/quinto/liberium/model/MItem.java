package com.quinto.liberium.model;

import java.util.List;

public class MItem {

	private String id;
	private String nombre;

	private MChecklist mChecklists = new MChecklist();

	private List<MEscrito> mEscritos;
	
	private List<MArchivo> mArchivo;
	
	private Integer orden;
	
	private Integer limite;
	private Boolean habil;
	private boolean estado;
	
	public List<MSubItem> mSubItem;
	
	public List<MArchivo> getmArchivo() {
		return mArchivo;
	}
	public void setmArchivo(List<MArchivo> mArchivo) {
		this.mArchivo = mArchivo;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MChecklist getmChecklists() {
		return mChecklists;
	}

	public void setmChecklists(MChecklist mChecklists) {
		this.mChecklists = mChecklists;
	}
	
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public List<MSubItem> getmSubItem() {
		return mSubItem;
	}
	public void setmSubItem(List<MSubItem> mSubItem) {
		this.mSubItem = mSubItem;
	}
	public Integer getLimite() {
		return limite;
	}
	public void setLimite(Integer limite) {
		this.limite = limite;
	}
	public List<MEscrito> getmEscritos() {
		return mEscritos;
	}
	public void setmEscritos(List<MEscrito> mEscritos) {
		this.mEscritos = mEscritos;
	}
	public Boolean getHabil() {
		return habil;
	}
	public void setHabil(Boolean habil) {
		this.habil = habil;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
}
