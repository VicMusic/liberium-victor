package com.quinto.liberium.model;

public class MComentario {

	private String id;
	private String comentario;
	private MCausa causa = new MCausa();
	private MUsuario usuario = new MUsuario();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public MCausa getCausa() {
		return causa;
	}

	public void setCausa(MCausa causa) {
		this.causa = causa;
	}

	public MUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(MUsuario usuario) {
		this.usuario = usuario;
	}

}
