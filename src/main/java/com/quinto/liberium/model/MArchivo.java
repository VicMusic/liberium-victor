package com.quinto.liberium.model;

public class MArchivo {
	
	private String id;
	private String ruta;
	private String nombre;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Override
	public String toString() {
		return "MArchivo [id=" + id + "]";
	}
	
	public MArchivo() {
		super();
	}
	public MArchivo(String id, String ruta) {
		super();
		this.id = id;
		this.ruta = ruta;
	}
	
}
