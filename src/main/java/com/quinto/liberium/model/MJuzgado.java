package com.quinto.liberium.model;

public class MJuzgado {

	private String id;
	private String nombre;

	private String domicilio;
	private String telefono;
	
	private MInstancia mInstancia = new MInstancia();
	private MTipoCausa mTipoCausa = new MTipoCausa();
	private MCircunscripcion circunscripcion = new MCircunscripcion();
	private MPais pais = new MPais();
	private MProvincia provincia = new MProvincia();
	public MJuzgado() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public MInstancia getmInstancia() {
		return mInstancia;
	}

	public void setmInstancia(MInstancia mInstancia) {
		this.mInstancia = mInstancia;
	}

	public MCircunscripcion getCircunscripcion() {
		return circunscripcion;
	}

	public void setCircunscripcion(MCircunscripcion circunscripcion) {
		this.circunscripcion = circunscripcion;
	}

	public MTipoCausa getmTipoCausa() {
		return mTipoCausa;
	}

	public void setmTipoCausa(MTipoCausa mTipoCausa) {
		this.mTipoCausa = mTipoCausa;
	}
	
	public MPais getPais() {
		return pais;
	}
	
	public void setPais(MPais pais) {
		this.pais = pais;
	}

	public MProvincia getProvincia() {
		return provincia;
	}

	public void setProvincia(MProvincia provincia) {
		this.provincia = provincia;
	}
	


}
