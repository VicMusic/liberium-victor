package com.quinto.liberium.model;

import java.util.Date;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;

public class MDiaNoLaboral {

	private String id;
	private String titulo;
	private String tipoEvento;
	private String color;
	private TipoDiaNoLaborable tipoDiaNoLaborable;
	private Date fechaInicio;
	private Date fechaFin;
	private Pais mPais = new Pais();
	private Provincia mProvincia = new Provincia();

	public String getId() {
		return id;
	}
	
	public TipoDiaNoLaborable getTipoDiaNoLaborable() {
		return tipoDiaNoLaborable;
	}
	
	public void setTipoDiaNoLaborable(TipoDiaNoLaborable tipoDiaNoLaborable) {
		this.tipoDiaNoLaborable = tipoDiaNoLaborable;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Pais getmPais() {
		return mPais;
	}

	public void setmPais(Pais mPais) {
		this.mPais = mPais;
	}

	public Provincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(Provincia mProvincia) {
		this.mProvincia = mProvincia;
	}

	@Override
	public String toString() {
		return "MDiaNoLaboral [id=" + id + ", titulo=" + titulo + ", tipoEvento=" + tipoEvento + ", color=" + color
				+ ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", mPais=" + mPais + ", mProvincia="
				+ mProvincia + "]";
	}
	

}
