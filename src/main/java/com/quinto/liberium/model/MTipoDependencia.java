package com.quinto.liberium.model;

public class MTipoDependencia {

	private String id;
	private String nombre;
		
	public MTipoDependencia() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
