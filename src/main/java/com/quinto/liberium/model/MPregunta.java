package com.quinto.liberium.model;

import java.util.List;

import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Opcion;
import com.quinto.liberium.enumeration.TipoPregunta;

public class MPregunta {
	private String id;
	private String titulo;
	private boolean obligatoria;
	private int orden;
	private String inputRespuesta;

	private String idFormulario;

	private TipoPregunta tipo;

	private Consulta consulta;

	private String respuestas;

	private List<Opcion> opciones;

//	public List<Opcion> getOpciones() {
//		if (opciones == null && respuestas != null) {
//			opciones = new ArrayList<>();
//
//			String[] datos = respuestas.replaceAll(",  ", ",").replaceAll(" ,", ",").split(",");
//			opciones = Arrays.asList(datos);
//		}
//		return opciones;
//	}
	
	public String getInputRespuesta() {
		return inputRespuesta;
	}
	
	public void setInputRespuesta(String inputRespuesta) {
		this.inputRespuesta = inputRespuesta;
	}

	public void setOpciones(List<Opcion> opciones) {
		this.opciones = opciones;
	}

	public List<Opcion> getOpciones() {
		return opciones;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public boolean isObligatoria() {
		return obligatoria;
	}

	public void setObligatoria(boolean obligatoria) {
		this.obligatoria = obligatoria;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public String getIdFormulario() {
		return idFormulario;
	}

	public void setIdFormulario(String idFormulario) {
		this.idFormulario = idFormulario;
	}

	public TipoPregunta getTipo() {
		return tipo;
	}

	public void setTipo(TipoPregunta tipo) {
		this.tipo = tipo;
	}

	public String getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(String respuestas) {
		this.respuestas = respuestas;
	}

}
