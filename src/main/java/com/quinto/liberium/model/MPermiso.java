package com.quinto.liberium.model;

public class MPermiso {

	private String id;

	private String nombre;
	private String nombreAmigable;
	
	private Boolean administrador;

	public MPermiso() {
		super();
	}

	public MPermiso(String id, String nombre, String nombreAmigable, Boolean administrador) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.nombreAmigable = nombreAmigable;
		this.administrador = administrador;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreAmigable() {
		return nombreAmigable;
	}

	public void setNombreAmigable(String nombreAmigable) {
		this.nombreAmigable = nombreAmigable;
	}

	public Boolean getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}
}