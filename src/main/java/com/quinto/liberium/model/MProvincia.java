package com.quinto.liberium.model;

import com.quinto.liberium.entity.Seleccionable;

public class MProvincia implements Seleccionable{

	private String id;
	private String nombre;
	private MPais mPais = new MPais();

	public MProvincia() {
	}

	public MProvincia(String id, String nombre, MPais mPais) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.mPais = mPais;
	}
	
	public MProvincia(String id, String nombre, String idPais, String nombrePais) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.mPais.setId(idPais);
		this.mPais.setNombre(nombrePais);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MPais getmPais() {
		return mPais;
	}

	public void setmPais(MPais mPais) {
		this.mPais = mPais;
	}

	
}
