package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

import com.quinto.liberium.enumeration.TipoGenero;
import com.quinto.liberium.enumeration.TipoPersona;
import com.quinto.liberium.enumeration.TipoVinculo;

public class MContacto {

	private String id;

	private String telefonoFijo;
	private String telefonoCelular;
	private String dni;
	private String cuilCuit;
	private String domicilio;
	private String email;
	private String observacion;
	private String nombre;
	private String apellido;
	private String nombreAdicional;
	private String telefonoAdicional;

	private Integer alturaDomicilio;

	private TipoPersona tipoPersona;
	private TipoVinculo tipoVinculo;
	private TipoGenero tipoGenero;

	private MPais mPais = new MPais();

	private MProvincia mProvincia = new MProvincia();

	private MLocalidad mLocalidad = new MLocalidad();

	private MUsuario mUsuario = new MUsuario();
	
	private MEstudio mEstudio = new MEstudio();

	private List<MCausa> mCausa = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCuilCuit() {
		return cuilCuit;
	}

	public void setCuilCuit(String cuilCuit) {
		this.cuilCuit = cuilCuit;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombreAdicional() {
		return nombreAdicional;
	}

	public void setNombreAdicional(String nombreAdicional) {
		this.nombreAdicional = nombreAdicional;
	}

	public String getTelefonoAdicional() {
		return telefonoAdicional;
	}

	public void setTelefonoAdicional(String telefonoAdicional) {
		this.telefonoAdicional = telefonoAdicional;
	}

	public Integer getAlturaDomicilio() {
		return alturaDomicilio;
	}

	public void setAlturaDomicilio(Integer alturaDomicilio) {
		this.alturaDomicilio = alturaDomicilio;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public TipoVinculo getTipoVinculo() {
		return tipoVinculo;
	}

	public void setTipoVinculo(TipoVinculo tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}

	public TipoGenero getTipoGenero() {
		return tipoGenero;
	}

	public void setTipoGenero(TipoGenero tipoGenero) {
		this.tipoGenero = tipoGenero;
	}

	public MPais getmPais() {
		return mPais;
	}

	public void setmPais(MPais mPais) {
		this.mPais = mPais;
	}

	public MProvincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}

	public MLocalidad getmLocalidad() {
		return mLocalidad;
	}

	public void setmLocalidad(MLocalidad mLocalidad) {
		this.mLocalidad = mLocalidad;
	}

	public MUsuario getmUsuario() {
		return mUsuario;
	}

	public void setmUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}
	
	public MEstudio getmEstudio() {
		return mEstudio;
	}
	
	public void setmEstudio(MEstudio mEstudio){
		this.mEstudio = mEstudio;
	}

	public List<MCausa> getmCausa() {
		return mCausa;
	}

	public void setmCausa(List<MCausa> mCausa) {
		this.mCausa = mCausa;
	}

}