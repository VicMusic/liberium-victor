package com.quinto.liberium.model;

public class MHorario {

	private String id;
	private String dia;
	private String hora;
	private boolean solicitado;

	private MAbogado mAbogado = new MAbogado();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	public String getHora() {
		return hora;
	}
	
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	public boolean isSolicitado() {
		return solicitado;
	}
	
	public void setSolicitado(boolean solicitado) {
		this.solicitado = solicitado;
	}
	
	public MAbogado getmAbogado() {
		return mAbogado;
	}

	public void setmAbogado(MAbogado mAbogado) {
		this.mAbogado = mAbogado;
	}

}
