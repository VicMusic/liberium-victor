package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.entity.Respuesta;

public class MPregutasYRespuestas {
	
	private List<Respuesta> respuestas = new ArrayList<>();
	private List<Pregunta> preguntas = new ArrayList<>();
	
	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}
	
	public List<Pregunta> getPreguntas() {
		return preguntas;
	}
	
	public void setRespuestas(List<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}
	
	public List<Respuesta> getRespuestas() {
		return respuestas;
	}
	
}
