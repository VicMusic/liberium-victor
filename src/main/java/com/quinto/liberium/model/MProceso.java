package com.quinto.liberium.model;

public class MProceso {

	private String id;

	private String nombre;

	private MMateria mMateria = new MMateria();

	public MProceso() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MMateria getmMateria() {
		return mMateria;
	}

	public void setmMateria(MMateria mMateria) {
		this.mMateria = mMateria;
	}

}
