package com.quinto.liberium.model;

public class MContable {

	private String id;
	private Double totalIngreso;
	private Double totalEgreso;
	private Double saldoCaja;
	private Double patrimonio;
	
	private MCausa mCausa = new MCausa();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getSaldoCaja() {
		return saldoCaja;
	}

	public void setSaldoCaja(Double saldoCaja) {
		this.saldoCaja = saldoCaja;
	}

	public Double getTotalIngreso() {
		return totalIngreso;
	}

	public void setTotalIngreso(Double totalIngreso) {
		this.totalIngreso = totalIngreso;
	}

	public Double getTotalEgreso() {
		return totalEgreso;
	}

	public void setTotalEgreso(Double totalEgreso) {
		this.totalEgreso = totalEgreso;
	}

	public Double getPatrimonio() {
		return patrimonio;
	}

	public void setPatrimonio(Double patrimonio) {
		this.patrimonio = patrimonio;
	}

	public MCausa getmCausa() {
		return mCausa;
	}

	public void setmCausa(MCausa mCausa) {
		this.mCausa = mCausa;
	}
	

}
