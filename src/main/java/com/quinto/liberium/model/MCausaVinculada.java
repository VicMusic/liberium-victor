package com.quinto.liberium.model;

public class MCausaVinculada {

	private String id;

	private MCausa mCausa = new MCausa();
	
	private MCausa mVinculada = new MCausa();
	
	public MCausaVinculada() {
		super();
	}

	public MCausaVinculada(String id, String idCausa, String idCausaVinculada) {
		super();
		this.id = id;
		this.mCausa.setId(idCausa);
		this.mVinculada.setId(idCausaVinculada);
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}

	public MCausa getmCausa() {
		return mCausa;
	}

	public void setmCausa(MCausa mCausa) {
		this.mCausa = mCausa;
	}

	public MCausa getmVinculada() {
		return mVinculada;
	}

	public void setmVinculada(MCausa mVinculada) {
		this.mVinculada = mVinculada;
	}

}