package com.quinto.liberium.model;

import com.quinto.liberium.entity.Seleccionable;

public class MInstancia implements Seleccionable{

	private String id;
	private String nombre;
	
	public MInstancia() {
		super();
	}

	public MInstancia(String id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}