package com.quinto.liberium.model;

import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;

public class ChecklistModel {

	private String id;
	private String nombre;
	private TipoRolCausa tipoRolCausa;
	private TipoFuero fuero;
	private String idPais;
	private String idProvincia;
	private String idCircunscripcion;
	private String idMateria;
	private String idInstacia;
	private String idTipoCausa;
	private String idFormulario;


	public String getIdPais() {
		return idPais;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getIdCircunscripcion() {
		return idCircunscripcion;
	}

	public void setIdCircunscripcion(String idCircunscripcion) {
		this.idCircunscripcion = idCircunscripcion;
	}

	public String getIdMateria() {
		return idMateria;
	}

	public void setIdMateria(String idMateria) {
		this.idMateria = idMateria;
	}

	public String getIdInstacia() {
		return idInstacia;
	}

	public void setIdInstacia(String idInstacia) {
		this.idInstacia = idInstacia;
	}

	public String getIdTipoCausa() {
		return idTipoCausa;
	}

	public void setIdTipoCausa(String idTipoCausa) {
		this.idTipoCausa = idTipoCausa;
	}

	public String getIdFormulario() {
		return idFormulario;
	}

	public void setIdFormulario(String idFormulario) {
		this.idFormulario = idFormulario;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public TipoFuero getFuero() {
		return fuero;
	}

	public void setFuero(TipoFuero fuero) {
		this.fuero = fuero;
	}

	public TipoRolCausa getTipoRolCausa() {
		return tipoRolCausa;
	}

	public void setTipoRolCausa(TipoRolCausa tipoRolCausa) {
		this.tipoRolCausa = tipoRolCausa;
	}


}
