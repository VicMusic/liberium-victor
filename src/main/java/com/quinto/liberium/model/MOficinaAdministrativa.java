package com.quinto.liberium.model;

public class MOficinaAdministrativa {

	private String id;
	private String nombre;

	private MProvincia mProvincia = new MProvincia();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MProvincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}

}
