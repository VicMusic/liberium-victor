package com.quinto.liberium.model;

import java.util.List;

public class MRol {

    private String id;

    private String nombre;

    private List<MPermiso> permiso;

    public MRol() {
	super();
    }

    public MRol(String id, String nombre, List<MPermiso> permiso) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.permiso = permiso;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public List<MPermiso> getPermiso() {
	return permiso;
    }

    public void setPermiso(List<MPermiso> permiso) {
	this.permiso = permiso;
    }

}