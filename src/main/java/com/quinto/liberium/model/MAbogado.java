package com.quinto.liberium.model;

import java.util.ArrayList;
import java.util.List;

public class MAbogado {

	private String id;

	private String domicilio;
	private String matricula;
	private String[] pasos;

	private Integer cantSemanas;

	private Integer duracion;

	private MUsuario mUsuario = new MUsuario();

	private List<MMateria> mMateria = new ArrayList<>();

	private MProvincia mProvincia = new MProvincia();

	private MLocalidad mLocalidad = new MLocalidad();

	private Integer dia;
	
	private Integer entrada;

	private Integer salida;

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Integer getCantSemanas() {
		return cantSemanas;
	}
	
	public void setCantSemanas(Integer cantSemanas) {
		this.cantSemanas = cantSemanas;
	}
	
	public String[] getPasos() {
		return pasos;
	}
	
	public void setPasos(String[] pasos) {
		this.pasos = pasos;
	}
	
	public MLocalidad getmLocalidad() {
		return mLocalidad;
	}

	public void setmLocalidad(MLocalidad mLocalidad) {
		this.mLocalidad = mLocalidad;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MUsuario getmUsuario() {
		return mUsuario;
	}

	public void setmUsuario(MUsuario mUsuario) {
		this.mUsuario = mUsuario;
	}

	public List<MMateria> getmMateria() {
		return mMateria;
	}

	public void setmMateria(List<MMateria> mMateria) {
		this.mMateria = mMateria;
	}

	public MProvincia getmProvincia() {
		return mProvincia;
	}

	public void setmProvincia(MProvincia mProvincia) {
		this.mProvincia = mProvincia;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public Integer getEntrada() {
		return entrada;
	}

	public void setEntrada(Integer entrada) {
		this.entrada = entrada;
	}

	public Integer getSalida() {
		return salida;
	}

	public void setSalida(Integer salida) {
		this.salida = salida;
	}

	public Integer getDuracion() {
		return duracion;
	}

	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

}
