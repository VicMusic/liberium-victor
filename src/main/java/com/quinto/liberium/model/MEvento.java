package com.quinto.liberium.model;

import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.enumeration.TipoPlazo;

public class MEvento {

	private String id;
	private String titulo;
	private String tipoEvento;
	private String fechaInicio;
	private String fechaFin;
	private Integer cantDias;
	private TipoPlazo tipoPlazo;
	private TipoDiaNoLaborable tipoDia;
	private String color;

	public String getId() {
		return id;
	}

	public Integer getCantDias() {
		return cantDias;
	}
	
	public TipoDiaNoLaborable getTipoDia() {
		return tipoDia;
	}
	
	public void setTipoDia(TipoDiaNoLaborable tipoDia) {
		this.tipoDia = tipoDia;
	}

	public TipoPlazo getTipoPlazo() {
		return tipoPlazo;
	}

	public void setCantDias(Integer cantDias) {
		this.cantDias = cantDias;
	}

	public void setTipoPlazo(TipoPlazo tipoPlazo) {
		this.tipoPlazo = tipoPlazo;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "MEvento [id=" + id + ", titulo=" + titulo + ", tipoEvento=" + tipoEvento + ", fechaInicio="
				+ fechaInicio + ", fechaFin=" + fechaFin + ", cantDias=" + cantDias + ", tipoPlazo=" + tipoPlazo
				+ ", color=" + color + "]";
	}
}