package com.quinto.liberium.model;

public class MMiembroEvento {

    private String id;
    
    private MMiembro mMiembro = new MMiembro();
    private MEvento mEvento = new MEvento();
    
    private Boolean isCreador;

	public MMiembroEvento() {
		super();
	}

	public MMiembroEvento(String id, String idMiembro,  String idEvento, Boolean isCreador) {
		super();
		this.id = id;
		this.mMiembro.setId(idMiembro);
		this.mEvento.setId(idEvento);
		this.isCreador = isCreador;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MMiembro getmMiembro() {
		return mMiembro;
	}

	public void setmMiembro(MMiembro mMiembro) {
		this.mMiembro = mMiembro;
	}

	public MEvento getmEvento() {
		return mEvento;
	}

	public void setmEvento(MEvento mEvento) {
		this.mEvento = mEvento;
	}

	public Boolean getIsCreador() {
		return isCreador;
	}

	public void setIsCreador(Boolean isCreador) {
		this.isCreador = isCreador;
	}
    
    
    

}