package com.quinto.liberium.model;

public class RespuestaM {
	
	private String id;
	private String respuesta;
	private int orden;
	
	private MConsulta consulta = new MConsulta();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public MConsulta getConsulta() {
		return consulta;
	}

	public void setConsulta(MConsulta consulta) {
		this.consulta = consulta;
	}

}
