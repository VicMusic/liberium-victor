package com.quinto.liberium.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.PermisoService;
import com.quinto.liberium.service.RolService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("/administracion/miembro")
public class MiembroController extends Controlador{

    @Autowired
    private MiembroService miembroService;

    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private RolService rolService;
    
    @Autowired
	private PermisoService permisoService;
    
    @Autowired
	private EstudioService estudioService;
    
    @GetMapping("/formulario")
    public String verPermiso(ModelMap model) {
	return "formulario-permiso";
    }

    @PreAuthorize("hasAuthority('VISUALIZAR_MIEMBRO_ADMINISTRADOR')")
    @GetMapping("/listado")
	public String verMiembros(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
    	model.put("page", miembroService.buscarTodos(paginable, q));
		return "miembro";
	}
    
    @PreAuthorize("hasAuthority('VISUALIZAR_MIEMBRO')")
    @GetMapping("/estudio/listado")
	public String ver(Pageable paginable, @RequestParam(required = false) String q, ModelMap model, HttpSession session) {
    	
    	System.out.println("Listar miembros estudio...");
    	
    	List<Estudio> estudios = estudioService.buscarPorCreador(getUsuario());
    	
    	if(estudios.isEmpty() == true) {
    		System.out.println("No es creador de un estudio...");
    	}else {
    		System.out.println("Estudios cant: " + estudios.size());
    	}
    	
    	model.put("page", miembroService.buscarTodosPorEstudio(paginable, q, getEstudio(session)));
		model.put("roles", rolService.buscarTodos());
		model.put("rolesEstudio", rolService.buscarTodosPorIdCreador(getUsuario()));
		model.put("permisos", permisoService.buscarPorAdministrador(false));
		
		return "estudio-miembro";
	}
    
}