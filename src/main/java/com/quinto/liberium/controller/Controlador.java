package com.quinto.liberium.controller;

import java.util.Iterator;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.report.PDF;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.UsuarioService;

public abstract class Controlador {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private MiembroService miembroService;
	
	
	public Usuario getUsuario(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return usuarioService.buscarUsuarioPorEmail(auth.getName());
	}
	
	public Estudio getEstudio(HttpSession session){
		return (Estudio) session.getAttribute("estudioSession");
	}
	
	public Miembro getMiembro(HttpSession session){
		return miembroService.buscarPorIdEstudioYIdUsuario(getEstudio(session).getId(), getUsuario().getId());
	}
	
	@SuppressWarnings("rawtypes")
	public ResponseEntity<byte[]> exportar(PDF pdf) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);
		headers.setContentDispositionFormData("reporte.pdf", "reporte.pdf");
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		pdf.exportar(out);

		return new ResponseEntity<>(out.toByteArray(), headers, HttpStatus.OK);
	}
	
	public void ordenar(Pageable paginable, ModelAndView mav) {
		
		if(!paginable.getSort().toString().equals("UNSORTED")) {
			
			String[] sort = paginable.getSort().toString().replaceAll(": ", ",").split(",");
			StringBuilder campos = new StringBuilder();
			for(int n = 0; n < sort.length; n++) {
				if(n % 2 == 0) {
					campos.append(sort[n]).append(" ");
				}
			}
			
			mav.addObject("sort_field", campos.toString().trim().replace(" ", ","));
			mav.addObject("sort_dir", sort[1]);
		}
	}
	
	@SuppressWarnings ("rawtypes")
	public String getRol() {
		Authentication principal = SecurityContextHolder.getContext().getAuthentication();
		Iterator it = principal.getAuthorities().iterator();
		while (it.hasNext()) {
			return it.next().toString();
		}
		return "";
	}
	
	public boolean isAbogado() {
		return getRol().equals("ROLE_ABOGADO");
	}
	
	
	public boolean isStringNull(String string) {
		return string == null || string.isEmpty() || string.equals("");
	}
	
}
