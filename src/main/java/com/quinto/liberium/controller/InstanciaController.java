package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.service.InstanciaService;

@Controller
@RequestMapping("/administracion/instancia")
public class InstanciaController {

	@Autowired
	private InstanciaService instanciaService;

	@PreAuthorize("hasAuthority('VISUALIZAR_INSTANCIAS')")
	@GetMapping("/listado")
	public String listado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Instancia> instancias = instanciaService.buscarTodos(paginable, q);

		if(instancias.getTotalPages() < 1 ) {
			instancias = instanciaService.buscarTodos(paginable, null);
			if(instancias.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("modulo", "instancias");
		model.put("page", instancias);
		model.put("formulario", new Instancia());

		// FIXME
//		ordenar(paginable, model);

		return "instancia";
	}
	

}