package com.quinto.liberium.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.quinto.liberium.model.MEscrito;
import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.FileService;
import com.quinto.liberium.util.Configuracion;

@Controller
@RequestMapping("/download")
public class FileDownloadController {
	
	@Autowired
	private Configuracion configuracion;
	
	@Autowired
	private EscritoService escritoService;
	
	@Autowired
	private FileService fileService;
	
	@RequestMapping("/pdf/{idEscrito}")
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
    public void downloadPDFResource( 
    		HttpServletRequest request, 
    		HttpServletResponse response, 
    		@PathVariable("idEscrito") String idEscrito) {
         
    	MEscrito modelo = escritoService.buscarEscritoModelPorId(idEscrito);
    	if(modelo != null) {
        	String ruta = configuracion.getPathEscritos() + modelo.getId() + ".pdf";
    		fileService.descargar( ruta, response,null,null);
    	}
        
    }

    @RequestMapping("/docx/{idEscrito}")
    @PreAuthorize("hasAuthority('EDITAR_CAUSA')")
    public void downloadDOCXResource( 
    		HttpServletRequest request, 
            HttpServletResponse response, 
            @PathVariable("idEscrito") String idEscrito) {
    	MEscrito modelo = escritoService.buscarEscritoModelPorId(idEscrito);
    	if(modelo != null) {
        	String ruta = configuracion.getPathEscritos() + modelo.getId() + ".docx";
    		fileService.descargar( ruta, response,null,null);
    	}
    }

}
