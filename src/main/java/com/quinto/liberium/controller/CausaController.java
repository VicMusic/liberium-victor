package com.quinto.liberium.controller;

import static com.quinto.liberium.util.Constantes.VISTA_ARCHIVADAS;
import static com.quinto.liberium.util.Constantes.VISTA_CAUSA;
import static com.quinto.liberium.util.Constantes.VISTA_CAUSA_EDITAR;
import static com.quinto.liberium.util.Constantes.VISTA_CAUSA_VER;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.converter.CausaConverter;
import com.quinto.liberium.converter.MiembroConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Modificacion;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MComentario;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MContacto;
import com.quinto.liberium.model.MEscrito;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.ComentarioService;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.ContactoService;
import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.JuzgadoService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.ModificacionService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.RolService;
import com.quinto.liberium.service.TipoCausaService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("/administracion/causa")
public class CausaController extends Controlador {

	@Autowired
	private CausaService causaService;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private ComentarioService comentarioService;

	@Autowired
	private ModificacionService modificacionService;

	@Autowired
	private ContactoService contactoService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private CausaConverter causaConverter;

	@Autowired
	private EscritoService escritoService;

	@Autowired
	private MiembroConverter miembroConverter;

	@Autowired
	private RolService rolService;

	@Autowired
	private MateriaService materiaService;

	@Autowired
	private FormularioService formularioService;

	@Autowired
	private JuzgadoService juzgadoService;

	@GetMapping("/formulario")
	public String verCausa(ModelMap model) {
		return "formulario-causa";
	}

	@GetMapping("/listado")
	@PreAuthorize("hasAuthority('VISUALIZAR_CAUSA')")
	public String causaListado(Pageable paginable, @RequestParam(required = false) String q,
			@RequestParam(required = false) String filtro, @RequestParam(required = false) String clave,
			Authentication usuario, ModelMap model, HttpSession session) {

		try {
			Estudio estudio = getEstudio(session);
			Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
			Page<Causa> listado;
			List<MCausa> causas = new ArrayList<>();

			if (usuarioLog.isAbogado()) {
				listado = causaService.listarCausa(estudio.getId(), filtro, clave, paginable, q);
			} else {
				listado = causaService.listarCausa(estudio.getId(), filtro, clave, paginable, q);
				// listado = new
				// PageImpl<Causa>(causaService.buscarPorMiembro(getMiembro(session).getId()));
			}
			for (MCausa causa : causaConverter.entidadesModelos(listado.getContent())) {
				if (causa.getCreador().getId().equals(usuarioLog.getId())) {
					causas.add(causa);
					continue;
				}
				causas.add(causa);
			}
			listado = causaService.colorBarra(causas, usuarioLog, estudio, true);
			causaService.promedioAvance(usuarioLog.getId(), estudio);

			Page<Pais> pais = paisService.buscarTodos(paginable, null);
			List<MProvincia> provincias = provinciaService.buscarProvinciaModelTodos();
			List<Instancia> instancias = instanciaService.buscarInstanciaTodos();
			Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);
			// List<MEscrito> escritos = escritoService.buscarOriginalesList();

			Page<Consulta> consultas = consultaService.buscarConsutasPorAbogado(getUsuario(), null,null);
			model.put("consultas", consultas);

			model.put("modulo", "causas");
			model.put("page", listado);
			model.put("pais", pais);
			model.put("provincias", provincias);
			model.put("formulario", new Causa());
			model.put("instancias", instancias);
			model.put("tipoCausas", tipoCausas);

			return VISTA_CAUSA;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new PeticionIncorrectaException(
					"ERROR - Ocurrió un error inesperado mientras se cargaban las Causas.");
		}
	}

	@GetMapping("/archivadas")
	@PreAuthorize("hasAuthority('VISUALIZAR_CAUSA')")
	public String causaArchivadas(Pageable paginable, @RequestParam(required = false) String q, Authentication usuario,
			ModelMap model, HttpSession session) {
		try {

			Estudio estudio = getEstudio(session);
			List<Causa> listado = causaService.listarArchivadas(estudio.getId());
			Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
			List<MCausa> causas = new ArrayList<>();

			for (MCausa causa : causaConverter.entidadesModelos(listado)) {
				if (causa.getCreador().getId().equals(usuarioLog.getId())) {
					causas.add(causa);
					continue;
				}
			}
			// causas = causaService.colorBarra(causas, usuarioLog, estudio, true);
			causaService.promedioAvance(usuarioLog.getId(), estudio);

			List<MProvincia> provincias = provinciaService.buscarProvinciaModelTodos();
			List<Instancia> instancias = instanciaService.buscarInstanciaTodos();
			Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);

			Page<Consulta> consultas = consultaService.buscarConsutasPorAbogado(getUsuario(), null,null);
			model.put("consultas", consultas);

			model.put("modulo", "causas");
			model.put("page", causas);
			model.put("provincias", provincias);
			model.put("formulario", new Causa());
			model.put("instancias", instancias);
			model.put("tipoCausas", tipoCausas);

			return VISTA_ARCHIVADAS;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new PeticionIncorrectaException(
					"ERROR - Ocurrió un error inesperado mientras se cargaban las Causas.");
		}
	}

	@GetMapping("/editar/{id}")
	@PreAuthorize("hasAuthority('VISUALIZAR_CAUSA')")
	public String causaCrear(Pageable paginable, @PathVariable String id, @RequestParam(required = false) String q,
			Authentication usuario, ModelMap model, HttpSession session) {

		Causa causa = causaService.buscarPorId(id);
		// Estudio estudio = getEstudio(session);
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		List<Instancia> instancias = instanciaService.buscarInstanciaTodos();
		Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);
		Page<Pais> paises = paisService.buscarTodos(null, null);
		List<Contacto> contactos = new ArrayList<Contacto>();
		List<MContacto> contactosDelUsuario = contactoService.buscarContactosPorUsuario(usuarioLog);

		if (causa.getConsulta() != null) {
			MConsulta consulta = consultaService.buscarModelPorId(causa.getConsulta().getId());
			model.put("consulta", consulta);
		} else {
			Page<Consulta> consultas = consultaService.buscarConsutasPorAbogado(getUsuario(), null,null);
			model.put("consultas", consultas);
		}

		for (Contacto contacto : causa.getContacto()) {
			if (contacto.getEliminado() == null) {
				contactos.add(contacto);
			}
		}

		Set<Miembro> miembros = causa.getMiembrosCausa();

		for (Miembro miembroCausa : causa.getMiembrosCausa()) {
			if (miembroCausa.getEliminado() == null) {
				miembros.add(miembroCausa);
			}
		}

		Checklist checklist = new Checklist();
		if (causa.getChecklist() != null) {
			checklist = causa.getChecklist();
		}

		List<MProvincia> provincias = null;

		if (causa.getPais() != null) {
			provincias = provinciaService.buscarProvinciaModelPorIdPais(causa.getPais().getId());
		}
		List<Modificacion> modificaciones = modificacionService.buscarPorCausa(id);
		List<MEscrito> escritos = escritoService.buscarOriginalesList();
		List<MEscrito> escritosUsuario = escritoService.buscarTodosUsuarioIdM(usuarioLog.getId());
		for (int i = 0; i < escritosUsuario.size(); i++) {
			escritos.add(escritosUsuario.get(i));
		}

		boolean bandera = false;

		String nombreActor = "";
		String apellidoActor = "";
		for (int i = 0; i < causa.getActor().length(); i++) {
			if (causa.getActor().codePointAt(i) == 44) {
				bandera = true;
			}
			if (bandera == false) {
				apellidoActor += String.valueOf(causa.getActor().charAt(i));
			} else {
				nombreActor += String.valueOf(causa.getActor().charAt(i));
			}
		}
		if (!nombreActor.isEmpty()) {
			nombreActor = nombreActor.substring(2, nombreActor.length());
		}

		bandera = false;
		String nombreDemandado = "";
		String apellidoDemandado = "";
		if (causa.getDemandado() != null) {
			for (int i = 0; i < causa.getDemandado().length(); i++) {
				if (causa.getDemandado().codePointAt(i) == 44) {
					bandera = true;
				}
				if (bandera == false) {
					apellidoDemandado += String.valueOf(causa.getDemandado().charAt(i));
				} else {
					nombreDemandado += String.valueOf(causa.getDemandado().charAt(i));
				}
			}
		}

		if (!nombreDemandado.isEmpty()) {
			nombreDemandado = nombreDemandado.substring(2, nombreDemandado.length());
		}

		List<MJuzgado> juzgadosM = juzgadoService.buscarJuzgadoModelTodos();

		model.put("checklist", checklist);
		model.put("modificaciones", modificaciones);
		model.put("provincias", provincias);
		model.put("contactos", contactos);
		model.put("contactosDelUsuario", contactosDelUsuario);
		model.put("miembros", miembros);
		model.put("paises", paises);
		model.put("causa", causa);
		model.put("instancias", instancias);
		model.put("tipoCausas", tipoCausas);
		model.put("escritos", escritos);
		model.put("roles", rolService.buscarTodos());
		model.put("actorNombre", nombreActor);
		model.put("actorApellido", apellidoActor);
		model.put("demandadoNombre", nombreDemandado);
		model.put("demandadoApellido", apellidoDemandado);
		model.put("juzgadosOficinas", juzgadosM);

		// Checklist
		model.put("paisesChecklist", paises);
		model.put("instanciasChecklist", instanciaService.buscarTodos(null, null));
		model.put("tipoCausasChecklist", tipoCausas);
		model.put("materiasChecklist", materiaService.buscarTodos(null, null));
		model.put("tipoRolCausasChecklist", TipoRolCausa.values());
		model.put("tiposFueroChecklist", TipoFuero.values());
		model.put("formulariosChecklist", formularioService.buscarTodos(null, null));

		return VISTA_CAUSA_EDITAR;
	}

	@GetMapping("/ver/{id}")
	@PreAuthorize("hasAuthority('VISUALIZAR_CAUSA')")
	public String causaVer(Pageable paginable, @PathVariable String id, @RequestParam(required = false) String q,
			Authentication usuario, ModelMap model) {
		Causa causa = causaService.buscarPorId(id);
		List<Instancia> instancias = instanciaService.buscarInstanciaTodos();
		Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);
		Page<Pais> paises = paisService.buscarTodos(null, null);
		List<Contacto> contactos = new ArrayList<Contacto>();

		if (causa.getConsulta() != null) {
			MConsulta consulta = consultaService.buscarModelPorId(causa.getConsulta().getId());
			model.put("consulta", consulta);
		}

		for (Contacto contacto : causa.getContacto()) {
			if (contacto.getEliminado() == null) {
				contactos.add(contacto);
			}
		}

		Set<MMiembro> miembros = new HashSet<>();

		for (Miembro miembroCausa : causa.getMiembrosCausa()) {
			if (miembroCausa.getEliminado() == null) {
				miembros.add(miembroConverter.entidadModelo(miembroCausa));
			}
		}

		Checklist checklist = new Checklist();
		for (Item item : causa.getItems()) {
			checklist = item.getCheckList();
			break;
		}
		List<MComentario> comentarios = comentarioService.buscarComentarioModelPorCausa(id);
		List<Modificacion> modificaciones = modificacionService.buscarPorCausa(id);
		model.put("modificaciones", modificaciones);
		model.put("comentarios", comentarios);
		model.put("contactos", contactos);
		model.put("miembros", miembros);
		model.put("paises", paises);
		model.put("checklist", checklist);
		model.put("causa", causa);
		model.put("instancias", instancias);
		model.put("tipoCausas", tipoCausas);

		return VISTA_CAUSA_VER;
	}

	@GetMapping("/cliente/{id}")
	public String verCausaCliente(@PathVariable String id, ModelMap model) {
		Causa causa = causaService.buscarPorId(id);
		List<Instancia> instancias = instanciaService.buscarInstanciaTodos();
		Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);
		Page<Pais> paises = paisService.buscarTodos(null, null);
		List<Contacto> contactos = new ArrayList<Contacto>();

		if (causa.getConsulta() != null) {
			MConsulta consulta = consultaService.buscarModelPorId(causa.getConsulta().getId());
			model.put("consulta", consulta);
		}

		for (Contacto contacto : causa.getContacto()) {
			if (contacto.getEliminado() == null) {
				contactos.add(contacto);
			}
		}

		Set<MMiembro> miembros = new HashSet<>();

		for (Miembro miembroCausa : causa.getMiembrosCausa()) {
			if (miembroCausa.getEliminado() == null) {
				miembros.add(miembroConverter.entidadModelo(miembroCausa));
			}
		}

		Checklist checklist = new Checklist();
		for (Item item : causa.getItems()) {
			checklist = item.getCheckList();
			break;
		}
		List<MComentario> comentarios = comentarioService.buscarComentarioModelPorCausa(id);
		List<Modificacion> modificaciones = modificacionService.buscarPorCausa(id);
		model.put("modificaciones", modificaciones);
		model.put("comentarios", comentarios);
		model.put("contactos", contactos);
		model.put("miembros", miembros);
		model.put("paises", paises);
		model.put("checklist", checklist);
		model.put("causa", causa);
		model.put("instancias", instancias);
		model.put("tipoCausas", tipoCausas);
		return "causacliente";
	}

	@PostMapping("/barraCausa/{idCausa}")
	public ResponseEntity<MCausa> barraCausa(@PathVariable String idCausa, Authentication usuario)
			throws ParseException {
		return new ResponseEntity<MCausa>(causaService.buscarCausaModelPorId(idCausa), HttpStatus.OK);
	}
}