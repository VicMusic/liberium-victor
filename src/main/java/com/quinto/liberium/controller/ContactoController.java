package com.quinto.liberium.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.service.ContactoService;
import com.quinto.liberium.service.PaisService;

@Controller
@RequestMapping("/contactos")
public class ContactoController extends Controlador{

    @Autowired
    private ContactoService contactoService;
    
    @Autowired
    private PaisService paisService;

    @GetMapping("/ver/{tipo}/{idSelectEstudio}")
    @PreAuthorize("hasAuthority('VISUALIZAR_CONTACTOS')")
    public String verContacto( 
    		@PathVariable(required = false) String tipo,
    		@RequestParam(required = false) String q,
    		ModelMap model, Authentication session, HttpSession httpSession, Pageable paginable) {
    	
    	Page<Contacto> contactos = contactoService.buscarTodos(paginable, getUsuario().getId(), getEstudio(httpSession).getId(), q, tipo);
    	
    	model.put("page", contactos);
    	model.put("resultado", true);
    	model.put("paises", paisService.buscarTodos(null, null));
    	    	
    	return "contactos-usuario";
    }
    
    
    
}