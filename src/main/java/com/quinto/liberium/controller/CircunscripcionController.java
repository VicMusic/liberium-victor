package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.ProvinciaService;

@Controller
@RequestMapping("/administracion/circunscripcion")
public class CircunscripcionController extends Controlador {

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private ProvinciaService provinciaService;

	@PreAuthorize("hasAuthority('VISUALIZAR_CIRCUNSCRIPCIONES')")
	@GetMapping("/listado")
	public String dependenciaListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Circunscripcion> page = circunscripcionService.buscarTodas(paginable, q);
		Page<Provincia> provincias = provinciaService.buscarTodos(null, null);

		if (page == null || page.isEmpty()) {
			model.put("resultado", true);
		}
		
		if(page.getTotalPages() < 1 ) {
			page = circunscripcionService.buscarTodas(paginable, null);
			if(page.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("provincias", provincias);
		model.put("modulo", "circunscripcion");
		model.put("page", page);
		model.put("formulario", new Circunscripcion());

		// FIXME
//		ordenar(paginable, model);

		return "circunscripcion";
	}

}
