package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping("/administracion/formulario")
public class FormularioController extends Controlador {

	@Autowired
	private FormularioService formularioService;

	@Autowired
	private MateriaService materiaService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@Autowired
	private PaisService paisService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_FORMULARIOS')")
	@GetMapping("/listado")
	public String dependenciaListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Formulario> formularios = formularioService.buscarTodos(paginable, q);
		
		if(formularios.getTotalPages() < 1 ) {
			model.put("resultado", true);
			formularios = formularioService.buscarTodos(paginable, null);
		}
		
		model.put("page", formularios);
		model.put("materias",  materiaService.buscarTodos(null, null));
		model.put("paises",  paisService.buscarTodos(null, null));
		model.put("tCausas", tipoCausaService.buscarTodas(null, null));
		model.put("formulario", new Formulario());

		// FIXME
//		ordenar(paginable, model);

		return "formulario";
	}

}
