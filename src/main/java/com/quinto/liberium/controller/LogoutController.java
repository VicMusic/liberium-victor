package com.quinto.liberium.controller;
/**
import static com.quinto.liberium.util.Constantes.MENSAJE_EXITO;
import static com.quinto.liberium.util.Constantes.VISTA_LOGIN;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.TurnoService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping()
	public class LogoutController {

	private Log log = LogFactory.getLog(LogoutController.class);

	@GetMapping("/logout")
	public String logout(Model model, @RequestParam(required = false) String error,
			@RequestParam(required = false) String info, @RequestParam(required = false) String logout) {

		log.info("METODO: login -- PARAMETROS: error= " + error + " logout=  " + logout);
		log.info("METODO: login -- PARAMETROS: info= " + info + " logout= " + logout);

		if (logout != null) {
			model.addAttribute(MENSAJE_EXITO, "Se cerro la sesión correctamente");
		}

		model.addAttribute(MENSAJE_EXITO, info);

		return VISTA_LOGIN;
	}

} 
	
	@GetMapping(value = "/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/logout";
	}
	
	@GetMapping("/logout")
	public String logout(ModelMap modelo, @RequestParam(required = false) String error) {

		if (error != null && !error.isEmpty()) {
			modelo.addAttribute("error", "Se cerró la sesión correctamente");
		}
		return "logout"; 

	}	
	public class LogoutController extends HttpServlet {
	   
	    protected void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        HttpSession session = request.getSession();
	        if(session.getAttribute("userName") != null){
	            session.removeAttribute("userName");
	            response.sendRedirect("logout");
	        }
	    }
}**/
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@WebServlet("/login?logout")
public class LogoutController extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public LogoutController() {
        super();
    }
    @GetMapping("/login?logout")
    protected void logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute("userName");
             
            RequestDispatcher dispatcher = request.getRequestDispatcher("login");
            dispatcher.forward(request, response);
        }
    }
}