package com.quinto.liberium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.EstudioService;

@Controller
@RequestMapping("/contabilidad")
public class ContabilidadEstudioController extends Controlador{

	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DATOS_CONTABLES')")
	@GetMapping("/ver")
	public String nuevo(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {

		Abogado abogado = abogadoService.buscarAbogadoPorMailUsuario(getUsuario().getEmail());
		
		List<Estudio> estudio= estudioService.buscarPorPertenencia(abogado.getId());
		
		model.put("estudio", estudio);
		model.put("modulo", "contables");
		
		return "contabilidadEstudio";
	}


}
