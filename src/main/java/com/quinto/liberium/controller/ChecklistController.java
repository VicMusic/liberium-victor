package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping("/administracion/checklist")
public class ChecklistController {
	
	@Autowired
	private ChecklistService checklistService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private EscritoService escritoService;

	@Autowired
	private FormularioService formularioService;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private MateriaService materiaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_CHECKLIST')")
	@GetMapping("/listado")
	public String verListadoCheckList(Pageable paginable, @RequestParam(required = false) String q, @RequestParam(required = false) String id1, ModelMap model) {

		Page<Checklist>checkLists;

		if(q != null && !q.equals("")){
			checkLists = checklistService.buscarTodos(paginable, q);
		}else{
			checkLists = checklistService.buscarTodosPlantilla(paginable);
		}

		if(checkLists.getTotalPages() < 1 ) {
			checkLists = checklistService.buscarTodos(paginable, null);
			if(checkLists.getTotalPages() > 0)
				model.put("resultado", true);
		}
		model.put("id1", id1);
		model.put("modulo", "checklists");
		model.put("page", checkLists);
		model.put("paises", paisService.buscarTodos(null, null));
		model.put("provincias", provinciaService.buscarTodos(null, null));
		model.put("instancias",  instanciaService.buscarTodos(null, null));
		model.put("tipoCausas",  tipoCausaService.buscarTodas(null, null));
		model.put("materias", materiaService.buscarTodos(null, null));
		model.put("tipoRolCausas", TipoRolCausa.values());
		model.put("tiposFuero", TipoFuero.values());
		model.put("items", itemService.buscarTodos(null));
		model.put("escritos", escritoService.buscarOriginalesList());
		model.put("formularios", formularioService.buscarTodos(null, null));

		// FIXME
//		ordenar(paginable, model);

		return "checklist";
	}

}
