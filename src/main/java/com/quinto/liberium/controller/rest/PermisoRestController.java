package com.quinto.liberium.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MPermiso;
import com.quinto.liberium.service.PermisoService;

@RestController
@RequestMapping("/api/permiso")
public class PermisoRestController extends Controlador{
	
	@Autowired
	private PermisoService permisoService;
	
	
	@GetMapping("/buscar")
	public ResponseEntity<List<MPermiso>> buscarPermisos(@RequestParam(required=true) Boolean administrador) {
		List<MPermiso> mPermisos = permisoService.buscarMPermisoPorAdministrador(administrador);
		return new ResponseEntity<List<MPermiso>>(mPermisos, HttpStatus.OK);
 			
	}
	
}
