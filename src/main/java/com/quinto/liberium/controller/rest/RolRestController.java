package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.service.RolService;

@RestController
@RequestMapping("/api/roles")
public class RolRestController extends Controlador {
	
	@Autowired
	private RolService rolService;
	
	@PreAuthorize("hasAuthority('EDITAR_ROL')")
	@PostMapping("/guardar")
	public ResponseEntity<Rol> crearUsuario(
			@RequestParam(required = false) String id,
			@RequestParam(required = false) String nombre, 
			@RequestParam(required = false) String permisosChecked, 
			@RequestParam(required = false) String isAbogado,
			HttpSession session) {

		Rol rol = rolService.guardar(id, nombre, permisosChecked.split(";"), getUsuario(), isAbogado);
		return new ResponseEntity<>(rol, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAuthority('EDITAR_ROL')")
	@PostMapping("/guardarRolEstudio")
	public ResponseEntity<Rol> crearRolEstudio(
			@RequestParam(required = false) String id,
			@RequestParam(required = false) String nombre, 
			@RequestParam(required = false) String permisosChecked, 
			@RequestParam(required = false) String isAbogado,
			HttpSession session) {
		
		System.out.println("Id Rol: " + id);
		Rol rol = rolService.guardar(id, nombre, permisosChecked.split(";"), getUsuario(), isAbogado);
		return new ResponseEntity<>(rol, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_ROL')")
	@PostMapping("/eliminar")
	public ResponseEntity<Object> eliminarUsuario(@RequestParam(required = false) String id) {
		Rol rol = rolService.eliminar(id, getUsuario());
		return new ResponseEntity<>(rol, HttpStatus.OK);
	}
	
	@PostMapping("/eliminarRolEstudio")
	public ResponseEntity<Object> eliminarRolEstudio(@RequestParam(required = false) String id) {
		Rol rol = rolService.eliminar(id, getUsuario());
		return new ResponseEntity<>(rol, HttpStatus.OK);
	}
}
