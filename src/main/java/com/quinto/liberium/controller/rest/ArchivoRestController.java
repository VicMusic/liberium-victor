package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.MArchivo;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.service.ArchivoService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.SubItemService;

@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR', 'ROLE_ABOGADO')")
@RequestMapping("/api/archivo")
public class ArchivoRestController extends Controlador {
	private final Log LOG = LogFactory.getLog(ArchivoRestController.class);

	@Autowired
	private ArchivoService archivoService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private SubItemService subItemService;

	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/eliminar/{idArchivo}/{idItem}/{tipo}")
	public ResponseEntity<MArchivo> eliminarArchivo(@PathVariable("idItem") String idItem,
			@PathVariable("idArchivo") String idArchivo, @PathVariable("tipo") String tipo) {
		LOG.info("METHOD: eliminarArchivo() --- PARAMS:, idItem= " + idItem + ", tipo= " + tipo + ", idArchivo= "
				+ idArchivo);

		MArchivo modelo = archivoService.eliminarArchivo(idArchivo, idItem, tipo, getUsuario());

		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/guardarArchivo")
	public ResponseEntity<Object> guardarArchivo(HttpServletRequest request, HttpSession session) throws WebException {
		
		String id = request.getParameter("idItem");
		String tipo = request.getParameter("tipo");
		String orden = request.getParameter("orden");
		String idEscrito = request.getParameter("escritos");
		String limite = request.getParameter("limite");
		String habil = request.getParameter("habil");
		
		List<Archivo> archivos = new ArrayList<>();
		archivos = archivoService.obtenerArchivosChecklist(request, orden, archivos, getUsuario());
		
		if (tipo.equals("ITEM")) {
			Item item = itemService.buscarItemPorId(id);
			if (item.getArchivos() != null) {
				for (Archivo a : item.getArchivos()) {
					archivos.add(a);
				}
			}
			if (limite != null && !limite.isEmpty()) {
				item.setLimite(new Integer(limite));
				item.setHabil(habil != null && !habil.isEmpty() && habil.equals("true") ? true : false);
			}
			itemService.guardarArchivos(item, archivos);
			if (idEscrito != null && !idEscrito.isEmpty()) {
				itemService.guardarEscrito(item, idEscrito);
			}
		} else {
			SubItem subItem = subItemService.buscarSubItemPorId(id);
			if (subItem.getArchivos() != null) {
				for (Archivo a : subItem.getArchivos()) {
					archivos.add(a);
				}
			}
			subItemService.guardarArchivos(subItem, archivos);
			if (idEscrito != null && !idEscrito.isEmpty()) {
				subItemService.guardarEscrito(subItem, idEscrito);
			}
		}

		return new ResponseEntity<>(new Response("El arrchivo fue creado con éxito", "", HttpStatus.OK), HttpStatus.OK);
	}

}
