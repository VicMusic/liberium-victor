package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MEstudio;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/estudio")
public class EstudioRestController extends Controlador {

	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	@Autowired
	private MiembroService miembroService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/guardar")
	public ResponseEntity<MEstudio> guardar(@RequestParam(required = false) String idEstudio,
			@RequestParam (name = "nombreEstudio") String nombreEstudio, HttpSession session){
		MEstudio mEstudio = estudioService.guardar(idEstudio, nombreEstudio, getUsuario());
		Miembro miembro = miembroService.generarMiembroAbogado(mEstudio.getId(), getUsuario());
		usuarioService.updatePermisos(miembro, session);
		
		return new ResponseEntity<>(mEstudio, HttpStatus.OK);
	}
	
	@PostMapping("/cambiar")
	public ResponseEntity<Object> cambiarEstudio(@RequestParam(required = false) String idEstudio,
			HttpSession session){
		Estudio estudio = estudioService.buscarPorId(idEstudio);
		if(estudio != null) {
			Miembro miembro = miembroService.buscarPorIdEstudioYIdUsuario(idEstudio, getUsuario().getId());
			usuarioService.updatePermisos(miembro, session);
		}else {
			List<Estudio> listEstudios = miembroService.buscarEstudioPorIdUsuario(getUsuario().getId());
			if(!listEstudios.isEmpty()) {
				Miembro miembro = miembroService.buscarPorIdEstudioYIdUsuario(listEstudios.get(0).getId(), getUsuario().getId());
				usuarioService.updatePermisos(miembro, session);
			}
		}
		return new ResponseEntity<>(new Response("éxito", "", HttpStatus.OK), HttpStatus.OK);
	}
	
	@PostMapping("/eliminar")
	public ResponseEntity<MEstudio> eliminar(@RequestParam(required=false) String id){
		MEstudio mEstudio = estudioService.eliminar(id, getUsuario());
		return new ResponseEntity<>(mEstudio, HttpStatus.OK);
	}
	
	@GetMapping("/abogados{query}")
	public ResponseEntity<MAbogado> buscarAbogados(@PathVariable(required = false) String query, HttpSession session){
		System.out.println("En Estudio Rest Controller");
		MAbogado abogados = abogadoService.buscarAbogadoPorDNI(query);
		if(abogados == null) {
			System.out.println("Respuesta Null");
		}
		return new ResponseEntity<>(abogados, HttpStatus.OK);
	}
	
	@GetMapping("/miembros{idEstudio}")
	public ResponseEntity<List<MAbogado>> buscarMiembros(@PathVariable(required = false) String idEstudio, HttpSession session){
				
		List<MAbogado> abogados = abogadoService.buscarAbogadoEstudio(idEstudio);
		
		System.out.println(abogados);
		
		return new ResponseEntity<>(abogados, HttpStatus.OK);
		
	}
	
}
