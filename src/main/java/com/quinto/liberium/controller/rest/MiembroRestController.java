package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.converter.MiembroConverter;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.RolService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/miembro")
public class MiembroRestController extends Controlador {
	
	private static final Logger log = LoggerFactory.getLogger(MiembroRestController.class);

	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private MiembroService miembroService;
	
	@Autowired
	private RolService rolService;
	
	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private MiembroConverter miembroConverter;
	
	@PostMapping("/buscar/email")
	public ResponseEntity<Usuario> crearUsuario(
			@RequestParam(required = false) String email,
			HttpSession session) {
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(email);
		
		if(usuario == null) {
			usuario = new Usuario();
		}
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_MIEMBRO')")
	@PostMapping("/guardar")
	public ResponseEntity<Object> guardarMiembro(@RequestParam(required = false) String id,
			@RequestParam(required = false) String idEmailBuscar,
			@RequestParam(required = false) String idRolSelect,
			HttpSession session) {
		
		Miembro miembro = new Miembro();
		boolean ok = true;
		String mensaje = "El miembro ha sido creado con éxito";
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(idEmailBuscar);
		Rol rol = rolService.buscarPorId(idRolSelect);
		if(usuario == null) {
			StringBuilder sb = new StringBuilder(idEmailBuscar);
			String nombreUsuario = "";
			for(int i =0; i < idEmailBuscar.length(); i++) {
				if(idEmailBuscar.charAt(i) == '@') {
					nombreUsuario = sb.substring(0, i);
				}
			}
			if(rol != null) {
				usuario = usuarioService.crear(null, nombreUsuario, idEmailBuscar, "Liberium123", null, null, TipoUsuario.USUARIO, rol.isAbogado(), null, getUsuario(), true);
			}else {
				usuario = usuarioService.crear(null, nombreUsuario, idEmailBuscar, "Liberium123", null, null, TipoUsuario.USUARIO, false, null, getUsuario(), true);
			}
		}
		Miembro miembroValidar = miembroService.buscarPorIdEstudioYIdUsuario(getEstudio(session).getId(), usuario.getId());
		if (miembroValidar != null) {
			mensaje = "El miembro ya esta en el estudio que se lo quiere agregar";
			ok = false;
		}
		
		List<Estudio> estudio = estudioService.buscarPorPertenencia(getUsuario().getId());
		if (estudio != null) {
			mensaje = "El miembro pertenece a otro estudio. El miembro ha sido creado con éxito";
		}

		log.info(getEstudio(session).getId() + " " + getEstudio(session).getNombre());
		
		if(ok == true) {
			miembro = miembroService.crearMiembro(id, usuario, rol, getEstudio(session), getUsuario());
		}

		return new ResponseEntity<>(new Response(mensaje, miembro.getId(), HttpStatus.OK), HttpStatus.OK);
	}

	/*
>>>>>>> origin/395-usuario-miembro-estudio
	@PreAuthorize("hasAuthority('EDITAR_MIEMBRO')")
	@GetMapping("/invitar/{idAbogadoInvitar}/{mailAbogadoInvitar}")
	public ResponseEntity<Object> invitarMiembro(
			@PathVariable(required = false) String idAbogadoInvitar,
			@PathVariable(required = false) String mailAbogadoInvitar,
			HttpSession session) {
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(mailAbogadoInvitar);
		Rol rol = rolService.buscarPorId("0");
		if(usuario == null) {
			usuario = usuarioService.crear(null, "", mailAbogadoInvitar, "liberium123", null, null, TipoUsuario.USUARIO, rol.isAbogado(), null, getUsuario(), true);
		}
		Miembro miembroValidar = miembroService.buscarPorIdEstudioYIdUsuario(getEstudio(session).getId(), usuario.getId());
		if (miembroValidar != null) {
			throw new PeticionIncorrectaException("El miembro ya esta en el estudio que se lo quiere agregar");
		}
		log.info(getEstudio(session).getId() + " " + getEstudio(session).getNombre());
		Miembro miembro = miembroService.crearMiembroInvitar(usuario, rol, getEstudio(session), getUsuario());
		
		return new ResponseEntity<>(new Response("El miembro ha sido creado con éxito", miembro.getId(), HttpStatus.OK), HttpStatus.OK);
	}
<<<<<<< HEAD
	
=======
*/
	
	@PreAuthorize("hasAuthority('EDITAR_MIEMBRO')")
	@PostMapping("/editar")
	public ResponseEntity<Object> editarrMiembro(
			@RequestParam(required = false) String id,
			@RequestParam(required = false) String email,
			@RequestParam(required = false) String idRol,
			HttpSession session) {
		Usuario usuario = getUsuario();
		Rol rol = rolService.buscarPorId(idRol);
		Miembro miembro = miembroService.buscarPorId(id);
		miembro.setRol(rol);
		miembro = miembroService.editar(miembroConverter.entidadModelo(miembro), usuario);
		return new ResponseEntity<>(new Response("El miembro ha sido guardado con éxito", miembro.getId(), HttpStatus.OK), HttpStatus.OK);
	}
	
	@PostMapping("/eliminar")
	public ResponseEntity<Object> eliminarMiembro(@RequestParam(required = false) String id,
			HttpSession session) {
		miembroService.eliminarMiembro(id, getEstudio(session), getUsuario());
		return new ResponseEntity<Object>(new Response("El miembro fue eliminado con éxito", "", HttpStatus.OK), HttpStatus.OK);
	}

	@GetMapping("/miembrosestudio")
	public ResponseEntity<List<MMiembro>> getAll(
			@RequestParam(name = "idEstudio") String idEstudio,
			@RequestParam(name = "idCausa") String idCausa) {
		List<MMiembro> miembros = miembroService.miembrosCombo(idEstudio, idCausa, getUsuario());
		return new ResponseEntity<>(miembros, HttpStatus.OK);
	}

	@PostMapping("/eliminarmiembrocausa")
	public ResponseEntity<MCausa> eliminarMiembroCausa(
		@RequestParam(name = "idCausa") String idCausa,
		@RequestParam(name = "idMiembros") String idMiembros) {
		MCausa causa = miembroService.eliminarMiembroCausa(idCausa, idMiembros, getUsuario());
		return new ResponseEntity<>(causa, HttpStatus.OK);
	}
	
	
	@PostMapping("/guardarmiembrocausa")
	public ResponseEntity<MCausa> guardarMiembrosCausa(
		@RequestParam(name = "idCausa") String idCausa,
		@RequestParam(name = "idEstudio") String idEstudio,
		@RequestParam(name = "idMiembros") List<String> idMiembros, 
		@RequestParam(name = "nuevo") String nuevo) {
		MCausa causa = miembroService.guardarMiembrosCausa(idCausa, idEstudio, idMiembros, getUsuario(), nuevo);
		return new ResponseEntity<>(causa, HttpStatus.OK);
	}
	
}
