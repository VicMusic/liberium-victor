package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.ChecklistModel;
import com.quinto.liberium.model.MChecklist;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.MiembroService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.SubItemService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/checklist")
public class ChecklistRestController extends Controlador {

	@Autowired
	private ChecklistService checklistService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private SubItemService subService;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private MiembroService miembroService;

	@PreAuthorize("hasAuthority('VISUALIZAR_CHECKLIST')")
	@PostMapping("/orden")
	public ResponseEntity<Integer> ordenFormulario(@RequestParam(required = true) String id) {
		Integer orden = itemService.ultimoNumero(id);
		return new ResponseEntity<>(orden, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/guardar")
	public ResponseEntity<Object> crearFormulario(HttpServletRequest request, HttpSession session) {

		String id = request.getParameter("id");
		String nombre = request.getParameter("nombre");
		String tipoChecklist = request.getParameter("tipoChecklist");
		String tipoFuero = request.getParameter("tipoFuero");
		String idPais = request.getParameter("idPais");
		String idMateria = request.getParameter("idMateria");
		String idProvincia = request.getParameter("idProvincia");
		String idCircunscripcion = request.getParameter("idCircunscripcion");
		String idInstancia = request.getParameter("idInstancia");
		String idTipo = request.getParameter("idTipoCausa");
		String idFormulario = request.getParameter("idFormulario");

		Checklist checkList = checklistService.guardar(id, nombre, tipoChecklist, idPais, tipoFuero, idMateria, idTipo,
				idProvincia, idCircunscripcion, idInstancia, idFormulario, getUsuario());
		try {
			itemService.gestionarItem(request, checkList, getUsuario());
		} catch (WebException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return new ResponseEntity<>(new Response("El checklist fue creado con éxito", "", HttpStatus.OK),
				HttpStatus.OK);
	}

	// @PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/guardarNuevoChecklist")
	public ResponseEntity<Checklist> guardarChecklisEstudio(HttpServletRequest request, HttpSession session) {

		String id = request.getParameter("idChecklist");
		String nombre = request.getParameter("nombreNuevoChecklist");
		String tipoChecklist = request.getParameter("tipoRolCausasChecklist");
		String idPais = request.getParameter("paisChecklist");
		String tipoFuero = request.getParameter("tipoFueroChecklist");
		String idMateria = request.getParameter("materiaChecklist");
		String idTipoCausa = request.getParameter("tipoCausaChecklist");
		String idProvincia = request.getParameter("provinciaChecklist");
		String idCircunscripcion = request.getParameter("circunscripcionChecklist");
		String idInstancia = request.getParameter("instanciaChecklist");
		String idFormulario = request.getParameter("formularioChecklist");

		Checklist checkList = checklistService.guardar(id, nombre, tipoChecklist, idPais, tipoFuero, idMateria,
				idTipoCausa, idProvincia, idCircunscripcion, idInstancia, idFormulario, getUsuario());
		try {
			itemService.gestionarItem(request, checkList, getUsuario());
		} catch (WebException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return new ResponseEntity<>(checkList, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/eliminar{id}")
	public ResponseEntity<MChecklist> eliminarChecklist(@RequestParam(required = false) String id) {
		MChecklist checkList = checklistService.eliminarChecklist(id, getUsuario());
		return new ResponseEntity<>(checkList, HttpStatus.OK);
	}
	
	@PostMapping("/cambiarVisibilidad{id}")
	public ResponseEntity<MChecklist> cambiarVisibilidadChecklist(@RequestParam(required = false) String id) {
		MChecklist checkList = checklistService.cambiarVisibilidadChecklist(id, getUsuario());
		return new ResponseEntity<>(checkList, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/buscar{id}")
	public ResponseEntity<ChecklistModel> buscarChecklist(@RequestParam(required = false) String id) {
		Checklist checkList = checklistService.buscarChecklist(id);
		ChecklistModel chmodel = new ChecklistModel();
		chmodel.setId(checkList.getId());
		chmodel.setNombre(checkList.getNombre());
		chmodel.setIdPais(checkList.getPais().getId());
		chmodel.setIdProvincia(checkList.getProvincia().getId());
		chmodel.setFuero(checkList.getTipoFuero());
		chmodel.setIdMateria(checkList.getMateria().getId());
		chmodel.setIdCircunscripcion(checkList.getCircunscripcion().getId());
		chmodel.setIdMateria(checkList.getMateria().getId());
		chmodel.setIdFormulario(checkList.getFormulario().getId());
		chmodel.setIdTipoCausa(checkList.getTipoCausa().getId());
		chmodel.setTipoRolCausa(checkList.getRol());
		return new ResponseEntity<>(chmodel, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/eliminarItem{id}")
	public ResponseEntity<MItem> eliminarItem(@RequestParam(required = false) String id) {
		MItem item = itemService.eliminarItem(id, getUsuario());
		return new ResponseEntity<>(item, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CHECKLIST')")
	@PostMapping("/eliminarSub")
	public ResponseEntity<MSubItem> eliminarSub(@RequestParam(required = false) String id) {

		Log log = LogFactory.getLog(getClass());

		log.info(id);

		MSubItem sItem = subService.eliminarSubItem(id, getUsuario());
		return new ResponseEntity<>(sItem, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('VISUALIZAR_CHECKLIST')")
	@PostMapping("/combo")
	public ResponseEntity<List<MChecklist>> listarPais(@RequestParam(required = false) String idPais) {
		return new ResponseEntity<>(checklistService.buscarPorPaisId(idPais), HttpStatus.OK);
	}

	@GetMapping("/combo1/{idPais}")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarProvincias(@PathVariable String idPais) {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelPorIdPais(idPais), HttpStatus.OK);
	}

//	@PostMapping("/combo2")
//	public ResponseEntity<List<MChecklist>> listarMateria(@RequestParam(required = false) String idMateria) {
//		return new ResponseEntity<>(checklistService.buscarPorMateriaId(idMateria), HttpStatus.OK);
//	}

	@PreAuthorize("hasAuthority('VISUALIZAR_CHECKLIST')")
	@GetMapping("/buscar/item/{idChecklist}")
	public @ResponseBody ResponseEntity<List<MItem>> buscarItems(@PathVariable String idChecklist) {
		List<MItem> items = itemService.buscarModelPorChecklistId(idChecklist);
		return new ResponseEntity<>(items, HttpStatus.CREATED);
	}

	@GetMapping("/buscar/checklists")
	public @ResponseBody ResponseEntity<List<MChecklist>> buscarChecklist(@RequestParam String idTipoCausa,
			@RequestParam String fuero, HttpSession session) {
		
		List<MChecklist> checklist = new ArrayList<MChecklist>();
		List<Usuario> admins = usuarioService.buscarAdministradores();
		List<MMiembro> miembrosModel = miembroService.buscarEMiembrosPoridEstudio(getEstudio(session).getId());
		
		for (Usuario usuario : admins) {
			if (fuero.equals("NACIONAL")) {
				checklist.addAll(checklistService.buscarPorTipoDeCausaIdYTipoFuero(idTipoCausa, TipoFuero.FEDERAL,
						usuario.getId()));
			} else {
				checklist.addAll(checklistService.buscarPorTipoDeCausaIdYTipoFuero(idTipoCausa, TipoFuero.PROVINCIAL,
						usuario.getId()));
			}
		}
		
		for(MMiembro miembro : miembrosModel) {
			checklist.addAll(checklistService.buscarPorUsuarioCreador(miembro.getmUsuario().getId()));
		}

		return new ResponseEntity<>(checklist, HttpStatus.OK);

	}

}
