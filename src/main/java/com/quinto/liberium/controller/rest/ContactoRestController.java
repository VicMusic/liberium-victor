package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoGenero;
import com.quinto.liberium.enumeration.TipoPersona;
import com.quinto.liberium.enumeration.TipoVinculo;
import com.quinto.liberium.model.MContacto;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ContactoService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/contacto")
public class ContactoRestController {
	
	private final Log LOG = LogFactory.getLog(ContactoRestController.class);

	
	@Autowired
	private ContactoService contactoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private CausaService causaService;
	
	@Autowired
	private CausaRepository causaRepository;
	
	@PostMapping("/guardar")
	@PreAuthorize("hasAuthority('EDITAR_CONTACTOS')")
	public ResponseEntity<MContacto> crearContacto(
			@RequestParam(name = "tipoPersona", required = false) TipoPersona tipoPersona,
			@RequestParam(name = "idEstudio", required = false) String idEstudio,
			@RequestParam(name = "tipoVinculo", required = false) TipoVinculo tipoVinculo,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "apellido", required = false) String apellido,
			@RequestParam(name = "idPais", required = false) String idPais,
			@RequestParam(name = "tipoGenero", required = false) TipoGenero tipoGenero,
			@RequestParam(name = "dni", required = false) String dni,
			@RequestParam(name = "cuitcuil", required = false) String cuilcuit,
			@RequestParam(name = "domicilio", required = false) String domicilio,
			@RequestParam(name = "numero", required = false) Integer alturaDomicilio,
			@RequestParam(name = "idProvincia", required = false) String idProvincia,
			@RequestParam(name = "idLocalidad", required = false) String idLocalidad,
			@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "telefonoFijo", required = false) String telefonoFijo,
			@RequestParam(name = "telefonoCelular", required = false) String telefonoCelular,
			@RequestParam(name = "nombreAdicional", required = false) String nombreAdicional,
			@RequestParam(name = "telefonoAdicional", required = false) String telefonoAdicional,
			@RequestParam(name = "observacion", required = false) String observacion,
			@RequestParam(name = "causaId", required = false) String causaId,
			@RequestParam(required=false) String id, 
			HttpSession session, Authentication usuario) {
		
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		
		LOG.info("METHOD guardarContacto() tipoPersona= " + tipoPersona + " idEstudio= " + idEstudio + " tipoVinculo= " + tipoVinculo + " nombre= " + nombre +
				" idPais= " + idPais + " tipoGenero= " + tipoGenero + " dni= " + dni + " cuil o cuit= " + cuilcuit +
				" domicilio= " + domicilio + " alturaDomicilio= " + alturaDomicilio + " idProvincia= " + idProvincia +
				" idLocalidad= " + idLocalidad + " email= " + email + " telefonoFijo= " + telefonoFijo + " telefonoCelular= " + telefonoCelular + 
				" nombreAdicional= "+ nombreAdicional + " telefonoAdicional= " + telefonoAdicional + " observacion= " + observacion);
		
		MContacto contacto = contactoService.guardar(tipoPersona, tipoVinculo, nombre, apellido, idPais, tipoGenero, dni, cuilcuit, domicilio, 
				alturaDomicilio, idProvincia, idLocalidad, email, telefonoFijo, telefonoCelular,nombreAdicional, telefonoAdicional, observacion, id, causaId, idEstudio, usuarioLog);
		
		return new ResponseEntity<>(contacto, HttpStatus.OK);	
	}
	
	@PostMapping("/agregar")
	@PreAuthorize("hasAuthority('EDITAR_CONTACTOS')")
	public ResponseEntity<MContacto> agregarContacto(
			@RequestParam(name = "contactoId", required = false) String contactoId,
			@RequestParam(name = "causaId", required = false) String causaId,
			@RequestParam(required=false) String id, 
			HttpSession session, Authentication usuario) {

		MContacto contacto = contactoService.agregarContacto(contactoId, causaId);
		
		return new ResponseEntity<>(contacto, HttpStatus.OK);	
	}
	
	@PostMapping("/quitar")
	@PreAuthorize("hasAuthority('EDITAR_CONTACTOS')")
	public ResponseEntity<MContacto> quitarContacto(
			@RequestParam(name = "contactoId", required = false) String contactoId,
			@RequestParam(name = "causaId", required = false) String causaId,
			@RequestParam(required=false) String id, 
			HttpSession session, Authentication usuario) {

		MContacto contacto = contactoService.quitarContacto(contactoId, causaId);
		
		return new ResponseEntity<>(contacto, HttpStatus.OK);	
	}
	
	@GetMapping("/buscarContacto")
	@PreAuthorize("hasAuthority('VISUALIZAR_CONTACTOS')")
	public ResponseEntity<Contacto> buscarContacto(@RequestParam String id){
		Contacto contacto = contactoService.buscarPorId(id);
		return new ResponseEntity<>(contacto, HttpStatus.OK);
	}
	
	@PostMapping("/eliminar")
	@PreAuthorize("hasAuthority('EDITAR_CONTACTOS')")
	public ResponseEntity<MContacto> eliminarContacto(@RequestParam(required=false) String id, @RequestParam(name = "causaId", required = false) String causaId, Authentication usuario) {
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MContacto contacto =  new MContacto();
		if(causaId != null && causaId != "" && !causaId.isEmpty()) {
			Causa causa = causaService.buscarPorId(causaId);
			for (Contacto contactoCausa : causa.getContacto()) {
				if(contactoCausa.getId().equals(id)) {
					causa.getContacto().remove(contactoCausa);
				}
			}
			causaRepository.save(causa);
		}else {
			contacto = contactoService.eliminarContacto(id, usuarioLog, causaId);
		}
		return new ResponseEntity<>(contacto, HttpStatus.OK);
	}
	
	@GetMapping("/combo1/{idPais}")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarProvincias(@PathVariable String idPais) {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelPorIdPais(idPais), HttpStatus.OK);
	}
}
