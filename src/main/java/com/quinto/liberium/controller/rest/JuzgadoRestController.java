package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Juzgado;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.service.JuzgadoService;

@RestController
@RequestMapping("/api/juzgado")
public class JuzgadoRestController extends Controlador{
	
	@Autowired
	private JuzgadoService juzgadoService;
	
	@PreAuthorize("hasAuthority('EDITAR_JUZGADO')")
	@PostMapping("/guardar")
	public ResponseEntity<MJuzgado> crearJuzgado(
			@RequestParam(required=false) String id,
			@RequestParam String nombre, 
			@RequestParam TipoFuero tipoFuero,
			@RequestParam(required=false) String idPais,
			@RequestParam(required=false) String idProvincia,
			@RequestParam String idInstancia,
			@RequestParam(required=false) String circunscripcion, HttpSession session) {
		MJuzgado juzgado = juzgadoService.guardar(id, nombre, tipoFuero, idPais, idProvincia, null, circunscripcion, idInstancia, getUsuario());
		return new ResponseEntity<>(juzgado, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_JUZGADO')")
	@PostMapping("/clonar")
	public ResponseEntity<MJuzgado> clonarJuzgado(@RequestParam(required=false) String id, HttpSession session) {
		Juzgado juzgado =  juzgadoService.buscarJuzgadoPorId(id);
	    MJuzgado mJuzgado = juzgadoService.clonar(juzgado, getUsuario());
		return new ResponseEntity<>(mJuzgado, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_JUZGADO')")
	@PostMapping("/eliminar")
	public ResponseEntity<MJuzgado> eliminarPais(@RequestParam(required=false) String id) {
		MJuzgado juzgado = juzgadoService.eliminarJuzgado(id, getUsuario());
		return new ResponseEntity<>(juzgado, HttpStatus.OK);
	}
	
	@PostMapping("/combo")
	public ResponseEntity<List<MJuzgado>> listarJuzgado(@RequestParam(required=false) String idInstancia) {
		return new ResponseEntity<>(juzgadoService.buscarJuzgadoModelPorInstancia(idInstancia), HttpStatus.OK);
	}
	
}