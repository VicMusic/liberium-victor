package com.quinto.liberium.controller.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.converter.UsuarioConverter;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MUsuario;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.repository.UsuarioRepository;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.DiaNoLaboralService;
import com.quinto.liberium.service.EmailService;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioRestController extends Controlador {
	
	private Log log = LogFactory.getLog(UsuarioRestController.class);

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioConverter usuarioConverter;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private LocalidadService localidadService;

	@Autowired
	private DiaNoLaboralService diaNoLaboralService;
	
	@Autowired
	private EmailService emailService;
	
	@PostMapping(value = "/registrar")
	public ResponseEntity<Usuario> registrar(Model model,
			@RequestParam(required = false, name = "nombre") String nombre,
			@RequestParam(required = false, name = "email") String email,
			@RequestParam(required = false, name = "password") String password,
			@RequestParam(required = false, name = "idPais") String idPais,
			@RequestParam(required = false, name = "idProvincia") String idProvincia) {
		
		Usuario usuario = usuarioService.crear(null, nombre, email, password,idPais, idProvincia, TipoUsuario.USUARIO, true, null, null, false);
		
		diaNoLaboralService.setDiasNoLaboralesFederales(usuario);
		diaNoLaboralService.setDiasNoLaboralesProvinciales(usuario);
		
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_USUARIOS')")
	@PostMapping("/guardar")
	public ResponseEntity<Usuario> crearUsuario(
			@RequestParam(required = false) String id,
			@RequestParam(required = false) String nombre, 
			@RequestParam(required = false) String mail, 
			@RequestParam(required = false) String permisosChecked, 
			@RequestParam(required = false) TipoUsuario tipoUsuario, HttpSession session) {
		
		Usuario usuario = usuarioService.crear(id, nombre, mail, null, null, null, tipoUsuario, false, permisosChecked.split(";"), getUsuario(), false);
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}

	@PostMapping("/editar")
	public ResponseEntity<MUsuario> editar(
			@RequestParam(required = false, name = "id") String id, 
			@RequestParam(required = false, name = "nombre") String nombre,
			@RequestParam(required = false, name = "dni") String dni,
			@RequestParam(required = false, name = "fechaDeNacimiento") String fechaDeNacimiento, 
			@RequestParam(required = false, name = "telefono") String telefono,
			@RequestParam(required = false, name = "email") String email) {
		
		usuarioService.validarDatosUsuario(dni, telefono);
		Usuario original = usuarioRepository.buscarPorId(id);
		MUsuario edicion = new MUsuario();
		edicion.setNombre(nombre);
		edicion.setDni(dni);

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.forLanguageTag("es"));
		Date nacimiento;
		try {
			nacimiento = df.parse(fechaDeNacimiento);
			edicion.setFechaDeNacimiento(nacimiento);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		edicion.setTelefono(telefono);
		edicion.setEmail(email);

		usuarioService.editar(original, edicion);

		return new ResponseEntity<>(edicion, HttpStatus.OK);
	}

	@PostMapping("/enviar-mail-soporte")
	public ResponseEntity<?> enviarMailSoporte(
			@RequestParam(name = "email") String email,
			@RequestParam(name = "asunto") String asunto,
			@RequestParam(name = "texto") String texto){
		log.info("METHOD: enviarMailSoporte() params: email= " + email + " asunto= " + asunto + " texto= " + texto);
		if (!email.isEmpty() && !asunto.isEmpty() && !texto.isEmpty()) {
			if (!usuarioService.isValid(email)) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				try {
					emailService.enviarMailSoporte(email, asunto, texto);
					return new ResponseEntity<>(HttpStatus.OK);
				} catch (Exception e) {
					return new ResponseEntity<>(HttpStatus.FORBIDDEN);
				}
			}
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasAuthority('EDITAR_USUARIOS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MUsuario> eliminarUsuario(@RequestParam(required = false) String id) {
		MUsuario usuario = usuarioService.eliminarUsuario(id, getUsuario());
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('VISUALIZAR_PERFIL', 'VISUALIZAR_PERFIL_ADMINISTRADOR')")
	@PostMapping("/cambiar/password")
	public ResponseEntity<Object> cambiarPassword(@RequestParam(required = false) String password,
			@RequestParam(required = false) String newPassword,
			@RequestParam(required = false) String repeatPassword) {
		usuarioService.actualizarPassword(password, newPassword, repeatPassword, getUsuario());
		return new ResponseEntity<>(new Response("Actualización realizada con éxito", "", HttpStatus.OK), HttpStatus.OK);
	}
	
	@PostMapping("/turnoconsulta")
	public ResponseEntity<MAbogado> turnoConsulta(
			@RequestParam(required = false, name = "cantSemanas") Integer cantSemanas,
			@RequestParam(required = false, name = "pasos") String[] pasosCrudos) {
		MAbogado abogado = abogadoService.guardarInfoTurnoConsulta(cantSemanas, pasosCrudos, getUsuario());
		return new ResponseEntity<MAbogado>(abogado, HttpStatus.OK);
	}
	
	@PostMapping("/eliminarpaso")
	public ResponseEntity<MAbogado> eliminarPasoTurnoConsulta(
			@RequestParam(required = false, name = "paso") String paso) {
		System.out.println("METHOD: eliminarPasoTurnoConsulta paso= " + paso);
		MAbogado abogado = abogadoService.eliminarPaso(paso, getUsuario());
		return new ResponseEntity<MAbogado>(abogado, HttpStatus.OK);
	}
	
	@GetMapping("/buscar-usuario")
	public ResponseEntity<MUsuario> buscarUsuarioLogeado(){
		MUsuario usuario = usuarioService.buscarPorEmail(getUsuario().getEmail());
		log.info("METHOD buscarUsuarioLogeado() mailUsuario= " + usuario.getEmail());
		return new ResponseEntity<MUsuario>(usuario, HttpStatus.OK);
	}
	
	@GetMapping("/buscar-abogado")
	public ResponseEntity<MAbogado> buscarAbogadoLogeado(){
		MAbogado usuario = abogadoService.buscarMAbogadoPorMailUsuario(getUsuario().getEmail());;
		return new ResponseEntity<MAbogado>(usuario, HttpStatus.OK);
	}
	
	@GetMapping("/combo1/{id}")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarProvincias(@PathVariable String id) {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelPorIdPais(id), HttpStatus.OK);
	}
	
	@GetMapping("/combo2/{id}")
	@ResponseBody
	public ResponseEntity<List<MLocalidad>> listarLocalidad(@PathVariable String id) {
		return new ResponseEntity<>(localidadService.buscarListadoPorProvincia(id), HttpStatus.OK);
	}
	
	@GetMapping("/listar")
	@ResponseBody
	public ResponseEntity<List<MUsuario>> listarUusarios() {
		return new ResponseEntity<>(usuarioConverter.entidadesModelos(usuarioService.buscarTodosAbogados(null)), HttpStatus.OK);
	}

}
