package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.service.LocalidadService;

@RestController
@RequestMapping("/api/localidad")
public class LocalidadRestController extends Controlador{

	@Autowired
	private LocalidadService localidadService;
	
	@PreAuthorize("hasAuthority('EDITAR_LOCALIDADES')")
	@PostMapping("/guardar")
	public ResponseEntity<MLocalidad> crearLocalidad(@RequestParam(required=false) String id, @RequestParam String nombre, @RequestParam String idProvincia, HttpSession session) {
		MLocalidad localidad= localidadService.guardar(id, nombre, idProvincia, getUsuario());
		return new ResponseEntity<>(localidad, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_LOCALIDADES')")
	@PostMapping("/eliminar")
	public ResponseEntity<MLocalidad> eliminarLocalidad(@RequestParam(required=false) String id) {
		MLocalidad localidad = localidadService.eliminarLocalidad(id, getUsuario());
		return new ResponseEntity<>(localidad, HttpStatus.OK);
	}
	
	
	
}
