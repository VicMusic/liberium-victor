package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MJurisdiccion;
import com.quinto.liberium.service.JurisdiccionService;

@RestController
@RequestMapping("/api/jurisdiccion")
public class JurisdiccionRestController extends Controlador{

	@Autowired
	private JurisdiccionService jurisdiccionService;
	
	@PostMapping("/guardar")
	public ResponseEntity<MJurisdiccion> crearJurisdiccion(@RequestParam(required=false) String id, @RequestParam String nombre, @RequestParam String idProvincia, HttpSession session) {
		MJurisdiccion jurisdiccion= jurisdiccionService.guardar(id, nombre, idProvincia, getUsuario());
		return new ResponseEntity<>(jurisdiccion, HttpStatus.OK);
 			
	}
	
	@PostMapping("/eliminar")
	public ResponseEntity<MJurisdiccion> eliminarJurisdiccion(@RequestParam(required=false) String id) {
		MJurisdiccion jurisdiccion= jurisdiccionService.eliminarJurisdiccion(id, getUsuario());
		return new ResponseEntity<>(jurisdiccion, HttpStatus.OK);
	}

	
	@PostMapping("/combo")
	public ResponseEntity<List<MJurisdiccion>> listarJurisdiccion(@RequestParam(required=false) String idProvincia) {
		return new ResponseEntity<>(jurisdiccionService.buscarJurisdiccionModelPorProvincia(idProvincia), HttpStatus.OK);
	}
	
	
}
