package com.quinto.liberium.controller.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.model.MDiaNoLaboral;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.service.DiaNoLaboralService;
import com.quinto.liberium.service.ProvinciaService;

@RestController
@RequestMapping("/api/dianolaborable")
public class DiaNoLaboralRestController extends Controlador{
	
	@Autowired
	private DiaNoLaboralService diaNoLaboralService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@PreAuthorize("hasAuthority('EDITAR_DIAS_NO_LABORABLES')")
	@PostMapping("/guardar")
	public ResponseEntity<MDiaNoLaboral> guardar(
			@RequestParam(name = "idDiaNoLaborable", required = false)String idDiaNoLaborable,
			@RequestParam(name = "titulo", required = false) String titulo, 
			@RequestParam(name = "tipoEvento", required = false)String tipoEvento, 
			@RequestParam(name = "fechaInicio", required = false)String stringFechaInicio,
			@RequestParam(name = "tipoDiaNoLaborable", required = false)TipoDiaNoLaborable tipoDiaNoLaborable,
			@RequestParam(name = "fechaFin", required = false)String stringFechaFin, 
			@RequestParam(name = "idPais", required = false)String idPais, 
			@RequestParam(name = "idProvincia", required = false) String idProvincia){
		
		String pattern = "yyyy-MM-dd'T'HH:mm";
		
		DateFormat df = new SimpleDateFormat(pattern);

		Date fechaInicio = null;
		Date fechaFin = null;
		
		try {
			fechaInicio = df.parse(stringFechaInicio);
			fechaFin = df.parse(stringFechaFin);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MDiaNoLaboral diaNoLaboral = diaNoLaboralService.crear(idDiaNoLaborable, titulo, tipoEvento, fechaInicio, fechaFin, idPais, idProvincia, tipoDiaNoLaborable, getUsuario());
		
		return new ResponseEntity<MDiaNoLaboral>(diaNoLaboral, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_DIAS_NO_LABORABLES')")
	@PostMapping("/eliminar/{idDiaNoLaborable}")
	public ResponseEntity<MDiaNoLaboral> eliminar(@PathVariable(name = "idDiaNoLaborable")String idDiaNoLaborable){
		MDiaNoLaboral diaNolLaboral = diaNoLaboralService.eliminar(idDiaNoLaborable, getUsuario());
		return new ResponseEntity<MDiaNoLaboral>(diaNolLaboral, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DIAS_NO_LABORABLES')")
	@GetMapping("/buscardia/{idDiaNoLaborable}")
	public ResponseEntity<MDiaNoLaboral> buscar(@PathVariable(name = "idDiaNoLaborable")String idDiaNoLaborable){
		MDiaNoLaboral diaNoLaboral = diaNoLaboralService.buscarModeloDiaNoLaboralPorId(idDiaNoLaborable);
		return new ResponseEntity<MDiaNoLaboral>(diaNoLaboral, HttpStatus.OK);
	}
	
	@GetMapping("/combo/{idPais}")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarProvincias(@PathVariable String idPais) {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelPorIdPais(idPais), HttpStatus.OK);
	}
}
