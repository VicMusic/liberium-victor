package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/abogado")
public class AbogadoRestController extends Controlador{

//	private final Log LOG = LogFactory.getLog(AbogadoRestController.class);

	@Autowired
	public AbogadoService abogadoService;

	@Autowired
	public UsuarioService usuarioService;

	@Autowired
	public ProvinciaService provinciaService;

	@Autowired
	public MateriaService materiaService;

	@PostMapping("/guardar")
	public ResponseEntity<MAbogado> crearAbogado(
			@RequestParam(required = false) String id,
			@RequestParam String telefono, 
			@RequestParam String idProvincia,
			@RequestParam String materias,
			@RequestParam String mailUsuario, Authentication session) {

		Usuario usuario = usuarioService.buscarUsuarioPorEmail(session.getName());
		
		//MAbogado abogado = abogadoService.guardar(id, idProvincia, telefono, materias, mailUsuario, usuario);
		return new ResponseEntity<>(null, HttpStatus.OK);

	}
	
	@PostMapping("/actualizar")
	public ResponseEntity<MAbogado> editar(Authentication usuario, 
			@RequestParam String id,
			@RequestParam String idProvincia, 
			@RequestParam String matricula,
			@RequestParam String localidad,
			@RequestParam String domicilio, 
			@RequestParam List<String> materia, HttpServletRequest request) {
			
		System.out.println("Materias: " + materia);
		
		MAbogado abogado = abogadoService.guardar(id, localidad , idProvincia, domicilio, matricula, materia, usuario.getName());	
		return new ResponseEntity<MAbogado>(abogado, HttpStatus.OK);
	}

	@PostMapping("/eliminar")
	public ResponseEntity<MAbogado> eliminarAbogado(@RequestParam(required = false) String id, HttpSession session) {
		Usuario usuario = usuarioService.buscarPorId(session.getId());
		MAbogado abogado = abogadoService.eliminar(id, usuario);
		return new ResponseEntity<>(abogado, HttpStatus.OK);
	}

	@PostMapping("/comboprovincia")
	public ResponseEntity<List<MAbogado>> listarProvincia(@RequestParam(required = false) String idProvincia) {
		return new ResponseEntity<>(abogadoService.buscarAbogadoPorProvincia(idProvincia), HttpStatus.OK);
	}

//	@PostMapping("/combomateria")
//	public ResponseEntity<List<MAbogado>> listarMateria(@RequestParam(required = false) String idMateria) {
//		return new ResponseEntity<>(abogadoService.buscarAbogadoPorMateria(idMateria), HttpStatus.OK);
//	}
	
	@PostMapping("/duracion{duracion}")
	public ResponseEntity<MAbogado> guardarDuracion(@PathVariable(required = false) String duracion, Authentication session) {
		Abogado abogado = abogadoService.buscarAbogadoPorMailUsuario(session.getName());
		
		Integer tipoTurno = Integer.parseInt(duracion);
		MAbogado retorno = abogadoService.guardarDuracion(tipoTurno, abogado, session.getName());
		
		return new ResponseEntity<>(retorno, HttpStatus.OK);
	}
	
	@ResponseBody
	@GetMapping("/obtenerduracion")
	public ResponseEntity<MAbogado> obtenerDuracion() {
		MAbogado abogado = abogadoService.buscarMAbogadoPorMailUsuario(getUsuario().getEmail());
		return new ResponseEntity<>(abogado, HttpStatus.OK);
	}

}
