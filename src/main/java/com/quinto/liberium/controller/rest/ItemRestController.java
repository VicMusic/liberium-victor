package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.SubItemService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/item")
public class ItemRestController extends Controlador {


	@Autowired
	private CausaService causaService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private SubItemService subItemService;
	
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/eliminarItem")
	public ResponseEntity<MItem> eliminarItem(
			@RequestParam(name = "idItemEliminar") String id, 
			@RequestParam(name = "causaIdItemEliminar") String idCausa,
			Authentication usuario) {

		Usuario usuarioLogg = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MItem item = causaService.eliminarItem(id, idCausa, usuarioLogg);
		return new ResponseEntity<MItem>(item, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/eliminarSubItem")
	public ResponseEntity<MSubItem> eliminarSubItem(
			@RequestParam(name="subItemIdSubItemEliminar") String id, 
			@RequestParam(name="itemIdSubItemEliminar") String idItem, 
			@RequestParam(name = "causaIdSubItemEliminar") String idCausa,
			Authentication usuario) {

		Usuario usuarioLogg = usuarioService.buscarUsuarioPorEmail(usuario.getName());
        MSubItem subItem = causaService.eliminarSubItem(id, idItem, idCausa, usuarioLogg);
		return new ResponseEntity<MSubItem>(subItem, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/agregar/paso")
	public @ResponseBody ResponseEntity<MItem> agregarPaso(
			@RequestParam(name="id", required = false) String id, 
			@RequestParam(name="titulo") String titulo, 
			@RequestParam(name="ordenAnterior", required = false) Integer ordenAnterior, 
			@RequestParam(name="idCausaAgregarItem") String idCausa, 
			Authentication usuario) {
		
		Usuario usuarioLogg = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MItem item = itemService.agregarItem(id, titulo, ordenAnterior, idCausa, usuarioLogg);
//		if(item.getmSubItem().isEmpty()) {
//			itemService.agregarSubItem(null, item.getNombre(), item.getId(), null, idCausa, usuarioLogg);
//		}
		return new ResponseEntity<>(item, HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/agregar/subPaso")
	public @ResponseBody ResponseEntity<MSubItem> agregarSubPaso(
			@RequestParam(name="id", required = false) String id, 
			@RequestParam(name="titulo") String titulo, 
			@RequestParam(name="idItem") String idItem, 
			@RequestParam(name="ordenAnterior", required = false) Integer ordenAnterior, 
			@RequestParam(name="idCausa") String idCausa, 
			Authentication usuario) {

		Usuario usuarioLogg = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MSubItem subItem = itemService.agregarSubItem(id, titulo, idItem, ordenAnterior, idCausa, usuarioLogg);
		return new ResponseEntity<>(subItem, HttpStatus.CREATED);
	}
	
	@GetMapping("/ver/archivos")
	public ResponseEntity<List<Archivo>> verArchivos(@RequestParam(name="id") String id, @RequestParam(name="tipo") String tipo){
		List<Archivo> archivos = new ArrayList<>();
		if(tipo.equals("ITEM")) {
			archivos = itemService.verArchivosItem(id);
		}else {
			archivos = itemService.verArchivosSubItem(id);
		}
		return new ResponseEntity<>(archivos, HttpStatus.OK);
	}
	
	@GetMapping("/ver/escritos")
	public ResponseEntity<List<Escrito>> verEscritos(@RequestParam(name="id") String id, @RequestParam(name="tipo") String tipo){
		List<Escrito> escritos = new ArrayList<>();
		if(tipo.equals("ITEM")) {
			escritos = itemService.verEscritosItem(id);
		}else {
			escritos = itemService.verEscritosSubItem(id);
		}
		return new ResponseEntity<>(escritos, HttpStatus.OK);
	}
	
	@GetMapping("/ver/dias")
	public ResponseEntity<Integer> verDias(@RequestParam(name="id") String id, @RequestParam(name="tipo") String tipo){
		Integer cantDias = 0;
		if(tipo.equals("ITEM")) {
			Item item = itemService.buscarItemPorId(id);
			if(item.getLimite() != null && item.getLimite() > 0) {
				cantDias = item.getLimite();
			}
		}else {
			SubItem subItem = subItemService.buscarSubItemPorId(id);
			if(subItem.getLimite() != null && subItem.getLimite() > 0) {
				cantDias = subItem.getLimite();
			}
		}
		return new ResponseEntity<>(cantDias, HttpStatus.OK);
	}
}
