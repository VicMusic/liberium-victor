package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Estadistica;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.service.EstadisticaService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/estadistica")
public class EstadisticaRestController extends Controlador {

	@Autowired
	private UsuarioService usuarioService;

//	@Autowired
//	private FinanzaService ingresoService;
//
//	@Autowired
//	private ContableService contableService;
	
	@Autowired
	private EstadisticaService estadisticaService;
	
	public Locale locale = Locale.getDefault();
	public Calendar fecha = Calendar.getInstance();


	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS')")
	@GetMapping("/buscar/estadisticas/admin")
	public ResponseEntity<Map<String, Integer>> estadisticasAdmin(@RequestParam String nombre){
		Map<String, Integer> cantidadPorMes = estadisticaService.estadisticasAdmin(nombre);
		
		return new ResponseEntity<Map<String, Integer>>(cantidadPorMes, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/tiempo")
	public ResponseEntity<Estadistica> buscarTiempoPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("Tiempo de sesión " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/ingresos")
	public ResponseEntity<Estadistica> buscarIngresosPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("INGRESO " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/gastos")
	public ResponseEntity<Estadistica> buscarGastosPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("GASTO " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/otros/gastos")
	public ResponseEntity<Estadistica> buscaOtrosrGastosPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("OTROGASTO " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/ingresos/activos")
	public ResponseEntity<Estadistica> buscaActivosPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("ACTIVO " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	
	

	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/causas")
	public ResponseEntity<Estadistica> buscarCausasPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("Cantidad totales de Causas " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/causas/archivadas")
	public ResponseEntity<Estadistica> buscarCausasArchivadasPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("Cantidad totales de Causas archivadas " + this.fecha.get(Calendar.YEAR), usuarioLog);
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS')")
	@GetMapping("/buscar/causas/activas")
	public ResponseEntity<Estadistica> buscarCausasActivasPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("Cantidad totales de Causas activas " + this.fecha.get(Calendar.YEAR), usuarioLog);
		
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
	@GetMapping("/buscar/causas/estado")
	public ResponseEntity<List<Estadistica>> buscarCausasPorEstadoPorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		List<Estadistica> estadisticas = new ArrayList<>();
		Estadistica verde = estadisticaService.buscarPorNombre("Cantidad totales de Causas en verde " + this.fecha.get(Calendar.YEAR), usuarioLog);
		Estadistica rojo = estadisticaService.buscarPorNombre("Cantidad totales de Causas en rojo " + this.fecha.get(Calendar.YEAR), usuarioLog);
		Estadistica amarillo = estadisticaService.buscarPorNombre("Cantidad totales de Causas en amarillo " + this.fecha.get(Calendar.YEAR), usuarioLog);
		estadisticas.add(verde);
		estadisticas.add(rojo);
		estadisticas.add(amarillo);
		return new ResponseEntity<List<Estadistica>>(estadisticas, HttpStatus.OK);
	}
	
	@GetMapping("/buscar/causas/promedioAvance")
	public ResponseEntity<Estadistica> buscarCausasPromedioAvancePorUsuario(Authentication usuario){
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Estadistica estadistica = estadisticaService.buscarPorNombre("Cantidad promedio de estado de avance de las causas " + this.fecha.get(Calendar.YEAR), usuarioLog);
		return new ResponseEntity<Estadistica>(estadistica, HttpStatus.OK);
	}
	
}
