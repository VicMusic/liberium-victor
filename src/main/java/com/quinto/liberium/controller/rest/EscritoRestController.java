package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.converter.EscritoConverter;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MEscrito;
import com.quinto.liberium.model.MEscritoVariables;
import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.PreguntaService;

@RestController
@RequestMapping("/api/escrito")
public class EscritoRestController extends Controlador{
	private static final Logger log = LoggerFactory.getLogger(EscritoRestController.class);
	@Autowired
	private EscritoService escritoService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private FormularioService formularioService;
	
	@Autowired
	private PreguntaService preguntaService;
	
	@PreAuthorize("hasAuthority('EDITAR_ESCRITOS')")
	@PostMapping("/guardar")
	public ResponseEntity<MEscrito> crearEscrito(
			@RequestParam(required = false, name = "id") String idEscrito,
			@RequestParam(required = false, name = "titulo") String titulo,
			@RequestParam(required = false, name = "idTipoCausa") String idTipoCausa,
			@RequestParam(required = false, name = "texto") String texto,
			@RequestParam(required = true, name = "original") String original,
			@RequestParam(required = false) String idItem, 
			@RequestParam(required = false) String tipo, HttpSession session){
		MEscrito modelo = new MEscrito();
		if (original.equals("true")) {
			modelo = escritoService.guardar(idEscrito, titulo, idTipoCausa, texto, idItem, tipo, true, getUsuario());
		} else {
			modelo = escritoService.guardar(idEscrito, titulo, idTipoCausa, texto, idItem, tipo, false, getUsuario());
		}
		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_ESCRITOS')")
	@PostMapping("/hacerPropio/{id1}/{idItem}")
	public ResponseEntity<MEscrito> hacerPropioEscrito(
			@PathVariable("id1") String id1,
			@PathVariable("idItem") String idItem,
			HttpSession session){
		MEscrito modelo = new MEscrito();
		Escrito e = escritoService.buscarEscritoPorId(id1);
		
//			modelo = escritoService.guardar(null, e.getTitulo(), e.getTipoCausa().getId(), e.getTexto(), e.ge, e.get, true, getUsuario());
		
		
		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}
	
	@GetMapping("/variables")
	public ResponseEntity<MEscritoVariables> remplazarVariables(
			@RequestParam(name = "idItem", required = false) String idItemSubItem,
			@RequestParam(name = "idCausa", required = false) String idCausa,
			@RequestParam(name = "idEscrito", required = false) String idEscrito){
		MEscritoVariables escritoVariables = escritoService.crearEscritoVariabes(idItemSubItem, idCausa, idEscrito, getUsuario());
		return new ResponseEntity<MEscritoVariables>(escritoVariables, HttpStatus.OK);
	}
	
	
	@PostMapping("/guardar/item")
	public ResponseEntity<String> guardarEnItem(@RequestParam String idItemSubItem, @RequestParam String idEscritoUsado, @RequestParam String idEscritoNuevo){
		
		escritoService.guardarEnItem(idItemSubItem, idEscritoUsado, idEscritoNuevo);
		
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_ESCRITOS')")
	@PostMapping("/eliminar-de-paso/{idEscrito}/{idItem}/{tipo}")
	public ResponseEntity<MEscrito> eliminarEscritodePaso(
			@PathVariable("idEscrito") String idEscrito,
			@PathVariable("idItem") String idItem,
			@PathVariable("tipo") String tipo, HttpSession session){
		
		MEscrito modelo = escritoService.eliminarDeItem(idEscrito, idItem, tipo, getUsuario());
		
		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_ESCRITOS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MEscrito> eliminarEscrito(
			@RequestParam(required=false) String id, HttpSession session) {
		MEscrito mEscrito = escritoService.eliminarEscrito(id, getUsuario());
		return new ResponseEntity<>(mEscrito, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('VISUALIZAR_ESCRITOS')")
	@PostMapping("/combo")
	public ResponseEntity<List<MEscrito>> listarEscrito(@RequestParam(required=false) String titulo) {
		return new ResponseEntity<>(escritoService.buscarEscritoModelPorTitulo(titulo), HttpStatus.OK);
	}
	
	@GetMapping("/select/{opcion}")
	public ResponseEntity<List<MEscrito>> listarSelect(@PathVariable(required = true, name = "opcion") Integer opcion){
		List<MEscrito> escritos = new ArrayList<>();
		if (opcion.equals(1)) {
			escritos = escritoService.buscarOriginalesList();
		} else if (opcion.equals(0)) {
			escritos = escritoService.buscarLocales(getUsuario());
		} else {
			throw new PeticionIncorrectaException("Ocurrio un error :(");
		}
		return new ResponseEntity<List<MEscrito>>(escritos, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESCRITOS')")
	@GetMapping("/id/{idEscrito}")
	public ResponseEntity<MEscrito> traerEscrito(@PathVariable("idEscrito") String idEscrito){
		MEscrito escrito = escritoService.buscarEscritoModelPorId(idEscrito);
		if(escrito == null) {
			throw new PeticionIncorrectaException("El Escrito no se encuentra en base de Datos!");
		}
		return new ResponseEntity<>(escrito, HttpStatus.OK);
	}
	
	@GetMapping("/buscarvariables/{idTipoCausa}")
	public ResponseEntity<List<Pregunta>> buscarVariablesPreguntas(@PathVariable String idTipoCausa){
		List<Pregunta> preguntas = null;
		try {
			Formulario formulario = formularioService.buscarFormularioPorTipoDeCausa(idTipoCausa);
			preguntas = preguntaService.buscarPorFormulario(formulario.getId());
		} catch (Exception e) {
			log.info("ERROR: " + e);
		}
		return new ResponseEntity<List<Pregunta>>(preguntas, HttpStatus.OK);
	}
	
	@GetMapping("/traerportipocausa")
	public ResponseEntity<List<MEscrito>> traerPorTipoCausa(
			@RequestParam(name = "tipoCausa", required = true) String idTipoCausa){
		List<MEscrito> escritos = escritoService.buscarOriginalesPorTipoCausa(idTipoCausa);
		return new ResponseEntity<>(escritos, HttpStatus.OK);
	}
	
	
	@GetMapping("/ver")
	public ResponseEntity<List<Escrito>> verEscritosByIds(@RequestParam String[] ids){
		List<Escrito> escritos = new ArrayList<>();
		try {
			escritos = escritoService.buscarEscritoPorId(ids);
		} catch (Exception e) {
			log.info("ERROR: " + e);
		}
		return new ResponseEntity<List<Escrito>>(escritos, HttpStatus.OK);
	}
}
