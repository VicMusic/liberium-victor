package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Combo;
import com.quinto.liberium.model.MCircunscripcion;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.LocalidadService;

@RestController
@RequestMapping("/api/circunscripcion")
public class CircunscripcionRestController extends Controlador{
	
	@Autowired
	private CircunscripcionService circunscripcionService;
	
	@Autowired
	private LocalidadService localidadService;
	
	@PreAuthorize("hasAuthority('EDITAR_CIRCUNSCRIPCIONES')")
	@PostMapping("/guardar")
	public ResponseEntity<MCircunscripcion> crearCircunscripcion(
			@RequestParam(required=false) String id, 
			@RequestParam String nombre, 
			@RequestParam String idProvincia, 
			@RequestParam List<String> idLocalidades, 
			HttpSession session) {
		
		MCircunscripcion circunscripcion = circunscripcionService.guardar(id, nombre, idProvincia, idLocalidades, getUsuario());
		return new ResponseEntity<>(circunscripcion, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_CIRCUNSCRIPCIONES')")
	@PostMapping("/eliminar")
	public ResponseEntity<MCircunscripcion> eliminarCircunscripcion(@RequestParam(required=false) String id) {
		MCircunscripcion circunscripcion = circunscripcionService.eliminarCircunscripcion(id, getUsuario());
		return new ResponseEntity<>(circunscripcion, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value = "/combo", produces = "application/json")
	public ResponseEntity<String> listarCircunscripciones(@RequestParam(required=false) String idProvincia) {
		List seleccionable = circunscripcionService.buscarCircunscripcionModelPorProvincia(idProvincia);
		Combo combo = new Combo(seleccionable);
		return new ResponseEntity<>(combo.getCombo(), HttpStatus.OK);
	}
	
	@PostMapping("/buscar")
	public ResponseEntity<MCircunscripcion> buscarCircunscripcionesPorId(@RequestParam(required=false) String id) {
		MCircunscripcion circunscripcion = circunscripcionService.buscarCircunscripcionModelPorId(id);
		return new ResponseEntity<>(circunscripcion, HttpStatus.OK);
	}
}
