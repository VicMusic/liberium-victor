package com.quinto.liberium.controller.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Horario;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.enumeration.TipoGenero;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MConsultaTurno;
import com.quinto.liberium.model.MEvento;
import com.quinto.liberium.model.MFormularioDinamico;
import com.quinto.liberium.model.MPregutasYRespuestas;
import com.quinto.liberium.model.MRespuesta;
import com.quinto.liberium.model.MTurno;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.HorarioService;
import com.quinto.liberium.service.TipoCausaService;
import com.quinto.liberium.service.TurnoService;
import com.quinto.liberium.util.Fecha;

@RestController
@RequestMapping("/api/consultas")
public class LandingRestController {

	private static final Log LOG = LogFactory.getLog(LandingRestController.class);

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private AbogadoService abogadoService;

	@Autowired
	private HorarioService horarioService;

	@Autowired
	private TurnoService turnoService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private FormularioService formularioService;

	@GetMapping("/abogados/{nombre}/{clienteMail}/{clienteNombre}/{clienteTelefono}")
	public ResponseEntity<List<MAbogado>> buscarNombreAbogado(@PathVariable(required = false) String nombre,
			@PathVariable(required = false) String clienteNombre,
			@PathVariable(required = false) String clienteTelefono,
			@PathVariable(required = false) String clienteMail) {

		String nombreParsed = nombre.substring(0, 1).toUpperCase() + nombre.substring(1);

		LOG.info("METHOD: buscarNombrePorAbogado() VALUES: nombre= " + nombreParsed);

		turnoService.validarMotivoConsulta(clienteNombre, clienteTelefono, clienteMail);

		List<MAbogado> abogado = abogadoService.buscarAbogadosPorNombre(nombreParsed);

		LOG.info("ABOGADOS ENCONTRADOS = " + abogado.size());

		return new ResponseEntity<>(abogado, HttpStatus.OK);
	}

	@PostMapping("/setturno/{idConsulta}/{idTurno}/{idFormulario}")
	public ResponseEntity<MRespuesta> setTurnoConsulta(@PathVariable(required = false) String idConsulta,
			@PathVariable(required = false) String idTurno, @PathVariable(required = false) String idFormulario) {

		LOG.info("METHOD: setTurnoConsulta() VALUES: idConsulta= " + idConsulta + " idTurno= " + idTurno
				+ " idFormulario= " + idFormulario);

		MRespuesta respuesta = new MRespuesta();

		consultaService.setFormularioYTurno(idTurno, idFormulario, idConsulta);

		respuesta.setMensaje("OK");
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping("/abogadosPorMyP/{pais}/{provincia}/{localidad}/{materia}/{clienteNombre}/{clienteTelefono}/{clienteMail}")
	public ResponseEntity<List<MAbogado>> buscarAbogadoPorMateriaProvincia(@PathVariable(name = "pais") String pais,
			@PathVariable(name = "provincia") String provincia, @PathVariable(name = "localidad") String localidad,
			@PathVariable(name = "materia") String materia, @PathVariable(name = "clienteNombre") String clienteNombre,
			@PathVariable(name = "clienteTelefono") String clienteTelefono,
			@PathVariable(name = "clienteMail") String clienteMail) {

		LOG.info("ID PAIS: " + pais);
		LOG.info("ID PROVINCIA: " + provincia);
		LOG.info("ID LOCALIDAD: " + localidad);
		LOG.info("ID MATERIA: " + materia);
		LOG.info("NOMBRE CLIENTE: " + clienteNombre);
		LOG.info("TELEFONO CLIENTE: " + clienteTelefono);
		LOG.info("EMAIL CLIENTE: " + clienteMail);

		turnoService.validarMotivoConsulta(clienteNombre, clienteTelefono, clienteMail);

		List<MAbogado> mAbogado = abogadoService.buscarAbogadosPorProvinciaYEspecialidad(provincia, materia);

		return new ResponseEntity<>(mAbogado, HttpStatus.OK);
	}

	@GetMapping("/formulario/{idConsulta}")
	public ResponseEntity<MFormularioDinamico> buscarFormularioDinamico(
			@PathVariable(name = "idConsulta") String idConsulta) {

		LOG.info("METHOD: buscarFormularioDinamico() ID CONSULTA: " + idConsulta);

		Consulta consulta = consultaService.buscarPorId(idConsulta);

		MFormularioDinamico formularioDinamico = consultaService.crearFormularioDinamico(consulta);

		return new ResponseEntity<>(formularioDinamico, HttpStatus.OK);
	}

	@GetMapping("/turnos/{id}")
	public ResponseEntity<MConsultaTurno> turnos(@PathVariable String id, @RequestParam(required = false) String dia) {

		LOG.info("METHOD: turnos() VALUES:  id= " + id);

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);

		DateTime fecha = new DateTime();
		if (dia != null) {
			fecha = new DateTime(Fecha.parseFechaGuiones(dia));
		}

		MConsultaTurno consulta = new MConsultaTurno();

		consulta.setId(id);

		Abogado abogado = abogadoService.buscarAbogadoPorId(id);

		consulta.setNombre(abogado.getUsuario().getNombre());
		List<Horario> horarios = horarioService.buscarHorariosPorAbogado(id);

		for (Horario horario : horarios) {
			String codigo = horario.getDia() + horario.getHora().replace(':', ' ').replaceAll(" ", "");
			consulta.getDisponibles().add(codigo);
		}

		List<MEvento> horariosCalendario = new ArrayList<>();

		for (Horario horario : horarios) {
			DateTime fechaActual = new DateTime();
			switch (horario.getDia()) {
			case "L":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(1);
				break;
			case "M":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(2);
				break;
			case "X":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(3);
				break;
			case "J":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(4);
				break;
			case "V":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(5);
				break;
			case "S":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(6);
				break;
			case "D":
				fechaActual = new DateTime(DateTimeZone.forID("America/Argentina/Buenos_Aires"))
						.withTime(Integer.parseInt(horario.getHora().substring(0, 2)),
								Integer.parseInt(horario.getHora().substring(3, 5)), 0, 0)
						.withDayOfWeek(7);
				break;
			default:
				break;
			}

			int cantSemanas = 2;
			if (abogado.getCantSemanas() != null) {
				cantSemanas = abogado.getCantSemanas();
			}

			for (int i = 0; i < cantSemanas; i++) {
				MEvento mEvento = new MEvento();
				if (i == 0) {
					mEvento.setFechaInicio(fechaActual.toString());
					if (abogado.getDuracion() == 30) {
						mEvento.setFechaFin(fechaActual.plusMinutes(30).toString());
					} else {
						mEvento.setFechaFin(fechaActual.plusHours(1).toString());
					}
				} else {
					mEvento.setFechaInicio(fechaActual.plusWeeks(1).toString());
					if (abogado.getDuracion() == 30) {
						mEvento.setFechaFin(fechaActual.plusWeeks(1).plusMinutes(30).toString());
					} else {
						mEvento.setFechaFin(fechaActual.plusWeeks(1).plusHours(1).toString());
					}
					fechaActual = fechaActual.plusWeeks(1);
				}
				mEvento.setId(mEvento.getFechaInicio());
				mEvento.setTitulo(horario.getHora());
				horariosCalendario.add(mEvento);
			}

		}

		consulta.setHorarios(horariosCalendario);

		DateTime inicio = new DateTime(fecha.dayOfWeek().withMinimumValue()).plusDays(-1);

		String titulo = new SimpleDateFormat("MMMM dd", new Locale("es", "ES")).format(inicio.plusDays(1).toDate());

		for (int n = 0; n < 7; n++) {
			inicio = inicio.plusDays(1);
			consulta.getDias().add(inicio.getDayOfMonth());
		}

		consulta.setTitulo(titulo.toUpperCase() + " - " + consulta.getDias().get(6));

		return new ResponseEntity<>(consulta, cabecera, HttpStatus.OK);
	}

	@PostMapping("/crearturno/{fechaElegida}/{idAbogado}/{mailCliente}/{idConsulta}")
	public ResponseEntity<MTurno> guardarTurno(@PathVariable String fechaElegida, @PathVariable String idAbogado,
			@PathVariable String mailCliente,@PathVariable String idConsulta) {

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		MTurno turno = turnoService.guardar(fechaElegida, idAbogado, mailCliente);
		if(idConsulta != null && !idConsulta.equals("a")) {
			Consulta c = consultaService.buscarPorId(idConsulta);
			c.setTurno(turnoService.buscarPorId(turno.getId()));
			consultaService.save(c);
		}

		return new ResponseEntity<>(turno, cabecera, HttpStatus.OK);
	}

	@PostMapping("/borrarturno/{idTurno}")
	public ResponseEntity<MTurno> borrarTurno(@PathVariable String idTurno) {
		turnoService.eliminar(idTurno);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@GetMapping("/notificarturno/{mailAbogado}/{nombreCliente}/{telefonoCliente}/{mailCliente}/{fechaTurno}")
	public ResponseEntity<MTurno> notificarTurno(
			@PathVariable(required = false, name = "mailAbogado") String mailAbogado,
			@PathVariable(required = false, name = "nombreCliente") String nombreCliente,
			@PathVariable(required = false, name = "telefonoCliente") String telefonoCliente,
			@PathVariable(required = false, name = "mailCliente") String mailCliente,
			@PathVariable(required = false, name = "fechaTurno") String fechaTurno) throws ParseException {

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		if (mailAbogado.contains("@")) {
			MTurno turno = turnoService.notificarTurnoAbogado(mailAbogado, nombreCliente, telefonoCliente, mailCliente, fechaTurno);
			return new ResponseEntity<>(turno, cabecera, HttpStatus.OK);
		} else {
			if(!mailAbogado.equals("a")) {
				Abogado abogado = abogadoService.buscarAbogadoPorId(mailAbogado);
				mailAbogado = abogado.getUsuario().getEmail();
			}else {
				Turno turno = turnoService.buscarTurnoPorClienteYFecha(mailCliente, fechaTurno);
				mailAbogado = turno.getAbogado().getUsuario().getEmail();
			}
			MTurno turno = turnoService.notificarTurnoAbogado(mailAbogado, nombreCliente, telefonoCliente, mailCliente, fechaTurno);
			return new ResponseEntity<>(turno, cabecera, HttpStatus.OK);

		}

	}

	@PostMapping("/guardarrespuestas")
	public ResponseEntity<MRespuesta> guardarRespuestas(
			@RequestParam(name = "respuesta" , required = false) List<String> respuesta,
			@RequestParam(name = "idPregunta" , required = false) List<String> idPreguntas,
			@RequestParam(name = "orden" , required = false) List<Integer> orden, 
			@RequestParam(name = "idConsulta" , required = false) String idConsulta) {

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);

		MRespuesta respuestaV = new MRespuesta();

		LOG.info("METHOD: guardarRespuestas() variables: idConsulta= " + idConsulta);

		try {
			Consulta consulta = consultaService.setRespuestas(orden, respuesta, idPreguntas, idConsulta);
			respuestaV.setMensaje("OK");
			respuestaV.setId(consulta.getId());

			return new ResponseEntity<>(respuestaV, cabecera, HttpStatus.OK);
		} catch (Exception e) {
			respuestaV.setError(e.getMessage());
			respuestaV.setMensaje("Ocurrió un error al crear la consulta.");
			return new ResponseEntity<>(respuestaV, cabecera, HttpStatus.OK);
		}
	}

	@GetMapping("/verificarabogados")
	public ResponseEntity<List<MAbogado>> revisarAbogados(
			@RequestParam(name = "idTipoCausa", required = false) String idTipoCausa,
			@RequestParam(name = "idLocalidad", required = false) String idLocalidad) {

		turnoService.validarConsulta(idLocalidad, idTipoCausa);

		TipoCausa tipoCausa = tipoCausaService.buscarPorId(idTipoCausa);

		List<MAbogado> abogados = abogadoService.buscarAbogadoPorLocalidadYMateria(idLocalidad,
				tipoCausa.getMateria().getId());

		return new ResponseEntity<>(abogados, HttpStatus.OK);
	}

	@PostMapping("/guardarconsulta")
	public ResponseEntity<MRespuesta> guardarConsulta(@RequestParam(required = false) String nombre,
			@RequestParam(required = false) String apellido, @RequestParam(required = false) String mail,
			@RequestParam(required = false) String dni, @RequestParam(required = false) String direccion,
			@RequestParam(required = false) String localidad, @RequestParam(required = false) String telefono,
			@RequestParam(required = false) TipoGenero genero, @RequestParam(required = false) String tipoCausa,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date nacimiento) {

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);

		MRespuesta respuesta = new MRespuesta();

		try {
			LOG.info("METHOD: guardarConsulta() VARIABLES: nombre= " + nombre + " apellido= " + apellido + " mail= "
					+ mail + " dni= " + dni + " direccion= " + direccion + " localidad= " + localidad + " telefono= "
					+ telefono + " genero= " + genero + " tipoCausa= " + tipoCausa + " fechaNacimiento= " + nacimiento);

			MConsulta consulta = consultaService.guardar(nombre, apellido, mail, dni, direccion, localidad, telefono,
					genero, tipoCausa, nacimiento);
			respuesta.setMensaje("OK");
			respuesta.setId(consulta.getId());
			respuesta.setMail(mail);
			return new ResponseEntity<>(respuesta, cabecera, HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setError(e.getMessage());
			respuesta.setMensaje("Ocurrió un error al crear la consulta.");
			return new ResponseEntity<>(respuesta, cabecera, HttpStatus.OK);
		}
	}

	@GetMapping("/buscarConsulta/{idConsulta}")
	public ResponseEntity<MConsulta> buscarConsulta(
			@PathVariable(name = "idConsulta", required = true) String idConsulta) {
		MConsulta consulta = consultaService.buscarModelPorId(idConsulta);
		return new ResponseEntity<MConsulta>(consulta, HttpStatus.OK);
	}
	
	@GetMapping("/buscarFormulario/{idFormulario}/{idConsulta}")
	public ResponseEntity<MPregutasYRespuestas> buscarFormulario(@PathVariable(required = true) String idFormulario,
			@PathVariable(required = true) String idConsulta) {

		LOG.info("METHOD buscarFomulario() idFormulario= " + idFormulario + " idConsulta= " + idConsulta);
		MPregutasYRespuestas preguntasRespuestas = formularioService.formarFormulario(idFormulario, idConsulta);
		return new ResponseEntity<MPregutasYRespuestas>(preguntasRespuestas, HttpStatus.OK);
	}
}
