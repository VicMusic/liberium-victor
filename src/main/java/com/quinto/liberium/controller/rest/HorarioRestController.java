package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.entity.Horario;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MHorario;
import com.quinto.liberium.service.HorarioService;
import com.quinto.liberium.service.UsuarioService;


@RestController
@RequestMapping("/api/horario")
public class HorarioRestController {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private HorarioService horarioService;
	
	@PostMapping("/guardar")
	public ResponseEntity<MHorario> guardarHorario(Authentication usuario, @RequestParam String[] horarios) {

		if (usuario != null) {
			Usuario creador = usuarioService.buscarUsuarioPorEmail(usuario.getName());
			horarioService.guardar(horarios, usuario.getName(), creador);
		}

		return new ResponseEntity<>(new MHorario(), HttpStatus.OK);
	}

	@PostMapping("/listado")
	public ResponseEntity<List<Horario>> listarHorarios(Authentication usuario) {

		List<Horario> horarios = new ArrayList<>();
		if (usuario != null) {
			horarios = horarioService.buscarHorariosPorMailAbogado(usuario.getName());
		}
		return new ResponseEntity<>(horarios, HttpStatus.OK);
	}
	
	@PostMapping("/listadoperfil")
	public ResponseEntity<List<MHorario>> listarHorariosPerfil(Authentication auth) {
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(auth.getName());
		List<MHorario> horarios = new ArrayList<>();
		if (usuario != null && usuario.isAbogado()) {
			horarios = horarioService.buscarHorariosPorMailAbogado2(auth.getName());
		}
		return new ResponseEntity<>(horarios, HttpStatus.OK);
	}

}
