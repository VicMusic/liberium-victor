package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MDependencia;
import com.quinto.liberium.service.DependenciaService;

@RestController
@RequestMapping("/api/dependencia")
public class DependenciaRestController extends Controlador{
	@Autowired
	private DependenciaService dependenciaService;
	
	@PreAuthorize("hasAuthority('EDITAR_DEPENDENCIA')")
	@PostMapping("/guardar")
	public ResponseEntity<MDependencia> crearDependencia(@RequestParam(required=false) String id, @RequestParam String nombre,@RequestParam String idTipoDependencia,@RequestParam String idInstancia, HttpSession session) {
		MDependencia provincia = dependenciaService.guardar(id, nombre, idTipoDependencia, idInstancia, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);	}
	
	@PreAuthorize("hasAuthority('EDITAR_DEPENDENCIA')")
	@PostMapping("/eliminar")
	public ResponseEntity<MDependencia> eliminarDependencia(@RequestParam(required=false) String id) {
		MDependencia provincia = dependenciaService.eliminar(id, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DEPENDENCIA')")
	@PostMapping("/combo")
	public ResponseEntity<List<MDependencia>> listarTipoDependencia(@RequestParam(required=false) String idTipoDependencia) {
		return new ResponseEntity<>(dependenciaService.buscarDependenciaModelPorIdTipoDependencia(idTipoDependencia), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DEPENDENCIA')")
	@PostMapping("/combo1")
	public ResponseEntity<List<MDependencia>> listarInstancia(@RequestParam(required=false) String idInstancia) {
		return new ResponseEntity<>(dependenciaService.buscarDependenciaModelPorIdInstancia(idInstancia), HttpStatus.OK);
	}
	
}
