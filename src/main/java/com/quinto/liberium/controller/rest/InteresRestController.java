package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MInteres;
import com.quinto.liberium.service.InteresService;

@RestController
@RequestMapping("/api/interes")
public class InteresRestController extends Controlador{
	
	@Autowired
	private InteresService interesService;

	@PreAuthorize("hasAuthority('EDITAR_INTERES')")
	@PostMapping("/guardar")
	public ResponseEntity<MInteres> crearInteres(
			@RequestParam(required=false) String id, 
			@RequestParam String nombre, @RequestParam Double porcentaje, HttpSession session) {
		MInteres interes = interesService.guardar(id, nombre, porcentaje, getUsuario());
		return new ResponseEntity<>(interes, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_INTERES')")
	@PostMapping("/eliminar")
	public ResponseEntity<MInteres> eliminarInteres(@RequestParam String id) {
		MInteres interes = interesService.eliminar(id, getUsuario());
		return new ResponseEntity<>(interes, HttpStatus.OK);
	}
	
}
