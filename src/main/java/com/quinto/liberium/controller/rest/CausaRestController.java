package com.quinto.liberium.controller.rest;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.converter.CausaConverter;
import com.quinto.liberium.converter.ItemConverter;
import com.quinto.liberium.converter.SubItemConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Notas;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MChecklist;
import com.quinto.liberium.model.MComentario;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.model.MNotas;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.model.Response;
import com.quinto.liberium.repository.NotasRepository;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.ChecklistService;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.ComentarioService;
import com.quinto.liberium.service.ConsultaService;
import com.quinto.liberium.service.EventoService;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.ItemService;
import com.quinto.liberium.service.JuzgadoService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.NotificacionService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.SubItemService;
import com.quinto.liberium.service.UsuarioService;

@RestController
@RequestMapping("/api/causa")
public class CausaRestController extends Controlador {

	@Autowired
	private ComentarioService comentarioService;

	@Autowired
	private CausaService causaService;

	@Autowired
	private ChecklistService checklistService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private ItemConverter itemConverter;

	@Autowired
	private SubItemConverter subItemConverter;

	@Autowired
	private JuzgadoService juzgadoService;

	@Autowired
	private NotificacionService notificacionService;

	@Autowired
	private CausaConverter causaConverter;

	@Autowired
	private EventoService eventoService;

	@Autowired
	private PaisService provinciaPais;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private MateriaService materiaService;

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private FormularioService formularioService;

	@PostMapping("/eliminar")
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	public ResponseEntity<MCausa> eliminarCausa(@RequestParam(name = "id", required = false) String id,
			Authentication usuario) {

		Usuario usuarioLogg = usuarioService.buscarUsuarioPorEmail(usuario.getName());

		MCausa causa = causaService.eliminar(id, usuarioLogg);

		return new ResponseEntity<MCausa>(causa, HttpStatus.OK);
	}

	@PostMapping("/comentar")
	public ResponseEntity<MComentario> crearComentario(@RequestParam(required = false) String id,
			@RequestParam String contenido, @RequestParam String idCausa, HttpSession session) {

		MComentario mComentario = comentarioService.guardar(id, contenido, idCausa, getUsuario());

		notificacionService.notificarComentarioUrl(idCausa,"/administracion/comentario/listado#agregar",mComentario.getId());

		return new ResponseEntity<MComentario>(mComentario, HttpStatus.OK);
	}

	@PostMapping("/eliminarComentario")
	public ResponseEntity<MComentario> eliminarComentario(@RequestParam(required = false) String id,
			HttpSession session) {

		MComentario mComentario = comentarioService.eliminarComentario(id, getUsuario());

		return new ResponseEntity<MComentario>(mComentario, HttpStatus.OK);
	}

	@GetMapping("/buscar/pasos/{idCausa}")
	public @ResponseBody ResponseEntity<List<MItem>> buscarPasos(@PathVariable String idCausa) {

		Causa causa = causaService.buscarPorId(idCausa);
		List<MItem> pasos = itemConverter.entidadesModelos(causa.getItems());
		Collections.sort(pasos, new Comparator<MItem>() {
			@Override
			public int compare(MItem i1, MItem i2) {
				return i1.getOrden().compareTo(i2.getOrden());
			}
		});
		return new ResponseEntity<>(pasos, HttpStatus.CREATED);

	}

	@GetMapping("/buscar/subPasos/{idItem}")
	public @ResponseBody ResponseEntity<List<MSubItem>> buscarSubPasos(@PathVariable String idItem) {

		List<MSubItem> subPasos = new ArrayList<>();
		Item paso = itemService.buscarItemPorId(idItem);
		if (!paso.getSubItems().isEmpty()) {
			subPasos = subItemConverter.entidadesModelos(paso.getSubItems());

		}
		Collections.sort(subPasos, new Comparator<MSubItem>() {
			@Override
			public int compare(MSubItem s1, MSubItem s2) {
				return s1.getOrden().compareTo(s2.getOrden());
			}
		});

		return new ResponseEntity<>(subPasos, HttpStatus.CREATED);

	}

	@PostMapping("/guardar")
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	public ResponseEntity<MCausa> crearCausa(
			// Causa
			@RequestParam(name = "id", required = false) String id,
			@RequestParam(name = "actorApellido") String actorApellido,
			@RequestParam(name = "actorNombre") String actorNombre,
			@RequestParam(name = "demandadoApellido") String demandadoApellido,
			@RequestParam(name = "demandadoNombre") String demandadoNombre,
			@RequestParam(name = "nroExpediente", required = false) String nroExpediente,
			@RequestParam(name = "tipoCausa", required = false) String tipoCausaId,
			@RequestParam(name = "idInstancia", required = false) String instanciaId,
			@RequestParam(name = "juzgadoOficina", required = false) String idJuzgado,
			@RequestParam(name = "idConsulta", required = false) String idConsulta,
			@RequestParam(name = "rolCausa", required = false) String rolCausa,
			@RequestParam(name = "tipoFuero", required = false) String tipoFuero,
			@RequestParam(name = "paisCausa", required = false) String idPais,
			@RequestParam(name = "provinciaCausa", required = false) String idProvincia,
			@RequestParam(name = "checklistSelect", required = false) String idChecklist, HttpSession session)
			throws IOException {

		String actor = actorApellido.toUpperCase() + ", " + actorNombre.substring(0, 1).toUpperCase()
				+ actorNombre.substring(1);
		String demandado = demandadoApellido.toUpperCase() + ", " + demandadoNombre.substring(0, 1).toUpperCase()
				+ demandadoNombre.substring(1);
		String nombre = actor + " c/ " + demandado + " p/ ";

		MCausa causaModelo = causaService.crearCausa(id, actor, nombre, demandado, nroExpediente, tipoCausaId,
				instanciaId, idJuzgado, idConsulta, rolCausa, tipoFuero, idPais, idProvincia, getEstudio(session),
				getUsuario());

		if (idChecklist != null) {
			if (!idChecklist.equals("0")) {
				System.out.println("ID Checklist If: " + idChecklist);
				clonarChecklist(causaModelo.getId(), idChecklist, session);
			}
		}
		return new ResponseEntity<MCausa>(causaModelo, HttpStatus.OK);
	}

	@PostMapping("/clonar/checklist")
	public ResponseEntity<MCausa> clonarChecklist(@RequestParam(name = "idCausa") String idCausa,
			@RequestParam(name = "idChecklist") String idChecklist, HttpSession session) throws IOException {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(auth.getName());
		Causa causa = causaService.buscarPorId(idCausa);
		MCausa mCausa = causaConverter.entidadModelo(causaService.copiar(idChecklist, causa, usuario));
		return new ResponseEntity<MCausa>(mCausa, HttpStatus.CREATED);
	}

	public MCausa clonarCheklistNuevaCausa(String idCausa, String idChecklist, Usuario usuario) throws IOException {
		Causa causa = causaService.buscarPorId(idCausa);
		MCausa mCausa = causaConverter.entidadModelo(causaService.copiar(idChecklist, causa, usuario));
		return mCausa;
	}

	@PostMapping("/archivar")
	public ResponseEntity<MCausa> archivarCausa(@RequestParam(name = "id") String id, HttpSession session) {
		Estudio estudio = getEstudio(session);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(auth.getName());
		MCausa causa = causaService.archivarCausa(id, usuario, estudio);
		return new ResponseEntity<MCausa>(causa, HttpStatus.CREATED);
	}

	@PostMapping("/activar")
	public ResponseEntity<MCausa> activarCausa(@RequestParam(name = "id") String id, HttpSession session) {
		Estudio estudio = getEstudio(session);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioService.buscarUsuarioPorEmail(auth.getName());
		MCausa causa = causaService.activarCausa(id, usuario, estudio);
		return new ResponseEntity<MCausa>(causa, HttpStatus.CREATED);
	}

	@PostMapping("/guardarNota")
	public ResponseEntity<Notas> guardarNota(@RequestParam(name = "idNota", required = false) String idNota,
			@RequestParam(name = "idAsignarNota") String idAsignarNota,
			@RequestParam(name = "tituloNota") String tituloNota, @RequestParam(name = "textoNota") String textoNota,
			HttpSession session) throws WebException {
		return new ResponseEntity<Notas>(causaService.guardarNota(idNota, idAsignarNota, tituloNota, textoNota),
				HttpStatus.OK);
	}

	@PostMapping("/eliminarNota")
	public ResponseEntity<Notas> eliminarNota(@RequestParam(name = "idNota", required = false) String idNota,
			HttpSession session) throws WebException {
		return new ResponseEntity<Notas>(causaService.eliminarNota(idNota), HttpStatus.OK);
	}

	@PostMapping("/notasTabla")
	public ResponseEntity<List<Notas>> cargarNotasTabla(@RequestParam(name = "idAsignarNota") String idAsignarNota,
			HttpSession session) throws WebException {
		return new ResponseEntity<List<Notas>>(causaService.listarNotasAsignadas(idAsignarNota), HttpStatus.OK);
	}

	@PostMapping("/contactoTabla")
	public ResponseEntity<List<Contacto>> cargarContactoTabla(@RequestParam(name = "causaId") String causaId,
			HttpSession session) throws WebException {
		List<Contacto> contactosList = causaService.buscarContactosConsulta(causaId);
		return new ResponseEntity<List<Contacto>>(contactosList, HttpStatus.OK);
	}

	@PostMapping("/causaconsulta/{idConsulta}")
	public ResponseEntity<MCausa> crearCausaConConsulta(
			@PathVariable(name = "idConsulta", required = true) String idConsulta, HttpSession session) {
		Estudio estudio = getEstudio(session);

		MConsulta consulta = consultaService.buscarModelPorId(idConsulta);

		System.out.println(consulta.getId());

		MCausa causa = new MCausa();

		causa.setActor(consulta.getNombre() + " " + consulta.getApellido());
		causa.setmTipoCausa(consulta.getmTipoCausa());
		causa.setNombre(consulta.getNombre() + " " + consulta.getApellido());
		causa.setmConsulta(consulta);

		try {
			MCausa causaM = causaService.guardar(causa, getUsuario(), estudio);
			System.out.println("idCausa= " + causaM.getId());
			return new ResponseEntity<MCausa>(causaM, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new PeticionIncorrectaException("ERROR - Ocurrió un error inesperado mientras se creaba la Causa.");
		}

	}

	@PostMapping("/checkearItem")
	public ResponseEntity<Item> checkearItem(@RequestParam String id, @RequestParam String idCausa,
			@RequestParam String idItemSiguiente, Authentication usuario) throws ParseException {
		Usuario creador = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Item itemSiguiente = itemService.buscarItemPorId(idItemSiguiente);
		Item item = itemService.checkear(id, idCausa, creador);
		if (itemSiguiente.getLimite() != null && itemSiguiente.getLimite() > 0) {
			String tipoPlazo = "";
			if (itemSiguiente.getHabil()) {
				tipoPlazo = "DIAS_HABILES";
			} else {
				tipoPlazo = "CORRIDO";
			}

			eventoService.crearEventoItemSubItem(idCausa, itemSiguiente.getId(), itemSiguiente.getNombre(), "PLAZO",
					tipoPlazo, new Date(), itemSiguiente.getLimite());
		}

		return new ResponseEntity<Item>(item, HttpStatus.OK);
	}

	@GetMapping("/contarcausas")
	public ResponseEntity<List<Integer>> contarCausasEstudio(@RequestParam(name = "idEstudio") String idEstudio) {
		List<Integer> numeros = new ArrayList<>();
		Integer causasActivas = causaService.contarActivasEstudio(idEstudio);
		Integer causasTotales = causaService.contarTodasCausasEstudio(idEstudio);
		numeros.add(causasActivas);
		numeros.add(causasTotales);
		return new ResponseEntity<List<Integer>>(numeros, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@PostMapping("/checkearSubItem")
	public ResponseEntity<MSubItem> checkearSubItem(@RequestParam String id, @RequestParam String idCausa,
			@RequestParam String idItem, @RequestParam(required = false) String idSubItemSiguiente,
			Authentication usuario) throws ParseException {

		Usuario creador = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MSubItem mSubItem = subItemService.checkearSubItem(id, idItem, idCausa, creador);

		Item item = itemService.buscarItemPorId(idItem);
		if (!item.getSubItems().isEmpty()) {
			boolean estadoCheck = true;
			for (int i = 0; i < item.getSubItems().size(); i++) {
				if (!item.getSubItems().get(i).isEstado()) {
					estadoCheck = false;
				}
			}
			if (item.isEstado() != estadoCheck) {
				item = itemService.checkear(item.getId(), idCausa, creador);
			}
		}

//		if ((idSubItemSiguiente != null) || (idSubItemSiguiente != "undefined")) {
//			SubItem subItemSiguiente = subItemService.buscarSubItemPorId(idSubItemSiguiente);
//			if (subItemSiguiente.getLimite() != null && subItemSiguiente.getLimite() > 0) {
//				String tipoPlazo = "";
//				if (subItemSiguiente.getHabil()) {
//					tipoPlazo = "DIAS_HABILES";
//				} else {
//					tipoPlazo = "CORRIDO";
//				}
//
//				eventoService.crearEventoItemSubItem(idCausa, subItemSiguiente.getId(), subItemSiguiente.getNombre(),
//						"PLAZO", tipoPlazo, new Date(), subItemSiguiente.getLimite());
//			}
//		}
		return new ResponseEntity<MSubItem>(mSubItem, HttpStatus.OK);
	}

	@PostMapping("/desCheckearSubItem")
	public ResponseEntity<MSubItem> desCheckearSubItem(@RequestParam String id, @RequestParam String idCausa,
			@RequestParam String idItem, Authentication usuario) throws ParseException {

		Usuario creador = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		MSubItem mSubItem = subItemService.checkearSubItem(id, idItem, idCausa, creador);
		itemService.checkear(idItem, idCausa, creador);

		return new ResponseEntity<MSubItem>(mSubItem, HttpStatus.OK);
	}

	@PostMapping("/eliminarconsulta/{id}")
	public ResponseEntity<MCausa> eliminarConsulta(@PathVariable(name = "id", required = true) String id) {
		MCausa causa = causaService.eliminarConsulta(id, getUsuario());
		return new ResponseEntity<MCausa>(causa, HttpStatus.OK);
	}

	@GetMapping("/buscar/juzgados")
	public ResponseEntity<List<MJuzgado>> buscarJuzgados(@RequestParam(name = "idInstancia") String idInstancia,
			@RequestParam(name = "idProvincia") String idProvincia, Authentication usuario) {
		return new ResponseEntity<>(
				juzgadoService.buscarPorIdInstanciaYIdTipoCausaYIdProvincia(idInstancia, idProvincia), HttpStatus.OK);
	}

	@GetMapping("/buscar/juzgadosProvincia")
	public ResponseEntity<List<MJuzgado>> buscarJuzgadosProvincia(
			@RequestParam(name = "idProvincia") String idProvincia, Authentication usuario) {
		return new ResponseEntity<>(juzgadoService.buscarPorIdProvincia(idProvincia), HttpStatus.OK);
	}

	@GetMapping("/combo1/{idPais}")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarProvincias(@PathVariable String idPais) {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelPorIdPais(idPais), HttpStatus.OK);
	}

}
