package com.quinto.liberium.controller.rest;

import static com.quinto.liberium.util.Constantes.PLAZO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MEvento;
import com.quinto.liberium.service.EventoService;
import com.quinto.liberium.service.TurnoService;
import com.quinto.liberium.service.UsuarioService;
import com.quinto.liberium.util.Fecha;;

@RestController
@RequestMapping("/api/calendario")
public class EventoRestController extends Controlador{
	
	@Autowired
	private EventoService eventoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private TurnoService turnoService;
	
	@PostMapping("/crear")
	@PreAuthorize("hasAuthority('EDITAR_AGENDA_PROPIA')")
	public ResponseEntity<Evento> crearEvento(
			@RequestParam(name = "idEvento", required = false) String idEvento,
			@RequestParam(name = "titulo") String titulo, 
			@RequestParam(name = "tipoEvento") String tipoEvento,
			@RequestParam(name = "fechaInicio")String stringFechaInicio, 
			@RequestParam(name = "fechaFin", required = false)String stringFechaFin, 
			@RequestParam(name = "cantDias", required = false) Integer cantDias, 
			@RequestParam(name = "tipoPlazo", required = false) String tipoPlazo,
			@RequestParam(name = "tipoDiaNoLaborable", required = false) TipoDiaNoLaborable tipoDiaNoLaborable,
			Authentication usuario) throws ParseException{
						
		System.out.println("idEvento= "+ idEvento +" Titulo= " + titulo + " tipoEvento= " + tipoEvento + " fechaInicio= " + stringFechaInicio + " fechaFin= " 
				+ stringFechaFin + " cantDias= " + cantDias + " tipoPlazo= " + tipoPlazo + " tipoDiaNoLaborable= " + tipoDiaNoLaborable);
				
		String pattern = "yyyy-MM-dd'T'HH:mm";
						
		DateFormat df = new SimpleDateFormat(pattern);

		Date fechaInicio = null;
		Date fechaFin = null;
		
		if (tipoEvento.equals(PLAZO)) {
			if (stringFechaInicio.equals("")) {
				throw new PeticionIncorrectaException("Debe seleccionar una fecha de inicio para crear el evento.");
			}
		} else if (!tipoEvento.equals(PLAZO)) {
			if (stringFechaInicio.equals("") || stringFechaFin.equals("")) {
				throw new PeticionIncorrectaException("Debe seleccionar una fecha de fin para crear el evento.");
			}
		}
		
//		if (tipoEvento.equals("PLAZO") && stringFechaInicio.equals("")) {
//			throw new PeticionIncorrectaException("Debe seleccionar una fecha de inicio para crear el evento.");
//		} else if(!tipoEvento.equals("PLAZO") && stringFechaInicio.equals("") || stringFechaFin.equals("")){
//			throw new PeticionIncorrectaException("Debe seleccionar una fecha de fin para crear el evento.");
//		}
		
		if (tipoEvento.equals(PLAZO)) {
			fechaInicio = df.parse(stringFechaInicio);
		} else {
			fechaInicio = df.parse(stringFechaInicio);
			fechaFin = df.parse(stringFechaFin);
		}
				
		Evento evento = eventoService.guardar(idEvento, titulo, tipoEvento, fechaInicio, fechaFin, cantDias, tipoPlazo, tipoDiaNoLaborable, null, getUsuario());
		
		return new ResponseEntity<>(evento, HttpStatus.OK);
	}
	
	@GetMapping("/listar/{idUsuario}")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<List<MEvento>> buscarEventos(@PathVariable(required = true, name = "idUsuario") String idUsuario){
		Usuario usuario = usuarioService.buscarPorId(idUsuario);
		List<MEvento> eventos = eventoService.buscarMEventosPorUsuario(usuario);
		eventoService.correccionFecheFinal(eventos);
		return new ResponseEntity<>(eventos, HttpStatus.OK);
	}
	
	@GetMapping("/listardiasnohabiles")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<List<MEvento>> buscarDiasNoHabiles(){
		List<MEvento> eventos = eventoService.buscarEventoNoLaboralesPorUsuario(getUsuario());
		return new ResponseEntity<>(eventos, HttpStatus.OK);
	}
	
	@GetMapping("/ver/dia")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<List<MEvento>> verEvento(@RequestParam(required = false) String dia, Authentication session) {
			
			Date diaActual = Fecha.parseFechaGuionesIngles(dia);
			Date diaPosterior = Fecha.diaPosterior(diaActual);
			
			Usuario usuario = usuarioService.buscarUsuarioPorEmail(session.getName());
			
			List<MEvento> eventos = eventoService.buscarEventosPorUsuarioYFechas(diaActual, diaPosterior, usuario);
					
			return new ResponseEntity<>(eventos, HttpStatus.OK);
	}
	
	@GetMapping("/ver/{id}")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<MEvento> editarEvento(
			@PathVariable(name = "id") String id, Authentication session) throws ParseException{
		MEvento evento = eventoService.buscarMEventoPorId(id);
		return new ResponseEntity<>(evento, HttpStatus.OK);
	}
	
	@GetMapping("/ver2/{id}")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<MEvento> editarEvento2(
			@PathVariable(name = "id") String id, Authentication session) throws ParseException{
		MEvento evento = eventoService.buscarMEventoPorId2(id);
		return new ResponseEntity<>(evento, HttpStatus.OK);
	}
	
	@PostMapping("/eliminar/{idEvento}")
	@PreAuthorize("hasAuthority('EDITAR_AGENDA_PROPIA')")
	public ResponseEntity<Evento> eliminarEvento(@PathVariable(name = "idEvento") String idEvento) {
		Evento evento = eventoService.eliminarEventoRest(idEvento, getUsuario());
		return new ResponseEntity<>(evento, HttpStatus.OK);
 			
	}
	
	@GetMapping("/q/{tipoEvento}")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<List<MEvento>> buscarEventoPorTipoEvento(@PathVariable(name = "tipoEvento") String tipoEvento){
		List<MEvento> eventos = eventoService.listarEventosPorTipoEvento(getUsuario(), tipoEvento);
		return new ResponseEntity<>(eventos, HttpStatus.OK);
	}
	
	@GetMapping("/qturno/{idEvento}")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public ResponseEntity<Turno> buscarTurnoPorEventoId(@PathVariable(name = "idEvento")String idEvento){
		Evento evento = eventoService.buscarPorId(idEvento);
		Turno turno = turnoService.buscarTurnoPorEventoId(evento);
		return new ResponseEntity<>(turno, HttpStatus.OK);
	}
	
}
