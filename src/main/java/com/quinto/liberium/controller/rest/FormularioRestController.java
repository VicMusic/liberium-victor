package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.model.MFormulario;
import com.quinto.liberium.model.MPregunta;
import com.quinto.liberium.model.MTipoCausa;
import com.quinto.liberium.service.FormularioService;
import com.quinto.liberium.service.PreguntaService;
import com.quinto.liberium.service.TipoCausaService;

@RestController
@RequestMapping("/api/formulario")
public class FormularioRestController extends Controlador{
	
	@Autowired
	private FormularioService formularioService;
	
	@Autowired
	private PreguntaService preguntaService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_FORMULARIOS')")
	@PostMapping("/orden")
	public ResponseEntity<Integer> ordenFormulario(@RequestParam(required=true) String id) {
		Integer orden = preguntaService.ultimoNumero(id);
		return new ResponseEntity<>(orden, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_FORMULARIOS')")
	@PostMapping("/guardar")
	public ResponseEntity<MFormulario> crearFormulario(HttpServletRequest request, HttpSession session) {
		String id = request.getParameter("id");
		String titulo = request.getParameter("titulo");
		String idTipo = request.getParameter("tipo");
		String idPais = request.getParameter("idPais");
		String idProvincia = request.getParameter("idProvincia");
		
		MFormulario formulario = formularioService.guardar(id, titulo, idTipo, idPais, idProvincia, getUsuario());
	
		return new ResponseEntity<MFormulario>(formulario, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_FORMULARIOS')")
	@PostMapping("/guardarpregunta")
	public ResponseEntity<List<Pregunta>> guardarPreguntaFormulario(
			@RequestParam(name = "idFormulario", required = false) String idFormulario,
			@RequestParam(name = "idPregunta", required = false) String idPregunta,
			@RequestParam(name = "textoPregunta", required = false) String textoPregunta, 
			@RequestParam(name = "tipoPregunta", required = false) String tipoPregunta,
			@RequestParam(name = "inputRespuesta", required = false) String inputRespuesta,
			@RequestParam(name = "opciones", required = false) List<String> opciones,
			@RequestParam(name = "variablePregunta", required = false) String variablePregunta) {
		
		preguntaService.guardar(idPregunta, textoPregunta,  tipoPregunta, inputRespuesta, opciones, variablePregunta, idFormulario, getUsuario());
		List<Pregunta> preguntas = preguntaService.buscarPorFormulario(idFormulario);
		
		return new ResponseEntity<List<Pregunta>>(preguntas, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_FORMULARIOS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MFormulario> eliminarFormulario(@RequestParam(required=false) String id) {
		MFormulario formulario = formularioService.eliminar(id, getUsuario());
		return new ResponseEntity<>(formulario, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_FORMULARIOS')")
	@PostMapping("/pregunta/eliminar/{idPregunta}")
	public @ResponseBody  ResponseEntity<MPregunta> eliminarPregunta(@PathVariable String idPregunta) {
		MPregunta pregunta = preguntaService.eliminar(idPregunta, getUsuario());
		return new ResponseEntity<>(pregunta, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_FORMULARIOS')")
	@GetMapping("/pregunta/buscar/{idPregunta}")
	public @ResponseBody  ResponseEntity<Pregunta> buscarPregunta(@PathVariable String idPregunta) {
		Pregunta pregunta = preguntaService.buscarPorId(idPregunta);
		return new ResponseEntity<>(pregunta, HttpStatus.OK);
	}
	
	@GetMapping("/combo/{idMateria}")
	@ResponseBody
	public ResponseEntity<List<MTipoCausa>> listarTipoCausas(@PathVariable String idMateria) {
		return new ResponseEntity<>(tipoCausaService.buscarPorMateriaId(idMateria), HttpStatus.OK);
	}
	
	@GetMapping("/buscar/preguntas/{idFormulario}")
	public @ResponseBody ResponseEntity<List<Pregunta>> buscarPreguntas(@PathVariable String idFormulario){
		List<Pregunta> preguntas = preguntaService.buscarPorFormulario(idFormulario);
		return new ResponseEntity<>( preguntas , HttpStatus.CREATED);
	}
}
