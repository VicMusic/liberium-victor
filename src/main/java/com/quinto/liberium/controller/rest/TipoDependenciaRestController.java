package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MTipoDependencia;
import com.quinto.liberium.service.TipoDependenciaService;

@RestController
@RequestMapping("/api/tipodependencia")
public class TipoDependenciaRestController extends Controlador {

	@Autowired
	private TipoDependenciaService tipoDependenciaService;

	@PreAuthorize("hasAuthority('EDITAR_TIPO_DE_DEPENDENCIA')")
	@PostMapping("/guardar")
	public ResponseEntity<MTipoDependencia> crearTipoDependencia(@RequestParam(required = false) String id,
			@RequestParam String nombre, HttpSession session) {
		MTipoDependencia tipoDependencia = tipoDependenciaService.guardar(id, nombre, getUsuario());
		return new ResponseEntity<>(tipoDependencia, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('EDITAR_TIPO_DE_DEPENDENCIA')")
	@PostMapping("/eliminar")
	public ResponseEntity<MTipoDependencia> eliminarTipoDependencia(@RequestParam(required = false) String id) {
		MTipoDependencia tipoDependencia = tipoDependenciaService.eliminarTipoDependencia(id, getUsuario());
		return new ResponseEntity<>(tipoDependencia, HttpStatus.OK);
	}



}
