package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MInstancia;
import com.quinto.liberium.service.InstanciaService;

@RestController
@RequestMapping("/api/instancia")
public class InstanciaRestController extends Controlador{
	
	@Autowired
	private InstanciaService instanciaService;
	
	@PreAuthorize("hasAuthority('EDITAR_INSTANCIAS')")
	@PostMapping("/guardar")
	public ResponseEntity<MInstancia> crearPais(@RequestParam(required=false) String id, @RequestParam String nombre, HttpSession session) {
		MInstancia instancia = instanciaService.guardar(id, nombre, getUsuario());
		return new ResponseEntity<>(instancia, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_INSTANCIAS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MInstancia> eliminarPais(@RequestParam(required=false) String id) {
		MInstancia instancia = instanciaService.eliminarInstancia(id, getUsuario());
		return new ResponseEntity<>(instancia, HttpStatus.OK);
	}
}
