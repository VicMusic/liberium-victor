package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Combo;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.service.ProvinciaService;

@RestController
@RequestMapping("/api/provincia")
public class ProvinciaRestController extends Controlador{
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@PreAuthorize("hasAuthority('EDITAR_PROVINCIAS')")
	@PostMapping("/guardar")
	public ResponseEntity<MProvincia> crearPais(@RequestParam(required=false) String id, @RequestParam String nombre, @RequestParam String idPais, HttpSession session) {
		MProvincia provincia = provinciaService.guardar(id, nombre, idPais, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_PROVINCIAS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MProvincia> eliminarPais(@RequestParam(required=false) String id) {
		MProvincia provincia = provinciaService.eliminarProvincia(id, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value = "/combo", produces = "application/json")
	public ResponseEntity<String> listarProvincia(@RequestParam(required=false, name = "idPais") String idPais) {
		List provincias = provinciaService.buscarProvinciaModelPorIdPais(idPais);
		Combo combo = new Combo(provincias);
		return new ResponseEntity<>(combo.getCombo(), HttpStatus.OK);
	}
	
	@GetMapping("/listar")
	@ResponseBody
	public ResponseEntity<List<MProvincia>> listarUusarios() {
		return new ResponseEntity<>(provinciaService.buscarProvinciaModelTodos(), HttpStatus.OK);
	}
	
}
