package com.quinto.liberium.controller.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.enumeration.TipoTransaccion;
import com.quinto.liberium.model.MContabilidadEstudio;
import com.quinto.liberium.model.MHonorario;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.service.ContabilidadEstudioService;
import com.quinto.liberium.service.HonorarioService;
import com.quinto.liberium.service.MiembroService;

@RestController
@RequestMapping("/api/contable")
public class ContabilidadEstudioRestController extends Controlador {

	@Autowired
	private ContabilidadEstudioService contabilidadEstudioService;
	
	@Autowired
	private HonorarioService honorarioService;
	
	@Autowired
	private MiembroService miembroService;
	
	@PostMapping("/guardar")
	private ResponseEntity<MContabilidadEstudio> guardar(
			@RequestParam(name = "idEstudio", required = false) String idEstudio, 
			@RequestParam(name = "idHonorario", required = false) String idHonorario,
			@RequestParam(name = "idMiembro", required = false) String idMiembro,
			@RequestParam(name = "tipoContabilidad", required = false) TipoTransaccion tipoContabilidad,
			@RequestParam(name = "detalle", required = false) String detalle,
			@RequestParam(name = "formaPago", required = false) String formaPago,
			@RequestParam(name = "fecha", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha,
			@RequestParam(name = "monto", required = false) Double monto){
		MContabilidadEstudio elementoContable = contabilidadEstudioService.guardar(idEstudio, idHonorario, idMiembro, tipoContabilidad, detalle, formaPago, fecha, monto, getUsuario());
		return new ResponseEntity<MContabilidadEstudio>(elementoContable, HttpStatus.OK);
	}
	
	@PostMapping("/eliminar/{id}")
	private ResponseEntity<MContabilidadEstudio> eliminar(
			@PathVariable(name = "id", required = true)String idContable){
		MContabilidadEstudio contabilidadEstudio = contabilidadEstudioService.eliminar(idContable, getUsuario());
		return new ResponseEntity<MContabilidadEstudio>(contabilidadEstudio, HttpStatus.OK);
	}
	
	@PostMapping("/eliminarHonorario/{id}")
	private ResponseEntity<MHonorario> eliminarHonorario(
			@PathVariable(name = "id", required = true)String idHonorario){
		MHonorario mHonorario = honorarioService.eliminar(idHonorario, getUsuario());
		return new ResponseEntity<MHonorario>(mHonorario, HttpStatus.OK);
	}
	
	@PostMapping("/guardarHonorario")
	private ResponseEntity<MHonorario> crearHonorario(
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "idEstudio", required = false) String idEstudio,
			@RequestParam(name = "tipoHonorario", required = false) String tipoHonorario){
		MHonorario mHonorario = honorarioService.guardarHonorario(null, nombre, tipoHonorario, null, idEstudio, getUsuario());
		return new ResponseEntity<MHonorario>(mHonorario, HttpStatus.OK);
	}
	
	@GetMapping("/buscar/{idEstudio}")
	private ResponseEntity<List<MContabilidadEstudio>> buscarPorEstudio(
			@PathVariable(name = "idEstudio") String idEstudio,
			@RequestParam(name = "fechaInicio", required = false) String fechaInicio,
			@RequestParam(name = "fechaFin", required = false) String fechaFin){
		List<MContabilidadEstudio> contabilidadEstudios = new ArrayList<>();
		if (!fechaInicio.equals("null") && !fechaFin.equals("null") && !fechaInicio.equals("undefined") && !fechaFin.equals("undefined")) {
			contabilidadEstudios = contabilidadEstudioService.buscarPorEstudioEntreFechas(idEstudio, fechaInicio, fechaFin);
		} else {
			contabilidadEstudios = contabilidadEstudioService.buscarPorEstudio(idEstudio);
		}
		return new ResponseEntity<List<MContabilidadEstudio>>(contabilidadEstudios, HttpStatus.OK);
	}
	
	@GetMapping("/buscarHonorarios/{idEstudio}")
	private ResponseEntity<List<MHonorario>> buscarHonorariosPorEstudio(
			@PathVariable(name = "idEstudio") String idEstudio){
		List<MHonorario> mHonorarios = honorarioService.buscarHonorariosPorEstudio(idEstudio);
		return new ResponseEntity<List<MHonorario>>(mHonorarios, HttpStatus.OK);
	}
	
	@GetMapping("/buscarMiembros/{idEstudio}")
	private ResponseEntity<List<MMiembro>> buscarMiembros(@PathVariable(name = "idEstudio") String idEstudio){
		List<MMiembro> miembros = miembroService.buscarMiembrosPoridEstudio(idEstudio);
		return new ResponseEntity<List<MMiembro>>(miembros, HttpStatus.OK);
	}
}
