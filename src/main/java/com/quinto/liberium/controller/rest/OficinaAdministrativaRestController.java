package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MOficinaAdministrativa;
import com.quinto.liberium.service.OficinaAdministrativaService;

@RestController
@RequestMapping("/api/oficinaadministrativa")
public class OficinaAdministrativaRestController extends Controlador{

	@Autowired
	private OficinaAdministrativaService oficinaService;
	
	@PostMapping("/guardar")
	public ResponseEntity<MOficinaAdministrativa> crearPais(@RequestParam(required=false) String id, @RequestParam String nombre, @RequestParam String idProvincia, HttpSession session) {
		MOficinaAdministrativa provincia = oficinaService.guardar(id, nombre, idProvincia, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);
 			
	}
	
	@PostMapping("/eliminar")
	public ResponseEntity<MOficinaAdministrativa> eliminarPais(@RequestParam(required=false) String id) {
		MOficinaAdministrativa provincia = oficinaService.eliminarOficinaAdministrativa(id, getUsuario());
		return new ResponseEntity<>(provincia, HttpStatus.OK);
	}

	
	@PostMapping("/combo")
	public ResponseEntity<List<MOficinaAdministrativa>> listarCircunscripciones(@RequestParam(required=false) String idProvincia) {
		return new ResponseEntity<>(oficinaService.buscarOficinaAdministrativaModelPorProvincia(idProvincia), HttpStatus.OK);
	}
	
	
}
