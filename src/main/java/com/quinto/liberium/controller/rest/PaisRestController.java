package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.service.PaisService;

@RestController
@RequestMapping("/api/pais")
public class PaisRestController extends Controlador{
	
	@Autowired
	private PaisService paisService;
	
	@PreAuthorize("hasAuthority('EDITAR_PAISES')")
	@PostMapping("/guardar")
	public ResponseEntity<MPais> crearPais(@RequestParam(required=false) String id, @RequestParam String nombre, HttpSession session) {
		MPais pais = paisService.guardar(id, nombre, getUsuario());
		return new ResponseEntity<>(pais, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_PAISES')")
	@PostMapping("/eliminar")
	public ResponseEntity<MPais> eliminarPais(@RequestParam(required=false) String id) {
		MPais pais = paisService.eliminar(id, getUsuario());
		return new ResponseEntity<>(pais, HttpStatus.OK);
	}
	
}
