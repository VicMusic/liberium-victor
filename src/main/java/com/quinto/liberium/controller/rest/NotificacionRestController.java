package com.quinto.liberium.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MNotificacion;
import com.quinto.liberium.service.NotificacionService;

@RestController
@RequestMapping("/api/notificacion")
public class NotificacionRestController extends Controlador{

	@Autowired
	private NotificacionService notificacionService;
	
	@GetMapping("/buscar")
	public ResponseEntity<List<MNotificacion>> buscarNotificacionesUserLog(){
		List<MNotificacion> notificaciones = notificacionService.buscarNotificacionesPorUsuario(getUsuario().getId());
		return new ResponseEntity<List<MNotificacion>>(notificaciones, HttpStatus.OK);
	}
	
	@PostMapping("/vistas")
	public ResponseEntity<List<MNotificacion>> marcarNotificacionesVistas(){
		List<MNotificacion> notificaciones = notificacionService.buscarNotificacionesPorUsuario(getUsuario().getId());
		notificacionService.marcarVistas(notificaciones, getUsuario());
		return new ResponseEntity<List<MNotificacion>>(notificaciones, HttpStatus.OK);
	}
}
