package com.quinto.liberium.controller.rest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.service.MateriaService;

@RestController
@RequestMapping("/api/materia")
public class MateriaRestController extends Controlador{
	
	@Autowired
	private MateriaService materiaService;
	
	@PreAuthorize("hasAuthority('EDITAR_MATERIAS')")
	@PostMapping("/guardar")
	public ResponseEntity<MMateria> crearPais(@RequestParam(required=false) String id, @RequestParam String nombre, HttpSession session) {
		MMateria materia = materiaService.guardar(id, nombre, getUsuario());
		return new ResponseEntity<>(materia, HttpStatus.OK);
 			
	}
	
	@PreAuthorize("hasAuthority('EDITAR_MATERIAS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MMateria> eliminarPais(@RequestParam(required=false) String id) {
		MMateria materia = materiaService.eliminarMateria(id, getUsuario());
		return new ResponseEntity<>(materia, HttpStatus.OK);
	}
}
