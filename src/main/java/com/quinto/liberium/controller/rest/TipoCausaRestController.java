package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.entity.Combo;
import com.quinto.liberium.model.MTipoCausa;
import com.quinto.liberium.service.TipoCausaService;

@RestController
@RequestMapping("/api/tipocausa")
public class TipoCausaRestController extends Controlador {

	@Autowired
	private TipoCausaService tipoCausaService;

	@PreAuthorize("hasAuthority('EDITAR_TIPO_DE_CAUSAS')")
	@PostMapping("/guardar")
	public ResponseEntity<MTipoCausa> crearTipoCausa(@RequestParam(required = false) String id,
			@RequestParam String nombre, @RequestParam String materiaId, HttpSession session) {
		MTipoCausa mtipoCausa = tipoCausaService.guardar(id, nombre, materiaId, getUsuario());
		return new ResponseEntity<>(mtipoCausa, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('EDITAR_TIPO_DE_CAUSAS')")
	@PostMapping("/eliminar")
	public ResponseEntity<MTipoCausa> eliminarTipoCausa(@RequestParam(required = false) String id) {
		MTipoCausa mTipoCausa = tipoCausaService.eliminarTipoCausa(id, getUsuario());
		return new ResponseEntity<>(mTipoCausa, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('EDITAR_CAUSA')")
	@GetMapping("/listar")
	public ResponseEntity<List<MTipoCausa>> buscarTipoCausa() {
		List<MTipoCausa> mTipoCausa = tipoCausaService.buscarTipoCausaModel();
		return new ResponseEntity<>(mTipoCausa, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value = "/combo", produces = "application/json")
	public ResponseEntity<String> listarProvincia(@RequestParam(required=false, name = "idMateria") String idMateria) {
		List tipoCausas = tipoCausaService.buscarPorMateriaId(idMateria);
		Combo combo = new Combo(tipoCausas);
		return new ResponseEntity<>(combo.getCombo(), HttpStatus.OK);
	}

}
