package com.quinto.liberium.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MProceso;
import com.quinto.liberium.service.ProcesoService;

@RestController
@RequestMapping("/api/proceso")
public class ProcesoRestController extends Controlador{
	
	@Autowired
	private ProcesoService procesoService;
	
	
	@PostMapping("/guardar")
	public ResponseEntity<MProceso> crearProceso(@RequestParam(required=false) String id, @RequestParam String nombre, @RequestParam String idMateria, HttpSession session) {
		MProceso proceso = procesoService.guardar(id, nombre, idMateria, getUsuario());
		return new ResponseEntity<>(proceso, HttpStatus.OK);
 			
	}
	
	@PostMapping("/eliminar")
	public ResponseEntity<MProceso> eliminarProceso(@RequestParam(required=false) String id) {
		MProceso proceso= procesoService.eliminarProceso(id, getUsuario());
		return new ResponseEntity<>(proceso, HttpStatus.OK);
	}
	
	@PostMapping("/combo")
	public ResponseEntity<List<MProceso>> listarMateria(@RequestParam(required=false) String idMateria) {
		return new ResponseEntity<>(procesoService.buscarProcesoModelPorMateria(idMateria), HttpStatus.OK);
	}
	
}
