package com.quinto.liberium.controller.rest;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quinto.liberium.controller.Controlador;
import com.quinto.liberium.model.MContabilidadCausa;
import com.quinto.liberium.model.MHonorario;
import com.quinto.liberium.model.MInteres;
import com.quinto.liberium.model.MRespuesta;
import com.quinto.liberium.service.ContabilidadCausaService;
import com.quinto.liberium.service.HonorarioService;
import com.quinto.liberium.service.InteresService;

@RestController
//@PreAuthorize("hasRole('ROLE_ABOGADO')")
@RequestMapping("/api/contable-causa")
public class ContabilidadCausaRestController extends Controlador {

	@Autowired
	private ContabilidadCausaService contabilidadCausaService;
	
	@Autowired
	private HonorarioService honorarioService;
	
	@Autowired
	private InteresService interesServce;

	@PostMapping("/calcular-juicio")
	public ResponseEntity<List<MContabilidadCausa>> calcularJuicio(
			@RequestParam(name = "idCausa", required = true) String idCausa,
			@RequestParam(name = "idHonorario", required = true) String idHonorario,
			@RequestParam(name = "posicionInteres", required = true) String posicionInteres,
			@RequestParam(name = "monto", required = false) Double monto,
			@RequestParam(name = "detalle", required = false) String detalle,
			@RequestParam(name = "idInteres", required = false) String idInteres,
			@RequestParam(name = "fechaInicio", required = false) String stringFechaInicio,
			@RequestParam(name = "fechaFin", required = false) String stringFechaFin) throws ParseException {

		List<MContabilidadCausa> contabilidadCausa = contabilidadCausaService.calcularInteres(monto, detalle,
				idInteres, stringFechaInicio, stringFechaFin, idCausa, idHonorario, posicionInteres, getUsuario());

		return new ResponseEntity<List<MContabilidadCausa>>(contabilidadCausa, HttpStatus.OK);
	}

	@PostMapping("/crear-gasto")
	public ResponseEntity<MContabilidadCausa> crearGasto(
			@RequestParam(name = "idCausa", required = true) String idCausa,
			@RequestParam(name = "monto", required = false) Double monto,
			@RequestParam(name = "fechaGasto", required = false) String stringFechaGasto,
			@RequestParam(name = "idInteres", required = false) String idInteres,
			@RequestParam(name = "fechaInicio", required = false) String stringFechaInicio,
			@RequestParam(name = "fechaFin", required = false) String stringFechaFin,
			@RequestParam(name = "detalle", required = false) String detalle,
			@RequestParam(name = "nombre", required = false) String nombre) throws ParseException {

		MContabilidadCausa contabilidadCausa = contabilidadCausaService.crearGasto(idCausa, monto, stringFechaGasto,
				idInteres, stringFechaInicio, stringFechaFin, detalle, nombre, getUsuario());

		return new ResponseEntity<MContabilidadCausa>(contabilidadCausa, HttpStatus.OK);
	}
	
	@PostMapping("/crear-cobro")
	public ResponseEntity<MContabilidadCausa> crearCobro(
			@RequestParam(name = "idCausa", required = true) String idCausa,
			@RequestParam(name = "idHonorario", required = true) String idHonorario,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "monto", required = false) Double monto,
			@RequestParam(name = "fechaGasto", required = false) String stringFechaGasto) throws ParseException{
		MContabilidadCausa contabilidadCausa = contabilidadCausaService.crearCobro(idCausa, idHonorario, nombre, monto, stringFechaGasto, getUsuario());
		return new ResponseEntity<MContabilidadCausa>(contabilidadCausa, HttpStatus.OK);
	}
	
	@PostMapping("/crear-instancia")
	public ResponseEntity<MHonorario> crearIntanciaDeHonorarios(
			@RequestParam(name = "id", required = false)String id,
			@RequestParam(name = "idCausa", required = true)String idCausa,
			@RequestParam(name = "nombre", required = false)String nombre){
		MHonorario honorario = honorarioService.guardarHonorario(id, nombre, null, idCausa, null, getUsuario());
		return new ResponseEntity<MHonorario>(honorario, HttpStatus.OK);
	}

	@PostMapping("/eliminar/{idContable}")
	public ResponseEntity<MRespuesta> eliminarItemContable(
			@PathVariable(name = "idContable", required = true) String idContable) {
		MRespuesta mRespuesta = contabilidadCausaService.eliminar(idContable, getUsuario());
		return new ResponseEntity<MRespuesta>(mRespuesta, HttpStatus.OK);
	}
	
	@PostMapping("/editar-vencimiento")
	public ResponseEntity<MContabilidadCausa> editarVencimiento(
			@RequestParam(name = "idElementoContable", required = true) String idContable,
			@RequestParam(name = "fechaVencimiento", required = false) String fechaVencimiento) throws ParseException {
		MContabilidadCausa contabilidadCausa = contabilidadCausaService.editarVencimiento(idContable, fechaVencimiento, getUsuario());
		return new ResponseEntity<>(contabilidadCausa, HttpStatus.OK);
	}
	
	@PostMapping("/set-tipo-instancia/{idHonorario}/{tipoInstancia}")
	public ResponseEntity<MHonorario> setTipoInstancia(
			@PathVariable(name = "idHonorario", required = true) String idHonorario,
			@PathVariable(name = "tipoInstancia", required = true) boolean tipoInstancia){
		MHonorario honorario = honorarioService.setTipoInstancia(idHonorario, tipoInstancia, getUsuario());
		return new ResponseEntity<MHonorario>(honorario, HttpStatus.OK);
	}

	@GetMapping("/buscar-contabilidad/{idCausa}")
	public ResponseEntity<List<MContabilidadCausa>> buscarContabilidadPorCausa(
			@PathVariable(name = "idCausa", required = true) String idCausa) {
		List<MContabilidadCausa> contabilidadCausa = contabilidadCausaService.buscarContabilidadPorCausa(idCausa);
		return new ResponseEntity<List<MContabilidadCausa>>(contabilidadCausa, HttpStatus.OK);
	}
	
	@GetMapping("/buscar-contable-honorario/{idHonorario}")
	public ResponseEntity<List<MContabilidadCausa>> buscarContabilidadesPorHonorario(
			@PathVariable(name = "idHonorario", required = true)String idHonorario){
		List<MContabilidadCausa> contabilidadCausas = contabilidadCausaService.buscarContabilidadesPorHonorario(idHonorario);
		return new ResponseEntity<List<MContabilidadCausa>>(contabilidadCausas, HttpStatus.OK);
	}
	
	@GetMapping("/buscar-honorarios/{idCausa}")
	public ResponseEntity<List<MHonorario>> buscarHonorariosPorCausa(
			@PathVariable(name = "idCausa", required = true) String idCausa){
		List<MHonorario> honorarios = honorarioService.buscarHonorariosPorCausa(idCausa);
		return new ResponseEntity<List<MHonorario>>(honorarios, HttpStatus.OK);
	}
	
	@GetMapping("/buscar-intereses")
	public ResponseEntity<List<MInteres>> buscarIntereses(){
		List<MInteres> interes = interesServce.buscarTodosMIntereses();
		return new ResponseEntity<List<MInteres>>(interes, HttpStatus.OK);
	}
	
}
