package com.quinto.liberium.controller;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.service.EventoService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("/calendario")
public class CalendarioController extends Controlador {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private EventoService eventoService;

	@GetMapping("/ver")
	@PreAuthorize("hasAuthority('VISUALIZAR_AGENDA_PROPIA')")
	public String verCalendario(ModelMap model, Authentication usuario,@RequestParam(required = false) String id1) {

		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());

		HashSet<String> otros = eventoService.buscarOtrosTipoDeEvento(usuarioLog);

		model.addAttribute("id1", id1);
		model.addAttribute("otros", otros);
		model.addAttribute("idUsuario", usuarioLog.getId());

		return "calendario";
	}
}