package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.service.MateriaService;

@Controller
@RequestMapping("/administracion/materia")
public class MateriaController extends Controlador {

	@Autowired
	private MateriaService materiaService;

	@PreAuthorize("hasAuthority('VISUALIZAR_MATERIAS')")
	@GetMapping("/listado")
	public String listado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Materia> materias = materiaService.buscarTodos(paginable, q);
		
		if(materias.getTotalPages() < 1 ) {
			materias = materiaService.buscarTodos(paginable, null);
			if(materias.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("modulo", "materias");
		model.put("page", materias);
		model.put("formulario", new Materia());

		// FIXME
//		ordenar(paginable, model);

		return "materia";
	}

}
