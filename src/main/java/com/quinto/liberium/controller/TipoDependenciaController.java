package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.service.TipoDependenciaService;

@Controller
@RequestMapping("/administracion/tipodependencia")
public class TipoDependenciaController {

	@Autowired
	private TipoDependenciaService tipoDependenciaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_TIPO_DE_DEPENDENCIA')")
	@GetMapping("/listado")
	public String listado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<TipoDependencia> tipoDependencias = tipoDependenciaService.buscarTodos(paginable, q);
		
		if(tipoDependencias.getTotalPages() < 1 ) {
			tipoDependencias = tipoDependenciaService.buscarTodos(paginable, null);
			if(tipoDependencias.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("page", tipoDependencias);
		model.put("modulo", "tipodependencias");
		model.put("formulario", new TipoDependencia());
		
		return "tipodependencia";
	}
	
}
