package com.quinto.liberium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.model.MInteres;
import com.quinto.liberium.service.InteresService;

@Controller
//@PreAuthorize("hasRole('ROLE_USUARIO')")	
@RequestMapping("/contabilidad-causa")
public class ContabilidadCausaController {
	
	@Autowired
	private InteresService interesService;

	@GetMapping("/ver/{idCausa}")
	public String ver(@PathVariable(required = true) String idCausa, 
			Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		List<MInteres> intereses = interesService.buscarTodosMIntereses();

		model.put("idCausaString", idCausa);
		model.put("modulo", "contabilidadCausa");
		model.put("intereses", intereses);
		
		return "contabilidadCausa";
	}
	
}
