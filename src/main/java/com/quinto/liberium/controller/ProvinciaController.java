package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;

@Controller
@RequestMapping("/administracion/provincia")
public class ProvinciaController extends Controlador {

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private PaisService paisService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_PROVINCIAS')")
	@GetMapping("/listado")
	public String listado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Provincia> provincias = provinciaService.buscarTodos(paginable, q);
		Page<Pais> paises = paisService.buscarTodos(null, null);

		if(provincias.getTotalPages() < 1 ) {
			provincias = provinciaService.buscarTodos(paginable, null);
			if(provincias.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("paises", paises);
		model.put("modulo", "provincia");
		model.put("page", provincias);
		model.put("formulario", new Provincia());

		// FIXME
//		ordenar(paginable, model);

		return "provincia";
	}

}
