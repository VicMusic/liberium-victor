package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Jurisdiccion;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.service.JurisdiccionService;
import com.quinto.liberium.service.ProvinciaService;

@Controller
@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
@RequestMapping("/administracion/jurisdiccion")
public class JurisdiccionController {
	
	@Autowired
	private JurisdiccionService jurisdiccionService;
	@Autowired
	private ProvinciaService provinciaService;
	
	@GetMapping("/listado")
	public String dependenciaListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Jurisdiccion> page = jurisdiccionService.buscarTodas(paginable, q);
		Page<Provincia> provincias = provinciaService.buscarTodos(null, null);

		model.put("provincias", provincias);
		model.put("modulo", "provincia");
		model.put("page", page);
		model.put("formulario", new Jurisdiccion());

		// FIXME
//		ordenar(paginable, model);

		return "jurisdiccion";
	}
}
