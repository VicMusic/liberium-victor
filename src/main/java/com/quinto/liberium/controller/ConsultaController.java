package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.service.ConsultaService;

@Controller
@RequestMapping("/consultas")
public class ConsultaController extends Controlador {
	
	@Autowired
	private ConsultaService consultaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_CONSULTA')")
	@GetMapping("/ver")
	public String verConsulta(ModelMap model,Pageable pageable, @RequestParam(required = false) String q) {
		Page<Consulta> consultas = consultaService.buscarConsutasPorAbogado(getUsuario(), pageable,q);
		model.addAttribute("page", consultas);
		return "consultas";
	}
}
