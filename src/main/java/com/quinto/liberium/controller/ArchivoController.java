package com.quinto.liberium.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.service.ArchivoService;

@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMINISTRADOR', 'ROLE_ABOGADO')")
@RequestMapping("/administracion/archivo")
public class ArchivoController extends Controlador {
	private final Log LOG = LogFactory.getLog(ArchivoController.class);
	
	@Autowired
	private ArchivoService archivoService;
	
	@PreAuthorize("hasAnyRole('ROLE_USUARIO','ROLE_ABOGADO','ROLE_ADMINISTRADOR')")
	@PostMapping("/guardar")
	public String guardarArchivo(MultipartFile archivo, @RequestParam("idItem") String idItem, @RequestParam("tipo") String tipo, @RequestParam String idCausa) {
		LOG.info("METHOD: guardarArchivo() --- PARAMS: archivo= " + archivo.toString() + 
				" - vacío?= " + archivo.isEmpty() + ", idItem= " + idItem + ", tipo= " + tipo);
		if (!archivo.isEmpty()) {
			archivoService.guardar(idItem, tipo, archivo, idCausa, getUsuario());
		} else {
			throw new EventoRestException("No se ha seleccionado ningun archivo...");
		}

		return "redirect:/administracion/causa/editar/"+ idCausa;
	}
	
	@PreAuthorize("permitAll()")
	@RequestMapping("/descargar/{idArchivo}")
	public void descargarArchivo(@PathVariable("idArchivo") String idArchivo, HttpServletResponse response, HttpServletRequest request) {
			archivoService.descargarArchivo(idArchivo, response);
	}
	
	@PreAuthorize("permitAll()")
    @GetMapping("/multimedia/{id}")
    public @ResponseBody ResponseEntity<byte[]> listado(HttpServletResponse response, @PathVariable String id) {
    	Archivo archivo = archivoService.buscarPorId(id);
    	return archivoService.abrirMultimedia(archivo,archivo.getNombre().substring(0, archivo.getNombre().lastIndexOf(".")),archivo.getNombre().substring(archivo.getNombre().lastIndexOf(".") + 1));
    }

}
