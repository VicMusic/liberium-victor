package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping()
public class LandingController {
	
	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private MateriaService materiaService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@RequestMapping({"/", ""})
	public String verVista(Model model, Pageable paginable) {

		Page<Materia> materias = materiaService.buscarTodos(paginable, null);
		Page<Pais> paises = paisService.buscarTodos(paginable, null);
		Page<Provincia> provincias = provinciaService.buscarTodos(paginable, null);
		Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(paginable, null);
		
		model.addAttribute("tipoCausas", tipoCausas);
		model.addAttribute("materias", materias);
		model.addAttribute("paises", paises);
		model.addAttribute("provincias", provincias);

		return "landing";
	}
}
