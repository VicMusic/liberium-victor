package com.quinto.liberium.controller;

import static com.quinto.liberium.util.Constantes.ESCRITO_VISTA;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.service.EscritoService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping("/administracion/escrito")
public class EscritosController extends Controlador{
	private final Log LOG = LogFactory.getLog(EscritosController.class);
	
	@Autowired
	private EscritoService escritoService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ESCRITOS')")
	@GetMapping("/listado")
	// Variable
	public String listadoEscritos(
			@RequestParam(name = "idItem", required = false) String idItem,
			@RequestParam(name = "idCausa", required = false) String idCausa,
			@RequestParam(name = "idEscrito", required = false) String idEscrito, 
			@RequestParam(required = false) String id1,
			@RequestParam(required = false) String q, Pageable pageable, ModelMap model) {
				String actorNombre = "Victor";
		if (!isStringNull(idCausa) && !isStringNull(idEscrito) && !isStringNull(idItem)) {
			LOG.info("METHOD: crearEscritoLiberium() PARAMS: idCausa=" + idCausa + " -- idItem=" + idItem + " -- idEscrito=" + idEscrito);
			model.addAttribute("idCausa", idCausa);
			model.addAttribute("idItem", idItem);
			model.addAttribute("idEscrito", idEscrito);
			model.addAttribute("escritoLiberium", "SI");
		} else {
			model.addAttribute("escritoLiberium", "NO");
			LOG.info("METHOD: crearEscritoNormal() PARAMS: q=" + q);
		}
		model.addAttribute("id1", id1);
		model.addAttribute("tipoCausas", tipoCausaService.buscarTodas(pageable, null));
		model.addAttribute("escritosLiberium", escritoService.buscarOriginales(pageable, q));
		model.addAttribute("escritosLocales", escritoService.buscarLocalesPage(pageable, getUsuario().getId(), q));
		model.addAttribute("escritosPropuestos", escritoService.buscarPropuestos());
		model.addAttribute("actorNombre", actorNombre);
		return ESCRITO_VISTA;
	}
}
