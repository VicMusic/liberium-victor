package com.quinto.liberium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Juzgado;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.service.CircunscripcionService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.JuzgadoService;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping("/administracion/juzgado")
public class JuzgadoController {

	@Autowired
	private JuzgadoService juzgadoService;

	@Autowired
	private InstanciaService instanciaService;
	
	@Autowired
	private CircunscripcionService circunscripcionService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@Autowired
	private PaisService paisService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_JUZGADO')")
	@GetMapping("/listado")
	public String listado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model, Authentication usuario) {
		Page<Juzgado> juzgados = juzgadoService.buscarTodos(paginable, q);
		Page<Instancia> instancias = instanciaService.buscarTodos(null, null);
		List<Circunscripcion> circunscripciones = circunscripcionService.buscarCircunscripcionTodos();
		Page<TipoCausa> tipoCausas = tipoCausaService.buscarTodas(null, null);
		Page<Pais> paises = paisService.buscarTodos(null, null);

		if(juzgados.getTotalPages() < 1 ) {
			juzgados = juzgadoService.buscarTodos(paginable, null);
			if(juzgados.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("circunscripciones", circunscripciones);
		model.put("tipoCausas", tipoCausas);
		model.put("instancias", instancias);
		model.put("modulo", "juzgados");
		model.put("page", juzgados);
		model.put("paises", paises);
		model.put("formulario", new Juzgado());

		// FIXME
//		ordenar(paginable, model);
		return "juzgado";
	}
	

}
