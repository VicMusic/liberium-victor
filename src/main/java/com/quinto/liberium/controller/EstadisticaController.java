package com.quinto.liberium.controller;

import java.util.Calendar;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("administracion/estadistica")
public class EstadisticaController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PreAuthorize("hasAnyAuthority('VISUALIZAR_ESTADISTICAS_PROPIAS', 'VISUALIZAR_ESTADISTICAS')")
	@GetMapping("/listado")
	public String verEstadistica(@RequestParam(required = false) String q, ModelMap model, Authentication usuario) {
		Usuario usuarioLog = usuarioService.buscarUsuarioPorEmail(usuario.getName());
		Locale locale = Locale.getDefault();
		Calendar fecha = Calendar.getInstance();
		
		
		model.put("modulo", "estadistica");
		model.put("usuario", usuarioLog);
		model.put("tipoUsuario", usuarioLog.getTipoUsuario().toString());
		model.put("fecha", fecha.get(Calendar.YEAR));
		
		return "estadistica";
	}
	
}
