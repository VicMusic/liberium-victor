package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("/administracion/abogado")
public class AbogadoController {

	@Autowired
	private ProvinciaService provinciaService; 
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired 
	private MateriaService materiaService;

	@Autowired
	private AbogadoService abogadoService;
	
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@GetMapping("/listado")
	public String verListadoAbogados(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		
		Page<Abogado> abogados = abogadoService.buscarTodos(paginable, q);
		Page<Provincia> provincias = provinciaService.buscarTodos(null, null);
		Page<Usuario> usuarios = usuarioService.buscarTodosUsuariosNoAbogados();
		Page<Materia> materias = materiaService.buscarTodos(null, null);
		
		model.put("page", abogados);
		model.put("provincias", provincias);
		model.put("usuarios", usuarios);
		model.put("materias", materias);
		model.put("formulario", new Abogado());
		
		// FIXME
//		ordenar(paginable, model);
		
		return "abogado";
	}
	
}
