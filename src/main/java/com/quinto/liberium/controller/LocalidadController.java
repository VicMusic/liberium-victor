package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.service.LocalidadService;
import com.quinto.liberium.service.ProvinciaService;

@Controller
@RequestMapping("/administracion/localidad")
public class LocalidadController {

	@Autowired
	private LocalidadService localidadService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_LOCALIDADES')")
	@GetMapping("/listado")
	public String localidadListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Localidad> localidades = localidadService.buscarTodos(paginable, q);
		Page<Provincia> provincias = provinciaService.buscarTodos(null, null);

		if(localidades.getTotalPages() < 1 ) {
			localidades = localidadService.buscarTodos(paginable, null);
			if(localidades.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("modulo", "localidades");
		model.put("page", localidades);
		model.put("provincias", provincias);
		model.put("formulario", new Localidad());

		return "localidad";
	}
}
