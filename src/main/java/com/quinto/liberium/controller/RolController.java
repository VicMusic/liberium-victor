package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.service.PermisoService;
import com.quinto.liberium.service.RolService;

@Controller
@RequestMapping("/administracion/roles")
public class RolController extends Controlador{
	
	@Autowired
	private RolService rolService;
	
	@Autowired
	private PermisoService permisoService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_ROL')")
	@GetMapping("/listado")
	public String verRoles(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Rol> roles = rolService.buscarTodos(paginable, q);
		if(roles.getTotalPages() < 1 ) {
			model.put("resultado", true);
			roles = rolService.buscarTodos(paginable, null);
		}
		model.put("page", roles);
		model.put("permisos", permisoService.buscarPorAdministrador(false));
		
		return "roles";
	}
	
}
