package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.service.PaisService;

@Controller
@RequestMapping("/administracion/pais")
public class PaisController extends Controlador {

	@Autowired
	private PaisService paisService;

	@PreAuthorize("hasAuthority('VISUALIZAR_PAISES')")
	@GetMapping("/listado")
	public String paisListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Pais> paises = paisService.buscarTodos(paginable, q);

		if(paises.getTotalPages() < 1 ) {
			paises = paisService.buscarTodos(paginable, null);
			if(paises.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("modulo", "paises");
		model.put("page", paises);
		model.put("formulario", new Pais());

		// FIXME
//		ordenar(paginable, model);

		return "pais";
	}

}
