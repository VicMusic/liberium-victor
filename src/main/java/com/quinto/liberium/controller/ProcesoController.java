package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Proceso;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.ProcesoService;

@Controller
@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
@RequestMapping("/administracion/proceso")
public class ProcesoController {

	@Autowired
	private ProcesoService procesoService;

	@Autowired
	private MateriaService materiaService;
	
	@GetMapping("/listado")
	public String dependenciaListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Proceso> procesos = procesoService.buscarTodos(paginable, q);
		Page<Materia> materias = materiaService.buscarTodos(null, null);

		model.put("materias", materias);
		model.put("modulo", "instancias");
		model.put("page", procesos);
		model.put("formulario", new Proceso());

		// FIXME
//		ordenar(paginable, model);

		return "proceso";
	}
	

}