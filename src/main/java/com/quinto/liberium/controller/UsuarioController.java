package com.quinto.liberium.controller;

import static com.quinto.liberium.util.Constantes.MENSAJE_ERROR;
import static com.quinto.liberium.util.Constantes.USUARIO_SESSION;
import static com.quinto.liberium.util.Constantes.VISTA_PERFIL_USUARIO;
import static com.quinto.liberium.util.Constantes.VISTA_USUARIO;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MUsuario;
import com.quinto.liberium.service.AbogadoService;
import com.quinto.liberium.service.CausaService;
import com.quinto.liberium.service.EstudioService;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.PermisoService;
import com.quinto.liberium.service.ProvinciaService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping("/administracion/usuario")
public class UsuarioController extends Controlador {

	private final Log LOG = LogFactory.getLog(UsuarioController.class);
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private AbogadoService abogadoService;

	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private MateriaService materiaService;
	
	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private PermisoService permisoService;
	
	@Autowired
	private CausaService causaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_USUARIOS')")
	@GetMapping("/listado")
	public String usuarioListado(Pageable paginable, @RequestParam(required = false) String q, Authentication usuario, ModelMap model) {
		Page<Usuario> usuarios;
		
		if(q != null && !q.equals("")){
			usuarios = usuarioService.buscarMiembrosLiberium(paginable, q);
		}else{
			usuarios = usuarioService.buscarMiembrosLiberium(paginable);
		}

		if(usuarios.getTotalPages() < 1 ) {
			model.put("resultado", true);
		}

		model.put("modulo", "usuario");
		model.put("page", usuarios);
		model.put("formulario", new Usuario());
		model.put("actualidad", new Date().getYear()+1900);
		model.put("tiposUsuario", TipoUsuario.values());
		model.put("permisos", permisoService.buscarMPermisoPorAdministrador(true));
		
		return VISTA_USUARIO;
	}

	@PreAuthorize("hasAnyAuthority('VISUALIZAR_PERFIL', 'VISUALIZAR_PERFIL_ADMINISTRADOR')")
	@GetMapping("/perfil")
	public String perfil(Authentication usuario, ModelMap model, HttpSession session) {

		MUsuario usuarioSession = usuarioService.buscarPorEmail(usuario.getName());
		if (usuarioSession.isAbogado()){
			MAbogado abogado = abogadoService.buscarMAbogadoPorMailUsuario(usuarioSession.getEmail());
			model.addAttribute("abogado", abogado);

			List<Estudio> estudio = estudioService.buscarPorPertenencia(abogado.getId());
			if(!estudio.isEmpty()) {
				model.addAttribute("estudio", estudio);
			}else {
				//estudio = estudioService.listarEstudio();
				model.addAttribute("estudio", estudio);
			}
			
		}
		
		List<MMateria> materias = materiaService.buscarMateriaModelTodos();
		model.addAttribute("materias", materias);
		
		List<MProvincia> provincias = provinciaService.buscarProvinciaModelTodos();
		model.addAttribute("provincias", provincias);
		
		model.addAttribute(USUARIO_SESSION, usuarioSession);

		return VISTA_PERFIL_USUARIO;
	}

	@PreAuthorize("hasAnyAuthority('VISUALIZAR_PERFIL', 'VISUALIZAR_PERFIL_ADMINISTRADOR')")
	@PostMapping("/actualizar-bio")
	public String perfil(Authentication usuario, ModelMap model, @RequestParam String nombre, MultipartFile foto, @RequestParam(required = false) boolean eliminarFoto) {

		LOG.info("METHOD: actualizarPerfil() VARIABLES: nombre: " + nombre + " foto: }" + foto.toString());
		try {
			if(!foto.isEmpty()) {
				usuarioService.actualizarBio(usuario.getName(), nombre, foto.getBytes(), eliminarFoto);
			} else {
				usuarioService.actualizarBio(usuario.getName(), nombre, new byte[0], eliminarFoto);
			}
			
		} catch(Exception e) {
			model.addAttribute(MENSAJE_ERROR, e.getMessage());
			e.printStackTrace();
		}
		
		MUsuario usuarioSession = usuarioService.buscarPorEmail(usuario.getName());
		model.addAttribute(USUARIO_SESSION, usuarioSession);

		return "redirect:/administracion/usuario/perfil";
	}


	@PreAuthorize("hasAnyAuthority('VISUALIZAR_PERFIL', 'VISUALIZAR_PERFIL_ADMINISTRADOR')")
	@GetMapping("/eliminar-foto")
	public String eliminarFoto(Authentication usuario) {
		usuarioService.eliminarFoto(usuario.getName());
		return "redirect:/administracion/usuario/perfil";
	}
	
	@GetMapping("/foto/{id}")
	public ResponseEntity<byte[]> imagen(@PathVariable String id){
		Usuario usuario = usuarioService.buscarPorId(id);
		
		HttpHeaders cabecera = new HttpHeaders();
		if(usuario.getFoto() != null) {
			cabecera.setContentLength(usuario.getFoto().length);
		}
		
		cabecera.setContentType(MediaType.IMAGE_JPEG);
		return new ResponseEntity<>(usuario.getFoto(), cabecera, HttpStatus.OK);
	}

}
