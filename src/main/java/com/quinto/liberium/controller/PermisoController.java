package com.quinto.liberium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MPermiso;
import com.quinto.liberium.service.PermisoService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
@RequestMapping("/administracion/permiso")
public class PermisoController {

    @Autowired
    private PermisoService permisoService;

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/formulario")
    public String verPermiso(ModelMap model) {
	return "formulario-permiso";
    }
    
    @GetMapping("/listado")
    public List<Permiso> listadoPermiso() {
        return permisoService.listarPermiso();
    }
    
    @PostMapping("/guardar")
    public String crearPermiso(@ModelAttribute MPermiso permiso) {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	Usuario creador = usuarioService.buscarUsuarioPorEmail(auth.getName());
	try {
	    permisoService.crear(permiso, creador);
	    return "EXITO - Permiso creado exitosamente.";
	} catch (Exception e) {
	    throw new PeticionIncorrectaException("ERROR - Ocurrió un error inesperado mientras se creaba el Permiso.");
	}
    }
    
    @PostMapping("/editar")
    public String editarPermiso(@ModelAttribute MPermiso permiso) {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	Usuario editor = usuarioService.buscarUsuarioPorEmail(auth.getName());
	try {
	    permisoService.editar(permiso, editor);
	    return "EXITO - Permiso editado correctamente.";
	} catch (Exception e) {
	    throw new PeticionIncorrectaException("ERROR - Ocurrió un error inesperado mientras se actualizaba el Permiso.");
	}
    }
    
    @PostMapping("/eliminar/{permisoId}")
    public String eliminarPermiso(@PathVariable String permisoId) {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	Usuario eliminador = usuarioService.buscarUsuarioPorEmail(auth.getName());
	try {
	    permisoService.eliminar(permisoId, eliminador);
	    return "EXITO - Permiso eliminado correctamente.";
	} catch (Exception e) {
	    throw new PeticionIncorrectaException("ERROR - Ocurrió un error inesperado mientras se eliminaba el Permiso.");
	}
    }
    
}