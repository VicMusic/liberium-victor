package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Interes;
import com.quinto.liberium.service.InteresService;

@Controller
@RequestMapping("/administracion/interes")
public class InteresController {

	@Autowired
	private InteresService interesService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_INTERES')")
	@GetMapping("/listado")
	public String interesListado(Pageable paginable, @RequestParam(required = false) String q, ModelMap model) {
		Page<Interes> intereses = interesService.buscarTodos(paginable, q);

		if(intereses.getTotalPages() < 1 ) {
			intereses = interesService.buscarTodos(paginable, null);
			if(intereses.getTotalPages() > 0)
				model.put("resultado", true);
		}
		model.put("page", intereses);

		return "interes";
	}
	
}
