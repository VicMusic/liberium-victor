package com.quinto.liberium.controller;

import static com.quinto.liberium.util.Constantes.MENSAJE_ERROR;
import static com.quinto.liberium.util.Constantes.MENSAJE_EXITO;
import static com.quinto.liberium.util.Constantes.REDIRECCION_CAMBIAR_CLAVE;
import static com.quinto.liberium.util.Constantes.REDIRECCION_CONFIRMAR_MAIL;
import static com.quinto.liberium.util.Constantes.REDIRECCION_LISTA_USUARIO;
import static com.quinto.liberium.util.Constantes.REDIRECCION_LOGIN;
import static com.quinto.liberium.util.Constantes.REDIRECCION_PERFIL_USUARIO;
import static com.quinto.liberium.util.Constantes.TOKEN;
import static com.quinto.liberium.util.Constantes.VISTA_CAMBIAR_CLAVE;
import static com.quinto.liberium.util.Constantes.VISTA_CUENTA_ERROR;
import static com.quinto.liberium.util.Constantes.VISTA_CUENTA_VERIFICADA;
import static com.quinto.liberium.util.Constantes.VISTA_LOGIN;
import static com.quinto.liberium.util.Constantes.VISTA_OLVIDE_CLAVE;
import static com.quinto.liberium.util.Constantes.VISTA_REGISTRO;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.service.PaisService;
import com.quinto.liberium.service.TurnoService;
import com.quinto.liberium.service.UsuarioService;

@Controller
@RequestMapping()
public class LoginController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private TurnoService turnoService;

	@Autowired
	private PaisService paisService;

	@Autowired(required = true)
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	private Log log = LogFactory.getLog(LoginController.class);

	@GetMapping("/login")
	public String login(Model model, @RequestParam(required = false) String error,
			@RequestParam(required = false) String info, @RequestParam(required = false) String logout) {

		log.info("METODO: login -- PARAMETROS: error= " + error + " logout=  " + logout);
		log.info("METODO: login -- PARAMETROS: info= " + info + " logout= " + logout);

		if (error != null) {
			model.addAttribute(MENSAJE_ERROR, "Error al iniciar sesión, revise el email o la contraseña ingresada");
		}

		if (logout != null) {
			model.addAttribute(MENSAJE_EXITO, "Se cerro la sesión correctamente");
		}

		model.addAttribute(MENSAJE_EXITO, info);

		return VISTA_LOGIN;
	}

	@GetMapping("/registro")
	public String registro(@RequestParam(required = false) String q, Model model) {

		Page<Pais> paises = paisService.buscarTodos(null, q);

		model.addAttribute("paises", paises);

		return VISTA_REGISTRO;
	}

	@GetMapping({ "/loginsuccess" })
	public String logincheck(HttpSession session) {
		log.info("METODO: logincheck ");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
			return REDIRECCION_LOGIN;
		} else {
			Usuario usuario = usuarioService.buscarUsuarioPorEmail(auth.getName());
			if (usuario == null) {
				return REDIRECCION_LOGIN;
			} else if (usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
				return REDIRECCION_LISTA_USUARIO;
			} else if (usuario.isEnabled()) {
				return REDIRECCION_PERFIL_USUARIO;
			} else {
				return REDIRECCION_CONFIRMAR_MAIL;
			}
		}
	}

	@GetMapping("/olvide-mi-clave/{email}")
	public String mostrarOlvideMiClaveView(@PathVariable(name = "email", required = false) String email,@RequestParam(name = "mail", required = false) String mail, Model model) {
		System.out.println("Mail: " + email);

		if (email != null && !email.isEmpty()) {
			if (!email.equals("mail@ejemplo.com.ar") && !email.equals("as")) {
				boolean reseteada = usuarioService.resetearClave(email);
				if (reseteada == false) {
					model.addAttribute(MENSAJE_ERROR, "No se pudo encontrar un usuario con esa dirección de e-mail.");
				} else {
					model.addAttribute(MENSAJE_EXITO,
							"Se ha enviado las instrucciones para restablecer su contraseña a: " + email + ".");
				}
			}else if(mail != null && !mail.isEmpty() ){
				boolean reseteada = usuarioService.resetearClave(mail);
				if (reseteada == false) {
					model.addAttribute(MENSAJE_ERROR, "No se pudo encontrar un usuario con esa dirección de e-mail.");
				} else {
					model.addAttribute(MENSAJE_EXITO,
							"Se ha enviado las instrucciones para restablecer su contraseña a: " + mail + ".");
				}
			}else {
				model.addAttribute(MENSAJE_ERROR, "Ingrese mail.");
			}
		}else {
			model.addAttribute(MENSAJE_ERROR, "Ingrese mail.");
		}
		return VISTA_OLVIDE_CLAVE;
	}

	@GetMapping("/cambiar-clave")
	public String cambiarClave(Model model, @RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "success", required = false) String success) {

		Usuario usuario = usuarioService.buscarPorResetToken(token);

		if (usuario != null) {
			model.addAttribute(TOKEN, token);
		}

		if (success != null) {
			model.addAttribute(MENSAJE_EXITO, "La contraseña ha sido cambiada correctamente.");
		}

		return VISTA_CAMBIAR_CLAVE;
	}

	@PostMapping("/cambiar-clave")
	public String cambiarClave(Model model, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {

		Usuario usuario = usuarioService.buscarPorResetToken(requestParams.get("token"));
		if (usuario != null) {

			Usuario resetUsuario = usuario;
			resetUsuario.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
			resetUsuario.setResetToken(null);

			usuarioService.guardar(resetUsuario, resetUsuario, "CambioClave");

			model.addAttribute(MENSAJE_EXITO, "La contraseña ha sido cambiada correctamente.");

			return REDIRECCION_CAMBIAR_CLAVE;
		} else {
			model.addAttribute(MENSAJE_ERROR,
					"La solicitud de cambio de contraseña ha expirado, por favor intente nuevamente.");
			return VISTA_CAMBIAR_CLAVE;
		}

	}

	@GetMapping("/confirmar-mail")
	public String mostrarConfirmarMailView() {
		return "confirmar-mail";
	}

	@GetMapping("/mail-confirmado")
	public ModelAndView confirmarCuenta(ModelAndView modelAndView, @RequestParam("token") String confirmToken) {

		Usuario usuario = usuarioService.buscarPorConfirmToken(confirmToken);

		if (usuario != null) {

			usuario.setEnabled(true);
			usuario.setConfirmToken(null);
			usuarioService.guardar(usuario, null, "actualizar");
			modelAndView.setViewName(VISTA_CUENTA_VERIFICADA);
		} else {
			modelAndView.addObject(MENSAJE_EXITO, "El link ya no es válido o está roto.");
			modelAndView.setViewName(VISTA_CUENTA_ERROR);
		}

		return modelAndView;
	}

	@GetMapping("/cancelar-turno-abogado")
	public ModelAndView cancelarTurnoAbogado(ModelAndView modelAndView,
			@RequestParam(name = "tokenCancelado", required = false) String tokenCancelado) {
		modelAndView.setViewName("cancelar-turno-abogado");

		Turno turno = turnoService.buscarPorTokenCancelado(tokenCancelado);

		if (turno == null || tokenCancelado == null) {
			modelAndView.addObject("error", "El link ya no es válido o esta roto");
		} else {
			modelAndView.addObject("exito", "El turno ha sido cancelado correctamente,"
					+ " ahora se le enviará un mail al cliente que ha solicitado el turno para notificarlo de que el turno se ha cancelado.");
			String string = "abogado";
			turnoService.cancelarTurno(turno, string);
		}

		return modelAndView;
	}

	@GetMapping("/cancelar-turno-cliente")
	public ModelAndView cancelarTurnoCliente(ModelAndView modelAndView,
			@RequestParam(name = "tokenCancelado", required = false) String tokenCancelado) {
		modelAndView.setViewName("cancelar-turno-cliente");

		Turno turno = turnoService.buscarPorTokenCancelado(tokenCancelado);

		if (turno == null || tokenCancelado == null) {
			modelAndView.addObject("error", "El link ya no es válido o esta roto");
		} else {
			modelAndView.addObject("exito", "El turno ha sido cancelado correctamente,"
					+ " se le enviará un mail a el abogado para notificarlo de que el turno ha sido cancelado.");

			String string = null;
			turnoService.cancelarTurno(turno, string);
		}

		return modelAndView;
	}
}
