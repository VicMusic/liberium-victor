package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.service.MateriaService;
import com.quinto.liberium.service.TipoCausaService;

@Controller
@RequestMapping("/administracion/tipocausa")
public class TipoCausaController {

    @Autowired
    private TipoCausaService tipoCausaService;
    
    @Autowired MateriaService materiaService;

    @PreAuthorize("hasAuthority('VISUALIZAR_TIPO_DE_CAUSAS')")
	@GetMapping("/listado")
	public String dependenciaListado(Pageable paginable, @RequestParam(required = false) String q,@RequestParam(required = false) String id1,ModelMap model) {
		Page<TipoCausa> page = tipoCausaService.buscarTodas(paginable, q);
		Page<Materia> materias = materiaService.buscarTodos(null, null);
		
		if(page.getTotalPages() < 1 ) {
			page = tipoCausaService.buscarTodas(paginable, null);
			if(page.getTotalPages() > 0)
				model.put("resultado", true);
		}
		
		model.put("id1", id1);
		model.put("page", page);
		model.put("materias", materias);
		model.put("formulario", new TipoCausa());
		model.put("modulo", "tipocausa");

		// FIXME
//		ordenar(paginable, model);

		return "tipocausa";
	}
    
}