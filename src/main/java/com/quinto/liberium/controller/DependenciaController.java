package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.Dependencia;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.service.DependenciaService;
import com.quinto.liberium.service.InstanciaService;
import com.quinto.liberium.service.TipoDependenciaService;

@Controller
@RequestMapping("/administracion/dependencia")
public class DependenciaController {

	@Autowired
	private DependenciaService dependenciaService;
	
	@Autowired
	private TipoDependenciaService tipoDependenciaService;

	@Autowired
	private InstanciaService instanciaService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DEPENDENCIA')")
	@GetMapping("/listado")
    public String listado(Pageable paginable,@RequestParam(required = false) String q, ModelMap model) {
		Page<Dependencia> dependencia = dependenciaService.buscarTodos(paginable, q);
		Page<TipoDependencia> tipoDependencia = tipoDependenciaService.buscarTodos(null, "");
		Page<Instancia> instancia = instanciaService.buscarTodos(null, "");
		
		model.addAttribute("page", dependencia);
		model.addAttribute("tipos", tipoDependencia);
		model.addAttribute("instancia", instancia);
		model.addAttribute("modulo", "dependencias");
		
		model.addAttribute("dependencia", new Dependencia());
		
		return "dependencia";
    }
	
}
