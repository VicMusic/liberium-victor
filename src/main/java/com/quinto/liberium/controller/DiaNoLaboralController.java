package com.quinto.liberium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.quinto.liberium.entity.DiaNoLaboral;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.service.DiaNoLaboralService;
import com.quinto.liberium.service.PaisService;

@Controller
@RequestMapping("/administracion/dianolaborable")
public class DiaNoLaboralController extends Controlador {
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private DiaNoLaboralService diaNoLaboralService;
	
	@PreAuthorize("hasAuthority('VISUALIZAR_DIAS_NO_LABORABLES')")
	@GetMapping("/listado")
	public String listado(@RequestParam(required = false) String q, ModelMap model, Pageable paginable) {
		
		Page<Pais> paises = paisService.buscarTodos(null, null);

		Page<DiaNoLaboral> diasNoLaborables = diaNoLaboralService.buscarTodos(paginable, q);
		
		if(diasNoLaborables.getTotalPages() < 1 ) {
			diasNoLaborables = diaNoLaboralService.buscarTodos(paginable, null);
			if(diasNoLaborables.getTotalPages() > 0)
				model.put("resultado", true);
		}
	
		model.put("paises", paises);
		model.put("diasNoLaborales", diasNoLaborables);
		
		return "diaNoLaboral";
	}

}
