package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.CausaVinculada;

@Repository("causaVinculadaRepository")
public interface CausaVinculadaRepository extends JpaRepository<CausaVinculada, String> {

    @Query("SELECT c FROM CausaVinculada c WHERE c.id = :id")
    public CausaVinculada buscarPorId(@Param("id") String causaId);
    
    @Query("SELECT c FROM CausaVinculada c WHERE c.causa.id = :idCausa")
    public List<CausaVinculada> buscarPorCausa(@Param("idCausa") String idCausa);
    
    @Query("SELECT c FROM CausaVinculada c WHERE c.vinculada.id = :idCausaVinculada")
    public List<CausaVinculada> buscarPorCausaVinculada(@Param("idCausaVinculada") String idCausaVinculada);
    
}