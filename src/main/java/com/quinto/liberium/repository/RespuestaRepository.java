package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Respuesta;

@Repository("respuestaRepository")
public interface RespuestaRepository extends JpaRepository<Respuesta, String> {

		@Query("SELECT p FROM Respuesta p WHERE p.id = :id AND p.eliminado IS NULL")
	    public Respuesta buscarPorId(@Param("id") String idProvincia);
		
	    @Query("SELECT p FROM Respuesta p WHERE p.consulta.id = :idConsulta AND p.eliminado IS NULL ORDER BY p.orden")
	    public List<Respuesta> buscarPorIdConsulta(@Param("idConsulta") String idConsulta);
	
}
