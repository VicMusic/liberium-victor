package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, String>{

	@Query("SELECT i FROM Item i WHERE i.id = :id AND i.eliminado IS NULL")
	public Item buscarPorId(@Param("id") String id);
	
	@Query("SELECT i FROM Item i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
	public Item buscarPorNombre(@Param("nombre") String nombre);
	
	@Query("SELECT MAX(i.orden) FROM Item i WHERE i.checkList.id = :id AND i.eliminado IS NULL")
	public Integer buscarMaximoOrden(@Param("id") String id);
	
    @Query("SELECT i FROM Item i WHERE i.checkList.id = :id AND i.orden = :orden AND i.eliminado IS NULL ORDER BY i.orden")
    public List<Item> buscarMismoOrden(@Param("id") String id, @Param("orden") int orden);
    
    @Query("SELECT i FROM Item i WHERE i.checkList.id = :id AND i.orden >= :orden AND i.eliminado IS NULL ORDER BY i.orden")
    public List<Item> buscarMayores(@Param("id") String id, @Param("orden") int orden);
    
	@Query("SELECT i FROM Item i WHERE i.checkList.id = :id AND i.eliminado IS NULL ORDER BY i.orden")
	public List<Item> buscarPorChecklistId(@Param("id") String id);
	
	@Query("SELECT i FROM Item i WHERE i.eliminado IS NULL AND i.checkList != NULL ORDER BY i.orden")
	public List<Item> buscarTodos(Pageable paginable);
	
	

}
