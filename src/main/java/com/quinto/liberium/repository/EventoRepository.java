package com.quinto.liberium.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;

@Repository("eventoRepository")
public interface EventoRepository extends JpaRepository<Evento, String> {
	
	@Query("SELECT e FROM Evento e WHERE e.creador = :idUsuario AND e.activo = true AND e.eliminado IS NULL")
	public List<Evento> buscarPorUsuario(@Param("idUsuario")Usuario idUsuario);
	
    @Query("SELECT c FROM Evento c WHERE c.id = :id AND c.eliminado is null")
    public Evento buscarPorId(@Param("id") String eventoId);
    
    @Query("SELECT c FROM Evento c WHERE c.titulo = :titulo AND c.eliminado is null")
    public Evento buscarPorTitulo(@Param("titulo") String titulo);
    
    @Query("SELECT c FROM Evento c WHERE c.creador = :usuario AND c.fechaInicio BETWEEN :desde AND :hasta AND c.fechaFin BETWEEN :desde AND :hasta AND c.eliminado IS NULL")
    public List<Evento> buscarPorFechas(@Param("desde") Date desde,@Param("hasta") Date hasta, @Param("usuario") Usuario usuario);
    
    @Query("SELECT e FROM Evento e WHERE e.creador = :usuario AND e.titulo = :titulo AND e.color = :color AND e.fechaInicio = :start AND e.fechaFin = :end AND e.eliminado is null")
    public Evento buscarEventoPorColorFechasYUsuario(@Param("usuario") Usuario usuario, @Param("titulo")String titulo, @Param("color") String color, @Param("start") Date start, @Param("end") Date end);
    
    @Query("SELECT e FROM Evento e WHERE e.creador = :usuario AND e.tipoEvento = :tipoEvento AND e.activo = true AND e.eliminado IS NULL")
    public List<Evento> buscarEventosPorTipoDeEvento(@Param("usuario")Usuario usuario, @Param("tipoEvento")String tipoEvento);
    
    @Query("SELECT e.tipoEvento FROM Evento e WHERE e.tipoEvento != 'REUNION' AND e.tipoEvento != 'DIA_INHABIL' AND e.tipoEvento != 'AUDIENCIA' AND e.tipoEvento != 'TURNO' AND e.tipoEvento != 'AUDIENCIA' AND e.tipoEvento != 'VENCIMIENTO_PROCESAL' AND e.tipoEvento != 'VENCIMIENTO_COBRO' AND e.tipoEvento != 'PLAZO' AND e.tipoEvento != 'FERIA_JUDICIAL' AND e.tipoEvento != 'FERIADO' AND e.creador = :usuario")
    public HashSet<String> buscarOtrosTipoDeEventos(@Param("usuario")Usuario usuairo);
    
    @Query("SELECT e FROM Evento e WHERE e.tipoEvento = 'FERIADO' OR e.tipoEvento = 'FERIA_JUDICIAL' OR e.tipoEvento = 'DIA_INHABIL' AND e.eliminado IS NULL AND e.pais = :pais")
    public List<Evento> buscarEventosNoLaboralesPorPais(@Param("pais") Pais pais);
    
    @Query("SELECT e FROM Evento e WHERE e.tipoEvento = 'FERIADO' OR e.tipoEvento = 'FERIA_JUDICIAL' OR e.tipoEvento = 'DIA_INHABIL' AND e.eliminado IS NULL AND e.provincia = :provincia")
    public List<Evento> buscarEventosNoLaboralesPorProvincia(@Param("provincia") Provincia provincia);
    
    @Query("SELECT e FROM Evento e WHERE e.tipoEvento = 'FERIADO' OR e.tipoEvento = 'FERIA_JUDICIAL' OR e.tipoEvento = 'DIA_INHABIL' AND e.eliminado IS NULL AND e.creador = :usuario")
    public List<Evento> buscarEventosNoLaborablesPorUsuario(@Param("usuario") Usuario usuario);
    
    @Query("SELECT e FROM Evento e WHERE e.eliminado IS NULL AND e.creador = :usuario AND e.isDiaNoLaboral = true")
    public List<Evento> buscarEventosFederalesProvinciales(@Param("usuario") Usuario usuario);
    
    @Query("SELECT e FROM Evento e WHERE e.eliminado IS NULL AND e.creador = :usuario AND e.isDiaNoLaboral = true AND e.tipoDiaNoLaborable = 'FEDERAL'")
    public List<Evento> buscarEventosFederales(@Param("usuario") Usuario usuario);
    
    
}