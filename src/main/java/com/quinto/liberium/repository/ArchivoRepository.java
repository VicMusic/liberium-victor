package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Archivo;

@Repository("archivoRepository")
public interface ArchivoRepository extends JpaRepository<Archivo, String>{
	@Query("SELECT a FROM Archivo a WHERE a.id = :id AND a.eliminado IS NULL")
	public Archivo buscarPorId(@Param("id")String id);
	
	@Query("SELECT a FROM Archivo a WHERE a.eliminado IS NULL  ORDER BY a.creado")
    public List<Archivo> buscarTodos();
	

    @Query("SELECT a FROM Archivo a WHERE a.eliminado IS NULL ORDER BY a.creado")
    public Page<Archivo> buscarTodas(Pageable paginable);
    
	@Query("SELECT a FROM Archivo a WHERE a.creador.id = :idUsuario AND a.eliminado IS NULL  ORDER BY a.creado")
    public List<Archivo> buscarTodosIdUsuario(@Param("idUsuario")String idUsuario);
	

    @Query("SELECT a FROM Archivo a WHERE a.creador.id = :idUsuario AND a.eliminado IS NULL ORDER BY a.creado")
    public Page<Archivo> buscarTodasIdUsuario(Pageable paginable, @Param("idUsuario")String idUsuario);
}
