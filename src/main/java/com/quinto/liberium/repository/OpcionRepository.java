package com.quinto.liberium.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Opcion;

@Repository("opcionRepository")
public interface OpcionRepository extends JpaRepository<Opcion, String> {


	@Query("SELECT o FROM Opcion o WHERE o.id = :id")
    public Opcion buscarPorId(@Param("id") String idOpcion);
}