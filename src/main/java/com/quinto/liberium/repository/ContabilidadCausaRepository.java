package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.ContabilidadCausa;

@Repository("contabilidadCausaRepository")
public interface ContabilidadCausaRepository extends JpaRepository<ContabilidadCausa, String> {
	
	@Query("SELECT c FROM ContabilidadCausa c WHERE c.eliminado IS NULL AND c.causa.id = :idCausa AND c.honorario IS NULL ORDER BY c.creado")
	public List<ContabilidadCausa> buscarContabilidadPorCausa(@Param("idCausa") String idCausa);
	
	@Query("SELECT c FROM ContabilidadCausa c WHERE c.eliminado IS NULL AND c.id = :idContabilidad")
	public ContabilidadCausa buscarContabilidadCausaPorId(@Param("idContabilidad") String id);
	
	@Query("SELECT c FROM ContabilidadCausa c WHERE c.eliminado IS NULL AND c.honorario.id = :idHonorario ORDER BY c.creado")
	public List<ContabilidadCausa> buscarContabilidadesPorHonorario(@Param("idHonorario")String idHonorario);

}
