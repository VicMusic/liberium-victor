package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Instancia;

@Repository("instanciaRepository")
public interface InstanciaRepository extends JpaRepository<Instancia, String> {

    @Query("SELECT i FROM Instancia i WHERE i.id = :id AND i.eliminado IS NULL")
    public Instancia buscarPorId(@Param("id") String instanciaId);
    
    @Query("SELECT i FROM Instancia i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
    public Instancia buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT i FROM Instancia i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
    public List<Instancia> duplicado(@Param("nombre") String nombre);

    @Query("SELECT i FROM Instancia i WHERE i.eliminado IS NULL")
    public List<Instancia> buscarTodos();
    
    @Query("SELECT i FROM Instancia i WHERE i.eliminado IS NULL")
    public Page<Instancia> buscarTodas(Pageable paginable);

    @Query("SELECT i FROM Instancia i WHERE i.eliminado IS NULL AND i.nombre LIKE :q")
    public Page<Instancia> buscarTodas(Pageable paginable, @Param("q") String q);
}