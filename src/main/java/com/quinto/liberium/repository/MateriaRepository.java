package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Materia;

@Repository("materiaRepository")
public interface MateriaRepository extends JpaRepository<Materia, String> {

    @Query("SELECT m FROM Materia m WHERE m.id = :id AND m.eliminado IS NULL")
    public Materia buscarPorId(@Param("id") String idMateria);
    
    @Query("SELECT m FROM Materia m WHERE m.nombre = :nombre AND m.eliminado IS NULL")
    public Materia buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT m FROM Materia m WHERE m.nombre = :nombre AND m.eliminado IS NULL")
    public Materia buscarMateriasPorNombre(@Param("nombre") String nombre);

    @Query("SELECT m FROM Materia m WHERE m.eliminado IS NULL")
    public List<Materia> buscarTodos();
    
    @Query("SELECT m FROM Materia m WHERE m.eliminado IS NULL")
    public Page<Materia> buscarTodas(Pageable paginable);
    
    @Query("SELECT m FROM Materia m WHERE m.eliminado IS NULL AND m.nombre LIKE :q")
    public Page<Materia> buscarTodas(Pageable paginable, @Param("q") String q);

}