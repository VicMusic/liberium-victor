package com.quinto.liberium.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Turno;

@Repository("turnoRepository")
public interface TurnoRepository extends JpaRepository<Turno, String>{

	@Query("SELECT t FROM Turno t WHERE t.id = :id AND t.eliminado IS NULL")
	public Turno buscarPorId(@Param ("id") String id);
	
	@Query("SELECT t FROM Turno t WHERE t.abogado.id = : id AND t.eliminado IS NULL")
	public Turno buscarPorAbogadoId(@Param ("id") String id);
	
	@Query("SELECT t FROM Turno t WHERE t.abogado.usuario.nombre = : nombre AND t.eliminado IS NULL")
	public Turno buscarPorAbogadoNombre(@Param ("nombre") String nombre);
	
	@Query("SELECT t FROM Turno t WHERE t.abogado.usuario.email = :mailAbogado AND t.mailCliente = :mailCliente AND t.fechaOriginal = :fechaOriginal AND t.cancelado = false order by t.creado desc")
	public List<Turno> buscarTurno(@Param ("mailAbogado") String mailAbogado, @Param ("mailCliente") String mailCliente, @Param ("fechaOriginal") String fechaOriginal);
	
	@Query("SELECT t FROM Turno t WHERE t.mailCliente = :mailCliente AND t.fechaOriginal = :fechaOriginal AND t.cancelado = false order by t.creado desc")
	public List<Turno> buscarTurnoPorClienteYFecha( @Param ("mailCliente") String mailCliente, @Param ("fechaOriginal") String fechaOriginal);
	
	@Query("SELECT t FROM Turno t WHERE t.evento = :evento AND t.eliminado IS NULL")
	public Turno buscarTurnoPorEventoId(@Param("evento")Evento evento);
	
	@Query("SELECT t FROM Turno t WHERE t.tokenCancelado = :tokenCancelado")
	public Turno buscarPorTokenCancelado(@Param("tokenCancelado") String tokenCancelado);
	
	@Query("SELECT t FROM Turno t WHERE t.eliminado IS NULL")
	public List<Turno> buscarTodos();
	
	@Query("SELECT t FROM Turno t WHERE t.eliminado IS NULL AND t.cancelado = false AND t.fechaTurno <= :fechaActual")
	public List<Turno> buscarTodosAnteriores(@Param("fechaActual") Date fechaActual);
	
	@Query("SELECT t FROM Turno t WHERE t.eliminado IS NULL AND t.cancelado = false AND t.fechaTurno >= :fechaActual")
	public List<Turno> buscarTodosFuturos(@Param("fechaActual") Date fechaActual);
	
//	@Query("SELECT t FROM Turno t WHERE t.juzgado.id = : id AND t.eliminado IS NULL")
//	public Turno buscarPorJuzgadoId(@Param ("id") String id);
//	
//	@Query("SELECT t FROM Turno t WHERE t.juzgado.nombre = : nombre AND t.eliminado IS NULL")
//	public Turno buscarPorJuzgadoNombre(@Param ("nombre") String nombre);
//	
//	@Query("SELECT t FROM Turno t WHERE t.cliente.id = : id AND t.eliminado IS NULL")
//	public Turno buscarPorUsuarioId(@Param ("id") String id);
//	
//	@Query("SELECT t FROM Turno t WHERE t.cliente.nombre = : nombre AND t.eliminado IS NULL")
//	public Turno buscarPorUsuarioNombre(@Param ("nombre") String nombre);
//
//	@Query("SELECT t FROM Turno t WHERE t.tokenAbogado = :tokenAbogado")
//	public Turno buscarPorTokenAbogado(@Param ("tokenAbogado") String tokenAbogado);
//	
//	@Query("SELECT t FROM Turno t WHERE t.tokenCliente = :tokenCliente")
//	public Turno buscarPorTokenCliente(@Param ("tokenCliente") String tokenCliente);

}
