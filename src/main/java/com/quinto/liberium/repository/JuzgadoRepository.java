package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Juzgado;

@Repository("juzgadoRepository")
public interface JuzgadoRepository extends JpaRepository<Juzgado, String> {

   @Query("SELECT i FROM Juzgado i WHERE i.id = :id AND i.eliminado IS NULL")
    public Juzgado buscarPorId(@Param("id") String juzgadoId);
    
    @Query("SELECT i FROM Juzgado i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
    public Juzgado buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT i FROM Juzgado i WHERE i.instancia.id = :idInstancia AND i.eliminado IS NULL")
    public List<Juzgado> buscarPorIdInstancia(@Param("idInstancia") String idInstancia);

    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL")
    public List<Juzgado> buscarTodos();
    
    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL")
    public Page<Juzgado> buscarTodas(Pageable paginable);

    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL AND (i.nombre LIKE :q) OR (i.instancia.nombre LIKE :q) OR (i.tipoFuero LIKE :q) OR (i.provincia.nombre LIKE :q)")
    public Page<Juzgado> buscarTodas(Pageable paginable, @Param("q") String q);
    
    @Query("SELECT i FROM Juzgado i WHERE i.instancia.id = :idInstancia AND i.provincia.id = :idProvincia AND i.eliminado IS NULL")
    public List<Juzgado> buscarPorIdInstanciaYIdTipoCausaYIdProvincia(@Param("idInstancia") String idInstancia, @Param("idProvincia") String idProvincia);
    
    @Query("SELECT i FROM Juzgado i WHERE i.provincia.id = :idProvincia AND i.eliminado IS NULL")
    public List<Juzgado> buscarPorIdProvincia(@Param("idProvincia") String idProvincia);
    
    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL AND (i.nombre LIKE :q OR i.domicilio LIKE :q OR i.telefono LIKE :q OR i.instancia.nombre LIKE :q OR i.tipoFuero LIKE :q)")
    public List<Juzgado> buscarTodas(@Param("q") String q);
    
    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL AND (i.provincia.nombre LIKE :q)")
    public List<Juzgado> buscarTodasPorProvincia(@Param("q") String q);
    
    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL AND (i.pais.nombre LIKE :q)")
    public List<Juzgado> buscarTodasPorPais(@Param("q") String q);
    
    @Query("SELECT i FROM Juzgado i WHERE i.eliminado IS NULL AND (i.circunscripcion.nombre LIKE :q)")
    public List<Juzgado> buscarTodasPorCir(@Param("q") String q);
}

