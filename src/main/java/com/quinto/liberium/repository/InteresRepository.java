package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Interes;

@Repository("interesRepository")
public interface InteresRepository extends JpaRepository<Interes ,String>{
	
	@Query("SELECT i FROM Interes i WHERE i.id = :idInteres AND i.eliminado IS NULL")
	public Interes buscarInteresPorId(@Param("idInteres")String idInteres);
	
	@Query("SELECT i FROM Interes i WHERE i.nombre = :nombre AND i.eliminado IS NULL")
	public Interes buscarInteresPorNombre(@Param("nombre")String nombre);
	
    @Query("SELECT i FROM Interes i WHERE i.eliminado IS NULL")
    public List<Interes> buscarTodos();
    
    @Query("SELECT i FROM Interes i WHERE i.eliminado IS NULL AND (i.nombre LIKE :q) OR (i.porcentaje LIKE :q) ORDER BY i.nombre")
	public Page<Interes> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT i FROM Interes i WHERE i.eliminado IS NULL ORDER BY i.nombre")
	public Page<Interes> buscarTodos(Pageable pageable);

}
