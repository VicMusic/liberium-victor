package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Dependencia;

@Repository("dependenciaRepository")
public interface DependenciaRepository extends JpaRepository<Dependencia, String> {

    @Query("SELECT d FROM Dependencia d WHERE d.id = :id AND d.eliminado IS NULL ORDER BY d.nombre")
    public Dependencia buscarPorId(@Param("id") String idDependencia);
    
    @Query("SELECT d FROM Dependencia d WHERE d.nombre = :nombre AND d.eliminado IS NULL ORDER BY d.nombre")
    public Dependencia buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT d FROM Dependencia d WHERE d.tipoDependencia.id = :idTipoDependencia AND d.eliminado IS NULL")
    public List<Dependencia> buscarPorIdTipoDependencia(@Param("idTipoDependencia") String idTipoDependencia);
        
    @Query("SELECT d FROM Dependencia d WHERE d.instancia.id = :idInstancia AND d.eliminado IS NULL")
    public List<Dependencia> buscarPorIdInstancia(@Param("idInstancia") String idInstancia);


    @Query("SELECT d FROM Dependencia d WHERE d.eliminado IS NULL ORDER BY d.nombre")
    public List<Dependencia> buscarTodos();
    
    @Query("SELECT d FROM Dependencia d WHERE d.eliminado IS NULL")
    public Page<Dependencia> buscarTodas(Pageable paginable);

    @Query("SELECT d FROM Dependencia d WHERE d.eliminado IS NULL AND d.nombre LIKE :q")
    public Page<Dependencia> buscarTodas(Pageable paginable, @Param("q") String q);

}