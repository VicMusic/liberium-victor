package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.model.MPermiso;

@Repository("permisoRepository")
public interface PermisoRepository extends JpaRepository<Permiso, String> {

    @Query("SELECT p FROM Permiso p WHERE p.id = :id")
    public Permiso buscarPorId(@Param("id") String permisoId);
    
    @Query("SELECT p FROM Permiso p WHERE p.nombre = :nombre")
    public Permiso buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT p FROM Permiso p WHERE p.administrador = :administrador")
    public List<Permiso> buscarPorAdministrador(@Param("administrador") Boolean administrador);
    
    @Query("SELECT p FROM Permiso p WHERE p.id IN :ids")
    public List<Permiso> buscarPorIds(@Param("ids") String[] ids);
    
    @Query("SELECT new com.quinto.liberium.model.MPermiso(p.id, p.nombre, p.nombreAmigable, p.administrador) FROM Permiso p WHERE p.administrador = :administrador")
    public List<MPermiso> buscarMPermisoPorAdministrador(@Param("administrador") Boolean administrador);
    
    
}