package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.entity.Usuario;

@Repository("rolRepository")
public interface RolRepository extends JpaRepository<Rol, String> {

    @Query("SELECT r FROM Rol r WHERE r.eliminado is null")
    public List<Rol> buscarTodos();
    
    @Query("SELECT r FROM Rol r WHERE r.creador = :creador AND r.eliminado is null")
    public List<Rol> buscarTodosPorIdCreador(@Param("creador") Usuario creador);
    
    @Query("SELECT r FROM Rol r WHERE r.id = :id AND r.eliminado is null")
    public Rol buscarPorId(@Param("id") String id);
    
    @Query("SELECT r FROM Rol r WHERE r.nombre = :nombre AND r.eliminado is null")
    public Rol buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT r FROM Rol r WHERE r.eliminado is null")
    public Page<Rol> buscarTodos(Pageable pageable);
    
    @Query("SELECT r FROM Rol r WHERE r.eliminado is null AND r.nombre like :q")
    public Page<Rol> buscarTodos(Pageable pageable, @Param("q") String q);
    
    
}