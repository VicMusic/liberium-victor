package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Honorario;

@Repository("honorarioRepository")
public interface HonorarioRepository extends JpaRepository<Honorario, String>{

	@Query("SELECT h FROM Honorario h WHERE h.eliminado IS NULL AND h.id = :idHonorario")
	public Honorario buscarHonorarioPorId(@Param("idHonorario") String idHonorario);
	
	@Query("SELECT h FROM Honorario h WHERE h.eliminado IS NULL AND h.causa.id = :idCausa AND h.estudio = null ORDER BY h.creado")
	public List<Honorario> buscarHonorarioPorCausa(@Param("idCausa") String idCausa);
	
	@Query("SELECT h FROM Honorario h WHERE h.eliminado IS NULL AND h.estudio.id = :idEstudio ORDER BY h.creado")
	public List<Honorario> buscarHonorarioPorEstudio(@Param("idEstudio") String idEstudio);
	
}
