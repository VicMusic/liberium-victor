package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Modificacion;

@Repository("modificacionRepository")
public interface ModificacionRepository extends JpaRepository<Modificacion, String> {

    @Query("SELECT m FROM Modificacion m WHERE m.causa.id = :causaId ORDER BY m.creado DESC")
    public List<Modificacion> buscarPorCausa(@Param("causaId") String causaId);
    
    @Query("SELECT m.causa FROM Modificacion m group by m.causa order by MIN(m.creado) asc")
    public Page<Causa> porAntiguoModificacion(Pageable pageable);
    
    @Query("SELECT m.causa FROM Modificacion m group by m.causa order by MAX(m.creado) desc")
    public Page<Causa> porRecienteModificacion(Pageable pageable);


}