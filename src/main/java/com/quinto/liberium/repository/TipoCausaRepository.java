package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.TipoCausa;

@Repository("tipoCausaRepository")
public interface TipoCausaRepository extends JpaRepository<TipoCausa, String> {

    @Query("SELECT t FROM TipoCausa t WHERE t.id = :id AND t.eliminado IS NULL")
    public TipoCausa buscarPorId(@Param("id") String tipoCausaId);
    
    @Query("SELECT t FROM TipoCausa t WHERE t.nombre = :nombre AND t.eliminado IS NULL")
    public TipoCausa buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT t FROM TipoCausa t WHERE t.materia.nombre = :materia AND t.eliminado IS NULL")
    public List<TipoCausa> buscarPorMateria(@Param("materia") String materia);
    
    @Query("SELECT t FROM TipoCausa t WHERE t.materia.id = :idMateria AND t.eliminado IS NULL")
    public List<TipoCausa> buscarPorMateriaId(@Param("idMateria") String idMateria);
    
	@Query("SELECT t FROM TipoCausa t WHERE t.eliminado IS NULL ORDER BY t.nombre")
    public List<TipoCausa> buscarTodos();
    
    @Query("SELECT t FROM TipoCausa t WHERE t.eliminado IS NULL AND (t.nombre LIKE :q) OR (t.materia.nombre LIKE :q) ORDER BY t.nombre")
	public Page<TipoCausa> buscarTodas(Pageable pageable, @Param("q") String q);

	@Query("SELECT t FROM TipoCausa t WHERE t.eliminado IS NULL ORDER BY t.nombre")
	public Page<TipoCausa> buscarTodas(Pageable pageable);

}