package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Jurisdiccion;

@Repository("JuridiccionRespository")
public interface JurisdiccionRepository extends JpaRepository<Jurisdiccion, String>{
	
	@Query("SELECT j FROM Jurisdiccion j WHERE j.id = :id AND j.eliminado IS NULL")
    public Jurisdiccion buscarPorId(@Param("id") String idJurisdiccion);
    
    @Query("SELECT j FROM Jurisdiccion j WHERE j.nombre = :nombre AND j.eliminado IS NULL")
    public Jurisdiccion buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT j FROM Jurisdiccion j WHERE j.provincia.id = :idProvincia AND j.eliminado IS NULL")
    public List<Jurisdiccion> buscarPorIdProvincia(@Param("idProvincia") String idProvincia);
    
    @Query("SELECT j FROM Jurisdiccion j WHERE j.eliminado IS NULL ORDER BY j.nombre")
    public List<Jurisdiccion> buscarTodos();

    @Query("SELECT j FROM Jurisdiccion j WHERE j.eliminado IS NULL")
    public Page<Jurisdiccion> buscarTodas(Pageable paginable);
    
    @Query("SELECT j FROM Jurisdiccion j WHERE j.eliminado IS NULL AND j.nombre LIKE :q OR j.provincia.nombre LIKE :q")
    public Page<Jurisdiccion> buscarTodas(Pageable paginable, @Param("q") String q);

}
