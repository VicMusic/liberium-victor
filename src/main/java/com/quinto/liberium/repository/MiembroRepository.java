package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.model.MMiembro;

@Repository("miembroRepository")
public interface MiembroRepository extends JpaRepository<Miembro, String> {

    @Query("SELECT m FROM Miembro m WHERE m.id = :id")
    public Miembro buscarPorId(@Param("id") String miembroId);
    
    @Query("SELECT m FROM Miembro m WHERE m.usuario.id = :idUsuario")
    public List<Miembro> buscarMiembrosPorIdUsuario(@Param("idUsuario") String idUsuario);
    
    @Query("SELECT m FROM Miembro m WHERE m.usuario.id = :idUsuario AND m.eliminado IS NULL")
    public Miembro buscarPorIdUsuario(@Param("idUsuario") String idUsuario);
    
    @Query("SELECT m.estudio FROM Miembro m WHERE m.usuario.id = :idUsuario AND m.estudio.eliminado IS NULL")
    public List<Estudio> buscarEstudioPorIdUsuario(@Param("idUsuario") String idUsuario);
    
    @Query("SELECT m FROM Miembro m WHERE m.estudio.id = :estudioId")
    public Miembro buscarPorIdEstudio(@Param("estudioId") String estudioId);
    
    @Query("SELECT m FROM Miembro m WHERE m.estudio.id IN (SELECT a.estudio.id FROM Miembro a WHERE a.usuario.id = :idUsuario)")
    public List<Miembro> buscarMiembrosEstudioPorIdUsuario(@Param("idUsuario") String idUsuario);
    
    @Query("SELECT m FROM Miembro m WHERE m.rol.id = :rolId")
    public Miembro buscarPorIdRol(@Param("rolId") String rolId);

    @Query("SELECT m FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.eliminado IS NULL")
    public List<Miembro> buscarEMiembrosPoridEstudio(@Param("idEstudio") String idEstudio);

    @Query("SELECT m FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.usuario.id != :idUsuario AND m.eliminado IS NULL")
	public List<Miembro> buscarMiembrosPorIdEstudio(@Param("idEstudio") String idEstudio, @Param("idUsuario") String idUsuario);

    @Query("SELECT m FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.usuario.id = :idUsuario AND m.eliminado IS NULL")
	public Miembro buscarPorIdEstudioYIdUsuario(@Param("idEstudio") String idEstudio,@Param("idUsuario") String idUsuario);

    @Query("SELECT new com.quinto.liberium.model.MMiembro(m.id, m.usuario.nombre, m.usuario.email, m.estudio.nombre, m.rol.id, m.rol.nombre) FROM Miembro m WHERE m.eliminado IS NULL AND m.usuario.eliminado IS NULL")
    public Page<MMiembro> buscarTodosMMiembro(Pageable paginable);
    
    @Query("SELECT new com.quinto.liberium.model.MMiembro(m.id, m.usuario.nombre, m.usuario.email, m.estudio.nombre, m.rol.id, m.rol.nombre) FROM Miembro m WHERE m.eliminado IS NULL AND m.usuario.nombre LIKE :q OR m.estudio.nombre LIKE :q OR m.rol.nombre LIKE :q OR m.usuario.email LIKE :q")
	public Page<MMiembro> buscarTodosMMiembro(Pageable paginable,@Param("q") String q);
    
    @Query("SELECT new com.quinto.liberium.model.MMiembro(m.id, m.usuario.nombre, m.usuario.email, m.estudio.nombre, m.rol.id, m.rol.nombre) FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.eliminado IS NULL")
    public List<MMiembro> buscarMiembrosPoridEstudio(@Param("idEstudio") String idEstudio);
    
    @Query("SELECT new com.quinto.liberium.model.MMiembro(m.id, m.usuario.nombre, m.usuario.email, m.estudio.nombre, m.rol.id, m.rol.nombre) FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.eliminado IS NULL")
    public Page<MMiembro> buscarTodosPorEstudioMMiembro(Pageable paginable, @Param("idEstudio") String idEstudio);
    
    @Query("SELECT new com.quinto.liberium.model.MMiembro(m.id, m.usuario.nombre, m.usuario.email, m.estudio.nombre, m.rol.id, m.rol.nombre) FROM Miembro m WHERE m.estudio.id = :idEstudio AND m.eliminado IS NULL AND (m.usuario.nombre like :q  OR m.usuario.email like :q OR m.estudio.nombre like :q OR m.rol.nombre like :q)")
	public Page<MMiembro> buscarTodosPorEstudioMMiembro(Pageable paginable,@Param("q") String q, @Param("idEstudio") String idEstudio);

}