package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Notas;

@Repository("notasRepository")
public interface NotasRepository  extends JpaRepository<Notas, String> {
	
	@Query("SELECT n FROM Notas n WHERE n.item.id = :id AND n.eliminado IS NULL")
    public List<Notas> buscarNotasItem(@Param("id") String id);
	
	@Query("SELECT n FROM Notas n WHERE n.subItem.id = :id AND n.eliminado IS NULL")
    public List<Notas> buscarNotasSubItem(@Param("id") String id);

}
