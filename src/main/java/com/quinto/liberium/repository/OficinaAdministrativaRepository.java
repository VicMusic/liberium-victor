package com.quinto.liberium.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.quinto.liberium.entity.OficinaAdministrativa;

public interface OficinaAdministrativaRepository extends JpaRepository<OficinaAdministrativa, String>{

	@Query("SELECT c FROM OficinaAdministrativa c WHERE c.id = :id AND c.eliminado IS NULL")
	public OficinaAdministrativa buscarPorId(@Param("id")String idOficinaAdministrativa);
	
	@Query("SELECT c FROM OficinaAdministrativa c WHERE c.nombre = :nombre AND c.eliminado IS NULL")
    public OficinaAdministrativa buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT c FROM OficinaAdministrativa c WHERE c.provincia.id = :idProvincia AND c.eliminado IS NULL")
    public List<OficinaAdministrativa> buscarPorIdProvincia(@Param("idProvincia") String idProvincia);
    
    @Query("SELECT c FROM OficinaAdministrativa c WHERE c.eliminado IS NULL ORDER BY c.nombre")
    public List<OficinaAdministrativa> buscarTodos();

    @Query("SELECT c FROM OficinaAdministrativa c WHERE c.eliminado IS NULL")
    public Page<OficinaAdministrativa> buscarTodas(Pageable paginable);
    
    @Query("SELECT c FROM OficinaAdministrativa c WHERE c.eliminado IS NULL AND c.nombre LIKE :q OR c.provincia.nombre LIKE :q")
    public Page<OficinaAdministrativa> buscarTodas(Pageable paginable, @Param("q") String q);
	
}
