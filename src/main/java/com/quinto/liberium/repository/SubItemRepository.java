package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.SubItem;

@Repository
public interface SubItemRepository extends JpaRepository<SubItem, String>{

	@Query("SELECT s FROM SubItem s WHERE s.id = :id AND s.eliminado IS NULL")
	public SubItem buscarPorId(@Param("id") String id);
	
	@Query("SELECT s FROM SubItem s WHERE s.nombre = :nombre AND s.eliminado IS NULL")
	public SubItem buscarPorNombre(@Param("nombre") String nombre);
	
//	@Query("SELECT s FROM SubItem s WHERE s.item.id = :id AND s.eliminado IS NULL ORDER BY s.creado")
//	public List<SubItem> buscarPorItemId(@Param("id") String id);
//	
	@Query("SELECT s FROM SubItem s WHERE s.eliminado IS NULL ORDER BY s.creado")
	public Page<SubItem> buscarTodos(Pageable paginable);
}
