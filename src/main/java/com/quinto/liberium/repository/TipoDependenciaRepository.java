package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.TipoDependencia;

@Repository
public interface TipoDependenciaRepository extends JpaRepository<TipoDependencia, String>{

	@Query("SELECT t FROM TipoDependencia t WHERE t.id = :id AND t.eliminado IS NULL")
	public TipoDependencia buscarPorId(@Param("id") String id);
	
	@Query("SELECT t FROM TipoDependencia t WHERE t.nombre = :nombre AND t.eliminado IS NULL")
	public TipoDependencia buscarPorNombre(@Param("nombre") String nombre);
	
	@Query("SELECT t FROM TipoDependencia t WHERE t.eliminado IS NULL ORDER BY t.nombre")
	public List<TipoDependencia> buscarTodos();
	
	@Query("SELECT t FROM TipoDependencia t WHERE t.eliminado IS NULL")
	public Page<TipoDependencia> buscarTodas(Pageable paginable);
	
	@Query("SELECT t FROM TipoDependencia t WHERE t.eliminado IS NULL AND t.nombre LIKE :q")
	public Page<TipoDependencia> buscarTodas(Pageable paginable, @Param("q") String q);
}
