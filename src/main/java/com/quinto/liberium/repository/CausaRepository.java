package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Causa;

@Repository("causaRepository")
public interface CausaRepository extends JpaRepository<Causa, String> {

    @Query("SELECT c FROM Causa c WHERE c.id = :id")
    public Causa buscarPorId(@Param("id") String causaId);
    
    @Query("SELECT COUNT(*) FROM Causa c WHERE c.eliminado IS NULL AND c.modo = 'ACTIVA' AND c.estudio.id = :idEstudio")
    public Integer contarActivasEstudio(@Param("idEstudio") String idEstudio);
    
    @Query("SELECT COUNT(*) FROM Causa c WHERE c.eliminado IS NULL AND c.estudio.id = :idEstudio")
    public Integer contarTodasCausasEstudio(@Param("idEstudio") String idEstudio);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.creado asc")
    public Page<Causa> buscarTodos(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null AND (c.nombre LIKE :q) ORDER BY c.creado asc")
    public Page<Causa> buscarPorNombre(@Param("idEstudio") String idEstudio, String q, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.actor asc")
    public Page<Causa> alfabeticoAZ(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.actor desc")
    public Page<Causa> alfabeticoZA(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.estado desc")
    public Page<Causa> avanzado(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.estado asc")
    public Page<Causa> atrazado(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.creado asc")
    public Page<Causa> antiguo(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.creado desc")
    public Page<Causa> reciente(@Param("idEstudio") String idEstudio, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.creador.id = :usuarioId")
    public Page<Causa> buscarPorUsuario(@Param("usuarioId") String usuarioId, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.provincia.id = :provinciaId")
    public Page<Causa> buscarPorProvincia(@Param("provinciaId") String provinciaId, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :idEstudio AND c.modo = 'ARCHIVADA' AND c.eliminado is null ORDER BY c.creado asc")
    public List<Causa> buscarArchivadas(@Param("idEstudio") String idEstudio);
    
    @Query("SELECT c FROM Causa c WHERE c.modo = 'ACTIVA' AND c.eliminado is null ORDER BY c.creado asc")
    public List<Causa> buscarActivas();
    
    @Query("SELECT c FROM Causa c WHERE c.numeroExpediente = :numeroExpediente")
    public Causa buscarPorNumeroExpediente(@Param("numeroExpediente") String numeroExpediente);
    
    @Query("SELECT c FROM Causa c WHERE c.juzgadoOficina = :juzgadoOficina")
    public List<Causa> buscarPorJuzgadoOficina(@Param("juzgadoOficina") String juzgadoOficina);
    
    @Query("SELECT c FROM Causa c WHERE c.estudio.id = :estudioId")
    public List<Causa> buscarPorEstudio(@Param("estudioId") String estudioId);
    
    @Query("SELECT c FROM Causa c WHERE c.tipoCausa.id = :tipoCausaId")
    public Page<Causa> buscarPorTipoCausa(@Param("tipoCausaId") String tipoCausaId, Pageable pageable);
    
    @Query("SELECT c FROM Causa c WHERE c.instancia.id = :instanciaId")
    public List<Causa> buscarPorInstancia(@Param("instanciaId") String instanciaId);
    
    @Query(value = "SELECT * FROM liberium.causa c inner join causa_miembro_causa cmc on cmc.causa_id = c.id where cmc.miembro_causa_id = :miembroCausaId", nativeQuery=  true)
    public List<Causa> buscarPorMiembro(@Param("miembroCausaId") String miembroCausaId);
    /*
    @Query("SELECT c FROM Causa c WHERE c.miembrosCausa = :miembroCausaId AND c.eliminado IS NULL")
    public Page<Causa> buscarPorMiembroPage(@Param("miembroCausaId") String miembroCausaId);*/
}