package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Provincia;

@Repository("provinciaRepository")
public interface ProvinciaRepository extends JpaRepository<Provincia, String> {

    @Query("SELECT p FROM Provincia p WHERE p.id = :id AND p.eliminado IS NULL")
    public Provincia buscarPorId(@Param("id") String idProvincia);
    
    @Query("SELECT p FROM Provincia p WHERE p.nombre = :nombre AND p.eliminado IS NULL")
    public Provincia buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT p FROM Provincia p WHERE p.eliminado IS NULL AND p.pais.id = :idPais AND p.nombre = :nombre")
    public List<Provincia> buscarProvinciasRepetidasPais(@Param("idPais") String idPais, @Param("nombre") String nombre);
    
    @Query("SELECT p FROM Provincia p WHERE p.pais.id = :idPais AND p.eliminado IS NULL ORDER BY p.nombre")
    public List<Provincia> buscarPorIdPais(@Param("idPais") String idPais);
    
    @Query("SELECT p FROM Provincia p WHERE p.eliminado IS NULL")
    public Page<Provincia> buscarTodos(Pageable paginable);
    
    @Query("SELECT p FROM Provincia p WHERE p.eliminado IS NULL AND p.nombre LIKE :q OR p.pais.nombre LIKE :q")
    public Page<Provincia> buscarTodos(Pageable paginable, @Param("q") String q);

}