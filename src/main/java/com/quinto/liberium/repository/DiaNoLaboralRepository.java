package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.DiaNoLaboral;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;

@Repository("diaNoLaboralRepository")
public interface DiaNoLaboralRepository extends JpaRepository<DiaNoLaboral, String>{
	
    @Query("SELECT d FROM DiaNoLaboral d WHERE d.id = :id AND d.eliminado IS NULL")
    public DiaNoLaboral buscarPorId(@Param("id") String idDiaNoLaboral);
    
    @Query("SELECT d FROM DiaNoLaboral d WHERE d.eliminado IS NULL")
    public List<DiaNoLaboral> buscarTodos();

    @Query("SELECT p FROM DiaNoLaboral p WHERE p.eliminado IS NULL AND (p.titulo LIKE :q) OR (p.pais.nombre LIKE :q) OR (p.provincia.nombre LIKE :q) OR (p.tipoDiaNoLaborable LIKE :q) OR (p.fechaInicio LIKE :q)  OR (p.fechaFin LIKE :q) ORDER BY p.creado")
	public Page<DiaNoLaboral> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT p FROM DiaNoLaboral p WHERE p.eliminado IS NULL ORDER BY p.titulo")
	public Page<DiaNoLaboral> buscarTodos(Pageable pageable);
    
    @Query("SELECT d FROM DiaNoLaboral d WHERE d.eliminado IS NULL AND d.pais = :pais AND d.provincia IS NULL")
    public List<DiaNoLaboral> buscarDiasNoLaboralesPorPais(@Param("pais")Pais pais);
    
    @Query("SELECT d FROM DiaNoLaboral d WHERE d.eliminado IS NULL AND d.provincia = :provincia")
    public List<DiaNoLaboral> buscarDiasNoLaboralesPorProvincia(@Param("provincia")Provincia provincia);
    
    @Query("SELECT p FROM DiaNoLaboral p WHERE p.eliminado IS NULL AND (p.titulo LIKE :q OR p.pais.nombre LIKE :q OR p.tipoDiaNoLaborable LIKE :q OR p.fechaInicio LIKE :q OR p.fechaFin LIKE :q) ORDER BY p.creado")
   	public List<DiaNoLaboral> buscarTodos(@Param("q") String q);
    
    @Query("SELECT p FROM DiaNoLaboral p WHERE p.eliminado IS NULL AND p.provincia.nombre LIKE :q ORDER BY p.creado")
   	public List<DiaNoLaboral> buscarTodosPorProvincia(@Param("q") String q);
	
}
