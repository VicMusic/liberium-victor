package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Circunscripcion;

@Repository("circunscripcionRepository")
public interface CircunscripcionRepository extends JpaRepository<Circunscripcion, String> {

    @Query("SELECT c FROM Circunscripcion c WHERE c.id = :id AND c.eliminado IS NULL")
    public Circunscripcion buscarPorId(@Param("id") String idCircunscripcion);
    
    @Query("SELECT c FROM Circunscripcion c WHERE c.nombre = :nombre AND c.eliminado IS NULL")
    public Circunscripcion buscarCircuncripcionPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT c FROM Circunscripcion c WHERE c.nombre = :nombre AND c.eliminado IS NULL")
    public List<Circunscripcion> buscarCircuncripcionesPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT c FROM Circunscripcion c WHERE c.provincia.id = :idProvincia AND c.eliminado IS NULL")
    public List<Circunscripcion> buscarPorIdProvincia(@Param("idProvincia") String idProvincia);
    
    @Query("SELECT c FROM Circunscripcion c WHERE c.eliminado IS NULL ORDER BY c.nombre")
    public List<Circunscripcion> buscarTodos();

    @Query("SELECT c FROM Circunscripcion c WHERE c.eliminado IS NULL")
    public Page<Circunscripcion> buscarTodas(Pageable paginable);
    
    @Query("SELECT c FROM Circunscripcion c, IN(c.localidades) l WHERE c.eliminado IS NULL AND (c.nombre LIKE :q OR c.provincia.nombre LIKE :q or l.nombre LIKE :q) AND c.eliminado IS NULL")
    public Page<Circunscripcion> buscarTodas(Pageable paginable, @Param("q") String q);
    
    @Query("SELECT c FROM Circunscripcion c, IN(c.localidades) l WHERE c.eliminado IS NULL AND l.nombre LIKE :q AND c.eliminado IS NULL")
    public Page<Circunscripcion> buscarTodasPorLocalidad(Pageable paginable, @Param("q") String q);
    
    @Query("SELECT c FROM Circunscripcion c WHERE c.eliminado IS NULL AND (c.nombre LIKE :q OR c.provincia.nombre LIKE :q) AND c.eliminado IS NULL")
    public List<Circunscripcion> buscarTodas(@Param("q") String q);
    
    @Query("SELECT c FROM Circunscripcion c, IN(c.localidades) l WHERE c.eliminado IS NULL AND l.nombre LIKE :q AND c.eliminado IS NULL")
    public List<Circunscripcion> buscarTodasPorLocalidad(@Param("q") String q);
    
}