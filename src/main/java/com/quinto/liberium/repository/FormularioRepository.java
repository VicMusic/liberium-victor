package com.quinto.liberium.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Formulario;

@Repository("formularioRepository")
public interface FormularioRepository extends JpaRepository<Formulario, String> {

    @Query("SELECT p FROM Formulario p WHERE p.id = :id AND p.eliminado IS NULL")
    public Formulario buscarPorId(@Param("id") String idFormulario);
    
    @Query("SELECT p FROM Formulario p WHERE p.titulo = :nombre AND p.eliminado IS NULL")
    public Formulario buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT p FROM Formulario p WHERE p.tipo.id = :idTipoCausa AND p.eliminado IS NULL")
    public Formulario buscarPorTipoDeCausa(@Param("idTipoCausa")String idTipoCausa);

	@Query("SELECT p FROM Formulario p WHERE p.eliminado IS NULL AND (p.titulo LIKE :q) OR (p.tipo.materia.nombre LIKE :q) OR (p.tipo.nombre LIKE :q) ORDER BY p.titulo")
	public Page<Formulario> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT p FROM Formulario p WHERE p.eliminado IS NULL ORDER BY p.titulo")
	public Page<Formulario> buscarTodos(Pageable pageable);

}