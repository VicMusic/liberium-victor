package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Abogado;

@Repository("abogadoRepository")
public interface AbogadoRepository extends JpaRepository<Abogado, String>{
	

	@Query("SELECT a FROM Abogado a WHERE a.id = :id")
	public Abogado buscarAbogadoPorId(@Param("id") String abogadoId);
	
	@Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL AND a.usuario.dni = :dni")
	public Abogado buscarAbogadoPorDNI(@Param("dni") String abogadoDNI);

	@Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL")
	public List<Abogado> buscarAbogadosActivos();

	@Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL AND (a.usuario.nombre LIKE :nombre AND a.materias IS NOT EMPTY)")
	public List<Abogado> buscarAbogadosPorNombre(@Param("nombre") String nombre);
	
	@Query("SELECT a FROM Abogado a WHERE a.provincia.id = :provincia AND a.eliminado IS NULL")
	public List<Abogado> buscarAbogadoPorProvincia(@Param("provincia") String provincia);
	
	@Query(value = "select * from abogado a join abogado_materias ma on ma.abogado_id = a.id join materia m on m.id = ma.materias_id where a.eliminado is null and m.id = :especialidad and a.localidad_id = :localidad", nativeQuery = true)
	public List<Abogado> buscarAbogadoPorLocalidadYMateria(@Param("especialidad") String especialidad, @Param("localidad") String localidad);
	
	@Query(value = "select * from abogado a join abogado_materias ma on ma.abogado_id = a.id join materia m on m.id = ma.materias_id where a.eliminado is null and m.id = :especialidad and a.provincia_id = :provincia", nativeQuery = 
			true)
	public List<Abogado> buscarAbogadosPorProvinciaYEspecialidad(@Param("provincia") String provincia,
			@Param("especialidad") String especialidad);
	
    @Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL")
    public Page<Abogado> buscarTodos(Pageable paginable);
    

    @Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL AND a.usuario.nombre LIKE :q")
    public Page<Abogado> buscarTodos(Pageable paginable, @Param("q") String q);
    
    @Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL")
    public List<Abogado> buscarTodos();
    
    @Query("SELECT a FROM Abogado a WHERE a.eliminado IS NULL AND a.usuario.nombre LIKE :q")
    public List<Abogado> buscarTodos(@Param ("q") String q);
    
    @Query("SELECT a FROM Abogado a WHERE a.usuario.email = :email and a.usuario.eliminado is null")
    public Abogado buscarPorMail(@Param("email") String email);
    
    @Query(value = "select * from abogado a join estudio_abogados ea on ea.abogados_id = a.id join estudio e on\n" + 
			"e.id = ea.estudio_id where e.eliminado is null and e.id = :idEstudio", nativeQuery = true)
	public List<Abogado> buscarAbogadosEstudio(@Param("idEstudio") String idEstudio);

}
