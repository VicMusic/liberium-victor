package com.quinto.liberium.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Pais;

@Repository("paisRepository")
public interface PaisRepository extends JpaRepository<Pais, String> {

    @Query("SELECT p FROM Pais p WHERE p.id = :id AND p.eliminado IS NULL")
    public Pais buscarPorId(@Param("id") String idPais);
    
    @Query("SELECT p FROM Pais p WHERE p.nombre = :nombre AND p.eliminado IS NULL")
    public Pais buscarPorNombre(@Param("nombre") String nombre);

	@Query("SELECT p FROM Pais p WHERE p.eliminado IS NULL AND (p.nombre LIKE :q) ORDER BY p.nombre")
	public Page<Pais> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT p FROM Pais p WHERE p.eliminado IS NULL ORDER BY p.nombre")
	public Page<Pais> buscarTodos(Pageable pageable);

}