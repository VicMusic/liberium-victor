package com.quinto.liberium.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Usuario;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, String>{

	@Query("SELECT c FROM Consulta c WHERE c.id = :id AND c.eliminado IS NULL")
	public Consulta buscarPorId(@Param ("id") String id);
	
	@Query("SELECT c FROM Consulta c WHERE c.turno.id = :id AND c.eliminado IS NULL")
	public Consulta buscarPorTurnoId(@Param ("id") String id);
	
	@Query("SELECT c FROM Consulta c WHERE c.mail = :mail AND c.eliminado IS NULL")
	public Consulta buscarPorMail(@Param ("mail") String mail);
	
	@Query("SELECT c FROM Consulta c WHERE c.eliminado IS NULL")
	public Page<Consulta> buscarTodas(Pageable paginable);
	
	@Query("SELECT c FROM Consulta c WHERE c.eliminado IS NULL AND c.turno.abogado.usuario = :usuario ORDER BY c.creado")
	Page<Consulta> buscarConsultasPorAbogado(@Param("usuario") Usuario usuario, Pageable pageable);
	
	@Query("SELECT c FROM Consulta c WHERE c.eliminado IS NULL AND c.turno.abogado.usuario = :usuario and (c.nombre like :q or c.apellido like :q or c.dni like :q or c.direccion like :q or c.mail like :q or c.localidad.nombre like :q or c.tipoCausa.nombre like :q or c.telefono like :q) ORDER BY c.creado")
	Page<Consulta> buscarConsultasPorAbogado(@Param("usuario") Usuario usuario, Pageable pageable, @Param("q") String q);
	
}
