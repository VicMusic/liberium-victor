
package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Pregunta;

@Repository("preguntaRepository")
public interface PreguntaRepository extends JpaRepository<Pregunta, String> {

    @Query("SELECT p FROM Pregunta p WHERE p.id = :id AND p.eliminado IS NULL")
    public Pregunta buscarPorId(@Param("id") String idPregunta);
    
    @Query("SELECT p FROM Pregunta p WHERE p.titulo = :nombre AND p.eliminado IS NULL")
    public Pregunta buscarPorNombre(@Param("nombre") String nombre);

    @Query("SELECT MAX(p.orden) FROM Pregunta p WHERE p.formulario.id = :id AND p.eliminado IS NULL")
    public Integer buscarMaximoOrden(@Param("id") String id);
    
    @Query("SELECT p FROM Pregunta p WHERE p.formulario.id = :id AND p.eliminado IS NULL ORDER BY p.orden")
    public List<Pregunta> buscarPorFormulario(@Param("id") String id);
    
    @Query("SELECT p FROM Pregunta p WHERE p.formulario.id = :id AND p.orden = :orden AND p.eliminado IS NULL ORDER BY p.orden")
    public List<Pregunta> buscarMismoOrden(@Param("id") String id, @Param("orden") int orden);

    @Query("SELECT p FROM Pregunta p WHERE p.formulario.id = :id AND p.orden >= :orden AND p.eliminado IS NULL ORDER BY p.orden")
    public List<Pregunta> buscarMayores(@Param("id") String id, @Param("orden") int orden);

	@Query("SELECT p FROM Pregunta p WHERE p.eliminado IS NULL AND (p.titulo LIKE :q) ORDER BY p.titulo")
	public Page<Pregunta> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT p FROM Pregunta p WHERE p.eliminado IS NULL ORDER BY p.titulo")
	public Page<Pregunta> buscarTodos(Pageable pageable);

}