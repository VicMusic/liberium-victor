package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Localidad;

@Repository
public interface LocalidadRepository extends JpaRepository<Localidad, String>{

	@Query("SELECT l FROM Localidad l WHERE l.id = :id AND l.eliminado IS NULL")
	public Localidad buscarPorId(@Param("id") String id);
	
	@Query("SELECT l FROM Localidad l WHERE l.nombre = :nombre AND l.eliminado IS NULL")
	public Localidad buscarPorNombre(@Param("nombre") String nombre);
	
    @Query("SELECT l FROM Localidad l WHERE l.eliminado IS NULL AND l.provincia.id = :idProvincia AND l.nombre = :nombre")
    public List<Localidad> buscarLocalidaesRepetidas(@Param("idProvincia") String idProvincia, @Param("nombre") String nombre);
	
	@Query("SELECT l FROM Localidad l WHERE l.provincia.id = :id AND l.eliminado IS NULL")
	public List<Localidad> buscarPorIdProvincia(@Param("id") String id);
	
	@Query("SELECT l FROM Localidad l WHERE l.eliminado IS NULL AND (l.nombre LIKE :q OR l.provincia.nombre LIKE :q) ORDER BY l.nombre")
	public Page<Localidad> buscarTodos(Pageable pageable, @Param("q") String q);

	@Query("SELECT l FROM Localidad l WHERE l.eliminado IS NULL ORDER BY l.nombre")
	public Page<Localidad> buscarTodos(Pageable pageable);
}
