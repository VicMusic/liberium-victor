package com.quinto.liberium.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.ContabilidadEstudio;

@Repository
public interface ContabilidadEstudioRepository extends JpaRepository<ContabilidadEstudio, String>{

	@Query("SELECT b FROM ContabilidadEstudio b WHERE b.id = :id AND b.eliminado IS NULL")
	public ContabilidadEstudio buscarPorId(@Param("id") String id);
	
//	@Query("SELECT b FROM ContabilidadEstudio b WHERE b.nombre = :nombre AND b.eliminado IS NULL")
//	public ContabilidadEstudio buscarPorNombre(@Param("nombre") String nombre);
	
	@Query("SELECT b FROM ContabilidadEstudio b WHERE b.estudio.id = :idEstudio AND b.eliminado IS NULL ORDER BY b.creado")
	public List<ContabilidadEstudio> buscarPorIdEstudio(@Param("idEstudio") String id);
	
	@Query("SELECT b FROM ContabilidadEstudio b WHERE b.estudio.id = :idEstudio AND b.fechaContabilidad BETWEEN :startDate AND :endDate AND b.eliminado IS NULL ORDER BY b.fechaContabilidad")
	public List<ContabilidadEstudio> buscarPorIdEstudioEntreDosFechas(@Param("idEstudio") String id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
