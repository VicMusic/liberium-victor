package com.quinto.liberium.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;

@Repository("usuarioRepository")
public interface UsuarioRepository extends JpaRepository<Usuario, String> {
	
	Optional<Usuario> findByEmail(String email);
	
	@Query("SELECT u FROM Usuario u WHERE u.resetToken = :resetToken AND u.eliminado IS NULL")
	public Usuario buscarPorResetToken(@Param("resetToken")String resetToken);
	
	@Query("SELECT u FROM Usuario u WHERE u.confirmToken = :confirmToken AND u.eliminado IS NULL")
	public Usuario buscarPorConfirmToken(@Param("confirmToken")String confirmToken);
	
    @Query("SELECT u FROM Usuario u WHERE u.email = :email AND u.eliminado IS NULL")
    public Usuario buscarPorEmail(@Param("email") String email);
    
    @Query("SELECT u FROM Usuario u WHERE u.nombre = :nombre AND u.eliminado IS NULL")
    public Usuario buscarPorNombre(@Param("nombre") String nombre);

    @Query("SELECT u FROM Usuario u WHERE u.id = :id AND u.eliminado IS NULL")
	public Usuario buscarPorId(@Param("id") String id);
    
    @Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND (c.tipoUsuario = 'USUARIO') AND c.isAbogado = true")
	public List<Usuario> buscarTodosAbogados();
    
	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND (c.nombre LIKE :nombre AND (c.tipoUsuario = 'USUARIO') AND c.isAbogado = true)")
	public List<Usuario> buscarTodosAbogados(@Param("nombre") String nombre);  
    
    @Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND (c.tipoUsuario = 'USUARIO') AND c.isAbogado = false")
	Page<Usuario> buscarTodosUsuariosNoAbogados(Pageable pageable);
	
    @Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL")
	Page<Usuario> buscarTodos(Pageable pageable);
	
	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND (c.nombre LIKE :q OR c.email LIKE :q)")
	Page<Usuario> buscarTodos(Pageable pageable, @Param("q") String q);
	
	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND c.tipoUsuario != 'USUARIO'")
	Page<Usuario> buscarTodosPanelAdministrador(Pageable pageable);
	
	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND c.tipoUsuario != 'USUARIO' AND (c.nombre LIKE :q OR c.email LIKE :q)")
	Page<Usuario> buscarTodosPanelAdministrador(Pageable pageable, @Param("q") String q);
	
	@Query("SELECT c FROM Usuario c WHERE c.tipoUsuario != 'USUARIO' AND c.eliminado IS NULL")
	Page<Usuario> buscarMiembrosLiberium(Pageable pageable);
	
	@Query("SELECT c FROM Usuario c WHERE c.tipoUsuario != 'USUARIO' AND c.eliminado IS NULL and (c.nombre LIKE :q or c.email LIKE :q or c.dni LIKE :q or c.tipoUsuario LIKE :q)")
	Page<Usuario> buscarMiembrosLiberium(Pageable pageable, @Param("q") String q);

	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND c.nombre LIKE :termino ORDER BY c.nombre ")
	List<Usuario> buscarTodos(@Param("termino") String termino);
	
	@Query("SELECT u FROM Usuario u WHERE u.eliminado IS NULL AND u.pais = :pais")
	List<Usuario> buscarUsuariosPorPais(@Param("pais") Pais pais);
	
	@Query("SELECT u FROM Usuario u WHERE u.eliminado IS NULL AND u.provincia = :provincia")
	List<Usuario> buscarUsuariosPorProvincia(@Param("provincia") Provincia provincia);
	
	@Query("SELECT c FROM Usuario c WHERE c.eliminado IS NULL AND c.tipoUsuario = 'ADMINISTRADOR'")
	public List<Usuario> buscarAdministradores();  
	
//	//=========Estadísticas==========//
//	
//	@Query("SELECT COUNT(enabled) FROM Usuario u WHERE u.enabled = true")
//	public Integer usuariosInactivos();
//	
//	@Query("SELECT COUNT(enabled) FROM Usuario u WHERE u.enabled = true")
//	public Integer usuariosActivos();
//	
}