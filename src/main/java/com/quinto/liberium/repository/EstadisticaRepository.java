package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.quinto.liberium.entity.Estadistica;
import com.quinto.liberium.entity.Usuario;

public interface EstadisticaRepository extends JpaRepository<Estadistica, String> {

	
	@Query("SELECT e FROM Estadistica e WHERE e.creador = :creador")
	public List<Estadistica> buscarEstadisticas(@Param("creador") Usuario creador);
	
	@Query("SELECT e FROM Estadistica e WHERE e.nombre = :nombre AND e.creador = :creador")
	public Estadistica buscarEstadisticasPorNombre(@Param("nombre") String nombre, @Param("creador") Usuario creador);
	
	@Query("SELECT e FROM Estadistica e WHERE e.nombre = :nombre")
	public List<Estadistica> buscarEstadisticasPorNombreAdmin(@Param("nombre") String nombre);
}
