package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;

@Repository
public interface ChecklistRepository extends JpaRepository<Checklist, String> {

	@Query("SELECT c FROM Checklist c WHERE c.id = :id AND c.eliminado IS NULL")
	public Checklist buscarPorId(@Param("id") String id);

	@Query("SELECT c FROM Checklist c WHERE c.nombre = :nombre AND c.eliminado IS NULL")
	public Checklist buscarPornombre(@Param("nombre") String nombre);

	@Query("SELECT c FROM Checklist c WHERE c.pais.id = :id AND c.eliminado IS NULL")
	public List<Checklist> buscarPorPaisId(@Param("id") String idPais);

	@Query("SELECT c FROM Checklist c WHERE c.provincia.id = :id AND c.eliminado IS NULL")
	public List<Checklist> buscarPorProvinciaId(@Param("id") String idProvincia);

	@Query("SELECT c FROM Checklist c WHERE c.provincia.pais.id = :id AND c.eliminado IS NULL")
	public List<Checklist> buscarPorProvinciaPorIdPais(@Param("id") String idPais);

	@Query("SELECT c FROM Checklist c WHERE c.tipoCausa.id = :id AND c.rol = :rol AND c.eliminado IS NULL")
	public Checklist buscarPorTipoDeCausaIdYTipoCheck(@Param("id") String idTipoCausa, @Param("rol") TipoRolCausa tipoCheck);
	
	@Query("SELECT c FROM Checklist c WHERE c.tipoCausa.id = :id AND c.tipoFuero = :tipoFuero AND c.creador.id = :creadorId AND c.eliminado IS NULL")
	public List<Checklist> buscarPorTipoDeCausaIdYTipoFuero(@Param("id") String idTipoCausa, @Param("tipoFuero") TipoFuero tipoFuero, @Param("creadorId") String usuarioId);
	
	@Query("SELECT c FROM Checklist c WHERE c.tipoCausa.id = :id AND c.tipoFuero = :tipoFuero AND c.eliminado IS NULL AND c.plantilla IS TRUE AND c.visibilidad = 'Publico'")
	public List<Checklist> buscarTodosPorTipoDeCausaIdYTipoFuero(@Param("id") String idTipoCausa, @Param("tipoFuero") TipoFuero tipoFuero);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND (c.nombre LIKE :q OR c.pais.nombre LIKE :q or c.tipoFuero LIKE :q OR c.formulario.titulo LIKE :q OR c.rol LIKE :q OR c.tipoCausa.nombre LIKE :q)")
	public Page<Checklist> buscarTodos(Pageable paginable, @Param("q") String q);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND (c.nombre LIKE :q OR c.pais.nombre LIKE :q OR c.rol LIKE :q OR c.tipoFuero LIKE :q OR c.visibilidad LIKE :q OR c.formulario.titulo LIKE :q OR c.tipoCausa.nombre LIKE :q)")
	public List<Checklist> buscarTodos(@Param("q") String q);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND c.provincia.nombre LIKE :q")
	public List<Checklist> buscarTodosProvincia(@Param("q") String q);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND c.circunscripcion.nombre LIKE :q")
	public List<Checklist> buscarTodosCircunscripcion(@Param("q") String q);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND c.instancia.nombre LIKE :q")
	public List<Checklist> buscarTodosInstancia(@Param("q") String q);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND c.materia.nombre LIKE :q")
	public List<Checklist> buscarTodosMateria(@Param("q") String q);

	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL ORDER BY c.nombre")
	public Page<Checklist> buscarTodos(Pageable paginable);
	
	@Query("SELECT c FROM Checklist c WHERE c.eliminado IS NULL AND c.plantilla = true ORDER BY c.nombre")
	public Page<Checklist> buscarTodosPlantilla(Pageable paginable);
	
	@Query("SELECT c FROM Checklist c WHERE c.creador.id = :id AND c.eliminado IS NULL AND c.plantilla IS TRUE AND c.visibilidad = 'Privado'")
	public List<Checklist> buscarPorUsuarioCreador(@Param("id") String idCreador);
}
