package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Proceso;

@Repository
public interface ProcesoRepository extends JpaRepository<Proceso, String>{

    @Query("SELECT p FROM Proceso p WHERE p.id = :id AND p.eliminado IS NULL")
    public Proceso buscarPorId(@Param("id") String procesoId);
    
    @Query("SELECT p FROM Proceso p WHERE p.nombre = :nombre AND p.eliminado IS NULL")
    public Proceso buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT p FROM Proceso p WHERE p.materia.id = :idMateria AND p.eliminado IS NULL")
    public List<Proceso> buscarPorIdMateria(@Param("idMateria") String idMateria);

    @Query("SELECT p FROM Proceso p WHERE p.eliminado IS NULL")
    public List<Proceso> buscarTodos();
    
    @Query("SELECT p FROM Proceso p WHERE p.eliminado IS NULL")
    public Page<Proceso> buscarTodas(Pageable paginable);

    @Query("SELECT p FROM Proceso p WHERE p.eliminado IS NULL AND p.nombre LIKE q")
    public Page<Proceso> buscarTodas(Pageable paginable, @Param("q") String q);
}
