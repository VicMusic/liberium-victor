package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Usuario;

@Repository("escirtoRepository")
public interface EscritoRepository extends JpaRepository<Escrito, String> {

	@Query("SELECT e FROM Escrito e WHERE e.id = :id AND e.eliminado = null")
	public Escrito buscarPorId(@Param("id") String escritoId);

	@Query("SELECT e FROM Escrito e WHERE e.titulo = :titulo AND e.eliminado = null")
	public List<Escrito> buscarPorTitulo(@Param("titulo") String escritoTitulo);

	@Query ("SELECT e FROM Escrito e WHERE e.eliminado = null ORDER BY e.titulo")
	public List<Escrito> buscarTodos();

	@Query ("SELECT e FROM Escrito e WHERE e.eliminado IS NULL")
	public Page<Escrito> buscarTodos(Pageable pageable);
	
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.titulo LIKE :q")
    public Page<Escrito> buscarTodos(Pageable paginable, @Param("q") String q);
    
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.original = true AND e.tipoCausa.id = :id")
    public List<Escrito> buscarOriginalesPorTipoCausa(@Param("id") String idTipoCausa);
    
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.original = true")
    public List<Escrito> buscarOriginalesList();
    
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.original = false AND e.creador = :usuario")
    public List<Escrito> buscarLocales(@Param("usuario") Usuario usuario);
    
    @Query("SELECT e FROM Escrito e WHERE e.original = false ORDER BY e.creado")
    public List<Escrito> buscarPropuestos();    
   
	@Query ("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.creador.id = :idUsuario ORDER BY e.titulo")
	public List<Escrito> buscarTodosUsuarioId(@Param("idUsuario") String idUsuario);
	
	@Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.id IN :ids")
	public List<Escrito> buscarPorIds(@Param("ids") String[] ids);
	
	@Query ("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.creador.id = :idUsuario AND (e.titulo LIKE :q) ORDER BY e.titulo")
	public Page<Escrito> buscarTodosUsuarioId(Pageable pageable, @Param("idUsuario") String idUsuario, @Param("q") String q);
	
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.original = true AND (e.titulo LIKE :q) ORDER BY e.titulo")
    public Page<Escrito> buscarOriginales(Pageable pageable, @Param("q") String q);
    
    @Query ("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.creador.id = :idUsuario ORDER BY e.titulo")
	public Page<Escrito> buscarTodosUsuarioId(Pageable pageable, @Param("idUsuario") String idUsuario);
	
    @Query("SELECT e FROM Escrito e WHERE e.eliminado IS NULL AND e.original = true ORDER BY e.titulo")
    public Page<Escrito> buscarOriginales(Pageable pageable);
    
}
