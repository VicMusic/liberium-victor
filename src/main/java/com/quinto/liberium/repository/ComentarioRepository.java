package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Comentario;

@Repository
public interface ComentarioRepository extends JpaRepository<Comentario, String>{

	@Query("SELECT c FROM Comentario c WHERE c.id = :id AND c.eliminado IS NULL")
	public Comentario buscarPorId(@Param("id") String id);

	@Query("SELECT c FROM Comentario c WHERE c.usuario.id = :usuarioId AND c.eliminado IS NULL")
	public Comentario buscarPorUsuario(@Param("usuarioId") String UsuarioId);

	@Query("SELECT c FROM Comentario c WHERE c.causa.id = :idCausa AND c.eliminado IS NULL")
	public List<Comentario> comentariosPorCausa(@Param("idCausa") String idCausa);
}
