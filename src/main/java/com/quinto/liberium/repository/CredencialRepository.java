package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Credencial;

@Repository("credencialRepository")
public interface CredencialRepository extends JpaRepository<Credencial, String> {

    @Query("SELECT c FROM Credencial c WHERE c.id = :id")
    public Credencial obtenerPorId(@Param("id") String credencialId);

    @Query("SELECT c FROM Credencial c WHERE c.usuario.id = :id")
    public List<Credencial> obtenerPorUsuario(@Param("id")String usuarioId);

}