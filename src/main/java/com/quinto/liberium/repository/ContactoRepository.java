package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Usuario;

@Repository("contactoRepository")
public interface ContactoRepository extends JpaRepository<Contacto, String> {

    @Query("SELECT c FROM Contacto c WHERE c.id = :id")
    public Contacto buscarPorId(@Param("id") String contactoId);
    
    @Query("SELECT c FROM Contacto c WHERE c.nombre = :nombre")
    public Contacto buscarPorNombre(@Param("nombre") String nombre);
    
    @Query("SELECT c FROM Contacto c WHERE c.nombre LIKE :q AND c.eliminado IS NULL ORDER BY c.apellido ASC")
    public Page<Contacto> buscarTodos(Pageable paginable,@Param("q") String q);
    
    @Query("SELECT c FROM Contacto c WHERE c.eliminado IS NULL ORDER BY c.apellido ASC")
    public Page<Contacto> buscarTodos(Pageable paginable);
    
    @Query("SELECT c FROM Contacto c WHERE c.creador = :usuario AND c.eliminado IS NULL ORDER BY c.apellido ASC")
    public List<Contacto> buscarContactosPorUsuario(@Param("usuario") Usuario usuario);
    
    @Query("SELECT c FROM Contacto c WHERE c.creador = :usuario AND c.nombre LIKE :q AND c.eliminado IS NULL ORDER BY c.apellido ASC")
    public List<Contacto> buscarContactosPorNombrePorUsuario(@Param("usuario") Usuario usuario, @Param("q") String q);
    
//  BUSQUEDAS CONTROLLADOR
    
    @Query("SELECT c FROM Contacto c WHERE c.creador.id = :idUsuario AND c.eliminado IS NULL AND (c.nombre LIKE :q OR c.apellido LIKE :q) ORDER BY c.apellido ASC")
    public Page<Contacto> buscarContactosPorUsuairoPage(@Param("idUsuario") String idUsuario, @Param("q") String q, Pageable pageable);
    
    @Query("SELECT c FROM Contacto c WHERE c.estudio.id = :idEstudio AND c.eliminado IS NULL AND (c.nombre LIKE :q) ORDER BY c.apellido ASC")
    public Page<Contacto> buscarContactosPorEstudioPage(@Param("idEstudio") String idEstudio, @Param("q") String q, Pageable pageable);
    
    @Query("SELECT c FROM Contacto c WHERE c.creador.id = :idUsuario AND c.eliminado IS NULL ORDER BY c.apellido ASC")
    public Page<Contacto> buscarContactosPorUsuairoPage(@Param("idUsuario") String idUsuario, Pageable pageable);
    
    @Query("SELECT c FROM Contacto c WHERE c.estudio.id = :idEstudio AND c.eliminado IS NULL ORDER BY c.apellido ASC")
    public Page<Contacto> buscarContactosPorEstudioPage(@Param("idEstudio") String idEstudio, Pageable pageable);
    
}