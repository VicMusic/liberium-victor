package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Notificacion;

@Repository("notificacionRepository")
public interface NotificacionRepository extends JpaRepository<Notificacion, String>{
	
	@Query("SELECT n FROM Notificacion n WHERE n.eliminado IS NULL AND n.id = :idNotificacion")
	public Notificacion buscarNotificacionPorId(@Param("idNotificacion") String idNotificacion);
	
	@Query("SELECT n FROM Notificacion n WHERE n.eliminado IS NULL and n.vista = false AND n.usuario.id = :idUsuario ORDER BY n.fechaNotificacion DESC")
	public List<Notificacion> buscarNotificacionesPorUsuario(@Param("idUsuario") String idUsuario);

}
