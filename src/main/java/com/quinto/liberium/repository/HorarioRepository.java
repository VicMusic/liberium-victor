package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Horario;

@Repository("horarioRepository")
public interface HorarioRepository extends JpaRepository<Horario, String> {

	@Query("SELECT h FROM Horario h")
	public List<Horario> buscarTodos();

	@Query("SELECT h FROM Horario h WHERE h.id = :idHorario")
	public Horario buscarHorarioPorId(@Param("idHorario") String idHorario);

	@Query("SELECT h FROM Horario h WHERE h.abogado.id = :idAbogado")
	public List<Horario> buscarHorariosPorAbogado2(@Param("idAbogado") String idAbogado);
	
	@Query("SELECT h FROM Horario h WHERE h.abogado.id = :idAbogado AND h.solicitado = false")
	public List<Horario> buscarHorariosPorAbogado(@Param("idAbogado") String idAbogado);

	@Query("SELECT h FROM Horario h WHERE h.abogado.id = :idAbogado AND h.dia = :dia AND h.hora = :hora AND h.eliminado IS NULL")
	public Horario buscarHorarioSolicitadoPorAbogado(@Param("idAbogado") String idAbogado, @Param("dia") String dia, @Param("hora")String hora);	 
	
}
