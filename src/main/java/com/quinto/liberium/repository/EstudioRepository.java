package com.quinto.liberium.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Usuario;

@Repository("estudioRepository")
public interface EstudioRepository extends JpaRepository<Estudio, String> {

	@Query("SELECT e FROM Estudio e WHERE e.id = :id AND e.eliminado IS NULL")
	public Estudio buscarPorId(@Param("id") String estudioId);

	@Query("SELECT e FROM Estudio e WHERE e.nombre = :nombre AND e.eliminado IS NULL")
	public Estudio buscarPorNombre(@Param("nombre") String nombre);

	@Query("SELECT e FROM Estudio e WHERE e.creador = :usuario AND e.eliminado IS NULL")
	public List<Estudio> buscarPorCreador(@Param("usuario") Usuario usuario);
	
	@Query(value = "select * from estudio e join estudio_abogados ea on ea.estudio_id = e.id join abogado a on\n" + 
			"a.id = ea.abogados_id where e.eliminado is null and a.id = :idAbogado ", nativeQuery = true)
	public List<Estudio> buscarPorPertenencia(@Param("idAbogado") String idAbogado);
	
	@Query("SELECT e FROM Estudio e WHERE e.eliminado IS NULL")
	public List<Estudio> buscarTodos();
	
}