package com.quinto.liberium.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import com.quinto.liberium.service.UsuarioService;

@Service
public class CustomLogoutHandler implements LogoutHandler {
	
	@Autowired
	private UsuarioService usuarioService;

    public void logout(HttpServletRequest request, HttpServletResponse response,
                       Authentication authentication) {
        String userName = authentication.getName();
        usuarioService.logout(userName);
    }
}