package com.quinto.liberium.security;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordEncoder {

	private static int workload = 12;

	public static String encriptarPassword(String pass) {
		String salt = BCrypt.gensalt(workload);
		return(BCrypt.hashpw(pass, salt));
	}

	public static boolean verificarPassword(String pass, String storedPass) {
		if(null == storedPass || !storedPass.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Password invalido");

		return(BCrypt.checkpw(pass, storedPass));
	}

	public static void main(String[] args) {
		String test_passwd = "1234";
		String test_hash = "$2a$04$pOcYh4aKp8TC6tr0sRRl/.9H10eiI28bO6cOeTV5N7ZlKBKMlcR4q";

		System.out.println("Testing BCrypt Password hashing and verification");
		System.out.println("Test password: " + test_passwd);
		System.out.println("Test stored hash: " + test_hash);
		System.out.println("Hashing test password...");
		System.out.println();

		String computed_hash = encriptarPassword(test_passwd);
		System.out.println("Test computed hash: " + computed_hash);
		System.out.println();
		System.out.println("Verifying that hash and stored hash both match for the test password...");
		System.out.println();

		String compare_test = verificarPassword(test_passwd, test_hash)
			? "Passwords Match" : "Passwords do not match";
		String compare_computed = verificarPassword(test_passwd, computed_hash)
			? "Passwords Match" : "Passwords do not match";

		System.out.println("Verify against stored hash:   " + compare_test);
		System.out.println("Verify against computed hash: " + compare_computed);

	}

}
