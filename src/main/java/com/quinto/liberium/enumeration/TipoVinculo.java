package com.quinto.liberium.enumeration;

public enum TipoVinculo {

	CLIENTE("Cliente"), TESTIGO("Testigo"), CONTRAPARTE("Contraparte"), ABOGADO_CONTRAPARTE("Abogado Contraparte"),
	TERCERO_CIVIL_RESPONSABLE("Tercero Civil Responsable"), PERITO("Perito"), MARTILLERO("Martillero"), OTRO("Otro");

	private final String tipoVinculo;

	private TipoVinculo(String tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}

	public String getTipoVinculo() {
		return tipoVinculo;
	}
	
}
