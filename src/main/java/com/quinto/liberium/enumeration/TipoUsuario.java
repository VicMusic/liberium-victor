package com.quinto.liberium.enumeration;

public enum TipoUsuario {

	ADMINISTRADOR("Administrador"), OPERADOR("Operador"), USUARIO("Usuario");
	
	private String texto;
	
	private TipoUsuario(String texto) {
		this.texto = texto;
	}
	
	public String getTexto() {
		return texto;
	}
}
