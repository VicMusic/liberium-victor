package com.quinto.liberium.enumeration;

public enum TipoFuero {
	
	INTERNACIONAL("Internacional"),
	PROVINCIAL("Provincial"),
	FEDERAL("Federal"),
	NACIONAL("Nacional");
	
	private final String tipoFuero;

	private TipoFuero(String tipoFuero) {
		this.tipoFuero = tipoFuero;
	}
	
	public String getTipoFuero() {
		return tipoFuero;
	}
}
