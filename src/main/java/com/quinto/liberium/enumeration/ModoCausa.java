package com.quinto.liberium.enumeration;

public enum ModoCausa {
	ACTIVA("ACTIVA"),
	ARCHIVADA("ARCHIVADA");
	
	
	 private final String descripcion;
     
	    private ModoCausa(String descripcion) {
	        this.descripcion = descripcion;
	    }
	     
	    public String getDescripcion() {
	        return descripcion;
	    }
}
