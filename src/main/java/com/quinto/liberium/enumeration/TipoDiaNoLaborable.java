package com.quinto.liberium.enumeration;

public enum TipoDiaNoLaborable {
	
	FEDERAL("Federal"),
	PROVINCIAL("Provincial");
	
	private String tipoDiaNoLaborable;

	private TipoDiaNoLaborable (String tipoDiaNoLaborable) {
		this.tipoDiaNoLaborable = tipoDiaNoLaborable;
	}

	public String toString() {
		return tipoDiaNoLaborable;
	}
	
}
