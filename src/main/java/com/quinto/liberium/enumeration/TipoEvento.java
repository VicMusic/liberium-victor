package com.quinto.liberium.enumeration;

public enum TipoEvento {
	
	REUNION("Reunion"),
	AUDIENCIA("Audiencia"),
	TURNO("Turno"),
	PLAZO("Plazo"),
	FERIADO("Feriado"),
	FERIA_JUDICIAL("Feria Judicial"),
	DIA_INHABIL("Dia Inhabil"),
	VENCIMIENTO_PROCESAL("Vencimiento Procesal"),
	VENCIMIENTO_COBRO("Vencimiento Cobro"),
	OTRO("Otro");	
	
	private String tipoEvento;

	private TipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String toString() {
		return tipoEvento;
	}

}
