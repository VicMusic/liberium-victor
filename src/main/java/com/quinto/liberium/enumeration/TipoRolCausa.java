package com.quinto.liberium.enumeration;

public enum TipoRolCausa {

	ACTOR("Actor"),
	DENUNCIANTE("Denunciante"),
	QUERELLANTE("Querellante"),
	DEMANDADO("Demandado");
	
	private final String tipoRolCausa;

	private TipoRolCausa(String tipoRolCausa) {
		this.tipoRolCausa = tipoRolCausa;
	}
	
	public String getTipoRolCausa() {
		return tipoRolCausa;
	}
}
