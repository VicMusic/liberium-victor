package com.quinto.liberium.enumeration;

public enum TipoRol {

	ADMINISTRADOR,
	ABOGADO,
	USUARIO;
}
