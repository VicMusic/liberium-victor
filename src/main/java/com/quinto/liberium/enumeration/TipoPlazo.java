package com.quinto.liberium.enumeration;

public enum TipoPlazo {

	CORRIDO("Corrido"),
	DIAS_HABILES("Dias Habiles");
	
	private String tipoPlazo;

	private TipoPlazo(String tipoPlazo) {
		this.tipoPlazo = tipoPlazo;
	}

	public String toString() {
		return tipoPlazo;
	}
	
}
