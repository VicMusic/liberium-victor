package com.quinto.liberium.enumeration;

public enum TipoPersona {

	PERSONA_FISICA("Persona Fisica"), PERSONA_LEGAL("Persona Legal");

	private final String tipoPersona;

	private TipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	public String getTipoPersona() {
		return tipoPersona;
	}

}
