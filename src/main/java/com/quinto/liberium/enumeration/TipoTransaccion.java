package com.quinto.liberium.enumeration;

public enum TipoTransaccion {
	
	INGRESO, ACTIVO, GASTO, OTROGASTO, BANCOINGRESO, BANCOEGRESO, DISTRIINGRESO, DISTRIEGRESO;

}
