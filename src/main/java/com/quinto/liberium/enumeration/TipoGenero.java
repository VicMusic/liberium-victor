package com.quinto.liberium.enumeration;

public enum TipoGenero {

	HOMBRE("Hombre"), 
	MUJER("Mujer"), 
	OTRO("Otro");

	private final String tipoGenero;

	private TipoGenero(String tipoGenero) {
		this.tipoGenero = tipoGenero;
	}

	public String getTipoGenero() {
		return tipoGenero;
	}

}
