package com.quinto.liberium.enumeration;

public enum TipoBloque {
	TEXTOCOMUN("Texto descriptivo"),
	DEMANDADO("Texto para demandado"),
	DEMANDANTE("Texto para demandante");
	
	
	 private final String descripcion;
     
	    private TipoBloque(String descripcion) {
	        this.descripcion = descripcion;
	    }
	     
	    public String getDescripcion() {
	        return descripcion;
	    }
}
