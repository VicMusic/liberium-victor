package com.quinto.liberium.exception;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private HttpStatus httpStatus;

	public AppException(String mensaje, Throwable causa) {
		super(mensaje, causa);
	}

	public AppException(String mensaje) {
		super(mensaje);
	}

	public AppException(String mensaje, HttpStatus httpStatus) {
		super(mensaje);
		this.httpStatus = httpStatus;
	}

}
