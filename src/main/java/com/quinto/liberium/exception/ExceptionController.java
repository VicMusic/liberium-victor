package com.quinto.liberium.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
	
	private Log log = LogFactory.getLog(this.getClass().getSimpleName());
	 
   @ExceptionHandler(value = PeticionIncorrectaException.class)
   public ResponseEntity<Object> exception(PeticionIncorrectaException exception) {
       return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage(), exception));
   }
   
   private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
	   log.info("Ocurrio un error en: " + apiError.getMessage());
	   return new ResponseEntity<>(apiError, apiError.getStatus());
   }

   
}
