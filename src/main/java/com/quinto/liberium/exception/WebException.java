package com.quinto.liberium.exception;

public class WebException extends Exception {

	private static final long serialVersionUID = 7516231487507635004L;

	public WebException(String mensaje) {
		super(mensaje);
	}
}
