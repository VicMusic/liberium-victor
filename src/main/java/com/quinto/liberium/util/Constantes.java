package com.quinto.liberium.util;

public final class Constantes {
	
	private Constantes() {}


	public static final String PUERTO_ESCRITOS = System.getProperty("user.home") + "\\escritos_liberium\\";// WEB-INF other case 
	public static final String PUERTO_ARCHIVOS = System.getProperty("user.home") + "\\archivos_liberium\\";// WEB-INF in other case

	public static final String ESCRITO_VISTA = "escritos";
	public static final String ITEM ="item";
	public static final String SUBITEM = "subitem";
	
	public static final String TOKEN = "token";
	public static final String PLAZO = "PLAZO";

	public static final String MENSAJE_ERROR = "error";
	public static final String MENSAJE_EXITO = "exito";
	
	public static final String USUARIO_SESSION = "usuariosession";
	
	public static final String VISTA_USUARIO = "usuario";
	public static final String VISTA_PERFIL_USUARIO = "perfil-usuario";
	
	public static final String VISTA_CAUSA = "causas";
	public static final String VISTA_ARCHIVADAS = "causas-archivadas";
	public static final String VISTA_CAUSA_EDITAR = "causanueva";
	public static final String VISTA_CAUSA_VER = "vercausa";
	
	public static final String VISTA_LOGIN = "login";
	public static final String VISTA_REGISTRO = "registro";
	
	public static final String VISTA_CAMBIAR_CLAVE = "cambiar-clave";
	public static final String VISTA_OLVIDE_CLAVE = "olvide-mi-clave";

	public static final String VISTA_CUENTA_VERIFICADA = "cuenta-verificada";
	public static final String VISTA_CUENTA_ERROR = "cuenta-error";

	
	public static final String REDIRECCION_LOGIN = "redirect:/login";
	
	public static final String REDIRECCION_CAMBIAR_CLAVE = "redirect:/cambiar-clave?success";

	public static final String REDIRECCION_CONFIRMAR_MAIL = "redirect:/confirmar-mail";
	
	public static final String REDIRECCION_PERFIL_USUARIO = "redirect:/administracion/usuario/perfil";
	public static final String REDIRECCION_LISTA_USUARIO = "redirect:/administracion/usuario/listado";
	
	

}
