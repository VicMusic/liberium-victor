package com.quinto.liberium.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value = { "classpath:config.properties" })
public class Configuracion {

	@Value("${pathArchivos}")
	private String pathArchivos;

	@Value("${url}")
	private String url;

	@Value("${pathEscritos}")
	private String pathEscritos;

	public String getPathArchivos() {
		return pathArchivos;
	}

	public void setPathArchivos(String pathArchivos) {
		this.pathArchivos = pathArchivos;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPathEscritos() {
		return pathEscritos;
	}

	public void setPathEscritos(String pathEscritos) {
		this.pathEscritos = pathEscritos;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}