package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.TipoDependenciaConverter;
import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MTipoDependencia;
import com.quinto.liberium.repository.TipoDependenciaRepository;

@Service("tipoDependenciaService")
public class TipoDependenciaService extends SimpleService {

	@Autowired
	private TipoDependenciaRepository tipoDependenciaRepository;

	@Autowired
	private TipoDependenciaConverter tipoDependenciaConverter;

	

	public MTipoDependencia guardar(String id, String nombre, Usuario usuario) {
		TipoDependencia tipoDependencia = new TipoDependencia();

		if (!isStringNull(id)) {
			tipoDependencia = buscarTipoDependenciaPorId(id);
			validarTipoDependencia(nombre, tipoDependencia);
		} else {
			validarTipoDependencia(nombre, null);
		}

		tipoDependencia.setNombre(nombre);

		return tipoDependenciaConverter.entidadModelo(guardar(tipoDependencia, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private TipoDependencia guardar(TipoDependencia tipoDependencia, Usuario usuario, String accion) {
		auditar(tipoDependencia, usuario, accion);
		return tipoDependenciaRepository.save(tipoDependencia);
	}

	public MTipoDependencia eliminarTipoDependencia(String id, Usuario usuario) {
		TipoDependencia tipoDependencia = buscarTipoDependenciaPorId(id);
		if (tipoDependencia == null) {
			throw new EventoRestException(
					"El tipo de dependencia que trata eliminar no se encuentra en la base de datos.");
		}

		tipoDependencia = guardar(tipoDependencia, usuario, SimpleService.ELIMINAR);
		return tipoDependenciaConverter.entidadModelo(tipoDependencia);
	}

	private void validarTipoDependencia(String nombre, TipoDependencia tipoDependencia) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del tipo de dependencia no puede estar vacio.");
		}else {
			TipoDependencia otra = buscarTipoDependenciaPorNombre(nombre);
			if ((tipoDependencia == null && otra != null)
					|| (tipoDependencia != null && otra != null && !tipoDependencia.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public Page<TipoDependencia> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return tipoDependenciaRepository.buscarTodas(paginable);
		} else {
			return tipoDependenciaRepository.buscarTodas(paginable, "%" + q + "%");
		}
	}

	public List<MTipoDependencia> buscarTipoDependenciaModelTodos() {
		return tipoDependenciaConverter.entidadesModelos(tipoDependenciaRepository.buscarTodos());
	}

	public List<TipoDependencia> buscarTipoDependenciaTodos() {
		return tipoDependenciaRepository.buscarTodos();
	}

	public MTipoDependencia buscarTipoDependenciaModelPorId(String idMateria) {
		return tipoDependenciaConverter.entidadModelo(tipoDependenciaRepository.buscarPorId(idMateria));
	}

	public TipoDependencia buscarTipoDependenciaPorId(String idTipoDependencia) {
		return tipoDependenciaRepository.buscarPorId(idTipoDependencia);
	}

	public MTipoDependencia buscarTipoDependenciaModelPorNombre(String nombre) {
		return tipoDependenciaConverter.entidadModelo(tipoDependenciaRepository.buscarPorNombre(nombre));
	}

	public TipoDependencia buscarTipoDependenciaPorNombre(String nombre) {
		return tipoDependenciaRepository.buscarPorNombre(nombre);
	}

}
