package com.quinto.liberium.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ContabilidadCausaConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.ContabilidadCausa;
import com.quinto.liberium.entity.Honorario;
import com.quinto.liberium.entity.Interes;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MContabilidadCausa;
import com.quinto.liberium.model.MRespuesta;
import com.quinto.liberium.repository.ContabilidadCausaRepository;

@Service("contabilidadCausaService")
public class ContabilidadCausaService extends SimpleService {

	private final static String TABLA1 = "TABLA1";
	private final static String TABLA2 = "TABLA2";
	private final static String TABLA3 = "TABLA3";
	private final static String HONORARIO1 = "HONORARIO1";
	private final static String HONORARIO2 = "HONORARIO2";
	private final static String HONORARIO3 = "HONORARIO3";

	@Autowired
	private InteresService interesService;

	@Autowired
	private CausaService causaService;
	
	@Autowired
	private HonorarioService honorarioService;

	@Autowired
	private ContabilidadCausaRepository contabilidadCausaRepository;

	@Autowired
	private ContabilidadCausaConverter contabilidadCausaConverter;

	public List<MContabilidadCausa> calcularInteres(Double monto, String detalle, String idInteres,
			String stringFechaInicio, String stringFechaFin, String idCausa, String idHonorario, String posicionInteres, Usuario usuario) throws ParseException {

		if (monto == null) {
			monto = 0.0;
		}

		Causa causa = causaService.buscarPorId(idCausa);

		if (causa != null) {
			validarInteres(monto, detalle, idInteres, stringFechaInicio, stringFechaFin);
		} else {
			throw new PersistenceException("Ocurrio un error al calcular el interes!");
		}
		
		Honorario honorario = null;
		
		if (idHonorario != "SIN_HONORARIO") {
			honorario = honorarioService.buscarHonorarioPorId(idHonorario);
		}

		List<MContabilidadCausa> contabilidadesCausa = new ArrayList<>();
		DateTime fechaInicioJoda = parseDateToJoda(stringFechaInicio);
		DateTime fechaFinJoda = parseDateToJoda(stringFechaFin);
		Integer cantDias = 0;
		DateTime fechaActual = fechaInicioJoda;

		while (!fechaActual.equals(fechaFinJoda)) {
			fechaActual = fechaActual.plusDays(1);
			cantDias++;
		}

		Interes interes = interesService.buscarInteresPorId(idInteres);
		Double porcentajeInteres = null;
		if (interes == null) {
			porcentajeInteres = (0.0 / 365) * cantDias;
		} else {
			porcentajeInteres = (interes.getPorcentaje() / 365) * cantDias;
		}
		
		porcentajeInteres = redondearDecimales(porcentajeInteres, 2);

		Double montoASumar = monto * porcentajeInteres / 100;
		Double montoTotal = monto + montoASumar;
		
		ContabilidadCausa contabilidadCausa = new ContabilidadCausa();

		contabilidadCausa.setCausa(causa);
		contabilidadCausa.setMontoCI(montoTotal);
		contabilidadCausa.setMontoSI(monto);
		contabilidadCausa.setDesde(fechaInicioJoda.toDate());
		contabilidadCausa.setHasta(fechaFinJoda.toDate());
		contabilidadCausa.setDetalle(detalle);
		contabilidadCausa.setTipoDeTasa("Con Interés");
		contabilidadCausa.setTipoDePago("Un solo pago");
		contabilidadCausa.setuHtml(TABLA2);
		
		if (interes == null) {
			contabilidadCausa.setTipoDeTasa("Sin Interes");
		}
		
		if (honorario != null) {
			contabilidadCausa.setHonorario(honorario);
			if (posicionInteres.equals(HONORARIO1)) {
				contabilidadCausa.setuHtml(HONORARIO1);
			} else {
				contabilidadCausa.setuHtml(HONORARIO3);
			}
		}
		
		if (posicionInteres.equals(TABLA1)) {
			contabilidadCausa.setuHtml(TABLA1);
		}

		contabilidadCausa = guardar(contabilidadCausa, usuario, ACTUALIZAR);

		contabilidadesCausa.add(contabilidadCausaConverter.entidadModelo(contabilidadCausa));

		return contabilidadesCausa;
	}

	public MContabilidadCausa crearGasto(String idCausa, Double monto, String stringFechaGasto, String idInteres,
			String stringFechaInicio, String stringFechaFin, String detalle, String nombre, Usuario usuario)
			throws ParseException {

		if (monto == null) {
			monto = 0.0;
		}

		Causa causa = causaService.buscarPorId(idCausa);

		if (causa != null) {
			validarGasto(monto, stringFechaGasto, detalle, nombre);
		} else {
			throw new PeticionIncorrectaException("Ocurrio un erro al crear el gasto!");
		}

		ContabilidadCausa contabilidadCausa = new ContabilidadCausa();

		DateTime fechaGasto = parseDateToJoda(stringFechaGasto);
		Interes interes = interesService.buscarInteresPorId(idInteres);
		
		contabilidadCausa.setCausa(causa);
		contabilidadCausa.setVencimiento(fechaGasto.toDate());
		contabilidadCausa.setDetalle(detalle);
		contabilidadCausa.setuHtml(TABLA3);
		contabilidadCausa.setTipoDePago(nombre);
		contabilidadCausa.setMontoSI(monto);
		
		if (interes != null) {
			
			DateTime fechaInicioJoda = parseDateToJoda(stringFechaInicio);
			DateTime fechaFinJoda = parseDateToJoda(stringFechaFin);
			
			Integer cantDias = 0;
			DateTime fechaActual = fechaInicioJoda;

			while (!fechaActual.equals(fechaFinJoda)) {
				fechaActual = fechaActual.plusDays(1);
				cantDias++;
			}
			
			Double porcentajeInteres = (interes.getPorcentaje() / 365) * cantDias;
			porcentajeInteres = redondearDecimales(porcentajeInteres, 2);

			Double montoASumar = monto * porcentajeInteres / 100;
			Double montoConInteres = monto + montoASumar;
			
			
			contabilidadCausa.setDesde(fechaInicioJoda.toDate());
			contabilidadCausa.setHasta(fechaFinJoda.toDate());
			contabilidadCausa.setMontoCI(montoConInteres);
			contabilidadCausa.setTipoDeTasa("Con interes");
			
			
		} else if (idInteres.equals("SIN_INTERES")) {
			
			contabilidadCausa.setTipoDeTasa("Sin Interes");
			contabilidadCausa.setMontoCI(monto);
			
		}
		
		return contabilidadCausaConverter.entidadModelo(guardar(contabilidadCausa, usuario, ACTUALIZAR));
	}
	
	public MContabilidadCausa crearCobro(String idCausa, String idHonorario, String nombre, Double monto, String fecha, Usuario usuario) throws ParseException {
		
		if (monto == null) {
			monto = 0.0;
		}
		
		validarCobro(nombre, monto, fecha);
		
		DateTime fechaDesde = parseDateToJoda(fecha);
		Causa causa = causaService.buscarPorId(idCausa);
		Honorario honorario = honorarioService.buscarHonorarioPorId(idHonorario);
		
		ContabilidadCausa contabilidadCausa = new ContabilidadCausa();
		
		contabilidadCausa.setDetalle(nombre);
		contabilidadCausa.setMontoSI(monto);
		contabilidadCausa.setDesde(fechaDesde.toDate());
		contabilidadCausa.setCausa(causa);
		contabilidadCausa.setHonorario(honorario);
		contabilidadCausa.setuHtml(HONORARIO2);
		
		return contabilidadCausaConverter.entidadModelo(guardar(contabilidadCausa, usuario, ACTUALIZAR));
	}

	public MRespuesta eliminar(String idContable, Usuario usuario) {
		MRespuesta respuesta = new MRespuesta();

		ContabilidadCausa contabilidadCausa = buscarPorId(idContable);

		if (contabilidadCausa != null) {
			guardar(contabilidadCausa, usuario, ELIMINAR);
			respuesta.setMensaje("Se elimino con exito!");
		} else {
			respuesta.setError("Ocurrio un error al eliminar el item contable!");
		}
		return respuesta;
	}
	
	public MContabilidadCausa editarVencimiento(String idContable, String fechaVencimiento, Usuario usuario) throws ParseException {
		
		if (isStringNull(fechaVencimiento)) {
			throw new PeticionIncorrectaException("La fecha de vencimiento no puede estar vacia!");
		}
		
		ContabilidadCausa contabilidadCausa = buscarPorId(idContable);
		
		if (contabilidadCausa != null) {
			
			DateTime fechaVencimientoJoda = parseDateToJoda(fechaVencimiento);

			contabilidadCausa.setVencimiento(fechaVencimientoJoda.toDate());
		} else {
			throw new PeticionIncorrectaException("Ocurrio un error al guardar la fecha!");
		}
		
		return contabilidadCausaConverter.entidadModelo(guardar(contabilidadCausa, usuario, ACTUALIZAR));
		
	}

	@Transactional
	private ContabilidadCausa guardar(ContabilidadCausa contabilidadCausa, Usuario usuario, String accion) {
		auditar(contabilidadCausa, usuario, accion);
		return contabilidadCausaRepository.save(contabilidadCausa);
	}

	public void validarInteres(Double monto, String detalle, String idInteres, String fechaInicio,
			String fechaFin) {
		
		if (monto == 0.0) {
			throw new PeticionIncorrectaException("El monto de el calculo no puede estar vacio!");
		}
		if (isStringNull(idInteres)) {
			throw new PeticionIncorrectaException("El interes de el calculo no puede estar vacio!");
		}
		if (isStringNull(fechaInicio)) {
			throw new PeticionIncorrectaException("La fecha de inicio de el calculo no puede estar vacia!");
		}
		if (isStringNull(fechaFin)) {
			throw new PeticionIncorrectaException("La fecha de fin de el calculo no puede estar vacia!");
		}
		DateTime fechaFinal = new DateTime(fechaInicio);
		Integer fecha1 = fechaFinal.getYear();
		if (fecha1.toString().length() >= 5) {
			throw new PeticionIncorrectaException("El año de la fecha es incorrecta!");
		}
		DateTime fechaFinal2 = new DateTime(fechaFin);
		Integer fecha2 = fechaFinal2.getYear();
		if (fecha2.toString().length() >= 5) {
			throw new PeticionIncorrectaException("El año de la fecha es incorrecta!");
		}
		if (isStringNull(detalle)) {
			throw new PeticionIncorrectaException("El detalle no puede estar vacio!");
		}
		if (limiteCaracteres(detalle)) {
			throw new PeticionIncorrectaException("El detalle excede el limite de caracteres!");
		}
	}
	
	public void validarCobro(String nombre, Double monto, String fecha) {

		if (monto == 0.0) {
			throw new PeticionIncorrectaException("El monto del cobro no puede estar vacio!");
		}
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El concepto del cobro no puede estar vacio!");
		}
		if (limiteCaracteres(nombre)) {
			throw new PeticionIncorrectaException("El detalle excede el limite de caracteres!");
		}
		if (isStringNull(fecha)) {
			throw new PeticionIncorrectaException("La fecha del cobro no puede estar vacia!");
		}
		DateTime fechaFinal = new DateTime(fecha);
		Integer fecha1 = fechaFinal.getYear();
		if (fecha1.toString().length() >= 5) {
			throw new PeticionIncorrectaException("El año de la fecha es incorrecta!");
		}
	}


	public void validarGasto(Double monto, String stringFechaGasto, String detalle, String nombre) {

		if (monto == 0.0) {
			throw new PeticionIncorrectaException("El monto del gasto no puede estar vacio!");
		}
		if (isStringNull(stringFechaGasto)) {
			throw new PeticionIncorrectaException("La fecha del gasto no puede estar vacia!");
		}
		if (isStringNull(detalle)) {
			throw new PeticionIncorrectaException("El detalle de el gasto no puede estar vacio!");
		}
		if (limiteCaracteres(detalle)) {
			throw new PeticionIncorrectaException("El detalle excede el limite de caracteres!");
		}
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la persona a cargo de el pago no puede estar vacio!");
		}
		if (limiteCaracteres(nombre)) {
			throw new PeticionIncorrectaException("El nombre excede el limite de caracteres!");
		}
	}

	private DateTime parseDateToJoda(String fecha) throws ParseException {
		String pattern = "yyyy-MM-dd";
		DateFormat df = new SimpleDateFormat(pattern);
		Date fechaDate = null;
		fechaDate = df.parse(fecha);
		return new DateTime(fechaDate);
	}

	public static double redondearDecimales(double valorInicial, int numeroDecimales) {
		double parteEntera, resultado;
		resultado = valorInicial;
		parteEntera = Math.floor(resultado);
		resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
		resultado = Math.ceil(resultado);
		resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
		return resultado;
	}

	public static double redondear(double valorInicial, int numeroDecimales) {
		double parteEntera, resultado;
		resultado = valorInicial;
		parteEntera = Math.floor(resultado);
		resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
		resultado = Math.round(resultado);
		resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
		return resultado;
	}

	public ContabilidadCausa buscarPorId(String id) {
		return contabilidadCausaRepository.buscarContabilidadCausaPorId(id);
	}
	
	public List<MContabilidadCausa> buscarContabilidadesPorHonorario(String idHonorario){
		return contabilidadCausaConverter.entidadesModelos(contabilidadCausaRepository.buscarContabilidadesPorHonorario(idHonorario));
	}

	public List<MContabilidadCausa> buscarContabilidadPorCausa(String idCausa) {
		return contabilidadCausaConverter
				.entidadesModelos(contabilidadCausaRepository.buscarContabilidadPorCausa(idCausa));
	}

}
