package com.quinto.liberium.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.CausaVinculadaConverter;
import com.quinto.liberium.entity.CausaVinculada;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MCausaVinculada;
import com.quinto.liberium.repository.CausaVinculadaRepository;

@Service("causaVinculadaService")
public class CausaVinculadaService {

	private static final String MENSAJE_ERROR = "ERROR";
	private static final String ACCION_ELIMINAR = "eliminar";
	private static final String ACCION_AGREGAR = "agregar";
	private static final String ACCION_MODIFICAR = "modificar";

	@Autowired
	private CausaVinculadaConverter causaVinculadaConverter;


	@Autowired
	private CausaVinculadaRepository causaVinculadaRepository;

	public CausaVinculada crear(MCausaVinculada mCausaVinculada, Usuario creador) {
		if (mCausaVinculada == null || creador == null) {
			throw new PeticionIncorrectaException(MENSAJE_ERROR);
		}
		CausaVinculada causaVinculada = causaVinculadaConverter.modeloEntidad(mCausaVinculada);
		return guardar(causaVinculada, creador, ACCION_AGREGAR);
	}

	public CausaVinculada editar(MCausaVinculada mCausaVinculada, Usuario editor) {
		if (mCausaVinculada == null || editor == null) {
			throw new PeticionIncorrectaException(MENSAJE_ERROR);
		}
		CausaVinculada causaVinculada = causaVinculadaConverter.modeloEntidad(mCausaVinculada);
		return guardar(causaVinculada, editor, ACCION_MODIFICAR);
	}

	public void eliminar(String causaVinculadaId, Usuario eliminador) {
		if (causaVinculadaId.isEmpty() || eliminador == null) {
			throw new PeticionIncorrectaException(MENSAJE_ERROR);
		}
		CausaVinculada causaVinculada = causaVinculadaRepository.buscarPorId(causaVinculadaId);
		guardar(causaVinculada, eliminador, ACCION_ELIMINAR);
	}

	private CausaVinculada guardar(CausaVinculada causaVinculada, Usuario interviniente, String accion) {
		return causaVinculadaRepository.save(causaVinculada);
	}

	public List<CausaVinculada> listarCausa() {
		return causaVinculadaRepository.findAll();
	}

	public CausaVinculada buscarPorId(String causaId) {
		return causaVinculadaRepository.buscarPorId(causaId);
	}
	
	public List<CausaVinculada> buscarPorCausa(String idCausa){
		return causaVinculadaRepository.buscarPorCausa(idCausa);
	}
	
	public List<CausaVinculada> buscarPorCausaVinculada(String idCausaVinculada) {
		return causaVinculadaRepository.buscarPorCausaVinculada(idCausaVinculada);
	}


}