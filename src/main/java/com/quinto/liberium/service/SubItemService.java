package com.quinto.liberium.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.quinto.liberium.converter.SubItemConverter;
import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.repository.SubItemRepository;

@Service
public class SubItemService extends SimpleService {

	@Autowired
	private SubItemRepository subItemRepository;

	@Autowired
	private SubItemConverter subItemConverter;

	@Autowired
	private CausaService causaService;

	@Autowired
	private ModificacionService modificacionService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private EscritoService escritoService;

	@Autowired
	private ArchivoService archivoService;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class,
			Exception.class })
	public List<SubItem> generarSubItem(HttpServletRequest request, String ordenStr, Usuario usuario)
			throws WebException {
		List<String> keys = request.getParameterMap().keySet().stream()
				.filter(key1 -> key1.startsWith("opcion-" + ordenStr)).map((key1 -> key1.split("-")[2])).sorted()
				.collect(Collectors.toList());

		int subItemOrden = 0;
		List<SubItem> subItems = new ArrayList<>();
		for (String subItemKey : keys) {
			SubItem subItem = null;
			String idSubItem = request.getParameter("idSubItem-" + ordenStr + "-" + subItemKey);
			String nombreSubItem = request.getParameter("opcion-" + ordenStr + "-" + subItemKey);
			String limite = request.getParameter("limite-" + ordenStr + "-" + subItemKey);
			String habil = request.getParameter("habil-" + ordenStr + "-" + subItemKey);
			if (!validar(nombreSubItem)) {
				List<Archivo> archivos = new ArrayList<Archivo>();
				if (idSubItem != null && !idSubItem.isEmpty()) {
					subItem = buscarSubItemPorId(idSubItem);
					if (subItem.getArchivos() != null) {
						archivos = subItem.getArchivos();
					}
				} else {
					subItem = new SubItem();
					String idItemClonado = request.getParameter("idItemClonado-" + ordenStr + "-" + subItemKey);
					if (!isStringNull(idItemClonado)) {
						try {
							archivos = archivoService.clonarArchivos(idItemClonado, archivos, "SubItem", usuario);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				subItem.setNombre(nombreSubItem);
				subItem.setOrden(++subItemOrden);

				archivos = archivoService.obtenerArchivosChecklist(request, ordenStr + "-" + subItemKey, archivos,
						usuario);
				subItem.setArchivos(archivos);

				List<Escrito> escritos = escritoService.obternerEscritosChecklist(request, ordenStr + "-" + subItemKey);
				subItem.setEscritos(escritos);

				if (limite != null && !limite.isEmpty()) {
					subItem.setLimite(new Integer(limite));
					subItem.setHabil(habil != null && !habil.isEmpty() && habil.equals("true") ? true : false);
				}

				subItem = guardar(subItem, usuario, ACTUALIZAR);
				subItems.add(subItem);
			}
		}

//		if(subItems.isEmpty()) {
//			SubItem subItem = new SubItem();
//			subItem.setNombre("Sub Paso 1");
//			subItem.setOrden(1);
//			subItem = guardar(subItem, usuario, ACTUALIZAR);
//			subItems.add(subItem);
//		}
		return subItems;
	}

	private boolean validar(String nombreSubItem) {
		if (nombreSubItem == null || nombreSubItem.isEmpty()) {
			throw new PeticionIncorrectaException("El titulo del sub paso no puede estar vacío.");
		}
		return false;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class,
			Exception.class })
	public List<SubItem> clonar(List<SubItem> subItems, Usuario creador) throws IOException {
		List<SubItem> subItemsNuevo = new ArrayList<>();
		for (SubItem subItem : subItems) {
			SubItem subItemCopia = new SubItem();
			subItemCopia.setNombre(subItem.getNombre());
			subItemCopia.setOrden(subItem.getOrden());
			if (subItem.getArchivos() != null && subItem.getArchivos().size() > 0) {
				List<Archivo> archivos = new ArrayList<>();
				archivos = archivoService.clonarArchivos(subItem.getId(), archivos, "SubItem", creador);
				subItemCopia.setArchivos(archivos);
			}

			if (subItem.getEscritos() != null && subItem.getEscritos().size() > 0) {
				subItemCopia.setEscritos(new ArrayList<>(subItem.getEscritos()));
			}
			subItemCopia = guardar(subItemCopia, creador, "ACTUALIZAR");

			subItemsNuevo.add(subItemCopia);
		}

		return subItemsNuevo;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class,
			Exception.class })
	public SubItem guardar(SubItem subItem, Usuario usuario, String accion) {
		auditar(subItem, usuario, accion);
		return subItemRepository.save(subItem);
	}

	public MSubItem eliminarSubItem(String id, Usuario usuario) {
		SubItem subItem = buscarSubItemPorId(id);
		if (subItem == null) {
			throw new EventoRestException("El sub item que trata eliminar no se encuentra en la base de datos.");
		}

		subItem = guardar(subItem, usuario, SimpleService.ELIMINAR);
		return subItemConverter.entidadModelo(subItem);
	}

	public MSubItem checkearSubItem(String id, String idItem, String idCausa, Usuario usuario) {
		Item item = itemService.buscarItemPorId(idItem);
		Causa causa = causaService.buscarPorId(idCausa);
		SubItem subItem = buscarSubItemPorId(id);
		if (!subItem.isEstado()) {
			subItem.setEstado(true);
			modificacionService.guardarModificacion(
					"Se completó el sub paso " + subItem.getOrden() + " del item " + item.getOrden(), causa, usuario);
		} else {
			subItem.setEstado(false);
		}

		subItemRepository.save(subItem);
		return subItemConverter.entidadModelo(subItem);

	}

	public void guardarArchivos(SubItem subItem, List<Archivo> archivos) {
		subItem.setArchivos(archivos);
		subItemRepository.save(subItem);
	}

	public void guardarEscrito(SubItem subItem, String idEscrito) {
		Escrito escrito = escritoService.buscarEscritoPorId(idEscrito);
		List<Escrito> escritos = subItem.getEscritos() != null ? subItem.getEscritos() : new ArrayList<>();
		escritos.add(escrito);
		subItem.setEscritos(escritos);
		subItemRepository.save(subItem);
	}

//
//	private void validarItem(String nombre, String idItem, SubItem subItem) {
//		if (isStringNull(idItem)) {
//			throw new PeticionIncorrectaException("El item al que pertenece el sub item no puede ser nulo.");
//		} else {
//			SubItem otro = buscarSubItemPorNombre(nombre);
//			if ((subItem == null && otro != null) || (subItem != null && otro != null && !subItem.getId().equals(otro.getId()))) {
//				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
//			}
//		}
//
//	}
//
	public SubItem buscarSubItemPorId(String id) {
		return subItemRepository.buscarPorId(id);
	}

//	
//	public SubItem buscarSubItemPorNombre(String nombre) {
//		return subItemRepository.buscarPorNombre(nombre);
//	}
//	
//	public List<SubItem> buscarSubItemPorItemId(String idItem) {
//		return subItemRepository.buscarPorItemId(idItem);
//	}
//	
//	public List<MSubItem> buscarModelSubItemPorItemId(String idItem) {
//		return subItemConverter.entidadesModelos(subItemRepository.buscarPorItemId(idItem));
//	}
//	
//	public Page<SubItem> buscarTodos(Pageable paginable){
//		return subItemRepository.buscarTodos(paginable);
//	}
//

	public void eliminar(List<SubItem> subItems, Usuario usuario) {
		for (SubItem subItem : subItems) {
			guardar(subItem, usuario, ELIMINAR);
		}
	}
}
