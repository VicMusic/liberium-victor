package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.PaisConverter;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.repository.PaisRepository;

@Service("paisService")
public class PaisService extends SimpleService {

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private PaisConverter paisConverter;

	public MPais guardar(String id, String nombre, Usuario creador) {
		Pais pais = new Pais(); 
		if(!isStringNull(id)) {
			pais = paisRepository.buscarPorId(id);
		}
		
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del país no puede ser nulo.");
		}else {
			Pais otro = buscarPaisPorNombre(nombre);
			if(otro != null) {
				throw new PeticionIncorrectaException("El pais ingresado ya se encuentra utilizado.");
			}
		}

		pais.setNombre(nombre);
		
		return paisConverter.entidadModelo(guardar(pais, creador, ACTUALIZAR));
	}

	public MPais eliminar(String idPais, Usuario eliminador) {
		Pais pais = paisRepository.buscarPorId(idPais);
		return paisConverter.entidadModelo(guardar(pais, eliminador, ELIMINAR));
	}

	@Transactional
	private Pais guardar(Pais pais, Usuario interviniente, String accion) {
		auditar(pais, interviniente, accion);
		return paisRepository.save(pais);
	}

	public Page<Pais> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return paisRepository.buscarTodos(paginable);
		} else {
			return paisRepository.buscarTodos(paginable, "%" + q + "%");
		}
		
	}

	public MPais buscarPaisModelPorId(String idPais) {
		return paisConverter.entidadModelo(paisRepository.buscarPorId(idPais));
	}

	public Pais buscarPaisPorId(String idPais) {
		return paisRepository.buscarPorId(idPais);
	}

	public MPais buscarPaisModelPorNombre(String nombre) {
		return paisConverter.entidadModelo(paisRepository.buscarPorNombre(nombre));
	}

	public Pais buscarPaisPorNombre(String nombre) {
		return paisRepository.buscarPorNombre(nombre);
	}

}