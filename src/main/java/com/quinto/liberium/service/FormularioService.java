package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.FormularioConverter;
import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Respuesta;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MFormulario;
import com.quinto.liberium.model.MPregutasYRespuestas;
import com.quinto.liberium.repository.FormularioRepository;
import com.quinto.liberium.repository.TipoCausaRepository;

@Service("formularioService")
public class FormularioService extends SimpleService {

	@Autowired
	private FormularioRepository formularioRepository;

	@Autowired
	private FormularioConverter formularioConverter;

	@Autowired
	private TipoCausaRepository tipoCausaRepository;
	
	@Autowired
	private PreguntaService preguntaService;
	
	@Autowired
	private RespuestaService respuestaService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private ProvinciaService provinciaService;
	
	public MFormulario guardar(String id, String nombre, String idTipo, String idPais, String idProvincia, Usuario creador) {
		
		validarFromulario(nombre, idTipo, idPais, idProvincia);
		
		Formulario formulario = new Formulario();
		
		if (!isStringNull(id)) {
			formulario = formularioRepository.buscarPorId(id);
		}

		TipoCausa tipoCausa = tipoCausaRepository.buscarPorId(idTipo);

		if (isStringNull(id)) {
			Formulario formularioExistente = buscarFormularioPorTipoDeCausa(idTipo);
			if (formularioExistente != null) {
				throw new PeticionIncorrectaException("Ya existe un formulario creado para el tipo de causa: " + tipoCausa.getNombre());
			}
		}
		
		formulario.setTipo(tipoCausa);
		
		Pais pais = paisService.buscarPaisPorId(idPais);
		formulario.setPais(pais);
		
		Provincia provincia = provinciaService.buscarProvinciaPorId(idProvincia);
		formulario.setProvincia(provincia);
		
		formulario.setTitulo(nombre);
		
		return formularioConverter.entidadModelo(guardar(formulario, creador, ACTUALIZAR));
	}

	public MFormulario eliminar(String idFormulario, Usuario eliminador) {
		Formulario formulario = formularioRepository.buscarPorId(idFormulario);
		return formularioConverter.entidadModelo(guardar(formulario, eliminador, ELIMINAR));
	}

	@Transactional
	private Formulario guardar(Formulario formulario, Usuario interviniente, String accion) {
		auditar(formulario, interviniente, accion);
		return formularioRepository.save(formulario);
	}


	public Page<Formulario> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return formularioRepository.buscarTodos(paginable);
		} else {
			return formularioRepository.buscarTodos(paginable, "%" + q + "%");
		}

	}
	
	public MPregutasYRespuestas formarFormulario(String idFormulario, String idConsulta) {
				
		MPregutasYRespuestas pregutasYRespuestas = new MPregutasYRespuestas();
		
		List<Respuesta> respuestas = respuestaService.buscarRespuestaPorIdConsulta2(idConsulta);
		List<Pregunta> preguntas = preguntaService.buscarPorFormulario(idFormulario);
		
		pregutasYRespuestas.setPreguntas(preguntas);
		pregutasYRespuestas.setRespuestas(respuestas);
		
		return pregutasYRespuestas;
	}
	
	private void validarFromulario(String nombre, String idTipo, String idPais, String idProvincia) {
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del formulario no puede estar vacio.");
		}
		if (isStringNull(idTipo)) {
			throw new PeticionIncorrectaException("El tipo de causa del formulario no puede estar vacia.");
		}
		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El pais del formulario no puede estar vacio.");
		}
		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException("La provincia del formulario no puede estar vacia.");
		}
	}
	
	public Formulario buscarFormularioPorTipoDeCausa(String idTipoCausa) {
		Formulario formulario = formularioRepository.buscarPorTipoDeCausa(idTipoCausa);
		return formulario;
	}
	
	public MFormulario buscarMFormularioPorTipoDeCausa(String idTipoCausa) {
		return formularioConverter.entidadModelo( formularioRepository.buscarPorTipoDeCausa(idTipoCausa));
	}

	public MFormulario buscarFormularioModelPorId(String idFormulario) {
		return formularioConverter.entidadModelo(formularioRepository.buscarPorId(idFormulario));
	}

	public Formulario buscarFormularioPorId(String idFormulario) {
		return formularioRepository.buscarPorId(idFormulario);
	}

	public MFormulario buscarFormularioModelPorNombre(String nombre) {
		return formularioConverter.entidadModelo(formularioRepository.buscarPorNombre(nombre));
	}

	public Formulario buscarFormularioPorNombre(String nombre) {
		return formularioRepository.buscarPorNombre(nombre);
	}

}