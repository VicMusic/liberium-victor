package com.quinto.liberium.service;

import java.nio.charset.StandardCharsets;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.util.Configuracion;

@Service
public class EmailService {

	private static final Logger log = LoggerFactory.getLogger(EmailService.class);
	
	@Autowired
	private Configuracion configuracion;
	
	private final String EMAIL_LIBERIUM = "consultas@liberium.la";
	private final String LIBERIUM = "Liberium";
	private final String URL = "url";
	private final String NOMBRE = "nombre";
	private final String CONTACTO = "contacto";
	private final String ESTUDIO = "estudio";
	private final String ROL = "rol";
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private NotificacionService notificacionService;
	
	@Async
	public void resetearClave(Usuario usuario) {
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			contexto.setVariable(NOMBRE, usuario.getNombre());
			contexto.setVariable(URL, configuracion.getUrl() + "cambiar-clave?token=" + usuario.getResetToken());
			contexto.setVariable(CONTACTO, EMAIL_LIBERIUM);
			
			String plantilla = templateEngine.process("mails/reset-contrasena.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(usuario.getEmail());
			helper.setSubject("Recuperación de contraseña de tu cuenta Liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Async
	public void mandarMailDeConfirmacion(Usuario usuario) {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable(NOMBRE, usuario.getNombre());
			contexto.setVariable(URL, configuracion.getUrl() + "mail-confirmado?token=" + usuario.getConfirmToken());
			contexto.setVariable(CONTACTO, EMAIL_LIBERIUM);
			
			String plantilla = templateEngine.process("mails/registro-nuevo.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(usuario.getEmail());
			helper.setSubject("Registro de nuevo usuario Liberium");
			helper.setText(plantilla, true);
			System.out.println("EL MAIL tenemos que enviar al mail " + usuario.getEmail());
			mailSender.send(message);
			System.out.println("EL MAIL DE NUEVO MIEMBRO SALIÓ CON ÉXITO");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Async
	public void mandarMailDeNuevoCliente(MMiembro miembro, MCausa causa) throws MailException {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable(NOMBRE, miembro.getmUsuario().getEmail());
			contexto.setVariable(ESTUDIO, miembro.getmEstudio().getNombre());
			contexto.setVariable(URL, configuracion.getUrl() + "administracion/causa/cliente/" + causa.getId());
			
			String plantilla = templateEngine.process("mails/miembro-nuevo-estudio.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(miembro.getmUsuario().getEmail());
			helper.setSubject("Te han vinculado a un estudio de liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Async
	public void mandarMailDeNuevoMiembro(Miembro miembro) throws MailException {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			if (miembro.getUsuario().getNombre() != null) {
				contexto.setVariable(NOMBRE, miembro.getUsuario().getNombre());
			}
			
			contexto.setVariable(ROL, miembro.getRol().getNombre());
			contexto.setVariable(ESTUDIO, miembro.getEstudio().getNombre());
			contexto.setVariable(URL, configuracion.getUrl() + "login");
			
			String plantilla = templateEngine.process("mails/miembro-nuevo-estudio.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(miembro.getUsuario().getEmail());
			helper.setSubject("Te han vinculado a un estudio de liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Async
	public void mailTurnoClienteYAbogado(Turno turno) {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! " + turno.getAbogado().getUsuario().getNombre() + 
				" se ha registrado un turno a su nombre en liberium, el mismo ya fue ingreado a su agenda." +
				"La informacion de el turno es la siguiente: " 
			+ "Su turno es el " + turno.getFecha() + " a las " + turno.getHora() +" en " + turno.getAbogado().getDomicilio());
			
			contexto.setVariable(URL, configuracion.getUrl() + "cancelar-turno-abogado?tokenCancelado="+ turno.getTokenCancelado());
			
			String plantilla = templateEngine.process("mails/nuevo-turno.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getAbogado().getUsuario().getEmail());
			helper.setSubject("Tienes agendado un nuevo turno de un/a cliente de liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! se ha registrado un turno a su nombre en liberium, la informacion de el turno es la siguiente: " 
				+ "Su turno es el " + turno.getFecha() + " a las " + turno.getHora() +" "
				+ "hs con el abogado " + turno.getAbogado().getUsuario().getNombre()
				+ " en " + turno.getAbogado().getDomicilio());
			contexto.setVariable(URL, configuracion.getUrl() + "cancelar-turno-cliente?tokenCancelado="+ turno.getTokenCancelado());
			
			String plantilla = templateEngine.process("mails/nuevo-turno.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getMailCliente());
			helper.setSubject("Tienes un turno con un abogado de liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Async
	public void cancelarTurnoAbogado(Turno turno) {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! nuestro usuario " + turno.getAbogado().getUsuario().getNombre() 
					+ " te informa que lamentablemente no podrá concurrir a la cita fijada para el" + turno.getFecha() 
					+ " a las " + turno.getHora() +" en " + turno.getAbogado().getDomicilio());
			
			String plantilla = templateEngine.process("mails/turno-cancelado.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getMailCliente());
			helper.setSubject("Tu turno con el/la " + turno.getAbogado().getUsuario().getNombre() + " de Liberium ha sido cancelado");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Async
	public void cancelarTurnoCliente(Turno turno) {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! el cliente " + turno.getMailCliente()
					+ " te informa que lamentablemente no podrá concurrir a la cita fijada para el" + turno.getFecha() 
					+ " a las " + turno.getHora() +" en " + turno.getAbogado().getDomicilio());
			
			String plantilla = templateEngine.process("mails/turno-cancelado.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getAbogado().getUsuario().getEmail());
			helper.setSubject("Tu turno con un cliente de Liberium ha sido cancelado");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Async
	public void mandarMailDiaAntes(Turno turno) {
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! " + turno.getAbogado().getUsuario().getNombre() + 
				" recuerda que tienes un turno pronto, el mismo ya fue ingreado a su agenda." +
				"La informacion de el turno es la siguiente: " 
			+ "Su turno es el " + turno.getFecha() + " a las " + turno.getHora() +" en " + turno.getAbogado().getDomicilio());
			
			contexto.setVariable(URL, configuracion.getUrl() + "cancelar-turno-abogado?tokenCancelado="+ turno.getTokenCancelado());
			
			String plantilla = templateEngine.process("mails/nuevo-turno.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getAbogado().getUsuario().getEmail());
			helper.setSubject("Recuerda que tienes un turno agendado con un/a cliente de Liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", "Buenos Dias! recuerda que tienes un turno pronto, la informacion de el turno es la siguiente: " 
				+ "Su turno es el " + turno.getFecha() + " a las " + turno.getHora() +" "
				+ "hs con el abogado " + turno.getAbogado().getUsuario().getNombre()
				+ " en " + turno.getAbogado().getDomicilio());
			contexto.setVariable(URL, configuracion.getUrl() + "cancelar-turno-cliente?tokenCancelado="+ turno.getTokenCancelado());
			
			String plantilla = templateEngine.process("mails/nuevo-turno.html", contexto);
			
			helper.setFrom(EMAIL_LIBERIUM, LIBERIUM);
			helper.setTo(turno.getMailCliente());
			helper.setSubject("Recuerda que tienes un turno agendado con un/a abogado/a de Liberium");
			helper.setText(plantilla, true);
			
			mailSender.send(message);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
	}
	
	@Async
	public void enviarMailSoporte(String email, String asunto, String texto) {
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			Context contexto = new Context();
			
			contexto.setVariable("texto", texto);
			
			String plantilla = templateEngine.process("mails/soporte.html", contexto);
			
			helper.setFrom(email);
			helper.setTo(EMAIL_LIBERIUM);
			helper.setSubject("Soporte Liberium | " + asunto + " Usuario: " + email);
			helper.setText(plantilla, true);
			
			mailSender.send(message);
			notificacionService.notificarConsuta();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
	}
}
