package com.quinto.liberium.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.quinto.liberium.converter.CausaConverter;
import com.quinto.liberium.converter.EstudioConverter;
import com.quinto.liberium.converter.ItemConverter;
import com.quinto.liberium.converter.SubItemConverter;
import com.quinto.liberium.converter.UsuarioConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Modificacion;
import com.quinto.liberium.entity.Notas;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.ModoCausa;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MEstudio;
import com.quinto.liberium.model.MInstancia;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.model.MNotas;
import com.quinto.liberium.model.MPais;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.model.MTipoCausa;
import com.quinto.liberium.model.MUsuario;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.repository.ItemRepository;
import com.quinto.liberium.repository.ModificacionRepository;
import com.quinto.liberium.repository.NotasRepository;
import com.quinto.liberium.repository.SubItemRepository;

@Service("causaService")
public class CausaService extends SimpleService {

	@Autowired
	private CausaConverter causaConverter;

	@Autowired
	private CausaRepository causaRepository;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private SubItemRepository subItemRepository;

	@Autowired
	private ItemService itemService;

	@Autowired
	private ChecklistService checklistService;

	@Autowired
	private ModificacionService modificacionService;

	@Autowired
	private ModificacionRepository modificacionRepository;

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private ItemConverter itemConverter;

	@Autowired
	private SubItemConverter subItemConverter;

	@Autowired
	private EstadisticaService estadisticaService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private EstudioService estudioService;

	@Autowired
	private EstudioConverter estudioConverter;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private JuzgadoService juzgadoService;

	@Autowired
	private UsuarioConverter usuarioConverter;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private NotasRepository notasRepository;

	public Locale locale = Locale.getDefault();
	public Calendar fecha = Calendar.getInstance();

	public MCausa crearCausa(String id, String actor, String nombre, String demandado, String nroExpediente,
			String tipoCausaId, String instanciaId, String idJuzgado, String idConsulta, String rolCausa,
			String tipoFuero, String idPais, String idProvincia, Estudio estudio, Usuario usuario) {

		validarCaracteres(actor, demandado, nombre);

		MCausa causa = new MCausa();
		MEstudio mEstudio = estudioConverter.entidadModelo(estudioService.buscarPorIdEstudio(estudio.getId()));
		MConsulta consulta = new MConsulta();

		if (!isStringNull(id)) {
			causa = causaConverter.entidadModelo(causaRepository.buscarPorId(id));
		}

		if (idPais != null) {
			MPais pais = paisService.buscarPaisModelPorId(idPais);
			causa.setPais(pais);
		}

		if (tipoFuero != null) {
			if (tipoFuero.equals("FEDERAL")) {
				causa.setFuero(TipoFuero.FEDERAL);
			} else {
				causa.setFuero(TipoFuero.PROVINCIAL);
			}
		}

		if (idProvincia != null) {
			MProvincia provincia = provinciaService.buscarProvinciaModelPorId(idProvincia);
			causa.setProvincia(provincia);
		}

		if (instanciaId != null) {
			MInstancia instancia = instanciaService.buscarInstanciaModelPorId(instanciaId);
			causa.setmInstancia(instancia);
		}

		if (idJuzgado != null) {
			MJuzgado juzgado = juzgadoService.buscarJuzgadoModelPorId(idJuzgado);
			causa.setJuzgadoOficina(juzgado);
		}

		if (tipoCausaId != null) {
			MTipoCausa tipoCausa = tipoCausaService.buscarModelPorId(tipoCausaId);
			nombre += tipoCausa.getNombre();
			causa.setmTipoCausa(tipoCausa);
		}

		if (rolCausa != null) {
			switch (rolCausa) {
			case "DENUNCIANTE":
				causa.setRol(TipoRolCausa.DENUNCIANTE);
				break;
			case "ACTOR":
				causa.setRol(TipoRolCausa.ACTOR);
				break;
			case "QUERELLANTE":
				causa.setRol(TipoRolCausa.QUERELLANTE);
				break;
			case "DEMANDADO":
				causa.setRol(TipoRolCausa.DEMANDADO);
				break;
			}
		}

		if (idConsulta != null) {
			consulta = consultaService.buscarModelPorId(idConsulta);
		}

		MUsuario mUsuario = usuarioConverter.entidadModelo(usuario);

		causa.setmEstudio(mEstudio);
		causa.setCreador(mUsuario);
		causa.setmConsulta(consulta);
		causa.setNombre(nombre);
		causa.setActor(actor);
		causa.setDemandado(demandado);
		causa.setNumeroExpediente(nroExpediente);

		return guardar(causa, usuario, estudio);
	}

	public MCausa guardar(MCausa modelo, Usuario creador, Estudio estudio) {
		Causa causa = new Causa();
		if (!isStringNull(modelo.getId())) {
			causa = causaRepository.buscarPorId(modelo.getId());
			validarCausa(modelo.getActor(), modelo.getNombre());
			modificacionService.guardarModificacion("Se modificó la cartula de la causa.", causa, creador);
		} else {
			validarCausa(modelo.getActor(), modelo.getNombre());
		}

		if (modelo.getmConsulta().getId() != null) {
			Consulta consulta = consultaService.buscarPorId(modelo.getmConsulta().getId());
			causa.setConsulta(consulta);
		}
		causa = causaConverter.modeloEntidad(modelo);
		if (isStringNull(modelo.getId())) {
//			MChecklist checklist = new MChecklist();
//			checklist = checklistService.buscarPorTipoDeCausaIdYTipoCheck(modelo.getmTipoCausa().getId(), modelo.getRol());
//			if(modelo.getRol() == TipoRolCausa.DEMANDANTE) {
//				checklist = checklistService.buscarPorTipoDeCausaIdYTipoCheck(modelo.getmTipoCausa().getId(), "DEMANDANTE");				
//			}else {
//				checklist = checklistService.buscarPorTipoDeCausaIdYTipoCheck(modelo.getmTipoCausa().getId(), "DEMANDADO");
//			}
//        	causa = copiar(checklist.getId(), causa, creador);
			if (estudio != null) {
				estadisticaService.guardarEstadistica("Cantidad totales de Causas " + this.fecha.get(Calendar.YEAR), 1,
						creador, null, estudio);
			}
		}
		if (causa.getId() == null) {
			causa.setModo(ModoCausa.ACTIVA);
		}
		causa = guardar(causa, creador, ACTUALIZAR);
		return causaConverter.entidadModelo(causa);
	}

	public Notas guardarNota(String idNota, String idAsignarNota, String tituloNota, String textoNota) {
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);

		Notas nota = new Notas();
		if (idNota != null) {
			Optional<Notas> respuesta = notasRepository.findById(idNota);
			if (respuesta.isPresent()) {
				nota = respuesta.get();
			}
		}

		nota.setCreado(date);
		nota.setTituloNota(tituloNota);
		nota.setTextoNota(textoNota);

		Item item = itemRepository.buscarPorId(idAsignarNota);
		if (item != null) {
			nota.setItem(item);
			notasRepository.save(nota);
		}

		SubItem subItem = subItemRepository.buscarPorId(idAsignarNota);
		if (subItem != null) {
			nota.setSubItem(subItem);
			notasRepository.save(nota);
		}
		return nota;
	}

	public Notas eliminarNota(String idNota) {
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);

		Notas nota = new Notas();
		if (idNota != null) {
			Optional<Notas> respuesta = notasRepository.findById(idNota);
			if (respuesta.isPresent()) {
				nota = respuesta.get();
			}
		}

		nota.setEliminado(date);
		nota = notasRepository.save(nota);
		return nota;
	}

	public List<Notas> listarNotasAsignadas(String idAsignarNota) {
		List<Notas> notasList = new ArrayList<Notas>();
		Item item = itemRepository.buscarPorId(idAsignarNota);
		if (item != null) {
			// notasList = item.getNotas();
			notasList = notasRepository.buscarNotasItem(idAsignarNota);
		}

		SubItem subItem = subItemRepository.buscarPorId(idAsignarNota);
		if (subItem != null) {
			// notasList = subItem.getNotas();
			notasList = notasRepository.buscarNotasSubItem(idAsignarNota);
		}
		return notasList;
	}

	public List<Contacto> buscarContactosConsulta(String causaId) {
		Causa causa = causaRepository.buscarPorId(causaId);
		return causa.getContacto();
	}

	public MCausa archivarCausa(String id, Usuario usuario, Estudio estudio) {
		Causa causa = buscarPorId(id);
		causa.setModo(ModoCausa.ARCHIVADA);
		causa = guardar(causa, usuario, ACTUALIZAR);
		estadisticaService.guardarEstadistica("Cantidad totales de Causas archivadas " + this.fecha.get(Calendar.YEAR),
				1, usuario, null, estudio);
		return causaConverter.entidadModelo(causa);
	}

	private void validarCaracteres(String actor, String demandado, String nombreCaratula) {
		if (limiteCaracteres(actor)) {
			throw new PeticionIncorrectaException("El nombre del actor excede el limite de caracteres");
		}
		if (limiteCaracteres(demandado)) {
			throw new PeticionIncorrectaException("El nombre del demandado excede el limite de caracteres");
		}
		if (limiteCaracteres(nombreCaratula)) {
			throw new PeticionIncorrectaException("El nombre de la caratula excede el limite de caracteres");
		}
	}

	public MCausa activarCausa(String id, Usuario usuario, Estudio estudio) {
		Causa causa = buscarPorId(id);
		causa.setModo(ModoCausa.ACTIVA);
		causa = guardar(causa, usuario, ACTUALIZAR);
		return causaConverter.entidadModelo(causa);
	}

	public MCausa eliminarConsulta(String idCausa, Usuario usuario) {
		Causa causa = new Causa();
		causa = causaRepository.buscarPorId(idCausa);
		causa.setConsulta(null);
		causa = guardar(causa, usuario, ACTUALIZAR);
		return causaConverter.entidadModelo(causa);
	}

	public Causa copiar(String idCheckList, Causa causa, Usuario creador) throws IOException {
		Checklist checklist = checklistService.clonar(idCheckList, creador);
		List<Item> itemsNuevo = itemService.clonar(idCheckList, checklist, creador);
		causa.setItems(itemsNuevo);
		causa.setChecklist(checklistService.buscarCheckListPorId(idCheckList));
		causaRepository.save(causa);

		return causa;
	}

	public MItem eliminarItem(String id, String idCausa, Usuario creador) {
		Item item = itemService.buscarItemPorId(id);
		Causa causa = buscarPorId(idCausa);
		if (causa.getItems().contains(item)) {
			causa.getItems().remove(item);
			modificacionService.guardarModificacion("Se eliminó el paso " + item.getOrden() + " de la causa.", causa,
					creador);
			itemService.auditar(item, creador, SimpleService.ELIMINAR);
			itemRepository.save(item);
			for (Item item2 : causa.getItems()) {
				if (item2.getOrden() > item.getOrden()) {
					item2.setOrden(item2.getOrden() - 1);
					itemRepository.save(item2);
				}
			}
			causaRepository.save(causa);
			this.estado(causa);
		}
		if (causa.getItems().isEmpty()) {
			List<Item> items = new ArrayList<>();

			SubItem subItem1 = new SubItem();
			subItem1.setNombre("Sub Paso 1");
			subItem1.setOrden(1);
			subItemRepository.save(subItem1);

			Item item1 = new Item();
			item1.setNombre("Paso 1");
			item1.setOrden(1);
			item1.setCheckList(causa.getChecklist());
			item1.getSubItems().add(subItem1);

			item1 = itemRepository.save(item1);

			items.add(item1);

			causa.setItems(items);

			causaRepository.save(causa);
			this.estado(causa);
		}
		return itemConverter.entidadModelo(item);
	}

	public MSubItem eliminarSubItem(String id, String idItem, String idCausa, Usuario creador) {
		SubItem subItem = subItemService.buscarSubItemPorId(id);
		Item item = itemService.buscarItemPorId(idItem);
		Causa causa = buscarPorId(idCausa);
		if (item.getSubItems().contains(subItem)) {
			item.getSubItems().remove(subItem);
			modificacionService.guardarModificacion("Se eliminó el sub paso " + subItem.getOrden() + " de la causa.",
					causa, creador);
			subItemService.auditar(subItem, creador, SimpleService.ELIMINAR);
			subItemRepository.save(subItem);
			for (SubItem subItem2 : item.getSubItems()) {
				if (subItem2.getOrden() > subItem.getOrden()) {
					subItem2.setOrden(subItem2.getOrden() - 1);
					subItemRepository.save(subItem2);
				}
			}
			itemRepository.save(item);
		}
		if (item.getSubItems().isEmpty()) {
			itemService.agregarSubItem(null, item.getNombre(), idItem, null, idCausa, creador);
		}
		return subItemConverter.entidadModelo(subItem);
	}

	public MCausa eliminar(String idCausa, Usuario eliminador) {
		Causa causa = causaRepository.buscarPorId(idCausa);
		return causaConverter.entidadModelo(guardar(causa, eliminador, ELIMINAR));
	}

	@Transactional
	private Causa guardar(Causa causa, Usuario usuario, String accion) {
		auditar(causa, usuario, accion);
		return causaRepository.save(causa);
	}

	private void validarCausa(String caratula, String nombre) {
		if (isStringNull(caratula)) {
			throw new PeticionIncorrectaException("La caratula de la causa no puede estar vacia!");
		}
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la causa no puede estar vacia!");
		}
	}

	public Causa estado(Causa causa) {
		int cant = 0;
		int estadoCausa = 0;
		for (Item paso : causa.getItems()) {
			if (paso.isEstado()) {
				cant++;
			}
		}
		if (cant == causa.getItems().size()) {
			estadoCausa = 100;
		} else {
			estadoCausa = (cant * 100) / causa.getItems().size();
		}
		if (causa.getItems().isEmpty()) {
			estadoCausa = 0;
		}
		causa.setEstado(estadoCausa);
		causaRepository.save(causa);
		return causa;
	}

	// PROMEDIO DEL ESTADO DE AVANCE DE TODAS LAS CAUSAS PARA ESTADISTICAS
	public void promedioAvance(String idUsuario, Estudio estudio) {
		Usuario usuario = usuarioService.buscarPorId(idUsuario);
		int estadoAvance = 0;
		int promedio = 0;
		Page<Causa> causas = listarCausa(estudio.getId(), null, null, null, null);
		if (causas.getTotalPages() > 0) {
			for (Causa causa : causas) {
				if (causa.getEstado() != null) {
					estadoAvance += causa.getEstado();
				}
			}
			promedio = estadoAvance / causas.getTotalPages();
			estadisticaService.guardarEstadistica(
					"Cantidad promedio de estado de avance de las causas " + this.fecha.get(Calendar.YEAR), promedio,
					usuario, null, estudio);
		}
	}

	// ESTADO DE LA CAUSA SEGUN TIEMPO
	public Page<Causa> colorBarra(List<MCausa> causas, Usuario usuario, Estudio estudio, boolean boleano) {
		Date fechaActual = new Date();
		List<Causa> causasConColor = new ArrayList<>();
		int verde = 0;
		int rojo = 0;
		int amarillo = 0;
		if (!boleano) {
			causas = listarActivasModel();
		}
		for (MCausa mCausa : causas) {
			Causa causa = buscarPorId(mCausa.getId());
			List<Modificacion> modificaciones = modificacionService.buscarPorCausa(mCausa.getId());
			Modificacion modificacionChecked = new Modificacion();
			for (Modificacion modificacion : modificaciones) {
				if (modificacion.getTipoModificacion().indexOf("Se completó el paso") != -1
						|| modificacion.getTipoModificacion().indexOf("Se completó el sub paso") != -1) {
					modificacionChecked = modificacion;
					break;
				}
			}
			int diferenciaDias;
			if (!isStringNull(modificacionChecked.getId())) {
				diferenciaDias = (int) ((fechaActual.getTime() - modificacionChecked.getCreado().getTime()) / 86400000);
			} else {
				diferenciaDias = -1;
			}
			if (diferenciaDias >= 31 && diferenciaDias < 60) {
				causa.setColor("amarillo");
				amarillo += 1;
			} else if (diferenciaDias >= 0 && diferenciaDias < 31) {
				causa.setColor("verde");
				verde += 1;
			} else {
				causa.setColor("rojo");
				rojo += 1;
			}
			mCausa = causaConverter.entidadModelo(causaRepository.save(causa));
			causasConColor.add(causa);
		}
		if (boleano) {
			estadisticaService.guardarEstadistica(
					"Cantidad totales de Causas en verde " + this.fecha.get(Calendar.YEAR), verde, usuario, null,
					estudio);
			estadisticaService.guardarEstadistica("Cantidad totales de Causas en rojo " + this.fecha.get(Calendar.YEAR),
					rojo, usuario, null, estudio);
			estadisticaService.guardarEstadistica(
					"Cantidad totales de Causas en amarillo " + this.fecha.get(Calendar.YEAR), amarillo, usuario, null,
					estudio);
		}
		Page<Causa> page = new PageImpl<Causa>(causasConColor);
		return page;
	}

	public MCausa buscarCausaModelPorId(String idCausa) {
		Causa c = causaRepository.buscarPorId(idCausa);
		if(c != null) {
			return causaConverter.entidadModelo(c);
		}
		return null;
	}

	public Page<Causa> listarCausa(String idEstudio, String filtro, String clave, Pageable paginable, String q) {
		if (filtro != null && !filtro.isEmpty()) {
			switch (filtro) {
			case "alfabetico az":
				return causaRepository.alfabeticoAZ(idEstudio, paginable);
			case "alfabetico za":
				return causaRepository.alfabeticoZA(idEstudio, paginable);

			case "mas avanzado":
				return causaRepository.avanzado(idEstudio, paginable);

			case "mas atrazado":
				return causaRepository.atrazado(idEstudio, paginable);

			case "mas antiguo":
				return causaRepository.antiguo(idEstudio, paginable);

			case "mas reciente":
				return causaRepository.reciente(idEstudio, paginable);

			case "movimiento mas reciente":
				return modificacionRepository.porRecienteModificacion(paginable);

			case "movimiento mas antiguo":
				return modificacionRepository.porAntiguoModificacion(paginable);

			case "tipo de causa":
				return causaRepository.buscarPorTipoCausa(clave, paginable);

			case "usuarios":
				return causaRepository.buscarPorUsuario(clave, paginable);

			case "zona":
				return causaRepository.buscarPorProvincia(clave, paginable);

			default:
				return causaRepository.buscarTodos(idEstudio, paginable);
			}
		} else {
			if (q == null || q.isEmpty()) {
				return causaRepository.buscarTodos(idEstudio, paginable);
			} else {
				return causaRepository.buscarPorNombre(idEstudio, "%" + q + "%", paginable);
			}
		}
	}

	public List<Causa> listarArchivadas(String idEstudio) {
		return causaRepository.buscarArchivadas(idEstudio);
	}

	public List<Causa> listarActivas() {
		return causaRepository.buscarActivas();
	}

	public List<MCausa> listarActivasModel() {
		return causaConverter.entidadesModelos(causaRepository.buscarActivas());
	}

	public List<Causa> listarCausasCreador(Usuario usuario) {
		List<Causa> causas = new ArrayList<>();
		for (Causa causa : causaRepository.findAll()) {
			if (causa.getEliminado() == null && causa.getCreador() == usuario) {
				causas.add(causa);
			}
		}
		return causas;
	}

	public Causa buscarPorId(String causaId) {
		return causaRepository.buscarPorId(causaId);
	}

	public Causa buscarPorNumeroExpediente(String numeroExpediente) {
		return causaRepository.buscarPorNumeroExpediente(numeroExpediente);
	}

	public List<Causa> listarPorEstudioId(String estudioId) {
		return causaRepository.buscarPorEstudio(estudioId);
	}

	public Page<Causa> listarPorTipoCausaId(String tipoCausaId, Pageable paginable) {
		return causaRepository.buscarPorTipoCausa(tipoCausaId, paginable);
	}

	public List<Causa> listarPorInstanciaId(String instanciaId) {
		return causaRepository.buscarPorInstancia(instanciaId);
	}

	public List<Causa> listarPorMiembroId(String miembroCausaId) {
		return causaRepository.buscarPorMiembro(miembroCausaId);
	}

	public Integer contarTodasCausasEstudio(String idEstudio) {
		return causaRepository.contarTodasCausasEstudio(idEstudio);
	}

	public Integer contarActivasEstudio(String idEstudio) {
		return causaRepository.contarActivasEstudio(idEstudio);
	}

	public List<Causa> buscarPorMiembro(String idMiembro) {
		return causaRepository.buscarPorMiembro(idMiembro);
	}

}