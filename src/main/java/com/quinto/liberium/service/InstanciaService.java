package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.InstanciaConverter;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MInstancia;
import com.quinto.liberium.repository.InstanciaRepository;

@Service("instanciaService")
public class InstanciaService extends SimpleService {

	@Autowired
	private InstanciaRepository instanciaRepository;

	@Autowired
	private InstanciaConverter instanciaConverter;

	public MInstancia guardar(String id, String nombre, Usuario usuario) {
		Instancia instancia = new Instancia();

		if (!isStringNull(id)) {
			instancia = buscarInstanciaPorId(id);
			validarInstancia(nombre, instancia);
		} else {
			validarInstancia(nombre, null);
		}

		instancia.setNombre(nombre);
		
		return instanciaConverter.entidadModelo(guardar(instancia, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Instancia guardar(Instancia instancia, Usuario usuario, String accion) {
		auditar(instancia, usuario, accion);
		return instanciaRepository.save(instancia);
	}

	public MInstancia eliminarInstancia(String id, Usuario usuario) {
		Instancia instancia  = buscarInstanciaPorId(id);
		if (instancia == null) {
			throw new EventoRestException("La instancia que trata eliminar no se encuentra en la base de datos.");
		}

		instancia = guardar(instancia, usuario, SimpleService.ELIMINAR);
		return instanciaConverter.entidadModelo(instancia);
	}

	private void validarInstancia(String nombre, Instancia instancia) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la instancia no puede estar vacio.");
		} else {
			List<Instancia> duplicado = instanciaRepository.duplicado(nombre);
			if (duplicado.size() > 0) {
				throw new PeticionIncorrectaException("El nombre de la instancia ingresada ya se encuentran utilizada.");
			}
		}
	}

	public List<MInstancia> buscarInstanciasModel() {
		return instanciaConverter.entidadesModelos(instanciaRepository.buscarTodos());
	}

	public Page<Instancia> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return instanciaRepository.buscarTodas(paginable);
		} else {
			return instanciaRepository.buscarTodas(paginable, "%" + q + "%");
		}
		
	}

	
	public MInstancia crear(MInstancia mInstancia, Usuario creador) {
		if (mInstancia == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mInstancia.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear un país");
		}
		Instancia instancia = instanciaConverter.modeloEntidad(mInstancia);
		return instanciaConverter.entidadModelo(guardar(instancia, creador));
	}

	public void eliminar(String idInstancia, Usuario eliminador) {
		if (isStringNull(idInstancia) || eliminador == null) {
			throw new PeticionIncorrectaException("Ocurrio un error al eliminar el país");
		}
		Instancia instancia = instanciaRepository.buscarPorId(idInstancia);
		guardar(instancia, eliminador, ELIMINAR);
	}

	private Instancia guardar(Instancia instancia, Usuario interviniente) {
		return guardar(instancia, interviniente, null);
	}
	
	public List<MInstancia> buscarInstanciaModelTodos() {
		return instanciaConverter.entidadesModelos(instanciaRepository.buscarTodos());
	}
	
	public List<Instancia> buscarInstanciaTodos() {
		return instanciaRepository.buscarTodos();
	}

	public MInstancia buscarInstanciaModelPorId(String idInstancia) {
		return instanciaConverter.entidadModelo(instanciaRepository.buscarPorId(idInstancia));
	}
	
	public Instancia buscarInstanciaPorId(String idInstancia) {
		return instanciaRepository.buscarPorId(idInstancia);
	}

	public MInstancia buscarInstanciaModelPorNombre(String nombre) {
		return instanciaConverter.entidadModelo(instanciaRepository.buscarPorNombre(nombre));
	}

	public Instancia buscarInstanciaPorNombre(String nombre) {
		return instanciaRepository.buscarPorNombre(nombre);
	}

}