package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.AbogadoConverter;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.repository.AbogadoRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service("abogadoService")
public class AbogadoService extends SimpleService {

	private final Log LOG = LogFactory.getLog(AbogadoService.class);

	@Autowired(required = true)
	private AbogadoRepository abogadoRepository;

	@Autowired
	private AbogadoConverter abogadoConverter;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private MateriaService materiaServicie;

	@Autowired
	private LocalidadService localidadService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	public MAbogado guardar(String id, String localidad, String idProvincia, String domicilio, String matricula,
			List<String> idMaterias, String mail) {
		Abogado abogado = new Abogado();

		if (!isStringNull(id)) {
			abogado = buscarAbogadoPorId(id);
			validarAbogado(idProvincia, idMaterias, domicilio, matricula);
		}

		Localidad localidadSet = localidadService.buscarPorId(localidad);

		abogado.setDomicilio(domicilio);
		abogado.setMatricula(matricula);
		abogado.setLocalidad(localidadSet);

		Provincia provincia = provinciaRepository.buscarPorId(idProvincia);
		abogado.setProvincia(provincia);

		List<Materia> materiasAbogado = new ArrayList<>();
		for (String idMateria : idMaterias) {
			Materia materia = materiaServicie.buscarMateriaPorId(idMateria);
			materiasAbogado.add(materia);
		}

		for (Materia materiaAbogado : materiasAbogado) {
			LOG.info("MATERIA ABOGADO: " + materiaAbogado.getNombre());
		}

		abogado.setMaterias(materiasAbogado);

		Usuario usuarioAbogado = usuarioService.buscarUsuarioPorEmail(mail);
		abogado.setUsuario(usuarioAbogado);

		usuarioAbogado.setAbogado(true);

		return abogadoConverter.entidadModelo(guardar(abogado, usuarioAbogado, SimpleService.ACTUALIZAR));
	}

	public boolean buscarAbogadosParaConsulta(String localidad, String tipoCausa) {

		TipoCausa tipoCausaE = tipoCausaService.buscarPorId(tipoCausa);

		List<MAbogado> abogadosDisponibles = buscarAbogadoPorLocalidadYMateria(tipoCausaE.getMateria().getId(),
				localidad);

		if (abogadosDisponibles.size() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public MAbogado guardarInfoTurnoConsulta(Integer cantSemanas, String[] pasosCrudos, Usuario usuario) {
		if (cantSemanas == null) {
			throw new PeticionIncorrectaException("Tiene que ingresar una cantidad de semanas para poder guardar!");
		}

		if (pasosCrudos == null) {
			throw new PeticionIncorrectaException("Tiene que ingresar por lo menos un paso para poder guardar!");
		}
		
		String[] newArray = new String[pasosCrudos.length];  
	    for(int cnt = 0; cnt < pasosCrudos.length; cnt++){  
	    	if (pasosCrudos[cnt] == "" || pasosCrudos[cnt].length() == 0) {
				throw new PeticionIncorrectaException("No puede guardar un paso vacío!");
			} else if (pasosCrudos[cnt].length() > 999) {
				throw new PeticionIncorrectaException("El paso excedió el limite de caracteres!");
			} else {
		        newArray[cnt] = (cnt + 1) + "-" + pasosCrudos[cnt];  
			}
	    }  
	    
		Abogado abogado = buscarAbogadoPorMailUsuario(usuario.getEmail());
		
		abogado.setCantSemanas(cantSemanas);
		
		abogado.setPasos(newArray);
		
		guardar(abogado, usuario, ACTUALIZAR);
		
	
		return abogadoConverter.entidadModelo(abogado);
	}

	public MAbogado eliminarPaso(String texto, Usuario usuario) {
		Abogado abogado = buscarAbogadoPorMailUsuario(usuario.getEmail());
		final List<String> list =  new ArrayList<String>();
		Collections.addAll(list, abogado.getPasos()); 
		for (String paso : abogado.getPasos()) {
			if (paso.equals(texto)) {
				list.remove(paso);
			}
		}
		String[] nuevosPasos = list.toArray(new String[list.size()]);
		if (nuevosPasos.length == 0) {
			nuevosPasos = null;
		}
		abogado.setPasos(nuevosPasos);
		return abogadoConverter.entidadModelo(guardar(abogado, usuario, ACTUALIZAR));
	}
	
	@Transactional
	private Abogado guardar(Abogado abogado, Usuario usuario, String accion) {
		auditar(abogado, usuario, accion);
		return abogadoRepository.save(abogado);
	}

	public MAbogado eliminar(String id, Usuario usuario) {
		Abogado abogado = buscarAbogadoPorId(id);

		if (abogado == null) {
			throw new EventoRestException("El abogado que trata eliminar no se encuentra en la base de datos.");
		}
		abogado = guardar(abogado, usuario, SimpleService.ACTUALIZAR);
		return abogadoConverter.entidadModelo(abogado);
	}

	private void validarAbogado(String idProvincia, List<String> idMaterias, String domicilio, String matricula) {

		if (idMaterias == null || idMaterias.isEmpty() == true) {
			throw new PeticionIncorrectaException("La materia de el abogado no puede estar vacía.");
		}
		
		if (domicilio.length() > 100) {
			throw new PeticionIncorrectaException("El domicilio supera el límite de caracteres.");
		}

		if (isStringNull(domicilio)) {
			throw new PeticionIncorrectaException("El domicilio no puede estar vacío.");
		}

		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException("El nombre de la provincia no puede estar vacía.");
		}
		
		if (matricula.length() > 40) {
			throw new PeticionIncorrectaException("La matrícula supera el límite de caracteres.");
		}
		
		if (isStringNull(matricula)) {
			throw new PeticionIncorrectaException("La matrícula del abogado no puede estar vacía.");
		}
	}

	public Page<Abogado> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return abogadoRepository.buscarTodos(paginable);
		} else {
			return abogadoRepository.buscarTodos(paginable, "%" + q + "%");
		}

	}

	public List<MAbogado> buscarTodosModel(String q) {

		if (q == null || q.isEmpty()) {
			return abogadoConverter.entidadesModelos(abogadoRepository.buscarTodos());			
		}else {
			return abogadoConverter.entidadesModelos(abogadoRepository.buscarTodos("%" + q +"%"));
		}
		
	}

	public MAbogado crear(MAbogado mAbogado, Usuario creador) {
		if (mAbogado == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}

		Abogado oficina = abogadoConverter.modeloEntidad(mAbogado);
		return abogadoConverter.entidadModelo(guardar(oficina, creador));
	}

	private Abogado guardar(Abogado oficina, Usuario interviniente) {
		return guardar(oficina, interviniente, null);
	}

	public List<Abogado> listarAbogados() {
		return abogadoRepository.findAll();
	}

	public List<MAbogado> buscarAbogadosPorNombre(String nombre) {
		return abogadoConverter.entidadesModelos(abogadoRepository.buscarAbogadosPorNombre("%" + nombre + "%"));
	}

	public Abogado buscarAbogadoPorId(String id) {
		return abogadoRepository.buscarAbogadoPorId(id);
	}
	
	public MAbogado buscarAbogadoPorDNI(String dni) {
		Abogado a = abogadoRepository.buscarAbogadoPorDNI(dni);
		if(a==null) {
			return null;
		}
		return abogadoConverter.entidadModelo(a);
	}

	public List<MAbogado> buscarAbogadosActivos() {
		return abogadoConverter.entidadesModelos(abogadoRepository.buscarAbogadosActivos());
	}

//	public List<MAbogado> buscarAbogadoPorMateria(String idMateria){
//		return abogadoConverter.entidadesModelos(abogadoRepository.findByMaterias_Id(idMateria));
//	}

	public List<MAbogado> buscarAbogadoPorProvincia(String provincia) {
		return abogadoConverter.entidadesModelos(abogadoRepository.buscarAbogadoPorProvincia(provincia));
	}

	public List<MAbogado> buscarAbogadosPorProvinciaYEspecialidad(String provincia, String especialidad) {
		return abogadoConverter
				.entidadesModelos(abogadoRepository.buscarAbogadosPorProvinciaYEspecialidad(provincia, especialidad));
	}

	public List<MAbogado> buscarAbogadoPorLocalidadYMateria(String localidad, String especialidad) {
		return abogadoConverter
				.entidadesModelos(abogadoRepository.buscarAbogadoPorLocalidadYMateria(especialidad, localidad));
	}

	public Abogado buscarAbogadoPorMailUsuario(String eMail) {
		return abogadoRepository.buscarPorMail(eMail);
	}
	
	public MAbogado buscarMAbogadoPorMailUsuario(String eMail) {
		Abogado abogado = abogadoRepository.buscarPorMail(eMail);
		if (abogado == null) {
			return new MAbogado();
		} else {
			return abogadoConverter.entidadModelo(abogado);
		}
	}
	
	public MAbogado guardarDuracion(Integer duracion, Abogado abogado, String eMail) {
		
		abogado.setDuracion(duracion);
		
		return abogadoConverter.entidadModelo(guardar(abogado, usuarioService.buscarUsuarioPorEmail(eMail), ACTUALIZAR));		
	}
	
	public List<MAbogado> buscarAbogadoEstudio(String idEstudio){
		return abogadoConverter.entidadesModelos(abogadoRepository.buscarAbogadosEstudio(idEstudio));
	}
}
