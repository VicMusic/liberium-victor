package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.quinto.liberium.converter.UsuarioConverter;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MUsuario;
import com.quinto.liberium.repository.AbogadoRepository;
import com.quinto.liberium.repository.UsuarioRepository;
import com.quinto.liberium.security.PasswordEncoder;

@Service("usuarioService")
public class UsuarioService extends SimpleService implements UserDetailsService {

	private final Log LOG = LogFactory.getLog(UsuarioService.class);
	private final Integer LIMITE_DNI = 8;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private AbogadoRepository abogadoRepository;

	@Autowired
	private UsuarioConverter usuarioConverter;

	@Autowired
	private EmailService emailService;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private PermisoService permisoService;

	@Autowired
	private MiembroService miembroService;

	@Autowired
	private EstadisticaService estadisticaService;

	public MUsuario actualizarBio(String mail, String nombre, byte[] foto, boolean eliminar) {

		LOG.info("METHOD: actualizarBio() -- PARAMS: mail=" + mail + " -- nombre=" + nombre + " -- eliminar(foto)="
				+ eliminar + " -- foto=" + foto.toString());
		Usuario usuario = buscarUsuarioPorEmail(mail);
		usuario.setNombre(nombre);
		if (eliminar) {
			usuario.setFoto(null);
		} else if (foto.length > 0) {
			usuario.setFoto(foto);
		}
		return usuarioConverter.entidadModelo(guardar(usuario, usuario, ACTUALIZAR));
	}

	public MUsuario eliminarFoto(String mail) {
		Usuario usuario = buscarUsuarioPorEmail(mail);
		usuario.setFoto(null);
		return usuarioConverter.entidadModelo(guardar(usuario, usuario, ACTUALIZAR));
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		Usuario usuario = buscarUsuarioPorEmail(username);
		String permisoStr = "";

		if (usuario != null) {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			boolean bandera = false;
			HttpSession session = attr.getRequest().getSession(true);
			session.setAttribute("usuarioSession", usuario);
			String iniciales = usuario.getNombre().substring(0, 1);
			for(int i = 0; i < usuario.getNombre().length(); i++) {
				if(bandera == true) {
					iniciales = iniciales + String.valueOf(usuario.getNombre().charAt(i));
					bandera = false;
				}
				if(usuario.getNombre().charAt(i) == ' ') {
					bandera = true;
				}
			}
			iniciales = iniciales.toUpperCase();
			session.setAttribute("iniciales", iniciales);
			for (Permiso permiso : usuario.getPermisos()) {
				permisoStr += permiso.getNombre() + " - ";
			}

			session.setAttribute("permisos", permisoStr);

			List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(usuario.getTipoUsuario().toString()));

			return transformar(usuario, session);
		} else {
			throw new UsernameNotFoundException("Usuario nulo");
		}
	}

	public boolean resetearClave(String mail) {
		Usuario usuario = usuarioRepository.buscarPorEmail(mail);
		if (usuario != null) {
			usuario.setResetToken(UUID.randomUUID().toString());
			usuarioRepository.save(usuario);

			emailService.resetearClave(usuario);

			return true;
		} else {
			return false;
		}

	}

	private User transformar(Usuario usuario, HttpSession session) {
		List<GrantedAuthority> permisos = new ArrayList<>();
		if (usuario != null) {
			if (usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)
					|| usuario.getTipoUsuario().equals(TipoUsuario.OPERADOR)) {
				List<Permiso> permisosD = usuario.getPermisos();
				permisos = asignarPermisos(permisosD, permisos, true);
				session.setAttribute("estudioSession", "NOT_ESTUDIO");
			} else if (usuario.getTipoUsuario().equals(TipoUsuario.USUARIO)) {
				List<Miembro> miembros = miembroService.buscarPorIdUsuario(usuario.getId());
				List<Estudio> estudios = miembroService.buscarEstudioPorIdUsuario(usuario.getId());
				if (miembros.size() > 0) {
					List<Permiso> permisosD = miembros.get(0).getRol().getPermisos();
					permisos = asignarPermisos(permisosD, permisos, false);
					session.setAttribute("estudioSession", miembros.get(0).getEstudio());
					session.setAttribute("estudiosSession", estudios);
				} else {
					permisos.add(new SimpleGrantedAuthority("VISUALIZAR_PERFIL"));
					session.setAttribute("estudioSession", "NOT_ESTUDIO");
				}
			}

			return new User(usuario.getEmail(), usuario.getPassword(), usuario.getEliminado() == null, true, true, true,
					permisos);
		} else {
			return null;
		}
	}

	private List<GrantedAuthority> asignarPermisos(List<Permiso> permisosD, List<GrantedAuthority> permisos,
			boolean isAdministrador) {
		if (isAdministrador) {
			permisos.add(new SimpleGrantedAuthority("VISUALIZAR_PERFIL_ADMINISTRADOR"));
		} else {
			permisos.add(new SimpleGrantedAuthority("VISUALIZAR_PERFIL"));
		}
		for (Permiso permiso : permisosD) {
			permisos.add(new SimpleGrantedAuthority(permiso.getNombre()));
		}

		return permisos;
	}

	public void updatePermisos(Miembro miembro, HttpSession session) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<GrantedAuthority> updatedAuthorities = new ArrayList<>();

		asignarPermisos(miembro.getRol().getPermisos(), updatedAuthorities, false);

		session.removeAttribute("estudioSession");
		session.setAttribute("estudioSession", miembro.getEstudio());

		List<Estudio> estudios = miembroService.buscarEstudioPorIdUsuario(miembro.getUsuario().getId());
		session.removeAttribute("estudiosSession");
		session.setAttribute("estudiosSession", estudios);

		Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(),
				updatedAuthorities);

		SecurityContextHolder.getContext().setAuthentication(newAuth);

	}

	public Usuario crear(String id, String nombre, String email, String password, String idPais, String idProvincia,
			TipoUsuario tipoUsuario, Boolean isAbogado, String[] permisosChecked, Usuario user, Boolean isMiembro) {
		Usuario usuario = new Usuario();

		if (!isStringNull(id)) {
			usuario = buscarPorId(id);
			if (!isMiembro) {
				validarUsuario(nombre, email, password, idPais != null ? idPais : user.getPais().getId(), idProvincia != null ? idProvincia : user.getProvincia().getId(), usuario, tipoUsuario);
			}
		} else {
			if (!isMiembro) {
				validarUsuario(nombre, email, password, idPais != null ? idPais : user.getPais().getId(), idProvincia != null ? idProvincia : user.getProvincia().getId(), null, tipoUsuario);
			}
		}

		usuario.setTipoUsuario(tipoUsuario);
		usuario.setAbogado(isAbogado);
		usuario.setNombre(nombre);
		usuario.setEmail(email);
		if (isStringNull(usuario.getId())) {
			usuario.setConfirmToken(UUID.randomUUID().toString());
			usuario.setEnabled(false);
		}

		Pais pais = paisService.buscarPaisPorId(idPais != null ? idPais : user.getPais().getId());
		Provincia provincia = provinciaService.buscarProvinciaPorId(idProvincia != null ? idProvincia : user.getProvincia().getId());

		usuario.setPais(pais);
		usuario.setProvincia(provincia);

		if (password != null && tipoUsuario.equals(TipoUsuario.USUARIO)) {
			usuario.setPassword(PasswordEncoder.encriptarPassword(password));
		} else if (usuario.getPassword() == null) {
			usuario.setPassword(PasswordEncoder.encriptarPassword("liberium123"));
		}

		if (!tipoUsuario.equals(TipoUsuario.USUARIO)) {
			usuario = generarPermisosPorTipoUsuario(usuario, tipoUsuario, permisosChecked);
		}

		usuario = guardar(usuario, user, "crear");

		if (isAbogado) {
			Abogado abogado = new Abogado();
			abogado.setCreado(new Date());
			abogado.setCreador(usuario);
			abogado.setProvincia(provincia);
			abogado.setUsuario(usuario);
			abogado.setCantSemanas(1);
			abogado.setDuracion(1);

			abogadoRepository.save(abogado);
		}

		if (!usuario.isEnabled()) {
			emailService.mandarMailDeConfirmacion(usuario);
		}

		LOG.info("INFO ABOGADO: id: " + usuario.getId());

		return guardar(usuario, user, "crear");
	}

	private Usuario generarPermisosPorTipoUsuario(Usuario usuario, TipoUsuario tipoUsuario, String[] permisosChecked) {
		if (tipoUsuario.equals(TipoUsuario.ADMINISTRADOR)) {
			usuario.setPermisos(permisoService.buscarPorAdministrador(true));
		} else if (tipoUsuario.equals(TipoUsuario.OPERADOR)) {
			usuario.setPermisos(permisoService.buscarPorIds(permisosChecked));
		}
		return usuario;
	}

	public Usuario guardar(Usuario usuario, Usuario creador, String accion) {
		auditar(usuario, creador, accion);
		return usuarioRepository.save(usuario);
	}

	public MUsuario eliminarUsuario(String id, Usuario usuario) {
		Usuario user = buscarPorId(id);
		if (user == null) {
			throw new EventoRestException("El usuario que trata eliminar no se encuentra en la base de datos");
		}
		auditar(user, usuario, "eliminar");
		usuarioRepository.save(user);
		return usuarioConverter.entidadModelo(user);
	}

	private void validarUsuario(String nombre, String email, String password, String idPais, String idProvincia,
			Usuario usuario, TipoUsuario tipoUsuario) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre para el usuario no puede estar vacio.");
		}

		if (nombre.length() > 100) {
			throw new PeticionIncorrectaException("El nombre es demasiado largo.");
		}

		if (nombre.matches(".*\\d.*")) {
			throw new PeticionIncorrectaException("El nombre no puede contener caracteres especiales.");
		}

		if (!isValid(email)) {
			throw new PeticionIncorrectaException("El formato de el mail ingresado no es el correcto.");
		}

		if (tipoUsuario == null || tipoUsuario.equals(null)) {
			throw new PeticionIncorrectaException("El tipo de usuario no puede estar vacio.");
		}

		if (tipoUsuario.equals(TipoUsuario.USUARIO)) {
			if (isStringNull(password)) {
				throw new PeticionIncorrectaException("La contraseña no puede estar vacia!");
			}

			if (password.length() < 8) {
				throw new PeticionIncorrectaException("La contraseña debe contener como mínimo 8 caracteres");
			}

			if (isStringNull(idPais)) {
				throw new PeticionIncorrectaException("El pais de el usuario no puede estar vacio!");
			}
			if (isStringNull(idProvincia)) {
				throw new PeticionIncorrectaException("La provincia de el usuario no puede estar vacia!");
			}
		}

		if (isStringNull(email)) {
			throw new PeticionIncorrectaException("El email para el usuario no puede estar vacio.");
		} else {
			Usuario user = buscarUsuarioPorEmail(email);
			if ((usuario == null && user != null)
					|| (usuario != null && user != null && !usuario.getId().equals(user.getId()))) {
				throw new PeticionIncorrectaException(
						"El email ingresado ya esta vinculado a una cuenta existente en liberium.");
			}
		}
	}

	public void validarDatosUsuario(String dni, String telefono) {
		convertirNum(dni, LIMITE_DNI,
				"El Dni contiene carácteres no deseados o la cantidad de dígitos no es la adecuada.");
		convertirNum(telefono, null, "El Telefono contiene carácteres no deseados o está vacío.");
	}

	public void convertirNum(String number, Integer limite, String mensajeErr) {
		number = number.replaceAll(" ", "");
		if (limite != null && (number.length() > limite || number.length() < (limite - 1))) {
			throw new PeticionIncorrectaException(mensajeErr);
		}
		try {
			Double.parseDouble(number);
		} catch (Exception e) {
			throw new PeticionIncorrectaException(mensajeErr);
		}
	}

	public boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	public boolean isAlphaNumeric(String s) {
		String pattern = "^[a-zA-Z0-9]*$";
		return s.matches(pattern);
	}

	public MUsuario editar(Usuario original, MUsuario edicion) {
		if (edicion.getNombre() != null && !edicion.getNombre().isEmpty()) {
			original.setNombre(edicion.getNombre());
		}
		if (edicion.getEmail() != null && !edicion.getEmail().isEmpty()) {
			original.setEmail(edicion.getEmail());
		}
		if (edicion.getDni() != null && !edicion.getDni().isEmpty()) {
			original.setDni(edicion.getDni());
		}

		if (edicion.getTelefono() != null && !edicion.getTelefono().isEmpty()) {
			original.setTelefono(edicion.getTelefono());
		}

		if (edicion.getFechaDeNacimiento() != null) {
			original.setFechaDeNacimiento(edicion.getFechaDeNacimiento());
		}

		DateTime fecha = new DateTime(edicion.getFechaDeNacimiento());
		if (fecha.isAfterNow()) {
			throw new PeticionIncorrectaException("La fecha de nacimiento ingresada no es valida.");
		}
		usuarioRepository.save(original);

		return usuarioConverter.entidadModelo(original);
	}

	public Usuario buscarPorResetToken(String resetToken) {
		return usuarioRepository.buscarPorResetToken(resetToken);
	}

	public Optional<Usuario> findUserByEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}

	public MUsuario buscarPorEmail(String email) {
		return usuarioConverter.entidadModelo(usuarioRepository.buscarPorEmail(email));
	}

	public Usuario buscarUsuarioPorEmail(String email) {
		Usuario usuario = usuarioRepository.buscarPorEmail(email);
		if (usuario != null) {
			usuario.setFechaLogin(new Date());
			usuarioRepository.save(usuario);
		}
		return usuario;
	}

	public Usuario logout(String email) {
		Usuario usuario = usuarioRepository.buscarPorEmail(email);
		usuario.setFechaLogOut(new Date());
		usuarioRepository.save(usuario);
		Locale locale = Locale.getDefault();
		Calendar fecha = Calendar.getInstance();
		int difMin = 0;
		Calendar fechaLogin = Calendar.getInstance();
		fechaLogin.setTime(usuario.getFechaLogin());
		Calendar fechaLogOut = Calendar.getInstance();
		fechaLogOut.setTime(usuario.getFechaLogOut());
		int minLogin = fechaLogin.get(Calendar.HOUR) * 60 + fechaLogin.get(Calendar.MINUTE);
		int minLogOut = fechaLogOut.get(Calendar.HOUR) * 60 + fechaLogOut.get(Calendar.MINUTE);
		if (fechaLogin.get(Calendar.DAY_OF_MONTH) == fechaLogOut.get(Calendar.DAY_OF_MONTH)) {
			difMin = minLogOut - minLogin;
			if (!usuario.getTipoUsuario().getTexto().equals("Administrador")) {
				estadisticaService.guardarEstadistica("Tiempo de sesión " + fecha.get(Calendar.YEAR), difMin, usuario,
						fecha, null);
			}
		} else if (fechaLogin.get(Calendar.DAY_OF_MONTH) != fechaLogOut.get(Calendar.DAY_OF_MONTH)) {
			minLogin = (24 - fechaLogin.get(Calendar.HOUR)) * 60;
			difMin = minLogOut + minLogin;
			estadisticaService.guardarEstadistica("Tiempo de sesión " + fecha.get(Calendar.YEAR), difMin, usuario,
					fecha, null);
		} else if (fechaLogin.get(Calendar.MONTH) != fechaLogOut.get(Calendar.MONTH)) {
			estadisticaService.guardarEstadistica("Tiempo de sesión " + fecha.get(Calendar.YEAR), minLogin, usuario,
					fechaLogin, null);
			estadisticaService.guardarEstadistica("Tiempo de sesión " + fecha.get(Calendar.YEAR), minLogOut, usuario,
					fecha, null);
		}

		return usuario;
	}

	public Usuario buscarUsuarioPorNombre(String nombre) {
		return usuarioRepository.buscarPorNombre(nombre);
	}

	public Usuario buscarPorId(String id) {
		return usuarioRepository.buscarPorId(id);
	}

	public Page<Usuario> buscarTodosUsuariosNoAbogados() {
		return usuarioRepository.buscarTodosUsuariosNoAbogados(null);
	}

	public Page<Usuario> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return usuarioRepository.buscarTodos(paginable);
		} else {
			return usuarioRepository.buscarTodos(paginable, "%" + q + "%");
		}
	}

	public Page<Usuario> buscarTodosPanelAdministrador(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return usuarioRepository.buscarTodosPanelAdministrador(paginable, null);
		} else {
			return usuarioRepository.buscarTodosPanelAdministrador(paginable, "%" + q + "%");
		}
	}

	public Page<Usuario> buscarMiembrosLiberium(Pageable paginable) {
		return usuarioRepository.buscarMiembrosLiberium(paginable);
	}
	
	public Page<Usuario> buscarMiembrosLiberium(Pageable paginable,String q) {
		return usuarioRepository.buscarMiembrosLiberium(paginable,"%" + q + "%");
	}
	
	public List<Usuario> buscarTodosAbogados(String nombre) {
		if (nombre == null || nombre.isEmpty()) {
			return usuarioRepository.buscarTodosAbogados();
		} else {
			return usuarioRepository.buscarTodosAbogados("%" + nombre + "%");
		}
	}

	public List<Usuario> buscarTodos() {
		return usuarioRepository.findAll();
	}

	public List<MUsuario> buscarTodosModel() {
		return usuarioConverter.entidadesModelos(usuarioRepository.findAll());
	}

	public Usuario buscarPorConfirmToken(String confirmToken) {
		return usuarioRepository.buscarPorConfirmToken(confirmToken);
	}

	public List<Usuario> buscarUsuariosPorPais(Pais pais) {
		return usuarioRepository.buscarUsuariosPorPais(pais);
	}

	public List<Usuario> buscarAdministradores() {
		return usuarioRepository.buscarAdministradores();
	}

	public List<Usuario> buscarUsuariosPorProvincia(Provincia provincia) {
		return usuarioRepository.buscarUsuariosPorProvincia(provincia);
	}
//	public Integer usuariosActivos() {
//		return usuarioRepository.usuariosActivos();
//	}
//	
//	public Integer usuariosInactivos() {
//		return usuarioRepository.usuariosInactivos();
//	}

	public void actualizarPassword(String password, String newPassword, String repeatPassword, Usuario usuario) {
		if (PasswordEncoder.verificarPassword(password, usuario.getPassword())) {
			if (newPassword.equals(repeatPassword)) {
				usuario.setPassword(PasswordEncoder.encriptarPassword(newPassword));
				guardar(usuario, usuario, ACTUALIZAR);
			} else {
				throw new PeticionIncorrectaException("Los nuevos password ingresados no coinciden.");
			}
		} else {
			throw new PeticionIncorrectaException("El password ingresado no coincide con el actual password.");
		}

	}

}