package com.quinto.liberium.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ContabilidadEstudioConverter;
import com.quinto.liberium.entity.ContabilidadEstudio;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Honorario;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoTransaccion;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MContabilidadEstudio;
import com.quinto.liberium.repository.ContabilidadEstudioRepository;

@Service
public class ContabilidadEstudioService extends SimpleService {

	@Autowired
	private ContabilidadEstudioRepository contabilidadEstudioRepository;

	@Autowired
	private ContabilidadEstudioConverter contabilidadEstudioConverter;

	@Autowired
	private EstudioService estudioService;
	
	@Autowired
	private HonorarioService honorarioService;
	
	@Autowired
	private MiembroService miembroService;
	
	@Autowired
	private EstadisticaService estadisticaService;
	
	public MContabilidadEstudio guardar(String idEstudio, String idHonorario, String idMiembro, TipoTransaccion tipoContabilidad, String detalle, String formaPago, Date fecha, Double monto, Usuario usuario) {
		ContabilidadEstudio contabilidadEstudio = new ContabilidadEstudio();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		
		if (monto == null) {
			monto = 0.0;
		}
		
		if (tipoContabilidad.equals(TipoTransaccion.DISTRIINGRESO)) {
			validarDistibucion(idMiembro, formaPago, fecha, monto);
		} else {
			validadContable(detalle, formaPago, fecha, monto);
		}
		
		Estudio estudio = estudioService.buscarPorIdEstudio(idEstudio);
		
		if (estudio.equals(null)) {
			throw new PeticionIncorrectaException("Ocurrio un error a la hora de guardar!");
		} else {
			contabilidadEstudio.setEstudio(estudio);
		}
		
		if (!isStringNull(idHonorario)) {
			Honorario honorario = honorarioService.buscarHonorarioPorId(idHonorario);
			contabilidadEstudio.setHonorario(honorario);
		}
		
		if (!isStringNull(idMiembro)) {
			Miembro miembro = miembroService.buscarPorId(idMiembro);
			contabilidadEstudio.setMiembro(miembro);
		}
		
		contabilidadEstudio.setTipoContabilidad(tipoContabilidad);
		contabilidadEstudio.setDetalle(detalle);
		contabilidadEstudio.setTipoPago(formaPago);
		contabilidadEstudio.setFechaContabilidad(fecha);
		contabilidadEstudio.setMonto(monto);
		
		estadisticaService.guardarEstadistica(tipoContabilidad.toString() + " " + calendar.get(Calendar.YEAR), (int)Math.round(monto), usuario, calendar, estudio);
		
		return contabilidadEstudioConverter.entidadModelo(guardar(contabilidadEstudio, usuario, ACTUALIZAR));
	}
	
	public MContabilidadEstudio eliminar(String idContable, Usuario usuario){
		ContabilidadEstudio contabilidadEstudio = buscarPorId(idContable);
		return contabilidadEstudioConverter.entidadModelo(guardar(contabilidadEstudio, usuario, ELIMINAR));
	}
	
	private void validadContable(String detalle, String formaPago, Date fecha, Double monto) {
		if (isStringNull(detalle)) {
			throw new PeticionIncorrectaException("Tiene que ingresar el detalle para poder guardar!");
		}
		if (isStringNull(formaPago)) {
			throw new PeticionIncorrectaException("Tiene que ingresar una forma de pago para poder guardar!");
		}
		if (fecha == null) {
			throw new PeticionIncorrectaException("Tiene que ingresar una fecha para poder guardar!");
		}
		if (monto == 0.0) {
			throw new PeticionIncorrectaException("Tiene que ingresar un monto para poder guardar!");
		}
	}
	
	private void validarDistibucion(String idMiembro, String formaPago, Date fecha, Double monto) {
		if (isStringNull(idMiembro)) {
			throw new PeticionIncorrectaException("Tiene que seleccionar un miembro para poder guardar!");
		}
		if (isStringNull(formaPago)) {
			throw new PeticionIncorrectaException("Tiene que ingresar una forma de pago para poder guardar!");
		}
		if (fecha == null) {
			throw new PeticionIncorrectaException("Tiene que ingresar una fecha para poder guardar!");
		}
		if (monto == 0.0) {
			throw new PeticionIncorrectaException("Tiene que ingresar un monto para poder guardar!");
		}
	}
	
	@Transactional
	private ContabilidadEstudio guardar(ContabilidadEstudio contabilidadEstudio, Usuario usuario, String accion) {
		auditar(contabilidadEstudio, usuario, accion);
		return contabilidadEstudioRepository.save(contabilidadEstudio);
	}

	public ContabilidadEstudio buscarPorId(String id) {
		return contabilidadEstudioRepository.buscarPorId(id);
	}

	public List<MContabilidadEstudio> buscarPorEstudio(String idEstudio) {
		return contabilidadEstudioConverter.entidadesModelos(contabilidadEstudioRepository.buscarPorIdEstudio(idEstudio));
	}
	
	public List<MContabilidadEstudio> buscarPorEstudioEntreFechas(String idEstudio, String fechaInicio, String fechaFin){
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime startDate = formatter.parseDateTime(fechaInicio);
		DateTime endDate = formatter.parseDateTime(fechaFin);
		return contabilidadEstudioConverter.entidadesModelos(contabilidadEstudioRepository.buscarPorIdEstudioEntreDosFechas(idEstudio, startDate.toDate(), endDate.toDate()));
	}
}
