package com.quinto.liberium.service;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.CausaConverter;
import com.quinto.liberium.entity.Estadistica;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.repository.EstadisticaRepository;

@Service("estadisticaService")
public class EstadisticaService extends SimpleService{

	@Autowired
	private CausaConverter causaConverter;

	@Autowired
	private CausaService causaService;
	
	@Autowired
	private EstadisticaRepository estadisticaRepository;
	
	
	
	public Estadistica guardarEstadistica(String nombre, int cantidad, Usuario creador, Calendar fecha, Estudio estudio) {
		Locale locale = Locale.getDefault();
		String mesActual = "";
		if(fecha != null) {
			mesActual = fecha.getDisplayName(Calendar.MONTH, Calendar.SHORT, locale);
		}else {
		Calendar fechaActual = 	Calendar.getInstance();
		mesActual = fechaActual.getDisplayName(Calendar.MONTH, Calendar.SHORT, locale);
		}
		Estadistica estadistica = new Estadistica();
		String mes = "";
		if(buscarPorNombre(nombre, creador) != null) {
			estadistica = buscarPorNombre(nombre, creador);
			if(fecha != null) {
				 Map<String, Integer> cantidadPorMes = estadistica.getCantidadPorMes();
				 if (cantidadPorMes.get(mesActual) != null) {
					 cantidad = cantidadPorMes.get(mesActual) + cantidad;
				 }
				 cantidadPorMes.put(mesActual, cantidad);
				estadistica.setCantidadPorMes(cantidadPorMes);
			}else {
				for(Entry<String, Integer> key : estadistica.getCantidadPorMes().entrySet()) {
					mes = key.getKey();
					break;
				}
				estadistica.getCantidadPorMes().put(mesActual, estadistica.getCantidadPorMes().get(mes) + cantidad);
			}
			
			
		}else {
		estadistica.setNombre(nombre);
		Map<String, Integer> cantidadPorMes = new LinkedHashMap<String, Integer>();
		cantidadPorMes.put(mesActual, cantidad);
		estadistica.setCantidadPorMes(cantidadPorMes);
		}
		estadistica.setEstudio(estudio);	
		return guardar(estadistica, creador, ACTUALIZAR);
	}
	
	
	@Transactional
	private Estadistica guardar(Estadistica estadistica, Usuario usuario, String accion) {
		auditar(estadistica, usuario, accion);
		return estadisticaRepository.save(estadistica);
	}
	
    public  Map<String, Integer> estadisticasAdmin(String nombre){
    	List<Estadistica> estadisticas = buscarPorNombreAdmin(nombre);
    	Map<String, Integer> cantidadPorMes =  new LinkedHashMap<>();
    	cantidadPorMes.put("ene", 0);
    	cantidadPorMes.put("feb", 0);
    	cantidadPorMes.put("mar", 0);
    	cantidadPorMes.put("abr", 0);
    	cantidadPorMes.put("may", 0);
    	cantidadPorMes.put("jun", 0);
    	cantidadPorMes.put("jul", 0);
    	cantidadPorMes.put("ago", 0);
    	cantidadPorMes.put("sep", 0);
    	cantidadPorMes.put("oct", 0);
    	cantidadPorMes.put("nov", 0);
    	cantidadPorMes.put("dic", 0);
    	
    	
    	for (Estadistica estadistica : estadisticas) {
    		if(estadistica.getCantidadPorMes().get("ene") != null) {
    			cantidadPorMes.put("ene", cantidadPorMes.get("ene") + estadistica.getCantidadPorMes().get("ene"));
    		}
    		if(estadistica.getCantidadPorMes().get("feb") != null) {
    			cantidadPorMes.put("feb", cantidadPorMes.get("feb") + estadistica.getCantidadPorMes().get("feb"));
    		}
    		if(estadistica.getCantidadPorMes().get("mar") != null) {
    			cantidadPorMes.put("mar", cantidadPorMes.get("mar") + estadistica.getCantidadPorMes().get("mar"));
    		}
    		if(estadistica.getCantidadPorMes().get("abr") != null) {
    			cantidadPorMes.put("abr", cantidadPorMes.get("abr") + estadistica.getCantidadPorMes().get("abr"));
    	    }
    		if(estadistica.getCantidadPorMes().get("may") != null) {
    			cantidadPorMes.put("may", cantidadPorMes.get("may") + estadistica.getCantidadPorMes().get("may"));
    		}
    		if(estadistica.getCantidadPorMes().get("jun") != null) {
    			cantidadPorMes.put("jun", cantidadPorMes.get("jun") + estadistica.getCantidadPorMes().get("jun"));
    	    }
    		if(estadistica.getCantidadPorMes().get("jul") != null) {
    			cantidadPorMes.put("jul", cantidadPorMes.get("jul") + estadistica.getCantidadPorMes().get("jul"));
    		}
    		if(estadistica.getCantidadPorMes().get("ago") != null) {
    			cantidadPorMes.put("ago", cantidadPorMes.get("ago") + estadistica.getCantidadPorMes().get("ago"));
    		}
    		if(estadistica.getCantidadPorMes().get("sep") != null) {
    			cantidadPorMes.put("sep", cantidadPorMes.get("sep") + estadistica.getCantidadPorMes().get("sep"));
    		}
    		if(estadistica.getCantidadPorMes().get("oct.") != null) {
    			cantidadPorMes.put("oct", cantidadPorMes.get("oct") + estadistica.getCantidadPorMes().get("oct."));
    		}
    		if(estadistica.getCantidadPorMes().get("nov") != null) {
    			cantidadPorMes.put("nov", cantidadPorMes.get("nov") + estadistica.getCantidadPorMes().get("nov"));
    		}
    		if(estadistica.getCantidadPorMes().get("dic") != null) {
    			cantidadPorMes.put("dic", cantidadPorMes.get("dic") + estadistica.getCantidadPorMes().get("dic"));
    		}
			
		}
    	
    	return cantidadPorMes;
    }
	
	public Estadistica buscarPorNombre(String nombre, Usuario usuario) {
		return estadisticaRepository.buscarEstadisticasPorNombre(nombre, usuario);
	}
	
	public List<Estadistica> buscarPorNombreAdmin(String nombre) {
		return estadisticaRepository.buscarEstadisticasPorNombreAdmin(nombre);
	}

}