package com.quinto.liberium.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.quinto.liberium.converter.ItemConverter;
import com.quinto.liberium.converter.SubItemConverter;
import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.MItem;
import com.quinto.liberium.model.MSubItem;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.repository.ItemRepository;
import com.quinto.liberium.repository.SubItemRepository;

@Service
public class ItemService extends SimpleService {

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private ItemConverter itemConverter;

	@Autowired
	private SubItemRepository subItemRepository;

	@Autowired
	private CausaService causaService;

	@Autowired
	private ArchivoService archivoService;

	@Autowired
	private CausaRepository causaRepository;

	@Autowired
	private ModificacionService modificacionService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private EscritoService escritoService;

	@Autowired
	private SubItemConverter subItemConverter;

	private void validar(Item modelo) throws PeticionIncorrectaException {
		if (modelo.getNombre() == null || modelo.getNombre().isEmpty()) {
			throw new PeticionIncorrectaException("El titulo del paso no puede ser nulo.");
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class,
			Exception.class })
	public List<Item> gestionarItem(HttpServletRequest request, Checklist checklist, Usuario usuario)
			throws WebException {
		List<Item> items = new ArrayList<>();
		for (String key : request.getParameterMap().keySet()) {
			if (key.startsWith("nombreItem")) {
				String ordenStr = key.split("-")[1];
				String eliminador = request.getParameter("eliminado-" + ordenStr);
				if (eliminador == null || eliminador.isEmpty()) {
					String nombre = request.getParameter("nombreItem-" + ordenStr);
					String id = request.getParameter("idItem-" + ordenStr);
					String orden = request.getParameter("orden-" + ordenStr);
					String limite = request.getParameter("limite-" + ordenStr);
					String habil = request.getParameter("habil-" + ordenStr);

					if (!validar(nombre, orden)) {
						Item item = null;
						List<Archivo> archivos = new ArrayList<Archivo>();
						if (id != null && !id.isEmpty()) {
							item = buscarItemPorId(id);
							if (item.getArchivos() != null) {
								archivos = item.getArchivos();
							}
						} else {
							item = new Item();
							String idItemClonado = request.getParameter("idItemClonado-" + ordenStr);
							if (!isStringNull(idItemClonado)) {
								try {
									archivos = archivoService.clonarArchivos(idItemClonado, archivos, "Item", usuario);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}

						item.setNombre(nombre);
						item.setOrden(new Integer(orden));
						item.setCheckList(checklist);

						List<SubItem> subItems = subItemService.generarSubItem(request, ordenStr, usuario);
						item.setSubItems(subItems);

						archivos = archivoService.obtenerArchivosChecklist(request, ordenStr, archivos, usuario);
						item.setArchivos(archivos);

						List<Escrito> escritos = escritoService.obternerEscritosChecklist(request, ordenStr);
						item.setEscritos(escritos);

						if (limite != null && !limite.isEmpty()) {
							item.setLimite(new Integer(limite));
							item.setHabil(habil != null && !habil.isEmpty() && habil.equals("true") ? true : false);
						}

						item = guardar(item, usuario, ACTUALIZAR);
						items.add(item);
					}
				}
			}
		}
		if(items.isEmpty()) {
			
			Item item = new Item();
			item.setNombre("Paso 1");
			item.setOrden(1);
			item.setCheckList(checklist);
			
			List<SubItem> subItems = subItemService.generarSubItem(request, "1", usuario);
			item.setSubItems(subItems);
			
			item = guardar(item, usuario, ACTUALIZAR);
		
			items.add(item);
		}
		return items;
	}

	private boolean validar(String nombre, String orden) {
		if (nombre == null || nombre.isEmpty()) {
			throw new PeticionIncorrectaException("El titulo del paso no puede estar vacío.");
		} else if (orden == null || orden.isEmpty()) {
			throw new PeticionIncorrectaException(
					"Error al generar el orden del checklist, comuniquese con soporte técnico.");
		}
		return false;
	}

	public List<Item> clonar(String idChecklist, Checklist checklist, Usuario creador) throws IOException {
		List<Item> items = buscarPorChecklistId(idChecklist);
		List<Item> itemsClonados = new ArrayList<>();
		if (items.size() > 0) {

			for (Item item : items) {
				Item itemCopia = new Item();
				itemCopia.setNombre(item.getNombre());
				itemCopia.setOrden(item.getOrden());

				if (!item.getSubItems().isEmpty()) {
					List<SubItem> subItemsClonados = subItemService.clonar(item.getSubItems(), creador);
					itemCopia.setSubItems(subItemsClonados);
				}

				if (item.getArchivos() != null && item.getArchivos().size() > 0) {
					List<Archivo> archivos = new ArrayList<>();
					archivos = archivoService.clonarArchivos(item.getId(), archivos, "Item", creador);
					itemCopia.setArchivos(archivos);
				}

				if (item.getEscritos() != null && item.getEscritos().size() > 0) {
					itemCopia.setEscritos(new ArrayList<>(item.getEscritos()));
				}

				itemCopia.setCheckList(checklist);

				itemCopia = guardar(itemCopia, creador, ACTUALIZAR);
				itemsClonados.add(itemCopia);

			}
		}
		return itemsClonados;

	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class,
			Exception.class })
	private Item guardar(Item item, Usuario usuario, String accion) {
		auditar(item, usuario, accion);
		return itemRepository.save(item);
	}

	@Transactional
	public void guardar(Collection<Item> items, Checklist checklist) {
		for (Item item : items) {
			validar(item);

			List<SubItem> subItems = new ArrayList<>();
			for (SubItem sub : item.getSubItems()) {
				if (sub.getNombre() == null || sub.getNombre().isEmpty()) {
					throw new PeticionIncorrectaException("El titulo del sub paso no puede estar vacío.");
				}

				auditar(sub, getUsuarioSession(), ACTUALIZAR);
				sub = subItemRepository.save(sub);
				subItems.add(sub);
			}

			item.setSubItems(subItems);
			item.setCheckList(checklist);
			auditar(item, getUsuarioSession(), ACTUALIZAR);
			itemRepository.save(item);
		}
	}

	public void editarr(Collection<Item> items, Checklist checklist) {
		for (Item item : items) {
			Item original = itemRepository.buscarPorId(item.getId());
			validar(item);

			List<SubItem> subItems = new ArrayList<>();
			for (SubItem opcion : item.getSubItems()) {
				if (opcion.getNombre() == null || opcion.getNombre().isEmpty()) {
					throw new PeticionIncorrectaException("El titulo del subItem no puede ser nulo o vacío.");
				}

				opcion = subItemRepository.save(opcion);
				subItems.add(opcion);
			}
			original.setNombre(item.getNombre());
			original.setSubItems(subItems);
			original.setCheckList(checklist);

			itemRepository.save(original);

		}
	}

	@Transactional
	public void ordenar(String idFormulario, int orden) {

		List<Item> item = itemRepository.buscarMismoOrden(idFormulario, orden);
		if (!item.isEmpty()) {
			List<Item> mayores = itemRepository.buscarMayores(idFormulario, orden);

			for (Item mayor : mayores) {
				mayor.setOrden(mayor.getOrden() + 1);
				itemRepository.save(mayor);
			}
		}
	}

	public int ultimoNumero(String idFormulario) {
		return itemRepository.buscarMaximoOrden(idFormulario);
	}

//	public List<MItem> guardar(List<String> id, List<String> nombres, String idChecklist, Usuario usuario) {
//		List<Item> todos = new ArrayList<>();
//		
//		if (nombres.size() != 0) {
//			
//		
//			for (int i = 0; i < nombres.size(); i++) {
//				Item item = new Item();
//				
//				String aId = id.get(i);
//				String nombre = nombres.get(i);
//				
//				item.setNombre(nombre);
//				item.setCheckList(checklistRepository.buscarPorId(idChecklist));
//				item.setCreado(new Date());
//				item.setCreador(usuario);
//
//				todos.add(item);
//		}
//	
//		
//		for (Item item : todos) {
//			guardar(item, usuario, ACTUALIZAR);
//		}
//		}
//		return itemConverter.entidadesModelos(todos);
//	}
//
//	@Transactional
//	private Item guardar(Item item, Usuario usuario, String accion) {
//		auditar(item, usuario, accion);
//		return itemRepository.save(item);
//	}
//
	@Transactional
	private Item eliminar(Item item, Usuario usuario, String accion) {
		auditar(item, usuario, accion);
		return itemRepository.save(item);
	}

	public MItem eliminarItem(String id, Usuario usuario) {
		Item item = buscarItemPorId(id);
		if (item == null) {
			throw new EventoRestException("El item que trata eliminar no se encuentra en la base de datos.");
		}

		List<SubItem> subItems = item.getSubItems();

		subItemService.eliminar(subItems, usuario);

		item = eliminar(item, usuario, SimpleService.ELIMINAR);
		return itemConverter.entidadModelo(item);
	}

	public Item checkear(String idItem, String idCausa, Usuario usuario) {
		Causa causa = causaService.buscarPorId(idCausa);
		Item item = buscarItemPorId(idItem);
		if (!item.isEstado()) {
			item.setEstado(true);
			modificacionService.guardarModificacion("Se completó el paso " + item.getOrden(), causa, usuario);
		} else {
			item.setEstado(false);
		}
		itemRepository.save(item);
		causaService.estado(causa);
		return item;
	}

	// AGREGAR UN ITEM A LA CAUSA
	public MItem agregarItem(String id, String titulo, Integer ordenAnterior, String idCausa, Usuario creador) {
		Item item = new Item();
		Integer orden = 0;
		Causa causa = causaService.buscarPorId(idCausa);
		if (limiteCaracteres(titulo)) {
			throw new PeticionIncorrectaException("El titulo del paso excede el limite de caracteres");
		}
		if (!isStringNull(id)) {
			item = buscarItemPorId(id);
		} else {
			orden = ordenAnterior + 1;
			item.setOrden(orden);
			item.setCheckList(causa.getItems().get(0).getCheckList());
		}
		item.setNombre(titulo);
		auditar(item, creador, ACTUALIZAR);
		itemRepository.save(item);
		if (isStringNull(id)) {
			for (Item itemOrden : causa.getItems()) {
				if (itemOrden.getOrden() >= orden) {
					itemOrden.setOrden(itemOrden.getOrden() + 1);
					itemRepository.save(itemOrden);
				}
			}
			modificacionService.guardarModificacion("Se añadio un paso a la causa", causa, creador);
			causa.getItems().add(item);
		} else {
			modificacionService.guardarModificacion("Se edito el paso " + item.getOrden(), causa, creador);
		}
		causaRepository.save(causa);
		causaService.estado(causa);
		return itemConverter.entidadModelo(item);
	}

	// AGREGAR UN SUB ITEM A LA CAUSA
	public MSubItem agregarSubItem(String id, String titulo, String idItem, Integer ordenAnterior, String idCausa,
			Usuario creador) {
		SubItem subItem = new SubItem();
		Item item = buscarItemPorId(idItem);
		Causa causa = causaService.buscarPorId(idCausa);
		if (!isStringNull(id)) {
			subItem = subItemService.buscarSubItemPorId(id);
		} else {
			if (ordenAnterior != null) {
				subItem.setOrden(ordenAnterior + 1);
			} else {
				subItem.setOrden(item.getSubItems().size() + 1);
			}
		}
		subItem.setNombre(titulo);
		subItemService.auditar(subItem, creador, ACTUALIZAR);
		subItemRepository.save(subItem);
		if (isStringNull(id)) {
			if (subItem.getOrden() <= item.getSubItems().size()) {
				for (SubItem subItemOrden : item.getSubItems()) {
					if (subItemOrden.getOrden() >= subItem.getOrden()) {
						subItemOrden.setOrden(subItemOrden.getOrden() + 1);
						subItemRepository.save(subItemOrden);
					}
				}
			}
			modificacionService.guardarModificacion("Se añadio un sub paso al paso " + item.getOrden(), causa, creador);
			item.getSubItems().add(subItem);
		} else {
			modificacionService.guardarModificacion(
					"Se edito el sub paso " + subItem.getOrden() + " del paso " + item.getOrden(), causa, creador);
		}

		itemRepository.save(item);

		return subItemConverter.entidadModelo(subItem);
	}

	public void guardarArchivos(Item item, List<Archivo> archivos) {
		item.setArchivos(archivos);
		itemRepository.save(item);
	}

	public void guardarEscrito(Item item, String idEscrito) {
		Escrito escrito = escritoService.buscarEscritoPorId(idEscrito);
		if (item.getEscritos() != null) {
			item.getEscritos().add(escrito);
		} else {
			List<Escrito> escritos = new ArrayList<>();
			item.setEscritos(escritos);
		}
		itemRepository.save(item);
	}

	public List<Archivo> verArchivosItem(String id) {
		Item item = buscarItemPorId(id);
		List<Archivo> archivos = item.getArchivos();
		return archivos;
	}

	public List<Archivo> verArchivosSubItem(String id) {
		SubItem subItem = subItemService.buscarSubItemPorId(id);
		List<Archivo> archivos = subItem.getArchivos();
		return archivos;
	}

	public List<Escrito> verEscritosItem(String id) {
		Item item = buscarItemPorId(id);
		List<Escrito> escritos = item.getEscritos();
		return escritos;
	}

	public List<Escrito> verEscritosSubItem(String id) {
		SubItem subItem = subItemService.buscarSubItemPorId(id);
		List<Escrito> escritos = subItem.getEscritos();
		return escritos;
	}

	public Item buscarItemPorId(String id) {
		return itemRepository.buscarPorId(id);
	}

	public Item buscarItemPorNombre(String nombre) {
		return itemRepository.buscarPorNombre(nombre);
	}

	public List<Item> buscarPorChecklistId(String idCheck) {
		return itemRepository.buscarPorChecklistId(idCheck);
	}
	
	public List<MItem> buscarModelPorChecklistId(String idCheck) {
		return itemConverter.entidadesModelos(itemRepository.buscarPorChecklistId(idCheck));
	}

	public List<MItem> buscarTodos(Pageable paginable) {
		return itemConverter.entidadesModelos(itemRepository.buscarTodos(paginable));
	}
}
