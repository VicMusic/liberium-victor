package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.HonorarioConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Honorario;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MHonorario;
import com.quinto.liberium.repository.HonorarioRepository;

@Service("honorarioService")
public class HonorarioService extends SimpleService{
	
	@Autowired
	private HonorarioRepository honorarioRepository;
	
	@Autowired
	private HonorarioConverter honorarioConverter;
	
	@Autowired
	private CausaService causaService;
	
	@Autowired
	private EstudioService estudioService;
	
	public MHonorario guardarHonorario(String id, String nombre, String tipoHonorario, String idCausa, String idEstudio, Usuario usuario) {
		
		Honorario honorario = new Honorario();
		
		if (!isStringNull(id)) {
			honorario = buscarHonorarioPorId(id);
		}
		
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de esta seccion no puede estar vacio!");
		}
		
		if (!isStringNull(idCausa)) {
			Causa causa = causaService.buscarPorId(idCausa);
			honorario.setCausa(causa);
		}
		
		if (!isStringNull(idEstudio)) {
			Estudio estudio = estudioService.buscarPorIdEstudio(idEstudio);
			honorario.setTipoHonorario(tipoHonorario);
			honorario.setEstudio(estudio);
		}
		
		
		honorario.setNombre(nombre);
		
		return honorarioConverter.entidadModelo(guardar(honorario, usuario, ACTUALIZAR));
	}
	
	public MHonorario setTipoInstancia(String idHonorario, boolean tipoInstancia, Usuario usuario) {
		
		Honorario honorario = buscarHonorarioPorId(idHonorario);
		
		honorario.setTipoInstancia(tipoInstancia);
		
		return honorarioConverter.entidadModelo(guardar(honorario, usuario, ACTUALIZAR));
	}
	
	public MHonorario eliminar(String idHonorario, Usuario usuario) {
		Honorario honorario = buscarHonorarioPorId(idHonorario);
		return honorarioConverter.entidadModelo(guardar(honorario, usuario, ELIMINAR));
	}
	
	@Transactional
	private Honorario guardar(Honorario honorario, Usuario usuario, String accion) {
		auditar(honorario, usuario, accion);
		return honorarioRepository.save(honorario);
	}
	
	public Honorario buscarHonorarioPorId(String idHonorario) {
		return honorarioRepository.buscarHonorarioPorId(idHonorario);
	}
	
	public List<MHonorario> buscarHonorariosPorCausa(String idCausa){
		return honorarioConverter.entidadesModelos(honorarioRepository.buscarHonorarioPorCausa(idCausa));
	}
	
	public List<MHonorario> buscarHonorariosPorEstudio(String idEstudio){
		return honorarioConverter.entidadesModelos(honorarioRepository.buscarHonorarioPorEstudio(idEstudio));
	}
}
