package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ProvinciaConverter;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MProvincia;
import com.quinto.liberium.repository.PaisRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service("provinciaService")
public class ProvinciaService extends SimpleService {

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private ProvinciaConverter provinciaConverter;

	public MProvincia guardar(String id, String nombre, String idPais, Usuario usuario) {
		Provincia provincia = new Provincia();

		if (!isStringNull(id)) {
			provincia = buscarProvinciaPorId(id);
			validarProvincia(nombre, idPais, provincia);
		} else {
			validarProvincia(nombre, idPais, null);
		}

		provincia.setNombre(nombre);

		Pais pais = paisRepository.buscarPorId(idPais);
		provincia.setPais(pais);

		return provinciaConverter.entidadModelo(guardar(provincia, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Provincia guardar(Provincia provincia, Usuario usuario, String accion) {
		auditar(provincia, usuario, accion);
		return provinciaRepository.save(provincia);
	}

	public MProvincia eliminarProvincia(String id, Usuario usuario) {
		Provincia provincia = buscarProvinciaPorId(id);
		if (provincia == null) {
			throw new EventoRestException("La provincia que trata eliminar no se encuentra en la base de datos.");
		}

		provincia = guardar(provincia, usuario, SimpleService.ELIMINAR);
		return provinciaConverter.entidadModelo(provincia);
	}

	private void validarProvincia(String nombre, String idPais, Provincia provincia) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la provincia no puede estar vacio.");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El país al que pertenece la provincia no puede ser nulo.");
		}
		List<Provincia> provincias = buscarProvinciasRepetidasPais(idPais, nombre);
		
		if (provincias.size() >= 1) {
			throw new PeticionIncorrectaException("Ya existe una provincia con ese nombre en ese pais.");
		}
		
	}

	public List<MProvincia> buscarProvinciaModelTodos() {
		return provinciaConverter.entidadesModelos(provinciaRepository.buscarTodos(null).getContent());
	}

	public Page<Provincia> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return provinciaRepository.buscarTodos(paginable);
		} else {
			return provinciaRepository.buscarTodos(paginable, "%" + q + "%");
		}

	}

	public MProvincia buscarProvinciaModelPorId(String idProvincia) {
		return provinciaConverter.entidadModelo(provinciaRepository.buscarPorId(idProvincia));
	}

	public Provincia buscarProvinciaPorId(String idProvincia) {
		return provinciaRepository.buscarPorId(idProvincia);
	}

	public MProvincia buscarProvinciaModelPorNombre(String nombre) {
		return provinciaConverter.entidadModelo(provinciaRepository.buscarPorNombre(nombre));
	}

	public Provincia buscarProvinciaPorNombre(String nombre) {
		return provinciaRepository.buscarPorNombre(nombre);
	}

	public List<MProvincia> buscarProvinciaModelPorIdPais(String idPais) {
		return provinciaConverter.entidadesModelos((provinciaRepository.buscarPorIdPais(idPais)));
	}

	public List<Provincia> buscarProvinciaPorIdPais(String idPais) {
		return provinciaRepository.buscarPorIdPais(idPais);
	}

	public List<Provincia> buscarProvinciasRepetidasPais(String idPais, String nombre){
		return provinciaRepository.buscarProvinciasRepetidasPais(idPais, nombre);
	}
}