package com.quinto.liberium.service;

import static com.quinto.liberium.util.Constantes.PUERTO_ARCHIVOS;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.quinto.liberium.converter.ArchivoConverter;
import com.quinto.liberium.entity.Archivo;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.WebException;
import com.quinto.liberium.model.MArchivo;
import com.quinto.liberium.repository.ArchivoRepository;
import com.quinto.liberium.repository.ItemRepository;
import com.quinto.liberium.repository.SubItemRepository;
import com.quinto.liberium.util.Configuracion;

@Service("archivoService")
public class ArchivoService extends SimpleService {

	private final Log LOG = LogFactory.getLog(ArchivoService.class);
	
	@Autowired
	private Configuracion configuracion;

	@Autowired
	private ArchivoRepository archivoRepository;
	@Autowired
	private ArchivoConverter archivoConverter;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private SubItemRepository subItemRepository;

	@Autowired
	private FileService fileService;
	
	@Autowired
	private ModificacionService modificacionService;
	
	@Autowired
	private CausaService causaService;

	public List<Archivo> obtenerArchivosChecklist(HttpServletRequest request, String ordenStr, List<Archivo> archivos, Usuario usuario) throws WebException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> multipartFiles = multipartRequest.getMultiFileMap().get("files-" + ordenStr);
		if (multipartFiles != null) {
			System.out.println("ARCHIVO PASO: " + ordenStr);
			for (MultipartFile multipartFile : multipartFiles) {
				Archivo archivo = archivoRepository.save(new Archivo());
				archivo = guardar(archivo, multipartFile, usuario);
				archivos.add(archivo);
			}
		}
		return archivos;
	}

	public Archivo guardar(Archivo archivo, MultipartFile multipartFile, Usuario usuario) throws WebException {
		try {

			File directorio = new File(configuracion.getPathArchivos());
			if (!directorio.exists()) {
				directorio.mkdir();
			}

			File imagen = new File(configuracion.getPathArchivos()  + archivo.getId());
			multipartFile.transferTo(imagen);

			archivo.setRuta(configuracion.getPathArchivos() + archivo.getId());
			archivo.setNombre(multipartFile.getOriginalFilename());
			archivo.setContentType(multipartFile.getContentType());
			
			auditar(archivo, usuario, ACTUALIZAR);
			return archivoRepository.save(archivo);
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
			throw new WebException("Error al guardar archivo.");
		}
	}
	
	public List<Archivo> clonarArchivos(String idItemClonado, List<Archivo> archivos, String className, Usuario usuario) throws IOException{
		if(className.equals("Item")) {
			Item item = itemRepository.buscarPorId(idItemClonado);
			if(item != null) {
				archivos = clonar(item.getArchivos(), usuario);
			}
		}else if(className.equals("SubItem")) {
			SubItem sub = subItemRepository.buscarPorId(idItemClonado);
			if(sub != null) {
				archivos = clonar(sub.getArchivos(), usuario);
			}
		}
		return archivos;
	}
	
	
	private List<Archivo> clonar(List<Archivo> archivos, Usuario usuario) throws IOException {
		List<Archivo> archivosNuevos = new ArrayList<Archivo>();
		for (Archivo archivoOriginal : archivos) {
			Archivo archivo = archivoRepository.save(new Archivo());
			FileUtils.copyFile(new File(configuracion.getPathArchivos() + archivoOriginal.getId()), new File(configuracion.getPathArchivos()  + archivo.getId()));
			
			archivo.setContentType(archivoOriginal.getContentType());
			archivo.setNombre(archivoOriginal.getNombre());
			archivo.setRuta(configuracion.getPathArchivos() + archivo.getId());
			
			auditar(archivo, usuario, ACTUALIZAR);
			archivo = archivoRepository.save(archivo);
			archivosNuevos.add(archivo);
			
		}
		
		return archivosNuevos;
	}

	public ResponseEntity<byte[]> abrirMultimedia(Archivo archivo,String name,String ext) {

		byte[] multimedia = new byte[] {};
		HttpHeaders headers = new HttpHeaders();
		try {
			if(archivo != null && archivo.getContentType() != null) {
				MediaType media = MediaType.parseMediaType(archivo.getContentType());
				headers.setContentType(media);
				headers.set("Content-Disposition", "attachment;filename=" + name + "." + ext);
		
				multimedia = IOUtils.toByteArray(new FileInputStream(archivo.getRuta()));
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}

		return new ResponseEntity<>(multimedia, headers, HttpStatus.OK);
	}
	
	
	
	

	public MArchivo guardar(String idItem, String tipo, MultipartFile archivo, String idCausa, Usuario usuario) {
		LOG.info("METHOD: guardar() -- PARAMS: idItem= " + idItem + ", Tipo= " + tipo + ", archivo="
				+ archivo.toString());
		Archivo entidad = new Archivo();

		Causa causa = causaService.buscarPorId(idCausa);
		
		String rutaCompleta = fileService.confirmarArchivo(PUERTO_ARCHIVOS, archivo.getOriginalFilename(), "");

		// acá buscar el nombre del archivo junto con su extención-,
		entidad.setRuta(rutaCompleta.substring(rutaCompleta.lastIndexOf("\\") + 1));

		try {
			Path folder = Paths.get(rutaCompleta);
			Path fileToSavePath = Files.createFile(folder);

			// Copy file to server
			InputStream input = archivo.getInputStream();

			Files.copy(input, fileToSavePath, StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
			throw new EventoRestException("Se ha producio un error al Intentar guardar el Archivo... ");
		}

		entidad = guardar(entidad, usuario, SimpleService.ACTUALIZAR);

		// lo relaciono lo conecto a item/subitem.
		switch (tipo) {
		case "ITEM":
			Item item = itemRepository.buscarPorId(idItem);
			item.getArchivos().add(entidad);
			itemRepository.save(item);
		    modificacionService.guardarModificacion("Se agrego un archivo al paso " + item.getOrden(), causa, usuario);	
			break;
		case "SUBITEM":
			SubItem subItem = subItemRepository.buscarPorId(idItem);
			subItem.getArchivos().add(entidad);
			subItemRepository.save(subItem);
			modificacionService.guardarModificacion("Se agrego un archivo al sub paso " + subItem.getOrden(), causa, usuario);
			break;

		default:
			break;
		}
		return archivoConverter.entidadModelo(entidad);
	}

	public MArchivo eliminarArchivo(String idArchivo, String idItem, String tipo, Usuario usuario) {
		Archivo entidad = new Archivo();
		entidad = eliminar(idArchivo, usuario);
		switch (tipo) {
		case "ITEM":
			Item item = itemRepository.buscarPorId(idItem);
			if (item != null) {
				item.setArchivos(eliminarDelaLista(item.getArchivos(), entidad));
				itemRepository.save(item);
			}
			break;

		case "SUBITEM":
			SubItem subItem = subItemRepository.buscarPorId(idItem);
			if (subItem != null) {
				subItem.setArchivos(eliminarDelaLista(subItem.getArchivos(), entidad));
				subItemRepository.save(subItem);
			}
			break;
		default:
			break;
		}
		return archivoConverter.entidadModelo(entidad);
	}

	private Archivo eliminar(String idArchivo, Usuario usuario) {
		Archivo entidad = archivoRepository.buscarPorId(idArchivo);
		Path path = Paths.get(configuracion.getPathArchivos() + entidad.getId());
		try {
			Files.delete(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		guardar(entidad, usuario, SimpleService.ELIMINAR);
		return entidad;
	}

	private List<Archivo> eliminarDelaLista(List<Archivo> destino, Archivo archivo) {
		if (destino.contains(archivo)) {
			destino.remove(archivo);
		}
		return destino;
	}
	
	public void descargarArchivo(String idArchivo, HttpServletResponse response) {
		Archivo entidad = archivoRepository.buscarPorId(idArchivo);
		if (entidad != null) {
			String rutaCompleta = entidad.getRuta();
			fileService.descargar(rutaCompleta, response,entidad.getNombre().substring(entidad.getNombre().lastIndexOf(".") + 1).toUpperCase(),entidad.getNombre().substring(0, entidad.getNombre().lastIndexOf(".")));
		}
	}
	
	@Transactional
	private Archivo guardar(Archivo archivo, Usuario usuario, String accion) {
		auditar(archivo, usuario, accion);
		return archivoRepository.save(archivo);
	}

	public Archivo buscarPorId(String idArchivo) {
		return archivoRepository.buscarPorId(idArchivo);
	}

	public MArchivo buscarModeloPorId(String idArchivo) {
		return archivoConverter.entidadModelo(archivoRepository.buscarPorId(idArchivo));
	}

	public List<Archivo> buscarPorIdUsuario(String idUsuario) {
		return archivoRepository.buscarTodosIdUsuario(idUsuario);
	}

	public List<MArchivo> buscarModelosPorIdUsuario(String idUsuario) {
		return archivoConverter.entidadesModelos(archivoRepository.buscarTodosIdUsuario(idUsuario));
	}

	public List<Archivo> buscarTodos() {
		return archivoRepository.buscarTodos();
	}

	public List<MArchivo> buscarModeloTodos() {
		return archivoConverter.entidadesModelos(archivoRepository.buscarTodos());
	}

	public Page<Archivo> buscarPorIdUsuarioPageable(Pageable paginable, String idUsuario) {
		return archivoRepository.buscarTodasIdUsuario(paginable, idUsuario);
	}

	public Page<Archivo> buscarTodos(Pageable paginable) {
		return archivoRepository.buscarTodas(paginable);
	}

}
