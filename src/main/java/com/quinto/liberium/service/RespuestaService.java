package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.RespuestaConverter;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.entity.Respuesta;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.RespuestaM;
import com.quinto.liberium.repository.RespuestaRepository;

@Service("respuestaService")
public class RespuestaService extends SimpleService{

	@Autowired
	private RespuestaRepository respuestaRepository;
	
	@Autowired
	private RespuestaConverter respuestaConverter;
	
	@Autowired
	private ConsultaService consultaService;
	
	public RespuestaM guardar(String stringRespuesta, Pregunta pregunta, int orden, String idConsulta) {
		
		validarRespuesta(stringRespuesta, idConsulta);
		
		Respuesta respuesta = new Respuesta();
		
		Consulta consulta = consultaService.buscarPorId(idConsulta);
		
		respuesta.setConsulta(consulta);
		respuesta.setRespuesta(stringRespuesta);
		respuesta.setOrden(orden);
		respuesta.setPregunta(pregunta);
		
		return respuestaConverter.entidadModelo(guardar(respuesta, null, SimpleService.ACTUALIZAR));
	}
	
	@Transactional
	private Respuesta guardar(Respuesta respuesta, Usuario usuario, String accion) {
		auditar(respuesta, usuario, accion);
		return respuestaRepository.save(respuesta);
	}
	
	private void validarRespuesta(String stringRespuesta, String idConsulta) {

		if (isStringNull(stringRespuesta)) {
			throw new PeticionIncorrectaException("La respuesta a la pregunta no puede estar vacia.");
		}

		if (isStringNull(idConsulta)) {
			throw new PeticionIncorrectaException("La respuesta a la pregunta no pertenece a ninguna consulta.");
		} 

	}

	public RespuestaM buscarRespuestaModelPorId(String idResouesta) {
		return respuestaConverter.entidadModelo(respuestaRepository.buscarPorId(idResouesta));
	}
	
	public Respuesta buscarRespuestaPorId(String idResouesta) {
		return respuestaRepository.buscarPorId(idResouesta);
	}
	
	public List<RespuestaM> buscarRespuestaPorIdConsulta(String idConsulta) {
		return respuestaConverter.entidadesModelos(respuestaRepository.buscarPorIdConsulta(idConsulta));
	}
	
	public List<Respuesta> buscarRespuestaPorIdConsulta2(String idConsulta){
		return respuestaRepository.buscarPorIdConsulta(idConsulta);
	}
	
	
	
}
