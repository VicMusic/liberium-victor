package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.DependenciaConverter;
import com.quinto.liberium.entity.Dependencia;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.TipoDependencia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MDependencia;
import com.quinto.liberium.repository.DependenciaRepository;
import com.quinto.liberium.repository.InstanciaRepository;
import com.quinto.liberium.repository.TipoDependenciaRepository;

@Service("dependenciaService")
public class DependenciaService extends SimpleService {

	@Autowired
	private DependenciaRepository dependenciaRepository;

	@Autowired
	private TipoDependenciaRepository tipoDependenciaRepository;

	@Autowired
	private InstanciaRepository instanciaRepository;

	@Autowired
	private DependenciaConverter dependenciaConverter;

	
	public MDependencia guardar(String id, String nombre, String idTipoDependencia, String idInstancia,  Usuario creador) {
		Dependencia dependencia = new Dependencia();

		if (!isStringNull(id)) {
			dependencia = buscarDependenciaPorId(id);
			validarDependencia(nombre, idTipoDependencia, idInstancia, dependencia);
		}else{
			validarDependencia(nombre, idTipoDependencia, idInstancia, null);
		}
		
		TipoDependencia tipoDependencia = tipoDependenciaRepository.buscarPorId(idTipoDependencia);
	    Instancia instancia = instanciaRepository.buscarPorId(idInstancia);
	    
	    dependencia.setNombre(nombre);
	    dependencia.setTipoDependencia(tipoDependencia);
	    dependencia.setInstancia(instancia);
	    
	  
		return dependenciaConverter.entidadModelo(guardar(dependencia, creador, SimpleService.ACTUALIZAR));
	}

	
	private void validarDependencia(String nombre, String idTipoDependencia, String idInstancia, Dependencia dependencia) {
		if(isStringNull(nombre)){
			throw new PeticionIncorrectaException("El nombre de la dependencia no puede estar vacio!");
		}
		
		
		if(isStringNull(idTipoDependencia)){
			throw new PeticionIncorrectaException("La TipoDependencia de la dependencia no puede ser nulo!");
		}
		if (isStringNull(idInstancia)) {
			throw new PeticionIncorrectaException("La Instancia de la dependencia no puede ser nulo!");
		}else{
			Dependencia otra = buscarDependenciaPorNombre(nombre);
			if ((dependencia == null && otra != null) || (dependencia != null && otra != null && !dependencia.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuantra utilizado");
			}
		}
	}
	
	@Transactional
	private Dependencia guardar(Dependencia dependencia, Usuario usuario, String accion) {
		auditar(dependencia, usuario, accion);
		return dependenciaRepository.save(dependencia);
	}

	public MDependencia eliminar(String idDependencia, Usuario eliminador) {
		Dependencia dependencia = buscarDependenciaPorId(idDependencia);
		if (dependencia == null) {
			throw new PeticionIncorrectaException("La Dependencia que intenta eliminar no se encontró en la base de datos");
		}
		dependencia = guardar(dependencia, eliminador, SimpleService.ELIMINAR);///<------- cambiar la manera de guardar?
		return dependenciaConverter.entidadModelo(dependencia);

	}

	public Page<Dependencia> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return dependenciaRepository.buscarTodas(paginable);
		} else {
			return dependenciaRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public List<MDependencia> buscarDependenciaModelTodos() {
		return dependenciaConverter.entidadesModelos(dependenciaRepository.buscarTodos());
	}

	public List<Dependencia> buscarDependenciaTodos() {
		return dependenciaRepository.buscarTodos();
	}

	public Dependencia buscarDependenciaPorId(String idDependencia) {
		return dependenciaRepository.buscarPorId(idDependencia);
	}
	public MDependencia buscarDependenciaModelPorId(String idDependencia){
		return dependenciaConverter.entidadModelo(dependenciaRepository.buscarPorId(idDependencia));
	}

	public MDependencia buscarDependenciaModelPorNombre(String nombre) {
		return dependenciaConverter.entidadModelo(dependenciaRepository.buscarPorNombre(nombre));
	}

	public Dependencia buscarDependenciaPorNombre(String nombre) {
		return dependenciaRepository.buscarPorNombre(nombre);
	}


	public List<Dependencia> buscarDependenciaPorIdTipoDependencia(String idTipoDependencia){
		return dependenciaRepository.buscarPorIdTipoDependencia(idTipoDependencia);
	}
	public List<MDependencia> buscarDependenciaModelPorIdTipoDependencia(String idTipoDependencia) {
		return dependenciaConverter.entidadesModelos(dependenciaRepository.buscarPorIdTipoDependencia(idTipoDependencia));
	}

	public List<Dependencia> buscarDependenciaPorIdInstancia(String idInstancia) {
		return dependenciaRepository.buscarPorIdInstancia(idInstancia);
	}

	public List<MDependencia> buscarDependenciaModelPorIdInstancia(String idInstancia) {
		return dependenciaConverter.entidadesModelos(dependenciaRepository.buscarPorIdInstancia(idInstancia));
	}
	
}