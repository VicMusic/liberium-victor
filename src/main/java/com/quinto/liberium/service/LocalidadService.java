package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.LocalidadConverter;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MLocalidad;
import com.quinto.liberium.repository.LocalidadRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service
public class LocalidadService extends SimpleService{

	@Autowired
	private LocalidadConverter localidadConverter;
	
	@Autowired
	private LocalidadRepository localidadRepository;
	
	@Autowired
	private ProvinciaRepository provinciaRepository;
	
	public MLocalidad guardar(String id, String nombre, String idProvincia, Usuario usuario) {
		Localidad localidad = new Localidad();

		if (!isStringNull(id)) {
			localidad = buscarPorId(id);
			validarLocalidad(nombre, idProvincia, localidad);
		} else {
			validarLocalidad(nombre, idProvincia, null);
		}

		localidad.setNombre(nombre);
		
		Provincia provincia = provinciaRepository.buscarPorId(idProvincia);
		localidad.setProvincia(provincia);

		return localidadConverter.entidadModelo(guardar(localidad, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Localidad guardar(Localidad localidad, Usuario usuario, String accion) {
		auditar(localidad, usuario, accion);
		return localidadRepository.save(localidad);
	}

	public MLocalidad eliminarLocalidad(String id, Usuario usuario) {
		Localidad localidad = buscarPorId(id);
		if (localidad == null) {
			throw new EventoRestException("La localidad que trata eliminar no se encuentra en la base de datos.");
		}

		localidad = guardar(localidad, usuario, SimpleService.ELIMINAR);
		return localidadConverter.entidadModelo(localidad);
	}

	private void validarLocalidad(String nombre, String idProvincia, Localidad localidad) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la localidad no puede estar vacio.");
		}
		
		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException("La provincia al que pertenece la localidad no puede ser nulo.");
		}
		
		List<Localidad> localidades = buscarLocalidaesRepetidas(idProvincia, nombre);
		
		if (localidades.size() >= 1) {
			throw new PeticionIncorrectaException("Ya existe una localidad con ese nombre en esta provincia.");
		}

	}

	public List<MLocalidad> buscarProvinciaModelTodos() {
		return localidadConverter.entidadesModelos(localidadRepository.buscarTodos(null).getContent());
	}

	public Page<Localidad> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return localidadRepository.buscarTodos(paginable);
		} else {
			return localidadRepository.buscarTodos(paginable, "%" + q + "%");
		}
		
	}
	
	public List<Localidad> buscarLocalidaesRepetidas(String idProvincia, String nombre){
		return localidadRepository.buscarLocalidaesRepetidas(idProvincia, nombre);
	}
	
	public Localidad buscarPorId(String id) {
		return localidadRepository.buscarPorId(id);
	}
	
	public Localidad buscarPorNombre(String nombre) {
		return localidadRepository.buscarPorNombre(nombre);
	}
	
	public List<MLocalidad> buscarListadoPorProvincia(String idProvincia){
		return localidadConverter.entidadesModelos(localidadRepository.buscarPorIdProvincia(idProvincia));
	}
}
