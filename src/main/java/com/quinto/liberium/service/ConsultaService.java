package com.quinto.liberium.service;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ConsultaConverter;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoGenero;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MConsulta;
import com.quinto.liberium.model.MFormularioDinamico;
import com.quinto.liberium.repository.ConsultaRepository;
import com.quinto.liberium.repository.TipoCausaRepository;

@Service
public class ConsultaService extends SimpleService {

	@Autowired
	private ConsultaRepository consultaRepository;

	@Autowired
	private TipoCausaRepository tipoCausaRepository;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private FormularioService formularioService;

	@Autowired
	private PreguntaService preguntaService;

	@Autowired
	private ConsultaConverter consultaConverter;

	@Autowired
	private RespuestaService respuestaService;

	@Autowired
	private TurnoService turnoService;
	
	@Autowired
	private LocalidadService localidadService;

	public MConsulta guardar(String nombre, String apellido, String mail, String dni, String direccion,
			String localidad, String telefono, TipoGenero genero, String idTipoCausa, Date nacimiento) {

		Consulta consulta = new Consulta();
		
		validarConsulta(nombre, apellido, mail, dni, direccion, localidad, telefono, idTipoCausa, genero, nacimiento);

		Localidad localidadSet = localidadService.buscarPorId(localidad);

		consulta.setLocalidad(localidadSet);
		consulta.setNombre(nombre);
		consulta.setApellido(apellido);
		consulta.setMail(mail);
		consulta.setDni(dni);
		consulta.setDireccion(direccion);
		consulta.setTelefono(telefono);
		consulta.setGenero(genero);
		LocalDate birthdate = new LocalDate(nacimiento);
		LocalDate now = new LocalDate(new Date());
		Years age = Years.yearsBetween(birthdate, now);
		consulta.setEdad(age.getYears());
		consulta.setNacimiento(nacimiento);

		TipoCausa tipo = tipoCausaRepository.buscarPorId(idTipoCausa);
		consulta.setTipoCausa(tipo);

		return consultaConverter.entidadModelo(guardar(consulta, SimpleService.ACTUALIZAR));
	}

	public Consulta setRespuestas(List<Integer> orden, List<String> respuestas, List<String> idPreguntas, String idConsulta) {

		Consulta consulta = buscarPorId(idConsulta);
		
		for (int i = 0; i < respuestas.size(); i++) {			
			if (respuestas.get(i).isEmpty()) {
				throw new PeticionIncorrectaException("Se olvido de responder preguntas");
			}
		}

		for (int i = 0; i < respuestas.size(); i++) {
			respuestaService.guardar(respuestas.get(i), preguntaService.buscarPorId(idPreguntas.get(i)), orden.get(i), idConsulta);
		}

		return guardar(consulta, ACTUALIZAR);
	}

	public MConsulta setFormularioYTurno(String idTurno, String idFormulario,String idConsulta) {
		
		Consulta consulta = buscarPorId(idConsulta);
		
		Turno turno = turnoService.buscarPorId(idTurno);
		
		Formulario formulario = formularioService.buscarFormularioPorId(idFormulario);
				
		consulta.setTurno(turno);
		
		consulta.setFormulario(formulario);
		
		return consultaConverter.entidadModelo(guardar(consulta, ACTUALIZAR));
	}
	
	public MFormularioDinamico crearFormularioDinamico(Consulta consulta) {

		TipoCausa tipoCausa = tipoCausaService.buscarPorId(consulta.getTipoCausa().getId());
		
		Formulario formulario = formularioService.buscarFormularioPorTipoDeCausa(tipoCausa.getId());
		
		List<Pregunta> preguntas = preguntaService.buscarPorFormulario(formulario.getId());

		MFormularioDinamico formularioDinamico = new MFormularioDinamico();

		formularioDinamico.setIdFormulario(formulario.getId());
		formularioDinamico.setIdConsulta(consulta.getId());
		formularioDinamico.setIdTipoCausa(tipoCausa.getId());
		formularioDinamico.setPreguntas(preguntas);
		formularioDinamico.setTituliTipoCausa(tipoCausa.getNombre());

		return formularioDinamico;
	}

	@Transactional
	public Consulta guardar(Consulta consulta, String accion) {
		auditar(consulta, null, accion);
		return consultaRepository.save(consulta);
	}

	public MConsulta eliminarConsulta(String id, Usuario usuario) {
		Consulta consulta = buscarPorId(id);
		if (consulta == null) {
			throw new EventoRestException("La consulta que trata eliminar no se encuentra en la base de datos.");
		}

		consulta = guardar(consulta, SimpleService.ELIMINAR);
		return consultaConverter.entidadModelo(consulta);
	}

	public static boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	private void validarConsulta(String nombre, String apellido, String mail, String dni, String direccion,
			String localidad, String telefono, String idTipoCausa, TipoGenero genero, Date nacimiento) {
		if (genero == null) {
			throw new PeticionIncorrectaException("El genero no pueede estar vacio!");
		}

		if (isValid(mail) == false) {
			throw new PeticionIncorrectaException("El formato de el mail ingresado no es el correcto.");
		}

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre no puede estar vacio!");
		}

		if (isStringNull(apellido)) {
			throw new PeticionIncorrectaException("El apellido no puede estar vacio!");
		}
		
		if (isStringNull(dni)) {
			throw new PeticionIncorrectaException("El DNI no puede estar vacio!");
		}
		
		if (dni.length() > 8 || dni.length() < 7) {
			throw new PeticionIncorrectaException("El formato de el DNI es incorrecto!");
		}

		if (nacimiento == null) {
			throw new PeticionIncorrectaException("La fecha de nacimiento no puede estar vacia");
		}
		
		LocalDate birthdate = new LocalDate(nacimiento);
		LocalDate now = new LocalDate(new Date());
		Years age = Years.yearsBetween(birthdate, now);
		
		if (age.getYears() < 16) {
			throw new PeticionIncorrectaException("Debés ser mayor de 16 años para realizar una consulta!");
		}
		if (age.getYears() > 110) {
			throw new PeticionIncorrectaException("La edad ingresada no es valida!");
		}
		if (isStringNull(direccion)) {
			throw new PeticionIncorrectaException("La direccion no puede estar vacia!");
		}
		if (isStringNull(localidad)) {
			throw new PeticionIncorrectaException("La localidad no puede estar vacia!");
		}
		if (isStringNull(mail)) {
			throw new PeticionIncorrectaException("El mail no puede estar vacio!");
		}
		if (isStringNull(telefono)) {
			throw new PeticionIncorrectaException("El telefono no puede estar vacio!");
		}
		if (isStringNull(idTipoCausa)) {
			throw new PeticionIncorrectaException("El tipo de Causa no puede estar vacia!");
		}
	}

	public Page<Consulta> buscarTodos(Pageable paginable, String q) {
		return consultaRepository.buscarTodas(paginable);
	}

	public MConsulta buscarModelPorId(String idConsulta) {
		return consultaConverter.entidadModelo(consultaRepository.buscarPorId(idConsulta));
	}

	public Consulta buscarPorId(String idConsulta) {
		return consultaRepository.buscarPorId(idConsulta);
	}
	
	public Consulta buscarPorTurnoId(String idTurno) {
		return consultaRepository.buscarPorTurnoId(idTurno);
	}
	
	public void save(Consulta nombre) {
		consultaRepository.save(nombre);
	}
	
	public Page<Consulta> buscarConsutasPorAbogado(Usuario usuario, Pageable pageable, String q){
		if(q != null && !q.isEmpty()) {
			return consultaRepository.buscarConsultasPorAbogado(usuario, pageable,"%" + q + "%");
		}else {
			return consultaRepository.buscarConsultasPorAbogado(usuario, pageable);
		}
	}

}
