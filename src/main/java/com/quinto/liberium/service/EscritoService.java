package com.quinto.liberium.service;

import static com.quinto.liberium.util.Constantes.ITEM;
import static com.quinto.liberium.util.Constantes.SUBITEM;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.quinto.liberium.converter.EscritoConverter;
import com.quinto.liberium.entity.Escrito;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Respuesta;
import com.quinto.liberium.entity.SubItem;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MAbogado;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MEscrito;
import com.quinto.liberium.model.MEscritoVariables;
import com.quinto.liberium.repository.EscritoRepository;
import com.quinto.liberium.repository.ItemRepository;
import com.quinto.liberium.repository.SubItemRepository;
import com.quinto.liberium.util.Configuracion;

@Service("escritoService")
public class EscritoService extends SimpleService {
	private static final Logger log = LoggerFactory.getLogger(EscritoService.class);
	
	@Autowired
	private Configuracion configuracion;
	
	@Autowired
	private EscritoRepository escritoRepository;

	@Autowired
	private EscritoConverter escritoConverter;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private SubItemRepository subItemRepository;
	
	@Autowired
	private NotificacionService notificacionService;
	
	@Autowired
	private CausaService causaService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private RespuestaService respuestaService;
	
	@Autowired
	private SubItemService subItemService;
	
	@Autowired
	private TipoCausaService tipoCausaService;
	
	@Autowired
	private AbogadoService abogadoService;
	
	public MEscrito eliminarDeItem(String idEscrito, String idItem, String tipo, Usuario usuario) {
		Escrito escrito = escritoRepository.buscarPorId(idEscrito);
		if(escrito!=null) {
			switch (tipo) {
			case ITEM:
				Item item = itemRepository.buscarPorId(idItem);
				if(item != null) {
					escrito = guardar(escrito, usuario, SimpleService.ELIMINAR);
					item.setEscritos(eliminarDelaLista(item.getEscritos(), escrito));
					itemRepository.save(item);
				}
				break;
			case SUBITEM:
				SubItem subitem = subItemRepository.buscarPorId(idItem);
				if(subitem != null) {
					escrito = guardar(escrito, usuario, SimpleService.ELIMINAR);
					subitem.setEscritos(eliminarDelaLista(subitem.getEscritos(), escrito));
					subItemRepository.save(subitem);
				}
				break;

			default:
				throw new EventoRestException("El tipo de item al que debería pertencer es erróneo ");
			}
		} else {
			throw new EventoRestException("El escrito que se desea eliminar no se encuentra en base de datos");
		}
		return escritoConverter.entidadModelo(escrito);
	}
	private List<Escrito> eliminarDelaLista(List<Escrito> destino, Escrito escrito){
		
		if(destino.contains(escrito)){
			destino.remove(escrito);
		}
		return destino;
	}
	public MEscrito guardar(String id, String titulo, String idTipoCausa, String texto, String itemId, String tipoItem, boolean original, Usuario usuario) {
		Escrito escrito = new Escrito();

		boolean tieneItem = false;
		boolean nuevo = false;
		if (!isStringNull(id)) {
			escrito = buscarEscritoPorId(id);
			tieneItem = validarEscrito(titulo, texto, itemId, escrito);
		} else {
			tieneItem = validarEscrito(titulo, texto, itemId, null);
			nuevo = true;
		}

		if (original) {
			escrito.setOriginal(true);
		} else {
			escrito.setOriginal(false);
		}
		
		if (!isStringNull(idTipoCausa)) {
			escrito.setTipoCausa(tipoCausaService.buscarPorId(idTipoCausa));
		}
		
		escrito.setTitulo(titulo);
		escrito.setTexto(texto);
		

		if (tieneItem) {
			switch (tipoItem) {
			case ITEM:
				Item item = itemRepository.buscarPorId(itemId);
				if(item != null) {
					escrito = guardar(escrito, true, usuario, SimpleService.ACTUALIZAR);
					item.setEscritos(agregarAlaLista(item.getEscritos(), escrito));
					itemRepository.save(item);
				}
				break;
			case SUBITEM:
				SubItem subitem = subItemRepository.buscarPorId(itemId);
				if(subitem != null) {
					escrito = guardar(escrito, true, usuario, SimpleService.ACTUALIZAR);
					subitem.setEscritos(agregarAlaLista(subitem.getEscritos(), escrito));
					subItemRepository.save(subitem);
				}
				break;
			default:
				throw new EventoRestException("El tipo de item al que debería pertencer es erróneo ");
			}
		} else {
			escrito = guardar(escrito, false, usuario, SimpleService.ACTUALIZAR);
		}
		
		guardarArchivos(escrito, ".docx");
		guardarArchivos(escrito, ".pdf");
		if (!original && nuevo) {
			notificacionService.notificarEscritoUrl("administracion/escrito/listado",escrito.getId());
		}

		return escritoConverter.entidadModelo(escrito);
	}
	
	@Async
	public void guardarArchivos(Escrito escrito, String formato) {
		String path = configuracion.getPathEscritos() + escrito.getId() + formato;

		try {
			OutputStream out = new FileOutputStream(new File(path));
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, out);
			document.open();
			InputStream inputStream = new ByteArrayInputStream(escrito.getTexto().getBytes());
			XMLWorkerHelper.getInstance().parseXHtml(writer, document, inputStream);
			document.close();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private Escrito guardar(Escrito escrito, boolean anclado, Usuario usuario, String accion) {
		escrito.setAnclado(anclado);
		return guardar(escrito, usuario, SimpleService.ACTUALIZAR);
	}

	@Transactional
	private Escrito guardar(Escrito escrito, Usuario usuario, String accion) {
		auditar(escrito, usuario, accion);
		return escritoRepository.save(escrito);
	}

	private List<Escrito> agregarAlaLista(List<Escrito> destino, Escrito escrito){
		destino.add(escrito);
		return destino;
	}
	
	public MEscrito eliminarEscrito(String id, Usuario usuario) {
		Escrito escrito = buscarEscritoPorId(id);
		if (escrito == null) {
			throw new EventoRestException("El escrito que trata eliminar no se encuentra en la base de datos.");
		}

		escrito = guardar(escrito, usuario, SimpleService.ELIMINAR);
		return escritoConverter.entidadModelo(escrito);
	}

	private boolean validarEscrito(String titulo, String texto, String itemId, Escrito escrito) {
		boolean sioNo = false;
		if (titulo.length() > 50) {
			throw new PeticionIncorrectaException("El nombre del escrito supera la cantidad máxima de caracteres permitidos.");
		}
		if (isStringNull(titulo.replace(" ", ""))) {
			throw new PeticionIncorrectaException("El titulo del Escrito no puede estar vacio.");
		}

		if (isStringNull(texto.replace(" ", ""))) {
			throw new PeticionIncorrectaException("El texto del Escrito no puede estar vacio.");

		}
		if (!isStringNull(itemId)) {
			sioNo = true;
		}
		return sioNo;

	}
	
	public MEscritoVariables crearEscritoVariabes(String idItemSubItem, String idCausa, String idEscrito, Usuario creador) {
		
		Escrito escrito = buscarEscritoPorId(idEscrito);
		MCausa causa = causaService.buscarCausaModelPorId(idCausa);
		List<Respuesta> respuestas = new ArrayList<>();
		MAbogado abogado = abogadoService.buscarMAbogadoPorMailUsuario(creador.getEmail());
		
		try {
			respuestas = respuestaService.buscarRespuestaPorIdConsulta2(causa.getmConsulta().getId());
		} catch (Exception e) {
			log.warn("WARNING: No se pudo encontrar una consulta vinculada a la causa. " + e);
		}
		
		MEscritoVariables escritoVariables = new MEscritoVariables();
		
		escritoVariables.setIdEscrito(escrito.getId());
		escritoVariables.setTituloEscrito(escrito.getTitulo());
		escritoVariables.setTextoEscrito(escrito.getTexto());
		escritoVariables.setIdItemSubItem(idItemSubItem);
		escritoVariables.setCausa(causa);
		escritoVariables.setRespuestas(respuestas);
		escritoVariables.setAbogado(abogado);
		
		return escritoVariables;
	}
	
	public void guardarEnItem(String idItemSubItem, String idEscritoUsado, String idEscritoNuevo) {
		
		if(itemService.buscarItemPorId(idItemSubItem) != null) {
			Item item = itemService.buscarItemPorId(idItemSubItem);
			for (int i = 0; i < item.getEscritos().size(); i++ ) {
				if(item.getEscritos().get(i).getId().equals(idEscritoUsado)) {
					item.getEscritos().remove(i);
					item.getEscritos().add(buscarEscritoPorId(idEscritoNuevo));
				}
			}
			itemRepository.save(item);
		}else {
			SubItem subItem = subItemService.buscarSubItemPorId(idItemSubItem);
			for (int i = 0; i < subItem.getEscritos().size(); i++ ) {
				if(subItem.getEscritos().get(i).getId().equals(idEscritoUsado)) {
					subItem.getEscritos().remove(i);
					subItem.getEscritos().add(buscarEscritoPorId(idEscritoNuevo));
				}
			}
			subItemRepository.save(subItem);
		}
		
	}

	public Page<Escrito> buscarTodas(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return escritoRepository.buscarTodos(paginable);
		} else {
			return escritoRepository.buscarTodos(paginable, "%" + q + "%");
		}

	}

	public MEscrito crear(MEscrito mEscrito, Usuario creador) {
		if (mEscrito == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
//		if (isStringNull(mEscrito.getTitulo())) {
//			throw new PeticionIncorrectaException("Debe ingresar un titulo para crear un Escrito");
//		}
		Escrito escrito = escritoConverter.modeloEntidad(mEscrito);
		return escritoConverter.entidadModelo(guardar(escrito, creador));
	}

	private Escrito guardar(Escrito escrito, Usuario interviniente) {
		return guardar(escrito, interviniente, null);
	}

	public List<MEscrito> buscarEscritoModelTodos() {
		return escritoConverter.entidadesModelos(escritoRepository.buscarTodos());
	}

	public List<Escrito> buscarEscritoTodos() {
		return escritoRepository.buscarTodos();
	}

	public MEscrito buscarEscritoModelPorId(String idEscrito) {
		return escritoConverter.entidadModelo(escritoRepository.buscarPorId(idEscrito));
	}

	public Escrito buscarEscritoPorId(String idEscrito) {
		return escritoRepository.buscarPorId(idEscrito);
	}

	public List<MEscrito> buscarEscritoModelPorTitulo(String titulo) {
		return escritoConverter.entidadesModelos(escritoRepository.buscarPorTitulo(titulo));
	}

	public List<Escrito> buscarEscritoPorTitulo(String titulo) {
		return escritoRepository.buscarPorTitulo(titulo);
	}

	public List<Escrito> buscarTodosUsuarioId(String idUsuario) {
		return escritoRepository.buscarTodosUsuarioId(idUsuario);
	}
	
	public List<MEscrito> buscarTodosUsuarioIdM(String idUsuario) {
		return escritoConverter.entidadesModelos(escritoRepository.buscarTodosUsuarioId(idUsuario));
	}
	
	public Page<Escrito> buscarOriginales(Pageable pageable, String q){
		if (q == null) {
			return escritoRepository.buscarOriginales(pageable);
		} else {
			return escritoRepository.buscarOriginales(pageable, "%" + q + "%");
		}
	}

//	public Page<Escrito> buscarOriginalesPage(Pageable pageable){
//		return escritoRepository.buscarOriginales(pageable);
//	}

	public List<MEscrito> buscarOriginalesList(){
		return escritoConverter.entidadesModelos(escritoRepository.buscarOriginalesList());
	}
	
	public List<MEscrito> buscarLocales(Usuario usuario){
		return escritoConverter.entidadesModelos(escritoRepository.buscarLocales(usuario));
	}
	
	public List<MEscrito> buscarPropuestos(){
		return escritoConverter.entidadesModelos(escritoRepository.buscarPropuestos());
	}
	
	public Page<Escrito> buscarLocalesPage(Pageable pageable, String idUsuario, String q){
		if (q == null) {
			return escritoRepository.buscarTodosUsuarioId(pageable, idUsuario);
		} else {
			return escritoRepository.buscarTodosUsuarioId(pageable, idUsuario, "%" + q + "%");
		}
	}
	
	public List<MEscrito> buscarOriginalesPorTipoCausa(String idTipoCausa){
		return escritoConverter.entidadesModelos(escritoRepository.buscarOriginalesPorTipoCausa(idTipoCausa));
	}
	
	public List<Escrito> obternerEscritosChecklist(HttpServletRequest request, String ordenStr) {
		String[] idsEscritos = request.getParameter("escritos-" + ordenStr).split(",");
		List<Escrito> escritos = new ArrayList<Escrito>();
		if(idsEscritos != null && idsEscritos.length > 0) {
			for (String idEscrito : idsEscritos) {
				escritos.add(buscarEscritoPorId(idEscrito));
			}
		}
		return escritos;
	}
	public List<Escrito> buscarEscritoPorId(String[] ids) {
		return escritoRepository.buscarPorIds(ids);
	}

}
