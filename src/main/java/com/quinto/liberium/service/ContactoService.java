package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ContactoConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Contacto;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoGenero;
import com.quinto.liberium.enumeration.TipoPersona;
import com.quinto.liberium.enumeration.TipoVinculo;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MContacto;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.repository.ContactoRepository;

@Service("contactoService")
public class ContactoService extends SimpleService {

	private static final Logger log = LoggerFactory.getLogger(ContactoService.class);

	@Autowired
	private ContactoConverter contactoConverter;

	@Autowired
	private ContactoRepository contactoRepository;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private LocalidadService localidadService;

	@Autowired
	private CausaService causaService;

	@Autowired
	private CausaRepository causaRepository;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ModificacionService modificacionService;

	@Autowired
	private EstudioService estudioService;

	public MContacto guardar(TipoPersona tipoPersona, TipoVinculo tipoVinculo, String nombre, String apellido,
			String idPais, TipoGenero tipoGenero, String dni, String cuilcuit, String domicilio,
			Integer alturaDomicilio, String idProvincia, String idLocalidad, String email, String telefonoFijo,
			String telefonoCelular, String nombreAdicional, String telefonoAdicional, String observacion, String id,
			String causaId, String idEstudio, Usuario usuario) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del contacto es obligatorio!");
		}

		Contacto contacto = new Contacto();
		Causa causa = new Causa();

		if (!isStringNull(id)) {
			contacto = buscarPorId(id);
		}

		Pais pais = paisService.buscarPaisPorId(idPais);
		Provincia provincia = provinciaService.buscarProvinciaPorId(idProvincia);
		Localidad localidad = localidadService.buscarPorId(idLocalidad);
		Estudio estudio = estudioService.buscarPorId(idEstudio);

		contacto.setTipoPersona(tipoPersona);
		contacto.setTipoVinculo(tipoVinculo);
		String nombreFirstUpper = nombre.substring(0, 1).toUpperCase() + nombre.substring(1);
		contacto.setNombre(nombreFirstUpper);
		contacto.setApellido(apellido.toUpperCase());
		contacto.setPais(pais);
		contacto.setTipoGenero(tipoGenero);
		contacto.setDni(dni);
		contacto.setCuilCuit(cuilcuit);
		contacto.setDomicilio(domicilio);
		contacto.setAlturaDomicilio(alturaDomicilio);
		contacto.setProvincia(provincia);
		contacto.setLocalidad(localidad);
		contacto.setEmail(email);
		contacto.setTelefonoFijo(telefonoFijo);
		contacto.setTelefonoCelular(telefonoCelular);
		contacto.setObservacion(observacion);
		contacto.setNombreAdicional(nombreAdicional);
		contacto.setTelefonoAdicional(telefonoAdicional);
		contacto.setEstudio(estudio);
		Usuario user = usuarioService.buscarUsuarioPorEmail(email);
		contacto.setUsuario(user);
		contacto = guardar(contacto, usuario, SimpleService.ACTUALIZAR);
		if (causaId != null && causaId != "" && !causaId.isEmpty()) {
			causa = causaService.buscarPorId(causaId);
			if (causa.getContacto().contains(contacto)) {
				modificacionService.guardarModificacion("Se editó un contacto en la causa.", causa, usuario);
			} else {
				causa.getContacto().add(contacto);
				causaRepository.save(causa);
				modificacionService.guardarModificacion("Se agregó un contacto a la causa.", causa, usuario);
			}
		}

		return contactoConverter.entidadModelo(contacto);
	}

	public MContacto agregarContacto(String contactoId, String causaId) {
		boolean existe = false;
		Contacto contacto = contactoRepository.buscarPorId(contactoId);
		Causa causa = causaRepository.buscarPorId(causaId);
		for (int i = 0; i < causa.getContacto().size(); i++) {
			if (causa.getContacto().get(i) == contacto) {
				existe = true;
			}
		}
		if (existe == false) {
			causa.getContacto().add(contacto);
			causaRepository.save(causa);
		}
		return contactoConverter.entidadModelo(contacto);
	}

	public MContacto quitarContacto(String contactoId, String causaId) {
		Contacto contacto = contactoRepository.buscarPorId(contactoId);
		Causa causa = causaRepository.buscarPorId(causaId);
		List<Contacto> listContacto = causa.getContacto();
		for (int i = 0; i < listContacto.size(); i++) {
			if (listContacto.get(i).getId() == contacto.getId()) {
				listContacto.remove(i);
			}
		}
		causa.setContacto(listContacto);
		causaRepository.save(causa);
		return contactoConverter.entidadModelo(contacto);
	}

	@Transactional
	private Contacto guardar(Contacto contacto, Usuario usuario, String accion) {
		auditar(contacto, usuario, accion);
		return contactoRepository.save(contacto);
	}

	public MContacto eliminarContacto(String id, Usuario usuario, String causaId) {
		Contacto contacto = buscarPorId(id);
		if (contacto == null) {
			throw new EventoRestException("El contacto que trata eliminar no se encuentra en la base de datos.");
		}
		if (causaId != null && causaId != "") {
			Causa causa = causaService.buscarPorId(causaId);
			modificacionService.guardarModificacion("Se eliminó un contacto de la causa.", causa, usuario);
		}

		contacto = guardar(contacto, usuario, SimpleService.ELIMINAR);
		return contactoConverter.entidadModelo(contacto);
	}

	@SuppressWarnings("unused")
	private void validarContacto(TipoPersona tipoPersona, TipoVinculo tipoVinculo, String nombre, String idPais,
			TipoGenero tipoGenero, Integer dni, Integer cuilcuit, String domicilio, Integer alturaDomicilio,
			String idProvincia, String idLocalidad, String email, Integer telefonoFijo, Integer telefonoCelular,
			String nombreAdicional, String telefonoAdicional, String observacion, String id) {

		if (isStringNull(email)) {
			throw new PeticionIncorrectaException("El email de el contacto no puede estar vacio.");
		}

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del contacto no puede estar vacio.");
		}

	}

	public List<Contacto> listarContacto() {
		return contactoRepository.findAll();
	}

	public Contacto buscarPorId(String id) {
		return contactoRepository.buscarPorId(id);
	}

	public Contacto buscarPorNombre(String nombre) {
		return contactoRepository.buscarPorNombre(nombre);
	}

	public Contacto buscarPorIdEstudio(String estudioId) {
		return contactoRepository.buscarPorId(estudioId);
	}

	public Page<Contacto> buscarTodos(Pageable pageable, String idUsuario, String idEstudio, String q, String tipo) {
		Page<Contacto> contactos = null;
		switch (tipo) {
		case "personales":
			if (q == null || q.isEmpty()) {
				contactos = contactoRepository.buscarContactosPorUsuairoPage(idUsuario, pageable);
			} else {
				contactos = contactoRepository.buscarContactosPorUsuairoPage(idUsuario, "%" + q + "%", pageable);
			}
			break;
		case "estudio":
			if (q == null || q.isEmpty()) {
				contactos = contactoRepository.buscarContactosPorEstudioPage(idEstudio, pageable);
			} else {
				contactos = contactoRepository.buscarContactosPorEstudioPage(idEstudio, "%" + q + "%", pageable);
			}
			break;
		}
		return contactos;
	}

	public List<MContacto> buscarContactosPorUsuario(Usuario usuario) {
		return contactoConverter.entidadesModelos(contactoRepository.buscarContactosPorUsuario(usuario));
	}

	public List<MContacto> buscarContactosPorNombrePorUsuario(Usuario usuario, String q) {
		return contactoConverter.entidadesModelos(contactoRepository.buscarContactosPorNombrePorUsuario(usuario, q));
	}

}