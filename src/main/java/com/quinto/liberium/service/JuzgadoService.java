package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.JuzgadoConverter;
import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.DiaNoLaboral;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Juzgado;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MJuzgado;
import com.quinto.liberium.repository.InstanciaRepository;
import com.quinto.liberium.repository.JuzgadoRepository;

@Service("juzgadoService")
public class JuzgadoService extends SimpleService {

	@Autowired
	private JuzgadoRepository juzgadoRepository;

	@Autowired
	private JuzgadoConverter juzgadoConverter;

	@Autowired
	private InstanciaRepository instanciaRepository;

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private ProvinciaService provinciaService;
	
	@Autowired
	private PaisService paisService;

	public MJuzgado guardar(String id, String nombre, TipoFuero tipoFuero, String idPais, String idProvincia, String telefono,
			String idCircunscripcion, String idInstancia, Usuario usuario) {
		Juzgado juzgado = new Juzgado();

		if (!isStringNull(id)) {
			juzgado = buscarJuzgadoPorId(id);
			validarJuzgado(nombre, idInstancia, juzgado);
		} else {
			validarJuzgado(nombre, idInstancia, null);
		}

		juzgado.setTipoFuero(tipoFuero);
		juzgado.setNombre(nombre);

		Instancia instancia = instanciaRepository.buscarPorId(idInstancia);
		juzgado.setInstancia(instancia);
		
		if (tipoFuero == TipoFuero.FEDERAL) {
			juzgado.setPais(paisService.buscarPaisPorId(idPais));
		} else if (tipoFuero == TipoFuero.PROVINCIAL) {
			juzgado.setPais(paisService.buscarPaisPorId(idPais));
			juzgado.setProvincia(provinciaService.buscarProvinciaPorId(idProvincia));
		}	
		
		Circunscripcion circunscripcion = circunscripcionService.buscarCircunscripcionPorId(idCircunscripcion);
		juzgado.setCircunscripcion(circunscripcion);

		return juzgadoConverter.entidadModelo(guardar(juzgado, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Juzgado guardar(Juzgado juzgado, Usuario usuario, String accion) {
		auditar(juzgado, usuario, accion);
		return juzgadoRepository.save(juzgado);
	}

	public MJuzgado eliminarJuzgado(String id, Usuario usuario) {
		Juzgado juzgado = buscarJuzgadoPorId(id);
		if (juzgado == null) {
			throw new EventoRestException("La juzgado que trata eliminar no se encuentra en la base de datos.");
		}

		juzgado = guardar(juzgado, usuario, SimpleService.ELIMINAR);
		return juzgadoConverter.entidadModelo(juzgado);
	}

	private void validarJuzgado(String nombre, String idInstancia, Juzgado juzgado) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la juzgado no puede estar vacio.");
		}

		if (isStringNull(idInstancia)) {
			throw new PeticionIncorrectaException("La instancia a la que pertenece la juzgado no puede ser nula.");
		} else {
			Juzgado otra = buscarJuzgadoPorNombre(nombre);
			if ((juzgado == null && otra != null)
					|| (juzgado != null && otra != null && !juzgado.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}
	}

	public List<MJuzgado> buscarJuzgadosModel() {
		return juzgadoConverter.entidadesModelos(juzgadoRepository.buscarTodos());
	}

	public Page<Juzgado> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return juzgadoRepository.buscarTodas(paginable);
		} else {
			 q = "%"+q+"%";
			 List<Juzgado> juzgados = new ArrayList<>();
			 List<Juzgado> juzgadosList = juzgadoRepository.buscarTodas(q);
			 if(juzgadosList != null && !juzgadosList.isEmpty()) {
				 juzgados.addAll(juzgadosList);
			 }
			 List<Juzgado> juzgadoProvincia = juzgadoRepository.buscarTodasPorProvincia(q);
			 if(juzgadoProvincia != null && !juzgadoProvincia.isEmpty()) {
				 for(Juzgado c : juzgadoProvincia) {
					 if(!juzgados.contains(c)) {
						 juzgados.add(c);
					 }
				 }
			 }
			 List<Juzgado> juzgadoPais = juzgadoRepository.buscarTodasPorPais(q);
			 if(juzgadoPais != null && !juzgadoPais.isEmpty()) {
				 for(Juzgado cc : juzgadoPais) {
					 if(!juzgados.contains(cc)) {
						 juzgados.add(cc);
					 }
				 }
			 }
			 List<Juzgado> juzgadoCir = juzgadoRepository.buscarTodasPorCir(q);
			 if(juzgadoCir != null && !juzgadoCir.isEmpty()) {
				 for(Juzgado ccc : juzgadoCir) {
					 if(!juzgados.contains(ccc)) {
						 juzgados.add(ccc);
					 }
				 }
			 }
		     final int start = (int) paginable.getOffset();
		     final int end = Math.min(start + paginable.getPageSize(), juzgados.size());
		     
		     
		     Page<Juzgado> juzgadosPage = new PageImpl<>(juzgados.subList(start, end), paginable, juzgados.size());
			 return juzgadosPage;
		}
	}

	public MJuzgado crear(MJuzgado mJuzgado, Usuario creador) {
		if (mJuzgado == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mJuzgado.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear un país");

		}
		Juzgado juzgado = juzgadoConverter.modeloEntidad(mJuzgado);
		return juzgadoConverter.entidadModelo(guardar(juzgado, creador));
	}

	public void eliminar(String idJuzgado, Usuario eliminador) {
		if (isStringNull(idJuzgado) || eliminador == null) {
			throw new PeticionIncorrectaException("Ocurrio un error al eliminar el país");
		}
		Juzgado juzgado = juzgadoRepository.buscarPorId(idJuzgado);
		guardar(juzgado, eliminador, ELIMINAR);
	}

	private Juzgado guardar(Juzgado juzgado, Usuario interviniente) {
		return guardar(juzgado, interviniente, null);
	}

	public MJuzgado clonar(Juzgado juzgado, Usuario usuario) {
		Juzgado juzgadoCopia = new Juzgado();

		juzgadoCopia.setNombre(juzgado.getNombre() + " Copia");

		Instancia instancia = instanciaRepository.buscarPorId(juzgado.getInstancia().getId());
		juzgadoCopia.setInstancia(instancia);

		Provincia provincia = provinciaService.buscarProvinciaPorId(juzgado.getProvincia().getId());
		juzgadoCopia.setProvincia(provincia);

		Circunscripcion circunscripcion = circunscripcionService
				.buscarCircunscripcionPorId(juzgado.getCircunscripcion().getId());
		juzgadoCopia.setCircunscripcion(circunscripcion);

		return juzgadoConverter.entidadModelo(guardar(juzgadoCopia, usuario, SimpleService.ACTUALIZAR));
	}

	public List<MJuzgado> buscarJuzgadoModelTodos() {
		return juzgadoConverter.entidadesModelos(juzgadoRepository.buscarTodos());
	}

	public List<Juzgado> buscarJuzgadoTodos() {
		return juzgadoRepository.buscarTodos();
	}

	public MJuzgado buscarJuzgadoModelPorId(String idJuzgado) {
		return juzgadoConverter.entidadModelo(juzgadoRepository.buscarPorId(idJuzgado));
	}

	public Juzgado buscarJuzgadoPorId(String idJuzgado) {
		return juzgadoRepository.buscarPorId(idJuzgado);
	}

	public MJuzgado buscarJuzgadoModelPorNombre(String nombre) {
		return juzgadoConverter.entidadModelo(juzgadoRepository.buscarPorNombre(nombre));
	}

	public Juzgado buscarJuzgadoPorNombre(String nombre) {
		return juzgadoRepository.buscarPorNombre(nombre);
	}

	public List<MJuzgado> buscarJuzgadoModelPorInstancia(String idInstancia) {
		return juzgadoConverter.entidadesModelos(juzgadoRepository.buscarPorIdInstancia(idInstancia));
	}

	public List<Juzgado> buscarJuzgadoPorInstancia(String idInstancia) {
		return juzgadoRepository.buscarPorIdInstancia(idInstancia);
	}

	public List<MJuzgado> buscarPorIdInstanciaYIdTipoCausaYIdProvincia(String idInstancia, String idProvincia) {
		return juzgadoConverter.entidadesModelos(
				juzgadoRepository.buscarPorIdInstanciaYIdTipoCausaYIdProvincia(idInstancia, idProvincia));
	}
	
	public List<MJuzgado> buscarPorIdProvincia(String idProvincia) {
		return juzgadoConverter.entidadesModelos(
				juzgadoRepository.buscarPorIdProvincia(idProvincia));
	}
}
