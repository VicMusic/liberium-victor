package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ComentarioConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Comentario;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MComentario;
import com.quinto.liberium.repository.CausaRepository;
import com.quinto.liberium.repository.ComentarioRepository;

@Service
public class ComentarioService extends SimpleService{

	@Autowired
	private ComentarioRepository comentarioRepository;
	
	@Autowired
	private ComentarioConverter comentarioConverter; 
	
	@Autowired
	private CausaRepository causaRepository;
	
	public MComentario guardar(String id, String contenido, String idCausa, Usuario usuario) {
		Comentario comentario = new Comentario();
		Causa causa = causaRepository.buscarPorId(idCausa);
		if (!isStringNull(id)) {
			comentario = buscarPorId(id);
			validarComentario(contenido, idCausa, comentario);
		} else {
			validarComentario(contenido, idCausa, null);
			comentario.setCausa(causa);
		}

		comentario.setUsuario(usuario);
		comentario.setComentario(contenido);

		return comentarioConverter.entidadModelo(guardar(comentario, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Comentario guardar(Comentario comentario, Usuario usuario, String accion) {
		auditar(comentario, usuario, accion);
		return comentarioRepository.save(comentario);
	}

	public MComentario eliminarComentario(String id, Usuario usuario) {
		Comentario comentario = buscarPorId(id);
		if (comentario == null) {
			throw new EventoRestException("El comentario que trata eliminar no se encuentra en la base de datos.");
		}

		comentario = guardar(comentario, usuario, SimpleService.ELIMINAR);
		return comentarioConverter.entidadModelo(comentario);
	}

	private void validarComentario(String contenido, String idCausa, Comentario comentario) {

		if (isStringNull(contenido)) {
			throw new PeticionIncorrectaException("El contenido del comentario no puede estar vacio.");
		}

//		if (isStringNull(idCausa)) {
//			throw new PeticionIncorrectaException("La causa a la que pertenece el comentario no puede ser nula.");
//		} 

	}

	public List<MComentario> buscarComentarioModelPorCausa(String idCausa) {
		return comentarioConverter.entidadesModelos(comentarioRepository.comentariosPorCausa(idCausa));
	}
	
	public Comentario buscarPorId(String id) {
		return comentarioRepository.buscarPorId(id);
	}
	
}
