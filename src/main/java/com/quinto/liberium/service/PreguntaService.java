package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.PreguntaConverter;
import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Opcion;
import com.quinto.liberium.entity.Pregunta;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoPregunta;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MPregunta;
import com.quinto.liberium.repository.OpcionRepository;
import com.quinto.liberium.repository.PreguntaRepository;

@Service
public class PreguntaService extends SimpleService {

	@Autowired
	private PreguntaRepository preguntaRepository;

	@Autowired
	private OpcionRepository opcionRepositoy; 
	
	@Autowired
	private PreguntaConverter preguntaConverter;  
	
	@Autowired
	private FormularioService formularioService;
	
	public MPregunta guardar(String idPregunta, String textoPregunta, String tipoPregunta, String inputRespuesta, 
			List<String> opciones, String variablePregunta, String idFormulario, Usuario usuario) {
		
		Pregunta pregunta = new Pregunta();
		
		if (!isStringNull(idPregunta)) {
			pregunta = buscarPorId(idPregunta);
		}
		
		switch (tipoPregunta) {
		case "CERRADA":
			if (opciones.size() < 2) {
				throw new PeticionIncorrectaException("Tiene que ingresar por lo menosd dos opciones");
			}
			pregunta.setTipo(TipoPregunta.CERRADA);
			List<Opcion> opcionesPregunta = new ArrayList<>();
			for (int i = 0; i < opciones.size(); i++) {
				Opcion opcion = new Opcion();
				opcion.setTitulo(opciones.get(i));
				opcionesPregunta.add(opcion);
				opcionRepositoy.save(opcion);
			}
			pregunta.setOpciones(opcionesPregunta);
			break;
		case "ABIERTA":
			if (isStringNull(textoPregunta)) {
				throw new PeticionIncorrectaException("Tiene que ingresar el texto de la pregunta");
			}
			if (isStringNull(inputRespuesta)) {
				throw new PeticionIncorrectaException("Tiene que elegir un tipo de respuesta");
			}
			pregunta.setInputRespuesta(inputRespuesta);
			pregunta.setTipo(TipoPregunta.ABIERTA);
			break;
		}
		
		pregunta.setTitulo(textoPregunta);
		
		if (!isStringNull(variablePregunta)) {
			pregunta.setVariableAmigable(variablePregunta);
			String variableValue = "@" + toTitleCase(variablePregunta).replace(" ", "");
			pregunta.setVariableValue(variableValue);
		}
		
		Formulario formulario = formularioService.buscarFormularioPorId(idFormulario);
		pregunta.setFormulario(formulario);
		
		return preguntaConverter.entidadModelo(guardar(pregunta, usuario, ACTUALIZAR));
	}
	
	public void editar(Collection<Pregunta> preguntas, Formulario formulario) {
		for(Pregunta pregunta : preguntas) {
			if(pregunta.getId() == null) {
				continue;
			}
		    Pregunta preguntaOriginal = preguntaRepository.buscarPorId(pregunta.getId());
			
			List<Opcion> opciones = new ArrayList<>();
			if(pregunta.getOpciones() != null) {
				for (int j = 0; j < pregunta.getOpciones().size(); j++) {
				if(pregunta.getOpciones().get(j).getTitulo() == null || pregunta.getOpciones().get(j).getTitulo().isEmpty()) {
					throw new PeticionIncorrectaException("El titulo de la opción no puede ser nulo o vacío.");
				}
		        Opcion opcionOriginal = opcionRepositoy.buscarPorId(pregunta.getOpciones().get(j).getId());
		        opcionOriginal.setTitulo(pregunta.getOpciones().get(j).getTitulo());
				opcionRepositoy.save(opcionOriginal);
				opciones.add(opcionOriginal);
				pregunta.getOpciones().remove(j);
			}
			}
			preguntaOriginal.setTitulo(pregunta.getTitulo());
			preguntaOriginal.setOpciones(opciones);
			preguntaOriginal.setFormulario(formulario);
			
			preguntaRepository.save(preguntaOriginal);
		}
	}
	
	@Transactional
	public void ordenar(String idFormulario, int orden){
		
		List<Pregunta> pregunta = preguntaRepository.buscarMismoOrden(idFormulario, orden);
		if(!pregunta.isEmpty()){
			List<Pregunta> mayores = preguntaRepository.buscarMayores(idFormulario, orden);
			
			for (Pregunta mayor : mayores) {
				mayor.setOrden(mayor.getOrden() + 1);
				preguntaRepository.save(mayor);
			}
		}
	}
	
	public MPregunta eliminar(String idPregunta, Usuario eliminador) {
		Pregunta pregunta = preguntaRepository.buscarPorId(idPregunta);
		return preguntaConverter.entidadModelo(guardar(pregunta, eliminador, ELIMINAR));
	}
	
	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}  
	
	
	@Transactional
	private Pregunta guardar(Pregunta pregunta, Usuario interviniente, String accion) {
		auditar(pregunta, interviniente, accion);
		return preguntaRepository.save(pregunta);
	}

	public int ultimoNumero(String idFormulario) {
		return preguntaRepository.buscarMaximoOrden(idFormulario);
	}
	
	public Pregunta buscarPorId(String idPregunta) {
		return preguntaRepository.buscarPorId(idPregunta);
	}
	
	public List<Pregunta> buscarPorFormulario(String idFormulario){
		return preguntaRepository.buscarPorFormulario(idFormulario);
	}
}
