package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.TipoCausaConverter;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MTipoCausa;
import com.quinto.liberium.repository.MateriaRepository;
import com.quinto.liberium.repository.TipoCausaRepository;

@Service("tipoCausaService")
public class TipoCausaService extends SimpleService {

	@Autowired
	private TipoCausaConverter tipoCausaConverter;

	@Autowired
	private TipoCausaRepository tipoCausaRepository;

	@Autowired
	private MateriaRepository materiaRepository;
	
	@Autowired
	private NotificacionService notificacionService;

	public MTipoCausa guardar(String id, String nombre, String materiaId, Usuario usuario) {
		TipoCausa tipoCausa = new TipoCausa();
		if (!isStringNull(id)) {
			tipoCausa = buscarPorId(id);
			validarTipoCausa(nombre, materiaId, tipoCausa);
			;
		} else {
			validarTipoCausa(nombre, materiaId, null);
		}

		tipoCausa.setNombre(nombre);

		Materia materia = materiaRepository.buscarPorId(materiaId);
		tipoCausa.setMateria(materia);
		if (!isStringNull(id) && !usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			notificacionService.notificarTipoCausaUrl(tipoCausa.getNombre(), false,"/administracion/tipocausa/listado#agregar",tipoCausa.getId());
		} else if (isStringNull(id) && !usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)){
			notificacionService.notificarTipoCausaUrl(tipoCausa.getNombre(), true,"/administracion/tipocausa/listado#agregar",tipoCausa.getId());
		}
		return tipoCausaConverter.entidadModelo(guardar(tipoCausa, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private TipoCausa guardar(TipoCausa tipoCausa, Usuario usuario, String accion) {
		auditar(tipoCausa, usuario, accion);
		return tipoCausaRepository.save(tipoCausa);
	}

	public MTipoCausa eliminarTipoCausa(String id, Usuario usuario) {
		TipoCausa tipoCausa = buscarPorId(id);
		if (tipoCausa == null) {
			throw new EventoRestException("El tipo de causa que trata eliminar no se encuentra en la base de datos.");
		}

		tipoCausa = guardar(tipoCausa, usuario, SimpleService.ELIMINAR);
		return tipoCausaConverter.entidadModelo(tipoCausa);
	}

	private void validarTipoCausa(String nombre, String materiaId, TipoCausa tipoCausa) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del tipo de causa no puede estar vacio.");
		}
		if (isStringNull(materiaId)) {
			throw new PeticionIncorrectaException("La materia a la que pertenece el tipo de causa no puede ser nulo.");
		} else {
			TipoCausa otra = buscarPorNombre(nombre);
			if ((tipoCausa == null && otra != null)
					|| (tipoCausa != null && otra != null && !tipoCausa.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public List<MTipoCausa> buscarTipoCausaModel() {
		return tipoCausaConverter.entidadesModelos(tipoCausaRepository.buscarTodos());
	}

	public Page<TipoCausa> buscarTodas(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return tipoCausaRepository.buscarTodas(paginable);
		} else {
			return tipoCausaRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public List<TipoCausa> listarTipoCausa() {
		return tipoCausaRepository.findAll();
	}

	public TipoCausa buscarPorId(String tipoCausaId) {
		return tipoCausaRepository.buscarPorId(tipoCausaId);
	}

	public MTipoCausa buscarModelPorId(String tipoCausaId) {
		return tipoCausaConverter.entidadModelo(tipoCausaRepository.buscarPorId(tipoCausaId));
	}

	public List<TipoCausa> buscarPorMateriaNombre(String materiaId) {
		return tipoCausaRepository.buscarPorMateria(materiaId);
	}
	
	public List<MTipoCausa> buscarPorMateriaId(String materiaId) {
		return tipoCausaConverter.entidadesModelos(tipoCausaRepository.buscarPorMateriaId(materiaId));
	}
	
	public List<TipoCausa> buscarPorMateriaIdEntity(String materiaId) {
		return tipoCausaRepository.buscarPorMateriaId(materiaId);
	}


	public TipoCausa buscarPorNombre(String nombre) {
		return tipoCausaRepository.buscarPorNombre(nombre);
	}

	public MTipoCausa buscarModelPorNombre(String nombre) {
		return tipoCausaConverter.entidadModelo(tipoCausaRepository.buscarPorNombre(nombre));
	}

//	public MTipoCausa crear(MTipoCausa mTipoCausa, Usuario creador) {
//		if (mTipoCausa == null || creador == null) {
//			throw new PeticionIncorrectaException("");
//		}
//		if (isStringNull(mTipoCausa.getNombre())) {
//			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear un tipo de causa");
//		}
//		TipoCausa tipoCausa = tipoCausaConverter.modeloEntidad(mTipoCausa);
//		return tipoCausaConverter.entidadModelo(guardar(tipoCausa, creador));
//	}
//    private TipoCausa guardar(TipoCausa tipoCausa, Usuario interviniente) {
//		return guardar(tipoCausa, interviniente, null);
//	}
}