package com.quinto.liberium.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.PermisoConverter;
import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MPermiso;
import com.quinto.liberium.repository.PermisoRepository;

@Service("permisoService")
public class PermisoService {

    private static final String MENSAJE_ERROR = "ERROR - El valor nombre no puede estar vacío.";
    private static final String ACCION_ELIMINAR = "eliminar";
    private static final String ACCION_AGREGAR = "agregar";
    private static final String ACCION_MODIFICAR = "modificar";

    @Autowired
    private PermisoConverter permisoConverter;

    @Autowired
    private PermisoRepository permisoRepository;

    public Permiso crear(MPermiso mPermiso, Usuario creador) {
	if (mPermiso == null || creador == null) {
	    throw new PeticionIncorrectaException(MENSAJE_ERROR);
	}
	Permiso permiso = permisoConverter.modeloEntidad(mPermiso);
	return guardar(permiso, creador, ACCION_AGREGAR);
    }

    public Permiso editar(MPermiso mPermiso, Usuario editor) {
	if (mPermiso == null || editor == null) {
	    throw new PeticionIncorrectaException(MENSAJE_ERROR);
	}
	Permiso permiso = permisoConverter.modeloEntidad(mPermiso);
	return guardar(permiso, editor, ACCION_MODIFICAR);
    }

    public void eliminar(String permisoId, Usuario eliminador) {
	if (permisoId.isEmpty() || eliminador == null) {
	    throw new PeticionIncorrectaException(MENSAJE_ERROR);
	}
	Permiso permiso = permisoRepository.buscarPorId(permisoId);
	guardar(permiso, eliminador, ACCION_ELIMINAR);
    }

    private Permiso guardar(Permiso permiso, Usuario interviniente, String accion) {
	return permisoRepository.save(permiso);
    }

    public List<Permiso> listarPermiso() {
	return permisoRepository.findAll();
    }

    public Permiso buscarPorId(String permisoId) {
	return permisoRepository.buscarPorId(permisoId);
    }

    public Permiso buscarPorNombre(String nombre) {
	return permisoRepository.buscarPorNombre(nombre);
    }
    
	public List<Permiso> buscarPorAdministrador(Boolean administrador) {
		return permisoRepository.buscarPorAdministrador(administrador);
	}
	
	public List<Permiso> buscarPorIds(String[] ids) {
		return permisoRepository.buscarPorIds(ids);
	}
	
	public List<MPermiso> buscarMPermisoPorAdministrador(Boolean administrador) {
		return permisoRepository.buscarMPermisoPorAdministrador(administrador);
	}

}