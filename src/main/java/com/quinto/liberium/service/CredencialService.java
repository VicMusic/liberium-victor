package com.quinto.liberium.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.quinto.liberium.entity.Credencial;
import com.quinto.liberium.repository.CredencialRepository;

@Service("credencialService")
public class CredencialService {

	@Autowired
	@Qualifier("credencialRepository")
	private CredencialRepository credencialRepository;

	public Credencial obtenerPorId(String credencialId) {
		return credencialRepository.obtenerPorId(credencialId);
	}

	public Credencial guardar(Credencial credencial) {
		return credencialRepository.save(credencial);
	}

}