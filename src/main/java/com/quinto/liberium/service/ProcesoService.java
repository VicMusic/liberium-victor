package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.ProcesoConverter;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Proceso;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MProceso;
import com.quinto.liberium.repository.MateriaRepository;
import com.quinto.liberium.repository.ProcesoRepository;

@Service
public class ProcesoService extends SimpleService {

	@Autowired
	private ProcesoRepository procesoRepository;

	@Autowired
	private ProcesoConverter procesoConverter;

	@Autowired
	private MateriaRepository materiaRepository;

	public MProceso guardar(String id, String nombre, String idMateria, Usuario usuario) {
		Proceso proceso = new Proceso();

		if (!isStringNull(id)) {
			proceso = buscarProcesoPorId(id);
			validarProceso(nombre, idMateria, proceso);
		} else {
			validarProceso(nombre, idMateria, null);
		}

		proceso.setNombre(nombre);

		Materia materia = materiaRepository.buscarPorId(idMateria);
		proceso.setMateria(materia);

		return procesoConverter.entidadModelo(guardar(proceso, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Proceso guardar(Proceso proceso, Usuario usuario, String accion) {
		auditar(proceso, usuario, accion);
		return procesoRepository.save(proceso);
	}

	public MProceso eliminarProceso(String id, Usuario usuario) {
		Proceso proceso = buscarProcesoPorId(id);
		if (proceso == null) {
			throw new EventoRestException("El proceso que trata eliminar no se encuentra en la base de datos.");
		}

		proceso = guardar(proceso, usuario, SimpleService.ELIMINAR);
		return procesoConverter.entidadModelo(proceso);
	}

	private void validarProceso(String nombre, String idMateria, Proceso proceso) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del proceso no puede estar vacio.");
		}

		if (isStringNull(idMateria)) {
			throw new PeticionIncorrectaException("La materia a la que pertenece el proceso no puede ser nula.");
		} else {
			Proceso otra = buscarProcesoPorNombre(nombre);
			if ((proceso == null && otra != null)
					|| (proceso != null && otra != null && !proceso.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public Page<Proceso> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return procesoRepository.buscarTodas(paginable);
		} else {
			return procesoRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public List<MProceso> buscarProcesoModelTodos() {
		return procesoConverter.entidadesModelos(procesoRepository.buscarTodos());
	}

	public List<Proceso> buscarProcesoTodos() {
		return procesoRepository.buscarTodos();
	}

	public MProceso buscarProcesoModelPorId(String idProceso) {
		return procesoConverter.entidadModelo(procesoRepository.buscarPorId(idProceso));
	}

	public Proceso buscarProcesoPorId(String idProceso) {
		return procesoRepository.buscarPorId(idProceso);
	}

	public MProceso buscarProcesoModelPorNombre(String nombre) {
		return procesoConverter.entidadModelo(procesoRepository.buscarPorNombre(nombre));
	}

	public Proceso buscarProcesoPorNombre(String nombre) {
		return procesoRepository.buscarPorNombre(nombre);
	}

	public List<MProceso> buscarProcesoModelPorMateria(String idMateria) {
		return procesoConverter.entidadesModelos(procesoRepository.buscarPorIdMateria(idMateria));
	}

	public List<Proceso> buscarProcesoPorMateria(String idMateria) {
		return procesoRepository.buscarPorIdMateria(idMateria);
	}

}
