package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.InteresConverter;
import com.quinto.liberium.entity.Interes;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MInteres;
import com.quinto.liberium.repository.InteresRepository;

@Service
public class InteresService extends SimpleService {
	
	@Autowired
	private InteresRepository interesRepository;
	
	@Autowired
	private InteresConverter interesConverter;
	
	public MInteres guardar(String id, String nombre, Double porcentaje, Usuario creador) {
		Interes interes = new Interes();
		
		if(!isStringNull(id)) {
			interes = buscarInteresPorId(id);
			validarInteres(nombre, porcentaje, interes);
		}else {
			validarInteres(nombre, porcentaje, null);
		}
		
		interes.setNombre(nombre);
		interes.setPorcentaje(porcentaje);
		
		return interesConverter.entidadModelo(guardar(interes, creador, ACTUALIZAR));
	}

	public MInteres eliminar(String idInteres, Usuario eliminador) {
		Interes interes = buscarInteresPorId(idInteres);
		return interesConverter.entidadModelo(guardar(interes, eliminador, ELIMINAR));
	}
	
	@Transactional
	private Interes guardar(Interes interes, Usuario interviniente, String accion) {
		auditar(interes, interviniente, accion);
		return interesRepository.save(interes);
	}
	
	public void validarInteres(String nombre, Double porcentaje, Interes interes) {
		Interes otro = null;
		
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del interes no puede ser nulo.");
		}else {
			otro = buscarPorNombre(nombre);
		}
		
		if (porcentaje == null) {
			throw new PeticionIncorrectaException("El porcentaje del interes no puede ser nulo.");
		}
		
		if(otro != null && porcentaje.equals(otro.getPorcentaje())) {
			throw new PeticionIncorrectaException("El nombre y el porcentaje ingresados ya se encuentran utilizados.");
		}
	}
	
	public Interes buscarInteresPorId(String idInteres){
		return interesRepository.buscarInteresPorId(idInteres);
	}
	
	public Interes buscarPorNombre(String nombre) {
		return interesRepository.buscarInteresPorNombre(nombre);
	}
	
	public List<Interes> buscarTodosIntereses(){
		return interesRepository.buscarTodos();
	}
	
	public MInteres buscarMInteresPorId(String idInteres) {
		return interesConverter.entidadModelo(interesRepository.buscarInteresPorId(idInteres));
	}
	
	public List<MInteres> buscarTodosMIntereses(){
		return interesConverter.entidadesModelos(interesRepository.buscarTodos());
	}
	
	public Page<Interes> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return interesRepository.buscarTodos(paginable);
		} else {
			return interesRepository.buscarTodos(paginable, "%" + q + "%");
		}
		
	}
}
