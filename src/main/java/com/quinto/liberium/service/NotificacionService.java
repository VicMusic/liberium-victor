package com.quinto.liberium.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.NotificacionConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Notificacion;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.model.MNotificacion;
import com.quinto.liberium.repository.NotificacionRepository;

@Service("notificacionService")
public class NotificacionService extends SimpleService {

	@Autowired
	private NotificacionConverter notificacionConverter;
	
	@Autowired
	private NotificacionRepository notificacionRepository;
	
	@Autowired
	private CausaService causaService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	
	public MNotificacion guardar(String id, String texto, Usuario usuario) {
		Notificacion notificacion = new Notificacion();
		
		if (!isStringNull(id)) {
			notificacion = buscarNotificacionPorId(id);
		}
		
		notificacion.setTexto(texto);
		notificacion.setUsuario(usuario);
		notificacion.setVista(false);
		notificacion.setFechaNotificacion(new Date());
		
		return notificacionConverter.entidadModelo(guardar(notificacion, usuario, ACTUALIZAR));
	}
	
	public MNotificacion guardarUrl(String id, String texto, Usuario usuario, String url, String idEntidad) {
		Notificacion notificacion = new Notificacion();
		
		if (!isStringNull(id)) {
			notificacion = buscarNotificacionPorId(id);
		}
		
		if(url != null) {
			notificacion.setUrl(url);
		}
		
		if(idEntidad != null) {
			notificacion.setIdEntidad(idEntidad);
		}
		notificacion.setTexto(texto);
		notificacion.setUsuario(usuario);
		notificacion.setVista(false);
		notificacion.setFechaNotificacion(new Date());
		
		return notificacionConverter.entidadModelo(guardar(notificacion, usuario, ACTUALIZAR));
	}
	
	public void notificarComentario(String idCausa) {
		Causa causa = causaService.buscarPorId(idCausa);
		Set<Miembro> miembrosCausa = causa.getMiembrosCausa();
		
		for (Miembro miembroCausa : miembrosCausa) {
			guardar(null, "Se comento en una causa", miembroCausa.getUsuario());
		}
	}
	
	public void notificarComentarioUrl(String idCausa,String url, String idEntidad) {
		Causa causa = causaService.buscarPorId(idCausa);
		Set<Miembro> miembrosCausa = causa.getMiembrosCausa();
		
		for (Miembro miembroCausa : miembrosCausa) {
			guardarUrl(null, "Se comento en una causa", miembroCausa.getUsuario(),url,idEntidad);
		}
	}
	
	public void notificarEscrito(){
		List<Usuario> admins = usuarioService.buscarAdministradores();
		for (Usuario usuario : admins) {
			guardar(null, "Nuevo modelo de escrito", usuario);
		}
	}
	
	public void notificarEscritoUrl(String url, String idEntidad){
		List<Usuario> admins = usuarioService.buscarAdministradores();
		for (Usuario usuario : admins) {
			guardarUrl(null, "Nuevo modelo de escrito", usuario,url,idEntidad);
		}
	}
	
	public void notificarCheckList(String texto){
		List<Usuario> admins = usuarioService.buscarAdministradores();
		for (Usuario usuario : admins) {
			guardar(null, texto, usuario);
		}
	}
	
	public void notificarCheckListUrl(String texto, String url, String idEntidad){
		List<Usuario> admins = usuarioService.buscarAdministradores();
		for (Usuario usuario : admins) {
			guardarUrl(null, texto, usuario,url,idEntidad);
		}
	}
	
	public void notificarTipoCausa1(String nombre, boolean nuevo) {
		List<Usuario> admins = usuarioService.buscarAdministradores();
		if (nuevo) {
			for (Usuario usuario : admins) {
				guardar(null, "Tipo causa " + nombre + " agregado!", usuario);
			}
		} else {
			for (Usuario usuario : admins) {
				guardar(null, "Tipo causa " + nombre + " editada!", usuario);
			}
		}
		
	}
	
	public void notificarTipoCausaUrl(String nombre, boolean nuevo, String url, String idEntidad) {
		List<Usuario> admins = usuarioService.buscarAdministradores();
		if (nuevo) {
			for (Usuario usuario : admins) {
				guardarUrl(null, "Tipo causa " + nombre + " agregado!", usuario,url,idEntidad);
			}
		} else {
			for (Usuario usuario : admins) {
				guardarUrl(null, "Tipo causa " + nombre + " editada!", usuario,url,idEntidad);
			}
		}
		
	}
	
	public void notificarConsuta(){
		List<Usuario> admins = usuarioService.buscarAdministradores();
		for (Usuario usuario : admins) {
			guardar(null, "Nueva consulta en la casilla de email", usuario);
		}
	}
	
	public List<MNotificacion> marcarVistas(List<MNotificacion> notificaciones, Usuario usuario){
		for (MNotificacion mNotificacion : notificaciones) {
			if (!mNotificacion.isVista()) {
				mNotificacion.setVista(true);
				guardar(notificacionConverter.modeloEntidad(mNotificacion), usuario, ACTUALIZAR);
			}
		}
		return notificaciones;
	}
	
	@Transactional
	private Notificacion guardar(Notificacion notificacion, Usuario usuario, String accion) {
		auditar(notificacion, usuario, accion);
		return notificacionRepository.save(notificacion);
	}
	
	public Notificacion buscarNotificacionPorId(String idNotificacion) {
		return notificacionRepository.buscarNotificacionPorId(idNotificacion);
	}
	
	public List<MNotificacion> buscarNotificacionesPorUsuario(String idUsuario){
		return notificacionConverter.entidadesModelos(notificacionRepository.buscarNotificacionesPorUsuario(idUsuario));
	}
}
