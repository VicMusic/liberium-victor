package com.quinto.liberium.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quinto.liberium.converter.HorarioConverter;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Horario;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.model.MHorario;
import com.quinto.liberium.repository.AbogadoRepository;
import com.quinto.liberium.repository.HorarioRepository;

@Service("horarioService")
public class HorarioService extends SimpleService {

	@Autowired
	private HorarioRepository horarioRepository;
	
	@Autowired
	private HorarioConverter horarioConverter;
	
	@Autowired
	private AbogadoRepository abogadoRepository;
	
	
	@Transactional
	public void guardar(String[] horarios, String mail, Usuario usuario) {

		Abogado abogado = abogadoRepository.buscarPorMail(mail);
		
		List<Horario> eliminados = horarioRepository.buscarHorariosPorAbogado(abogado.getId());
		horarioRepository.deleteAll(eliminados);
		
		
		for(String horario : horarios) {

			String dia = horario.substring(0,1);
			String hora = horario.substring(1, 3);
			String minutos = horario.substring(3, 5);
			
			Horario entidad = new Horario();
			entidad.setDia(dia);
			entidad.setHora(hora + ":" + minutos);
			
			entidad.setAbogado(abogado);
			
			entidad.setCreado(new Date());
			entidad.setCreador(usuario);
			entidad.setSolicitado(false);
			
			horarioRepository.save(entidad);
		}
	}
	
	public MHorario guardar(String id, Integer dias, String hora, String idAbogado, Usuario usuario) {
		Horario horario = new Horario();
		
		if (!isStringNull(id)) {
			horario = buscarHorarioPorId(id);
		}
		
		Abogado abogado = abogadoRepository.buscarAbogadoPorId(idAbogado);
		horario.setAbogado(abogado);
		
		
		
		return horarioConverter.entidadModelo(guardar(horario, usuario, SimpleService.ACTUALIZAR));
	}
	
	
	public List<MHorario> listarHorarios(String mail){
		Abogado abogado = abogadoRepository.buscarPorMail(mail);
		return horarioConverter.entidadesModelos(horarioRepository.buscarHorariosPorAbogado(abogado.getId()));
	}
	
	@Transactional
	public Horario guardar(Horario horario, Usuario usuario, String accion) {
		auditar(horario, usuario, accion);
		
		return horarioRepository.save(horario);
	}
	
	public MHorario eliminarHorario(String id, Usuario usuario) {
		Horario horario = buscarHorarioPorId(id);
		if (horario == null) {
			throw new EventoRestException("El horario que trata eliminar no se encuentra en la base de datos.");
		}

		horario = guardar(horario, usuario, SimpleService.ELIMINAR);
		return horarioConverter.entidadModelo(horario);
	}

	
	public List<Horario> buscarTodos(){
		return horarioRepository.buscarTodos();
	}

	public List<Horario> buscarHorariosPorMailAbogado(String mail){
		Abogado abogado = abogadoRepository.buscarPorMail(mail);
		return horarioRepository.buscarHorariosPorAbogado(abogado.getId());
	}
	
	public List<Horario> buscarHorariosPorAbogado(String idAbogado){
		return horarioRepository.buscarHorariosPorAbogado(idAbogado);
	}
	
	public List<MHorario> buscarHorariosPorMailAbogado2(String mail){
		Abogado abogado = abogadoRepository.buscarPorMail(mail);
		return horarioConverter.entidadesModelos(horarioRepository.buscarHorariosPorAbogado2(abogado.getId()));
	}
	
	public Horario buscarHorarioSolicitadoPorAbogado(String idAbogado, String dia, String hora) {
		return horarioRepository.buscarHorarioSolicitadoPorAbogado(idAbogado, dia, hora); 
	}
	
	public Horario buscarHorarioPorId(String idHorario) {
		return horarioRepository.buscarHorarioPorId(idHorario);
	}
	
	
}
