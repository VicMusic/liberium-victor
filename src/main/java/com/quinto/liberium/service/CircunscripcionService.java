package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.CircunscripcionConverter;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Localidad;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MCircunscripcion;
import com.quinto.liberium.repository.CircunscripcionRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service("circunscripcionService")
public class CircunscripcionService extends SimpleService {

	@Autowired
	private CircunscripcionRepository circunscripcionRepository;

	@Autowired
	private CircunscripcionConverter circunscripcionConverter;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Autowired
	private LocalidadService localidadService;

	public MCircunscripcion guardar(String id, String nombre, String idProvincia, List<String> idLocalidades,
			Usuario usuario) {
		Circunscripcion circunscripcion = new Circunscripcion();

		if (!isStringNull(id)) {
			circunscripcion = buscarCircunscripcionPorId(id);
			validarCircunscripcion(nombre, idProvincia, idLocalidades, circunscripcion);
		} else {
			validarCircunscripcion(nombre, idProvincia, idLocalidades, null);
		}

		circunscripcion.setNombre(nombre);

		Provincia provincia = provinciaRepository.buscarPorId(idProvincia);
		circunscripcion.setProvincia(provincia);

		if (idLocalidades.isEmpty() == false) {
			List<Localidad> localidades = new ArrayList<>();
			Localidad localidad;
			for (int i = 0; i < idLocalidades.size(); i++) {
				localidad = new Localidad();
				localidad = localidadService.buscarPorId(idLocalidades.get(i));
				localidades.add(localidad);
			}
			circunscripcion.setLocalidades(localidades);
		}
		return circunscripcionConverter.entidadModelo(guardar(circunscripcion, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Circunscripcion guardar(Circunscripcion circunscripcion, Usuario usuario, String accion) {
		auditar(circunscripcion, usuario, accion);
		return circunscripcionRepository.save(circunscripcion);
	}

	public MCircunscripcion eliminarCircunscripcion(String id, Usuario usuario) {
		Circunscripcion circunscripcion = buscarCircunscripcionPorId(id);
		if (circunscripcion == null) {
			throw new EventoRestException("La circunscripción que trata eliminar no se encuentra en la base de datos.");
		}

		circunscripcion = guardar(circunscripcion, usuario, SimpleService.ELIMINAR);
		return circunscripcionConverter.entidadModelo(circunscripcion);
	}

	private void validarCircunscripcion(String nombre, String idProvincia, List<String> idLocalidades,
			Circunscripcion circunscripcion) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la circunscripcion no puede estar vacio.");
		}

		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException(
					"La provincia a la que pertenece la circunscripcion no puede ser nula.");
		} else {
			if (circunscripcion == null) {
				List<Circunscripcion> otras = buscarCircunscripcionPorNombre(nombre);
				for (Circunscripcion circunscripcion2 : otras) {
					if (nombre.equals(circunscripcion2.getNombre())
							&& idProvincia.equals(circunscripcion2.getProvincia().getId())) {
						throw new PeticionIncorrectaException(
								"El nombre ingresado ya se encuentra utilizado en esta provincia.");
					}
				}
			}
		}

		if (idLocalidades.isEmpty()) {
			throw new PeticionIncorrectaException("Debe seleccionar localidades");
		}

	}

	public List<MCircunscripcion> buscarCircunscripcionModel() {
		return circunscripcionConverter.entidadesModelos(circunscripcionRepository.buscarTodos());
	}

	public Page<Circunscripcion> buscarTodas(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return circunscripcionRepository.buscarTodas(paginable);
		} else {
			 q = "%"+q+"%";
			 List<Circunscripcion> circFinal = new ArrayList<>();
			 List<Circunscripcion> circ = circunscripcionRepository.buscarTodas(q);
			 if(circ != null && !circ.isEmpty()) {
				 circFinal.addAll(circ);
			 }
			 List<Circunscripcion> circLocal = circunscripcionRepository.buscarTodasPorLocalidad(q);
			 if(circLocal != null && !circLocal.isEmpty()) {
				 for(Circunscripcion c : circLocal) {
					 if(!circFinal.contains(c)) {
						 circFinal.add(c);
					 }
				 }
			 }
		     final int start = (int) paginable.getOffset();
		     final int end = Math.min(start + paginable.getPageSize(), circFinal.size());
		     
		     
		     Page<Circunscripcion> circPage = new PageImpl<>(circFinal.subList(start, end), paginable, circFinal.size());
			return circPage;
		}

	}

	public MCircunscripcion crear(MCircunscripcion mCircunscripcion, Usuario creador) {
		if (mCircunscripcion == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mCircunscripcion.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear una circunscripción");
		}
		Circunscripcion circunscripcion = circunscripcionConverter.modeloEntidad(mCircunscripcion);
		return circunscripcionConverter.entidadModelo(guardar(circunscripcion, creador));
	}

	private Circunscripcion guardar(Circunscripcion circunscripcion, Usuario interviniente) {
		return guardar(circunscripcion, interviniente, null);
	}

	public List<MCircunscripcion> buscarCircunscripcionModelTodos() {
		return circunscripcionConverter.entidadesModelos(circunscripcionRepository.buscarTodos());
	}

	public List<Circunscripcion> buscarCircunscripcionTodos() {
		return circunscripcionRepository.buscarTodos();
	}

	public MCircunscripcion buscarCircunscripcionModelPorId(String idCircunscripcion) {
		return circunscripcionConverter.entidadModelo(circunscripcionRepository.buscarPorId(idCircunscripcion));
	}

	public Circunscripcion buscarCircunscripcionPorId(String idCircunscripcion) {
		return circunscripcionRepository.buscarPorId(idCircunscripcion);
	}

	public MCircunscripcion buscarCircunscripcionModelPorNombre(String nombre) {
		return circunscripcionConverter.entidadModelo(circunscripcionRepository.buscarCircuncripcionPorNombre(nombre));
	}

	public List<Circunscripcion> buscarCircunscripcionPorNombre(String nombre) {
		return circunscripcionRepository.buscarCircuncripcionesPorNombre(nombre);
	}

	public List<MCircunscripcion> buscarCircunscripcionModelPorProvincia(String idProvincia) {
		return circunscripcionConverter.entidadesModelos(circunscripcionRepository.buscarPorIdProvincia(idProvincia));
	}

	public List<Circunscripcion> buscarCircunscripcionPorProvincia(String idProvincia) {
		return circunscripcionRepository.buscarPorIdProvincia(idProvincia);
	}

}