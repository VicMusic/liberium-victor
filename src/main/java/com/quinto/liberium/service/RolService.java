package com.quinto.liberium.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.entity.Permiso;
import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.repository.RolRepository;

@Service("rolService")
public class RolService extends SimpleService {

	@Autowired
	private RolRepository rolRepository;

	@Autowired
	private PermisoService permisoService;

	public List<Rol> listarRol() {
		return rolRepository.findAll();
	}

	public List<Rol> buscarTodos() {
		return rolRepository.buscarTodos();
	}

	public List<Rol> buscarTodosPorIdCreador(Usuario creador) {
		return rolRepository.buscarTodosPorIdCreador(creador);
	}

	public Rol buscarPorId(String id) {
		return rolRepository.buscarPorId(id);
	}

	public Rol buscarPorNombre(String nombre) {
		return rolRepository.buscarPorNombre(nombre);
	}

	public Page<Rol> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return rolRepository.buscarTodos(paginable);
		} else {
			return rolRepository.buscarTodos(paginable, "%" + q + "%");
		}
	}

	public Rol guardar(String id, String nombre, String[] permisosChecked, Usuario usuario, String isAbogado) {
		List<Permiso> permisos = permisoService.buscarPorIds(permisosChecked);
		validarRol(nombre, permisos);

		Rol rol = null;
		if (isStringNull(id)) {
			rol = new Rol();
		} else {
			rol = buscarPorId(id);
		}

		if (isAbogado != null && isAbogado.equals("on")) {
			rol.setAbogado(true);
		} else {
			rol.setAbogado(false);
		}

		rol.setNombre(nombre);
		rol.setPermisos(permisos);

		auditar(rol, usuario, ACTUALIZAR);

		return rolRepository.save(rol);
	}

	private void validarRol(String nombre, List<Permiso> permisos) {
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre no puede estar vacio.");
		}

		if (permisos.size() < 1) {
			throw new PeticionIncorrectaException("Debe seleccionar al menos un permiso.");
		}
	}

	public Rol eliminar(String id, Usuario usuario) {
		Rol rol = buscarPorId(id);
		if (rol == null) {
			throw new EventoRestException("El rol que trata eliminar no se encuentra en la base de datos");
		}
		auditar(rol, usuario, "eliminar");
		rolRepository.save(rol);
		return rol;
	}

}