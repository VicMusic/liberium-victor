package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.JurisdiccionConverter;
import com.quinto.liberium.entity.Jurisdiccion;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MJurisdiccion;
import com.quinto.liberium.repository.JurisdiccionRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service("JurisdiccionService")
public class JurisdiccionService extends SimpleService {

	@Autowired
	private JurisdiccionRepository jurisdiccionRepository;

	@Autowired
	private JurisdiccionConverter jurisdiccionConverter;
	
	@Autowired
	private ProvinciaRepository provinciaRepository;

	public MJurisdiccion guardar(String id, String nombre, String idProvincia, Usuario usuario ) {
		Jurisdiccion juris = new Jurisdiccion();
		if(!isStringNull(id)) {
			juris = buscarJurisdiccionPorId(id);
			validarJurisdiccion(nombre, idProvincia, juris);
		}else {
			validarJurisdiccion(nombre, idProvincia, null);
		}
		
		juris.setNombre(nombre);
		Provincia provincia = provinciaRepository.buscarPorId(idProvincia);
		juris.setProvincia(provincia);
		
		return jurisdiccionConverter.entidadModelo(guardar(juris, usuario, SimpleService.ACTUALIZAR));
	}
	
	@Transactional
	private Jurisdiccion guardar(Jurisdiccion jurisdiccion, Usuario usuario, String accion) {
		auditar(jurisdiccion, usuario, accion);
		return jurisdiccionRepository.save(jurisdiccion);
	}

	public MJurisdiccion eliminarJurisdiccion(String id, Usuario usuario) {
		Jurisdiccion jurisdiccion = buscarJurisdiccionPorId(id);
		if (jurisdiccion == null) {
			throw new EventoRestException("La jurisdicción que trata eliminar no se encuentra en la base de datos.");
		}

		jurisdiccion = guardar(jurisdiccion, usuario, SimpleService.ELIMINAR);
		return jurisdiccionConverter.entidadModelo(jurisdiccion);
	}

	private void validarJurisdiccion(String nombre, String idPais, Jurisdiccion jurisdiccion) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la jurisdicción no puede estar vacio.");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException(
					"La provincia a la que pertenece la jurisdicción no puede ser nula.");
		} else {
			Jurisdiccion otra = buscarJurisdiccionPorNombre(nombre);
			if ((jurisdiccion == null && otra != null)
					|| (jurisdiccion != null && otra != null && !jurisdiccion.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public List<MJurisdiccion> buscarJurisdiccionModel() {
		return jurisdiccionConverter.entidadesModelos(jurisdiccionRepository.buscarTodos());
	}

	public Page<Jurisdiccion> buscarTodas(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return jurisdiccionRepository.buscarTodas(paginable);
		} else {
			return jurisdiccionRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public MJurisdiccion crear(MJurisdiccion mJurisdiccion, Usuario creador) {
		if (mJurisdiccion == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mJurisdiccion.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear una jurisdicción");
		}
		Jurisdiccion jurisdiccion = jurisdiccionConverter.modeloEntidad(mJurisdiccion);
		return jurisdiccionConverter.entidadModelo(guardar(jurisdiccion, creador));
	}

	private Jurisdiccion guardar(Jurisdiccion jurisdiccion, Usuario interviniente) {
		return guardar(jurisdiccion, interviniente, null);
	}

	public List<MJurisdiccion> buscarJurisdiccionModelTodos() {
		return jurisdiccionConverter.entidadesModelos(jurisdiccionRepository.buscarTodos());
	}

	public List<Jurisdiccion> buscarJurisdiccionTodos() {
		return jurisdiccionRepository.buscarTodos();
	}

	public MJurisdiccion buscarJurisdiccionModelPorId(String idJurisdiccion) {
		return jurisdiccionConverter.entidadModelo(jurisdiccionRepository.buscarPorId(idJurisdiccion));
	}

	public Jurisdiccion buscarJurisdiccionPorId(String idJurisdiccion) {
		return jurisdiccionRepository.buscarPorId(idJurisdiccion);
	}

	public MJurisdiccion buscarJurisdiccionModelPorNombre(String nombre) {
		return jurisdiccionConverter.entidadModelo(jurisdiccionRepository.buscarPorNombre(nombre));
	}

	public Jurisdiccion buscarJurisdiccionPorNombre(String nombre) {
		return jurisdiccionRepository.buscarPorNombre(nombre);
	}

	public List<MJurisdiccion> buscarJurisdiccionModelPorProvincia(String idProvincia) {
		return jurisdiccionConverter.entidadesModelos(jurisdiccionRepository.buscarPorIdProvincia(idProvincia));
	}

	public List<Jurisdiccion> buscarJurisdiccionPorProvincia(String idProvincia) {
		return jurisdiccionRepository.buscarPorIdProvincia(idProvincia);
	}


}
