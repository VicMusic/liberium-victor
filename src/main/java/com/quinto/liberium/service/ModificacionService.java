package com.quinto.liberium.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Modificacion;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.repository.ModificacionRepository;

@Service("modificacionService")
public class ModificacionService extends SimpleService{

	
	@Autowired
	private ModificacionRepository modificacionRepository;
	
	public Modificacion guardarModificacion(String tipoModificacion, Causa causa, Usuario usuario) {
		Modificacion modificacion = new Modificacion();
		
		modificacion.setTipoModificacion(tipoModificacion);
		modificacion.setCausa(causa);
		
		auditar(modificacion, usuario, ACTUALIZAR);		
		return modificacionRepository.save(modificacion);
	}
	
	
	public List<Modificacion> buscarPorCausa(String causaId){
		return modificacionRepository.buscarPorCausa(causaId);
	}
	
}
