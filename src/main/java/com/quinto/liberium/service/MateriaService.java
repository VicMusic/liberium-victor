package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.MateriaConverter;
import com.quinto.liberium.entity.Materia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MMateria;
import com.quinto.liberium.repository.MateriaRepository;

@Service("materiaService")
public class MateriaService extends SimpleService {

	@Autowired
	private MateriaRepository materiaRepository;

	@Autowired
	private MateriaConverter materiaConverter;

	public MMateria guardar(String id, String nombre, Usuario usuario) {
		Materia materia = new Materia();

		if (!isStringNull(id)) {
			materia = buscarMateriaPorId(id);
			validarMateria(nombre, materia);
		} else {
			validarMateria(nombre, null);
		}

		materia.setNombre(nombre);
		
		return materiaConverter.entidadModelo(guardar(materia, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Materia guardar(Materia materia, Usuario usuario, String accion) {
		auditar(materia, usuario, accion);
		return materiaRepository.save(materia);
	}

	public MMateria eliminarMateria(String id, Usuario usuario) {
		Materia materia = buscarMateriaPorId(id);
		if (materia == null) {
			throw new EventoRestException("La materia que trata eliminar no se encuentra en la base de datos.");
		}

		materia = guardar(materia, usuario, SimpleService.ELIMINAR);
		return materiaConverter.entidadModelo(materia);
	}

	private void validarMateria(String nombre, Materia materia) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la materia no puede estar vacio.");
		}
		Materia otra = buscarMateriasPorNombre(nombre);
		if (otra != null) {
			throw new PeticionIncorrectaException("El nombre de la materia ingresada ya se encuentra utilizado.");
		}

	}

	public List<MMateria> buscarMateriasModel() {
		return materiaConverter.entidadesModelos(materiaRepository.buscarTodos());
	}

	public Page<Materia> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return materiaRepository.buscarTodas(paginable);
		} else {
			return materiaRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public MMateria crear(MMateria mMateria, Usuario creador) {
		if (mMateria == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mMateria.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear un país");
		}
		Materia materia = materiaConverter.modeloEntidad(mMateria);
		return materiaConverter.entidadModelo(guardar(materia, creador));
	}

	public void eliminar(String idMateria, Usuario eliminador) {
		if (isStringNull(idMateria) || eliminador == null) {
			throw new PeticionIncorrectaException("Ocurrio un error al eliminar el país");
		}
		Materia materia = materiaRepository.buscarPorId(idMateria);
		guardar(materia, eliminador, ELIMINAR);
	}

	private Materia guardar(Materia materia, Usuario interviniente) {
		return guardar(materia, interviniente, null);
	}

	public List<MMateria> buscarMateriaModelTodos() {
		return materiaConverter.entidadesModelos(materiaRepository.buscarTodos());
	}

	public List<Materia> buscarMateriaTodos() {
		return materiaRepository.buscarTodos();
	}

	public MMateria buscarMateriaModelPorId(String idMateria) {
		return materiaConverter.entidadModelo(materiaRepository.buscarPorId(idMateria));
	}

	public Materia buscarMateriaPorId(String idMateria) {
		return materiaRepository.buscarPorId(idMateria);
	}

	public MMateria buscarMateriaModelPorNombre(String nombre) {
		return materiaConverter.entidadModelo(materiaRepository.buscarPorNombre(nombre));
	}

	public Materia buscarMateriasPorNombre(String nombre) {
		return materiaRepository.buscarMateriasPorNombre(nombre);
	}

}