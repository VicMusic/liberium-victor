package com.quinto.liberium.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.OficinaAdministrativaConverter;
import com.quinto.liberium.entity.OficinaAdministrativa;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MOficinaAdministrativa;
import com.quinto.liberium.repository.OficinaAdministrativaRepository;
import com.quinto.liberium.repository.ProvinciaRepository;

@Service("oficinaAdministrativaService")
public class OficinaAdministrativaService extends SimpleService {

	@Autowired
	private OficinaAdministrativaRepository oficinaRepository;

	@Autowired
	private OficinaAdministrativaConverter oficinaConverter;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	public MOficinaAdministrativa guardar(String id, String nombre, String idProvincia, Usuario usuario) {
		OficinaAdministrativa oficina = new OficinaAdministrativa();

		if (!isStringNull(id)) {
			oficina = buscarOficinaAdministrativaPorId(id);
			validarOficinaAdministrativa(nombre, idProvincia, oficina);
		} else {
			validarOficinaAdministrativa(nombre, idProvincia, null);
		}

		oficina.setNombre(nombre);

		Provincia provincia = provinciaRepository.buscarPorId(idProvincia);
		oficina.setProvincia(provincia);

		return oficinaConverter.entidadModelo(guardar(oficina, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private OficinaAdministrativa guardar(OficinaAdministrativa oficina, Usuario usuario, String accion) {
		auditar(oficina, usuario, accion);
		return oficinaRepository.save(oficina);
	}

	public MOficinaAdministrativa eliminarOficinaAdministrativa(String id, Usuario usuario) {
		OficinaAdministrativa oficina = buscarOficinaAdministrativaPorId(id);
		if (oficina == null) {
			throw new EventoRestException("La oficina que trata eliminar no se encuentra en la base de datos.");
		}

		oficina = guardar(oficina, usuario, SimpleService.ELIMINAR);
		return oficinaConverter.entidadModelo(oficina);
	}

	private void validarOficinaAdministrativa(String nombre, String idPais, OficinaAdministrativa oficina) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre de la oficina no puede estar vacio.");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException(
					"La provincia a la que pertenece la oficina no puede ser nula.");
		} else {
			OficinaAdministrativa otra = buscarOficinaAdministrativaPorNombre(nombre);
			if ((oficina == null && otra != null)
					|| (oficina != null && otra != null && !oficina.getId().equals(otra.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public List<MOficinaAdministrativa> buscarOficinaAdministrativaModel() {
		return oficinaConverter.entidadesModelos(oficinaRepository.buscarTodos());
	}

	public Page<OficinaAdministrativa> buscarTodas(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return oficinaRepository.buscarTodas(paginable);
		} else {
			return oficinaRepository.buscarTodas(paginable, "%" + q + "%");
		}

	}

	public MOficinaAdministrativa crear(MOficinaAdministrativa mOficinaAdministrativa, Usuario creador) {
		if (mOficinaAdministrativa == null || creador == null) {
			throw new PeticionIncorrectaException("");
		}
		if (isStringNull(mOficinaAdministrativa.getNombre())) {
			throw new PeticionIncorrectaException("Debe ingresar un nombre para crear un país");
		}
		OficinaAdministrativa oficina = oficinaConverter.modeloEntidad(mOficinaAdministrativa);
		return oficinaConverter.entidadModelo(guardar(oficina, creador));
	}

	private OficinaAdministrativa guardar(OficinaAdministrativa oficina, Usuario interviniente) {
		return guardar(oficina, interviniente, null);
	}

	public List<MOficinaAdministrativa> buscarOficinaAdministrativaModelTodos() {
		return oficinaConverter.entidadesModelos(oficinaRepository.buscarTodos());
	}

	public List<OficinaAdministrativa> buscarOficinaAdministrativaTodos() {
		return oficinaRepository.buscarTodos();
	}

	public MOficinaAdministrativa buscarOficinaAdministrativaModelPorId(String idCircunscripcion) {
		return oficinaConverter.entidadModelo(oficinaRepository.buscarPorId(idCircunscripcion));
	}

	public OficinaAdministrativa buscarOficinaAdministrativaPorId(String idCircunscripcion) {
		return oficinaRepository.buscarPorId(idCircunscripcion);
	}

	public MOficinaAdministrativa buscarOficinaAdministrativaModelPorNombre(String nombre) {
		return oficinaConverter.entidadModelo(oficinaRepository.buscarPorNombre(nombre));
	}

	public OficinaAdministrativa buscarOficinaAdministrativaPorNombre(String nombre) {
		return oficinaRepository.buscarPorNombre(nombre);
	}

	public List<MOficinaAdministrativa> buscarOficinaAdministrativaModelPorProvincia(String idProvincia) {
		return oficinaConverter.entidadesModelos(oficinaRepository.buscarPorIdProvincia(idProvincia));
	}

	public List<OficinaAdministrativa> buscarOficinaAdministrativaPorProvincia(String idProvincia) {
		return oficinaRepository.buscarPorIdProvincia(idProvincia);
	}

}
