package com.quinto.liberium.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.EventoConverter;
import com.quinto.liberium.entity.Causa;
import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoPlazo;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MEvento;
import com.quinto.liberium.repository.EventoRepository;
import com.quinto.liberium.repository.TurnoRepository;

@Service("eventoService")
public class EventoService extends SimpleService {

	@Autowired
	private EventoConverter eventoConverter;

	@Autowired
	private EventoRepository eventoRepository;
	
	@Autowired
	private TurnoRepository turnoRepository;
	
	@Autowired
	private CausaService causaService;

	public Evento guardar(String idEvento, String titulo, String tipoEvento, Date fechaInicio, Date fechaFin,
			Integer cantDias, String tipoPlazo, TipoDiaNoLaborable tipoDiaNoLaborable, Turno turno, Usuario usuario)
			throws ParseException {

		Evento evento = new Evento();

		if (cantDias == null) {
			cantDias = 0;
		}
		
		if (!isStringNull(idEvento)) {
			evento = buscarPorId(idEvento);
			if (tipoEvento.equals("PLAZO")) {
				validarPlazo(titulo, tipoEvento, fechaInicio, cantDias, tipoPlazo);
			} else {
				validarEvento(titulo, tipoEvento, fechaInicio, fechaFin);
			}
		} else if (tipoEvento.equals("PLAZO")) {
			validarPlazo(titulo, tipoEvento, fechaInicio, cantDias, tipoPlazo);
		} else {
			validarEvento(titulo, tipoEvento, fechaInicio, fechaFin);
		}

		validarDiaNoLaboral(usuario, fechaInicio);
		validarEventoUsuario(usuario, fechaInicio);
		
		if (tipoEvento.equals("PLAZO")) {

			evento.setTitulo(titulo);
			evento.setActivo(true);
			evento.setTipoEvento(tipoEvento);
			evento.setCantDias(cantDias);
			setColor(evento);

			DateTime fechaInicioPlazo = new DateTime(fechaInicio);

			if (tipoPlazo.equals("DIAS_HABILES")) {
				evento = crearPlazo(fechaInicioPlazo, cantDias, tipoPlazo, evento, usuario, tipoDiaNoLaborable);
			} else {

				DateTime fechaFinal = new DateTime(fechaInicio);

				evento.setFechaFin(fechaFinal.plusDays(cantDias - 1).toDate());
				evento.setFechaInicio(fechaInicio);
				evento.setTipoPlazo(TipoPlazo.CORRIDO);
				evento.setTitulo(titulo);
				evento.setActivo(true);
				evento.setTipoEvento(tipoEvento);
				evento.setCantDias(cantDias);

				setColor(evento);
			}

		} else {

			evento.setTitulo(titulo);
			evento.setActivo(true);
			evento.setTipoEvento(tipoEvento);
			evento.setFechaInicio(fechaInicio);
			evento.setFechaFin(fechaFin);
			evento.setTipoPlazo(TipoPlazo.CORRIDO);
			evento.setTipoDiaNoLaborable(tipoDiaNoLaborable);
			evento.setCantDias(1);

			if (tipoEvento.equals("DIA_INHABIL") || tipoEvento.equals("FERIADO") || tipoEvento.equals("FERIA_JUDICIAL")) {
				evento.setDiaNoLaboral(true);
			} else {
				evento.setDiaNoLaboral(false);
			}
			
			setColor(evento);
		}
		evento.setTurno(turno);
		return guardar(evento, usuario, ACTUALIZAR);
	}

	public Evento crearPlazo(DateTime fechaInicio, Integer cantDias, String tipoPlazo, Evento evento, Usuario usuario,
			TipoDiaNoLaborable tipoDiaNoLaborable) throws ParseException {

		DateTime fechaWhile = fechaInicio;
		HashSet<DateTime> diasNoLaborales  = buscarDiasNoLaborales(usuario, tipoDiaNoLaborable);

		Integer i = 0;
		int y = cantDias;
		while (i < cantDias) {
			if (validarFinDeSemana(fechaWhile) == false && validarFechaActual(diasNoLaborales, fechaWhile) == false) {
				fechaWhile = fechaWhile.plusDays(1);
				i++;
			} else {
				fechaWhile = fechaWhile.plusDays(1);
			}
		}
		
		DateTime fechaFinal = fechaWhile;

		evento.setTipoPlazo(TipoPlazo.DIAS_HABILES);
		evento.setTipoDiaNoLaborable(tipoDiaNoLaborable);
		evento.setFechaInicio(fechaInicio.toDate());
		evento.setFechaFin(fechaFinal.minusDays(1).toDate());
		evento.setCantDias(cantDias);

		return evento;

	}
	
	public void crearEventoItemSubItem(String idCausa, String idItemSubItem, String titulo, String tipoEvento, String tipoPlazo, Date fechaHoy, Integer cantDias) throws ParseException {
		Causa causa = causaService.buscarPorId(idCausa);
		TipoDiaNoLaborable tipoDiaNoLaborable;
		if(causa.getFuero() == TipoFuero.FEDERAL) {
			tipoDiaNoLaborable = TipoDiaNoLaborable.FEDERAL;
		}else {
			tipoDiaNoLaborable = TipoDiaNoLaborable.PROVINCIAL;
		}
		for (Miembro miembro : causa.getMiembrosCausa()) {
			guardar(null, titulo, tipoEvento, fechaHoy, null, cantDias, tipoPlazo, tipoDiaNoLaborable, null,  miembro.getUsuario());
			
		}
		guardar(null, titulo, tipoEvento, fechaHoy, null, cantDias, tipoPlazo, tipoDiaNoLaborable, null, causa.getCreador());
	}

	public Evento eliminarEventoRest(String id, Usuario usuario) {
		Evento evento = buscarPorId(id);
		if (evento == null) {
			throw new PeticionIncorrectaException("El evento que trata eliminar no se encuentra en la base de datos");
		}
		auditar(evento, usuario, ELIMINAR);
		return eventoRepository.save(evento);
	}

	public boolean validarFechaActual(HashSet<DateTime> diasNoLaborales, DateTime fechaActual) {
		
		List<DateTime> diasNoLaborablesSort = new ArrayList<>();
		diasNoLaborablesSort.addAll(diasNoLaborales);
		Collections.sort(diasNoLaborablesSort);
		
		String fechaNoLaborableFor = null;
		String fechaActualFor = null;

		for (DateTime fechaNolaboral : diasNoLaborablesSort) {
			fechaNoLaborableFor = fechaNolaboral.getMonthOfYear() + "-" + fechaNolaboral.getDayOfMonth();
			fechaActualFor = fechaActual.getMonthOfYear() + "-" + fechaActual.getDayOfMonth();
			if (fechaActualFor.equals(fechaNoLaborableFor)) {
				diasNoLaborablesSort.remove(fechaNolaboral);
				return true;
			} 
		}
		return false;
	}

	public boolean validarFinDeSemana(DateTime fechaActual) {
		if (fechaActual.getDayOfWeek() == 6 || fechaActual.getDayOfWeek() == 7) {
			System.out.println(fechaActual + " ES FIN DE SEMANA!");
			return true;
		}
		return false;
	}

	public void correccionFecheFinal(List<MEvento> eventos) {

		for (MEvento mEvento : eventos) {
			if (mEvento.getTipoEvento().equals("FERIADO") || mEvento.getTipoEvento().equals("FERIA_JUDICIAL")
					|| mEvento.getTipoEvento().equals("DIA_INHABIL")) {
				DateTime fechaInicio = new DateTime(mEvento.getFechaInicio());
				DateTime fechaFin = new DateTime(mEvento.getFechaFin());
				String string1 = fechaInicio.getMonthOfYear() + "-" + fechaInicio.getDayOfMonth();
				String string2 = fechaFin.getMonthOfYear() + "-" + fechaFin.getDayOfMonth();
				if (!string1.equals(string2)) {
					mEvento.setFechaFin(fechaFin.plusDays(1).toString());
				}

			}
		}

	}

	public HashSet<DateTime> buscarDiasNoLaborales(Usuario usuario, TipoDiaNoLaborable tipoDiaNoLaborable)
			throws ParseException {

		HashSet<DateTime> diasNoLaborales = new HashSet<>();

		List<Evento> eventosNolaborales = new ArrayList<>();
		List<Evento> eventosFederalesProvinciales = buscarEventosFederalesProvinciales(usuario);
		List<Evento> eventosFederales = buscarEventosFederales(usuario);

		if (tipoDiaNoLaborable.equals(TipoDiaNoLaborable.PROVINCIAL)) {
			eventosNolaborales.addAll(eventosFederalesProvinciales);
		} else if (tipoDiaNoLaborable.equals(TipoDiaNoLaborable.FEDERAL)) {
			eventosNolaborales.addAll(eventosFederales);
		}

		for (Evento evento : eventosNolaborales) {

			DateTime fechaInicio = new DateTime(evento.getFechaInicio());
			DateTime fechaFin = new DateTime(evento.getFechaFin());
			DateTime fechaActual = fechaInicio;

			while (fechaActual.getDayOfMonth() != fechaFin.getDayOfMonth()
					|| fechaActual.getMonthOfYear() != fechaFin.getMonthOfYear()) {
				diasNoLaborales.add(fechaActual);
				fechaActual = fechaActual.plusDays(1);
			}
			diasNoLaborales.add(fechaFin);
		}

		return diasNoLaborales;
	}

	@Transactional
	public Evento guardar(Evento evento, Usuario interviniente, String accion) {
		auditar(evento, interviniente, accion);
		return eventoRepository.save(evento);
	}

	private void validarDiaNoLaboral(Usuario usuario, Date fechaInicio) throws ParseException {
		HashSet<DateTime> diasNoLaborales  = buscarDiasNoLaborales(usuario, TipoDiaNoLaborable.PROVINCIAL);
		DateTime fechaEvento = new DateTime(fechaInicio);
		String fechaNoLaborableFor = null;
		String fechaActualFor = fechaEvento.getMonthOfYear() + "-" + fechaEvento.getDayOfMonth();
		for (DateTime diaNoLaboral : diasNoLaborales) {
			fechaNoLaborableFor = diaNoLaboral.getMonthOfYear() + "-" + diaNoLaboral.getDayOfMonth();
			if (fechaActualFor.equals(fechaNoLaborableFor)) {
				throw new PeticionIncorrectaException("No se puede guardar un evento durante un día no laboral!");
			}
		}
	}
	
	private void validarEventoUsuario(Usuario usuario, Date fechaInicio) {
		List<Evento> eventos = buscarEventosPorUsuario(usuario);
		DateTime fechaEvento = new DateTime(fechaInicio);
		for (Evento evento : eventos) {
			DateTime a = new DateTime(evento.getFechaInicio());
			if (fechaEvento.isEqual(a)) {
				throw new PeticionIncorrectaException("Ya existe un evento en ese día y hora!");
			}
		}
	}
	
	private void validarEvento(String titulo, String tipoEvento, Date fechaInicio, Date fechaFin) {

		if (isStringNull(titulo)) {
			throw new PeticionIncorrectaException("El título del evento no puede estar vacio!");
		}

		if (isStringNull(tipoEvento)) {
			throw new PeticionIncorrectaException("El Tipo de evento no puede estar vacio!");
		}

		if (fechaInicio.equals(null)) {
			throw new PeticionIncorrectaException("La fecha del evento no puede estar vacía!");
		}
		
		DateTime fechaEvento = new DateTime(fechaInicio);
		DateTime fechaActual = new DateTime(new Date());
		/*
		if (fechaEvento.isBefore(fechaActual.minusDays(1))) {
			throw new PeticionIncorrectaException("No se puede guardar un evento con fecha anterior a la actual!");
		}
		*/
		if (fechaFin.equals(null)) {
			throw new PeticionIncorrectaException("La fecha del evento no puede estar vacia!");
		}
		
		DateTime fechaFinal = new DateTime(fechaFin);
		
		if (fechaFinal.isBefore(fechaEvento)) {
			throw new PeticionIncorrectaException("La fecha final del evento no puede ser anterior a la inicial!");
		}
		
		Integer fecha1 = fechaEvento.getYear();
		Integer fecha2 = fechaFinal.getYear();

		if (fecha1.toString().length() >= 5) {
			throw new PeticionIncorrectaException("El año de la fecha inicial es incorrecto!");
		}
		
		if (fecha2.toString().length() >= 5) {
			throw new PeticionIncorrectaException("El año de la fecha final es incorrecto!");
		}
	}
	
	
	private void validarPlazo(String titulo, String tipoEvento, Date fechaInicio, Integer cantDias, String tipoPlazo) {

		if (isStringNull(titulo)) {
			throw new PeticionIncorrectaException("El título del plazo no puede estar vacio!");
		}

		if (isStringNull(tipoEvento)) {
			throw new PeticionIncorrectaException("El Tipo de evento no puede estar vacio!");
		}

		if (fechaInicio.equals(null)) {
			throw new PeticionIncorrectaException("La fecha de inicio del plazo no puede estar vacia!");
		}
		
		DateTime fechaEvento = new DateTime(fechaInicio);
		DateTime fechaActual = new DateTime(new Date());
		
		if (fechaEvento.isBefore(fechaActual.minusDays(1))) {
			throw new PeticionIncorrectaException("No se puede guardar un evento con fecha anterior a la actual!");
		}
		
		if (cantDias == 0) {
			throw new PeticionIncorrectaException("La cantidad de días del plazo no puede estar vacia!");
		}

		if (isStringNull(tipoPlazo)) {
			throw new PeticionIncorrectaException("El tipo de plazo no puede estar vacio!");
		}
	}

	public Evento setColor(Evento evento) {

		String color = "";

		switch (evento.getTipoEvento()) {
		case "REUNION":
			color = "green";
			evento.setColor(color);
			break;
		case "AUDIENCIA":
			color = "blue";
			evento.setColor(color);
			break;
		case "TURNO":
			color = "salmon";
			evento.setColor(color);
			break;
		case "VENCIMIENTO_PROCESAL":
			color = "red";
			evento.setColor(color);
			break;
		case "VENCIMIENTO_COBRO":
			color = "orange";
			evento.setColor(color);
			break;
		case "PLAZO":
			color = "coral";
			evento.setColor(color);
			break;
		case "FERIADO":
			color = "navy";
			evento.setColor(color);
			break;
		case "FERIA_JUDICIAL":
			color = "navy";
			evento.setColor(color);
			break;
		case "DIA_INHABIL":
			color = "navy";
			evento.setColor(color);
			break;
		default:
			color = "black";
			evento.setColor(color);
			break;
		}

		return evento;
	}

	public static Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public List<Evento> buscarEventosFederalesProvinciales(Usuario usuario) {
		return eventoRepository.buscarEventosFederalesProvinciales(usuario);
	}

	public List<Evento> buscarEventosFederales(Usuario usuario) {
		return eventoRepository.buscarEventosFederales(usuario);
	}

	public Evento buscarEventoPorColorUsuarioInicioFin(Usuario usuario, String titulo, String color, Date inicio,
			Date fin) {
		return eventoRepository.buscarEventoPorColorFechasYUsuario(usuario, titulo, color, inicio, fin);
	}

	public List<Evento> buscarEventosPorUsuario(Usuario idUsuario) {
		return eventoRepository.buscarPorUsuario(idUsuario);
	}

	public List<MEvento> buscarMEventosPorUsuario(Usuario idUsuario) {
		return eventoConverter.entidadesModelos(eventoRepository.buscarPorUsuario(idUsuario));
	}

	public List<MEvento> buscarEventosPorUsuarioYFechas(Date desde, Date hasta, Usuario usuario) {
		return eventoConverter.entidadesModelos(eventoRepository.buscarPorFechas(desde, hasta, usuario));
	}

	public List<Evento> listarEvento() {
		return eventoRepository.findAll();
	}

	public List<MEvento> listarEventosPorTipoEvento(Usuario usuario, String tipoEvento) {
		return eventoConverter.entidadesModelos(eventoRepository.buscarEventosPorTipoDeEvento(usuario, tipoEvento));
	}

	public HashSet<String> buscarOtrosTipoDeEvento(Usuario usuario) {
		return eventoRepository.buscarOtrosTipoDeEventos(usuario);
	}

	public List<MEvento> listarMEvento() {
		List<Evento> eventos = listarEvento();
		return eventoConverter.entidadesModelos(eventos);
	}

	public MEvento buscarMEventoPorId2(String eventoId) {
		Turno t = turnoRepository.buscarPorId(eventoId);
		return eventoConverter.entidadModelo(eventoRepository.buscarPorId(t.getEvento().getId()));
	}
	
	public MEvento buscarMEventoPorId(String eventoId) {
		return eventoConverter.entidadModelo(eventoRepository.buscarPorId(eventoId));
	}

	public Evento buscarPorTitulo(String titulo) {
		return eventoRepository.buscarPorTitulo(titulo);
	}

	public Evento buscarPorId(String idEvento) {
		return eventoRepository.buscarPorId(idEvento);
	}

	public List<MEvento> buscarEventoNoLaboralesPorUsuario(Usuario usuario) {
		return eventoConverter.entidadesModelos(eventoRepository.buscarEventosNoLaborablesPorUsuario(usuario));
	}

	public List<Evento> buscarEventoNoLaboralesPorProvincia(Provincia provincia) {
		return eventoRepository.buscarEventosNoLaboralesPorProvincia(provincia);
	}

	public List<Evento> buscarEventoNoLaboralesPorPais(Pais pais) {
		return eventoRepository.buscarEventosNoLaboralesPorPais(pais);
	}
}