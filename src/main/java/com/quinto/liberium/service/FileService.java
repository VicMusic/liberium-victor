package com.quinto.liberium.service;

import static com.quinto.liberium.util.Constantes.PUERTO_ESCRITOS;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class FileService {
	private final Log LOG = LogFactory.getLog(FileService.class);
	
	private static final int BUFFER_SIZE = 4096;

	@Autowired
	private ServletContext servlet;

/**	public void descargar(String ruta, HttpServletResponse response, String nombre) {
		File fullPath = new File(ruta);		
	
		LOG.info("DESCARGANDO ESCRITO: " + nombre + " tipo: " + getExtensionByApacheCommonLib(fullPath.getName()));
		
		if (fullPath.exists()) {
			try {
				FileInputStream fileInputStream = new FileInputStream(fullPath);
				String mimeType = servlet.getMimeType(ruta);
				LOG.info(mimeType);
				response.setContentType(mimeType);
				response.addHeader("Content-Disposition", "attachment; filename=" + nombre + "." + getExtensionByApacheCommonLib(fullPath.getName()));
				OutputStream out = response.getOutputStream();
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				while ((bytesRead = fileInputStream.read(buffer)) != -1) {
					out.write(buffer, 0, bytesRead);
				}
				fileInputStream.close();
				out.flush();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
//	} **/

	public HttpServletResponse descargar(String ruta, HttpServletResponse response, String extension, String nombre) {
        try {
                         // ruta se refiere a la ruta del archivo a descargar.
            File file = new File(ruta);
            
                         // Obtener el nombre del archivo.
            String filename = file.getName();
                         // Obtener el nombre de la extensión del archivo.
            String ext = "";
            if(extension != null && !extension.isEmpty()) {
            	 ext = extension;
            }else {
            	 ext = filename.substring(filename.lastIndexOf(".") + 1);
            }
            
            LOG.info("DESCARGANDO ESCRITO: " + filename + " tipo: " + ext);
                         // Descarga el archivo como una secuencia.
            InputStream fis = new BufferedInputStream(new FileInputStream(ruta));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
                         // Respuesta clara
            response.reset();
                         // Establecer el encabezado de la respuesta
            String name = new String(filename.getBytes());
            if(name.contains(ext)) {
            	name = name.replace("."+ext, "");
            }
            if(nombre != null && !nombre.isEmpty()) {
            	name = nombre;
            }
            response.addHeader("Content-Disposition", "attachment;filename=" + name +'.'+ new String(ext.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
        
    }
	public String confirmarArchivo(String ruta, String titulo, String extencion) {
		confirmarRuta(ruta);
		//Los espacios producen verdadera interferencia
		titulo = titulo.replace(" ", "_");
		String rutaCompleta = ruta + titulo + extencion;

		// si ya existe uno le agrego un contador
		File existente = new File(rutaCompleta);
		int fileNo = 0;
		while (existente.exists()) {
			fileNo++;
			int punto = rutaCompleta.lastIndexOf('.');
			String extension = rutaCompleta.substring(punto, rutaCompleta.length());
			String nombre = rutaCompleta.substring(0, punto);
			rutaCompleta = numerarCorrectamente(nombre, fileNo, extension);

			existente = new File(rutaCompleta);
		}
		return rutaCompleta;
	}

	public String numerarCorrectamente(String nombre, int fileNo, String extension) {
		int aux = nombre.lastIndexOf('(');
		if (aux != -1 && (aux + 2) < nombre.length()) {
			nombre = nombre.substring(0, aux);
		}

		String rutaCompleta = nombre + "(" + fileNo + ")" + extension;

		return rutaCompleta;
	}

	private void confirmarRuta(String ruta) {
		File directorio = new File(ruta);

		if (!directorio.exists()) {
			directorio.mkdir();
		}

	}

	public void eliminarArchivoXRuta(String ruta) {
		File fila = new File(ruta);
		if (fila.exists()) {
			fila.delete();
		}
	}

	public void eliminarEscritoXNombre(String nombreArchivo) {// se requiere de extencion tambien...
		File fila = new File(PUERTO_ESCRITOS + nombreArchivo);
		if (fila.exists()) {
			fila.delete();
		}
	}

	public String getExtensionByApacheCommonLib(String filename) {
	    return FilenameUtils.getExtension(filename);
	}
}
