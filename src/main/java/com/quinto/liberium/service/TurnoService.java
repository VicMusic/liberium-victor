package com.quinto.liberium.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.TurnoConverter;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Consulta;
import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Horario;
import com.quinto.liberium.entity.Turno;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MTurno;
import com.quinto.liberium.repository.TurnoRepository;

@Service("turnoService")
public class TurnoService extends SimpleService {
	private static final Logger log = LoggerFactory.getLogger(TurnoService.class);
	
	@Autowired
	private TurnoRepository turnoRepository;

	@Autowired
	private TurnoConverter turnoConverter;

	@Autowired
	private AbogadoService abogadoService;

	@Autowired
	private EmailService emailSender;

	@Autowired
	private HorarioService horarioService;
	
	@Autowired
	private ConsultaService consultaService;

	@Autowired
	private EventoService eventoService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private NotificacionService notificacionService;

	public MTurno guardar(String fecha, String idAbogado, String mailCliente) {

		Abogado abogado = abogadoService.buscarAbogadoPorId(idAbogado);

		validarTurno(abogado, fecha, mailCliente);

		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		DateTime dt = formatter.parseDateTime(fecha);
		
		DateTime fechaActual = new DateTime();
		
		if (dt.isBefore(fechaActual)) {
			throw new PeticionIncorrectaException("No puede elegir una fecha anterior a la actual!");
		}
		
		Turno turno = new Turno();
		
		
		

		String fechaAmigable = crearFechaAmigable(dt);
		String fechaOriginal = crearFechaHorario(dt);
		
		DecimalFormat df = new DecimalFormat("00");
		String hora = df.format(dt.getHourOfDay()).toString();
		String minutos = df.format(dt.getMinuteOfHour()).toString();
		
		turno.setFechaTurno(dt.toDate()); // ESTA SERIA LA FECHA COMPLETA EN FORMATO DATE
		turno.setFecha(fechaAmigable); // ESTA SERIA LA FECHA AMIGABLE
		turno.setFechaOriginal(fechaOriginal); // ESTE SERIA DIA Y HORA DE HORARIO (X0730)
		turno.setHora(hora + ":" + minutos); // ESTA SERIA LA HORA CON DOS PUNTOS ENTRE LA HORA Y LOS MINUTOS (07:30)

		turno.setMailCliente(mailCliente);
		turno.setAbogado(abogado);
		turno.setDireccion(abogado.getDomicilio());

		return turnoConverter.entidadModelo(guardar(turno, null, ACTUALIZAR));
	}

	public MTurno notificarTurnoAbogado(String mailAbogado, String nombreCliente, String telefonoCliente, String mailCliente, String original) throws ParseException {

		Turno turno = buscarTurno(mailAbogado, mailCliente, original);
		if(nombreCliente.equals("a")) {
			Consulta c = consultaService.buscarPorTurnoId(turno.getId());
			nombreCliente = c.getNombre() + ' ' + c.getApellido();
			telefonoCliente = c.getTelefono();
		}
		
		turno.setNombreCliente(nombreCliente);
		turno.setTelefonoCliente(telefonoCliente);
		
		Abogado abogado = abogadoService.buscarAbogadoPorMailUsuario(mailAbogado);

		turno.setCancelado(false);
		turno = guardar(turno, null, ACTUALIZAR);
		turnoSolicitadoMail(turno);

		try {
			emailSender.mailTurnoClienteYAbogado(turno);			
		} catch (Exception e) {
			
		}

		String dia = turno.getFechaOriginal();
		String diaLetra = dia.substring(0, 1);
		
		Horario horarioSolicitado = horarioService.buscarHorarioSolicitadoPorAbogado(abogado.getId(), diaLetra, turno.getHora());
		horarioSolicitado.setSolicitado(true);

		horarioService.guardar(horarioSolicitado, null, ACTUALIZAR);
		
		notificacionService.guardarUrl(null, "Se registro un turno a su nombre", abogado.getUsuario(),"calendario/ver",turno.getId());

		return turnoConverter.entidadModelo(turno);

	}

	public void turnoSolicitadoMail(Turno turno) throws ParseException {

		Turno turnoSolicitado = buscarPorId(turno.getId());
		turnoSolicitado.setTokenCancelado(UUID.randomUUID().toString());
		
		Abogado a = turnoSolicitado.getAbogado();
		Integer duracion = a.getDuracion();

		Date dateFechaInicio = turnoSolicitado.getFechaTurno();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateFechaInicio); //tuFechaBase es un Date;
		if(duracion == 30) {
			calendar.add(Calendar.MINUTE, 30); //minutosASumar es int.
		}
		if(duracion == 1) {
			calendar.add(Calendar.HOUR,   1); //horasASumar es int.
		}
		//lo que más quieras sumar
		Date dateFechaFin = calendar.getTime();
		
		int hora1 = calendar.get(Calendar.HOUR_OF_DAY);
        int minuto1 = calendar.get(Calendar.MINUTE);
        String horaFin = hora1 + ":" + minuto1;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fechaInicioDate = df.format(dateFechaInicio);
		String fechaFinDate = df.format(dateFechaFin);

		String fechaInicioString = fechaInicioDate + "T" + turnoSolicitado.getHora();		
		String fechaFinString = fechaFinDate + "T" + horaFin;
		
		System.out.println(fechaFinString + " " + fechaFinString);

		String pattern = "yyyy-MM-dd'T'HH:mm";

		DateFormat df2 = new SimpleDateFormat(pattern);
		
		Date fechaInicio = df2.parse(fechaInicioString);
		Date fechaFin = df2.parse(fechaFinString);

		System.out.println(fechaInicio + " " + fechaFin);

		Evento evento = eventoService.guardar(null, "Turno", "TURNO", fechaInicio, fechaFin, null, null, null, turnoSolicitado, turno.getAbogado().getUsuario());
		turnoSolicitado.setEvento(evento);
		
		guardar(turnoSolicitado, null, ACTUALIZAR);
	}

	public void cancelarTurno(Turno turno, String string) {

		Abogado abogado = abogadoService.buscarAbogadoPorMailUsuario(turno.getAbogado().getUsuario().getEmail());

		String dia = turno.getFechaOriginal();
		String diaLetra = dia.substring(0, 1);

		Horario horarioTurno = horarioService.buscarHorarioSolicitadoPorAbogado(abogado.getId(), diaLetra,
				turno.getHora());
		horarioTurno.setSolicitado(false);
		horarioService.guardar(horarioTurno, null, ACTUALIZAR);

		turno.setCancelado(true);
		turno.setTokenCancelado(null);
		
		Evento evento = eventoService.buscarPorId(turno.getEvento().getId());
		evento.setActivo(false);
		eventoService.guardar(evento, abogado.getUsuario(), ACTUALIZAR);
		
		guardar(turno, null, ACTUALIZAR);

		if (string == "abogado") {
			emailSender.cancelarTurnoAbogado(turno);
		} else if (string == "cron") {
			
		} else {
			emailSender.cancelarTurnoCliente(turno);
		}
		
	}

	public void cancelarTurnosAnteriores() {	
		DateTime fechaActual = new DateTime();
		log.info("METHOD: cancelarTurnosPasados() fechaActual= " + fechaActual);
		List<Turno> turnos = buscarTodosAnteriores(fechaActual.toDate());
		for (Turno turno : turnos) {
			log.info("FECHA TURNO: " + turno.getFechaTurno());
			cancelarTurno(turno, "cron");
		}
	}
	
	public void notificarTurnosDiaAntes() {
		DateTime fechaActual = DateTime.now().withTimeAtStartOfDay();
		log.info("METHOD: notificarTurnosDiaAntes() fechaActual= " + fechaActual.toString("yyyy-MM-dd"));
		List<Turno> turnos = buscarTodosFuturos(fechaActual.toDate());
		
		for (Turno turno : turnos) {
			DateTime fechaTurno = new DateTime(turno.getFechaTurno());
			String turnoString = fechaTurno.minusDays(1).toString("yyyy-MM-dd");
			if (turnoString.equals(fechaActual.toString("yyyy-MM-dd"))) {
				emailService.mandarMailDiaAntes(turno);
				notificacionService.guardarUrl(null, "Mañana tiene un turno!", turno.getAbogado().getUsuario(),"calendario/ver",turno.getId());
			}
		}
	}

	public void validarMotivoConsulta(String clienteNombre, String clienteTelefono, String clienteMail) {

		if ((clienteNombre.isEmpty() == true) || (clienteNombre == null)) {
			throw new PeticionIncorrectaException("Debe ingresar el nombre del cliente.");
		}
			
		if ((clienteTelefono.isEmpty() == true) || (clienteTelefono == null)) {
			throw new PeticionIncorrectaException("Debe ingresar el teléfono del cliente.");
		}
		
		if (isValid(clienteMail) == false) {
			throw new PeticionIncorrectaException("El formato de el mail ingresado no es el correcto.");
		}
	}

	public void validarTurno(Abogado abogado, String hora, String clienteMail) {
		if (isValid(clienteMail) == false) {
			throw new PeticionIncorrectaException("El formato de el mail ingresado no es el correcto.");
		}

		if (abogado == null) {
			throw new PeticionIncorrectaException("Hubo un error en la base de datos :(");
		}

		if (isStringNull(hora)) {
			throw new PeticionIncorrectaException("Debe seleccionar la fecha de el turno!");
		}
	}

	public String crearFechaAmigable(DateTime fechaOg) {
		
		String dia = null;
		int numeroDia = fechaOg.getDayOfMonth();
		String mes = null;
		
		switch (fechaOg.getDayOfWeek()) {
		case 1:
			dia = "Lunes";
			break;
		case 2:
			dia = "Martes";
			break;
		case 3:
			dia = "Miercoles";
			break;
		case 4:
			dia = "Jueves";
			break;
		case 5:
			dia = "Viernes";
			break;
		case 6:
			dia = "Sabado";
			break;
		case 7:
			dia = "Domingo";
			break;
		default:
			break;
		}
		
		switch (fechaOg.getMonthOfYear()) {
		case 1:
			mes = "Enero";
			break;
		case 2:
			mes = "Febrero";
			break;
		case 3:
			mes = "Marzo";
			break;
		case 4:
			mes = "Abril";
			break;
		case 5:
			mes = "Mayo";
			break;
		case 6:
			mes = "Junio";
			break;
		case 7:
			mes = "Julio";
			break;
		case 8:
			mes = "Agosto";
			break;
		case 9:
			mes = "Septiembre";
			break;
		case 10:
			mes = "Octubre";
			break;
		case 11:
			mes = "Noviembre";
			break;
		case 12:
			mes = "Diciembre";
			break;
		default:
			break;
		}
		
		return "el " + dia + " " + numeroDia + " de " + mes;
	}
	
	public String crearFechaHorario(DateTime fechaOg) {
		String dia = null;
		switch (fechaOg.getDayOfWeek()) {
		case 1:
			dia = "L";
			break;
		case 2:
			dia = "M";
			break;
		case 3:
			dia = "X";
			break;
		case 4:
			dia = "J";
			break;
		case 5:
			dia = "V";
			break;
		case 6:
			dia = "S";
			break;
		case 7:
			dia = "D";
			break;
		default:
			break;
		}
		DecimalFormat df = new DecimalFormat("00");
		String hora = df.format(fechaOg.getHourOfDay()).toString();
		String minutos = df.format(fechaOg.getMinuteOfHour()).toString();
		return dia + hora + minutos;
	}
	
	@Transactional
	private Turno guardar(Turno turno, Usuario interviniente, String accion) {
		auditar(turno, interviniente, accion);
		return turnoRepository.save(turno);
	}

	public void validarConsulta(String idLocalidad, String idTipoCausa) {
		if (isStringNull(idLocalidad)) {
			throw new PeticionIncorrectaException("Complete todos los campos del formulario para poder avanzar!");
		}
		if (isStringNull(idTipoCausa)) {
			throw new PeticionIncorrectaException("Complete todos los campos del formulario para poder avanzar!");
		}
	}
	
	public static boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	public Turno buscarPorId(String id) {
		return turnoRepository.buscarPorId(id);
	}

	public Turno buscarPorAbogadoId(String id) {
		return turnoRepository.buscarPorAbogadoId(id);
	}

	public Turno buscarPorAbogadoNombre(String nombre) {
		return turnoRepository.buscarPorAbogadoNombre(nombre);
	}
	
	public void save(Turno nombre) {
		turnoRepository.save(nombre);
	}

	public Turno buscarTurno(String mailAbogado, String mailCliente, String original) {
		List<Turno> turnos = turnoRepository.buscarTurno(mailAbogado, mailCliente, original);
		if(turnos != null && !turnos.isEmpty()) {
			return turnos.get(0);
		}
		return null;
	}
	
	public Turno buscarTurnoPorClienteYFecha(String mailCliente, String original) {
		List<Turno> turnos = turnoRepository.buscarTurnoPorClienteYFecha(mailCliente, original);
		if(turnos != null && !turnos.isEmpty()) {
			return turnos.get(0);
		}
		return null;
	}

	public List<Turno> buscarTodos() {
		return turnoRepository.buscarTodos();
	}
	
	public List<Turno> buscarTodosAnteriores(Date fechaActual){
		return turnoRepository.buscarTodosAnteriores(fechaActual);
	}
	
	public List<Turno> buscarTodosFuturos(Date fechaActual){
		return turnoRepository.buscarTodosFuturos(fechaActual);
	}

	public Turno buscarPorTokenCancelado(String tokenCancelado) {
		return turnoRepository.buscarPorTokenCancelado(tokenCancelado);
	}
	
	public Turno buscarTurnoPorEventoId(Evento evento) {
		return turnoRepository.buscarTurnoPorEventoId(evento);
	}

//	public Turno buscarPorTokenAbogado(String tokenAbogado) {
//		return turnoRepository.buscarPorTokenAbogado(tokenAbogado);
//	}
//
//	public Turno buscarPorTokenCliente(String tokenCliente) {
//		return turnoRepository.buscarPorTokenCliente(tokenCliente);
//	}
//
//	public Turno buscarPorJuzgadoId(String id) {
//		return turnoRepository.buscarPorJuzgadoId(id);
//	}
//
//	public Turno buscarPorJuzgadoNombre(String nombre) {
//		return turnoRepository.buscarPorJuzgadoNombre(nombre);
//	}
//
//	public Turno buscarPorUsuarioId(String id) {
//		return turnoRepository.buscarPorUsuarioId(id);
//	}
//
//	public Turno buscarPorUsuarioNombre(String nombre) {
//		return turnoRepository.buscarPorUsuarioNombre(nombre);
//	}
//	public MTurno enviarMailConfirmacionCliente(Turno turno) {
//	
//	Turno eTurno = buscarPorId(turno.getId());
//	eTurno.setTokenAbogado(null);
//	eTurno.setTokenCliente(UUID.randomUUID().toString());
//	guardar(eTurno, null, ACTUALIZAR);
//
//	emailSender.confirmarTurnoCliente(eTurno);
//	
//	return turnoConverter.entidadModelo(eTurno);
//}
//
//public MTurno confirmarTurnoFinal(Turno turno) {
//	
//	Turno eTurno = buscarPorId(turno.getId());
//	eTurno.setTokenCliente(null);
//	eTurno.setConfirmado(true);
//	guardar(eTurno, null, ACTUALIZAR);
//	
//	return turnoConverter.entidadModelo(eTurno);
//	
//}
	
	@Transactional
	public Turno eliminar(String idTurno) {
		Turno turno = buscarPorId(idTurno);
		turnoRepository.delete(turno);
		return turno;
	}

}
