package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.DiaNoLaboralConverter;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.DiaNoLaboral;
import com.quinto.liberium.entity.Evento;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoDiaNoLaborable;
import com.quinto.liberium.enumeration.TipoPlazo;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MDiaNoLaboral;
import com.quinto.liberium.repository.DiaNoLaboralRepository;

@Service("diaNoLaboralService")
public class DiaNoLaboralService extends SimpleService {

	@Autowired
	private DiaNoLaboralRepository diaNoLaboralRepository;

	@Autowired
	private DiaNoLaboralConverter diaNoLavoralConverter;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private EventoService eventoService;
	
	@Autowired
	private UsuarioService usuarioService;

	public MDiaNoLaboral crear(String idDiaNoLaboral, String titulo, String tipoEvento, Date fechaInicio, Date fechaFin,
			String idPais, String idProvincia, TipoDiaNoLaborable tipoDia, Usuario usuario) {

		if (tipoDia == null) {
			throw new PeticionIncorrectaException("El tipo de el dia no puede estar vacio!");
		}
		if (tipoDia.equals(TipoDiaNoLaborable.FEDERAL)) {
			validarDiaFederal(titulo, tipoEvento, fechaInicio, fechaFin, idPais);
		} else {
			validarDiaProvincial(titulo, tipoEvento, fechaInicio, fechaFin, idPais, idProvincia);
		}

		DiaNoLaboral diaNoLaboral = new DiaNoLaboral();

		if (!isStringNull(idDiaNoLaboral)) {
			diaNoLaboral = buscarEntidadDiaNoLaboralPorId(idDiaNoLaboral);
		}

		if (tipoDia.equals(TipoDiaNoLaborable.FEDERAL)) {
			Pais pais = paisService.buscarPaisPorId(idPais);
			diaNoLaboral.setPais(pais);
			diaNoLaboral.setProvincia(null);
		} else {
			Provincia provincia = provinciaService.buscarProvinciaPorId(idProvincia);
			Pais pais = paisService.buscarPaisPorId(idPais);
			diaNoLaboral.setPais(pais);
			diaNoLaboral.setProvincia(provincia);
		}

		diaNoLaboral.setTitulo(titulo);
		diaNoLaboral.setTipoEvento(tipoEvento);
		diaNoLaboral.setFechaInicio(fechaInicio);
		diaNoLaboral.setFechaFin(fechaFin);
		diaNoLaboral.setTipoDiaNoLaborable(tipoDia);
		
		setDiasNoLaborablesUsuarios(diaNoLaboral);

		return diaNoLavoralConverter.entidadModelo(guardar(diaNoLaboral, usuario, ACTUALIZAR));
	}

	public void setDiasNoLaborablesUsuarios(DiaNoLaboral diaNoLaboral) {
		
		List<Usuario> usuarios = null;
		
		Evento evento = new Evento();
		
		if (diaNoLaboral.getTipoDiaNoLaborable().equals(TipoDiaNoLaborable.FEDERAL)) {
			usuarios = usuarioService.buscarUsuariosPorPais(diaNoLaboral.getPais());
			for (Usuario usuario : usuarios) {
				
				evento.setCreador(usuario);
				evento.setPais(usuario.getPais());
				evento.setTitulo(diaNoLaboral.getTitulo());
				evento.setTipoEvento(diaNoLaboral.getTipoEvento());
				evento.setFechaInicio(diaNoLaboral.getFechaInicio());
				evento.setFechaFin(diaNoLaboral.getFechaFin());
				evento.setTipoDiaNoLaborable(TipoDiaNoLaborable.FEDERAL);
				evento.setTipoPlazo(TipoPlazo.CORRIDO);
				evento.setActivo(true);
				evento.setDiaNoLaboral(true);
				eventoService.setColor(evento);
				eventoService.guardar(evento, usuario, ACTUALIZAR);
			}
		} else {
			usuarios = usuarioService.buscarUsuariosPorProvincia(diaNoLaboral.getProvincia());
			for (Usuario usuario : usuarios) {

				evento.setCreador(usuario);
				evento.setPais(usuario.getPais());
				evento.setTitulo(diaNoLaboral.getTitulo());
				evento.setTipoEvento(diaNoLaboral.getTipoEvento());
				evento.setFechaInicio(diaNoLaboral.getFechaInicio());
				evento.setFechaFin(diaNoLaboral.getFechaFin());
				evento.setTipoDiaNoLaborable(TipoDiaNoLaborable.PROVINCIAL);
				evento.setTipoPlazo(TipoPlazo.CORRIDO);
				evento.setActivo(true);
				evento.setDiaNoLaboral(true);
				eventoService.setColor(evento);	
				eventoService.guardar(evento, usuario, ACTUALIZAR);
			}
		}
		
	}

	public void setDiasNoLaboralesFederales(Usuario usuario) {

		List<MDiaNoLaboral> diasNoLaborables = buscarDiaNoLaboralPorPais(usuario.getPais());
		
		for (MDiaNoLaboral mDiaNoLaboral : diasNoLaborables) {
			System.out.println(mDiaNoLaboral.toString());
		}

		List<Evento> diasPredefinidos = new ArrayList<>();

		for (int i = 0; i < diasNoLaborables.size(); i++) {
			Evento evento = new Evento();

			evento.setCreador(usuario);
			evento.setPais(usuario.getPais());
			evento.setTitulo(diasNoLaborables.get(i).getTitulo());
			evento.setTipoEvento(diasNoLaborables.get(i).getTipoEvento());
			evento.setFechaInicio(diasNoLaborables.get(i).getFechaInicio());
			evento.setFechaFin(diasNoLaborables.get(i).getFechaFin());
			evento.setTipoPlazo(TipoPlazo.CORRIDO);
			evento.setActivo(true);
			evento.setDiaNoLaboral(true);
			eventoService.setColor(evento);

			diasPredefinidos.add(evento);
		}

		for (Evento evento : diasPredefinidos) {
			System.out.println(evento.toString());
		}
		
		for (Evento evento : diasPredefinidos) {
			eventoService.guardar(evento, usuario, ACTUALIZAR);
		}

	}

	public void setDiasNoLaboralesProvinciales(Usuario usuario) {

		List<MDiaNoLaboral> diasNoLaborables = buscarDiaNoLaboralPorProvincia(usuario.getProvincia());

		for (MDiaNoLaboral mDiaNoLaboral : diasNoLaborables) {
			System.out.println(mDiaNoLaboral.toString());
		}

		List<Evento> diasPredefinidos = new ArrayList<>();

		for (int i = 0; i < diasNoLaborables.size(); i++) {
			Evento evento = new Evento();

			evento.setCreador(usuario);
			evento.setProvincia(usuario.getProvincia());
			evento.setTitulo(diasNoLaborables.get(i).getTitulo());
			evento.setTipoEvento(diasNoLaborables.get(i).getTipoEvento());
			evento.setFechaInicio(diasNoLaborables.get(i).getFechaInicio());
			evento.setFechaFin(diasNoLaborables.get(i).getFechaFin());
			evento.setTipoPlazo(TipoPlazo.CORRIDO);
			evento.setActivo(true);
			evento.setDiaNoLaboral(true);
			eventoService.setColor(evento);

			diasPredefinidos.add(evento);
		}
		for (Evento evento : diasPredefinidos) {
			System.out.println(evento.toString());
		}

		for (Evento eventoProvincial : diasPredefinidos) {
			eventoService.guardar(eventoProvincial, usuario, ACTUALIZAR);
		}

	}

	public MDiaNoLaboral eliminar(String idDiaNoLaboral, Usuario usuario) {
		DiaNoLaboral diaNoLaboral = buscarEntidadDiaNoLaboralPorId(idDiaNoLaboral);
		if (diaNoLaboral == null) {
			throw new PeticionIncorrectaException("Ocurrio un error al eliminar el dia no laborable!");
		}
		diaNoLaboral = guardar(diaNoLaboral, usuario, ELIMINAR);
		return diaNoLavoralConverter.entidadModelo(diaNoLaboral);
	}

	@Transactional
	private DiaNoLaboral guardar(DiaNoLaboral diaNoLaboral, Usuario usuario, String accion) {
		auditar(diaNoLaboral, usuario, accion);
		return diaNoLaboralRepository.save(diaNoLaboral);
	}

	private void validarExistencia(DiaNoLaboral diaNoLaboral) {
		List<MDiaNoLaboral> diaNoLaborables = buscarDiaNoLaborales();
		for (MDiaNoLaboral mDiaNoLaboral : diaNoLaborables) {
			if (diaNoLaboral.getTipoDiaNoLaborable().equals(TipoDiaNoLaborable.FEDERAL)) {
				if (diaNoLaboral.getTitulo().equals(mDiaNoLaboral.getTitulo()) && diaNoLaboral.getPais().getId().equals(mDiaNoLaboral.getmPais().getId())) {
					throw new PeticionIncorrectaException("Ya existe un dia no laboral con ese nombre en ese pais");
				} else if (diaNoLaboral.getPais().getId().equals(mDiaNoLaboral.getmPais().getId()) && diaNoLaboral.getFechaInicio().equals(mDiaNoLaboral.getFechaInicio())) {
					throw new PeticionIncorrectaException("Ya existe un dia no laborable en esa fecha en el pais seleccionado");
				}
			}
			if (diaNoLaboral.getTipoDiaNoLaborable().equals(TipoDiaNoLaborable.PROVINCIAL)) {
				if (diaNoLaboral.getTitulo().equals(mDiaNoLaboral.getTitulo()) && diaNoLaboral.getProvincia().getId().equals(mDiaNoLaboral.getmProvincia().getId())) {
					throw new PeticionIncorrectaException("Ya existe un dia no laboral con ese nombre en esa provincia");
				} else if (diaNoLaboral.getProvincia().getId().equals(mDiaNoLaboral.getmProvincia().getId()) && diaNoLaboral.getFechaInicio().equals(mDiaNoLaboral.getFechaInicio())) {
					throw new PeticionIncorrectaException("Ya existe un dia no laborable en esa fecha en la provincia seleccionada");
				}
			}
		}
	}
	
	private void validarDiaFederal(String titulo, String tipoEvento, Date fechaInicio, Date fechaFin, String idPais) {

		if (isStringNull(titulo)) {
			throw new PeticionIncorrectaException("El titulo de el dia no laborable no puede estar vacio!");
		}

		if (isStringNull(tipoEvento)) {
			throw new PeticionIncorrectaException("El tipo de dia no laborable no puede estar vacio!");
		}

		if (fechaInicio == null) {
			throw new PeticionIncorrectaException("La fecha de inicio de el dia no laborable no puede estar vacia!");
		}

		if (fechaFin == null) {
			throw new PeticionIncorrectaException("La fecha de fin de el dia no laborable no puede estar vacia!");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El pais de el dia no laborable no puede estar vacio!");
		}
	}

	private void validarDiaProvincial(String titulo, String tipoEvento, Date fechaInicio, Date fechaFin, String idPais,
			String idProvincia) {

		if (isStringNull(titulo)) {
			throw new PeticionIncorrectaException("El titulo de el dia no laborable no puede estar vacio!");
		}

		if (isStringNull(tipoEvento)) {
			throw new PeticionIncorrectaException("El tipo de dia no laborable no puede estar vacio!");
		}

		if (fechaInicio == null) {
			throw new PeticionIncorrectaException("La fecha de inicio de el dia no laborable no puede estar vacia!");
		}

		if (fechaFin == null) {
			throw new PeticionIncorrectaException("La fecha de fin de el dia no laborable no puede estar vacia!");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El pais de el dia no laborable no puede estar vacio!");
		}

		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException("La provincia de el dia no laborable no puede estar vacia!");
		}

	}

	public Page<DiaNoLaboral> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return diaNoLaboralRepository.buscarTodos(paginable);
		} else {
			 q = "%"+q+"%";
			 List<DiaNoLaboral> diasNL = new ArrayList<>();
			 List<DiaNoLaboral> diaNLlist = diaNoLaboralRepository.buscarTodos(q);
			 if(diaNLlist != null && !diaNLlist.isEmpty()) {
				 diasNL.addAll(diaNLlist);
			 }
			 List<DiaNoLaboral> diaNLProvincia = diaNoLaboralRepository.buscarTodosPorProvincia(q);
			 if(diaNLProvincia != null && !diaNLProvincia.isEmpty()) {
				 for(DiaNoLaboral c : diaNLProvincia) {
					 if(!diasNL.contains(c)) {
						 diasNL.add(c);
					 }
				 }
			 }
		     final int start = (int) paginable.getOffset();
		     final int end = Math.min(start + paginable.getPageSize(), diasNL.size());
		     
		     
		     Page<DiaNoLaboral> diasNLPage = new PageImpl<>(diasNL.subList(start, end), paginable, diasNL.size());
			return diasNLPage;
		}
		
	}

	public MDiaNoLaboral buscarModeloDiaNoLaboralPorId(String idDiaNoLaboral) {
		return diaNoLavoralConverter.entidadModelo(diaNoLaboralRepository.buscarPorId(idDiaNoLaboral));
	}

	public DiaNoLaboral buscarEntidadDiaNoLaboralPorId(String idDiaNoLaboral) {
		return diaNoLaboralRepository.buscarPorId(idDiaNoLaboral);
	}

	public List<MDiaNoLaboral> buscarDiaNoLaborales() {
		return diaNoLavoralConverter.entidadesModelos(diaNoLaboralRepository.buscarTodos());
	}

	public List<MDiaNoLaboral> buscarDiaNoLaboralPorProvincia(Provincia provincia) {
		return diaNoLavoralConverter
				.entidadesModelos(diaNoLaboralRepository.buscarDiasNoLaboralesPorProvincia(provincia));
	}

	public List<MDiaNoLaboral> buscarDiaNoLaboralPorPais(Pais pais) {
		return diaNoLavoralConverter.entidadesModelos(diaNoLaboralRepository.buscarDiasNoLaboralesPorPais(pais));
	}

}
