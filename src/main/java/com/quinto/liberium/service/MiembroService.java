package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.MiembroConverter;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Miembro;
import com.quinto.liberium.entity.Rol;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MCausa;
import com.quinto.liberium.model.MMiembro;
import com.quinto.liberium.model.MPermiso;
import com.quinto.liberium.repository.MiembroRepository;

@Service("miembroService")
public class MiembroService extends SimpleService {

	private static final String MENSAJE_ERROR = "ERROR - El valor nombre no puede estar vacío.";
	private static final String ACCION_ELIMINAR = "eliminar";
	private static final String ACCION_AGREGAR = "agregar";
	private static final String ACCION_MODIFICAR = "modificar";

	@Autowired
	private MiembroConverter miembroConverter;

	@Autowired
	private MiembroRepository miembroRepository;

	@Autowired
	private RolService rolService;

	@Autowired
	private EstudioService estudioService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private CausaService causaService;

	public Miembro crearMiembro(String id, Usuario usuario, Rol rol, Estudio estudio, Usuario creador) {
		Miembro miembro = null;

		if (!isStringNull(id)) {
			miembro = buscarPorId(id);
		} else {
			miembro = new Miembro();
		}

		miembro.setEstudio(estudio);
		miembro.setUsuario(usuario);
		miembro.setRol(rol);

		emailService.mandarMailDeNuevoMiembro(miembro);

		return guardar(miembro, creador, ACCION_AGREGAR);
	}

	public Miembro crearMiembroInvitar(Usuario usuario, Rol rol, Estudio estudio, Usuario creador) {
		Miembro miembro = new Miembro();

		miembro.setEstudio(estudio);
		miembro.setUsuario(usuario);
		miembro.setRol(rol);

		emailService.mandarMailDeNuevoMiembro(miembro);

		return guardar(miembro, creador, ACCION_AGREGAR);
	}

	public List<MMiembro> miembrosCombo(String idEstudio, String idCausa, Usuario usuario) {
		List<MMiembro> miembros = buscarEMiembrosPoridEstudio(idEstudio);
		List<MMiembro> miembros2 = new ArrayList<>();
		Set<Miembro> miembros3 = causaService.buscarPorId(idCausa).getMiembrosCausa();
		for (MMiembro mMiembro : miembros) {
			boolean permisosCausa = false;
			boolean perteneceCausa = false;
			if (mMiembro.getmUsuario().getId() == usuario.getId()) {
				continue;
			}
			for (Miembro miembrosCausa : miembros3) {
				if (mMiembro.getId().equals(miembrosCausa.getId())) {
					perteneceCausa = true;
					break;
				}
			}
			List<MPermiso> permisosMiembro = mMiembro.getmRol().getPermiso();
			if (permisosMiembro != null) {
				for (int j = 0; j < permisosMiembro.size(); j++) {
					if (permisosMiembro.get(j).getNombre().equals("VISUALIZAR_CAUSA")
							|| permisosMiembro.get(j).getNombre().equals("EDITAR_CAUSA")) {
						permisosCausa = true;
						break;
					}
				}
			}
			if (permisosCausa && !perteneceCausa) {
				miembros2.add(mMiembro);
			}
		}
		return miembros2;
	}

	public MCausa guardarMiembrosCausa(String idCausa, String idEstudio, List<String> idMiembros, Usuario creador,
			String nuevo) {
		MCausa causa = causaService.buscarCausaModelPorId(idCausa);
		Estudio estudio = estudioService.buscarPorIdEstudio(idEstudio);
		Set<MMiembro> miembrosCausa = causa.getmMiembro();
		for (String idMiembro : idMiembros) {
			MMiembro miembro = new MMiembro();
			if (nuevo.equals("true")) {
				System.out.println("Nuevo SI: " + nuevo);
				miembro = buscarMMiembroPorId(idMiembro);
			} else {
				System.out.println("Nuevo NO: " + nuevo);
				miembro = buscarMMiembroPorId(idMiembro);
				//miembro = buscarMMiembroPorIdUsuario(idMiembro);
			}
			if (miembro.getmRol().getNombre() != null) {
				if (miembro.getmRol().getNombre().equals("CLIENTE")) {
					emailService.mandarMailDeNuevoCliente(miembro, causa);
				}
			}
			miembrosCausa.add(miembro);
		}
		causa.setmMiembro(miembrosCausa);
		return causaService.guardar(causa, creador, estudio);
	}

	public MCausa eliminarMiembroCausa(String idCausa, String idMiembro, Usuario eliminador) {
		MCausa causa = causaService.buscarCausaModelPorId(idCausa);
		Set<MMiembro> mMiembros = causa.getmMiembro();
		for (MMiembro mMiembro : mMiembros) {
			if (mMiembro.getId().equals(idMiembro)) {
				mMiembros.remove(mMiembro);
				break;
			}
		}
		causa.setmMiembro(mMiembros);
		return causaService.guardar(causa, eliminador, estudioService.buscarPorIdEstudio(causa.getmEstudio().getId()));
	}

	public Miembro editar(MMiembro mMiembro, Usuario editor) {
		if (mMiembro == null || editor == null) {
			throw new PeticionIncorrectaException(MENSAJE_ERROR);
		}
		Miembro miembro = miembroConverter.modeloEntidad(mMiembro);
		return guardar(miembro, editor, ACCION_MODIFICAR);
	}

	public void eliminar(String miembroId, Usuario eliminador) {
		if (miembroId.isEmpty() || eliminador == null) {
			throw new PeticionIncorrectaException(MENSAJE_ERROR);
		}
		Miembro miembro = miembroRepository.buscarPorId(miembroId);
		guardar(miembro, eliminador, ACCION_ELIMINAR);
	}

	private Miembro guardar(Miembro miembro, Usuario interviniente, String accion) {
		auditar(miembro, interviniente, accion);
		return miembroRepository.save(miembro);
	}

	public List<Miembro> listarMiembro() {
		return miembroRepository.findAll();
	}

	public Miembro buscarPorId(String miembroId) {
		return miembroRepository.buscarPorId(miembroId);
	}

	public MMiembro buscarMMiembroPorId(String idMiembro) {
		return miembroConverter.entidadModelo(miembroRepository.buscarPorId(idMiembro));
	}

	public List<Miembro> buscarPorIdUsuario(String idUsuario) {
		return miembroRepository.buscarMiembrosPorIdUsuario(idUsuario);
	}

	public MMiembro buscarMMiembroPorIdUsuario(String idUsuario) {
		return miembroConverter.entidadModelo(miembroRepository.buscarPorIdUsuario(idUsuario));
	}

	public List<Estudio> buscarEstudioPorIdUsuario(String idUsuario) {
		return miembroRepository.buscarEstudioPorIdUsuario(idUsuario);
	}

	public Miembro buscarPorIdEstudio(String estudioId) {
		return miembroRepository.buscarPorIdEstudio(estudioId);
	}

	public Miembro buscarPorIdEstudioYIdUsuario(String idEstudio, String idUsuario) {
		return miembroRepository.buscarPorIdEstudioYIdUsuario(idEstudio, idUsuario);
	}

	public Miembro buscarPorIdRol(String rolId) {
		return miembroRepository.buscarPorIdRol(rolId);
	}

	public List<MMiembro> buscarMiembrosPoridEstudio(String idEstudio) {
		return miembroRepository.buscarMiembrosPoridEstudio(idEstudio);
	}

	public List<MMiembro> buscarEMiembrosPoridEstudio(String idEstudio) {
		return miembroConverter.entidadesModelos(miembroRepository.buscarEMiembrosPoridEstudio(idEstudio));
	}

	public List<MMiembro> buscarMiembrosPorEstudio(String idEstudio, String idUsuario) {
		List<Miembro> miembros = miembroRepository.buscarMiembrosPorIdEstudio(idEstudio, idUsuario);
		return miembroConverter.entidadesModelos(miembros);
	}

	public Page<MMiembro> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return miembroRepository.buscarTodosMMiembro(paginable);
		} else {
			return miembroRepository.buscarTodosMMiembro(paginable, "%" + q + "%");
		}
	}

	public Miembro generarMiembroAbogado(String idEstudio, Usuario usuario) {
		Miembro miembro = buscarPorIdEstudioYIdUsuario(idEstudio, usuario.getId());
		if (miembro == null) {
			miembro = new Miembro();
			Estudio estudio = estudioService.buscarPorIdEstudio(idEstudio);
			miembro.setEstudio(estudio);
			miembro.setUsuario(usuario);

			Rol rol = rolService.buscarPorNombre("ABOGADO/ASOCIADO");
			miembro.setRol(rol);

			return guardar(miembro, usuario, ACCION_AGREGAR);
		}

		return miembro;

	}

	public Page<MMiembro> buscarTodosPorEstudio(Pageable paginable, String q, Estudio estudio) {
		if (q == null || q.isEmpty()) {
			return miembroRepository.buscarTodosPorEstudioMMiembro(paginable, estudio.getId());
		} else {
			return miembroRepository.buscarTodosPorEstudioMMiembro(paginable, "%" + q + "%", estudio.getId());
		}
	}

	public void eliminarMiembro(String id, Estudio estudio, Usuario usuario) {
		Miembro miembro = buscarPorId(id);
		if (miembro.getEstudio().getId().equals(estudio.getId())) {
			auditar(miembro, usuario, ACCION_ELIMINAR);
			miembroRepository.save(miembro);
		}
	}

}