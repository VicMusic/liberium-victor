package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quinto.liberium.converter.EstudioConverter;
import com.quinto.liberium.entity.Abogado;
import com.quinto.liberium.entity.Estudio;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MEstudio;
import com.quinto.liberium.repository.AbogadoRepository;
import com.quinto.liberium.repository.EstudioRepository;

@Service("estudioService")
public class EstudioService extends SimpleService {

	@Autowired
	private EstudioConverter estudioConverter;

	@Autowired
	private EstudioRepository estudioRepository;

	@Autowired
	private AbogadoRepository abogadoRepository;

	public MEstudio guardar(String id, String nombre, Usuario usuario) {
		Estudio estudio = new Estudio();

		if (!isStringNull(id)) {
			estudio = buscarPorIdEstudio(id);
			validarEstudio(nombre, estudio, usuario);
		} else {
			validarEstudio(nombre, null, usuario);
		}

		estudio.setNombre(nombre);

		List<Abogado> abogados = new ArrayList<>();

		abogados.add(abogadoRepository.buscarPorMail(usuario.getEmail()));

		estudio.setAbogados(abogados);

		return estudioConverter.entidadModelo(guardar(estudio, usuario, SimpleService.ACTUALIZAR));
	}

	@Transactional
	private Estudio guardar(Estudio estudio, Usuario usuario, String accion) {
		auditar(estudio, usuario, accion);
		return estudioRepository.save(estudio);
	}

	public MEstudio eliminar(String id, Usuario usuario) {
		Estudio estudio = buscarPorIdEstudio(id);

		if (estudio == null) {
			throw new EventoRestException("El estudio que trata eliminar no se encuentra en la base de datos.");
		}
		estudio = guardar(estudio, usuario, SimpleService.ELIMINAR);
		return estudioConverter.entidadModelo(estudio);
	}

	private void validarEstudio(String nombre, Estudio estudio, Usuario usuario) {

		if (nombre.length() > 50) {
			throw new PeticionIncorrectaException("El nombre del estudio supera la cantidad maxima de caracteres permitidos.");
		}
		
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del estudio no puede estar vacio.");
		}
		
		Estudio otro = buscarPorNombre(nombre);
		if ((estudio == null && otro != null)
				|| (estudio != null && otro != null && !estudio.getId().equals(otro.getId()))) {
			throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
		}
	}

	public List<Estudio> listarEstudio() {
		return estudioRepository.findAll();
	}

	public Estudio buscarPorIdEstudio(String idEstudio) {
		return estudioRepository.buscarPorId(idEstudio);
	}

	public Estudio buscarPorNombre(String nombre) {
		return estudioRepository.buscarPorNombre(nombre);
	}

	public List<Estudio> buscarPorCreador(Usuario usuario) {
		return estudioRepository.buscarPorCreador(usuario);
	}
	
	public Estudio buscarPorId(String idEstudio) {
		return estudioRepository.buscarPorId(idEstudio);
	}

	public List<Estudio> buscarPorPertenencia(String abogado) {
		return estudioRepository.buscarPorPertenencia(abogado);
	}
}