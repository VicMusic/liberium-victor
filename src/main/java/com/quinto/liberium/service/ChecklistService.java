package com.quinto.liberium.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.quinto.liberium.converter.ChecklistConverter;
import com.quinto.liberium.entity.Checklist;
import com.quinto.liberium.entity.Circunscripcion;
import com.quinto.liberium.entity.Formulario;
import com.quinto.liberium.entity.Instancia;
import com.quinto.liberium.entity.Item;
import com.quinto.liberium.entity.Pais;
import com.quinto.liberium.entity.Provincia;
import com.quinto.liberium.entity.TipoCausa;
import com.quinto.liberium.entity.Usuario;
import com.quinto.liberium.enumeration.TipoFuero;
import com.quinto.liberium.enumeration.TipoRolCausa;
import com.quinto.liberium.enumeration.TipoUsuario;
import com.quinto.liberium.exception.EventoRestException;
import com.quinto.liberium.exception.PeticionIncorrectaException;
import com.quinto.liberium.model.MChecklist;
import com.quinto.liberium.repository.ChecklistRepository;
import com.quinto.liberium.util.Configuracion;

@Service
public class ChecklistService extends SimpleService {

	@Autowired
	private ChecklistRepository checklistRepository;

	@Autowired
	private ChecklistConverter checklistConverter;

	@Autowired
	private PaisService paisService;

	@Autowired
	private ProvinciaService provinciaService;

	@Autowired
	private TipoCausaService tipoCausaService;

	@Autowired
	private CircunscripcionService circunscripcionService;

	@Autowired
	private InstanciaService instanciaService;

	@Autowired
	private FormularioService formularioService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private NotificacionService notificacionService;
	
	@Autowired
	private Configuracion configuracion;
	

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { PeticionIncorrectaException.class, Exception.class })
	public Checklist guardar(String id, String nombre, String tipoChecklist, String idPais, String tipoFuero,
			String idMateria, String idTipoCausa, String idProvincia, String idCircunscripcion, String idInstancia,
			String idFormulario, Usuario usuario) {
		Checklist checklist = new Checklist();
		
		if (tipoFuero.length() == 0) {
			throw new PeticionIncorrectaException("Tiene que ingresar un tipo de fuero para poder guardar");
		}

		TipoFuero tipoFueroEnum = TipoFuero.valueOf(tipoFuero);

		if (!validar(nombre, tipoChecklist, idPais, idTipoCausa, idFormulario, tipoFueroEnum, idProvincia))

			if (!isStringNull(id)) {
				checklist = checklistRepository.buscarPorId(id);
			}

		checklist.setNombre(nombre);

		TipoCausa tipoCausa = tipoCausaService.buscarPorId(idTipoCausa);
		checklist.setTipoCausa(tipoCausa);
		checklist.setMateria(tipoCausa.getMateria());

		Formulario formulario = formularioService.buscarFormularioPorId(idFormulario);
		checklist.setFormulario(formulario);

		Pais pais = paisService.buscarPaisPorId(idPais);
		checklist.setPais(pais);

		TipoRolCausa tipoRolCausa = TipoRolCausa.valueOf(tipoChecklist);
		checklist.setRol(tipoRolCausa);

		if (tipoFueroEnum.equals(TipoFuero.PROVINCIAL)) {
			Provincia provincia = provinciaService.buscarProvinciaPorId(idProvincia);
			checklist.setProvincia(provincia);

			Circunscripcion circunscripcion = circunscripcionService.buscarCircunscripcionPorId(idCircunscripcion);
			checklist.setCircunscripcion(circunscripcion);

			Instancia instancia = instanciaService.buscarInstanciaPorId(idInstancia);
			checklist.setInstancia(instancia);

		}
		checklist.setTipoFuero(TipoFuero.valueOf(tipoFuero));
		if (isStringNull(id) && !usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			notificacionService.notificarCheckListUrl("Se ha creado el checklist: " + checklist.getNombre(),"administracion/checklist/listado", checklist.getId());
		} else if (!isStringNull(id) && !usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			notificacionService.notificarCheckListUrl("Se ha modificado el checklist: " + checklist.getNombre(),"administracion/checklist/listado", checklist.getId());
		}
		if(usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)){
			checklist.setVisibilidad("Publico");
		}else {
			checklist.setVisibilidad("Privado");
		}
		checklist.setPlantilla(true);
		return guardar(checklist, usuario, ACTUALIZAR);
	}

	private boolean validar(String nombre, String tipoChecklist, String idPais, String idTipoCausa,
			String idFormulario, TipoFuero tipoFuero, String idProvincia) {
		
		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del checklist no puede estar vacio.");
		}

		if (isStringNull(tipoChecklist)) {
			throw new PeticionIncorrectaException("El rol del checklist no ha sido seleccionado.");
		}

		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El país del checklist no puede estar vacio.");
		}

		if (isStringNull(idTipoCausa)) {
			throw new PeticionIncorrectaException("El tipo de causa no puede estar vacia.");
		}

		if (isStringNull(idFormulario)) {
			throw new PeticionIncorrectaException("El formulario de checklist no puede estar vacio.");
		}

		if (tipoFuero.equals(TipoFuero.PROVINCIAL)) {
			if (isStringNull(idProvincia)) {
				throw new PeticionIncorrectaException("La provincia del checklist no puede estar vacia.");
			}

//			if(isStringNull(idCircunscripcion)) {
//				throw new PeticionIncorrectaException("La circunscripción del checklist no puede estar vacio.");
//			}
//			
//			if(isStringNull(idInstancia)) {
//				throw new PeticionIncorrectaException("La instancía del checklist no puede estar vacia.");
//			}
		}

		return false;
	}

	public Checklist clonar(String idCheckList, Usuario creador) {
		Checklist checklist = buscarCheckListPorId(idCheckList);
		Checklist checklistClonado = new Checklist();
		BeanUtils.copyProperties(checklist, checklistClonado);
		checklistClonado.setId(null);
		checklistClonado.setCreado(new Date());
		checklistClonado.setCreador(null);
		checklistClonado.setNombre(checklist.getNombre());

		if (checklist.getPais() != null) {
			checklistClonado.getPais().setId(checklist.getPais().getId());
			checklistClonado.getPais().setNombre(checklist.getPais().getNombre());
		}

		if (checklist.getProvincia() != null) {
			checklistClonado.getProvincia().setId(checklist.getProvincia().getId());
			checklistClonado.getProvincia().setNombre(checklist.getProvincia().getNombre());
		}

		if (checklist.getTipoCausa() != null) {
			checklistClonado.getTipoCausa().setId(checklist.getTipoCausa().getId());
			checklistClonado.getTipoCausa().setNombre(checklist.getTipoCausa().getNombre());
		}

		if (checklist.getCircunscripcion() != null) {
			checklistClonado.getCircunscripcion().setId(checklist.getCircunscripcion().getId());
			checklistClonado.getCircunscripcion().setNombre(checklist.getCircunscripcion().getNombre());
		}

		if (checklist.getInstancia() != null) {
			checklistClonado.getInstancia().setId(checklist.getInstancia().getId());
			checklistClonado.getInstancia().setNombre(checklist.getInstancia().getNombre());
		}

		checklistClonado.setVisibilidad("Privado");
		checklistClonado.setPlantilla(false);
		return guardar(checklistClonado, creador, ACTUALIZAR);
	}

	@Transactional
	public void editar(String id, String nombre, String idPais, String idProvincia, String idTipo,
			Collection<Item> items, Usuario editor) {
		Checklist checklist = new Checklist();
		checklist = checklistRepository.buscarPorId(id);

		validarChecklist(nombre, idPais, idProvincia, idTipo, checklist);

		TipoCausa respuesta = tipoCausaService.buscarPorId(idTipo);
		if (respuesta != null) {
			if (nombre != null && !nombre.isEmpty()) {
				checklist.setNombre(nombre);
			}

			if (idTipo != null && !idTipo.isEmpty()) {
				checklist.setTipoCausa(respuesta);
			}

			if (editor != null) {
				checklist.setEditor(editor);
			}
			guardar(checklist, getUsuarioSession(), ACTUALIZAR);

			itemService.editarr(items, checklist);
		} else {
			throw new PeticionIncorrectaException("El formulario debe estar vinculado a un tipo de causa.");
		}
	}

	@Transactional
	public Checklist guardar(Checklist checklist, Usuario usuario, String accion) {
		auditar(checklist, usuario, accion);
		return checklistRepository.save(checklist);
	}

	public MChecklist eliminarChecklist(String id, Usuario usuario) {
		Checklist checklist = buscarCheckListPorId(id);
		if (checklist == null) {
			throw new EventoRestException("El checklist que trata eliminar no se encuentra en la base de datos.");
		}

		checklist = guardar(checklist, usuario, SimpleService.ELIMINAR);
		return checklistConverter.entidadModelo(checklist);
	}
	
	public MChecklist cambiarVisibilidadChecklist(String id, Usuario usuario) {
		Checklist checklist = buscarCheckListPorId(id);
		if (checklist == null) {
			throw new EventoRestException("El checklist que trata de cambiar de visibilidad no se encuentra en la base de datos.");
		}
		if(checklist.getVisibilidad().equals("Privado")) {
			checklist.setVisibilidad("Publico");
		}else {
			checklist.setVisibilidad("Privado");
		}
		checklist = guardar(checklist, usuario, SimpleService.ACTUALIZAR);
		return checklistConverter.entidadModelo(checklist);
	}
	
	public Checklist buscarChecklist(String id) {
		Checklist checklist = buscarCheckListPorId(id);
		return checklist;
	}

	private void validarChecklist(String nombre, String idPais, String idProvincia, String idTipoCausa,
			Checklist checklist) {

		if (isStringNull(nombre)) {
			throw new PeticionIncorrectaException("El nombre del checklist no puede estar vacio.");
		}
		if (isStringNull(idPais)) {
			throw new PeticionIncorrectaException("El país la que pertenece el checklist no puede estar vacia.");
		}
		if (isStringNull(idProvincia)) {
			throw new PeticionIncorrectaException("La provincia a la que pertenece el checklist no puede estar vacia.");
		}
		if (isStringNull(idTipoCausa)) {
			throw new PeticionIncorrectaException("El tipo de causa al que pertenece el checklist no puede ser nulo.");
		} else {
			Checklist otro = buscarCheckListPorNombre(nombre);
			if ((checklist == null && otro != null)
					|| (checklist != null && otro != null && !checklist.getId().equals(otro.getId()))) {
				throw new PeticionIncorrectaException("El nombre ingresado ya se encuentra utilizado.");
			}
		}

	}

	public Checklist buscarCheckListPorId(String id) {
		return checklistRepository.buscarPorId(id);
	}

	public Checklist buscarCheckListPorNombre(String nombre) {
		return checklistRepository.buscarPornombre(nombre);
	}

	public Page<Checklist> buscarTodos(Pageable paginable, String q) {
		if (q == null || q.isEmpty()) {
			return checklistRepository.buscarTodos(paginable);
		} else {
			 q = "%"+q+"%";
			 List<Checklist> checklistFinal = new ArrayList<>();
			 List<Checklist> checklist = checklistRepository.buscarTodos(q);
			 if(checklist != null && !checklist.isEmpty()) {
				 checklistFinal.addAll(checklist);
			 }
			 List<Checklist> checklistProvincia = checklistRepository.buscarTodosProvincia(q);
			 if(checklistProvincia != null && !checklistProvincia.isEmpty()) {
				 for(Checklist c : checklistProvincia) {
					 if(!checklistFinal.contains(c)) {
						 checklistFinal.add(c);
					 }
				 }
			 }
			 List<Checklist> checklistC = checklistRepository.buscarTodosCircunscripcion(q);
			 if(checklistC != null && !checklistC.isEmpty()) {
				 for(Checklist cc : checklistC) {
					 if(!checklistFinal.contains(cc)) {
						 checklistFinal.add(cc);
					 }
				 }
			 }
			 List<Checklist> checklistM = checklistRepository.buscarTodosMateria(q);
			 if(checklistM != null && !checklistM.isEmpty()) {
				 for(Checklist mm : checklistM) {
					 if(!checklistFinal.contains(mm)) {
						 checklistFinal.add(mm);
					 }
				 }
			 }
			 List<Checklist> checklistI = checklistRepository.buscarTodosInstancia(q);
			 if(checklistI != null && !checklistI.isEmpty()) {
				 for(Checklist ii : checklistI) {
					 if(!checklistFinal.contains(ii)) {
						 checklistFinal.add(ii);
					 }
				 }
			 }
		     final int start = (int) paginable.getOffset();
		     final int end = Math.min(start + paginable.getPageSize(), checklistFinal.size());
		     
		     
		     Page<Checklist> checklistPage = new PageImpl<>(checklistFinal.subList(start, end), paginable, checklistFinal.size());
			return checklistPage;
		}
	}

	public Page<Checklist> buscarTodosPlantilla(Pageable paginable) {
		return checklistRepository.buscarTodosPlantilla(paginable);
	}

	public MChecklist buscarPorTipoDeCausaIdYTipoCheck(String idTipoCausa, TipoRolCausa tipoCheck) {
		return checklistConverter
				.entidadModelo(checklistRepository.buscarPorTipoDeCausaIdYTipoCheck(idTipoCausa, tipoCheck));
	}

	public List<MChecklist> buscarPorTipoDeCausaIdYTipoFuero(String idTipoCausa, TipoFuero tipoFuero,
			String usuarioId) {
		return checklistConverter.entidadesModelos(
				checklistRepository.buscarPorTipoDeCausaIdYTipoFuero(idTipoCausa, tipoFuero, usuarioId));
	}

	public List<MChecklist> buscarPorPaisId(String idPais) {
		return checklistConverter.entidadesModelos(checklistRepository.buscarPorPaisId(idPais));
	}

	public List<MChecklist> buscarPorProvinciaId(String idProvincia) {
		return checklistConverter.entidadesModelos(checklistRepository.buscarPorProvinciaId(idProvincia));
	}

	public List<MChecklist> buscarProvinciaPorPaisId(String idPais) {
		return checklistConverter.entidadesModelos(checklistRepository.buscarPorProvinciaPorIdPais(idPais));
	}
	
	public List<MChecklist> buscarPorUsuarioCreador(String idCreador) {
		return checklistConverter.entidadesModelos(checklistRepository.buscarPorUsuarioCreador(idCreador));
	}

}
