package com.quinto.liberium.service;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.quinto.liberium.entity.Auditable;
import com.quinto.liberium.entity.Usuario;


public class SimpleService {
	
	@Autowired
	private UsuarioService usuarioService;
	
	private Log log = LogFactory.getLog(this.getClass().getSimpleName());

	protected static final String ACTUALIZAR = "actualizar";
	protected static final String ELIMINAR = "eliminar";

	public Boolean isStringNull(String valor) {
		return valor == null || valor.isEmpty() || valor.equals("");
	}
	
	public Boolean limiteCaracteres(String valor) {
		return valor.length() > 250;
	}
	public void auditarCarga(Auditable auditable, Usuario usuario) {
		log.info("Auditando Creación de " + auditable.getClass().getSimpleName());

		auditable.setCreado(new Date());
		auditable.setCreador(usuario);
	}
	
	public void auditarModificacion(Auditable auditable, Usuario usuario) {
		log.info("Auditando Modificación de " + auditable.getClass().getSimpleName());

		auditable.setEditado(new Date());
		auditable.setEditor(usuario);
	}

	public void auditar(Auditable auditable, Usuario usuario, String accion) {
		log.info("Generando auditoria de:  " + auditable.getClass().getSimpleName());
		if(accion.equals(ELIMINAR)) {
			auditarBaja(auditable, usuario);
		}else {
			if (auditable.getCreado() != null) {
				auditarModificacion(auditable, usuario);
			} else {
				auditarCarga(auditable, usuario);
			}
		}
		log.info("Auditoria: " + auditable.getCreado());
	}

	public void auditarBaja(Auditable auditable, Usuario usuario) {
		log.info("Auditando Baja de " + auditable.getClass().getSimpleName());
		
		auditable.setEliminado(new Date());
		auditable.setEliminador(usuario);
	}
	
	public Usuario getUsuarioSession() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return usuarioService.buscarUsuarioPorEmail(auth.getName());
	}
}
